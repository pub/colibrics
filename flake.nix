{
  description = "Colibrics and Colibri2";

  inputs.nixpkgs.url = github:NixOS/nixpkgs/nixos-unstable;

  outputs = { self, nixpkgs }: {

    packages.x86_64-linux.default =
      # Notice the reference to nixpkgs here.
      with import nixpkgs { system = "x86_64-linux"; };
      stdenv.mkDerivation {
        name = "colibri2";
        buildInputs = [ ocamlPackages.dune_3 ocamlPackages.ocaml
                        coq coqPackages.flocq

                      ];
        src = self;
        buildPhase = "make";
        installPhase = "dune install --prefix=$out";
      };

  };
}
