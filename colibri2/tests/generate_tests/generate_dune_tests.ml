(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

let cmd = "%{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500"

let pp_opts fmt = function
  | None | Some "" -> ()
  | Some s ->
      let _ =
        List.iter
          (function
            | "" -> ()
            | s -> (
                match String.split_on_char '=' s with
                | [ option ] ->
                    if String.length option = 1 then
                      Format.fprintf fmt " -%s" option
                    else Format.fprintf fmt " --%s" option
                | [ option; value ] ->
                    if String.length option = 1 then
                      Format.fprintf fmt " -%s=%s" option value
                    else Format.fprintf fmt " --%s=%s" option value
                | _ ->
                    failwith
                      (Format.sprintf
                         "Expection options in the form [OPTION=VAL] or \
                          [OPTION], got %s."
                         s)))
          (String.split_on_char ' ' s)
      in
      ()

let check_status fmt ?options result file =
  Format.fprintf fmt
    "(rule (alias runtest) (action (run %s%a --check-status %s \n\
     --dont-print-result %%{dep:%s})) (package colibri2))\n"
    cmd pp_opts options result file;
  Format.fprintf fmt
    "(rule (alias runtest-learning) (action (run %s%a --check-status %s \
     --learning --dont-print-result %%{dep:%s})) (package colibri2))\n"
    cmd pp_opts options result file

let print_test fmt ?result ?options file =
  match result with
  | Some "model" ->
      Format.fprintf fmt
        "(rule (action (with-stdout-to %s.res (run %s%a --check-status sat \
         %%{dep:%s}))))\n"
        file cmd pp_opts options file;
      Format.fprintf fmt
        "(rule (alias runtest) (action (diff %s.oracle %s.res)) (package \
         colibri2))\n"
        file file
  | Some "both" ->
      check_status fmt ?options "unsat --negate-goal" file;
      check_status fmt ?options "sat" file
  | Some result -> check_status fmt ?options result file
  | None ->
      Format.fprintf fmt
        "(rule (alias runtest) (action (run %s%a --check-status colibri2 \
         --dont-print-result %%{dep:%s})) (package colibri2))\n"
        cmd pp_opts options file;
      Format.fprintf fmt
        "(rule (alias runtest-learning) (action (run %s%a --check-status \
         colibri2 --dont-print-result --learning %%{dep:%s})) (package \
         colibri2))\n"
        cmd pp_opts options file

let cli_term =
  let open Cmdliner in
  let aux dir result options_list =
    let files = Sys.readdir dir in
    Array.sort String.compare files;
    let files = Array.to_list files in
    let files =
      List.filter
        (fun f ->
          List.exists (Filename.check_suffix f) [ "cnf"; ".smt2"; ".psmt2" ])
        files
    in
    let cout = open_out (Filename.concat dir "dune.inc") in
    let fmt = Format.formatter_of_out_channel cout in
    if options_list = [] then List.iter (print_test ?result fmt) files
    else
      List.iter
        (fun options -> List.iter (print_test ?result ~options fmt) files)
        options_list;
    close_out cout
  in
  let dir = Arg.(required & pos 0 (some string) None & info []) in
  let result = Arg.(value & pos 1 (some string) None & info []) in
  let options_list =
    let doc =
      "A string containing cli options to pass to Colibri2. The cli options \
       should come as a string in which the options are separated by spaces \
       and don't have the - or -- as a prefix (- is added when the option is \
       composed of one letter, -- is addded otherwise). The options should be \
       written as [OPTION=VAL] when they expect values."
    in
    Arg.(value & opt_all string [] & info [ "options" ] ~doc)
  in
  Cmd.v (Cmd.info "run") Term.(const aux $ dir $ result $ options_list)

let () =
  match Cmdliner.Cmd.eval_value cli_term with
  | Ok (`Version | `Help | `Ok ()) -> exit 0
  | Error (`Parse | `Term | `Exn) -> exit 2
