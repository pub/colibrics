(set-logic ALL)

(declare-fun x () Real)
(declare-fun y () Real)

(assert (< 1 x 10))
(assert (and (< 1 y) (<= y 10)))

(define-fun f ((a Real)(b Real)) Real (+ (/ (/ (- (* x y) y) (+ y 1)) 12.0) 1))

(define-fun z1_1 () Real (f x y))
(define-fun z2_1 () Real (f y x))

(define-fun z1_2 () Real (f z1_1 z2_1))
(define-fun z2_2 () Real (f z2_1 z1_1))

(define-fun z1_3 () Real (f z1_2 z2_2))
(define-fun z2_3 () Real (f z2_2 z1_2))

(define-fun z1_4 () Real (f z1_3 z2_3))
(define-fun z2_4 () Real (f z2_3 z1_3))

(define-fun z1_5 () Real (f z1_4 z2_4))
(define-fun z2_5 () Real (f z2_4 z1_4))

(define-fun z1_6 () Real (f z1_5 z2_5))
(define-fun z2_6 () Real (f z2_5 z1_5))

(define-fun z1_7 () Real (f z1_6 z2_6))
(define-fun z2_7 () Real (f z2_6 z1_6))

(define-fun z1_8 () Real (f z1_7 z2_7))
(define-fun z2_8 () Real (f z2_7 z1_8))

(define-fun z1_9 () Real (f z1_8 z2_8))
(define-fun z2_9 () Real (f z2_8 z1_8))


(assert (! (and (< 0.0 z1_9 10.0) (< 0.0 z2_9 10.0))  :colibri2 goal))
(check-sat)
