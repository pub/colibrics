(set-logic ALL)

(declare-const a Real)
(declare-const b Real)
(declare-const c Real)

(assert (< 0 b))

(assert (<= 0.0 (- (* a b) (* c b))))

(assert (not (<= 0.0 (- a c))))

(check-sat)
