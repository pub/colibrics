(set-logic ALL)

(declare-sort t 0)
(declare-fun num (t) Int)
(declare-fun den (t) Int)

(declare-const r Int)
(declare-const a3 t)

(assert (< 0 (den a3)))

(assert
 (< (- (* (to_real r) (to_real (den a3))) (to_real (num a3))) (to_real(den a3))))

(assert (not (< (- (to_real r) (/ (to_real (num a3)) (to_real (den a3)))) 1.0)))

(check-sat)
