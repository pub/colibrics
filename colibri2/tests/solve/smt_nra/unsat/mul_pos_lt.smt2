(set-logic ALL)

(declare-const a Real)
(declare-const b Real)
(declare-const c Real)

(assert (< 0 b))

(assert (< (* a b) (* c b)))

(assert (not (< a c)))

(check-sat)
