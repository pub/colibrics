(set-logic ALL)

(declare-const x Real)
(declare-const y Real)
(declare-const v Real)
(declare-const z Real)
(declare-const w Real)

(assert (< 0 x))
(assert (< 0 y))
(assert (< 0 v))
(assert (< 0 z))
(assert (< 0 w))

;(assert (not (= y 0.0)))
;(assert (not (= v 0.0)))
;(assert (< 0.0 (* z w)))

(assert (= (* x y) (/ z (* y (* v v)))))
(assert (= (* y v) (colibri_sqrt (/ z w))))
(assert (not (= w x)))
(check-sat)

; &xy=zy^{-1}v^{-2} & yv = \sqrt{\frac{z}{w}} \\
