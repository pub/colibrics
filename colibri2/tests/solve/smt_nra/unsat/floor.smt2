(set-logic ALL)

(declare-const r Real)
(declare-const n Int)

(assert (<= 0 (- r (to_real n) )))
(assert (< (- r (to_real n)) 1))

(assert (distinct (to_int r) n))

(check-sat)
