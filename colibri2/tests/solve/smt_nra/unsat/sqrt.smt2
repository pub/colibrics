(set-logic ALL)

(assert (not (= 2.0 (* (colibri_sqrt 2.0) (colibri_sqrt 2.0)))))

(check-sat)
