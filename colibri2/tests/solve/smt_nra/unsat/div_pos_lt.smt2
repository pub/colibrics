(set-logic ALL)

(declare-const a Real)
(declare-const b Real)
(declare-const c Real)

(assert (< 0 b))

(assert (< (* a b) c))

(assert (not (< a (/ c b))))

(check-sat)
