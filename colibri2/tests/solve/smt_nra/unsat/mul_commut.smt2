(set-logic ALL)

(declare-const a Real)
(declare-const b Real)

(assert (not (= (* a b) (* b a))))

(check-sat)
