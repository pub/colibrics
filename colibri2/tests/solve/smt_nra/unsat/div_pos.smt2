(set-logic ALL)

(declare-const a Real)
(declare-const b Real)
(declare-const c Real)

(assert (< 0 b))
(assert (= (/ c b) a))

(assert (not (= (* a b) c)))

(check-sat)
