(set-logic ALL)

(assert (not (<= (colibri_sqrt 2.0) (colibri_sqrt 3.0))))

(check-sat)
