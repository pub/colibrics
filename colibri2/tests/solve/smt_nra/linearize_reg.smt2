(set-info :smt-lib-version 2.6)
(set-logic QF_NRA)
(set-info :status sat)

(declare-fun x0 () Real)
(declare-fun y0 () Real)
(declare-fun z0 () Real)

(declare-fun x1 () Real)
(declare-fun y1 () Real)
(declare-fun z1 () Real)

(assert (= z0 (* x0 y0)))
(assert (= z1 (* x1 y1)))

(assert (<= 2 x0 200))
(assert (<= 2 y0 200))
(assert (<= 2 z0 4000))

(assert (<= 30 x0 500))
(assert (<= 30 y0 500))
(assert (<= 30 z0 5000))

(assert (<= z0 z1))

(check-sat)
(get-model)
(exit)
