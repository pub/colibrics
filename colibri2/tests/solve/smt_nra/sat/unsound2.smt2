(set-logic QF_NRA)

(declare-const x Real)
(declare-const y Real)

(assert (and (= 0.0 (+ 1.0 x)) (= 0.0 (+ 1.0 (* x x y)))))

(check-sat)
