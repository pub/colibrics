(set-logic ALL)

(declare-const a Real)
(declare-const b Real)

(assert (= (* a b) (* b a)))

(check-sat)
