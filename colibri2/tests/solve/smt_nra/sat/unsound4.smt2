(set-logic QF_NRA)

(declare-const x Real)
(declare-const y Real)
(declare-const z Real)

(assert (and true (= 0.0 (+ 0.0 (+ y (* z x)))) (= 0.0 (+ 1 y)) (or false (and true (< x 0.0)))))

(check-sat)
