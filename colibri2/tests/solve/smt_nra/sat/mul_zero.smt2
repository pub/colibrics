(set-logic ALL)

(declare-fun a () Real)
(declare-fun b () Real)

(assert (= 0.0 (* a b)))
(assert (distinct a b))

(check-sat)
