(set-logic ALL)

(declare-const r Real)
(declare-const n Int)

(assert (<= 0 (- (to_real n) r)))
(assert (< (- (to_real n) r) 1))

(assert (= (- (to_int (- r))) n))

(check-sat)
