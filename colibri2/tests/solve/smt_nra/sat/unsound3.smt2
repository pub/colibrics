(set-logic QF_NRA)

(declare-fun x () Real)

(assert (and (= 0.0 (+ x 1.0)) (= 0.0 (+ 1 (* x x (- 1))))))

(check-sat)
