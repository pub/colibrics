(set-logic QF_NRA)

(declare-fun x () Real)

(assert (= 1 (* x x)))
(assert (< x 0.0))

(check-sat)
