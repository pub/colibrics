(set-logic ALL)

(declare-const a Real)
(declare-const b Real)
(declare-const c Real)
(declare-const d Real)

(assert (< 0 b))

(assert (< (* a b) (* c b)))

(assert (< a c))

(check-sat)
