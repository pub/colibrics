(set-logic ALL)

(declare-const a Real)
(declare-const b Real)
(declare-const c Real)
(declare-const d Real)
(declare-const e Real)

(assert (= (* a b c d e) (* c b a e d)))

(check-sat)
