(set-logic ALL)

(define-fun f ((x Real)) Real (* (colibri_sqrt 2.0) (+ x 1.0)))

(declare-fun r0 () Real)
(assert (= r0 0.0))

(declare-fun r1 () Real)
(assert (= r1 (f r0)))

(declare-fun r2 () Real)
(assert (= r2 (f r1)))

(declare-fun r3 () Real)
(assert (= r3 (f r2)))

(declare-fun r4 () Real)
(assert (= r4 (f r3)))

(declare-fun r5 () Real)
(assert (= r5 (f r4)))

(check-sat)
