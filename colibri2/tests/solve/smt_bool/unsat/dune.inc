(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:simplify_trivial_ites.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:simplify_trivial_ites.smt2})) (package colibri2))
