(set-logic QF_IDL)
(declare-const b1 Bool)
(declare-const b2 Bool)
(declare-const b3 Bool)
(declare-const b4 Bool)
(declare-const b5 Bool)
(declare-const b6 Bool)
(declare-const b7 Bool)
(declare-const b8 Bool)
(declare-const b9 Bool)
(declare-const b10 Bool)
(declare-const b11 Bool)
(declare-const b12 Bool)
(declare-const b13 Bool)
(assert (distinct
  false
  (ite
    (or
      (and b10 (or b1 b3))
      (ite (or b9 (and b6 b7) (and b2 b5)) false false))
    false
    (ite
      (ite (or (and b12 b4) (ite b11 b13 b13)) b8 false)
      false
      (ite false false false)
    )
  )
))
(check-sat)
