(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:anomaly_agetooold.cnf})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:anomaly_agetooold.cnf})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:modus_ponens.cnf})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:modus_ponens.cnf})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:pigeon-1.cnf})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:pigeon-1.cnf})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:pigeon-2.cnf})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:pigeon-2.cnf})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:pigeon-3.cnf})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:pigeon-3.cnf})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:pigeon-4.cnf})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:pigeon-4.cnf})) (package colibri2))
