(set-logic QF_IDL)

(declare-const a Int)
(declare-const b Int)

(assert (and (> (- b a) 1) (>= (- b a) 1)))

(check-sat)
