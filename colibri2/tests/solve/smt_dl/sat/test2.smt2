(set-logic QF_UFIDL)
(declare-fun S (Int) Int)
(declare-fun x () Int)
(assert (and (= x (S 0)) (or true (= x 1))))
(check-sat)
