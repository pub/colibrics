(set-logic QF_LRA)
(declare-fun k () Real)
(assert (and (< (/ (- 1) 1) k) (and (< k 1) (< 0 k))))
(check-sat)
