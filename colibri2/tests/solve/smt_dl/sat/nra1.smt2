(set-logic QF_LRA)
(declare-const s Real)
(assert (and (> s 1) (and (> 3.1 s) (> s 3))))
(check-sat)
