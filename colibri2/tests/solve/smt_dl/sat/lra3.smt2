(set-logic QF_LRA)
(declare-fun t () Real)
(assert (and (<= t 2) (< t 2) (> t 1)))
(check-sat)
