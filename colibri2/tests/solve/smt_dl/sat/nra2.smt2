(set-logic QF_LRA)
(declare-fun s () Real)
(assert (and (< (- 1) s) (and (> 0.0 s) (> s (- 1)))))
(check-sat)
