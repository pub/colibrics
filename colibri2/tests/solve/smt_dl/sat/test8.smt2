(set-logic QF_IDL)

(declare-const a Int)
(declare-const b Int)
(declare-const c Int)
(declare-const d Int)

(assert (and
  (= 1 (- c a))
  (>= (- b a) 1)
  (= 2 (- d c))
  (or false (> (- b d) 1) (> (- d b) 0))
))

(check-sat)
