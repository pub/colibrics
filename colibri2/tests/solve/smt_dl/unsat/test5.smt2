(set-logic QF_UFIDL)
(declare-fun S (Int) Int)
(declare-fun u (Int Int) Int)
(declare-fun y () Int)
(declare-fun y2 () Int)
(assert (and (= 1 (S 0)) (= 1 (S 1)) (= 0 (S y2)) (or (= y2 0) (= y2 1) false false) (= 0 (u (S 0) (ite (< y 1) 0 0))) ))
(check-sat)
