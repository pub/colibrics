(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --lra-dl --check-status unsat 
--dont-print-result %{dep:dl_useless_let.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --lra-dl --check-status unsat --learning --dont-print-result %{dep:dl_useless_let.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --lra-incr-dl --check-status unsat 
--dont-print-result %{dep:dl_useless_let.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --lra-incr-dl --check-status unsat --learning --dont-print-result %{dep:dl_useless_let.smt2})) (package colibri2))
