(set-logic QF_IDL)

(declare-const a Int)
(declare-const b Int)
(declare-const c Int)
(declare-const d Int)
(declare-const e Int)
(declare-const f Int)

(assert
  ;(let ((_1 f) (_2 d))
    (and
      (distinct a b)
      (distinct (- c d) 1)
      (> (- c d) 0)
      (< (- b e) 7)
      (> (- d e) 0)
      (< (- f e) 3)
      (> (- f e) 0)
      (distinct (- d f) 0)
      (= (- c a) (- 1))
      (distinct (- d f) (- 1))
      (= (- d b) (- f c))
    )
  ;)
)

(check-sat)
