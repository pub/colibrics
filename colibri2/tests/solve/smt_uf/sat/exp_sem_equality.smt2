(set-logic QF_UF)
(declare-sort S1 0)
(declare-sort S0 0)
(declare-fun p2 ( S0) Bool)
(declare-fun v1 () S0)
(assert 
 (let ((?n1 true))
 (let ((?n2 (p2 v1))) 
 (let ((?n3 (not ?n2))) 
 (let ((?n4 (xor ?n1 ?n3))) 
 (let ((?n5 (xor ?n1 ?n4))) ?n5
))))))
(check-sat)
(exit)
