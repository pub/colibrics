(set-logic QF_UF)
(declare-sort S2 0)
(declare-sort S1 0)
(declare-sort S0 0)
(declare-fun p2 ( S1 S1 S0) Bool)
(declare-fun f2 ( S1 S1) S0)
(declare-fun v1 () S1)
(declare-fun f1 ( S2 S2 S2) S1)
(declare-fun v2 () S2)
(declare-fun f3 ( S2 S1) S1)
(declare-fun f0 ( S1) S2)
(declare-fun v0 () S0)
(declare-fun p4 ( S2 S0) Bool)
(declare-fun f4 ( S0 S2 S0) S0)
(declare-fun p1 ( S1) Bool)
(assert
 (let ((?n8 (f2 v1 v1)))
 (let ((?n9 (= v0 ?n8)))
 (let ((?n4 (f1 v2 v2 v2)))
 (let ((?n1 (f4 v0 v2 v0)))
 (let ((?n2 (f4 ?n1 v2 ?n1)))
 (let ((?n3 (distinct ?n1 ?n2)))
 (let ((?n5 (ite ?n3 ?n4 v1)))
 (let ((?n6 (p1 ?n5)))
 (let ((?n10 (p4 v2 v0)))
 (let ((?n11 (or ?n9 ?n10)))
 (let ((?n12 (ite ?n9 ?n8 v0)))
 (let ((?n13 (ite ?n10 ?n12 ?n12)))
 (let ((?n14 (= ?n1 ?n13)))
 (let ((?n15 (f0 v1)))
 (let ((?n16 (f3 ?n15 v1)))
 (let ((?n17 (p2 ?n16 ?n4 ?n8)))
 (let ((?n18 (ite ?n14 ?n17 false)))
 (let ((?n19 (and ?n11 ?n18)))
 (let ((?n20 (or false ?n19)))
 (let ((?n21 (= ?n6 ?n20))) ?n21
)))))))))))))))))))))
(check-sat)
(exit)
