(set-logic QF_UF)
(declare-sort S0 0)
(declare-fun v0 () S0)
(declare-fun b1 () Bool)
(assert
 (let ((?n3 (distinct v0 v0)))
 (let ((?n4 (ite b1 ?n3 true)))
 (let ((?n5 (not ?n4)))
 (let ((?n6 (xor true ?n5))) ?n6
)))))
(check-sat)
(exit)
