(set-logic QF_UF)
(declare-sort S0 0)
(declare-fun p2 ( S0) Bool)
(declare-fun f4 ( S0 S0) S0)
(declare-fun f0 () S0)
(declare-fun f1 ( S0) S0)
(declare-fun v0 () S0)
(declare-fun p1 ( S0 S0) Bool)
(assert
 (let ((?n1 (f1 v0)))
 (let ((?n3 (f4 v0 f0)))
 (let ((?n4 (f4 ?n3 f0)))
 (let ((?n5 (distinct v0 ?n4)))
 (let ((?n6 (p2 ?n1)))
 (let ((?n7 (ite ?n6 ?n3 ?n4)))
 (let ((?n8 (p1 v0 ?n7)))
 (let ((?n10 (=> ?n8 false)))
 (let ((?n12 (= v0 ?n1)))
 (let ((?n13 (ite ?n12 ?n3 ?n1)))
 (let ((?n14 (p1 ?n13 ?n1)))
 (let ((?n15 (xor true ?n14)))
 (let ((?n18 (p1 f0 ?n1)))
 (let ((?n19 (p2 ?n4)))
 (let ((?n20 (=> ?n18 ?n19)))
 (let ((?n21 (= ?n15 ?n20)))
 (let ((?n22 (xor ?n10 ?n21)))
 (let ((?n23 (xor ?n5 ?n22))) ?n23
)))))))))))))))))))
(check-sat)
(exit)
