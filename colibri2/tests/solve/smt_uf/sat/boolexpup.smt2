(set-logic QF_UF)
(declare-sort S0 0)
(declare-fun p3 ( S0) Bool); false
(declare-fun v0 () S0)     ; @us_S0_0
(declare-fun v1 () S0)     ; @us_S0_1
(assert
 (let ((?n1 true))
 (let ((?n2 (distinct v0 v1)))
 (let ((?n3 (p3 v0)))
 (let ((?n4 (=> ?n2 ?n3)))
 (let ((?n5 (xor ?n1 ?n4))) ?n5
))))))
(check-sat)
(exit)
