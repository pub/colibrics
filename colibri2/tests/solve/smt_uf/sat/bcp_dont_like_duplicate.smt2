(set-logic QF_UF)
(declare-sort S1 0)
(declare-sort S0 0)
(declare-fun p3 ( S1 ) Bool)
(declare-fun v1 () S1)
(assert
 (let ((?n1 true))
 (let ((?n2 (p3 v1)))
 (let ((?n3 (and ?n2 ?n2)))
 (let ((?n4 (xor ?n1 ?n3))) ?n4
)))))
(check-sat)
(exit)
