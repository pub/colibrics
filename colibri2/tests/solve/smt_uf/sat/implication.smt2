(set-logic QF_UF)
(declare-sort S0 0)
(declare-fun v0 () S0)
(declare-fun v2 () S0)
(assert
 (let ((?n1 (= v0 v2)))
 (let ((?n2 (distinct v0 v0)))
 (let ((?n3 (=> ?n1 ?n2))) ?n3
))))
(check-sat)
(exit)
