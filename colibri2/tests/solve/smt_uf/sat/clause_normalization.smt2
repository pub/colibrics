(set-logic QF_UF)
(declare-sort S0 0)
(assert 
 (let ((?n1 false))
 (let ((?n2 (=> ?n1 ?n1))) ?n2
)))
(check-sat)
(exit)
