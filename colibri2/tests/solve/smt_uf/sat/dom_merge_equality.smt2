(set-logic QF_UF)
(declare-sort S0 0)
(declare-fun v3 () S0)
(declare-fun v2 () S0)
(declare-fun v0 () S0)
(declare-fun v1 () S0)
(assert
 (let ((?n2 (distinct v1 v0)))
 (let ((?n4 (ite false v0 v2)))
 (let ((?n6 (ite ?n2 ?n4 v3)))
 (let ((?n7 (distinct v2 ?n6))) ?n7
)))))
(check-sat)
(exit)
