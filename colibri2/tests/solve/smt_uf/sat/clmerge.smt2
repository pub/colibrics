(set-logic QF_UF)
(declare-sort S0 0)
(assert
 (let ((?n3 (= true false)))
 (let ((?n4 (ite ?n3 true true))) ?n4
)))
(check-sat)
(exit)
