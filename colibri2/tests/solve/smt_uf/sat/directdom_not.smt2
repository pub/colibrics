(set-logic QF_UF)
(declare-sort S0 0)
(declare-fun p2 ( S0 ) Bool)
(declare-fun v0 () S0)
(assert 
 (let ((?n2 (p2 v0))) 
 (let ((?n3 (not ?n2))) 
 (let ((?n4 (xor false ?n3))) ?n4
))))
(check-sat)
(exit)
