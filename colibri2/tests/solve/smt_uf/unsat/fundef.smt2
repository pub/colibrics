(set-logic ALL)

(define-fun foo ((x Int)) Bool (= x 1))

(assert (foo 0))

(check-sat)
