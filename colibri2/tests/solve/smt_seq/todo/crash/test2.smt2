(set-logic ALL)
(declare-sort E 0)
(declare-fun _1 () (Seq E))
(declare-fun _5 () (Seq E))
(declare-fun _14 () E)
(assert (= (seq.update (seq.update _1 0 (seq.unit (seq.nth _5 0))) 0 (seq.unit _14)) (seq.update (seq.update _1 0 (seq.unit (seq.nth _5 0))) 0 (seq.unit _14))))
(assert (= _5 (seq.update (seq.update _1 0 (seq.nth _5 0)) 0 (seq.unit _14))))
(check-sat)
