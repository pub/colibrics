(set-logic ALL)
(declare-sort E 0)
(declare-fun a () (Seq E))
(declare-fun i () Int)
(assert (= (seq.unit (seq.nth (ite false a (seq.++ (seq.extract a 0 0) (seq.extract a 0 (seq.len a)))) i)) (ite false (seq.unit (seq.nth a 0)) (seq.++ (seq.unit (seq.nth a 0)) (seq.extract (seq.unit (seq.nth a 0)) 0 i) (seq.extract (seq.++ a a (seq.unit (seq.nth a 0))) 0 (seq.len (seq.unit (seq.nth a 0))))))))
(check-sat)
