(set-logic ALL)
(declare-const x Int)
(declare-sort E 0)
(declare-fun a () (Seq E))
(declare-fun e () E)
(assert (= (ite false a (seq.++ (seq.unit (seq.nth a 0)) (seq.extract a 0 0) (seq.extract a 1 x))) (ite false a (seq.++ (seq.unit (seq.nth a 0)) (seq.extract a 0 0) (seq.extract (ite false a (seq.++ (seq.unit e) (seq.extract a 0 0) (seq.extract a 0 (seq.len a)))) 1 (- (seq.len (ite false a (seq.++ (seq.unit e) (seq.extract a 0 0) (seq.extract a 0 (- (seq.len a) 0))))) 1))))))
(check-sat)
