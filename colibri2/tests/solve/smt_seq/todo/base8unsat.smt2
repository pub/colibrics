(set-logic ALL)
(declare-const x Bool)
(declare-sort E 0)
(declare-fun a () (Seq E))
(assert (distinct
  (ite x
    (seq.unit (seq.nth a 0))
    (seq.unit (seq.nth a 0))
  )
  (seq.unit
    (seq.nth
      (seq.++
        a
        (seq.extract (seq.unit (seq.nth a 0)) 0 1))
    0))
))
(check-sat)
