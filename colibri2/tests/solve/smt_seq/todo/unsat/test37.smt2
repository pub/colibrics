(set-logic ALL)
(declare-sort E 0)
(declare-fun a () (Seq E))
(declare-fun i () Int)
(declare-fun i1 () Int)
(assert (distinct (seq.update (seq.update a i1 (seq.unit (seq.nth a i))) i (seq.unit (seq.nth a i1))) (seq.update (seq.update a i (seq.unit (seq.nth a i1))) i1 (seq.unit (seq.nth a i)))))
(check-sat)
