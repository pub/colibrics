(set-logic ALL)

(declare-sort E 0)
(declare-fun a () (Seq E))
(declare-fun b () (Seq E))
(declare-fun v () E)
(declare-fun i1 () Int)
(declare-fun i2 () Int)

(assert (= b (seq.unit (seq.nth (seq.update a i1 (seq.unit v)) 0))))

(assert (=
  (seq.update (seq.unit v) i1 (seq.nth a 0))
  (seq.update a i2 b)
))
(assert (=
  (seq.nth a 0)
  (seq.nth (seq.update a i2 b) i2)
))
(check-sat)
