(set-logic ALL)
(declare-sort E 0)
(define-fun d ((s (Seq E)) (i Int) (t (Seq E))) (Seq E) s)
(declare-fun a () (Seq E))
(declare-fun i () Int)
(assert (= (ite false (seq.unit (seq.nth a 0)) (seq.unit (seq.nth a 0))) (d (ite false a (seq.++ (seq.extract a 0 i) (seq.unit (seq.nth a 0)))) 0 (seq.unit (seq.nth a i)))))
(check-sat)
