(set-logic ALL)
(declare-sort E 0)
(declare-fun _8 () (Seq E))
(declare-fun e () E)
(assert (= (seq.update _8 0 (seq.unit e)) (seq.update _8 1 (seq.unit e))))
(assert (= (seq.update (seq.update _8 0 (seq.unit e)) 0 (seq.unit e)) (seq.update (seq.update _8 0 (seq.unit e)) 0 (seq.unit e))))
(assert (distinct (seq.update _8 0 e) (seq.update _8 0 (seq.nth (seq.update (seq.update _8 0 (seq.unit e)) 0 (seq.unit e)) 1))))
(check-sat)
