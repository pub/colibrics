(set-logic ALL)
(declare-sort E 0)
(declare-fun a () (Seq E))
(assert (distinct
  (seq.unit (seq.nth a 0))
  (seq.extract a 0 1)
))
  ;(seq.extract a 0 (seq.len (seq.unit (seq.len a))))))
(check-sat)
