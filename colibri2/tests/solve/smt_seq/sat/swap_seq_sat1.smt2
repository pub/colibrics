(set-logic ALL)
(declare-sort E 0)
(declare-fun a () (Seq E))
(declare-fun b () (Seq E))
(declare-fun e () E)
(declare-fun v () E)
(assert (=
  (seq.update b 0 (seq.unit e))
  (seq.update a 1 (seq.unit v))))
(assert (distinct
  (seq.update a 1 (seq.unit e))
  (seq.update a 0 (seq.unit e))))
(check-sat)
