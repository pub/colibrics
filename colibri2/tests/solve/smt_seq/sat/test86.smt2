(set-logic ALL)
(declare-sort E 0)
(declare-fun a () (Seq E))
(declare-fun v () E)
(declare-fun i () Int)

(assert (=
  (seq.update (seq.unit v) 0 (seq.nth a 0))
  (seq.update a i (seq.unit (seq.nth (seq.update a 0 (seq.unit v)) 0)))
))
(assert (= (seq.nth a 0) (seq.nth (seq.update a 0 (seq.unit v)) 0)))

(check-sat)
