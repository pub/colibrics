(set-logic ALL)

(declare-sort E 0)
(declare-fun a () (Seq E))
(declare-fun b () (Seq E))
(declare-fun c () (Seq E))
(declare-fun v () E)
(declare-fun i1 () Int)
(declare-fun i2 () Int)
(declare-fun i3 () Int)

(assert (= b (seq.update b i2 (seq.unit v))))
(assert (= (seq.update b 0 (seq.unit v)) (seq.update (seq.update b 0 (seq.unit v)) i1 (seq.nth (seq.update b 1 (seq.nth b 1)) 1))))
(assert (= (seq.update a 0 (seq.unit v)) (seq.update (seq.update b i2 (seq.unit v)) i1 (seq.unit v))))
(assert (= (seq.unit v) (seq.update c i3 (seq.nth b 0))))

(check-sat)
