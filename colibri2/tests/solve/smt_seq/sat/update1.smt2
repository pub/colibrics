(set-logic ALL)

(assert (= (seq.update (seq.unit 0) 0 (seq.unit 1)) (seq.unit 1)))

(check-sat)
