(set-logic ALL)
(declare-sort E 0)
(declare-fun a () (Seq E))
(declare-fun i () Int)
(declare-fun i3 () Int)
(assert (distinct (seq.update (seq.update a 0 (seq.unit (seq.nth a 0))) 0 (seq.unit (seq.nth (seq.update (seq.update a 0 (seq.unit (seq.nth a 0))) i (seq.unit (seq.nth a 0))) 0))) (seq.update (seq.update (seq.update a 0 (seq.unit (seq.nth a 0))) 1 (seq.unit (seq.nth a 0))) 0 (seq.unit (seq.nth (seq.update (seq.update a 0 (seq.unit (seq.nth a 0))) i3 (seq.unit (seq.nth a 0))) 0)))))
(check-sat)
