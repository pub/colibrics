(set-logic ALL)
(declare-sort E 0)
(declare-fun a () (Seq E))
(declare-fun i () Int)
(assert (distinct
  a
  (seq.update
    (seq.update
      (seq.update a 0 (seq.unit (seq.nth a 1)))
      0 (seq.unit (seq.nth a 0)))
    0 (seq.unit (seq.nth a i)))
))
(check-sat)
