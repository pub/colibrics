(set-logic ALL)

(declare-sort E 0)
(declare-const S (Seq E))

(assert (distinct S (seq.unit (seq.nth S 0))))

(check-sat)
