(set-logic ALL)
(declare-datatypes ((T@ 0)) ((($1))))
(declare-datatypes ((@ 0)) ((($))))
(declare-datatypes ((T 0)) (((M (l @) (|#| Int) (|#$| (Seq T@))))))
(declare-fun i () T)
(assert (= i
  (M
    (l i)
    (|#| i)
    (seq.extract
      (seq.update (|#$| i) 0 (|#$| i))
      0
      (seq.len (seq.unit (|#$| i)))
    )
  )
))
(check-sat)
