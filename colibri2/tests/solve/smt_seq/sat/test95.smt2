(set-logic ALL)
(declare-sort E 0)
(declare-fun a () (Seq E))
(declare-fun i () Int)
(assert (distinct (seq.update (seq.update (seq.update (seq.update (seq.update (seq.update a 0 (seq.nth a 0)) 0 (seq.nth a 1)) 0 (seq.update a 0 (seq.nth a 0))) i (seq.nth (seq.update a 0 (seq.nth a 0)) 0)) 0 (seq.nth (seq.update a i (seq.nth a 0)) i)) 0 (seq.nth (seq.update (seq.update (seq.update a 0 (seq.nth a 0)) 0 (seq.update a 0 (seq.nth a 0))) 0 (seq.nth (seq.update a 0 (seq.nth a 0)) 0)) i)) (seq.update (seq.update a 0 (seq.nth a 0)) 0 (seq.nth (seq.update (seq.update a 0 (seq.nth a 0)) 0 (seq.nth a 1)) 0))))
(check-sat)
