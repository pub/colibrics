(set-logic ALL)
(declare-sort E 0)
(declare-fun a () (Seq E))
(assert (distinct
  (seq.++ (seq.unit (seq.nth a 0)) (seq.unit (seq.nth a 0)))
  (ite true
    (seq.unit (seq.nth a 0))
    (seq.++
      (seq.extract (seq.unit (seq.nth a 0)) 0 0)
      (seq.unit (seq.nth a 0))))))
(check-sat)
