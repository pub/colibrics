(set-logic ALL)
(declare-sort E 0)
(declare-fun a () (NSeq E))
(assert (=
  (nseq.const 0 0 (nseq.get a 0))
  (nseq.concat a
    (nseq.relocate
      (nseq.concat
        (nseq.slice a 0 0)
        (nseq.relocate
          (nseq.const 0 0 (nseq.get a 0))
          (+ (nseq.last (nseq.slice a 0 0)) 1))
      )
    (+ (nseq.last a) 1))
  )
))
(check-sat)
