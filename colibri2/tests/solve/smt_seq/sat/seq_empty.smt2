(set-logic ALL)
(declare-fun n () (Seq Int))
(assert (and (= n (as seq.empty (Seq Int))) (exists ((i Int)) (= (seq.nth n i) (seq.nth n 0)))))
(check-sat)
