(set-logic ALL)
(declare-sort E 0)
(declare-fun a () (Seq E))
(declare-fun _5 () (Seq E))
(declare-fun _4 () E)
(declare-fun _6 () E)
(assert (= (seq.update _5 0 (seq.unit _6)) (seq.update (seq.update a 0 (seq.unit _4)) 0 (seq.unit _4))))
(check-sat)
