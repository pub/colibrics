(set-logic ALL)
(declare-sort E 0)
(declare-fun b () (Seq E))
(declare-fun v () E)
(declare-fun e () E)
(declare-fun a () (Seq E))
(declare-fun i () Int)
(assert (= (seq.update b i (seq.unit v)) (seq.update a 0 (seq.unit v))))
(assert (= (seq.update a 0 (seq.unit e)) (seq.update a i (seq.unit v))))
(check-sat)
