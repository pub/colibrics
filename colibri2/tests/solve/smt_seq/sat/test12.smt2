(set-logic ALL)
(declare-sort E 0)
(declare-fun a () (Seq E))
(declare-fun a2 () (Seq E))
(assert (= (seq.update (seq.update a 0 (seq.unit (seq.nth a2 0))) 0 (seq.unit (seq.nth a2 0))) (seq.update (seq.update a2 0 (seq.unit (seq.nth a2 0))) 0 (seq.unit (seq.nth (seq.unit (seq.nth a2 0)) 0)))))
(check-sat)
