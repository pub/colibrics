(set-logic ALL)
(declare-sort E 0)
(declare-fun a () (Seq E))
(declare-fun i () Int)
(assert (distinct
  (seq.update
    (seq.update
      (seq.update a 0 (seq.unit (seq.nth a 0))) 1
      (seq.unit (seq.nth (seq.update a 0 (seq.unit (seq.nth a 0))) i)))
    1
    (seq.unit (seq.nth (seq.update a 0 (seq.unit (seq.nth a 0))) 1)))
  (seq.update
    (seq.update a 0 (seq.unit (seq.nth a 0)))
    0
    (seq.unit (seq.nth (seq.update a 0 (seq.unit (seq.nth a 0))) 1)))
))
(check-sat)
;(get-model)