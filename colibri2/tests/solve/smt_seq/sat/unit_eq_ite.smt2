(set-logic ALL)

(declare-sort E 0)
(declare-fun S () (Seq E))
(declare-fun w () E)

(assert (= (seq.unit w) (ite true S (seq.extract S 0 0))))
; for some reason tries to generate values for the alt of the ite

(check-sat)
