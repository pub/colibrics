(set-logic ALL)

(assert (= (seq.++ seq.empty (seq.unit 1)) (seq.unit 1)))

(check-sat)
