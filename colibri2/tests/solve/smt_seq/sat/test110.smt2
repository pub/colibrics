(set-logic ALL)
(declare-sort E 0)
(declare-fun a () (Seq E))
(declare-fun i () Int)
(assert (=
  (seq.update a i (seq.nth a 0))
  (seq.update
    (seq.update (seq.update (seq.update a 0 (seq.nth a 0)) 1 (seq.nth a 0)) 0 (seq.nth (seq.update a 0 (seq.nth a 0)) 0))
    0
    (seq.nth (seq.update (seq.update a 0 (seq.nth a 0)) 0 (seq.nth a 0)) 0))))
(check-sat)
