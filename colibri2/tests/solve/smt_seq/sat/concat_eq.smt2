(set-logic ALL)
(declare-sort E 0)
(declare-fun a () (Seq E))
(assert (distinct
  (seq.++
    (seq.unit (seq.nth a 0))
    (seq.unit
      (seq.nth (seq.extract a 0 (seq.len (seq.unit (seq.nth a 0)))) 0)))
  (ite false
    (seq.unit (seq.nth a 0))
    (seq.++
      (seq.unit (seq.nth a 0))
      (seq.unit (seq.nth a 0))
      (seq.unit
        (seq.nth
          (ite (> 0 (seq.len a)) a (seq.extract a 0 (seq.len a)))
          0))))))
(check-sat)
