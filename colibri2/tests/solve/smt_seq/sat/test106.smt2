(set-logic ALL)
(declare-sort E 0)
(declare-fun a () (Seq E))
(declare-fun b () (Seq E))
(declare-fun e1 () E)
(declare-fun e2 () E)
(declare-fun i () Int)
(assert (= (seq.update b i (seq.unit e1)) (seq.update a 1 (seq.unit e1))))
(assert (distinct (seq.update b 1 (seq.unit e2)) (seq.update a 1 (seq.unit e2))))
(check-sat)
