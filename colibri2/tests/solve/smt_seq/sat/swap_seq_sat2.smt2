(set-logic ALL)
(declare-sort E 0)
(declare-fun a () (Seq E))
(declare-fun b () (Seq E))
(declare-fun i () Int)
(assert (=
  (seq.update
    a
    0
    (seq.unit (seq.nth (seq.unit (seq.nth b 0)) 0)))
  (seq.update
    (seq.update b i (seq.unit (seq.nth b 0)))
    0
    (seq.unit (seq.nth (seq.update b 0 (seq.unit (seq.nth a 0))) 0)))))

(assert (distinct a b))
(check-sat)
