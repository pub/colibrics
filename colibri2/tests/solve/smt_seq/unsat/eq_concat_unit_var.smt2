(set-logic ALL)
(declare-const c Bool)
(declare-sort E 0)
(declare-fun a () (Seq E))
(declare-fun b () (Seq E))
(declare-fun v () E)

(assert (= (seq.++ (seq.unit v) b) (seq.++ (seq.unit v) a)))

(assert (distinct a b))
(check-sat)
