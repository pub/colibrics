(set-logic ALL)
;A small step is kept, in order to check that learning works well
(set-option :max-steps-colibri2 600)
(declare-sort n 0)
(declare-fun n (n) Int)
(declare-sort u 0)
(declare-fun g (u) (NSeq n))
(declare-fun o ((Array Int n) Int Int) u)
(declare-const b (Array Int n))
(declare-const c (Array Int n))
(define-fun d ((_1 (Array Int n))) Bool true)
(declare-fun q (Int) (Array Int n))
(assert (or (and false (d b)) (and (d c) (d (nseq.content (g (o (q 1) 0 0)))) (exists ((_7 Int)) (exists ((r (Array Int n))) (and (exists ((_6 Int)) (distinct (n (select r 1)) (n (select b 1)))) (exists ((r2 (Array Int n))) (and (d r2) (exists ((o1 n)) (= r (store r2 0 o1))) (forall ((_8 Int)) (= (n (select r2 1)) (n (select b 1))))))))))))
(check-sat)
