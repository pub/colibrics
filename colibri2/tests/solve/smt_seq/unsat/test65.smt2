(set-logic ALL)
(declare-sort E 0)
(declare-fun a () (Seq E))
(assert (= (seq.unit (seq.nth a 0)) (seq.++ (seq.unit (seq.nth a 0)) (seq.++ (seq.unit (seq.nth a 0)) (seq.unit (seq.nth a 0)) (seq.unit (seq.nth a 0))))))
(check-sat)
