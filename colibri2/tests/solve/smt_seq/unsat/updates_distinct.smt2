(set-logic ALL)
(declare-sort E 0)
(declare-fun v () E)

(assert (=
  (seq.update (seq.unit v) 0 (seq.unit v))
  (seq.update (seq.unit v) 0 (seq.nth (seq.unit v) 0))
))
(assert (distinct v (seq.nth (seq.update (seq.unit v) 1 (seq.unit v)) 0)))

(check-sat)
