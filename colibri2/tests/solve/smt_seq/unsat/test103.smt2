(set-logic ALL)
(declare-sort E 0)
(declare-fun a () (Seq E))
(declare-fun b () (Seq E))
(declare-fun i () Int)
(declare-fun j () Int)
; a[ i<-b[i] ][j <- b[ i<-a[i] ][j]]
; b[ i<-a[i] ][j <- a[ i<-b[i] ][j]]
(assert (=
  (seq.update
    (seq.update a i (seq.unit (seq.nth b i)))
    j
    (seq.unit (seq.nth (seq.update b i (seq.unit (seq.nth a i))) j)))
  (seq.update
    (seq.update b i (seq.unit (seq.nth a i)))
    j
    (seq.unit (seq.nth (seq.update a i (seq.unit (seq.nth b i))) j)))))
(assert (distinct a b))
(check-sat)
