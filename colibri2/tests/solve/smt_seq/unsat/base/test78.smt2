
(set-logic ALL)

(declare-sort T 0)
(declare-const a (NSeq T))

(assert (= (nseq.first a) 0))
(assert (= (nseq.last a) 10))

(assert (distinct a (nseq.concat (nseq.slice a 0 5) (nseq.slice a 6 10))))

(check-sat)
