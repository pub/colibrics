(set-logic ALL)

(declare-datatypes ((B 0)) (((A1 (d (Seq (Seq Int)))))))
(declare-datatypes ((A 0)) (((A (l Int) (u Int)))))

(declare-fun I (A Int) Bool)
(declare-fun s () (Seq (Seq Int)))

(assert (forall ((r A) (i Int)) (= (I r i) (< 0 (u r)))))
(assert
  (and
    (= (A1 seq.empty) (A1 s))
    (not (forall ((i Int)) (not (I (A 0 (seq.len s)) 0)))))
)
(check-sat)
