(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat 
--dont-print-result %{dep:concat_units.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat --learning --dont-print-result %{dep:concat_units.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat 
--dont-print-result %{dep:swap_seq_set.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat --learning --dont-print-result %{dep:swap_seq_set.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat 
--dont-print-result %{dep:swap_with_set.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat --learning --dont-print-result %{dep:swap_with_set.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat 
--dont-print-result %{dep:concat_units.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat --learning --dont-print-result %{dep:concat_units.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat 
--dont-print-result %{dep:swap_seq_set.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat --learning --dont-print-result %{dep:swap_seq_set.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat 
--dont-print-result %{dep:swap_with_set.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat --learning --dont-print-result %{dep:swap_with_set.smt2})) (package colibri2))
