(set-info :smt-lib-version 2.6)
(set-logic ALL)
(set-info :status unsat)
(set-info :source |
Benchmarks used in the followin paper:
Big proof engines as little proof engines: new results on rewrite-based satisfiability procedure
Alessandro Armando, Maria Paola Bonacina, Silvio Ranise, Stephan Schulz.
PDPAR'05
http://www.ai.dist.unige.it/pdpar05/


|)
(set-info :category "crafted")

(declare-sort Element 0)
(declare-fun a1 () (Seq Element))
(declare-fun a2 () (Seq Element))
(declare-fun a3 () (Seq Element))
(declare-fun a4 () (Seq Element))
(declare-fun e1 () Element)
(declare-fun e2 () Element)
(declare-fun i () Int)
(assert (= a1 (seq.update a3 i e1)))
(assert (= a2 (seq.update a4 i e2)))
(assert (= e1 (seq.nth a4 i)))
(assert (= e2 (seq.nth a3 i)))
(assert (= a1 a2))
(assert (not (= a3 a4)))
(check-sat)
(exit)
