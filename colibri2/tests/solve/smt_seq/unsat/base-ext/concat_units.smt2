(set-logic ALL)
(declare-sort E 0)
(declare-const v E)
(assert (distinct
  (seq.++
    (seq.unit v)
    (seq.unit v))
  (seq.++
    (seq.unit v)
    (seq.extract (seq.unit v) 0 1))
))
(check-sat)
