(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:concat_units2.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:concat_units2.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:empty_seq.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:empty_seq.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:eq_concat_unit_var.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:eq_concat_unit_var.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:ite_concat_ext.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:ite_concat_ext.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:ite_concat_unit.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:ite_concat_unit.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:nseq_rebase_reg.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:nseq_rebase_reg.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:reg1.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:reg1.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:swap_seq_unsat1.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:swap_seq_unsat1.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:swap_seq_unsat2.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:swap_seq_unsat2.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:swap_seq_unsat3.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:swap_seq_unsat3.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:swap_seq_unsat4.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:swap_seq_unsat4.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:test103.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:test103.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:test105.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:test105.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:test107.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:test107.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:test108.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:test108.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:test65.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:test65.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:test84.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:test84.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:unit.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:unit.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:unit_eq_conc.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:unit_eq_conc.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:updates_distinct.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:updates_distinct.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat 
--dont-print-result %{dep:concat_units2.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat --learning --dont-print-result %{dep:concat_units2.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat 
--dont-print-result %{dep:empty_seq.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat --learning --dont-print-result %{dep:empty_seq.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat 
--dont-print-result %{dep:eq_concat_unit_var.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat --learning --dont-print-result %{dep:eq_concat_unit_var.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat 
--dont-print-result %{dep:ite_concat_ext.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat --learning --dont-print-result %{dep:ite_concat_ext.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat 
--dont-print-result %{dep:ite_concat_unit.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat --learning --dont-print-result %{dep:ite_concat_unit.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat 
--dont-print-result %{dep:nseq_rebase_reg.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat --learning --dont-print-result %{dep:nseq_rebase_reg.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat 
--dont-print-result %{dep:reg1.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat --learning --dont-print-result %{dep:reg1.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat 
--dont-print-result %{dep:swap_seq_unsat1.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat --learning --dont-print-result %{dep:swap_seq_unsat1.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat 
--dont-print-result %{dep:swap_seq_unsat2.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat --learning --dont-print-result %{dep:swap_seq_unsat2.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat 
--dont-print-result %{dep:swap_seq_unsat3.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat --learning --dont-print-result %{dep:swap_seq_unsat3.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat 
--dont-print-result %{dep:swap_seq_unsat4.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat --learning --dont-print-result %{dep:swap_seq_unsat4.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat 
--dont-print-result %{dep:test103.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat --learning --dont-print-result %{dep:test103.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat 
--dont-print-result %{dep:test105.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat --learning --dont-print-result %{dep:test105.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat 
--dont-print-result %{dep:test107.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat --learning --dont-print-result %{dep:test107.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat 
--dont-print-result %{dep:test108.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat --learning --dont-print-result %{dep:test108.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat 
--dont-print-result %{dep:test65.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat --learning --dont-print-result %{dep:test65.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat 
--dont-print-result %{dep:test84.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat --learning --dont-print-result %{dep:test84.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat 
--dont-print-result %{dep:unit.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat --learning --dont-print-result %{dep:unit.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat 
--dont-print-result %{dep:unit_eq_conc.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat --learning --dont-print-result %{dep:unit_eq_conc.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat 
--dont-print-result %{dep:updates_distinct.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat --learning --dont-print-result %{dep:updates_distinct.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat 
--dont-print-result %{dep:concat_units2.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat --learning --dont-print-result %{dep:concat_units2.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat 
--dont-print-result %{dep:empty_seq.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat --learning --dont-print-result %{dep:empty_seq.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat 
--dont-print-result %{dep:eq_concat_unit_var.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat --learning --dont-print-result %{dep:eq_concat_unit_var.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat 
--dont-print-result %{dep:ite_concat_ext.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat --learning --dont-print-result %{dep:ite_concat_ext.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat 
--dont-print-result %{dep:ite_concat_unit.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat --learning --dont-print-result %{dep:ite_concat_unit.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat 
--dont-print-result %{dep:nseq_rebase_reg.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat --learning --dont-print-result %{dep:nseq_rebase_reg.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat 
--dont-print-result %{dep:reg1.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat --learning --dont-print-result %{dep:reg1.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat 
--dont-print-result %{dep:swap_seq_unsat1.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat --learning --dont-print-result %{dep:swap_seq_unsat1.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat 
--dont-print-result %{dep:swap_seq_unsat2.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat --learning --dont-print-result %{dep:swap_seq_unsat2.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat 
--dont-print-result %{dep:swap_seq_unsat3.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat --learning --dont-print-result %{dep:swap_seq_unsat3.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat 
--dont-print-result %{dep:swap_seq_unsat4.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat --learning --dont-print-result %{dep:swap_seq_unsat4.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat 
--dont-print-result %{dep:test103.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat --learning --dont-print-result %{dep:test103.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat 
--dont-print-result %{dep:test105.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat --learning --dont-print-result %{dep:test105.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat 
--dont-print-result %{dep:test107.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat --learning --dont-print-result %{dep:test107.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat 
--dont-print-result %{dep:test108.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat --learning --dont-print-result %{dep:test108.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat 
--dont-print-result %{dep:test65.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat --learning --dont-print-result %{dep:test65.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat 
--dont-print-result %{dep:test84.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat --learning --dont-print-result %{dep:test84.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat 
--dont-print-result %{dep:unit.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat --learning --dont-print-result %{dep:unit.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat 
--dont-print-result %{dep:unit_eq_conc.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat --learning --dont-print-result %{dep:unit_eq_conc.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat 
--dont-print-result %{dep:updates_distinct.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat --learning --dont-print-result %{dep:updates_distinct.smt2})) (package colibri2))
