(set-logic ALL)
(declare-sort E 0)
(declare-fun a () (Seq E))
(declare-fun e () E)
(declare-fun i () Int)
(assert (=
  (seq.update a 0 (seq.unit e))
  (seq.update a 0 (seq.nth a 0))))
(assert (distinct
  (seq.update a i (seq.unit e))
  (seq.update
    (seq.update a i (seq.nth a 0))
    i
    (seq.nth a 0))))
(check-sat)