(set-logic ALL)
(assert (distinct
  (_ bv0 2)
  (ite (bvsgt (_ bv0 2) (_ bv0 2)) (_ bv1 2) (_ bv0 2))
))
(check-sat)

