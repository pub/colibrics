(set-logic ALL)

(declare-fun x1 () Real)
(declare-fun x2 () Real)

(assert (= x1 (colibri_floor x2)))

(assert (<= 2.5 x2 3.5))

(assert (! (<= 2 x1 3) :colibri2 goal))


(check-sat)
