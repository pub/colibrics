(set-logic ALL)

(declare-fun x1 () Real)
(declare-fun x2 () Real)

(assert (= x1 (colibri_ceil x2)))

(assert (<= (- 3.5) x2 (- 2.5)))

(assert (! (<= (- 3) x1 (- 2)) :colibri2 goal))


(check-sat)
