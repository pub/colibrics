(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status steplimitreached 
--dont-print-result %{dep:mjrty-Mjrty-mjrtyqtvc_5.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status steplimitreached --learning --dont-print-result %{dep:mjrty-Mjrty-mjrtyqtvc_5.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status steplimitreached 
--dont-print-result %{dep:wp_float_real_f.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status steplimitreached --learning --dont-print-result %{dep:wp_float_real_f.psmt2})) (package colibri2))
