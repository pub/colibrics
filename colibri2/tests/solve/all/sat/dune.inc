(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status sat 
--dont-print-result %{dep:div.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status sat --learning --dont-print-result %{dep:div.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status sat 
--dont-print-result %{dep:div_abs.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status sat --learning --dont-print-result %{dep:div_abs.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status sat 
--dont-print-result %{dep:div_abs2.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status sat --learning --dont-print-result %{dep:div_abs2.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status sat 
--dont-print-result %{dep:sqrt-reduced.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status sat --learning --dont-print-result %{dep:sqrt-reduced.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status sat 
--dont-print-result %{dep:test_sqrt_assert_KO_Why3_Colibri2_n_a.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status sat --learning --dont-print-result %{dep:test_sqrt_assert_KO_Why3_Colibri2_n_a.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status sat 
--dont-print-result %{dep:union-Union-is_singletonqtvc_2.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status sat --learning --dont-print-result %{dep:union-Union-is_singletonqtvc_2.psmt2})) (package colibri2))
