(set-logic ALL)
(set-info :smt-lib-version 2.6)
(declare-const x Real)
(declare-const y Real)

(assert
  (not (= (colibri_abs_real (/ x y))
          (/ (colibri_abs_real x) (colibri_abs_real y)))))
(check-sat)
