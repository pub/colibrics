(set-logic ALL)

(declare-fun x1 () Real)
(declare-fun x2 () Real)
(declare-fun x3 () Real)

(assert (<= 1.0 x1))
(assert (<= 1.0 x3))

(assert (= x3 (/ x1 x2)))

(assert (<= x2 0.0))


(check-sat)
