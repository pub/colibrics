(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unknown 
--dont-print-result %{dep:float_interval-GenericFloat-div_finite_rev_1.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unknown --learning --dont-print-result %{dep:float_interval-GenericFloat-div_finite_rev_1.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unknown 
--dont-print-result %{dep:trigger_exception.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unknown --learning --dont-print-result %{dep:trigger_exception.smt2})) (package colibri2))
