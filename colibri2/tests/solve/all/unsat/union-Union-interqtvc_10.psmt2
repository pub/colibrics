;; produced by local colibri2.drv ;;
(set-logic ALL)
(set-info :smt-lib-version 2.6)
(set-option :max-steps-colibri2 30000)
;;; SMT-LIB2: integer arithmetic
;;; SMT-LIB2: real arithmetic
(declare-sort string 0)

(declare-datatypes ((tuple0 0))
  (((Tuple0))))

(declare-datatypes ((option 1))
  ((par (a) ((None) (Some (Some_proj_1 a))))))

;; is_none
(define-fun is_none (par (a)
  ((o (option a))) Bool
  (ite ((_ is None) o) true false)))

;; is_none'spec
(assert (par (a)
  (forall ((o (option a))) (= (is_none o) (= o (as None (option a)))))))

;; add_div
(assert
  (forall ((x Real) (y Real) (z Real))
    (=> (not (= z 0.0)) (= (/ (+ x y) z) (+ (/ x z) (/ y z))))))

;; sub_div
(assert
  (forall ((x Real) (y Real) (z Real))
    (=> (not (= z 0.0)) (= (/ (- x y) z) (- (/ x z) (/ y z))))))

;; neg_div
(assert
  (forall ((x Real) (y Real))
    (=> (not (= y 0.0)) (= (/ (- x) y) (- (/ x y))))))

;; assoc_mul_div
(assert
  (forall ((x Real) (y Real) (z Real))
    (=> (not (= z 0.0)) (= (/ (* x y) z) (* x (/ y z))))))

;; assoc_div_mul
(assert
  (forall ((x Real) (y Real) (z Real))
    (=>
      (and (not (= y 0.0)) (not (= z 0.0)))
      (= (/ (/ x y) z) (/ x (* y z))))))

;; assoc_div_div
(assert
  (forall ((x Real) (y Real) (z Real))
    (=>
      (and (not (= y 0.0)) (not (= z 0.0)))
      (= (/ x (/ y z)) (/ (* x z) y)))))

;; Zero
(assert (= (to_real 0) 0.0))

;; One
(assert (= (to_real 1) 1.0))

;; Add
(assert
  (forall ((x Int) (y Int))
    (= (to_real (+ x y)) (+ (to_real x) (to_real y)))))

;; Sub
(assert
  (forall ((x Int) (y Int))
    (= (to_real (- x y)) (- (to_real x) (to_real y)))))

;; Mul
(assert
  (forall ((x Int) (y Int))
    (= (to_real (* x y)) (* (to_real x) (to_real y)))))

;; Neg
(assert (forall ((x Int)) (= (to_real (- x)) (- (to_real x)))))

;; Injective
(assert (forall ((x Int) (y Int)) (=> (= (to_real x) (to_real y)) (= x y))))

;; Monotonic
(assert
  (forall ((x Int) (y Int)) (=> (<= x y) (<= (to_real x) (to_real y)))))

;; ddivisible_using
(define-fun ddivisible_using ((a1 Int) (b Int) (c Int)) Bool
  (= a1 (* c b)))

;; ddivisible
(define-fun ddivisible ((a1 Int) (b Int)) Bool
  (exists ((d Int)) (ddivisible_using a1 b d)))

(declare-datatypes ((tuple2 2))
  ((par (a1 a2) ((Tuple2 (Tuple2_proj_1 a1)(Tuple2_proj_2 a2))))))

;; gcd
(declare-fun gcd (Int
  Int) Int)

(declare-datatypes ((tuple3 3))
  ((par (a3 a4
   a5) ((Tuple3 (Tuple3_proj_1 a3)(Tuple3_proj_2 a4)(Tuple3_proj_3 a5))))))

;; lcm
(declare-fun lcm (Int
  Int) Int)

(declare-sort infix_mngt 2)

;; infix @
(declare-fun infix_at (par (a6
  b)
  ((infix_mngt a6
  b)
  a6) b))

(declare-datatypes ((tuple4 4))
  ((par (a7 a8 a9
   a10) ((Tuple4
         (Tuple4_proj_1 a7)(Tuple4_proj_2 a8)(Tuple4_proj_3 a9)(Tuple4_proj_4 a10))))))

;; prime_to_one_another
(declare-fun prime_to_one_another (Int
  Int) Bool)

;; prime_to_one_another_refl
(assert (forall ((a11 Int)) (prime_to_one_another a11 a11)))

;; GCD1
(assert
  (forall ((a11 Int) (b1 Int) (c Int))
    (=> (prime_to_one_another a11 b1) (prime_to_one_another (gcd a11 c) b1))))

;; GCD2
(assert
  (forall ((a11 Int) (b1 Int) (c Int))
    (=> (prime_to_one_another a11 b1) (prime_to_one_another a11 (gcd b1 c)))))

;; GCD3
(assert
  (forall ((a11 Int) (b1 Int) (c Int))
    (=>
      (prime_to_one_another a11 b1)
      (=> (prime_to_one_another a11 c) (prime_to_one_another a11 (lcm b1 c))))))

;; GCD4
(assert
  (forall ((a11 Int) (b1 Int) (c Int))
    (=>
      (prime_to_one_another a11 b1)
      (=> (prime_to_one_another c b1) (prime_to_one_another (lcm a11 c) b1)))))

;; GCD_comm
(assert (forall ((a11 Int) (b1 Int)) (= (gcd a11 b1) (gcd b1 a11))))

(declare-sort t 0)

;; num
(declare-fun num (t) Int)

;; den
(declare-fun den (t) Int)

;; t'invariant
(assert
  (forall ((self t))
    (! (and (< 0 (den self)) (prime_to_one_another (num self) (den self))) :pattern (
    (den
      self)) :pattern ((num self)) )))

;; real
(define-fun real ((q t)) Real
  (/ (to_real (num q)) (to_real (den q))))

;; equal
(define-fun equal ((a11 t) (b1 t)) Bool
  (= (real a11) (real b1)))

;; equal_is_eq
(assert (forall ((a11 t) (b1 t)) (=> (equal a11 b1) (= a11 b1))))

;; infix <=
(define-fun infix_lseq ((a11 t) (b1 t)) Bool
  (<= (real a11) (real b1)))

;; infix <
(define-fun infix_ls ((a11 t) (b1 t)) Bool
  (< (real a11) (real b1)))

;; abs
(define-fun abs1 ((x Real)) Real
  (ite (>= x 0.0) x (- x)))

;; Abs_le
(assert
  (forall ((x Real) (y Real))
    (= (<= (abs1 x) y) (and (<= (- y) x) (<= x y)))))

;; Abs_pos
(assert (forall ((x Real)) (>= (abs1 x) 0.0)))

;; Abs_sum
(assert
  (forall ((x Real) (y Real)) (<= (abs1 (+ x y)) (+ (abs1 x) (abs1 y)))))

;; Abs_prod
(assert
  (forall ((x Real) (y Real)) (= (abs1 (* x y)) (* (abs1 x) (abs1 y)))))

;; triangular_inequality
(assert
  (forall ((x Real) (y Real) (z Real))
    (<= (abs1 (- x z)) (+ (abs1 (- x y)) (abs1 (- y z))))))

(declare-datatypes ((t1 0))
  (((Eq) (Lt) (Gt))))

;; infix +
(declare-fun infix_pl (t
  t) t)

;; infix +'spec
(assert
  (forall ((a11 t) (b1 t))
    (= (+ (real a11) (real b1)) (real (infix_pl a11 b1)))))

;; infix -
(declare-fun infix_mn (t
  t) t)

;; infix -'spec
(assert
  (forall ((a11 t) (b1 t))
    (= (- (real a11) (real b1)) (real (infix_mn a11 b1)))))

;; infix *
(declare-fun infix_as (t
  t) t)

;; infix *'spec
(assert
  (forall ((a11 t) (b1 t))
    (= (* (real a11) (real b1)) (real (infix_as a11 b1)))))

;; infix /
(declare-fun infix_sl (t
  t) t)

;; infix /'spec
(assert
  (forall ((a11 t) (b1 t))
    (=>
      (not (= (real b1) 0.0))
      (= (/ (real a11) (real b1)) (real (infix_sl a11 b1))))))

;; make
(declare-fun make (Int
  Int) t)

;; make_def
(assert
  (forall ((num1 Int) (den1 Int))
    (=>
      (not (= den1 0))
      (= (real (make num1 den1)) (/ (to_real num1) (to_real den1))))))

;; truncate
(declare-fun truncate (Real) Int)

;; Truncate_int
(assert (forall ((i Int)) (= (truncate (to_real i)) i)))

;; Truncate_down_pos
(assert
  (forall ((x Real))
    (=>
      (>= x 0.0)
      (and (<= (to_real (truncate x)) x) (< x (to_real (+ (truncate x) 1)))))))

;; Truncate_up_neg
(assert
  (forall ((x Real))
    (=>
      (<= x 0.0)
      (and (< (to_real (- (truncate x) 1)) x) (<= x (to_real (truncate x)))))))

;; Real_of_truncate
(assert
  (forall ((x Real))
    (and
      (<= (- x 1.0) (to_real (truncate x)))
      (<= (to_real (truncate x)) (+ x 1.0)))))

;; Truncate_monotonic
(assert
  (forall ((x Real) (y Real)) (=> (<= x y) (<= (truncate x) (truncate y)))))

;; Truncate_monotonic_int1
(assert
  (forall ((x Real) (i Int)) (=> (<= x (to_real i)) (<= (truncate x) i))))

;; Truncate_monotonic_int2
(assert
  (forall ((x Real) (i Int)) (=> (<= (to_real i) x) (<= i (truncate x)))))

;; Floor_int
(assert (forall ((i Int)) (= (to_int (to_real i)) i)))

;; Ceil_int
(assert (forall ((i Int)) (= (- (to_int (- (to_real i)))) i)))

;; Floor_down
(assert
  (forall ((x Real))
    (and (<= (to_real (to_int x)) x) (< x (to_real (+ (to_int x) 1))))))

;; Ceil_up
(assert
  (forall ((x Real))
    (and
      (< (to_real (- (- (to_int (- x))) 1)) x)
      (<= x (to_real (- (to_int (- x))))))))

;; Floor_monotonic
(assert
  (forall ((x Real) (y Real)) (=> (<= x y) (<= (to_int x) (to_int y)))))

;; Ceil_monotonic
(assert
  (forall ((x Real) (y Real))
    (=> (<= x y) (<= (- (to_int (- x))) (- (to_int (- y)))))))

(declare-datatypes ((t2 0))
  (((Strict) (Large))))

(declare-datatypes ((tqtqt 0))
  (((Sin (Sin_proj_1 t)(Sin_proj_2 tqtqt))
   (End (End_proj_1 t)(End_proj_2 t2)(End_proj_3 tqtqt)) (Inf))))

(declare-datatypes ((tqt 0))
  (((On (On_proj_1 tqtqt)) (Off (Off_proj_1 tqtqt)))))

;; length'
(define-fun-rec lengthqt ((l tqtqt)) Int
  (ite ((_ is Sin) l) (let ( (x (Sin_proj_2 l))) (+ (lengthqt x) 1)) 
    (ite ((_ is End) l) (let (  (x1 (End_proj_3 l))) (+ (lengthqt x1) 1)) 0)))

;; pos_length'
(assert (forall ((l tqtqt)) (>= (lengthqt l) 0)))

;; length
(define-fun length ((l tqt)) Int
  (ite ((_ is On) l) (let ((x2 (On_proj_1 l))) (+ (lengthqt x2) 1)) (let ((x3 (Off_proj_1 l))) 
                                                                    (lengthqt
                                                                    x3))))

;; lt_bound'
(define-fun-rec lt_boundqt ((x4 Real) (l tqtqt)) Bool
  (ite ((_ is Sin) l) (let ((x5 (Sin_proj_1 l))
                        (x6 (Sin_proj_2 l))) (and
                                               (< x4 (real x5))
                                               (lt_boundqt x4 x6))) (ite ((_ is End) l) 
                                                                    (let ((x7 (End_proj_1 l))
                                                                    
                                                                    (x8 (End_proj_3 l))) 
                                                                    (and
                                                                    (< x4 
                                                                    (real
                                                                    x7))
                                                                    (lt_boundqt
                                                                    x4
                                                                    x8))) true)))

;; lt_bound
(define-fun lt_bound ((x9 t) (l tqt)) Bool
  (ite ((_ is On) l) (let ((x10 (On_proj_1 l))) (lt_boundqt (real x9) x10)) 
    (let ((x11 (Off_proj_1 l))) (lt_boundqt (real x9) x11))))

;; ordered'
(define-fun-rec orderedqt ((l tqtqt)) Bool
  (ite ((_ is Sin) l) (let ((x12 (Sin_proj_1 l))
                        (x13 (Sin_proj_2 l))) (and
                                                (lt_boundqt (real x12) x13)
                                                (orderedqt x13))) (ite ((_ is End) l) 
                                                                    (let ((x14 (End_proj_1 l))
                                                                    
                                                                    (x15 (End_proj_3 l))) 
                                                                    (and
                                                                    (lt_boundqt
                                                                    (real
                                                                    x14)
                                                                    x15)
                                                                    (orderedqt
                                                                    x15))) true)))

;; ordered
(define-fun ordered ((l tqt)) Bool
  (ite ((_ is On) l) (let ((x16 (On_proj_1 l))) (orderedqt x16)) (let ((x17 (Off_proj_1 l))) 
                                                                   (orderedqt
                                                                    x17))))

(declare-datatypes ((tuple1 1))
  ((par (a11) ((Tuple1 (Tuple1_proj_1 a11))))))

(declare-sort t3 0)

;; a
(declare-fun a12 (t3) tqt)

;; t'invariant
(assert
  (forall ((self t3))
    (! (and (ordered (a12 self)) (not (= (a12 self) (Off Inf)))) :pattern (
    (a12
      self)) )))

;; cmp
(define-fun cmp ((x18 Real) (b1 t2) (y Real)) Bool
  (ite ((_ is Large) b1) (<= x18 y) (< x18 y)))

;; mem_on
(define-fun-rec mem_on ((x18 Real) (l tqtqt)) Bool
  (ite ((_ is Sin) l) (let ((x19 (Sin_proj_1 l))
                        (x20 (Sin_proj_2 l))) (or
                                                (< x18 (real x19))
                                                (and
                                                  (< (real x19) x18)
                                                  (mem_on x18 x20)))) 
    (ite ((_ is End) l) (let ((x21 (End_proj_1 l))
                          (x22 (End_proj_2 l))
                          (x23 (End_proj_3 l))) (or
                                                  (not
                                                    (cmp (real x21) x22 x18))
                                                  (and
                                                    (cmp (real x21) x22 x18)
                                                    (not (mem_on x18 x23))))) true)))

;; mem
(define-fun mem ((x24 Real) (l tqt)) Bool
  (ite ((_ is On) l) (let ((x25 (On_proj_1 l))) (mem_on x24 x25)) (let ((x26 (Off_proj_1 l))) 
                                                                    (not
                                                                    (mem_on
                                                                    x24
                                                                    x26)))))

;; mem_if
(define-fun-rec mem_if ((if_ Bool) (x27 Real) (l tqtqt)) Bool
  (ite ((_ is Sin) l) (let ((x28 (Sin_proj_1 l))
                        (x29 (Sin_proj_2 l))) (ite (< (real x28) x27)
                                                (mem_if if_ x27 x29)
                                                (ite (= if_ true)
                                                  (< x27 (real x28))
                                                  (= x27 (real x28))))) 
    (ite ((_ is End) l) (let ((x30 (End_proj_1 l))
                          (x31 (End_proj_2 l))
                          (x32 (End_proj_3 l))) (ite (cmp (real x30) x31 x27)
                                                  (mem_if (not if_) x27 x32)
                                                  (= if_ true))) (= if_ true))))

;; mem_if_def
(assert
  (forall ((if_ Bool) (x33 Real) (l tqtqt))
    (=
      (mem_if if_ x33 l)
      (ite (= if_ true) (mem_on x33 l) (not (mem_on x33 l))))))

;; u
(declare-fun u () t3)

;; v
(declare-fun v () t3)

;; ifu
(declare-fun ifu () Bool)

;; ifv
(declare-fun ifv () Bool)

;; u
(declare-fun u1 () tqtqt)

;; v
(declare-fun v1 () tqtqt)

;; Requires
(assert (orderedqt u1))

;; Requires
(assert (orderedqt v1))

;; result
(declare-fun result () tqtqt)

;; o
(declare-fun o () t1)

;; l
(declare-fun l () tqtqt)

;; l
(declare-fun l1 () tqtqt)

;; l
(declare-fun l2 () tqtqt)

;; o
(declare-fun o1 () t1)

;; l
(declare-fun l3 () tqtqt)

;; o
(declare-fun o2 () t2)

;; l
(declare-fun l4 () tqtqt)

;; l
(declare-fun l5 () tqtqt)

;; o
(declare-fun o3 () t1)

;; l
(declare-fun l6 () tqtqt)

;; b
(declare-fun b1 () t2)

;; l
(declare-fun l7 () tqtqt)

;; l
(declare-fun l8 () tqtqt)

;; o
(declare-fun o4 () t1)

;; l
(declare-fun l9 () tqtqt)

;; o
(declare-fun o5 () t2)

;; l
(declare-fun l10 () tqtqt)

;; l
(declare-fun l11 () tqtqt)

;; x
(declare-fun x33 () t)

;; x
(declare-fun x34 () tqtqt)

;; H
(assert (= v1 (Sin x33 x34)))

;; x
(declare-fun x35 () t)

;; x
(declare-fun x36 () t2)

;; x
(declare-fun x37 () tqtqt)

;; H
(assert (= u1 (End x35 x36 x37)))

;; Ensures
(assert
  (let ((subject o1))
    (ite ((_ is Eq) subject) (equal x35 x33) (ite ((_ is Lt) subject) 
                                               (infix_ls
                                                 x35
                                                 x33) (infix_ls x33 x35)))))

;; H
(assert (= o1 Eq))

;; H
(assert (lt_boundqt (real x35) x34))

;; H
(assert (forall ((r Real)) (=> (<= r (real x35)) (mem_on r x37))))

;; H
(assert (orderedqt l3))

;; H
(assert
  (forall ((x38 Real))
    (=
      (and (mem_if (not ifu) x38 x37) (mem_if ifv x38 x34))
      (mem_if (and (not ifu) ifv) x38 l3))))

;; H
(assert
  (forall ((q Real))
    (=> (lt_boundqt q x37) (=> (lt_boundqt q x34) (lt_boundqt q l3)))))

;; H
(assert (not (= ifv true)))

;; H
(assert (= x36 Large))

;; H
(assert (= ifu true))

;; H
(assert (= result l3))

;; Ensures
(assert (orderedqt result))

;; x
(declare-fun x38 () Real)

;; H
(assert (mem_if ifu x38 u1))

;; H
(assert (mem_if ifv x38 v1))

;; h
(assert (cmp (real x35) x36 x38))

;; h
(assert (mem_if (not ifu) x38 x37))

;; h
(assert (mem_if ifv x38 x34))

;; Goal inter'vc
;; File "/home/bobot/Sources/colibrics/src_common/union.mlw", line 269, characters 8-13
(assert
  (not (mem_if (and ifu ifv) x38 result)))

(check-sat)
