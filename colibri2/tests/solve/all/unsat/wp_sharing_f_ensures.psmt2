;;(* WP Task for Prover Colibri2,n/a *)
;; produced by local colibri2.drv ;;
(set-logic ALL)
(set-info :smt-lib-version 2.6)
(set-option :max-steps-colibri2 14000)
;;; SMT-LIB2: integer arithmetic
;;; SMT-LIB2: real arithmetic
(declare-sort string 0)

(declare-datatypes ((tuple0 0))
  (((Tuple0))))

;; "abs"
(define-fun abs1 ((x Int)) Int
  (ite (>= x 0) x (- x)))

;; "Abs_le"
(assert
  (forall ((x Int) (y Int)) (= (<= (abs1 x) y) (and (<= (- y) x) (<= x y)))))

;; "Abs_pos"
(assert (forall ((x Int)) (>= (abs1 x) 0)))

;; "div"
(declare-fun div1 (Int
  Int) Int)

;; "mod"
(declare-fun mod1 (Int
  Int) Int)

;; "Div_mod"
(assert
  (forall ((x Int) (y Int))
    (=> (not (= y 0)) (= x (+ (* y (div1 x y)) (mod1 x y))))))

;; "Div_bound"
(assert
  (forall ((x Int) (y Int))
    (=> (and (>= x 0) (> y 0)) (and (<= 0 (div1 x y)) (<= (div1 x y) x)))))

;; "Mod_bound"
(assert
  (forall ((x Int) (y Int))
    (=>
      (not (= y 0))
      (and (< (- (abs1 y)) (mod1 x y)) (< (mod1 x y) (abs1 y))))))

;; "Div_sign_pos"
(assert
  (forall ((x Int) (y Int)) (=> (and (>= x 0) (> y 0)) (>= (div1 x y) 0))))

;; "Div_sign_neg"
(assert
  (forall ((x Int) (y Int)) (=> (and (<= x 0) (> y 0)) (<= (div1 x y) 0))))

;; "Mod_sign_pos"
(assert
  (forall ((x Int) (y Int))
    (=> (and (>= x 0) (not (= y 0))) (>= (mod1 x y) 0))))

;; "Mod_sign_neg"
(assert
  (forall ((x Int) (y Int))
    (=> (and (<= x 0) (not (= y 0))) (<= (mod1 x y) 0))))

;; "Rounds_toward_zero"
(assert
  (forall ((x Int) (y Int))
    (=> (not (= y 0)) (<= (abs1 (* (div1 x y) y)) (abs1 x)))))

;; "Div_1"
(assert (forall ((x Int)) (= (div1 x 1) x)))

;; "Mod_1"
(assert (forall ((x Int)) (= (mod1 x 1) 0)))

;; "Div_inf"
(assert
  (forall ((x Int) (y Int)) (=> (and (<= 0 x) (< x y)) (= (div1 x y) 0))))

;; "Mod_inf"
(assert
  (forall ((x Int) (y Int)) (=> (and (<= 0 x) (< x y)) (= (mod1 x y) x))))

;; "Div_mult"
(assert
  (forall ((x Int) (y Int) (z Int))
    (! (=>
         (and (> x 0) (and (>= y 0) (>= z 0)))
         (= (div1 (+ (* x y) z) x) (+ y (div1 z x)))) :pattern ((div1
                                                                  (+ (* x y) z)
                                                                  x)) )))

;; "Mod_mult"
(assert
  (forall ((x Int) (y Int) (z Int))
    (! (=>
         (and (> x 0) (and (>= y 0) (>= z 0)))
         (= (mod1 (+ (* x y) z) x) (mod1 z x))) :pattern ((mod1
                                                            (+ (* x y) z)
                                                            x)) )))

;; "add_div"
(assert
  (forall ((x Real) (y Real) (z Real))
    (=> (not (= z 0.0)) (= (/ (+ x y) z) (+ (/ x z) (/ y z))))))

;; "sub_div"
(assert
  (forall ((x Real) (y Real) (z Real))
    (=> (not (= z 0.0)) (= (/ (- x y) z) (- (/ x z) (/ y z))))))

;; "neg_div"
(assert
  (forall ((x Real) (y Real))
    (=> (not (= y 0.0)) (= (/ (- x) y) (- (/ x y))))))

;; "assoc_mul_div"
(assert
  (forall ((x Real) (y Real) (z Real))
    (=> (not (= z 0.0)) (= (/ (* x y) z) (* x (/ y z))))))

;; "assoc_div_mul"
(assert
  (forall ((x Real) (y Real) (z Real))
    (=>
      (and (not (= y 0.0)) (not (= z 0.0)))
      (= (/ (/ x y) z) (/ x (* y z))))))

;; "assoc_div_div"
(assert
  (forall ((x Real) (y Real) (z Real))
    (=>
      (and (not (= y 0.0)) (not (= z 0.0)))
      (= (/ x (/ y z)) (/ (* x z) y)))))

;; "infix +."
(define-fun infix_pldt ((x Real) (y Real)) Real
  (+ x y))

;; "infix -."
(define-fun infix_mndt ((x Real) (y Real)) Real
  (- x y))

;; "infix *."
(define-fun infix_asdt ((x Real) (y Real)) Real
  (* x y))

;; "infix /."
(define-fun infix_sldt ((x Real) (y Real)) Real
  (/ x y))

;; "prefix -."
(define-fun prefix_mndt ((x Real)) Real
  (- x))

;; "inv"
(define-fun inv ((x Real)) Real
  (/ 1.0 x))

;; "infix <=."
(define-fun infix_lseqdt ((x Real) (y Real)) Bool
  (<= x y))

;; "infix >=."
(define-fun infix_gteqdt ((x Real) (y Real)) Bool
  (>= x y))

;; "infix <."
(define-fun infix_lsdt ((x Real) (y Real)) Bool
  (< x y))

;; "infix >."
(define-fun infix_gtdt ((x Real) (y Real)) Bool
  (> x y))

;; "Zero"
(assert (= (to_real 0) 0.0))

;; "One"
(assert (= (to_real 1) 1.0))

;; "Add"
(assert
  (forall ((x Int) (y Int))
    (= (to_real (+ x y)) (+ (to_real x) (to_real y)))))

;; "Sub"
(assert
  (forall ((x Int) (y Int))
    (= (to_real (- x y)) (- (to_real x) (to_real y)))))

;; "Mul"
(assert
  (forall ((x Int) (y Int))
    (= (to_real (* x y)) (* (to_real x) (to_real y)))))

;; "Neg"
(assert (forall ((x Int)) (= (to_real (- x)) (- (to_real x)))))

;; "Injective"
(assert (forall ((x Int) (y Int)) (=> (= (to_real x) (to_real y)) (= x y))))

;; "Monotonic"
(assert
  (forall ((x Int) (y Int)) (=> (<= x y) (<= (to_real x) (to_real y)))))

;; "eqb"
(declare-fun eqb (par (a)
  (a
  a) Bool))

;; "eqb"
(assert (par (a) (forall ((x a) (y a)) (= (= (eqb x y) true) (= x y)))))

;; "neqb"
(declare-fun neqb (par (a)
  (a
  a) Bool))

;; "neqb"
(assert (par (a)
  (forall ((x a) (y a)) (= (= (neqb x y) true) (not (= x y))))))

;; "zlt"
(declare-fun zlt (Int
  Int) Bool)

;; "zleq"
(declare-fun zleq (Int
  Int) Bool)

;; "zlt"
(assert (forall ((x Int) (y Int)) (= (= (zlt x y) true) (< x y))))

;; "zleq"
(assert (forall ((x Int) (y Int)) (= (= (zleq x y) true) (<= x y))))

;; "rlt"
(declare-fun rlt (Real
  Real) Bool)

;; "rleq"
(declare-fun rleq (Real
  Real) Bool)

;; "rlt"
(assert (forall ((x Real) (y Real)) (= (= (rlt x y) true) (infix_lsdt x y))))

;; "rleq"
(assert
  (forall ((x Real) (y Real)) (= (= (rleq x y) true) (infix_lseqdt x y))))

;; "real_of_int"
(define-fun real_of_int ((x Int)) Real
  (to_real x))

;; "c_euclidian"
(assert
  (forall ((n Int) (d Int))
    (! (=> (not (= d 0)) (= n (+ (* (div1 n d) d) (mod1 n d)))) :pattern (
    (div1
      n
      d)
    (mod1 n d)) )))

;; "cmod_remainder"
(assert
  (forall ((n Int) (d Int))
    (! (and
         (=> (>= n 0) (=> (> d 0) (and (<= 0 (mod1 n d)) (< (mod1 n d) d))))
         (and
           (=>
             (<= n 0)
             (=> (> d 0) (and (< (- d) (mod1 n d)) (<= (mod1 n d) 0))))
           (and
             (=>
               (>= n 0)
               (=> (< d 0) (and (<= 0 (mod1 n d)) (< (mod1 n d) (- d)))))
             (=>
               (<= n 0)
               (=> (< d 0) (and (< d (mod1 n d)) (<= (mod1 n d) 0))))))) :pattern (
    (mod1
      n
      d)) )))

;; "cdiv_neutral"
(assert (forall ((a1 Int)) (! (= (div1 a1 1) a1) :pattern ((div1 a1 1)) )))

;; "cdiv_inv"
(assert
  (forall ((a1 Int))
    (! (=> (not (= a1 0)) (= (div1 a1 a1) 1)) :pattern ((div1 a1 a1)) )))

;; "cdiv_closed_remainder"
(assert
  (forall ((a1 Int) (b Int) (n Int))
    (=>
      (<= 0 a1)
      (=>
        (<= 0 b)
        (=>
          (and (<= 0 (- b a1)) (< (- b a1) n))
          (=> (= (mod1 a1 n) (mod1 b n)) (= a1 b)))))))

(declare-sort infix_mngt 2)

;; "infix @"
(declare-fun infix_at (par (a1
  b)
  ((infix_mngt a1
  b)
  a1) b))

;; "get"
(define-fun get (par (a
  b1)
  ((f (infix_mngt a b1)) (x a)) b1
  (infix_at f x)))

;; "set"
(declare-fun set (par (a
  b1)
  ((infix_mngt a
  b1)
  a
  b1) (infix_mngt a
  b1)))

;; "set'def"
(assert (par (a b1)
  (forall ((f (infix_mngt a b1)) (x a) (v b1) (y a))
    (= (infix_at (set f x v) y) (ite (= y x) v (infix_at f y))))))

;; "mixfix []"
(define-fun mixfix_lbrb (par (a
  b1)
  ((f (infix_mngt a b1)) (x a)) b1
  (infix_at f x)))

;; "mixfix [<-]"
(define-fun mixfix_lblsmnrb (par (a
  b1)
  ((f (infix_mngt a b1)) (x a) (v b1)) (infix_mngt a b1)
  (set f x v)))

(declare-datatypes ((addr 0))
  (((addrqtmk (base Int)(offset Int)))))

;; "addr_le"
(declare-fun addr_le (addr
  addr) Bool)

;; "addr_lt"
(declare-fun addr_lt (addr
  addr) Bool)

;; "addr_le_bool"
(declare-fun addr_le_bool (addr
  addr) Bool)

;; "addr_lt_bool"
(declare-fun addr_lt_bool (addr
  addr) Bool)

;; "addr_le_def"
(assert
  (forall ((p addr) (q addr))
    (! (=>
         (= (base p) (base q))
         (= (addr_le p q) (<= (offset p) (offset q)))) :pattern ((addr_le
                                                                   p
                                                                   q)) )))

;; "addr_lt_def"
(assert
  (forall ((p addr) (q addr))
    (! (=> (= (base p) (base q)) (= (addr_lt p q) (< (offset p) (offset q)))) :pattern (
    (addr_lt
      p
      q)) )))

;; "addr_le_bool_def"
(assert
  (forall ((p addr) (q addr))
    (! (= (addr_le p q) (= (addr_le_bool p q) true)) :pattern ((addr_le_bool
                                                                 p
                                                                 q)) )))

;; "addr_lt_bool_def"
(assert
  (forall ((p addr) (q addr))
    (! (= (addr_lt p q) (= (addr_lt_bool p q) true)) :pattern ((addr_lt_bool
                                                                 p
                                                                 q)) )))

;; "null"
(define-fun null () addr
  (addrqtmk 0 0))

;; "global"
(define-fun global ((b2 Int)) addr
  (addrqtmk b2 0))

;; "shift"
(define-fun shift ((p addr) (k Int)) addr
  (addrqtmk (base p) (+ (offset p) k)))

;; "included"
(define-fun included ((p addr) (a2 Int) (q addr) (b2 Int)) Bool
  (=>
    (> a2 0)
    (and
      (>= b2 0)
      (and
        (= (base p) (base q))
        (and
          (<= (offset q) (offset p))
          (<= (+ (offset p) a2) (+ (offset q) b2)))))))

;; "separated"
(define-fun separated ((p addr) (a2 Int) (q addr) (b2 Int)) Bool
  (or
    (<= a2 0)
    (or
      (<= b2 0)
      (or
        (not (= (base p) (base q)))
        (or
          (<= (+ (offset q) b2) (offset p))
          (<= (+ (offset p) a2) (offset q)))))))

;; "eqmem"
(define-fun eqmem (par (a)
  ((m1 (infix_mngt addr a)) (m2 (infix_mngt addr a)) (p addr) (a2 Int)) Bool
  (forall ((q addr))
    (! (=> (included q 1 p a2) (= (mixfix_lbrb m1 q) (mixfix_lbrb m2 q))) :pattern (
    (mixfix_lbrb
      m1
      p)) :pattern ((mixfix_lbrb m2 q)) ))))

;; "havoc"
(declare-fun havoc (par (a)
  ((infix_mngt addr
  a)
  (infix_mngt addr
  a)
  addr
  Int) (infix_mngt addr
  a)))

;; "valid_rw"
(define-fun valid_rw ((m (infix_mngt Int Int)) (p addr) (n Int)) Bool
  (=>
    (> n 0)
    (and
      (< 0 (base p))
      (and (<= 0 (offset p)) (<= (+ (offset p) n) (mixfix_lbrb m (base p)))))))

;; "valid_rd"
(define-fun valid_rd ((m (infix_mngt Int Int)) (p addr) (n Int)) Bool
  (=>
    (> n 0)
    (and
      (not (= 0 (base p)))
      (and (<= 0 (offset p)) (<= (+ (offset p) n) (mixfix_lbrb m (base p)))))))

;; "valid_obj"
(define-fun valid_obj ((m (infix_mngt Int Int)) (p addr) (n Int)) Bool
  (=>
    (> n 0)
    (or
      (= p null)
      (and
        (not (= 0 (base p)))
        (and
          (<= 0 (offset p))
          (<= (+ (offset p) n) (+ 1 (mixfix_lbrb m (base p)))))))))

;; "invalid"
(define-fun invalid ((m (infix_mngt Int Int)) (p addr) (n Int)) Bool
  (or
    (<= n 0)
    (or
      (= (base p) 0)
      (or (<= (mixfix_lbrb m (base p)) (offset p)) (<= (+ (offset p) n) 0)))))

;; "valid_rw_rd"
(assert
  (forall ((m (infix_mngt Int Int)))
    (forall ((p addr))
      (forall ((n Int)) (=> (valid_rw m p n) (valid_rd m p n))))))

;; "valid_string"
(assert
  (forall ((m (infix_mngt Int Int)))
    (forall ((p addr))
      (=>
        (< (base p) 0)
        (=>
          (and (<= 0 (offset p)) (< (offset p) (mixfix_lbrb m (base p))))
          (and (valid_rd m p 1) (not (valid_rw m p 1))))))))

;; "separated_1"
(assert
  (forall ((p addr) (q addr))
    (forall ((a2 Int) (b2 Int) (i Int) (j Int))
      (! (=>
           (separated p a2 q b2)
           (=>
             (and (<= (offset p) i) (< i (+ (offset p) a2)))
             (=>
               (and (<= (offset q) j) (< j (+ (offset q) b2)))
               (not (= (addrqtmk (base p) i) (addrqtmk (base q) j)))))) :pattern (
      (separated
        p
        a2
        q
        b2)
      (addrqtmk (base p) i)
      (addrqtmk (base q) j)) ))))

;; "region"
(declare-fun region (Int) Int)

;; "linked"
(declare-fun linked ((infix_mngt Int
  Int)) Bool)

;; "sconst"
(declare-fun sconst ((infix_mngt addr
  Int)) Bool)

;; "framed"
(define-fun framed ((m (infix_mngt addr addr))) Bool
  (forall ((p addr))
    (! (=> (<= (region (base p)) 0) (<= (region (base (mixfix_lbrb m p))) 0)) :pattern (
    (mixfix_lbrb
      m
      p)) )))

;; "separated_included"
(assert
  (forall ((p addr) (q addr))
    (forall ((a2 Int) (b2 Int))
      (! (=>
           (> a2 0)
           (=>
             (> b2 0)
             (=> (separated p a2 q b2) (not (included p a2 q b2))))) :pattern (
      (separated
        p
        a2
        q
        b2)
      (included p a2 q b2)) ))))

;; "included_trans"
(assert
  (forall ((p addr) (q addr) (r addr))
    (forall ((a2 Int) (b2 Int) (c Int))
      (! (=>
           (included p a2 q b2)
           (=> (included q b2 r c) (included p a2 r c))) :pattern ((included
                                                                    p
                                                                    a2
                                                                    q
                                                                    b2)
      (included q b2 r c)) ))))

;; "separated_trans"
(assert
  (forall ((p addr) (q addr) (r addr))
    (forall ((a2 Int) (b2 Int) (c Int))
      (! (=>
           (included p a2 q b2)
           (=> (separated q b2 r c) (separated p a2 r c))) :pattern (
      (included
        p
        a2
        q
        b2)
      (separated q b2 r c)) ))))

;; "separated_sym"
(assert
  (forall ((p addr) (q addr))
    (forall ((a2 Int) (b2 Int))
      (! (= (separated p a2 q b2) (separated q b2 p a2)) :pattern ((separated
                                                                    p
                                                                    a2
                                                                    q
                                                                    b2)) ))))

;; "eqmem_included"
(assert (par (a)
  (forall ((m1 (infix_mngt addr a)) (m2 (infix_mngt addr a)))
    (forall ((p addr) (q addr))
      (forall ((a2 Int) (b2 Int))
        (! (=>
             (included p a2 q b2)
             (=> (eqmem m1 m2 q b2) (eqmem m1 m2 p a2))) :pattern ((eqmem
                                                                    m1
                                                                    m2
                                                                    p
                                                                    a2)
        (eqmem m1 m2 q b2)) ))))))

;; "eqmem_sym"
(assert (par (a)
  (forall ((m1 (infix_mngt addr a)) (m2 (infix_mngt addr a)))
    (forall ((p addr))
      (forall ((a2 Int)) (=> (eqmem m1 m2 p a2) (eqmem m2 m1 p a2)))))))

;; "havoc_access"
(assert (par (a)
  (forall ((m0 (infix_mngt addr a)) (m1 (infix_mngt addr a)))
    (forall ((q addr) (p addr))
      (forall ((a2 Int))
        (= (mixfix_lbrb (havoc m0 m1 p a2) q) (ite (separated q 1 p a2)
                                                (mixfix_lbrb m1 q)
                                                (mixfix_lbrb m0 q))))))))

;; "cinits"
(declare-fun cinits ((infix_mngt addr
  Bool)) Bool)

;; "is_init_range"
(define-fun is_init_range ((m (infix_mngt addr Bool)) (p addr) (l Int)) Bool
  (forall ((i Int))
    (=> (and (<= 0 i) (< i l)) (= (mixfix_lbrb m (shift p i)) true))))

;; "set_init"
(declare-fun set_init ((infix_mngt addr
  Bool)
  addr
  Int) (infix_mngt addr
  Bool))

;; "set_init_access"
(assert
  (forall ((m (infix_mngt addr Bool)))
    (forall ((q addr) (p addr))
      (forall ((a2 Int))
        (= (mixfix_lbrb (set_init m p a2) q) (ite (separated q 1 p a2)
                                               (mixfix_lbrb m q)
                                               true))))))

;; "monotonic_init"
(define-fun monotonic_init ((m1 (infix_mngt addr Bool)) (m2 (infix_mngt addr Bool))) Bool
  (forall ((p addr))
    (=> (= (mixfix_lbrb m1 p) true) (= (mixfix_lbrb m2 p) true))))

;; "int_of_addr"
(declare-fun int_of_addr (addr) Int)

;; "addr_of_int"
(declare-fun addr_of_int (Int) addr)

(declare-sort table 0)

;; "table_of_base"
(declare-fun table_of_base (Int) table)

;; "table_to_offset"
(declare-fun table_to_offset (table
  Int) Int)

;; "table_to_offset_zero"
(assert (forall ((t table)) (= (table_to_offset t 0) 0)))

;; "table_to_offset_monotonic"
(assert
  (forall ((t table))
    (forall ((o1 Int) (o2 Int))
      (= (<= o1 o2) (<= (table_to_offset t o1) (table_to_offset t o2))))))

;; "int_of_addr_bijection"
(assert (forall ((a2 Int)) (= (int_of_addr (addr_of_int a2)) a2)))

;; "addr_of_int_bijection"
(assert (forall ((p addr)) (= (addr_of_int (int_of_addr p)) p)))

;; "addr_of_null"
(assert (= (int_of_addr null) 0))

;; "max_uint8"
(define-fun max_uint8 () Int
  256)

;; "max_sint8"
(define-fun max_sint8 () Int
  128)

;; "max_uint16"
(define-fun max_uint16 () Int
  65536)

;; "max_sint16"
(define-fun max_sint16 () Int
  32768)

;; "max_uint32"
(define-fun max_uint32 () Int
  4294967296)

;; "max_sint32"
(define-fun max_sint32 () Int
  2147483648)

;; "max_uint64"
(define-fun max_uint64 () Int
  18446744073709551616)

;; "max_sint64"
(define-fun max_sint64 () Int
  9223372036854775808)

;; "is_bool"
(define-fun is_bool ((x Int)) Bool
  (or (= x 0) (= x 1)))

;; "is_uint8"
(declare-fun is_uint8 (Int) Bool)

;; "is_uint8_def"
(assert
  (forall ((x Int))
    (! (= (is_uint8 x) (and (<= 0 x) (< x max_uint8))) :pattern ((is_uint8 x)) )))

;; "is_sint8"
(declare-fun is_sint8 (Int) Bool)

;; "is_sint8_def"
(assert
  (forall ((x Int))
    (! (= (is_sint8 x) (and (<= (- max_sint8) x) (< x max_sint8))) :pattern (
    (is_sint8
      x)) )))

;; "is_uint16"
(declare-fun is_uint16 (Int) Bool)

;; "is_uint16_def"
(assert
  (forall ((x Int))
    (! (= (is_uint16 x) (and (<= 0 x) (< x max_uint16))) :pattern ((is_uint16
                                                                    x)) )))

;; "is_sint16"
(define-fun is_sint16 ((x Int)) Bool
  (and (<= (- max_sint16) x) (< x max_sint16)))

;; "is_uint32"
(declare-fun is_uint32 (Int) Bool)

;; "is_uint32_def"
(assert
  (forall ((x Int))
    (! (= (is_uint32 x) (and (<= 0 x) (< x max_uint32))) :pattern ((is_uint32
                                                                    x)) )))

;; "is_sint32"
(declare-fun is_sint32 (Int) Bool)

;; "is_sint32_def"
(assert
  (forall ((x Int))
    (! (= (is_sint32 x) (and (<= (- max_sint32) x) (< x max_sint32))) :pattern (
    (is_sint32
      x)) )))

;; "is_uint64"
(declare-fun is_uint64 (Int) Bool)

;; "is_uint64_def"
(assert
  (forall ((x Int))
    (! (= (is_uint64 x) (and (<= 0 x) (< x max_uint64))) :pattern ((is_uint64
                                                                    x)) )))

;; "is_sint64"
(declare-fun is_sint64 (Int) Bool)

;; "is_sint64_def"
(assert
  (forall ((x Int))
    (! (= (is_sint64 x) (and (<= (- max_sint64) x) (< x max_sint64))) :pattern (
    (is_sint64
      x)) )))

;; "is_bool0"
(assert (is_bool 0))

;; "is_bool1"
(assert (is_bool 1))

;; "to_bool"
(define-fun to_bool ((x Int)) Int
  (ite (= x 0) 0 1))

;; "to_uint8"
(declare-fun to_uint8 (Int) Int)

;; "to_sint8"
(declare-fun to_sint8 (Int) Int)

;; "to_uint16"
(declare-fun to_uint16 (Int) Int)

;; "to_sint16"
(declare-fun to_sint16 (Int) Int)

;; "to_uint32"
(declare-fun to_uint32 (Int) Int)

;; "to_sint32"
(declare-fun to_sint32 (Int) Int)

;; "to_uint64"
(declare-fun to_uint64 (Int) Int)

;; "to_sint64"
(declare-fun to_sint64 (Int) Int)

;; "two_power_abs"
(declare-fun two_power_abs (Int) Int)

;; "is_uint"
(define-fun is_uint ((n Int) (x Int)) Bool
  (and (<= 0 x) (< x (two_power_abs n))))

;; "is_sint"
(define-fun is_sint ((n Int) (x Int)) Bool
  (and (<= (- (two_power_abs n)) x) (< x (two_power_abs n))))

;; "to_uint"
(declare-fun to_uint (Int
  Int) Int)

;; "to_sint"
(declare-fun to_sint (Int
  Int) Int)

;; "is_to_uint8"
(assert (forall ((x Int)) (is_uint8 (to_uint8 x))))

;; "is_to_sint8"
(assert (forall ((x Int)) (is_sint8 (to_sint8 x))))

;; "is_to_uint16"
(assert (forall ((x Int)) (is_uint16 (to_uint16 x))))

;; "is_to_sint16"
(assert (forall ((x Int)) (is_sint16 (to_sint16 x))))

;; "is_to_uint32"
(assert (forall ((x Int)) (is_uint32 (to_uint32 x))))

;; "is_to_sint32"
(assert (forall ((x Int)) (is_sint32 (to_sint32 x))))

;; "is_to_uint64"
(assert (forall ((x Int)) (is_uint64 (to_uint64 x))))

;; "is_to_sint64"
(assert (forall ((x Int)) (is_sint64 (to_sint64 x))))

;; "id_uint8"
(assert
  (forall ((x Int))
    (! (=> (and (<= 0 x) (< x max_uint8)) (= (to_uint8 x) x)) :pattern (
    (to_uint8
      x)) )))

;; "id_sint8"
(assert
  (forall ((x Int))
    (! (=> (and (<= (- max_sint8) x) (< x max_sint8)) (= (to_sint8 x) x)) :pattern (
    (to_sint8
      x)) )))

;; "id_uint16"
(assert
  (forall ((x Int))
    (! (=> (and (<= 0 x) (< x max_uint16)) (= (to_uint16 x) x)) :pattern (
    (to_uint16
      x)) )))

;; "id_sint16"
(assert
  (forall ((x Int))
    (! (=> (and (<= (- max_sint16) x) (< x max_sint16)) (= (to_sint16 x) x)) :pattern (
    (to_sint16
      x)) )))

;; "id_uint32"
(assert
  (forall ((x Int))
    (! (=> (and (<= 0 x) (< x max_uint32)) (= (to_uint32 x) x)) :pattern (
    (to_uint32
      x)) )))

;; "id_sint32"
(assert
  (forall ((x Int))
    (! (=> (and (<= (- max_sint32) x) (< x max_sint32)) (= (to_sint32 x) x)) :pattern (
    (to_sint32
      x)) )))

;; "id_uint64"
(assert
  (forall ((x Int))
    (! (=> (and (<= 0 x) (< x max_uint64)) (= (to_uint64 x) x)) :pattern (
    (to_uint64
      x)) )))

;; "id_sint64"
(assert
  (forall ((x Int))
    (! (=> (and (<= (- max_sint64) x) (< x max_sint64)) (= (to_sint64 x) x)) :pattern (
    (to_sint64
      x)) )))

;; "proj_int8"
(assert
  (forall ((x Int))
    (! (= (to_sint8 (to_uint8 x)) (to_sint8 x)) :pattern ((to_sint8
                                                            (to_uint8 x))) )))

;; "proj_int16"
(assert
  (forall ((x Int))
    (! (= (to_sint16 (to_uint16 x)) (to_sint16 x)) :pattern ((to_sint16
                                                               (to_uint16 x))) )))

;; "proj_int32"
(assert
  (forall ((x Int))
    (! (= (to_sint32 (to_uint32 x)) (to_sint32 x)) :pattern ((to_sint32
                                                               (to_uint32 x))) )))

;; "proj_int64"
(assert
  (forall ((x Int))
    (! (= (to_sint64 (to_uint64 x)) (to_sint64 x)) :pattern ((to_sint64
                                                               (to_uint64 x))) )))

;; "shift_sint32"
(define-fun shift_sint32 ((p addr) (k Int)) addr
  (shift p k))

;; Goal "wp_goal"
(assert
  (not
  (forall ((t (infix_mngt Int Int)) (t1 (infix_mngt addr Int)) (i Int) (a2 addr) (i1 Int))
    (let ((a3 (shift_sint32 a2 0)))
      (let ((a4 (shift_sint32 a2 i1)))
        (let ((m (set t1 a3 (get t1 a4))))
          (let ((m1 (set m (shift_sint32 a2 1) (get m a4))))
            (let ((m2 (set m1 (shift_sint32 a2 2) (get m1 a4))))
              (let ((m3 (set m2 (shift_sint32 a2 3) (get m2 a4))))
                (=>
                  (<= 0 i1)
                  (=>
                    (<= 0 i)
                    (=>
                      (<= (region (base a2)) 0)
                      (=>
                        (<= i1 9)
                        (=>
                          (<= i 9)
                          (=>
                            (linked t)
                            (=>
                              (is_sint32 i1)
                              (=>
                                (valid_rw t a3 10)
                                (=>
                                  (forall ((i2 Int))
                                    (=>
                                      (<= 0 i2)
                                      (=>
                                        (<= i2 9)
                                        (<= 0 (get t1 (shift_sint32 a2 i2))))))
                                  (<= 0 (get
                                          (set
                                            m3
                                            (shift_sint32 a2 4)
                                            (get m3 a4))
                                          (shift_sint32 a2 i)))))))))))))))))))))

(check-sat)
