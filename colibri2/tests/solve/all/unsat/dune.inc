(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:bag-BagImpl-createqtvc.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:bag-BagImpl-createqtvc.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:div_abs.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:div_abs.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:fact-FactRecursive-fact_recqtvc.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:fact-FactRecursive-fact_recqtvc.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:float_interval-GenericFloat-add_special_1.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:float_interval-GenericFloat-add_special_1.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:init.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:init.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:interval-Convexe-exists_memqtvc_1.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:interval-Convexe-exists_memqtvc_1.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:interval-Convexe-exists_memqtvc_2.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:interval-Convexe-exists_memqtvc_2.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:lost_in_search_union.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:lost_in_search_union.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:mjrty-Mjrty-mjrtyqtvc_3.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:mjrty-Mjrty-mjrtyqtvc_3.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:mul_abs.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:mul_abs.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:ordered_is_ordered.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:ordered_is_ordered.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:power2-Pow2int-Div_doublea35fb5.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:power2-Pow2int-Div_doublea35fb5.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:range_loop_invariant_CHECK_preserved_Why3_Colibri2_n_a.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:range_loop_invariant_CHECK_preserved_Why3_Colibri2_n_a.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:range_loop_invariant_CHECK_preserved_Why3_Colibri2_n_a_simplified.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:range_loop_invariant_CHECK_preserved_Why3_Colibri2_n_a_simplified.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:slow_convergence_BinaryMultiplication_loop_invariant_inv2_ok_deductible_preserved.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:slow_convergence_BinaryMultiplication_loop_invariant_inv2_ok_deductible_preserved.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:union-Union-interqtvc_10.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:union-Union-interqtvc_10.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:union-Union-interqtvc_5.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:union-Union-interqtvc_5.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:union-Union-is_includedqtvc_1.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:union-Union-is_includedqtvc_1.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:union-Union-is_singletonqtvc_1.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:union-Union-is_singletonqtvc_1.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:union-Union-is_singletonqtvc_2.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:union-Union-is_singletonqtvc_2.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:union-Union-is_singletonqtvc_5.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:union-Union-is_singletonqtvc_5.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:union-Union-length0_positiveqtvc_1.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:union-Union-length0_positiveqtvc_1.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:union-Union-leqtqtvc_13.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:union-Union-leqtqtvc_13.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:work_with_fourier_not_simplex.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:work_with_fourier_not_simplex.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:work_with_fourier_not_simplex2.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:work_with_fourier_not_simplex2.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:wp_float_real_r.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:wp_float_real_r.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:wp_initialize.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:wp_initialize.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:wp_sharing_f_ensures.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:wp_sharing_f_ensures.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:wp_simpl_is_type_f_ensures.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:wp_simpl_is_type_f_ensures.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:wp_simpl_is_type_g_ensures.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:wp_simpl_is_type_g_ensures.psmt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat 
--dont-print-result %{dep:wp_terminates_formulae_decreases_variant.psmt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --cut-fix-model=20 --check-status unsat --learning --dont-print-result %{dep:wp_terminates_formulae_decreases_variant.psmt2})) (package colibri2))
