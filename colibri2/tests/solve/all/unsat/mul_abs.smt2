(set-logic ALL)

(declare-fun a () Real)
(declare-fun b () Real)

(assert (not (= (* (colibri_abs_real a) (colibri_abs_real b)) (colibri_abs_real (* a b)))))

(check-sat)
