(set-logic ALL)
(declare-fun rm1 () RoundingMode)
(declare-fun rm2 () RoundingMode)
(assert (= rm1 rm2))
(check-sat)

