(set-logic ALL)
(assert (exists ((x (_ FloatingPoint 8 24)) (y (_ FloatingPoint 8 24))) 
  (and (= x y) (not (fp.eq x y)))))
(check-sat)