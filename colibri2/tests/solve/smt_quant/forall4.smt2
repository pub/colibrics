(set-logic ALL)
(set-info :status-colibri2 unknown)

(declare-sort S 0)

(declare-fun P0 (S S) Bool)

(assert (forall ((x S)(y S)) (=> (P0 x y) (P0 y x))))

(declare-fun a () S)
(declare-fun b () S)
(assert (P0 a b))
;(assert (not (P0 b a)))

(check-sat)
