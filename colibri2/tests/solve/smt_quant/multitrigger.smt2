(set-logic ALL)
(set-info :status-colibri2 unsat)

(declare-sort S 0)
(declare-fun P (S) Bool)

(assert (forall ((x S)(y S)) (=> (P x) (P y))))

(declare-fun a () S)
(declare-fun b () S)
(assert (P a))
(assert (not (P b)))
(check-sat)
