(set-logic ALL)

(declare-sort S 0)
(declare-fun P0 (S) Bool)
(declare-fun P1 (S) Bool)

(assert (forall ((x S)) (=> (P0 x) (P1 x))))

(declare-fun a () S)
(declare-fun b () S)
(assert (P0 a))
(assert (not (P1 b)))
(assert (= a b))

(check-sat)
