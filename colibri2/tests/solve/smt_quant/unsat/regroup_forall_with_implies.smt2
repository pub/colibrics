(set-logic ALL)

(declare-sort S 0)
(declare-fun P (S S) Bool)
(declare-fun a () S)
(declare-fun b () S)
(assert (distinct a b))

(assert
 (forall ((x S)) 
     (=> (distinct x a)
        (forall ((y S))
           (=> (distinct y a) (P x y) )))))

(assert (not (P b b)))

(check-sat)
