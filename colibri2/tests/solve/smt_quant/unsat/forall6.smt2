(set-logic ALL)

(declare-sort S 0)
(declare-fun Q (S S) Bool)
(declare-fun P0 (S) Bool)
(declare-fun P1 (S) Bool)

(assert (forall ((x S)(y S)) (=> (P0 x) (Q x y) (P1 y))))

(declare-fun a () S)
(declare-fun b () S)

(assert (Q a b))
(assert (P0 a))
(assert (not (P1 b)))

(check-sat)
