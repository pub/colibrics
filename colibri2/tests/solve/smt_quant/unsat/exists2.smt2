(set-logic ALL)

(assert (not
   (forall ((x Real))
     (=> (= x 1.0) (forall ((y Real)) (= y (* y x)))))))

(check-sat)
