(set-logic ALL)
(declare-sort S 0)
(declare-fun P (S S) Bool)
(declare-fun Q (S S) Bool)

(assert (forall ((x S)(y S)) (=> (P x y) (Q x y))))

(declare-fun a () S)
(declare-fun b () S)
(assert (P a b))
(assert (not (Q a b)))

(check-sat)
