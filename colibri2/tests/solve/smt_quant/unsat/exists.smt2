(set-logic ALL)

(assert (exists ((x Int)) (and (= x 1) (= x 2))))

(check-sat)
