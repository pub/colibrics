(set-logic ALL)
(declare-sort S 0)
(declare-fun P (S) Bool)

(assert (forall ((x S)) (P x)))

(declare-fun a () S)
(assert (not (P a)))

(check-sat)
