(set-logic ALL)
(declare-sort S 0)
(declare-fun P (S S) Bool)

(assert (forall ((x S)(y S)) (P x y)))

(declare-fun a () S)
(declare-fun b () S)
(assert (not (P a b)))

(check-sat)
