(set-logic ALL)
(declare-sort S 0)
(declare-fun P (S S) Bool)

(assert (forall ((x S)) (=> (P x x) (forall ((y S)) (P x y)))))

(declare-fun a () S)
(declare-fun b () S)
(assert (P a a))
(assert (not (P a b)))

(check-sat)
