(set-logic ALL)

(declare-sort S 0)
(declare-fun a () S)
(declare-fun P (S) Bool)
(declare-fun f (S S) S)
(declare-fun g (S) S)

(assert (forall ((x S)) (= (f x x) (g x))))
(assert (forall ((x S)) (P (g x))))

(assert (not (P (f a a))))

(check-sat)
