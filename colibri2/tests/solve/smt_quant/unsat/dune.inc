(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:exists.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:exists.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:exists2.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:exists2.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:forall0.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:forall0.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:forall1.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:forall1.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:forall2.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:forall2.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:forall3.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:forall3.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:forall4.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:forall4.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:forall5.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:forall5.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:forall6.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:forall6.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:forall7.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:forall7.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:forall8.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:forall8.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:forall_false.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:forall_false.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat 
--dont-print-result %{dep:regroup_forall_with_implies.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status unsat --learning --dont-print-result %{dep:regroup_forall_with_implies.smt2})) (package colibri2))
