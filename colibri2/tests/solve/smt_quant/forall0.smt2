(set-logic ALL)
(set-info :status-colibri2 unknown)

(declare-sort S 0)
(declare-fun P (S) Bool)

(assert (forall ((x S)) (P x)))

(declare-fun a () S)
(assert (P a))

(check-sat)
