(set-logic ALL)

(declare-const x Int)
(declare-const y Bool)

(assert y)
(check-sat)
(assert (> x 0))
(check-sat)
(get-model)
(get-value (x))
