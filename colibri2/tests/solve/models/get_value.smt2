(set-logic ALL)
(set-info :status-colibri2 sat)

(declare-fun a () Int)

(assert (<= a (+ a 1)))

(check-sat)
(get-model)
(get-value (a))
(get-value ((+ a 1)))
(get-value ((+ a 2)))
