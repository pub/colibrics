(set-logic ALL)
(set-info :status-colibri2 sat)

(declare-fun f (Int) Int)

(assert (< (f 1) (f 2)))

(check-sat)
(get-model)
(get-value ((f 1)))
(get-value ((f 2)))
(get-value ((f 3)))
(get-value ((f 4)))
;(get-value (f))
