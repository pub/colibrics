(set-logic ALL)
(set-info :status-colibri2 sat)

(declare-fun b () Real)

(assert (= (* b b) 2.0))

(check-sat)
(get-model)
(get-value ((colibri_sqrt 2.0)))
