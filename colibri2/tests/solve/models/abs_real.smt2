(set-logic ALL)
(set-info :status-colibri2 sat)

(declare-fun a () Int)
(declare-fun b () Real)

(assert (< a 0))
(assert (< b 0))

(check-sat)
(get-model)
(get-value ((abs a)))
(get-value ((colibri_abs_real b)))
