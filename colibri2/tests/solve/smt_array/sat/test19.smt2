(set-logic QF_AX)
(declare-sort I 0)
(declare-sort E 0)
(declare-fun a () (Array I E))
(declare-fun i () I)
(declare-fun i5 () I)
(assert (not
  (=
    (store a i5 (select a i5))
    (store (store a i (select a i5)) i5 (select a i))
  )))
(check-sat)
