(set-logic QF_AX)
(declare-sort I 0)
(declare-sort E 0)
(declare-fun a () (Array I E))
(declare-fun v () E)
(declare-fun i () I)
(declare-fun j () I)
(assert (= (store a i v) (store a j v)))
(assert (not (= a (store (store a i v) i v))))
(check-sat)