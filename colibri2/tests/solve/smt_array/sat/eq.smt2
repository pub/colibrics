(set-logic ALL)
(declare-fun a () (Array Int Int))
(assert (= (store a 0 0) (store a 1 0)))
(check-sat)
