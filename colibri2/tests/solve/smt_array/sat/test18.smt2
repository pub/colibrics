(set-logic QF_ALIA)
(declare-fun a () (Array Int Int))
(declare-fun i () Int)
(declare-fun j () Int)
(assert (not
  (=
    (store a j (select a j))
    (store (store a i (select a j)) j (select a i))
  )))
(check-sat)
