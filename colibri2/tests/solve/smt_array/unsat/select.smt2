(set-logic ALL)

(declare-fun a () (Array Int Int))

(assert (= (select a 1) 1))

(assert (= (select a 1) 2))

(check-sat)
