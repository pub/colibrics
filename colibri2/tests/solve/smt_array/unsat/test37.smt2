(set-logic QF_AX)
(declare-sort I 0)
(declare-sort E 0)
(declare-fun a () (Array I E))
(declare-fun e1 () E)
(declare-fun e2 () E)
(declare-fun i () I)
(declare-fun j () I)

(assert (=
  (store a j e2)
  (store a j (select (store (store a j e1) i e1) j))
))
(assert (= a (store a j e1)))
(assert (not (=
  (store a i e2)
  (store (store a j e1) i e1))))

(check-sat)

