(set-logic ALL)

(declare-fun a () (Array Int Int))

(assert (= (- (select (store a 0 2) 0) 2) 1))

(check-sat)
