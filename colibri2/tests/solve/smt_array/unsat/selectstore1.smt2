(set-logic ALL)

(declare-fun a () (Array Int Int))

(assert (= (select (store a 1 2) 1) 1))

(check-sat)
