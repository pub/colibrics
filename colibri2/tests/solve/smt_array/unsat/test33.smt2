(set-logic QF_AX)
(declare-sort I 0)
(declare-sort E 0)
(declare-fun a () (Array I E))
(declare-fun i () I)
(declare-fun j () I)
(assert (not (=
  a
  (store a j (select (store (store a i (select a j)) j (select a i)) i))
)))
(check-sat)


; a
; a[ j <- a[i <- a[j]][j <- a[i]][i] ]
; a[ j <- a[j] ]
