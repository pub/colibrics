(set-logic QF_AX)
(declare-sort I 0)
(declare-sort E 0)
(declare-fun a () (Array I E))
(declare-fun i () I)
(declare-fun j () I)
(declare-fun k () I)
(assert (not (=
  (store (store a i (select a j)) j (select a i))
  (store (store a j (select a i)) i (select (store a k (select a k)) j))
)))
(check-sat)

; a[i <- a[j]][j <- a[i]]
; a[j <- a[i]][i <- a[k <- a[k]][j]]