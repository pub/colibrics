(set-logic ALL)

(declare-fun a () (Array Int Int))
(declare-fun b () (Array Int Int))
(declare-fun i () Int)
(declare-fun j () Int)

(assert (distinct (select a i) (select (store b j (select a i)) j)))

(check-sat)
