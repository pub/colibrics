(set-logic ALL)

(declare-fun a () (Array Int Int))
(declare-fun b () (Array Int Int))
(declare-fun i () Int)
(declare-fun v () Int)

(assert (not (=>
  (= b (store a i v))
  (= v (select b i))
)))

(check-sat)
