(set-logic QF_AX)
(declare-sort I 0)
(declare-sort E 0)
(declare-fun a () (Array I E))
(declare-fun v () E)
(declare-fun i () I)
(declare-fun j () I)
(assert (=
  (store a j v)
  (store (store a i (select a i)) j v)))
(assert (=
  a
  (store (store a i (select a i)) i (select a i))))
(assert (not (=
  (store (store a i v) i v)
  (store a i (select (store a i v) i)))))
(check-sat)
