(set-logic QF_AX)
(declare-sort I 0)
(declare-sort E 0)
(declare-fun a () (Array I E))
(declare-fun v () E)
(declare-fun i () I)
(declare-fun j () I)

(assert (= (store a i v) (store a j (select a j))))
(assert (=
  (store a i (select a j))
  (store (store a j v) j (select a j))
))
(assert (not (= a (store a j v))))

(check-sat)
