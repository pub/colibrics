(set-logic ALL)

(declare-fun a () (Array Int Int))

(assert (let ((a1 (store a 0 1)) (a2 (store a 0 2))) (= a1 a2)))

(check-sat)
