(set-logic ALL)

(declare-fun a () (Array Int Int))
(declare-fun i () Int)
(declare-fun v () Int)

(assert (not (=>
  (= a (store a i v))
  (= (select a i) v)
)))

(check-sat)
