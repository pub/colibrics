(set-logic ALL)

(declare-fun a () (Array Int Int))
(declare-fun i () Int)

(assert (not (= a (store a i (select a i)))))

(check-sat)
