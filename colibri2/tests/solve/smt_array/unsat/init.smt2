  (set-logic ALL)
  (set-info :smt-lib-version 2.6)

  (declare-sort ty 0)


  (declare-datatypes ((S3_Sc 0))
    (((S3_Sc1 (F3_Sc_a Int)(F3_Sc_b (Array Int Int))(F3_Sc_c Int)))))

  ;; "S3_Sc"
  (declare-fun S3_Sc2 () ty)

  ;; "is_sint32"
  (declare-fun is_sint32 (Int) Bool)


  ;; "IsArray_sint32"
  (declare-fun IsArray_sint32 ((Array Int Int)) Bool)

  ;; "IsArray_sint32'def"

  ;; "EqArray_int"
  (declare-fun EqArray_int (Int
    (Array Int Int)
    (Array Int Int)) Bool)

  ;; "EqArray_int'def"
  ;(assert
  ;  (forall ((n Int) (T (Array Int Int)) (T1 (Array Int Int)))
  ;    (=
  ;      (EqArray_int n T T1)
  ;      (forall ((i Int))
  ;        (=> (<= 0 i) (=> (< i n) (= (select T1 i) (select T i))))))))

  (assert
    (forall ((n Int) (T (Array Int Int)) (T1 (Array Int Int)))
      (=
        (EqArray_int n T T1)
        (forall ((i Int))
        (and
          (=> (= 0 i) (= (select T1 i) (select T i)))
          (=> (= 1 i) (= (select T1 i) (select T i)))
          (=> (= 2 i) (= (select T1 i) (select T i)))
          (=> (<= 3 i) (=> (< i n) (= (select T1 i) (select T i)))))))))


  ;; "IsS3_Sc"
  (declare-fun IsS3_Sc (S3_Sc) Bool)


  ;; "EqS3_Sc"
  (declare-fun EqS3_Sc (S3_Sc
    S3_Sc) Bool)

  ;; "EqS3_Sc'def"
  (assert
    (forall ((S S3_Sc) (S1 S3_Sc))
      (=
        (EqS3_Sc S S1)
        (and
          (and (= (F3_Sc_a S1) (F3_Sc_a S)) (= (F3_Sc_c S1) (F3_Sc_c S)))
          (EqArray_int 3 (F3_Sc_b S) (F3_Sc_b S1))))))

  ;; Goal "wp_goal"
  (assert
    (not
    (forall ((S S3_Sc) (S1 S3_Sc))
      (let ((a (F3_Sc_b S1)))
        (let ((a1 (F3_Sc_b S)))
          (=>
            (= (F3_Sc_a S1) 1)
            (=>
              (= (F3_Sc_c S1) 5)
              (=>
                (= (F3_Sc_a S) 1)
                (=>
                  (= (F3_Sc_c S) 5)
                  (=>
                    (= (select a 0) 2)
                    (=>
                      (= (select a 1) 3)
                      (=>
                        (= (select a 2) 4)
                        (=>
                          (= (select a1 0) 2)
                          (=>
                            (= (select a1 1) 3)
                            (=>
                              (= (select a1 2) 4)
                              (=> (IsS3_Sc S1) (=> (IsS3_Sc S) (EqS3_Sc S1 S))))))))))))))))))

  (check-sat)
