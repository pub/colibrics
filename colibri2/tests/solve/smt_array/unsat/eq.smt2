(set-logic ALL)

(declare-fun a () (Array Int Int))

(assert (not (= (store a 1 2) (store a 1 2))))

(check-sat)
