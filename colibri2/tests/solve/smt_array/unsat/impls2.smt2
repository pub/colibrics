(set-logic ALL)

(declare-fun a () (Array Int Int))
(declare-fun i () Int)
(declare-fun j () Int)
(declare-fun v () Int)
(declare-fun w () Int)

(assert (distinct i j))
(assert (distinct v w))
(assert (not (= (store (store a i v) j w) (store (store a j w) i v))))

(check-sat)
