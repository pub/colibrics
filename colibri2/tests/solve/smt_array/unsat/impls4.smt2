(set-logic ALL)

(declare-fun a () (Array Int Int))
(declare-fun i () Int)
(declare-fun j () Int)
(declare-fun v () Int)

(assert (not (=>
  (= i j)
  (= a (store a i (select a j)))
)))

(check-sat)
