(set-logic QF_AX)
(declare-sort I 0)
(declare-sort E 0)
(declare-fun a () (Array I E))
(declare-fun i () I)
(declare-fun i1 () I)
(declare-fun i2 () I)

(assert (not (= (store (store (store a i (select a i1)) i1 (select a i)) i2 (select (store (store a i (select a i1)) i1 (select a i)) i2)) (store (store (store a i1 (select a i)) i (select a i1)) i2 (select (store (store a i1 (select a i)) i (select a i1)) i2)))))

(check-sat)
