(set-logic ALL)

(declare-fun a () (Array Int Int))

(declare-fun k () Int)
(declare-fun i () Int)
(declare-fun j () Int)
(declare-fun v () Int)
(declare-fun w () Int)

(assert (not (=>
  (distinct i j)
  (< k i)
  (distinct k j)
  (= (select (store (store a i v) j w) k) 100)
  (= (select (store (store a j w) i v) k) 200)
  false
)))

(check-sat)
