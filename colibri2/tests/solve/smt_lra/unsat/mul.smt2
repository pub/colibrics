(set-logic ALL)

(declare-fun a () Real)
(declare-fun b () Real)
(declare-fun t1 () Real)
(declare-fun t1_ () Real)
(declare-fun t2 () Real)

(assert (= t1 (- a b)))
(assert (= t1_ (* a b)))
(assert (= t2 (* 2.0 b)))

(assert (= t1 t1_))
(assert (= a 1.0))
(assert (not (= 1.0 t2)))

(check-sat)
