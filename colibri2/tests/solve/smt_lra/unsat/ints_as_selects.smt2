(set-logic QF_ALIA)
(declare-fun a () (Array Int Int))
(assert (= (+ 1 (select a 1)) (* (- 1) (select a 1))))
(check-sat)
