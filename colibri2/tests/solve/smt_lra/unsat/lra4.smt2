(set-logic QF_LRA)
(declare-fun n () Bool)

(declare-fun a () Real)
(declare-fun b () Real)
(declare-fun c () Real)
(declare-fun d () Real)

(assert (= a (+ b 1)))
(assert (= c (+ d 1)))

(assert (ite n (= a c) (= a c)))

(assert (distinct b d))

(check-sat)
