(set-logic QF_LRA)
(declare-fun z () Real)
(assert
 (let ((?2 2))
 (let ((?n2 (+ z ?2)))
 (let ((?n3 (= z ?n2))) ?n3
))))
(check-sat)
(exit)
