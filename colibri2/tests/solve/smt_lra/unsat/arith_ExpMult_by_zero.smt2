(set-logic QF_LRA)
(declare-fun v0 () Real)
(assert
 (let ((?2 2))
 (let ((?n4 (* ?2 v0)))
 (let ((?n2 (= v0 0)))
 (let ((?n5 (ite ?n2 ?n4 v0)))
 (let ((?n6 (distinct v0 ?n5))) ?n6
))))))
(check-sat)
(exit)
