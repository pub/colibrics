(set-logic ALL)

(declare-fun a () Int)
(declare-fun b () Int)

(assert (not (= (to_real (+ a b)) (+ (to_real a) (to_real b)))))

(check-sat)
