(set-logic QF_IDL)
(declare-fun _2 () Int)
(declare-fun _25 () Int)
(declare-fun _6 () Int)
(assert (and (> _25 _2) (= 1 (- _6 _2)) (or false (and (< _25 _6) (= _6 _25)))))
(check-sat)
;; _2 <= _25 - 1
;; _2 = _6 - 1
;; _25 <= _6 - 1

;; _25 <= _2 <= _25 - 1