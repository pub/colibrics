(set-logic ALL)

(declare-fun a () Int)

(assert (<= (+ a 1) a))

(check-sat)
;[Egraph] @0 is a
;[Egraph] @1 is 1
;[Egraph] @2 is +(@0, @1)
;[Egraph] @3 is (@2 ≤ @0)
;[Egraph] @4 is @0-@2