(set-logic ALL)

(declare-fun a () Int)
(declare-fun b () Int)

(assert (<= a b))
(assert (< b a))

(check-sat)
