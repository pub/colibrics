(set-logic ALL)

(declare-fun a () Real)
(declare-fun b () Real)
(declare-fun c () Real)
(declare-fun t1 () Real)
(declare-fun t2 () Real)

(assert (= t1 (+ a b)))
(assert (= t2 (+ a c)))

(assert (= b c))
(assert (not (= t1 t2)))

(check-sat)
