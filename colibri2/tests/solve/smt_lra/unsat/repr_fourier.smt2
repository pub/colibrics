(set-logic ALL)

(declare-fun num () Int)
(declare-fun den () Int)

;; Assert
  (assert (< num den))

(assert (not (< (to_real num) (to_real den))))

(check-sat)
