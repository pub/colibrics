(set-logic ALL)
(declare-fun m () Real)
(assert (> m (- m)))
(check-sat)
