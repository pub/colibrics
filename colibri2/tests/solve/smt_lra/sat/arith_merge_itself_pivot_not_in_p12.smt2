(set-logic QF_LRA)
(declare-fun v1 () Real)
(assert
 (let ((?1 1))
 (let ((?n2 (= ?1 v1)))
 (let ((?n3 (+ v1 v1)))
 (let ((?n4 (- v1 ?n3)))
 (let ((?n6 (ite ?n2 ?n4 ?n4)))
 (let ((?n7 (= ?1 ?n6))) ?n7
)))))))
(check-sat)
(exit)
