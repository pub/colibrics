(set-logic QF_LRA)
(declare-fun v2 () Real) ; 1.0
(declare-fun v1 () Real) ; 0.5
(declare-fun v0 () Real) ; 0.0
(assert
 (let ((?n2 (= 0 v1)))
 (let ((?n3 (ite ?n2 v2 v0)))
 (let ((?1 1))
 (let ((?n5 (= ?1 v2)))
 (let ((?n6 (* 0 v1)))
 (let ((?n7 (ite ?n5 ?n6 v2)))
 (let ((?n8 (= ?n3 ?n7))) ?n8
))))))))
(check-sat)
(exit)
