(set-logic QF_LRA)
(declare-fun v0 () Real)
(assert
 (let ((?n1 (- v0)))
 (let ((?n2 (distinct v0 ?n1)))
 (let ((?2 2))
 (let ((?n4 (ite ?n2 v0 v0)))
 (let ((?n5 (= ?2 ?n4)))
 (let ((?n6 (and ?n2 ?n5))) ?n6
)))))))
(check-sat)
(exit)
