(set-logic QF_LRA)
(declare-fun v2 () Real)
(declare-fun v1 () Real)
(assert
 (let ((?1 1))
 (let ((?n3 (- ?1)))
 (let ((?n4 (+ ?n3 v1)))
 (let ((?n5 (- ?n4)))
 (let ((?n6 (= v1 v2)))
 (let ((?n7 (ite ?n6 ?1 v2)))
 (let ((?n8 (= ?n5 ?n7))) ?n8
))))))))
(check-sat)
(exit)
