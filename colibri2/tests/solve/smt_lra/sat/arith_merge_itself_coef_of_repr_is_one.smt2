(set-logic QF_LRA)
(declare-fun v1 () Real)
(assert
 (let ((?n1 1))
 (let ((?n2bis (= ?n1 v1)))
 (let ((?n2 (not ?n2bis)))
 (let ((?n4 (ite ?n2 v1 0)))
 (let ((?n5 (= v1 0)))
 (let ((?n6 (- v1 ?n1)))
 (let ((?n7 (ite ?n5 ?n1 ?n6)))
 (let ((?n8 (= ?n4 ?n7))) ?n8
)))))))))
(check-sat)
(exit)
