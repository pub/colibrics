(set-logic QF_LRA)
(declare-fun v0 () Real)
(declare-fun v1 () Real)
(assert
 (let ((?n1 (- v0)))
 (let ((?n2 (= ?n1 v1)))
 (let ((?n3 (= ?n1 v0)))
 (let ((?1 1))
 (let ((?n5 (ite ?n3 ?1 v0)))
 (let ((?n6 (= v1 ?n5)))
 (let ((?n7 (= ?n2 ?n6))) ?n7
))))))))
(check-sat)
(exit)
