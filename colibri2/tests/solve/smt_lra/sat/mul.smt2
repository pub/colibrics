(set-logic ALL)
(assert 
 (let ((?n1 8.0))
 (let ((?n2 5.0))
 (let ((?n3 (* ?n1 ?n2)))
 (let ((?n40 (ite true 0.0 ?n3))) 
 (let ((?n42 (= 0.0 ?n40))) 
  ?n42
))))))
(check-sat)
(exit)
