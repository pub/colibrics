(set-logic QF_LRA)
(declare-fun v2 () Real)
(assert
 (let ((?n3 (= v2 0)))
 (let ((?n4 (- 1)))
 (let ((?n7 (* ?n4 v2)))
 (let ((?n8 (ite ?n3 ?n4 ?n7)))
 (let ((?n9 (= v2 ?n8)))
 (let ((?n10 (or true ?n9))) ?n10
)))))))
(check-sat)
(exit)
