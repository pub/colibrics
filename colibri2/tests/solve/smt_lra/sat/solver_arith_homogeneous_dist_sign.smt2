(set-logic QF_LRA)
(declare-fun v0 () Real)
(assert
 (let ((?n1 1))
 (let ((?n2 (- v0)))
 (let ((?n3 (= ?n1 ?n2)))
 (let ((?n4 (* ?n1 v0)))
 (let ((?n5 (ite ?n3 ?n4 v0)))
 (let ((?n6 (= ?n1 ?n5))) ?n6
)))))))
(check-sat)
(exit)
