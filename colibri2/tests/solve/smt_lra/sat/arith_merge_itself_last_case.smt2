(set-logic QF_LRA)
(declare-fun v2 () Real)
(assert
 (let ((?n2 (+ v2 v2)))
 (let ((?n3 (= v2 ?n2)))
 (let ((?1 1))
 (let ((?n5 (- v2)))
 (let ((?n6 (ite ?n3 ?1 ?n5)))
 (let ((?n7 (= ?n2 ?n6)))
 (let ((?n8 (or true ?n7))) ?n8
))))))))
(check-sat)
(exit)
