(set-logic QF_LRA)
(declare-fun v0 () Real)
(assert
 (let ((?n2 (distinct v0 0)))
 (let ((?2 2))
 (let ((?n5 (* v0 ?2)))
 (let ((?n6 (= v0 ?n5)))
 (let ((?n7 (ite ?n6 v0 v0)))
 (let ((?n8 (= v0 ?n7)))
 (let ((?n9 (xor false ?n8)))
 (let ((?n10 (and ?n2 ?n9))) ?n10
)))))))))
(check-sat)
(exit)
