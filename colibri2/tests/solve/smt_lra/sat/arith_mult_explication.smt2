(set-logic ALL)
(declare-fun v0 () Real)
(declare-fun b () Bool)
(assert
 (let ((?6 6) (?5 5))
 (let ((?n3 (* ?6 ?5)))
 (let ((?n4 (ite b ?n3 ?6)))
 (let ((?n5 (= ?6 ?n4))) ?n5
)))))
(check-sat)
(exit)
