(set-logic QF_RDL)
(declare-const x Real)
(declare-const x5 Real)
(declare-const x7 Real)
(declare-fun A () Real)
(assert (and (> x7 x5) (> (- x A) 4) (< (- x A) 5)))
(check-sat)

