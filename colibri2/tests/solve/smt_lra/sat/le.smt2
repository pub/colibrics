(set-logic ALL)

(declare-fun a () Int)

(assert (<= a (+ a 1)))

(check-sat)
