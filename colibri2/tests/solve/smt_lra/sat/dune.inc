(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:arith_CombiRepr_normalize.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:arith_CombiRepr_normalize.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:arith_conflict_add_disequality.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:arith_conflict_add_disequality.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:arith_conpoly.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:arith_conpoly.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:arith_decide_must_test_is_dis_equal.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:arith_decide_must_test_is_dis_equal.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:arith_init_always_merge_itself.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:arith_init_always_merge_itself.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:arith_init_and_propa_must_be_ordered.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:arith_init_and_propa_must_be_ordered.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:arith_merge_case1.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:arith_merge_case1.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:arith_merge_case_4.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:arith_merge_case_4.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:arith_merge_case_4_bis.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:arith_merge_case_4_bis.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:arith_merge_itself_coef_of_repr_is_one.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:arith_merge_itself_coef_of_repr_is_one.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:arith_merge_itself_last_case.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:arith_merge_itself_last_case.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:arith_merge_itself_pivot_not_in_p12.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:arith_merge_itself_pivot_not_in_p12.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:arith_merge_must_use_find.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:arith_merge_must_use_find.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:arith_mult_explication.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:arith_mult_explication.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:arith_mult_not_linear_in_conflict.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:arith_mult_not_linear_in_conflict.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:arith_normalize_use_find_def.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:arith_normalize_use_find_def.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:arith_own_repr.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:arith_own_repr.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:arith_pivot_modulo_constant.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:arith_pivot_modulo_constant.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:arith_propacl.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:arith_propacl.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:arith_subst_and_conflict_add.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:arith_subst_and_conflict_add.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:arith_zero_dom.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:arith_zero_dom.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:assert_union.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:assert_union.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:attach_only_when_dom_present.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:attach_only_when_dom_present.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:factorization.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:factorization.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:init_not_repr.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:init_not_repr.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:le.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:le.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:le2.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:le2.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:lra_pivot_upds_merge.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:lra_pivot_upds_merge.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:mul.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:mul.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:polypaver-bench-exp-3d-chunk-0022.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:polypaver-bench-exp-3d-chunk-0022.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:real_interval_intersect_int.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:real_interval_intersect_int.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:relop_norm_reg.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:relop_norm_reg.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:sem_invariant_in_learnt_dec.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:sem_invariant_in_learnt_dec.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:solver_add_pexp_cl.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:solver_add_pexp_cl.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:solver_arith_homogeneous_dist_sign.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:solver_arith_homogeneous_dist_sign.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:solver_merge_itself_repr_inside.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:solver_merge_itself_repr_inside.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:solver_set_pending_merge_expsameexp.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:solver_set_pending_merge_expsameexp.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:solver_subst_eventdom_find.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:solver_subst_eventdom_find.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:to_real.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:to_real.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:to_real2.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:to_real2.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat 
--dont-print-result %{dep:zero_should_not_be_simplified.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --check-status sat --learning --dont-print-result %{dep:zero_should_not_be_simplified.smt2})) (package colibri2))
