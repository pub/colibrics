(set-logic QF_LRA)
(declare-fun z () Real)
(assert
 (let ((?2 2))
 (let ((?n2 (= z ?2)))
 (let ((?n3 (+ z ?2)))
 (let ((?n4 (= z ?n3)))
 (let ((?n6 (not ?n4)))
 (let ((?n7 (and ?n2 ?n6)))
 (let ((?n8 (not ?n7))) ?n8
))))))))
(check-sat)
(exit)
