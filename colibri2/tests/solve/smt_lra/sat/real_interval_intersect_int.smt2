(set-logic QF_LRA)
(declare-fun s () Real)
(declare-fun o () Real)
(assert (let ((__ 0.0))
  (and
    (<= 0 s)
    (<= o (+ (* s (- 1)) (* o (- 1))))
    (<= 0 o)
  )
))
(check-sat)
