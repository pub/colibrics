(set-logic QF_LRA)
(declare-fun v0 () Real)
(assert
 (let ((?n2 (- v0)))
 (let ((?n3 (+ v0 ?n2)))
 (let ((?n4 (= ?n2 ?n3)))
 (let ((?1 1))
 (let ((?n6 (+ v0 ?n3)))
 (let ((?n7 (ite ?n4 ?1 ?n6)))
 (let ((?n8 (= ?n7 v0)))
 (let ((?n9 (=> false ?n8))) ?n9
)))))))))
(check-sat)
(exit)
