(set-logic QF_LRA)
(declare-fun v0 () Real)
(assert
 (let ((?12 12))
 (let ((?n3 (ite false ?12 ?12)))
 (let ((?n4 (= v0 ?n3)))
 (let ((?2 2))
 (let ((?n6 (= ?2 ?n3)))
 (let ((?n7 (= ?n4 ?n6))) ?n7
)))))))
(check-sat)
(exit)
