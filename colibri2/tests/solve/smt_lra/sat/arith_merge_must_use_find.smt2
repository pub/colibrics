(set-logic QF_LRA)
(declare-fun v0 () Real)
(assert
 (let ((?n2 2))
 (let ((?n3 (* v0 ?n2)))
 (let ((?n4 (= v0 ?n3)))
 (let ((?n5 (ite false v0 v0)))
 (let ((?n6 (ite ?n4 v0 ?n5)))
 (let ((?n7 (= v0 ?n6)))
 (let ((?n8 (xor false ?n7))) ?n8
))))))))
(check-sat)
(exit)
