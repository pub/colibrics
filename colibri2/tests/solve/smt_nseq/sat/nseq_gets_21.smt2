(set-logic ALL)

(declare-const a (NSeq Int))
(declare-const b (NSeq Int))

(assert (= (nseq.first a) 0))
(assert (= (nseq.last a) 1))
(assert (= (nseq.first b) 0))
(assert (= (nseq.last b) 1))

(assert (= (nseq.get a 0) 1))
(assert (= (nseq.get a 1) 2))

(assert (= (nseq.get b 0) 1))
(assert (= (nseq.get b 1) 3))

(assert (distinct a b))
(check-sat)
