(set-logic ALL)
(declare-sort p 0)
(declare-fun p (p) Int)
(declare-sort c 0)
(declare-fun t (c) Int)
(declare-sort u 0)
(declare-fun g (u) (NSeq c))
(define-fun e ((a2 u) (i Int)) c (nseq.get (g a2) i))
(declare-fun l (u) Int)
(declare-const n u)
(assert (exists ((b (Array Int p))) (and (distinct (nseq.get (g n) 1) (nseq.get (g n) (+ 1 (l n)))) (exists ((c Int)) (not (ite (= (p (select b 0)) (+ 1 (l n))) (forall ((k Int)) (= 0 (t (nseq.get (g n) 0)))) false))))))
(check-sat)

; step limit reached with --nseq-ext and no options
