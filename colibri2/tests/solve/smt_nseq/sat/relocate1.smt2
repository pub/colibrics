(set-logic ALL)
(declare-sort E 0)
(declare-fun a () (NSeq Int))
(declare-fun e () Int)
(assert (distinct
  (nseq.relocate (nseq.const 0 0 e) 1)
  (nseq.relocate (nseq.const 0 0 e) 0)))
(check-sat)
