(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status sat 
--dont-print-result %{dep:test4.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status sat --learning --dont-print-result %{dep:test4.smt2})) (package colibri2))
