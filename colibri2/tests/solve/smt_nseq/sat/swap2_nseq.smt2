(set-logic ALL)
(declare-sort E 0)
(declare-fun a () (NSeq E))
(declare-fun b () (NSeq E))
(declare-fun i () Int)
(assert (= (nseq.first a) 0))
(assert (= (nseq.first b) 0))
(assert (=
  (nseq.set
    a
    0
    (nseq.get (nseq.const 0 0 (nseq.get b 0)) 0))
  (nseq.set
    (nseq.set b i (nseq.get b 0))
    0
    (nseq.get (nseq.set b 0 (nseq.get a 0)) 0))))
(assert (distinct a b))
(check-sat)
