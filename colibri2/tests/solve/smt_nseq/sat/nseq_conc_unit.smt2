(set-logic ALL)

(declare-const a (NSeq Int))
(assert (= (nseq.first a) 0))
(assert (= (nseq.last a) (- 1)))

(assert (=
  (nseq.concat a (nseq.const 0 0 1))
  (nseq.const 0 0 1)))

(check-sat)
