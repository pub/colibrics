(set-logic ALL)

(declare-const a Int)
(declare-const b Int)
(declare-const c Int)

(declare-const x (NSeq Int))
(declare-const y (NSeq Int))
(declare-const z (NSeq Int))

(assert (< a b c))

(assert (= (nseq.first y) a))
(assert (= (nseq.last  y) b))
(assert (= (nseq.first z) (+ b 1)))
(assert (= (nseq.last  z) c))

(assert (= x (nseq.concat y z)))

(check-sat)
