(set-logic ALL)

(declare-const a Int)
(declare-const b Int)
(declare-const c Int)
(declare-const d Int)

(assert (< a b c d))

(define-fun x1 () (NSeq Int) (nseq.set (nseq.const a b 0) b 1))
(define-fun x2 () (NSeq Int) (nseq.set (nseq.const (+ b 1) d 0) d 1))
(define-fun y1 () (NSeq Int) (nseq.set (nseq.const a c 0) b 1))
(define-fun y2 () (NSeq Int) (nseq.set (nseq.const (+ c 1) d 0) d 1))

(assert (distinct (nseq.concat x1 x2) (nseq.concat y1 y2)))
(check-sat)
