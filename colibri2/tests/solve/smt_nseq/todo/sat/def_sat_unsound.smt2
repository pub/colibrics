(set-logic ALL)

(declare-sort E 0)
(declare-const ns (NSeq E))
(declare-const i Int)

(assert (and
  (not (<= 1 i))
  (= 0 (nseq.first ns))
  (= 0 (nseq.last ns))
  (= ns (nseq.slice ns 1 i))
))
(check-sat)
