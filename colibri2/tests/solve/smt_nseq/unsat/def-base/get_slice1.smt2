(set-logic ALL)

(declare-const a (NSeq Int))
(declare-const b (NSeq Int))

(assert (= a (nseq.set (nseq.set (nseq.const 0 5 0) 2 1) 5 2)))
(assert (= b (nseq.slice a 2 3)))

(assert (distinct (nseq.get b 2) 1))
(check-sat)
