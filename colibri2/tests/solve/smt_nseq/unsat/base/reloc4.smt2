(set-logic ALL)

(declare-const a (NSeq Int))
(declare-const b (NSeq Int))
(declare-const v Int)

(assert (= a (nseq.set (nseq.set (nseq.const 0 5 0) 1 1) 3 3)))
(assert (= b (nseq.set (nseq.set (nseq.const 5 10 0) 6 1) 8 3)))

(assert (distinct a (nseq.relocate b 0)))
(check-sat)

; add two callbacks that detect equalities of relocated consts