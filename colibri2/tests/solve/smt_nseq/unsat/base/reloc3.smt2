(set-logic ALL)
(set-option :max-steps-colibri2 4000)

(declare-const a (NSeq Int))
(declare-const b (NSeq Int))
(declare-const v Int)

(assert (= a (nseq.set (nseq.const 0 5 0) 1 1)))
(assert (= b (nseq.set (nseq.const 5 10 0) 6 1)))

(assert (distinct (nseq.relocate a 15) (nseq.relocate (nseq.relocate b 0) 15)))
(check-sat)
