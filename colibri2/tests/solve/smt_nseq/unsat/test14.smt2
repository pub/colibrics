(set-logic ALL)

(declare-sort i 0)
(declare-sort u 0)
(declare-fun f (u) (NSeq i))
(define-fun g ((_low Int) (high Int)) Bool (< 0 high))
(declare-fun h (u) Int)
(declare-const x1 u)
(declare-const x2 u)
(declare-const x3 u)

(assert (not (=>
      (g (nseq.first (f x1)) (nseq.last (f x1)))
      (= 0 (nseq.last (f x3)))
      (ite
        (= (nseq.first (f x1)) (nseq.first (f x3)))
        (and
          (= (h x2) (nseq.length (f x3)))
          (= (h x1) (nseq.length (f x1)))
        )
        false)
      (> (+ (h x1) (nseq.first (f x1))) 0)
)))

(check-sat)

; works with 81e6dfa7234b294c5cff164d6e7334e114e0da47
