(set-logic ALL)
(declare-sort e 0)
(declare-sort u 0)
(declare-fun g (u) (NSeq e))
(declare-fun o ((Array Int e) Int Int) u)
(declare-datatypes ((s_ 0)) (((s_ (e (Array Int e))))))
(declare-datatypes ((us 0)) (((us (us_ s_)))))
(declare-const s us)

(assert (exists ((t u)) (and
  (distinct 0 (nseq.last (g t)))
  (= 0 (nseq.last (g (o (e (us_ s)) 0 0))))
  (= 0 (nseq.first (g (o (e (us_ s)) 0 0))))
  (= (g t) (nseq.relocate (g (o (e (us_ s)) 0 0)) 0))
)))

(check-sat)
