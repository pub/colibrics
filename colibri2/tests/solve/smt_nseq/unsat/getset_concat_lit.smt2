(set-logic ALL)

(declare-const y (NSeq Int))

(define-fun a () Int (- 5))
(define-fun b () Int 5)
(define-fun c () Int 10)

(assert (= y (nseq.concat
    (nseq.set (nseq.const a b 0) b 2)
    (nseq.const (+ b 1) c 1)
)))
(assert ;(or
  ; (distinct (nseq.get y 0) 0) ; too many steps for some reason
  (distinct (nseq.get y b) 2)
  ;(distinct (nseq.get y c) 1)
);)

(check-sat)

; TODO: investigate why it requires many steps (> 3500)
; especially with "--nseq-ext"
