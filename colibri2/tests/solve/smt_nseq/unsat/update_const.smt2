(set-logic ALL)
(declare-sort E 0)
(declare-fun a () (NSeq E))
(declare-fun v1 () E)
(declare-fun v2 () E)
(assert (= (nseq.first a) 0))
(assert (> (nseq.last a) 0)) ; works with equal
; takes forever with no constraints on first and last,
; while it should be able to generate counter examples
(assert (distinct v1 v2))
(assert (=
  (nseq.update a (nseq.const 0 0 v1))
  (nseq.update a (nseq.const 0 0 v2))))
(check-sat)
