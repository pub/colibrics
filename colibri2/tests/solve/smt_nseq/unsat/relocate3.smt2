(set-logic ALL)

(declare-const a (NSeq Int))
(declare-const i Int)

(assert (= a (nseq.const 0 5 0)))
(assert (=
  (nseq.relocate (nseq.set a 0 1) i)
  (nseq.relocate (nseq.set a 0 2) i)
))

(check-sat)
