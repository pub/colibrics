(set-logic ALL)

(declare-const a (NSeq Int))
(declare-const b (NSeq Int))
(declare-const v Int)

(assert (= a (nseq.set (nseq.const 0 5 0) 1 1)))
(assert (= b (nseq.set (nseq.const 5 10 0) 6 1)))

(assert (= a b))
(check-sat)
