(set-logic ALL)

(declare-const y (NSeq Int))
(declare-const z (NSeq Int))

(assert (< (nseq.last y) (nseq.last z)))
(assert (= (nseq.last y) (nseq.first y)))
(assert (= (nseq.first z) (+ 1 (nseq.last y))))
(assert (= (nseq.concat z z) (nseq.concat y z)))

;(assert (distinct (nseq.get z (nseq.last z)) (nseq.get (nseq.concat z z) (nseq.last z))))

(check-sat)
