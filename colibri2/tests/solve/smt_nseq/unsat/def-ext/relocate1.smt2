(set-logic ALL)

(declare-const i Int)

(assert (distinct (nseq.get (nseq.relocate (nseq.const 0 5 0) i) i) 0))

(check-sat)
