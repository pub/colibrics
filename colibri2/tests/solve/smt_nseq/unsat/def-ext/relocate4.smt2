(set-logic ALL)

(declare-const a (NSeq Int))
(declare-const b (NSeq Int))
(declare-const i Int)

(assert (= a (nseq.set (nseq.const 0 5 0) 3 1)))
(assert (= b (nseq.relocate a i)))

(assert (distinct (nseq.get b (+ 3 i)) 1))

(check-sat)
