(set-logic ALL)
(declare-sort T2 0)
(declare-sort T3 0)
(declare-fun f2 (T3) (NSeq T2))
(declare-fun f4 ((Array Int T2) Int Int) T3)
(declare-const j Int)
(declare-const sc3 (Array Int T2))

(assert (forall ((f Int) (l Int) (a (Array Int T2)))
  (and
    (= 1 (nseq.last (f2 (f4 a 0 0))))
    (= 0 (nseq.first (f2 (f4 a f l))))
    (forall ((i Int))
      (=
        (select a i)
        (nseq.get (f2 (f4 a 0 1)) 0))))))

(assert
  (distinct
    (select sc3 j)
    (nseq.get
      (nseq.slice (f2 (f4 sc3 0 1)) 0 0)
      (nseq.last (nseq.slice (f2 (f4 sc3 0 0)) 0 0) )
    )))

(check-sat)

