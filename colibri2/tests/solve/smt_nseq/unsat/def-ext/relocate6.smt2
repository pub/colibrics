(set-logic ALL)

(declare-const a (NSeq Int))
(declare-const i Int)

(assert (= a (nseq.relocate (nseq.set (nseq.const 0 5 0) 3 1) i)))

(assert (distinct (nseq.get a (+ 3 i)) 1))
(check-sat)
