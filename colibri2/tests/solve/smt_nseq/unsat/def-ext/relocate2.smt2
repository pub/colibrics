(set-logic ALL)

(declare-const i Int)

(assert (distinct
  (nseq.get (nseq.relocate (nseq.set (nseq.const 0 5 0) 0 1) i) i)
  1
))

(check-sat)
