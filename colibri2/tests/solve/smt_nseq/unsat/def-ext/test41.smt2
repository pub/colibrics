(set-logic ALL)
(declare-sort T1 0)
(declare-sort T2 0)
(declare-fun f1 (T2) (NSeq T1))
(declare-fun f2 ((Array Int T1) Int Int) T2)
(declare-const s (Array Int T1))

(assert (forall ((f Int) (l Int) (a (Array Int T1)))
  (and
    (= 1 (nseq.last (f1 (f2 a 0 0))))
    (= 0 (nseq.first (f1 (f2 a f l))))
    (forall ((i Int))
      (=
        (select a i)
        (nseq.get (f1 (f2 a 0 1)) 0))))))

(assert
  (distinct
    (select s 0)
    (nseq.get
      (nseq.slice (f1 (f2 s 0 1)) 0 0)
      (nseq.last (nseq.slice (f1 (f2 s 0 0)) 0 0) )
    )))

(check-sat)
