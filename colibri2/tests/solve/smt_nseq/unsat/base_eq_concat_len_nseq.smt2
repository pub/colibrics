(set-logic ALL)
(declare-const c Bool)
(declare-sort E 0)
(declare-fun a () (NSeq E))
(declare-fun b () (NSeq E))
(declare-fun v () E)

(assert (= (nseq.first a) 0))
(assert (= (nseq.first b) 0))
(assert (ite (< (nseq.last a) 0) (= (nseq.last a) (- 1)) true))
(assert (ite (< (nseq.last b) 0) (= (nseq.last b) (- 1)) true))

(assert (=
  (nseq.concat (nseq.const 0 0 v) (nseq.relocate b 1))
  (nseq.concat (nseq.const 0 0 v) (nseq.relocate a 1))
))
(assert (distinct a b))
(check-sat)
