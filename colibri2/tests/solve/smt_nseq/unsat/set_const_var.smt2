(set-logic ALL)

(declare-const v Int)

(assert (distinct
  (nseq.set (nseq.const 0 0 0) 0 v)
  (nseq.set (nseq.const 0 0 1) 0 v)
))

(check-sat)

; step limit reached --nseq-base and no options; unsat with nseq-ext
