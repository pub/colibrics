(set-logic ALL)

(declare-const a Int)
(declare-const b Int)
(declare-const c Int)
(declare-const y (NSeq Int))

(assert (< a b c))

(assert (= y (
  nseq.concat
    (nseq.set (nseq.const a b 0) b 1)
    (nseq.set (nseq.const (+ b 1) c 0) c 1)
)))

(assert
  (distinct (nseq.get y b) 1)
)

(check-sat)
