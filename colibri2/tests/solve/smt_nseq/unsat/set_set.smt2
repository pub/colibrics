(set-logic ALL)

(declare-const a (NSeq Int))

(assert (= (nseq.first a) 0))
(assert (= (nseq.last a) 1))

(assert
  (distinct (nseq.set (nseq.set a 1 1) 0 0) (nseq.set (nseq.set a 0 0) 1 1))
)

(check-sat)