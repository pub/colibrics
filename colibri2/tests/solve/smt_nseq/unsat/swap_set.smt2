(set-logic ALL)

(declare-fun a1 () (NSeq Int))
(declare-fun a2 () (NSeq Int))
(declare-fun i () Int)

(assert (=
  (nseq.set a1 i (nseq.get a2 i))
  (nseq.set a2 i (nseq.get a1 i))
))
(assert (distinct a1 a2))

(check-sat)
