(set-logic ALL)

(assert (or
  (distinct (nseq.get (nseq.set (nseq.const 0 5 0) 1 1) 1) 1)
  (distinct (nseq.get (nseq.set (nseq.const 0 5 0) 1 1) 2) 0)
))

(check-sat)
