(set-logic ALL)

(declare-sort E 0)
(declare-const v E)

(assert (distinct
  (nseq.concat
    (nseq.const 0 0 v)
    (nseq.relocate (nseq.const 0 0 v) 1))
  (nseq.concat
    (nseq.const 0 0 v)
    (nseq.relocate (nseq.slice (nseq.const 0 0 v) 0 0) 1))
))

(check-sat)
