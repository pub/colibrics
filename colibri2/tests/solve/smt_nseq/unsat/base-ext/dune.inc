(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat 
--dont-print-result %{dep:concat_consts.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat --learning --dont-print-result %{dep:concat_consts.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat 
--dont-print-result %{dep:nseq_concat_empty.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-base --check-status unsat --learning --dont-print-result %{dep:nseq_concat_empty.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat 
--dont-print-result %{dep:concat_consts.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat --learning --dont-print-result %{dep:concat_consts.smt2})) (package colibri2))
(rule (alias runtest) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat 
--dont-print-result %{dep:nseq_concat_empty.smt2})) (package colibri2))
(rule (alias runtest-learning) (action (run %{bin:colibri2} --backtrace --size=50M --time=60s --max-steps 3500 --nseq-ext --check-status unsat --learning --dont-print-result %{dep:nseq_concat_empty.smt2})) (package colibri2))
