(set-logic ALL)

(declare-const v Int)

(assert (distinct
  (nseq.set (nseq.const 0 0 0) 0 1)
  (nseq.set (nseq.const 0 0 1) 0 1)
))

(check-sat)

; step limit reached --nseq-base and no options; unsat with nseq-ext
