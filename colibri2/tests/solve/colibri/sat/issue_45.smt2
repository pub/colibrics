;; produced by colibri.drv ;;
(set-logic ALL)
(set-info :smt-lib-version 2.6)
(set-info :status sat)


(declare-const x Float32)

(declare-const y Float32)

(declare-const z Float32)

;; Assume
(assert (fp.leq (fp #b0 #b00000000 #b00000000000000000000000) x))

;; Assume
(assert (fp.leq (fp #b0 #b00000000 #b00000000000000000000000) y))

;; Assume
(assert (fp.leq (fp #b0 #b00000000 #b00000000000000000000000) z))

;; Assume
(assert (fp.leq x (fp #b0 #b10010111 #b00000000000000000000000)))

;; Assume
(assert (fp.leq y (fp #b0 #b10010111 #b00000000000000000000000)))


(assert
  (not (= (ite (fp.lt (fp.neg (fp.mul RNE x y)) z) true false) true)))

(check-sat)
