;; produced by colibri.drv ;;
(set-logic ALL)
(set-info :smt-lib-version 2.6)
(set-info :status-colibri2 steplimitreached)

(define-fun fp.isFinite32 ((x Float32)) Bool (not (or (fp.isInfinite x) (fp.isNaN x))))

(declare-const x Float32)

;; Assume
(assert
  (and
    (fp.leq (fp #b0 #b10000001 #b01000000000000000000000) x)
    (fp.leq x (fp #b0 #b10000010 #b01000000000000000000000))))

;; o
(define-fun o () Float32
  (fp.mul RNE x (fp #b0 #b10000000 #b00000000000000000000000)))

;; Ensures
(assert (fp.isFinite32 o))

;; o
(define-fun o1 () Float32
  (fp.sub RNE o (fp #b0 #b10000001 #b01000000000000000000000)))

;; Ensures
(assert (fp.isFinite32 o1))

;; Goal def'vc
;; File "range_mult.adb", line 4, characters 0-0
(assert
  (not (fp.lt x o1)))

(check-sat)
