(set-logic ALL)
(set-info :smt-lib-version 2.6)
(set-info :status-colibri2 sat)
(declare-const modes_on Bool)
(declare-const unit_delay_memory Bool)
(assert
(not
(= (ite (= unit_delay_memory true) true (ite (= modes_on true) false true)) true)))
(check-sat)
