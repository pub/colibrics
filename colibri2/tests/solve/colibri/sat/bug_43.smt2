(set-logic ALL)
(set-info :smt-lib-version 2.6)
(set-info :status-colibri2 steplimitreached)

(define-fun in_range1 ((x Int)) Bool (and (<= (- 2147483648) x) (<= x 2147483647)))

(declare-const x Int)

(define-fun res () Int (- x (colibri_cdiv (- (* 2 x) 1) 2)))

;; Ensures
  (assert (in_range1 res))

;; H
  (assert (<= 0 x))

(assert
;; defqtvc
 ;; File "arithmetic.adb", line 291, characters 0-0
  (not (= res 0)))
(check-sat)
