(set-logic ALL)
(set-info :smt-lib-version 2.6)
(set-info :status-colibri2 sat)

(assert (not (= (colibri_crem (- 1) (- 1)) (- 1))))
(check-sat)
