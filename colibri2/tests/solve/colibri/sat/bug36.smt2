;; produced by colibri.drv ;;
(set-logic ALL)
(set-info :status-colibri2 sat)

(declare-fun bool_eq (Real Real) Bool)
(declare-const x Int)
(declare-fun to_big_real (Int) Real)

(assert
  (not
  (= (bool_eq (to_big_real x) (fp.to_real ((_ to_fp 11 53) RNE (to_real x)))) true)))
(check-sat)
