(set-logic ALL)
(set-info :smt-lib-version 2.6)
(set-info :status-colibri2 sat)
(declare-const x Int)
(declare-const y Int)
(assert
  (not (= (colibri_abs_int (colibri_cdiv x y))
          (colibri_cdiv (colibri_abs_int x) (colibri_abs_int y)))))
(check-sat)
