(set-logic ALL)
(set-info :smt-lib-version 2.6)
(set-info :status-colibri2 sat)

(declare-const x (_ BitVec 512))
(declare-const y (_ BitVec 512))
(declare-const z (_ BitVec 512))

(assert (= (bvurem x y) (bvurem x z)))
(assert (= (bvudiv x y) (bvudiv x z)))

(assert (not (= y z)))

(check-sat)
