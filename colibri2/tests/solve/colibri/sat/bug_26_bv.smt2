(set-logic ALL)
(set-info :smt-lib-version 2.6)
(set-info :status-colibri2 sat)
(declare-const salt (_ BitVec 64))
(declare-const hash (_ BitVec 64))
(define-fun o () (_ BitVec 64) (bvxor (bvmul #x0000000000000005 salt) (bvmul #x0000000000000007 hash)))
(assert
;; defqtvc
 ;; File "usergroup_examples.adb", line 42, characters 0-0
  (not (bvult #x0000000000000000 (bvurem o #x0000000000000100))))
(check-sat)
