;; produced by colibri.drv ;;
(set-logic ALL)
(set-info :smt-lib-version 2.6)
(set-info :status-colibri2 sat)
(declare-sort tuple0 0)

;;  uncomment to pass the VC to cvc4 or z3
;; (define-fun colibri_cdiv ((x Int) (y Int)) Int (div x y))
;; (define-fun colibri_crem ((x Int) (y Int)) Int (mod x y))
;; (define-fun colibri_abs_int ((x Int)) Int (abs x))

(define-fun mod1 ((x Int)
  (y Int)) Int (ite (< 0 y) (mod x y) (+ (mod x y) y)))

(declare-sort integer 0)

(define-fun in_range ((x Int)) Bool (and (<= (- 2147483648) x)
                                    (<= x 2147483647)))


(define-fun dynamic_invariant ((temp___expr_18 Int) (temp___is_init_14 Bool)
  (temp___skip_constant_15 Bool) (temp___do_toplevel_16 Bool)
  (temp___do_typ_inv_17 Bool)) Bool (=>
                                    (or (= temp___is_init_14 true)
                                    (<= (- 2147483648) 2147483647)) (in_range
                                    temp___expr_18)))

(declare-const x Int)

(declare-const y Int)

(declare-const z Int)

(declare-const tmp1 Int)

(declare-const tmp2 Int)

;; Assume
  (assert (dynamic_invariant x true false true true))

;; Assume
  (assert (dynamic_invariant y true false true true))

;; Assume
  (assert (dynamic_invariant z true false true true))

;; Assume
  (assert (dynamic_invariant tmp1 false false true true))

;; Assume
  (assert (dynamic_invariant tmp2 false false true true))

(define-fun o () Int (+ x y))

;; Ensures
  (assert (in_range o))

;; Ensures
  (assert (in_range (+ o z)))

(define-fun o1 () Int (+ x y))

;; Ensures
  (assert (in_range o1))

;; Ensures
  (assert (in_range (- o1 z)))

(define-fun o2 () Int (+ x y))

;; Ensures
  (assert (in_range o2))

;; Ensures
  (assert (in_range (+ o2 z)))

(define-fun o3 () Int (- y z))

;; Ensures
  (assert (in_range o3))

;; Ensures
  (assert (in_range (+ x o3)))

;; Assert
  (assert (or (not (= x (- 2147483648))) (not (= y (- 1)))))

;; Ensures
  (assert (in_range (* x y)))

;; Uncomment to prove VC in z3 and cvc4
;;  (assert (distinct y 0))

(assert
;; defqtvc
 ;; File "arith.adb", line 1, characters 0-0
  (not (in_range (colibri_cdiv x y))))
(check-sat)
