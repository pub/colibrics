;; produced by colibri.drv ;;
(set-logic ALL)
(set-info :smt-lib-version 2.6)
(set-info :status-colibri2 steplimitreached)

(define-fun mod1 ((x Int) (y Int)) Int (ite (< 0 y) (mod x y) (+ (mod x y) y)))


(define-fun in_range1 ((x Int)) Bool (and (<= 1 x) (<= x 100)))


(declare-const x Int)

;; Assume
  (assert (in_range1 x))

(assert (not (in_range1 (mod1 (+ x 1) 100))))
(check-sat)
;(get-value (x))
