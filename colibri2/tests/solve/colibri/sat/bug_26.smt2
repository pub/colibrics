;; produced by colibri.drv ;;
(set-logic ALL)
(set-info :smt-lib-version 2.6)
(set-info :status-colibri2 sat)

(define-fun in_range ((x Int)) Bool (and (<= (- 2147483648) x) (<= x 2147483647)))

(declare-const salt (_ BitVec 64))

(declare-const hash (_ BitVec 64))

(define-fun o () Int (bv2nat (bvxor (bvmul #x0000000000000005 salt) (bvmul #x0000000000000007 hash))))

;; Ensures
  (assert (in_range o))

(assert
;; defqtvc
 ;; File "usergroup_examples.adb", line 42, characters 0-0
  (not (<= 1 (mod o 256))))
(check-sat)
