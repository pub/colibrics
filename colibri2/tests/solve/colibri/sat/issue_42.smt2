;; produced by colibri.drv ;;
(set-logic ALL)
(set-info :smt-lib-version 2.6)
(set-info :status-colibri2 sat)

(declare-const a Int)
(assert (and (< 1 a) (< a 100)))

(assert (= (colibri_crem 100 a) 0))
(check-sat)
