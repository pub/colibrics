;; produced by colibri.drv ;;
(set-logic ALL)
(set-info :smt-lib-version 2.6)
(set-info :status-colibri2 steplimitreached)




(define-fun in_range1 ((x Int)) Bool (and (<= (- 2147483648) x)
                                     (<= x 2147483647)))

(declare-const a Int)
(declare-const n Int)
(declare-const b Int)

(assert (< (colibri_pow_int_int a n) 2147483647))





(assert
;; defqtvc
 ;; File "fib.ads", line 29, characters 0-0
  (not (in_range1 (* b b))))
(check-sat)
