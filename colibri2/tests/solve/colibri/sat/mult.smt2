;; produced by colibri.drv ;;
(set-logic ALL)
(set-info :smt-lib-version 2.6)
(set-info :status-colibri2 steplimitreached)

(define-fun in_range1 ((x Int)) Bool (and (<= (- 2147483648) x)
                                     (<= x 2147483647)))

(declare-const b11 Int)
(assert (in_range1 b11))

(assert (not (<= (* b11 b11) 2147483647)))
(check-sat)
