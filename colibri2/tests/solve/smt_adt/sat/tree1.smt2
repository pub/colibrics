;; produced by colibri.drv ;;
(set-logic ALL)

(declare-datatype tree ( par (X) (
  ( leaf )
  ( node ( head X) ( left ( tree X )) ( right ( tree X ))))))

(declare-fun a () (tree Int))

(assert ((_ is node) a))
(assert ((_ is leaf) (left a)))

(check-sat)
