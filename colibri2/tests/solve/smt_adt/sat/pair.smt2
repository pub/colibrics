;; produced by colibri.drv ;;
(set-logic ALL)

(declare-datatype pair (
  ( pair (fst Int) (snd Int))))

(declare-fun a () pair)
(declare-fun b () pair)

(assert (= (fst a) (fst b)))
(assert (= (snd a) (snd b)))

(assert (> (fst a) 1))

(assert (= a b))

(check-sat)
