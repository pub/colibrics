;; produced by colibri.drv ;;
(set-logic ALL)

(declare-datatype list ( par (X) (
  ( nil )
  ( cons ( head X) ( tail ( list X ))))))

(assert (not (= nil (cons 1 nil))))

(check-sat)
