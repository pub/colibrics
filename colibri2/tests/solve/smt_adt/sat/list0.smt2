;; produced by colibri.drv ;;
(set-logic ALL)

(declare-datatype list ( par (X) (
  ( nil )
  ( cons ( head X) ( tail ( list X ))))))

(assert ((_ is cons) (cons 1 nil)))

(check-sat)
