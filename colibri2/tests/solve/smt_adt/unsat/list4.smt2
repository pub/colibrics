;; produced by colibri.drv ;;
(set-logic ALL)

(declare-datatype list ( par (X) (
  ( nil )
  ( cons ( head X) ( tail ( list X ))))))

(assert (= (cons 2 (cons 0 nil))  (cons 2 (cons 1 nil))))

(check-sat)
