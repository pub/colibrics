;; produced by colibri.drv ;;
(set-logic ALL)

(declare-datatype enum (
  ( A )
  ( B )
  ( C )))

(declare-fun f (enum) Bool)
(declare-fun x () enum)

(assert (f B))
(assert (f C))
(assert (not (f x)))
(assert (not ((_ is A) x)))

(check-sat)
