;; produced by colibri.drv ;;
(set-logic ALL)

(declare-datatype list ( par (X) (
  ( nil )
  ( cons ( head X) ( tail ( list X ))))))

(assert (not ((_ is cons) (cons 1 nil))))

(check-sat)
