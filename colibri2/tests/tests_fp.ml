(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open OUnit2
open Colibri2_core
open Tests_lib
open Colibri2_solver
open Colibri2_theories_bool
open Colibri2_theories_fp

let theories = [ Boolean.th_register; Equality.th_register; Fp.th_register ]
let ( $$ ) f x = f x
let run f = Scheduler.run_exn ~step_limit:3000 ~nodec:() ~theories f

let recognize_float32 _ =
  run @@ fun env ->
  let a = fresht (Expr.Ty.float 8 24) "a" in
  let b = fresht (Expr.Ty.float 8 24) "b" in
  let t = Expr.Term.eq a b in
  let an = Ground.convert env a in
  let bn = Ground.convert env b in
  let tn = Ground.convert env t in
  Egraph.register env an;
  Egraph.register env bn;
  Egraph.register env tn;
  Boolean.set_true env tn;
  fun env -> assert_bool "a = b" (is_equal env an bn)

let recognize_float64 _ =
  run @@ fun env ->
  let a = fresht (Expr.Ty.float 11 53) "a" in
  let b = fresht (Expr.Ty.float 11 53) "b" in
  let t = Expr.Term.eq a b in
  let an = Ground.convert env a in
  let bn = Ground.convert env b in
  let tn = Ground.convert env t in
  Egraph.register env an;
  Egraph.register env bn;
  Egraph.register env tn;
  Boolean.set_true env tn;
  fun env -> assert_bool "a = b" (is_equal env an bn)

let basic = "FP.Basic" &: [ recognize_float32; recognize_float64 ]
let tests = test_list [ basic ]
