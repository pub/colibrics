(prover
 (name colibri2)
 (binary $cur_dir/regression/static/colibri2)
 (cmd "$binary --debug-flag stats $file")
 (version "$binary --version")
 (sat "^sat$")
 (unsat "^unsat$")
 (unknown "^unknown$")
 )

(dir
    (name sat)
    (path $cur_dir/regression/sat/)
    (pattern ".*[.][p]?smt2")
    (expect (const sat))
)

(dir
    (name unsat)
    (path $cur_dir/regression/unsat/)
    (pattern ".*[.][p]?smt2")
    (expect (const unsat))
)

(dir
    (name all)
    (path $cur_dir/regression/all/)
    (pattern ".*[.][p]?smt2")
    (expect (run smtlib-read-status))
)


(task
  (name runall)
  (action
    (run_provers_slurm
      (nodes 4)
      (addr 10.7.18.101)
      (j 36)
      (ntasks 10)
      (partition cpu)
      (provers colibri2)
      (dirs (${dir:sat} ${dir:unsat} ${dir:all})))))