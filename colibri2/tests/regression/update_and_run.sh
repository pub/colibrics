#!/bin/sh -eu
# On factory-ia

cd $(dirname $0)

SHABEFORE=$(sha1sum $0)

git pull

SHAAFTER=$(sha1sum $0)

if [ "$SHABEFORE" != "$SHAAFTER" ]; then exec $0; fi

mkdir static -p
wget "https://git.frama-c.com/pub/colibrics/-/jobs/artifacts/master/raw/bin/colibri2/colibri2?job=generate-static:%20[4.14.0]" -O static/colibri2
chmod u+x static/colibri2

mkdir -p all

if [ $(git -C  all/QF_UFIDL/ rev-parse HEAD) != 6a34b677cc73cae2564e9a47c7afd8d3a305116b ]; then
  echo wrong version of QF_UFIDL;
  exit 1
fi

exec ./run_regression.sh