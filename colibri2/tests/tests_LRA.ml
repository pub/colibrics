(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open OUnit2
open Tests_lib
open Colibri2_theories_bool
open Colibri2_theories_LRA
open Colibri2_solver
open Colibri2_core
open Colibri2_stdlib.Std
open Colibri2_stdlib

let theories =
  [
    Boolean.th_register;
    Equality.th_register;
    Colibri2_theories_quantifiers.Uninterp.th_register;
    LRA.th_register;
  ]

let run f = Scheduler.run_exn ~step_limit:3000 ~nodec:() ~theories f
let run_dec f = Scheduler.run_exn ~step_limit:3000 ?nodec:None ~theories f
let ( $$ ) f x = f x

(* The tests with rundec check only the result on a model satisfying
   the hypothesis *)

open Expr

let solve0a _ =
  run @@ fun env ->
  let a = fresh env Ty.real "ar" in
  let _1 = LRA.RealValue.cst A.one in
  let a1 = LRA.add env a _1 in
  register env a1;
  register env _1;
  merge env a1 _1;
  fun env -> assert_bool "a+1 = 1 => a = 0" (is_equal env a LRA.RealValue.zero)

let solve0b _ =
  run @@ fun env ->
  let a = fresh env Ty.real "ar" in
  let _1 = LRA.RealValue.cst A.one in
  let _2 = LRA.RealValue.cst A.two in
  let _4 = LRA.RealValue.cst (A.of_int 4) in
  let a1 = LRA.add env a _1 in
  let _2a2 = LRA.add' env A.two a A.one _2 in
  List.iter (register env) [ a1; _1; _2; _4; _2a2 ];
  merge env a1 _2;
  fun env -> assert_bool "a+1 = 2 => 2*a+2 = 4" (is_equal env _2a2 _4)

let solve0c _ =
  run @@ fun env ->
  let a = fresh env Ty.real "ar" in
  let b = fresh env Ty.real "br" in
  let _1 = LRA.RealValue.cst A.one in
  let a1 = LRA.add env a _1 in
  let b1 = LRA.add env b _1 in
  register env a1;
  register env b1;
  merge env a1 b1;
  fun env -> assert_bool "a+1 = b+1 => a = b" (is_equal env a b)

let solve1 _ =
  run @@ fun env ->
  let a, b = Shuffle.seq2 (fresh env Ty.real) ("ar", "br") in
  let _1 = LRA.RealValue.cst A.one in
  let a1 = LRA.add env a _1 in
  let b1 = LRA.add env b _1 in
  let _2 = LRA.RealValue.cst (A.of_int 2) in
  let a2 = LRA.add env a _2 in
  let b2 = LRA.add env b _2 in
  Shuffle.seql' (register env) [ a1; b1; a2; b2 ];
  merge env a1 b1;
  fun env -> assert_bool "a+1 = b+1 => a+2 = b+2" (is_equal env a2 b2)

let solve2 _ =
  run @@ fun env ->
  let a, b = Shuffle.seq2 (fresh env Ty.real) ("ar", "br") in
  let _1 = LRA.RealValue.cst A.one in
  let a1 = LRA.add env a _1 in
  let b1 = LRA.add env b _1 in
  let _2 = LRA.RealValue.cst (A.of_int 2) in
  let a2 = LRA.add env a _2 in
  let b2 = LRA.add env b _2 in
  Shuffle.seql' (register env) [ a1; b1; a2; b2 ];
  merge env a2 b1;
  fun env -> assert_bool "a+2 = b+1 => a+1 = b" (is_equal env a1 b)

let solve3 _ =
  run @@ fun env ->
  let a, b = Shuffle.seq2 (fresh env Ty.real) ("ar", "br") in
  let _1 = LRA.RealValue.cst A.one in
  let b1 = LRA.add env b _1 in
  let _2 = LRA.RealValue.cst (A.of_int 2) in
  let a2 = LRA.add env a _2 in
  let _3 = LRA.RealValue.cst (A.of_int 3) in
  Shuffle.seql
    [
      (fun () ->
        Shuffle.seql' (register env) [ b1; a2 ];
        merge env a2 b1);
      (fun () ->
        Shuffle.seql' (register env) [ a; _2 ];
        merge env a _2);
      (fun () -> register env _3);
    ];
  fun env ->
    assert_bool "" (not (is_equal env b _2));
    assert_bool "a+2 = b+1 => a = 2 => b = 3" (is_equal env b _3)

let solve4 _ =
  run @@ fun env ->
  let a, b, c = Shuffle.seq3 (fresh env Ty.real) ("ar", "br", "cr") in
  let t1 = LRA.RealValue.cst (A.of_int 2) in
  let t1 = LRA.add env t1 c in
  let t1 = LRA.add env a t1 in
  let t1' = LRA.RealValue.cst (A.of_int 1) in
  let t1' = LRA.add env b t1' in
  let t2 = a in
  let t2' = LRA.RealValue.cst (A.of_int 2) in
  let t2' = LRA.add env t2' b in
  let t3' = LRA.RealValue.cst (A.of_int (-3)) in
  Shuffle.seql
    [
      (fun () ->
        Shuffle.seql' (register env) [ t1; t1' ];
        merge env t1 t1');
      (fun () ->
        Shuffle.seql' (register env) [ t2; t2' ];
        merge env t2 t2');
      (fun () -> register env t3');
    ];
  fun env ->
    assert_bool "a+(2+c) = b+1 => a = 2 + b => c = -3" (is_equal env c t3')

let solve5 _ =
  run @@ fun env ->
  let a = fresh env Ty.real "ar" in
  let b = fresh env Ty.real "br" in
  let c = fresh env Ty.real "cr" in
  let t1 = LRA.sub env b c in
  let t1 = LRA.add env a t1 in
  let t1' = LRA.RealValue.cst (A.of_int 2) in
  let t2 = a in
  let t2' = LRA.RealValue.cst (A.of_int 2) in
  let t3 = LRA.add env b c in
  let t3' = LRA.add env b b in
  Shuffle.seql
    [
      (fun () ->
        Shuffle.seql' (register env) [ t1; t1' ];
        merge env t1 t1');
      (fun () ->
        Shuffle.seql' (register env) [ t2; t2' ];
        merge env t2 t2');
      (fun () -> Shuffle.seql' (register env) [ t3; t3' ]);
    ];
  fun env ->
    assert_bool "a+(b-c) = 2 => a = 2 => b + c = b + b" (is_equal env t3 t3')

let basic =
  "LRA.Basic"
  &: [ solve0a; (* solve0b; *) solve0c; solve1; solve2; solve3; solve4; solve5 ]

let mult0 _ =
  run @@ fun env ->
  let a = fresh env Ty.real "ar" in
  let b = fresh env Ty.real "br" in
  let t1 = LRA.sub env a b in
  let t1' = LRA.mult env a b in
  let t2 = a in
  let t2' = LRA.RealValue.cst (A.of_int 1) in
  let t3 = LRA.mult_cst env (A.of_int 2) b in
  let t3' = LRA.RealValue.cst (A.of_int 1) in
  Shuffle.seql
    [
      (fun () ->
        Shuffle.seql' (register env) [ t1; t1' ];
        merge env t1 t1');
      (fun () ->
        Shuffle.seql' (register env) [ t2; t2' ];
        merge env t2 t2');
      (fun () -> Shuffle.seql' (register env) [ t3; t3' ]);
    ];
  fun env ->
    assert_bool "a - b = a * b -> a = 1 -> 1 = 2b" (is_equal env t3 t3')

(** test that mult normalization trigger the needed solve *)
let mult1 _ =
  run @@ fun env ->
  let a = fresh env Ty.real "ar" in
  let b = fresh env Ty.real "br" in
  let c = fresh env Ty.real "cr" in
  let t1 = LRA.mult env a b in
  let t1 = LRA.add env a t1 in
  let t1' = LRA.add env b c in
  let t1' = LRA.mult env t1' a in
  let t2 = a in
  let t2' = LRA.RealValue.cst (A.of_int 2) in
  let t3 = c in
  let t3' = LRA.RealValue.cst (A.of_int 1) in
  Shuffle.seql
    [
      (fun () ->
        Shuffle.seql' (register env) [ t1; t1' ];
        merge env t1 t1');
      (fun () ->
        Shuffle.seql' (register env) [ t2; t2' ];
        merge env t2 t2');
      (fun () -> Shuffle.seql' (register env) [ t3; t3' ]);
    ];
  fun env ->
    assert_bool "a + (a * b) = (b + c) * a -> a = 2 -> c = 1"
      (is_equal env t3 t3')

let mult = "LRA.Mult" &: [ mult0; mult1 ]

(* let files = ["tests/tests_altergo_arith.split";
 *              "tests/tests_popop.split";
 *              "tests/tests_altergo_qualif.split"
 *             ]
 * 
 * let altergo = TestList (List.map Tests_lib.test_split files) *)

let tests =
  test_list
    [
      basic;
      mult
      (* altergo;*)
      (* smtlib2sat; smtlib2unsat *);
    ]
