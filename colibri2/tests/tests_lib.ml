(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open OUnit2
open Colibri2_core

let debug = Debug.register_flag ~desc:" Run the test in verbose mode." "ounit"

let fresht ty s =
  Expr.Term.apply_cst (Expr.Term.Const.mk (Dolmen_std.Path.global s) ty) [] []

let fresh env ty s =
  Ground.convert env
    (Expr.Term.apply_cst
       (Expr.Term.Const.mk (Dolmen_std.Path.global s) ty)
       [] [])

let ( &: ) s l = s >::: List.map (fun f -> OUnit2.test_case f) l

let register d cl =
  Egraph.register d cl;
  ForSchedulers.flush_internal d

let merge d cl1 cl2 =
  Egraph.merge d cl1 cl2;
  ForSchedulers.flush_internal d

let is_equal = Egraph.is_equal

type t = {
  wakeup_daemons : Events.daemon_key Queue.t;
  solver_state : ForSchedulers.Backtrackable.t;
  context : Colibri2_stdlib.Context.context;
}
(** without decisions *)

let new_solver () =
  let context = Colibri2_stdlib.Context.create () in
  {
    wakeup_daemons = Queue.create ();
    solver_state =
      ForSchedulers.Backtrackable.new_t
        (Colibri2_stdlib.Context.creator context);
    context;
  }

exception ReachStepLimit
exception Contradiction

let run_exn = Colibri2_solver.Scheduler.run_exn ~step_limit:3000
let for_tests_exn = Colibri2_solver.Scheduler.for_tests ~step_limit:3000

let check_file ?limit ~theories filename =
  let st =
    Colibri2_solver.Input.mk_state ~input:(`File filename) theories ~bt:false
      ?step_limit:limit
  in
  Colibri2_solver.Input.read ~show_check_sat_result:false
    ~set_true:Colibri2_theories_bool.Boolean.set_true
    ~handle_exn:Colibri2_solver.Input.handle_exn_with_error st;
  let r = Colibri2_solver.Input.get_state st in
  match r with
  | `Sat _ -> `Sat
  | `Unsat -> `Unsat
  | `UnknownUnsat -> `Unknown
  | `Search -> `Unknown
  | `Unknown _ -> `Unknown
  | `StepLimitReached -> `StepLimitReached

let tests_file ?limit ~theories expected dir =
  if Sys.file_exists dir then (
    let files = Sys.readdir dir in
    Array.sort String.compare files;
    let files = Array.to_list files in
    List.map
      (fun s ->
        s
        >: test_case (fun _ ->
               match check_file ?limit ~theories (Filename.concat dir s) with
               | `Sat ->
                   Colibri2_stdlib.Debug.dprintf1 debug "@[%s: Sat@]" s;
                   assert_bool s (`Sat = expected)
               | `Unsat ->
                   Colibri2_stdlib.Debug.dprintf1 debug "@[%s: Unsat@]" s;
                   assert_bool s (`Unsat = expected)
               | `Unknown ->
                   Colibri2_stdlib.Debug.dprintf1 debug "@[%s: Unknown@]" s;
                   assert_bool s (`Unknown = expected)
               | `StepLimitReached ->
                   Colibri2_stdlib.Debug.dprintf0 debug "StepLimit Reached@.";
                   assert_bool s false
               | exception exn ->
                   Colibri2_stdlib.Debug.dprintf2 debug "@.Error: %s@.%s@."
                     (Printexc.to_string exn)
                     (Printexc.get_backtrace ());
                   assert_bool s false))
      files)
  else []
