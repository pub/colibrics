(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open OUnit2
open Colibri2_core
open Tests_lib
open Colibri2_theories_bool

let theories =
  [
    Boolean.th_register;
    Equality.th_register;
    Colibri2_theories_quantifiers.Uninterp.th_register;
    Colibri2_theories_quantifiers.Quantifier.th_register;
  ]

let run f = Tests_lib.run_exn ~theories ~nodec:() f
let ( $$ ) f x = f x

open Expr

let ta = fresht Ty.bool "a"
let tb = fresht Ty.bool "b"
let tc = fresht Ty.bool "c"

let f =
  Expr.Term.Const.mk
    (Dolmen_std.Path.global "f")
    (Expr.Ty.arrow [ Ty.bool ] Ty.bool)

let g =
  Expr.Term.Const.mk
    (Dolmen_std.Path.global "g")
    (Expr.Ty.arrow [ Ty.bool; Ty.bool ] Ty.bool)

let h =
  Expr.Term.Const.mk
    (Dolmen_std.Path.global "h")
    (Expr.Ty.arrow [ Ty.bool; Ty.bool ] Ty.bool)

let rec gen_calls i s =
  let i = i + 1 in
  match s.[i] with
  | 'a' -> (ta, i)
  | 'b' -> (tb, i)
  | 'c' -> (tc, i)
  | 'f' ->
      let x, i = gen_calls i s in
      (Expr.Term.apply_cst f [] [ x ], i)
  | 'g' ->
      let x, i = gen_calls i s in
      let y, i = gen_calls i s in
      (Expr.Term.apply_cst g [] [ x; y ], i)
  | 'h' ->
      let x, i = gen_calls i s in
      let y, i = gen_calls i s in
      (Expr.Term.apply_cst h [] [ x; y ], i)
  | _ -> assert false

let gen_calls env s = Ground.convert env (fst (gen_calls (-1) s))

let empty _ =
  run @@ fun env ->
  let a = gen_calls env "a" in
  let b = gen_calls env "b" in
  register env a;
  register env b;
  fun env -> assert_bool "⊬ a == b" (not (is_equal env a b))

let tauto _ =
  run @@ fun env ->
  let a = gen_calls env "a" in
  let b = gen_calls env "b" in
  register env a;
  register env b;
  merge env a b;
  fun env -> assert_bool "a = b => a = b" (is_equal env a b)

let tauto_equal _ =
  run @@ fun env ->
  let a = gen_calls env "a" in
  let b = gen_calls env "b" in
  register env a;
  register env b;
  merge env a b;
  fun env -> assert_bool "a = b => a = b" (is_equal env a b)

let trans _ =
  run @@ fun env ->
  let a = gen_calls env "a" in
  let b = gen_calls env "b" in
  let c = gen_calls env "c" in
  register env a;
  register env b;
  register env c;
  merge env a b;
  merge env b c;
  fun env ->
    assert_bool "a = b => b = c => a = c" (is_equal env a c);
    assert_bool "a = b => b = c => a = b" (is_equal env a b);
    assert_bool "a = b => b = c => b = c" (is_equal env b c)

let noteq _ =
  run @@ fun env ->
  let a = gen_calls env "a" in
  let b = gen_calls env "b" in
  let c = gen_calls env "c" in
  register env a;
  register env b;
  register env c;
  merge env b c;
  fun env -> assert_bool "b = c => a != c" (not (is_equal env a c))

let basic =
  "Uf.Basic"
  >::: [
         "empty" >:: empty;
         "tauto" >:: tauto;
         "trans" >:: trans;
         "noteq" >:: noteq;
       ]

let refl _ =
  run @@ fun env ->
  let fa = gen_calls env "fa" in
  register env fa;
  fun env -> assert_bool "f(a) = f(a)" (is_equal env fa fa)

let empty _ =
  run @@ fun env ->
  let fa = gen_calls env "fa" in
  let fb = gen_calls env "fb" in
  register env fa;
  register env fb;
  fun env -> assert_bool "f(a) != f(b)" (not (is_equal env fa fb))

let congru _ =
  run @@ fun env ->
  let a = gen_calls env "a" in
  let b = gen_calls env "b" in
  let fa = gen_calls env "fa" in
  let fb = gen_calls env "fb" in
  Colibri2_stdlib.Shuffle.seql
    [
      (fun () -> register env fa);
      (fun () -> register env fb);
      (fun () -> register env a);
      (fun () -> register env b);
    ];
  merge env a b;
  fun env -> assert_bool "a = b => f(a) = f(b)" (is_equal env fa fb)

let _2level _ =
  run @@ fun env ->
  let a = gen_calls env "a" in
  let b = gen_calls env "b" in
  let ffa = gen_calls env "ffa" in
  let ffb = gen_calls env "ffb" in
  Colibri2_stdlib.Shuffle.seql
    [
      (fun () -> register env ffa);
      (fun () -> register env ffb);
      (fun () -> register env a);
      (fun () -> register env b);
    ];
  merge env a b;
  fun env -> assert_bool "a = b => f(f(a)) = f(f(b))" (is_equal env ffa ffb)

let _2level' _ =
  run @@ fun env ->
  let a = gen_calls env "a" in
  let b = gen_calls env "b" in
  let ffa = gen_calls env "ffa" in
  let ffb = gen_calls env "ffb" in
  register env a;
  register env b;
  merge env a b;
  register env ffa;
  register env ffb;
  fun env -> assert_bool "a = b => f(f(a)) = f(f(b))" (is_equal env ffa ffb)

let bigger _ =
  run @@ fun env ->
  let a = gen_calls env "a" in
  let fa = gen_calls env "fa" in
  let fffa = gen_calls env "fffa" in
  let fffffa = gen_calls env "fffffa" in
  Colibri2_stdlib.Shuffle.seql
    [
      (fun () -> register env a);
      (fun () -> register env fa);
      (fun () -> register env fffa);
      (fun () -> register env fffffa);
    ];
  Colibri2_stdlib.Shuffle.seql
    [ (fun () -> merge env a fffa); (fun () -> merge env fffffa a) ];
  fun env ->
    assert_bool "a = f(f(f(a))) => f(f(f(f(f(a))))) = a => f(a) = a"
      (is_equal env fa a)

let congru1 =
  "Uf.Congru 1 arg"
  >::: [
         "refl" >:: refl;
         "congru" >:: congru;
         "2level" >:: _2level;
         "2level'" >:: _2level';
         "bigger" >:: bigger;
       ]

let refl _ =
  run @@ fun env ->
  let gab = gen_calls env "gab" in
  register env gab;
  fun env -> assert_bool "g(a,b) = g(a,b)" (is_equal env gab gab)

let congru _ =
  run @@ fun env ->
  let gab = gen_calls env "gab" in
  let gaa = gen_calls env "gaa" in
  let a = gen_calls env "a" in
  let b = gen_calls env "b" in
  Colibri2_stdlib.Shuffle.seql
    [
      (fun () -> register env gab);
      (fun () -> register env gaa);
      (fun () -> register env a);
      (fun () -> register env b);
    ];
  merge env a b;
  fun env -> assert_bool "a = b => g(a,b) = g(a,a)" (is_equal env gab gaa)

let notcongru _ =
  run @@ fun env ->
  let gab = gen_calls env "gab" in
  let gaa = gen_calls env "gaa" in
  let a = gen_calls env "a" in
  register env a;
  register env gab;
  register env gaa;
  merge env a gab;
  fun env ->
    assert_bool "a = g(a,b) => g(a,b) != g(a,a)" (not (is_equal env gab gaa))

let _2level _ =
  run @@ fun env ->
  let gab = gen_calls env "gab" in
  let ggabb = gen_calls env "ggabb" in
  let a = gen_calls env "a" in
  Colibri2_stdlib.Shuffle.seql
    [ (fun () -> register env a); (fun () -> register env gab) ];
  Colibri2_stdlib.Shuffle.seql
    [ (fun () -> merge env gab a); (fun () -> register env ggabb) ];
  fun env -> assert_bool "g(a,b) = a => g(g(a,b),b) = a" (is_equal env ggabb a)

let congru2 =
  "Uf.congru 2 args"
  >::: [ "refl" >:: refl; "congru" >:: congru; "2level" >:: _2level ]

let merge env x y =
  register env x;
  register env y;
  merge env x y

let altergo0 _ =
  run @@ fun env ->
  let gab = gen_calls env "gab" in
  let b = gen_calls env "b" in
  let a = gen_calls env "a" in
  let fb = gen_calls env "fb" in
  let ggafbb = gen_calls env "ggafbb" in
  Colibri2_stdlib.Shuffle.seql
    [ (fun () -> merge env fb b); (fun () -> merge env gab a) ];
  register env ggafbb;
  fun env ->
    assert_bool "f(b)=b and g(a,b)=a ->  g(g(a,f(b)),b)=a"
      (is_equal env ggafbb a)

let altergo1 _ =
  run @@ fun env ->
  let b = gen_calls env "b" in
  let a = gen_calls env "a" in
  let fffa = gen_calls env "fffa" in
  let fffffa = gen_calls env "fffffa" in
  let gbc = gen_calls env "gbc" in
  let hggbcca = gen_calls env "hggbcca" in
  let hbfa = gen_calls env "hbfa" in
  Colibri2_stdlib.Shuffle.seql
    [
      (fun () -> merge env fffa a);
      (fun () -> merge env fffffa a);
      (fun () -> merge env gbc b);
    ];
  register env hggbcca;
  register env hbfa;
  fun env ->
    assert_bool
      "f(f(f(a)))=a and f(f(f(f(f(a)))))=a and g(b,c)=b -> \
       h(g(g(b,c),c),a)=h(b,f(a))"
      (is_equal env hggbcca hbfa)

let altergo2 _ =
  run @@ fun env ->
  let b = gen_calls env "b" in
  let a = gen_calls env "a" in
  let gbc = gen_calls env "gbc" in
  let fa = gen_calls env "fa" in
  let hggbcca = gen_calls env "hggbcca" in
  let hbfa = gen_calls env "hbfa" in
  Colibri2_stdlib.Shuffle.seql
    [ (fun () -> merge env fa a); (fun () -> merge env gbc b) ];
  register env hggbcca;
  register env hbfa;
  fun env ->
    assert_bool "f(a) = a -> g(b,c)=b -> h(g(g(b,c),c),a)=h(b,f(a))"
      (is_equal env hggbcca hbfa)

let altergo = "Uf.altergo tests" &: [ altergo0; altergo1; altergo2 ]
let tests = OUnit2.test_list [ basic; congru1; congru2; altergo ]
