(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open OUnit2
open Colibri2_stdlib
open Colibri2_core
open Tests_lib
open Colibri2_theories_bool

let theories =
  [ Colibri2_theories_quantifiers.Uninterp.th_register; Boolean.th_register ]

let ( $$ ) f x = f x
let run f = Tests_lib.run_exn ~theories f
let for_tests f = Tests_lib.for_tests_exn ~nodec:() ~theories f

open Expr

let solve0a _ =
  for_tests
  @@ `Add_assertion
       (fun env ->
         let a = fresh env Ty.real "ab" in
         let b = fresh env Ty.real "bb" in
         let _1 = Boolean.set_true in
         let a1 = Boolean._and env [ a; b ] in
         register env a1;
         `Add_assertion
           (fun env ->
             Colibri2_core.ForSchedulers.flush_internal env;
             Boolean.set_true env a1;
             `Check
               (fun env -> assert_bool "a or b => a" (Boolean.is_true env a))))

let true_is_true _ =
  run (fun _ env ->
      assert_bool "" (Boolean.is_true env Boolean._true);
      assert_bool "" (not (Boolean.is_false env Boolean._true)))

let not_true_is_false _ =
  run (fun env ->
      let not_true = Boolean._not env Boolean._true in
      Egraph.register env not_true;
      fun env ->
        assert_bool "" (Boolean.is_false env not_true);
        assert_bool "" (not (Boolean.is_true env not_true)))

let and_true_is_true _ =
  run (fun env ->
      let _t = Boolean._true in
      let _and = Boolean._and env [ _t; _t; _t ] in
      Egraph.register env _and;
      fun env ->
        assert_bool "" (Boolean.is_true env _and);
        assert_bool "" (not (Boolean.is_false env _and)))

let or_not_true_is_false _ =
  run (fun env ->
      let _f = Boolean._not env Boolean._true in
      let _or = Boolean._and env [ _f; _f; _f ] in
      Egraph.register env _or;
      fun env ->
        assert_bool "" (Boolean.is_false env _or);
        assert_bool "" (not (Boolean.is_true env _or)))

let merge_true _ =
  run @@ fun env ->
  let a = fresh env Ty.bool "a" in
  let b = fresh env Ty.bool "b" in
  let c = fresh env Ty.bool "c" in
  let d = fresh env Ty.bool "d" in
  let _and = Boolean._and env [ a; b; c ] in

  Egraph.register env _and;
  List.iter (Egraph.register env) [ a; b; c; d ];
  Shuffle.seql [ (fun () -> merge env a b); (fun () -> merge env a c) ];
  merge env a d;
  Boolean.set_true env d;
  fun env -> assert_bool "" (Boolean.is_true env _and)

let imply_implies _ =
  run @@ fun env ->
  let a = fresht Ty.bool "a" in
  let b = fresht Ty.bool "b" in
  let t = Term.imply a b in
  let an = Ground.convert env a in
  let bn = Ground.convert env b in
  let tn = Ground.convert env t in
  Egraph.register env tn;
  Boolean.set_true env tn;
  Egraph.register env an;
  Boolean.set_true env an;
  fun env -> assert_bool "" (Boolean.is_true env bn)

let basic =
  "Boolean.Basic"
  >::: [
         "solve0a" >:: solve0a;
         "true_is_true" >:: true_is_true;
         "not_true_is_false" >:: not_true_is_false;
         "and_true_is_true" >:: and_true_is_true;
         "or_not_true_is_false" >:: or_not_true_is_false;
         "merge_true" >:: merge_true;
         "imply_implies" >:: imply_implies
         (* "modus_ponens"         >:: modus_ponens; *);
       ]

let tests = test_list [ basic ]
