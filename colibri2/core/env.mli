(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

(** Theory specific environment *)

module Saved: sig
  (** Environment should currently be persistent data-structure in order
      to be backtracked correctly *)

  include Keys.Key

  val register: 'a Format.printer -> 'a t -> 'a -> unit
  (** Only a pretty printer is needed for registration *)

  val print: 'a t -> 'a Format.printer
  (** Get a pretty printer for a particular environment *)

  val is_well_initialized: unit -> bool
  (** Check if all the keys created have been registered *)

  val init: 'a t -> 'a
  (** Get the initial value *)
  
end

module Unsaved: sig
  (** These environment are not saved automatically *)

  include Keys.Key

  val register: init:(Context.creator -> 'a) -> pp:'a Format.printer -> 'a t -> unit
  (** Only a pretty printer and an initialization function is needed for registration *)

  val print: 'a t -> 'a Format.printer
  (** Get a pretty printer for a particular environment *)

  val is_well_initialized: unit -> bool
  (** Check if all the keys created have been registered *)

  val init: Context.creator -> 'a t -> 'a
  (** Create the initial value *)
end
