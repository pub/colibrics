(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

(** Dem *)

module Dem = Keys.Make_key (struct end)

type delay =
  | Immediate
  | Delayed_by of int
  | LastEffort of int
  | LastEffortUncontextual of int
  | FixingModel
  | Delayed_by_and_FixingModel of int

type enqueue =
  | EnqRun : 'r Dem.t * 'r * Nodes.ThTerm.t option -> enqueue
      (** Schedule a daemon run *)
  | EnqLast : 'r Dem.t * 'r * Nodes.ThTerm.t option -> enqueue
      (** Same as EnqRun but remove the waiting event *)
  | EnqAlready : enqueue  (** Don't run but keep the waiting event *)
  | EnqStopped : enqueue  (** Stop and don't run *)

module Wait = struct
  type daemon_key =
    | DaemonKey :
        'runable Dem.t * 'runable * Nodes.ThTerm.t option
        -> daemon_key

  module type S = sig
    type delayed

    module type Dem = sig
      type runable

      val print_runable : runable Format.printer
      val run : delayed -> runable -> unit
      val key : runable Dem.t
      val delay : delay
    end

    module type WithDatatype = sig
      type runable [@@deriving eq, hash]
    end

    val register_dem :
      ?datatype:(module WithDatatype with type runable = 'd) ->
      (module Dem with type runable = 'd) ->
      unit

    val get_dem : 'd Dem.t -> (module Dem with type runable = 'd)

    val get_dem_datatype :
      'd Dem.t -> (module WithDatatype with type runable = 'd) option

    val get_priority : daemon_key -> delay
    val print_dem_runable : 'b Dem.t -> 'b Format.printer
    val is_well_initialized : unit -> bool
  end

  module Make (S : sig
    type delayed
  end) : S with type delayed = S.delayed = struct
    type delayed = S.delayed

    module type Dem = sig
      type runable

      val print_runable : runable Format.printer
      val run : delayed -> runable -> unit
      val key : runable Dem.t
      val delay : delay
    end

    module type WithDatatype = sig
      type runable [@@deriving eq, hash]
    end

    type 'd data = {
      exec : (module Dem with type runable = 'd);
      datatype : (module WithDatatype with type runable = 'd) option;
    }

    module Registry = Dem.Make_Registry (struct
      type nonrec 'd data = 'd data

      let pp (type d) (dem : d data) =
        let module Dem = (val dem.exec) in
        Dem.print_runable

      let key (type d) (dem : d data) =
        let module Dem = (val dem.exec) in
        Dem.key
    end)

    let register_dem ?datatype exec = Registry.register { exec; datatype }
    let get_dem k = (Registry.get k).exec
    let get_dem_datatype k = (Registry.get k).datatype

    let get_priority = function
      | DaemonKey (dem, _, _) ->
          let module Dem = (val get_dem dem) in
          Dem.delay

    let print_dem_runable = Registry.print
    let is_well_initialized = Registry.is_well_initialized
  end
end
