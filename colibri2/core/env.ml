(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

module Saved = struct
  module Env = Keys.Make_key(struct end)

  include Env

  type 'a data = {key: 'a Env.t; pp: 'a Format.printer; init : 'a}

  module VEnv = Env.Make_Registry(struct
      type nonrec 'a data = 'a data
      let pp d = d.pp
      let key d = d.key
    end)

  let register pp key init = VEnv.register {key;pp;init}
  let print = VEnv.print
  let is_well_initialized = VEnv.is_well_initialized
  let init k = (VEnv.get k).init

end

module Unsaved = struct
  module Env = Keys.Make_key(struct end)

  include Env

  type 'a data = {key: 'a Env.t; pp: 'a Format.printer;
                  init : (Context.creator -> 'a)}

  module VEnv = Env.Make_Registry(struct
      type nonrec 'a data = 'a data
      let pp (d:_ data) = d.pp
      let key (d:_ data) = d.key
    end)

  let register ~init ~pp key = VEnv.register {key;pp;init}
  let print = VEnv.print
  let is_well_initialized = VEnv.is_well_initialized
  let init creator k = (VEnv.get k).init creator

end
