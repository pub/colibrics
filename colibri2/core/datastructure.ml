(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

module type Sig = sig
  type 'a t
  type key

  val create : 'a Format.printer -> string -> 'a t
  val remove : 'a t -> _ Egraph.t -> key -> unit
  val set : 'a t -> _ Egraph.t -> key -> 'a -> unit
  val find : 'a t -> _ Egraph.t -> key -> 'a
  val find_opt : 'a t -> _ Egraph.t -> key -> 'a option
  val find_exn : 'a t -> _ Egraph.t -> exn -> key -> 'a
  val mem : 'a t -> _ Egraph.t -> key -> bool
  val change : ('a option -> 'a option) -> 'a t -> _ Egraph.t -> key -> unit
  val iter : f:(key -> 'a -> unit) -> 'a t -> _ Egraph.t -> unit
  val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> _ Egraph.t -> 'b -> 'b

  val filter_map_inplace :
    (key -> 'a -> 'a option) -> 'a t -> _ Egraph.t -> unit

  val clear : 'a t -> _ Egraph.t -> unit
  val length : 'a t -> _ Egraph.t -> int
end

module Hashtbl (S : Colibri2_popop_lib.Popop_stdlib.Datatype) :
  Sig with type key := S.t = struct
  module H = Context.Hashtbl (S)

  type 'a t = 'a H.t Env.Unsaved.t

  let create : type a. a Colibri2_popop_lib.Pp.pp -> _ -> a t =
   fun pp name ->
    let module M = struct
      type t = a H.t

      let name = name
    end in
    let key = Env.Unsaved.create (module M) in
    Env.Unsaved.register ~init:H.create ~pp:(H.pp pp) key;
    key

  let find t d k = H.find (Egraph.get_unsaved_env d t) k
  let find_opt t d k = H.find_opt (Egraph.get_unsaved_env d t) k
  let find_exn t d exn k = H.find_exn (Egraph.get_unsaved_env d t) exn k
  let mem t d k = H.mem (Egraph.get_unsaved_env d t) k
  let set t d k v = H.set (Egraph.get_unsaved_env d t) k v
  let remove t d k = H.remove (Egraph.get_unsaved_env d t) k
  let change f t d k = H.change f (Egraph.get_unsaved_env d t) k
  let iter ~f t d = H.iter ~f (Egraph.get_unsaved_env d t)

  let fold (f : S.t -> 'a -> 'b -> 'b) (t : 'a t) (d : _ Egraph.t) (acc : 'b) =
    H.fold (fun acc k v -> f k v acc) acc (Egraph.get_unsaved_env d t)

  let filter_map_inplace (f : S.t -> 'a -> 'a option) (t : 'a t)
      (d : _ Egraph.t) =
    H.filter_map_inplace f (Egraph.get_unsaved_env d t)

  let clear t d = H.clear (Egraph.get_unsaved_env d t)
  let length t d = H.length (Egraph.get_unsaved_env d t)
end

module type Sig2 = sig
  type 'a t
  type key

  val create : 'a Format.printer -> string -> (Context.creator -> 'a) -> 'a t
  val set : 'a t -> _ Egraph.t -> key -> 'a -> unit
  val find : 'a t -> _ Egraph.t -> key -> 'a
  val change : ('a -> 'a) -> 'a t -> _ Egraph.t -> key -> unit
end

module Hashtbl2 (S : Colibri2_popop_lib.Popop_stdlib.Datatype) :
  Sig2 with type key := S.t = struct
  module H = Context.HashtblWithDefault (S)

  type 'a t = 'a H.t Env.Unsaved.t

  let create : type a.
      a Colibri2_popop_lib.Pp.pp -> _ -> (Context.creator -> a) -> a t =
   fun pp name def ->
    let module M = struct
      type t = a H.t

      let name = name
    end in
    let key = Env.Unsaved.create (module M) in
    Env.Unsaved.register ~init:(fun c -> H.create c def) ~pp:(H.pp pp) key;
    key

  let find t d k = H.find (Egraph.get_unsaved_env d t) k
  let set t d k v = H.set (Egraph.get_unsaved_env d t) k v
  let change f t d k = H.change f (Egraph.get_unsaved_env d t) k
end

module type Memo = sig
  type 'a t
  type key

  val create :
    'a Format.printer -> string -> (Context.creator -> key -> 'a) -> 'a t

  val find : 'a t -> _ Egraph.t -> key -> 'a
  val iter : (key -> 'a -> unit) -> 'a t -> _ Egraph.t -> unit
  val fold : (key -> 'a -> 'acc -> 'acc) -> 'a t -> _ Egraph.t -> 'acc -> 'acc
  val length : 'a t -> _ Egraph.t -> int
end

module Memo (S : Colibri2_popop_lib.Popop_stdlib.Datatype) :
  Memo with type key := S.t = struct
  module H = Context.Memo (S)

  type 'a t = 'a H.t Env.Unsaved.t

  let create : type a.
      a Colibri2_popop_lib.Pp.pp -> _ -> (Context.creator -> S.t -> a) -> a t =
   fun pp name def ->
    let module M = struct
      type t = a H.t

      let name = name
    end in
    let key = Env.Unsaved.create (module M) in
    Env.Unsaved.register ~init:(fun c -> H.create c def) ~pp:(H.pp pp) key;
    key

  let find t d k = H.find (Egraph.get_unsaved_env d t) k
  let iter f t d = H.iter f (Egraph.get_unsaved_env d t)
  let fold f t d = H.fold f (Egraph.get_unsaved_env d t)
  let length t d = H.length (Egraph.get_unsaved_env d t)
end

module type Memo2 = sig
  type ('a, 'b) t
  type key

  val create :
    'a Format.printer -> string -> ('b Egraph.t -> key -> 'a) -> ('a, 'b) t

  val find : ('a, 'b) t -> 'b Egraph.t -> key -> 'a
  val iter : (key -> 'a -> unit) -> ('a, 'b) t -> 'b Egraph.t -> unit

  val fold :
    (key -> 'a -> 'acc -> 'acc) -> ('a, 'b) t -> 'b Egraph.t -> 'acc -> 'acc
end

module Memo2 (S : Colibri2_popop_lib.Popop_stdlib.Datatype) :
  Memo2 with type key := S.t = struct
  type ('a, 'b) t' = { h : 'a S.H.t; def : 'b Egraph.t -> S.t -> 'a }
  type ('a, 'b) t = ('a, 'b) t' Env.Unsaved.t

  let create : type a b.
      a Colibri2_popop_lib.Pp.pp -> _ -> (b Egraph.t -> S.t -> a) -> (a, b) t =
   fun pp name def ->
    let module M = struct
      type t = (a, b) t'

      let name = name
    end in
    let key = Env.Unsaved.create (module M) in
    let init _ = { h = S.H.create 5; def } in
    let iter f h = S.H.iter (fun k r -> f k r) h.h in
    let pp = Colibri2_popop_lib.Pp.(iter2 iter semi arrow S.pp pp) in
    Env.Unsaved.register ~init ~pp key;
    key

  let find t d k =
    let h = Egraph.get_unsaved_env d t in
    match S.H.find_opt h.h k with
    | Some r -> r
    | None ->
        let r = h.def d k in
        S.H.add h.h k r;
        r

  let iter f t d = S.H.iter f (Egraph.get_unsaved_env d t).h
  let fold f t d = S.H.fold f (Egraph.get_unsaved_env d t).h
end

module HNode = Hashtbl (Nodes.Node)

module Push = struct
  type 'a t = 'a Context.Push.t Env.Unsaved.t

  let create : type a. a Colibri2_popop_lib.Pp.pp -> _ -> a t =
   fun pp name ->
    let module M = struct
      type t = a Context.Push.t

      let name = name
    end in
    let key = Env.Unsaved.create (module M) in
    let init = Context.Push.create in
    let pp = Colibri2_popop_lib.Pp.(iter1 Context.Push.iter semi pp) in
    Env.Unsaved.register ~init ~pp key;
    key

  let push t d v = Context.Push.push (Egraph.get_unsaved_env d t) v
  let iter ~f t d = Context.Push.iter f (Egraph.get_unsaved_env d t)
  let fold ~f ~init t d = Context.Push.fold f init (Egraph.get_unsaved_env d t)
  let exists ~f t d = Context.Push.exists f (Egraph.get_unsaved_env d t)
  let length t d = Context.Push.length (Egraph.get_unsaved_env d t)
  let get t d i = Context.Push.get (Egraph.get_unsaved_env d t) i
end

module Ref = struct
  type 'a t = 'a Context.Ref.t Env.Unsaved.t

  let create : type a. a Colibri2_popop_lib.Pp.pp -> _ -> a -> a t =
   fun pp name v ->
    let module M = struct
      type t = a Context.Ref.t

      let name = name
    end in
    let key = Env.Unsaved.create (module M) in
    let init d = Context.Ref.create d v in
    let pp = Context.Ref.pp pp in
    Env.Unsaved.register ~init ~pp key;
    key

  let get t d = Context.Ref.get (Egraph.get_unsaved_env d t)
  let set t d v = Context.Ref.set (Egraph.get_unsaved_env d t) v
  let incr ?trace t d = Context.Ref.incr ?trace (Egraph.get_unsaved_env d t)
  let get_private t d = Context.Ref.get (Egraph.Private.get_unsaved_env d t)
end

module type Trie = sig
  type 'a t
  type key

  val create : 'a Format.printer -> string -> 'a t

  module List : sig
    val set : 'a t -> _ Egraph.t -> key list -> 'a -> unit

    val find_def :
      default:(Context.creator -> 'a) -> 'a t -> _ Egraph.t -> key list -> 'a
  end

  module Set : sig
    type set

    val set : 'a t -> _ Egraph.t -> set -> 'a -> unit

    val find_def :
      default:(Context.creator -> 'a) -> 'a t -> _ Egraph.t -> set -> 'a
  end
end

module Trie (S : Colibri2_popop_lib.Popop_stdlib.Datatype) :
  Trie with type key := S.t and type Set.set := S.S.t = struct
  module Trie = Context.Trie (S)

  type 'a t = 'a Trie.t Env.Unsaved.t

  let create : type a. a Colibri2_popop_lib.Pp.pp -> _ -> a t =
   fun pp name ->
    let module M = struct
      type t = a Trie.t

      let name = name
    end in
    let key = Env.Unsaved.create (module M) in
    let init d = Trie.create d in
    let pp = Trie.pp pp in
    Env.Unsaved.register ~init ~pp key;
    key

  module List = struct
    let set t d l v = Trie.List.set (Egraph.get_unsaved_env d t) l v

    let find_def ~default t d l =
      Trie.List.find_def ~default (Egraph.get_unsaved_env d t) l
  end

  module Set = struct
    let set t d l v = Trie.Set.set (Egraph.get_unsaved_env d t) l v

    let find_def ~default t d l =
      Trie.Set.find_def ~default (Egraph.get_unsaved_env d t) l
  end
end
