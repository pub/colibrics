(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

(** Egraph is the main module of core *)

(** The solver contains all the information. It keeps track of
    equivalence classes, values. It take care to schedule event that
    happened. *)

open Nodes

exception NotRegistered

exception UninitializedEnv of string

exception Contradiction

type ro
type rw

type +'a t

type wt = rw t

type rt = ro t

val attach_before_merge: _ t -> (wt -> repr:Node.t -> Node.t -> unit) -> unit
(** wakeup when a merge will be done between registered node *)

val attach_after_merge: _ t -> (wt -> repr:Node.t -> Node.t -> unit) -> unit
(** wakeup when a merge has just be done between registered node *)

val attach_dom: _ t -> ?direct:bool -> Node.t -> 'a DomKind.t -> (ro t -> Node.t -> Events.enqueue) -> unit
(** wakeup when the dom of the node change *)

val attach_any_dom: _ t -> 'a DomKind.t -> (ro t -> Node.t -> Events.enqueue) -> unit
(** wakeup when the dom of any node change *)

val attach_value: _ t -> ?direct:bool -> Node.t -> ('a,'b) ValueKind.t -> (ro t -> Node.t -> 'b -> Events.enqueue) -> unit
(** wakeup when a value is attached to this equivalence class *)

val attach_any_value: _ t -> ?direct:bool -> Node.t -> (ro t -> Node.t -> Value.t -> Events.enqueue) -> unit
(** wakeup when any kind of value is attached to this equivalence class *)

val attach_reg_any: _ t -> (ro t -> Node.t -> Events.enqueue) -> unit
(** wakeup when any node is registered *)

val attach_reg_node: _ t -> Node.t -> (ro t -> Node.t -> Events.enqueue) -> unit
(** wakeup when this node is registered *)

val attach_reg_sem: _ t -> ('a,'b) ThTermKind.t -> (ro t -> 'b -> Events.enqueue) -> unit
(** wakeup when a new semantical class is registered *)

val attach_reg_value: _ t -> ('a,'b) ValueKind.t -> (ro t -> 'b -> Events.enqueue) -> unit
(** wakeup when a new value is registered *)

val attach_repr: _ t -> Node.t -> (ro t -> Node.t -> Events.enqueue) -> unit
(** wakeup when it is not anymore the representative class *)

val attach_any_repr: _ t -> (ro t -> Node.t -> Events.enqueue) -> unit
(** wakeup when a node is not its representative class anymore *)

val is_equal : _ t -> Node.t -> Node.t -> bool

val find_def : _ t -> Node.t -> Node.t

val get_dom : _ t -> 'a DomKind.t -> Node.t -> 'a option
(** dom of the class *)

val get_value : _ t -> Node.t -> Value.t option
(** one of the value of the class *)

(** {4 The classes must have been registered} *)

val find : _ t -> Node.t -> Node.t

val is_repr : _ t -> Node.t -> bool

val is_registered : _ t -> Node.t -> bool

val get_env : _ t -> 'a Env.Saved.t -> 'a
(** Can raise UninitializedEnv *)

val set_env : _ t -> 'a Env.Saved.t -> 'a -> unit

val get_unsaved_env : _ t -> 'a Env.Unsaved.t -> 'a

val context : _ t -> Context.creator

val register : _ t -> Node.t -> unit
(** Add a new node to register *)

(** {3 Immediate modifications} *)
val set_dom  : wt -> 'a DomKind.t -> Node.t -> 'a -> unit
(** change the dom of the equivalence class *)

val unset_dom  : wt -> 'a DomKind.t -> Node.t -> unit
(** remove the dom of the equivalence class *)


(** {3 Delayed modifications} *)
val set_thterm  : wt -> Node.t -> ThTerm.t -> unit
(** attach a theory term to an equivalence class *)

val set_value: wt -> Node.t -> Value.t -> unit
(** attach value to an equivalence class *)

val merge    : wt -> Node.t -> Node.t -> unit

type choice_state =
  | DecNo
  | DecTodo of (wt -> unit) list

and choice = {
  choice: wt -> choice_state;
  prio: int;
  print_cho: unit Fmt.t;
  key: Node.t option;
}

val register_decision: _ t -> choice -> unit
(** register a decision that would be scheduled later. The
    [choose_decision] of the [Cho] will be called at that time to know
    if the decision is still needed. *)

val new_pending_daemon: _ t -> ?thterm:Nodes.ThTerm.t -> 'a Events.Dem.t -> 'a -> unit
(** register an event for later (immediate or not). *)

val contradiction: unit -> 'b

(** {3 Low level} *)
val flush_internal: wt -> unit
(** Apply all the modifications and direct consequences.
    Should be used only during wakeup of not immediate daemon
*)

module Wait : Events.Wait.S with type delayed = wt


(** {2 Domains and Semantic Values key creation} *)

module type Dom = DomKind.Dom_partial with type delayed := wt and
                                           type delayed_ro := rt

val register_dom : (module Dom with type t = 'a) -> unit

val check_initialization: unit -> bool
(** Check if the initialization of all the dom, sem and dem have been done *)

val print_dom: 'a DomKind.t -> 'a Format.printer
val print_dom_opt: 'a DomKind.t -> 'a option Format.printer
val register_node_info_printer: 'a t -> (ro t -> Node.t -> (string * string) list) -> unit

(** {2 External use of the solver} *)
module Backtrackable: sig
  type delayed = wt

  type t

  val is_equal  : t -> Node.t -> Node.t -> bool
  val find_def  : t -> Node.t -> Node.t
  val get_dom   : t -> 'a DomKind.t -> Node.t -> 'a option
  (** dom of the class *)
  val get_value : t -> Node.t -> Value.t option
  (** one of the value of the class *)

  (** {4 The classes must have been registered} *)

  val find      : t -> Node.t -> Node.t
  val is_repr   : t -> Node.t -> bool

  val is_registered : t -> Node.t -> bool

  val get_env : t -> 'a Env.Saved.t -> 'a
  (** Can raise UninitializedEnv *)
  val set_env : t -> 'a Env.Saved.t -> 'a -> unit

  val get_unsaved_env : t -> 'a Env.Unsaved.t -> 'a

  val context : t -> Context.creator

  val new_t    :
    Context.creator -> t

  val set_sched:
    sched_daemon:(Events.Wait.daemon_key -> unit) ->
    sched_decision:(choice -> unit) ->
    t -> unit

  val get_delayed:
    t -> delayed

  val run_daemon: delayed -> Events.Wait.daemon_key -> unit
  (** schedule the run of a deamon *)

  val has_nothing_todo: t -> bool
  (** Return if there is nothing todo. *)

  val delayed_stop: t -> unit
  (** Check that not work need to be done. *)

  val flush: t -> unit
  (** Apply all the modifications and direct consequences. *)

  (** Debug *)
  val draw_graph: ?force:bool -> t -> unit
  val output_graph : string -> t -> unit

  val get_unknown: t -> bool

end

val print_decision: Colibri2_stdlib.Debug.flag
val print_contradiction: Colibri2_stdlib.Debug.flag

type theory = wt -> unit

val add_default_theory: theory -> unit
val default_theories: unit -> theory list

val set_unknown: wt -> bool -> unit

val ro: _ t -> ro t

val pp: _ t Fmt.t

module Private: sig
  type t
  val get_unsaved_env : t -> 'a Env.Unsaved.t -> 'a
  val get_dotgui_only: (t -> string list) ref
  (** forward ref for Options *)
  val get_multiple_merging_round: (wt -> bool) ref
  (** forward ref for Options *)

end