(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Base
open Colibri2_popop_lib
open Nodes

module Seq = struct
  type mapping = Value.t Node.M.t
  type 'a t = mapping -> ('a * mapping) Sequence.t

  let ( let+ ) (t : 'a t) (f : 'a -> 'b) m =
    Sequence.( >>| ) (t m) (fun (x, m) -> (f x, m))

  let ( let* ) (t : 'a t) (f : 'a -> 'b t) m =
    Sequence.bind (t m) ~f:(fun (x, mx) -> f x mx)

  let ( and* ) (x : 'a t) (y : 'b t) : ('a * 'b) t =
   fun m ->
    Sequence.map (x m) ~f:(fun (x, m) ->
        Sequence.map (y m) ~f:(fun (y, m) -> ((x, y), m)))
    |> Sequence.interleave

  let concat (t : 'a t t) : 'a t =
   fun m -> Sequence.map (t m) ~f:(fun (s, m) -> s m) |> Sequence.interleave

  let of_seq (t : 'a Sequence.t) : 'a t =
   fun _ -> Sequence.map t ~f:(fun x -> (x, Node.M.empty))

  let return (x : 'a) : 'a t = fun _ -> Sequence.return (x, Node.M.empty)
end

let debug =
  Debug.register_info_flag ~desc:"for the scheduler in the simple version"
    "interp"

let debug_all =
  Debug.register_flag ~desc:"for the scheduler in the simple version"
    "interp.all"

type during = {
  limitreached : bool ref;  (** it is non backtrable variable *)
  limit : int;
  nextnode : Node.t Sequence.t; [@opaque]
}
[@@deriving show]

type env = Before  (** fixing model *) | During of during [@@deriving show]
type check = Right | Wrong | NA | Unknown
type compute = Value of Nodes.Value.t | NA

exception CantInterpretTy of Ground.Ty.t
exception CantInterpretNode of Node.t

exception
  CantCheckGround of
    [ `Ground of Ground.t
    | `ClosedQuantifier of Ground.ClosedQuantifier.t
    | `NotTotallyApplied of Ground.NotTotallyApplied.t ]

exception
  CantComputeGround of
    [ `Ground of Ground.t
    | `ClosedQuantifier of Ground.ClosedQuantifier.t
    | `NotTotallyApplied of Ground.NotTotallyApplied.t ]

exception
  GroundTermWithoutValueAfterModelFixing of
    [ `Ground of Ground.t
    | `ClosedQuantifier of Ground.ClosedQuantifier.t
    | `NotTotallyApplied of Ground.NotTotallyApplied.t ]

exception ArgOfGroundTermWithoutValueAfterModelFixing of Ground.t * Node.t

module Data = struct
  let check = Datastructure.Push.create Fmt.nop "Interp.check"

  let check_closed_quantifier =
    Datastructure.Push.create Fmt.nop "Interp.check_closed_quantifier"

  let check_not_totally_applied =
    Datastructure.Push.create Fmt.nop "Interp.check_not_totally_applied"

  let compute = Datastructure.Push.create Fmt.nop "Interp.compute"

  let compute_closed_quantifier =
    Datastructure.Push.create Fmt.nop "Interp.compute_closed_quantifier"

  let compute_not_totally_applied =
    Datastructure.Push.create Fmt.nop "Interp.compute_not_totally_applied"

  let node : (_ -> _ -> _ -> Value.t Seq.t option) Datastructure.Push.t =
    Datastructure.Push.create Fmt.nop "Interp.node"

  let ty : (_ -> _ -> Value.t Sequence.t option) Datastructure.Push.t =
    Datastructure.Push.create Fmt.nop "Interp.ty"

  let reg_ground = Datastructure.Push.create Fmt.nop "Interp.reg_ground"

  let reg_closed_quantifiers =
    Datastructure.Push.create Fmt.nop "Interp.reg_closed_quantifiers"

  let reg_not_totally_applied =
    Datastructure.Push.create Fmt.nop "Interp.reg_not_totally_applied"

  let reg_dependency_by_ground_term =
    Datastructure.Push.create Fmt.nop "Interp.reg_dependency_by_ground_term"

  (* let reg_node = Datastructure.Push.create Fmt.nop "Interp.reg_node" *)

  module PrintValue = Nodes.ValueKind.MkHashtbl (struct
    type (_, 'a, _) t = Egraph.rt -> Ground.Ty.t -> 'a Fmt.t
  end)

  let print_value = PrintValue.create 10

  let env =
    Env.Saved.create
      (module struct
        type t = env

        let name = "Interp.env"
      end)

  let () = Env.Saved.register pp_env env Before
end

let check_of_bool = function true -> Right | false -> Wrong

module Register = struct
  open Data

  let check d f = Datastructure.Push.push check d f

  let check_closed_quantifier d f =
    Datastructure.Push.push check_closed_quantifier d f

  let check_not_totally_applied d f =
    Datastructure.Push.push check_not_totally_applied d f

  let compute d f = Datastructure.Push.push compute d f

  let compute_closed_quantifier d f =
    Datastructure.Push.push compute_closed_quantifier d f

  let compute_not_totally_applied d f =
    Datastructure.Push.push compute_not_totally_applied d f

  let dependency_by_ground_term d f =
    Datastructure.Push.push reg_dependency_by_ground_term d f

  let node d f = Datastructure.Push.push node d f
  let ty d f = Datastructure.Push.push ty d f
  let print_value_smt s = Data.PrintValue.set Data.print_value s
end

let print_value_smt d ty fmt v =
  let d = Egraph.ro d in
  match Nodes.Value.kind v with
  | Value (k, v) ->
      if Data.PrintValue.is_uninitialized Data.print_value k then
        let module M = (val Nodes.get_value k) in
        let s = M.value v in
        Fmt.pf fmt "(todo %a)" M.SD.pp s
      else
        let pr = Data.PrintValue.get Data.print_value k in
        pr d ty fmt v

let spy_sequence pp msg s =
  Sequence.mapi
    ~f:(fun i x ->
      Debug.dprintf4 debug "%s %i : %a" msg i pp x;
      x)
    s

let spy_sequence_if_debug pp msg s =
  if Debug.test_flag debug_all then spy_sequence pp msg s else s

let get_registered (type a) exn db call d x =
  let exception Found of a in
  match
    Datastructure.Push.iter
      ~f:(fun f -> Option.iter (call f d x) ~f:(fun s -> raise (Found s)))
      db d
  with
  | exception Found s -> s
  | () -> exn x
[@@inline always]

let ty d ty =
  let seq =
    get_registered
      (fun ty -> raise (CantInterpretTy ty))
      Data.ty
      (fun f d -> f (Egraph.ro d))
      d ty
  in
  spy_sequence_if_debug Value.pp "ty" seq

let node d n : Value.t Seq.t =
  let parent = Node.H.create 5 in
  let rec aux d n' : Value.t Seq.t =
    assert (Egraph.is_registered d n');
    let n' = Egraph.find_def d n' in
    if Node.H.mem parent n' then (
      Debug.dprintf6 debug "Interp.node recursive value: %a starting at %a (%a)"
        Node.pp n' Node.pp n
        Fmt.(iter_bindings ~sep:comma Node.H.iter (pair Node.pp nop))
        parent;
      raise Impossible)
    else
      match Egraph.get_value d n' with
      | Some v -> fun m -> Sequence.singleton (v, m)
      | None -> (
          fun m ->
            match Node.M.find_opt n' m with
            | Some v -> Sequence.singleton (v, m)
            | None ->
                Node.H.replace parent n' ();
                let r =
                  try
                    get_registered
                      (fun n' -> raise (CantInterpretNode n'))
                      Data.node
                      (fun f d n -> f aux d n)
                      d n' m
                  with CantInterpretNode _ as exn ->
                    let s =
                      if Ground.Ty.S.is_empty (Ground.tys d n') then
                        match Nodes.Only_for_solver.thterm n' with
                        | None ->
                            Fmt.epr "@.%a is not a thterm@." Node.pp n';
                            raise exn
                        | Some th -> ty (Egraph.ro d) (ThTerm.ty th)
                      else
                        ty (Egraph.ro d) (Ground.Ty.S.choose (Ground.tys d n'))
                    in
                    Sequence.map s ~f:(fun x -> (x, Node.M.empty))
                in
                Node.H.remove parent n';
                Sequence.map r ~f:(fun (x, m) ->
                    assert (not (Node.M.mem n' m));
                    (x, Node.M.add n' x m)))
  in
  aux d n

let check db d n =
  let unknown = ref false in
  let exception Found of check in
  match
    Datastructure.Push.iter
      ~f:(fun f ->
        match (f d n : check) with
        | NA -> ()
        | Unknown -> unknown := true
        | r -> raise (Found r))
      db d
  with
  | exception Found s -> s
  | () -> if !unknown then Unknown else NA

(** return if it is unknown *)
let compute db d n =
  let exception Found of Value.t in
  match
    Datastructure.Push.iter
      ~f:(fun f ->
        match (f d n : compute) with NA -> () | Value r -> raise (Found r))
      db d
  with
  | exception Found s -> Value s
  | () -> NA

(** Check that registered arguments have a value. It is the responsability of
    the theory to ensure needed terms are registered *)
let check_args d g =
  IArray.iter (Ground.sem g).args ~f:(fun n ->
      if Option.is_none (Egraph.get_value d n) && Egraph.is_registered d n then
        raise (ArgOfGroundTermWithoutValueAfterModelFixing (g, n)))

let check_ground_terms d =
  Datastructure.Push.fold Data.reg_ground d ~init:`Right ~f:(fun acc g ->
      if Option.is_none (Egraph.get_value d (Ground.node g)) then
        raise (GroundTermWithoutValueAfterModelFixing (`Ground g));
      check_args d g;
      match check Data.check d g with
      | Right -> acc
      | Unknown -> `Unknown
      | NA -> raise (CantCheckGround (`Ground g))
      | Wrong ->
          Debug.dprintf2 Egraph.print_contradiction
            "Interp: checking of %a failed" Ground.pp g;
          Egraph.contradiction ())

let check_closed_quantifier d =
  Datastructure.Push.fold Data.reg_closed_quantifiers d ~init:`Right
    ~f:(fun acc g ->
      if Option.is_none (Egraph.get_value d (Ground.ClosedQuantifier.node g))
      then raise (GroundTermWithoutValueAfterModelFixing (`ClosedQuantifier g));
      match check Data.check_closed_quantifier d g with
      | Right -> acc
      | Unknown -> `Unknown
      | NA -> raise (CantCheckGround (`ClosedQuantifier g))
      | Wrong ->
          Debug.dprintf2 Egraph.print_contradiction
            "Interp: checking of %a failed" Ground.ClosedQuantifier.pp g;
          Egraph.contradiction ())

let check_not_totally_applied d =
  Datastructure.Push.fold Data.reg_not_totally_applied d ~init:`Right
    ~f:(fun acc g ->
      if Option.is_none (Egraph.get_value d (Ground.NotTotallyApplied.node g))
      then raise (GroundTermWithoutValueAfterModelFixing (`NotTotallyApplied g));
      match check Data.check_not_totally_applied d g with
      | Right -> acc
      | Unknown -> `Unknown
      | NA -> raise (CantCheckGround (`NotTotallyApplied g))
      | Wrong ->
          Debug.dprintf2 Egraph.print_contradiction
            "Interp: checking of %a failed" Ground.NotTotallyApplied.pp g;
          Egraph.contradiction ())

(** interp *)
let interp d e =
  let n =
    Ground.convert_and_iter
      (fun d g ->
        check_args d g;
        let n = Ground.node g in
        Egraph.register d n;
        if Option.is_none (Egraph.get_value d n) then (
          let v =
            match compute Data.compute (Egraph.ro d) g with
            | NA -> raise (CantComputeGround (`Ground g))
            | Value v -> v
          in
          Egraph.register d n;
          Egraph.set_value d n v;
          Egraph.flush_internal d))
      (fun d g ->
        let n = Ground.ClosedQuantifier.node g in
        Egraph.register d n;
        if Option.is_none (Egraph.get_value d n) then (
          let v =
            match compute Data.compute_closed_quantifier (Egraph.ro d) g with
            | NA -> raise (CantComputeGround (`ClosedQuantifier g))
            | Value v -> v
          in
          Egraph.register d n;
          Egraph.set_value d n v;
          Egraph.flush_internal d))
      (fun d g ->
        let n = Ground.NotTotallyApplied.node g in
        Egraph.register d n;
        if Option.is_none (Egraph.get_value d n) then (
          let v =
            match compute Data.compute_not_totally_applied (Egraph.ro d) g with
            | NA -> raise (CantComputeGround (`NotTotallyApplied g))
            | Value v -> v
          in
          Egraph.register d n;
          Egraph.set_value d n v;
          Egraph.flush_internal d))
      d Ground.Subst.empty e
  in
  assert (Option.is_some (Egraph.get_value d n));
  Base.Option.value_exn (Egraph.get_value d n)

module WTO = Colibri2_stdlib.Wto.Make (Node)

let rec component_to_seq p =
  let open Sequence.Generator in
  match p with
  | Wto.Component (n, part) -> yield n >>= fun () -> partition_to_seq part
  | Wto.Node n -> yield n

and partition_to_seq' l =
  let open Sequence.Generator in
  match l with
  | [] -> return ()
  | a :: l -> component_to_seq a >>= fun () -> partition_to_seq' l

and partition_to_seq l = partition_to_seq' (List.rev l)

(** Fix the order of the fixing of the model.

    Unregistered nodes are not fixed. *)
let compute_ordering_of_model_choice d =
  let open Base in
  let n2g_initial =
    let h = Hashtbl.create (module Node) in
    Ground.iter (fun g -> Hashtbl.add_exn h ~key:(Ground.node g) ~data:g);
    h
  in
  let n2g = Hashtbl.create (module Node) in
  let n2n = Hashtbl.create (module Node) in
  let nDone = Hash_set.create (module Node) in
  let inits = Queue.create () in
  let rec push_node n =
    if Egraph.is_registered d n then (
      if not (Hash_set.mem nDone n) then (
        Hash_set.add nDone n;
        match Hashtbl.find n2g_initial n with
        | None -> push_possible_new_repr (Egraph.find_def d n)
        | Some g -> push_ground g))
    else
      Debug.dprintf2 debug
        "Fix model reached the unregistered node %a, ignoring" Node.pp n
  and push_possible_new_repr n =
    if not (Hashtbl.mem n2g n) then (
      Egraph.register d n;
      if Option.is_none (Egraph.get_value d n) then Queue.enqueue inits n;
      Hashtbl.set n2g ~key:n ~data:[])
  and push_ground g =
    Hash_set.add nDone (Ground.node g);
    let n = Egraph.find_def d (Ground.node g) in
    push_possible_new_repr n;
    add_edges g n;
    IArray.iter ~f:push_node (Ground.sem g).args
  and add_edges g n =
    get_registered
      (fun g -> Hashtbl.add_multi n2g ~key:n ~data:g)
      Data.reg_dependency_by_ground_term
      (fun f d g ->
        Option.map (f d g) ~f:(fun l ->
            List.iter l ~f:(fun (a, b) ->
                let a = Egraph.find_def d a in
                let b = Egraph.find_def d b in
                Hashtbl.add_multi n2n ~key:b ~data:a)))
      (Egraph.ro d) g
  in
  Datastructure.Push.iter Data.reg_ground d ~f:push_ground;
  let partition =
    WTO.partition ?pref:None ~inits:(Queue.to_list inits) (fun n ->
        assert (Egraph.is_registered d n);
        if Option.is_some (Egraph.get_value d n) then Sequence.empty
        else
          let ns = Hashtbl.find_multi n2n n in
          Sequence.append (Sequence.of_list ns)
          @@
          let gs = Hashtbl.find_multi n2g n in
          Sequence.concat_map (Sequence.of_list gs) ~f:(fun g ->
              (Ground.sem g).args
              |> IArray.map ~f:(Egraph.find_def d)
              |> IArray.to_seq
              |> Sequence.filter ~f:(Egraph.is_registered d)))
  in
  Debug.dprintf2 debug "FixModel wto:%a" (Wto.pp_partition Node.pp) partition;
  Sequence.Generator.run (partition_to_seq partition)

module Fix_model = struct
  let init d =
    let nextnode = compute_ordering_of_model_choice d in
    let env =
      {
        limitreached =
          (* true because we want to continue (i.e start) *)
          ref true;
        nextnode;
        limit = 1;
      }
    in
    Debug.dprintf0 debug "FixModel with ground terms";
    Egraph.set_env d Data.env (During env);
    env

  let limit_sequence d s =
    Sequence.unfold_with s ~init:0 ~f:(fun i x ->
        if i <= d.limit then Sequence.Step.Yield { value = x; state = i + 1 }
        else (
          d.limitreached := true;
          Sequence.Step.Done))

  let next_dec (d : Egraph.wt) =
    match Egraph.get_env d Data.env with
    | Before ->
        (* The first branching is about the limit *)
        let r = init d in
        Sequence.unfold ~init:1 ~f:(fun i ->
            if !(r.limitreached) then (
              r.limitreached := false;
              if i > Base.Int.shift_left 1 25 then raise Impossible;
              let limit = i * 2 in
              Debug.dprintf1 debug "FixModel with limit at %i" limit;
              let dec d = Egraph.set_env d Data.env (During { r with limit }) in
              Some (dec, limit))
            else None)
    | During r ->
        let rec aux nextnode =
          match Sequence.next nextnode with
          | None ->
              Debug.dprintf0 debug "FixModel check_ground terms";
              let unknown1 = check_ground_terms (Egraph.ro d) in
              let unknown2 = check_closed_quantifier (Egraph.ro d) in
              let unknown3 = check_not_totally_applied (Egraph.ro d) in
              let unknown =
                match (unknown1, unknown2, unknown3) with
                | `Right, `Right, `Right -> false
                | _ -> true
              in
              Debug.dprintf1 debug "unknown:%b" unknown;
              Egraph.set_unknown d unknown;
              Egraph.set_env d Data.env Before;
              Sequence.empty
          | Some (n, nextnode) -> (
              (* The others are about the values *)
              Debug.dprintf2 debug "FixModel look at %a" Node.pp n;
              match Egraph.get_value d n with
              | Some _ -> aux nextnode
              | None ->
                  Debug.dprintf2 debug "FixModel %a has no value" Node.pp n;
                  let seq = node d n Node.M.empty in
                  let seq = limit_sequence r seq in
                  let seq = Sequence.map seq ~f:snd in
                  let pp = Node.M.pp Value.pp in
                  let seq =
                    seq |> spy_sequence_if_debug pp "got" |> fun s ->
                    s |> Sequence.to_list |> Sequence.of_list
                  in
                  if Sequence.is_empty seq then
                    (* Usually because the limit has been reached *)
                    Egraph.contradiction ()
                  else
                    let seq =
                      Sequence.map seq ~f:(fun m d ->
                          Egraph.set_env d Data.env (During { r with nextnode });
                          if false && Node.M.cardinal m > 1 then
                            Fmt.pr "FixModel by setting %a@." pp m;
                          Debug.dprintf2 debug "FixModel by setting %a" pp m;
                          Node.M.iter
                            (fun n v ->
                              assert (Egraph.is_registered d n);
                              Egraph.set_value d n v)
                            m)
                    in
                    seq)
        in
        aux r.nextnode
end

let pp_gen_ground fmt = function
  | `Ground g -> Fmt.pf fmt "ground term %a" Ground.Term.pp (Ground.sem g)
  | `ClosedQuantifier g ->
      Fmt.pf fmt "closed quantifier %a" Ground.ClosedQuantifier.pp g
  | `NotTotallyApplied g ->
      Fmt.pf fmt "not totally applied function %a" Ground.NotTotallyApplied.pp g

let pp_ty_gen fmt = function
  | `Ground g -> Ground.Ty.pp fmt (Ground.sem g).ty
  | `ClosedQuantifier _ -> Ground.Ty.pp fmt Ground.Ty.bool
  | `NotTotallyApplied t -> Ground.Ty.pp fmt (Ground.NotTotallyApplied.ty t)

let () =
  Stdlib.Printexc.register_printer (fun exn ->
      match exn with
      | CantInterpretNode t ->
          Some (Fmt.str "Can't interpret the node %a." Node.pp t)
      | CantCheckGround g ->
          Some
            (Fmt.str "Can't check %a of type %a." pp_gen_ground g pp_ty_gen g)
      | CantComputeGround g ->
          Some
            (Fmt.str "Can't compute %a of type %a." pp_gen_ground g pp_ty_gen g)
      | CantInterpretTy t ->
          Some (Fmt.str "Can't interpret the type %a." Ground.Ty.pp t)
      | GroundTermWithoutValueAfterModelFixing g ->
          Some
            (Fmt.str "After model fixing %a of type %a has no value"
               pp_gen_ground g pp_ty_gen g)
      | ArgOfGroundTermWithoutValueAfterModelFixing (g, n) ->
          Some
            (Fmt.str
               "After model fixing arguments %a of %a of type %a has no value"
               Node.pp n Ground.Term.pp (Ground.sem g) Ground.Ty.pp
               (Ground.sem g).ty)
      | _ -> None)

(** Helpers *)
module WatchArgs = struct
  (** todo move the check to enqueue; but it requires to be able to attach event
      in delayed_ro *)

  module Daemon = struct
    type t = {
      callback : Egraph.wt -> Ground.t -> unit;
      ground : Ground.t;
      current : int Context.Ref.t;  (** already set before *)
    }

    type runable = t

    let print_runable = Fmt.nop

    let key =
      Events.Dem.create
        (module struct
          type t = runable

          let name = "Interp.WatchArgs"
        end)

    let delay = Events.FixingModel

    let rec check_if_set d args i =
      if Option.is_none (Egraph.get_value d (IArray.get args i)) then Some i
      else
        let i = 1 + i in
        if IArray.length args <= i then None else check_if_set d args i

    let attach d r i =
      Egraph.attach_any_value d (IArray.get (Ground.sem r.ground).args i)
        (fun _ _ _ -> Events.EnqRun (key, r, Some (Ground.thterm r.ground)))

    let run d r =
      let o_current = Context.Ref.get r.current in
      assert (o_current < IArray.length (Ground.sem r.ground).args);
      match check_if_set d (Ground.sem r.ground).args o_current with
      | None -> r.callback d r.ground
      | Some current ->
          assert (current <> o_current);
          attach d r current

    let create d callback ground =
      if IArray.is_empty (Ground.sem ground).args then callback d ground
      else
        let r current =
          (* Debug.dprintf3 debug "Interp create %a (%i)" Ground.pp ground
           *   current; *)
          {
            callback;
            ground;
            current = Context.Ref.create (Egraph.context d) current;
          }
        in
        match check_if_set d (Ground.sem ground).args 0 with
        | None -> callback d ground
        | Some current -> attach d (r current) current
  end

  let () = Egraph.Wait.register_dem (module Daemon)
  let create = Daemon.create
end

let init d =
  (* Demon.Fast.register_any_new_node_daemon ~name:"Interp.new_node"
   *   (fun d g -> Datastructure.Push.push Data.reg_node d g)
   *   d; *)
  Ground.register_converter d (fun d g ->
      match Egraph.get_env d Data.env with
      | Before -> Datastructure.Push.push Data.reg_ground d g
      | During _ -> ());
  Demon.WithFilter.attach_reg_sem d Ground.ClosedQuantifier.key (fun d g ->
      Datastructure.Push.push Data.reg_closed_quantifiers d g;
      None);
  Demon.WithFilter.attach_reg_sem d Ground.NotTotallyApplied.key (fun d g ->
      Datastructure.Push.push Data.reg_not_totally_applied d g;
      None)
