(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Base

module Seq : sig
  type 'a t
  (** Sequence, finite during module fixing *)

  val ( let+ ) : 'a t -> ('a -> 'b) -> 'b t

  val ( and* ) : 'a t -> 'b t -> ('a * 'b) t
  (** cartesian product {!Base.Sequence.interleaved_cartesian_product} *)
  val ( let* ) : 'a t -> ('a -> 'b t) -> 'b t

  val concat : 'a t t -> 'a t
  (* Base.Sequence.interleave *)

  val of_seq: 'a Base.Sequence.t -> 'a t

  val return: 'a -> 'a t
end

type check = Right | Wrong | NA | Unknown

val check_of_bool : bool -> check

type compute = Value of Nodes.Value.t | NA

exception CantInterpretTy of Ground.Ty.t
exception CantInterpretNode of Nodes.Node.t

exception
  CantCheckGround of
    [ `Ground of Ground.t
    | `ClosedQuantifier of Ground.ClosedQuantifier.t
    | `NotTotallyApplied of Ground.NotTotallyApplied.t ]

exception
  CantComputeGround of
    [ `Ground of Ground.t
    | `ClosedQuantifier of Ground.ClosedQuantifier.t
    | `NotTotallyApplied of Ground.NotTotallyApplied.t ]

exception
  GroundTermWithoutValueAfterModelFixing of
    [ `Ground of Ground.t
    | `ClosedQuantifier of Ground.ClosedQuantifier.t
    | `NotTotallyApplied of Ground.NotTotallyApplied.t ]

exception ArgOfGroundTermWithoutValueAfterModelFixing of Ground.t * Nodes.Node.t

module Register : sig
  val check : Egraph.wt -> (Egraph.rt -> Ground.t -> check) -> unit

  val check_closed_quantifier :
    Egraph.wt -> (Egraph.rt -> Ground.ClosedQuantifier.t -> check) -> unit

  val check_not_totally_applied :
    Egraph.wt -> (Egraph.rt -> Ground.NotTotallyApplied.t -> check) -> unit

  val compute : Egraph.wt -> (Egraph.rt -> Ground.t -> compute) -> unit

  val compute_closed_quantifier :
    Egraph.wt -> (Egraph.rt -> Ground.ClosedQuantifier.t -> compute) -> unit

  val compute_not_totally_applied :
    Egraph.wt -> (Egraph.rt -> Ground.NotTotallyApplied.t -> compute) -> unit

  val dependency_by_ground_term :
    Egraph.wt -> (Egraph.rt -> Ground.t -> (Nodes.Node.t * Nodes.Node.t) list option) -> unit
(**
  Return which node should preferably be choosen before another for the given ground term
*)


  val node :
    Egraph.wt ->
    ((Egraph.wt -> Nodes.Node.t -> Nodes.Value.t Seq.t) ->
    Egraph.wt ->
    Nodes.Node.t ->
    Nodes.Value.t Seq.t option) ->
    unit
  (** Register the computation of possible values for a node using the
      information on the domains. It could ask the computation of other nodes *)

  val ty :
    Egraph.wt ->
    (Egraph.rt -> Ground.Ty.t -> Nodes.Value.t Base.Sequence.t option) ->
    unit
  (** Register iterators on all the possible value of some types, all the values
      must be reached eventually *)

  val print_value_smt :
    (_, 'a) Nodes.ValueKind.t -> (Egraph.rt -> Ground.Ty.t -> 'a Fmt.t) -> unit
end

val ty : _ Egraph.t -> Ground.Ty.t -> Nodes.Value.t Base.Sequence.t
(** Iterate on all the possible value of the given type, usually all the values
    will be reached eventually *)

val print_value_smt : _ Egraph.t -> Ground.Ty.t -> Nodes.Value.t Fmt.t

val interp : Egraph.wt -> Expr.Term.t -> Nodes.Value.t
(** [interp d e] Should be used when the model has been computed. Compute the
   value of [e] in the current model. The value of [e] and the intermediary
   expression are also stored in the [d] *)

val init : Egraph.wt -> unit
(** Initialize the module for later use *)

module Fix_model : sig
  (** The model is search using iterative deepening depth-first search for
     ensuring optimality even in presence of infinite choice *)

  val next_dec : Egraph.wt -> (Egraph.wt -> unit) Sequence.t
  (** Add a level of decision for fixing the model, another level could be
      needed. Could raise unsatisfiable if all the model have been tried *)
end

module WatchArgs : sig
  val create : Egraph.wt -> (Egraph.wt -> Ground.t -> unit) -> Ground.t -> unit
  (** call the given function when all the arguments of the ground term have a value *)
end

val spy_sequence: 'a Fmt.t -> string -> 'a Sequence.t -> 'a Sequence.t