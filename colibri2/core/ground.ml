(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib
open Nodes

let debug = Debug.register_info_flag ~desc:"Ground debug" "ground"

module All = struct
  open! Base

  type ty = Nodes.ty = { app : Expr.Ty.Const.t; args : ty list }
  [@@deriving eq, ord, hash]

  let rec pp_ty fmt ty =
    match ty.args with
    | [] -> Fmt.pf fmt "%a" Expr.Ty.Const.pp ty.app
    | _ ->
        Fmt.pf fmt "%a(%a)" Expr.Ty.Const.pp ty.app
          Fmt.(list ~sep:comma pp_ty)
          ty.args

  type term = {
    app : Expr.Term.Const.t;
    tyargs : ty list;
    args : Node.t IArray.t;
    ty : ty;
  }
end

module Subst = struct
  module T = struct
    type ty = All.ty Expr.Ty.Var.M.t [@@deriving eq, ord, show, hash]

    type t = { term : Node.t Expr.Term.Var.M.t; ty : ty }
    [@@deriving eq, ord, show, hash]
  end

  include T
  include Colibri2_popop_lib.Popop_stdlib.MkDatatype (T)

  let empty = { term = Expr.Term.Var.M.empty; ty = Expr.Ty.Var.M.empty }

  let distinct_union subst1 subst2 =
    {
      ty = Expr.Ty.Var.M.union (fun _ _ -> assert false) subst1.ty subst2.ty;
      term =
        Expr.Term.Var.M.union (fun _ _ -> assert false) subst1.term subst2.term;
    }

  let map_repr d subst =
    { subst with term = Expr.Term.Var.M.map (Egraph.find_def d) subst.term }

  let mk term_l ty_l =
    { term = Expr.Term.Var.M.of_list term_l; ty = Expr.Ty.Var.M.of_list ty_l }
end

module Ty = struct
  module T = struct
    open! Base

    type t = All.ty = { app : Expr.Ty.Const.t; args : t list }
    [@@deriving hash, ord, eq]

    let pp = All.pp_ty
  end

  include T
  include Colibri2_popop_lib.Popop_stdlib.MkDatatype (T)

  type _ Expr.t += Arrow

  (** Could use array instead, keep separate for avoiding subtile bugs for now
  *)
  let id_arrow =
    Expr.(
      Id.mk ~builtin:Arrow
        (Dolmen_std.Path.global "arrow")
        { arity = 2; alias = No_alias })

  let extract_arrow ty =
    assert (match ty.app.builtin with Arrow -> true | _ -> false);
    match ty.args with [ arg; ret ] -> (arg, ret) | _ -> assert false

  let get_arrow ty =
    match ty.args with [ arg; ret ] -> Some (arg, ret) | _ -> None

  let arrow args ret =
    let rec aux ret = function
      | [] -> invalid_arg "arrow without arguments"
      | [ arg ] -> { app = id_arrow; args = [ arg; ret ] }
      | arg :: l -> { app = id_arrow; args = [ arg; aux ret l ] }
    in
    aux ret args

  let rec apply_term n (ty : t) =
    match (ty.app.builtin, n) with
    | _, 0 -> ty
    | Arrow, n ->
        assert (n > 0);
        let _, ret = extract_arrow ty in
        apply_term (n - 1) ret
    | _, _ -> assert false

  let rec convert subst (t : Expr.Ty.t) =
    match t.ty_descr with
    | TyVar v -> (
        match Expr.Ty.Var.M.find v subst with
        | exception Not_found ->
            invalid_arg (Fmt.str "Not_ground: %a" Expr.Ty.Var.pp v)
        | ty -> ty)
    | TyApp (f, args) ->
        let args = List.map (convert subst) args in
        { app = f; args }
    | Arrow (args, ret) ->
        let args = List.map (convert subst) args in
        let ret = convert subst ret in
        arrow args ret
    | Pi _ -> invalid_arg "Not_ground: polymorphism"

  let apply app args = { app; args }
  let prop = convert Expr.Ty.Var.M.empty Expr.Ty.prop
  let bool = convert Expr.Ty.Var.M.empty Expr.Ty.bool
  let unit = convert Expr.Ty.Var.M.empty Expr.Ty.unit
  let base = convert Expr.Ty.Var.M.empty Expr.Ty.base
  let int = convert Expr.Ty.Var.M.empty Expr.Ty.int
  let rat = convert Expr.Ty.Var.M.empty Expr.Ty.rat
  let real = convert Expr.Ty.Var.M.empty Expr.Ty.real
  let array a b = apply Expr.Ty.Const.array [ a; b ]
  let string = convert Expr.Ty.Var.M.empty Expr.Ty.string
  let string_reg_lang = convert Expr.Ty.Var.M.empty Expr.Ty.string_reg_lang

  let definition sym =
    match Expr.Ty.definition sym with None -> Expr.Abstract | Some x -> x
end

module Term = struct
  module T = struct
    open! Base

    type t = All.term = {
      app : Expr.Term.Const.t;
      tyargs : Ty.t list;
      args : Node.t IArray.t;
      ty : Ty.t;
    }
    [@@deriving hash, ord, eq]

    let list_paren ?sep ~(paren : _ Fmt.t -> _ Fmt.t) pp fmt = function
      | [] -> ()
      | l -> Fmt.pf fmt "%a" Fmt.(paren (list ?sep pp)) l

    let array_paren ?(sep = Fmt.nop) ~(paren : _ Fmt.t -> _ Fmt.t) pp fmt t =
      if IArray.is_empty t then ()
      else Fmt.pf fmt "%a" (paren (IArray.pp ~sep pp)) t

    let pp fmt (t : All.term) =
      let infix sep =
        Fmt.(
          array_paren
            ~sep:(fun fmt () -> Fmt.pf fmt "@ %s " sep)
            ~paren:parens Node.pp)
          fmt t.args
      in
      match t.app.builtin with
      | Expr.Equal | Expr.Equiv -> infix "="
      | Expr.Lt _ -> infix "<"
      | Expr.Leq _ -> infix "≤"
      | Expr.Gt _ -> infix ">"
      | Expr.Geq _ -> infix "≥"
      | Expr.Imply -> infix "⇒"
      | Expr.And -> infix "∧"
      | Expr.Or -> infix "∨"
      | Expr.Sub _ -> infix "-"
      | Expr.Mul _ -> infix "*"
      | _ ->
          Fmt.pf fmt "%a%a%a" Expr.Term.Const.pp t.app
            Fmt.(list_paren ~sep:comma ~paren:brackets Ty.pp)
            t.tyargs
            Fmt.(array_paren ~sep:comma ~paren:parens Node.pp)
            t.args
  end

  include T
  include Colibri2_popop_lib.Popop_stdlib.MkDatatype (T)

  let ty t = t.ty
  let name = "ground"
end

type s = All.term = {
  app : Expr.Term.Const.t;
  tyargs : Ty.t list;
  args : Node.t IArray.t;
  ty : Ty.t;
}

module ThTerm = RegisterThTerm (Term)

module ClosedQuantifier0 = struct
  module T = struct
    open! Base

    type binder = Forall | Exists [@@deriving eq, ord, hash]

    type t = {
      binder : binder;
      subst : Subst.t;
      ty_vars : Expr.Ty.Var.t list;
      term_vars : Expr.Term.Var.t list;
      body : Expr.Term.t;
    }
    [@@deriving eq, ord, hash]

    let pp_subst fmt (subst : Subst.t) =
      if not (Expr.Ty.Var.M.is_empty subst.ty) then
        Fmt.pf fmt "{%a}" (Expr.Ty.Var.M.pp All.pp_ty) subst.ty;
      if not (Expr.Term.Var.M.is_empty subst.term) then
        Fmt.pf fmt "{%a}" (Expr.Term.Var.M.pp Node.pp) subst.term

    let pp fmt t =
      Fmt.pf fmt "%s%a%a.%a%a"
        (match t.binder with Forall -> "∀" | Exists -> "∃")
        (Term.list_paren ~sep:Fmt.comma ~paren:Fmt.brackets Expr.Ty.Var.pp)
        t.ty_vars
        (Term.list_paren ~sep:Fmt.comma ~paren:Fmt.parens Expr.Term.Var.pp)
        t.term_vars Expr.Term.pp t.body pp_subst t.subst
  end

  include T
  include Colibri2_popop_lib.Popop_stdlib.MkDatatype (T)

  let ty _ = assert false (* todo *)
  let name = "ClosedQuantifier"
end

module ClosedQuantifier = struct
  type binder = ClosedQuantifier0.binder = Forall | Exists

  type s = ClosedQuantifier0.t = {
    binder : binder;
    subst : Subst.t;
    ty_vars : Expr.Ty.Var.t list;
    term_vars : Expr.Term.Var.t list;
    body : Expr.Term.t;
  }

  include (
    RegisterThTerm (ClosedQuantifier0) : RegisteredThTerm with type s := s)
end

module NotTotallyApplied0 = struct
  module T = struct
    open! Base

    type t =
      | Lambda of {
          subst : Subst.t;
          ty_vars : Expr.Ty.Var.t list;
          term_vars : Expr.Term.Var.t list;
          body : Expr.Term.t;
          ty : Ty.t;
        }
      | Cst of { tc : Expr.Term.Const.t; ty : Ty.t }
      | App of {
          app : Node.t;
          tyargs : Ty.t list;
          args : Node.t IArray.t;
          ty : Ty.t;
        }
    [@@deriving eq, ord, hash]

    let pp fmt = function
      | Lambda t ->
          Fmt.pf fmt "λ%a%a.%a"
            (Term.list_paren ~sep:Fmt.comma ~paren:Fmt.brackets Expr.Ty.Var.pp)
            t.ty_vars
            (Term.list_paren ~sep:Fmt.comma ~paren:Fmt.parens Expr.Term.Var.pp)
            t.term_vars Expr.Term.pp t.body
      | Cst { tc; _ } -> Expr.Term.Const.pp fmt tc
      | App t ->
          Fmt.pf fmt "%a%a%a" Node.pp t.app
            Fmt.(Term.list_paren ~sep:comma ~paren:brackets Ty.pp)
            t.tyargs
            Fmt.(Term.array_paren ~sep:comma ~paren:parens Node.pp)
            t.args
  end

  include T
  include Colibri2_popop_lib.Popop_stdlib.MkDatatype (T)

  let ty = function
    | App { ty; _ } -> ty
    | Cst { ty } -> ty
    | Lambda { ty; _ } -> ty

  let name = "ClosedLambda"
end

module NotTotallyApplied = struct
  type s = NotTotallyApplied0.t =
    | Lambda of {
        subst : Subst.t;
        ty_vars : Expr.Ty.Var.t list;
        term_vars : Expr.Term.Var.t list;
        body : Expr.Term.t;
        ty : Ty.t;
      }
    | Cst of { tc : Expr.Term.Const.t; ty : Ty.t }
    | App of {
        app : Node.t;
        tyargs : Ty.t list;
        args : Node.t IArray.t;
        ty : Ty.t;
      }

  include (
    RegisterThTerm (NotTotallyApplied0) : RegisteredThTerm with type s := s)

  let ty t = match sem t with Lambda { ty } | App { ty } | Cst { ty } -> ty
end

let cst_normalizations_db = Expr.Term.Const.H.create 17

let register_cst_normalization cst ty f =
  (* Needs to be contextual for the error to make sens, otherwise it crashes
  on unit tests. *)
  Expr.Term.Const.H.change
    (function
      | None -> Some (Ty.M.singleton ty f) | Some m -> Some (Ty.M.add ty f m))
      (* match Ty.M.find_opt ty m with
          | None ->
          | Some _ ->
              Fmt.failwith
                "Registering more that one rewriting for the constructor: %a \
                 of type %a"
                Expr.Term.Const.pp cst Ty.pp ty *)
    cst_normalizations_db cst

let mk_const app ?(tyargs = []) ?(args = IArray.empty) ty =
  match Expr.Term.Const.H.find_opt cst_normalizations_db app with
  | None -> { app; tyargs; args; ty }
  | Some m -> (
      match Ty.M.find_opt ty m with
      | None -> { app; tyargs; args; ty }
      | Some f -> f tyargs args ty)

let rec convert_one_app_and_iter d (f : Expr.Term.t) tyargs args ty
    (subst : Subst.t) fg fcq fnt =
  let ty = Ty.convert subst.ty ty in
  match (ty.app.builtin, f.term_descr) with
  | Ty.Arrow, _ | _, (Match _ | App _ | Var _ | Binder _) ->
      let nt =
        NotTotallyApplied.index
        @@ App
             {
               app = convert_and_iter fg fcq fnt d subst f;
               tyargs = List.map (Ty.convert subst.ty) tyargs;
               args;
               ty;
             }
      in
      fnt d nt;
      NotTotallyApplied.node nt
  | _, Cst f ->
      let tyargs = List.map (Ty.convert subst.ty) tyargs in
      let g = ThTerm.index @@ mk_const f ~tyargs ~args ty in
      fg d g;
      ThTerm.node g

and convert_one_cst_and_iter d (f : Expr.Term.Const.t) ty (subst : Subst.t) fg
    fnt =
  let ty_args, args, _ = Expr.Ty.poly_sig f.id_ty in
  if List.is_empty args then (
    assert (List.is_empty ty_args);
    let g =
      ThTerm.index
      @@ {
           Term.app = f;
           tyargs = [];
           args = IArray.empty;
           ty = Ty.convert subst.ty ty;
         }
    in
    fg d g;
    ThTerm.node g)
  else
    let nt =
      NotTotallyApplied.index @@ Cst { tc = f; ty = Ty.convert subst.ty ty }
    in
    fnt d nt;
    NotTotallyApplied.node nt

and convert_one_binder_and_iter d (b : Expr.binder) body ty subst fcq fnt =
  match b with
  | Exists (ty_vars, term_vars) | Forall (ty_vars, term_vars) ->
      let binder =
        match b with
        | Exists _ -> ClosedQuantifier0.Exists
        | Forall _ -> ClosedQuantifier0.Forall
        | Let_seq _ | Let_par _ | Lambda _ -> assert false
      in
      let cq =
        ClosedQuantifier.index @@ { binder; ty_vars; term_vars; body; subst }
      in
      fcq d cq;
      ClosedQuantifier.node cq
  | Lambda (ty_vars, term_vars) ->
      let nt =
        NotTotallyApplied.index
        @@ Lambda
             { ty_vars; term_vars; body; subst; ty = Ty.convert subst.ty ty }
      in
      fnt d nt;
      NotTotallyApplied.node nt
  | Let_seq _ | Let_par _ -> invalid_arg "convert_one_binder: let_* given"

and convert_let_seq_and_iter d l body (subst : Subst.t) convert_and_iter =
  let subst_term =
    List.fold_left
      (fun term (v, t) ->
        let n = convert_and_iter d { Subst.term; ty = subst.ty } t in
        Expr.Term.Var.M.add v n term)
      subst.term l
  in
  convert_and_iter d { term = subst_term; ty = subst.ty } body

and convert_let_par_and_iter d l body (subst : Subst.t) convert_and_iter =
  let subst_term =
    List.fold_left
      (fun term (v, t) ->
        let n = convert_and_iter d subst t in
        Expr.Term.Var.M.add v n term)
      subst.term l
  in
  convert_and_iter d { term = subst_term; ty = subst.ty } body

and convert_and_iter fg fcq fnt d subst (t : Expr.Term.t) =
  match t.term_descr with
  | Var v -> (
      match Expr.Term.Var.M.find v subst.term with
      | exception Not_found ->
          invalid_arg (Fmt.str "Not_ground: %a" Expr.Term.Var.pp v)
      | n -> n)
  | App (f, tyargs, args) ->
      let args =
        IArray.of_list_map ~f:(convert_and_iter fg fcq fnt d subst) args
      in
      convert_one_app_and_iter d f tyargs args t.term_ty subst fg fcq fnt
  | Cst f -> convert_one_cst_and_iter d f t.term_ty subst fg fnt
  | Expr.Binder (((Exists _ | Forall _ | Lambda _) as b), body) ->
      convert_one_binder_and_iter d b body t.term_ty subst fcq fnt
  | Expr.Binder (Let_seq l, body) ->
      convert_let_seq_and_iter d l body subst (convert_and_iter fg fcq fnt)
  | Expr.Binder (Let_par l, body) ->
      convert_let_par_and_iter d l body subst (convert_and_iter fg fcq fnt)
  | Expr.Match (_, _) -> invalid_arg "match from dolmen not implemented"
(* TODO convert to one multitest like
   the match of why3  and projection *)

let fg d th = Choice_group.make_choosable d (ThTerm.thterm th)
let fcq d th = Choice_group.make_choosable d (ClosedQuantifier.thterm th)
let fnt d th = Choice_group.make_choosable d (NotTotallyApplied.thterm th)
let convert ?(subst = Subst.empty) d e = convert_and_iter fg fcq fnt d subst e

let convert_not_choosable ?(subst = Subst.empty) e =
  let unit () _ = () in
  convert_and_iter unit unit unit () subst e

let convert_one_app subst d a b c e =
  convert_one_app_and_iter d a b c e subst fg fcq fnt

let convert_one_cst subst d a ty = convert_one_cst_and_iter d a ty subst fg fnt

let convert_one_binder subst d a b c =
  convert_one_binder_and_iter d a b c subst fcq fnt

let convert_let_seq subst d a b iter = convert_let_seq_and_iter d a b subst iter
let convert_let_par subst d a b iter = convert_let_par_and_iter d a b subst iter

let apply _ (f : Expr.Term.Const.t) tyargs args =
  let rec apply_ty subst tyargs (ty : Expr.Ty.t) =
    match (ty.ty_descr, tyargs) with
    | Pi (ty_vars, ty), _ ->
        let tyargs, tyargs' = Base.List.split_n tyargs (List.length ty_vars) in
        let subst =
          List.fold_left2
            (fun acc v ty -> Expr.Ty.Var.M.add v ty acc)
            subst ty_vars tyargs
        in
        apply_ty subst tyargs' ty
    | _, [] -> Ty.convert subst ty
    | _ -> assert false
  in
  let ty = apply_ty Expr.Ty.Var.M.empty tyargs f.id_ty in
  let ty = Ty.apply_term (IArray.length args) ty in
  mk_const f ~tyargs ~args ty

(** Definitions *)
module Defs = struct
  let handler = Datastructure.Push.create Fmt.nop "Ground.Defs.handler"
  let add_handler d f = Datastructure.Push.push handler d f

  let add d tc tyl tvl body =
    Datastructure.Push.iter handler d ~f:(fun f -> f d tc tyl tvl body)
end

let registered_converter = Datastructure.Push.create Fmt.nop "Ground.converters"
let register_converter d f = Datastructure.Push.push registered_converter d f

module Tys : sig
  val tys : _ Egraph.t -> Node.t -> Ty.S.t
  val add_ty : Egraph.wt -> Node.t -> Ty.t -> unit
end = struct
  let dom_tys =
    DomKind.create
      (module struct
        type nonrec t = Ty.S.t

        let name = "Ground.tys"
      end)

  let () =
    Egraph.register_dom
      (module struct
        include Ty.S

        let key = dom_tys
        let merged tys0 tys1 = Option.equal equal tys0 tys1

        let merge d (tys0, n0) (tys1, n1) _ =
          let s =
            Ty.S.union
              (Base.Option.value ~default:Ty.S.empty tys0)
              (Base.Option.value ~default:Ty.S.empty tys1)
          in
          Egraph.set_dom d key n0 s;
          Egraph.set_dom d key n1 s

        let wakeup_threshold = None
      end)

  let tys d n =
    match Egraph.get_dom d dom_tys n with
    | None -> (
        match Nodes.Only_for_solver.thterm n with
        | None -> Ty.S.empty
        | Some th -> Ty.S.singleton (Nodes.ThTerm.ty th))
    | Some tys -> tys

  let add_ty d n ty =
    match Egraph.get_dom d dom_tys n with
    | None -> Egraph.set_dom d dom_tys n (Ty.S.singleton ty)
    | Some tys -> (
        match Ty.S.add_new Exit ty tys with
        | exception Exit -> ()
        | tys -> Egraph.set_dom d dom_tys n tys)
end

let tys = Tys.tys
let add_ty = Tys.add_ty
let nb_registered = Datastructure.Ref.create Fmt.int "Ground.registered" 0

let init d =
  Demon.Simple.DelayedBy1FixModel.attach_reg_sem d ThTerm.key (fun d g ->
      Datastructure.Ref.incr ~trace:"Ground.registered" nb_registered d;
      Debug.dprintf2 debug "[Ground] run registered for %a" ThTerm.pp g;
      let n = ThTerm.node g in
      let s = ThTerm.sem g in
      Tys.add_ty d n s.ty;
      Datastructure.Push.iter registered_converter d ~f:(fun f -> f d g));
  Demon.Simple.DelayedBy1FixModel.attach_reg_sem d ClosedQuantifier.key
    (fun d g ->
      let n = ClosedQuantifier.node g in
      Tys.add_ty d n Ty.bool)

include (ThTerm : RegisteredThTerm with type s := s and type t = ThTerm.t)

let apply' env f tyargs args =
  node (index (apply env f tyargs (IArray.of_list args)))
