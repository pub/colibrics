(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

type t
(** A scope for decision, allows to wait before adding decisions for terms. Used
    for propagating into a term without decisions *)

val create : _ Egraph.t -> t
val activate : _ Egraph.t -> t -> unit
val add_to_group : _ Egraph.t -> Nodes.ThTerm.t -> t -> unit

val register_decision : _ Egraph.t -> Nodes.ThTerm.t -> Egraph.choice -> unit
(** If no group have been specified at that point, the default is for the
    decision to be directly activated *)

val make_choosable : _ Egraph.t -> Nodes.ThTerm.t -> unit
(** Register the decisions attached to the node, and will not delay them in the
    future *)

val add_to_group_of : _ Egraph.t -> Nodes.ThTerm.t -> Nodes.ThTerm.t -> unit
