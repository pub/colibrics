(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Nodes

module type Attach = sig
  type ('a, 'b, 'filter_arg) arg
  type 'a env
  type result

  val attach_dom :
    ( ?thterm:Nodes.ThTerm.t ->
      ?direct:bool ->
      Node.t ->
      'a DomKind.t ->
      (Node.t -> result) env ->
      unit,
      'b,
      Node.t )
    arg

  val attach_any_dom :
    ( ?thterm:Nodes.ThTerm.t -> 'a DomKind.t -> (Node.t -> result) env -> unit,
      'b,
      Node.t )
    arg

  val attach_value :
    ( ?thterm:Nodes.ThTerm.t ->
      ?direct:bool ->
      Node.t ->
      ('a, 'b) ValueKind.t ->
      (Node.t -> 'b -> result) env ->
      unit,
      'r,
      Node.t )
    arg

  val attach_any_value :
    ( ?thterm:Nodes.ThTerm.t ->
      ?direct:bool ->
      Node.t ->
      (Node.t -> Value.t -> result) env ->
      unit,
      'b,
      Node.t )
    arg

  val attach_reg_any :
    (?thterm:Nodes.ThTerm.t -> (Node.t -> result) env -> unit, 'b, Node.t) arg

  val attach_reg_node :
    ( ?thterm:Nodes.ThTerm.t -> Node.t -> (Node.t -> result) env -> unit,
      'b,
      Node.t )
    arg

  val attach_reg_sem :
    ( ?thterm:Nodes.ThTerm.t ->
      ('a, 'b) ThTermKind.t ->
      ('b -> result) env ->
      unit,
      'r,
      'b )
    arg

  val attach_reg_value :
    ( ?thterm:Nodes.ThTerm.t ->
      ('a, 'b) ValueKind.t ->
      ('b -> result) env ->
      unit,
      'r,
      'b )
    arg

  val attach_repr :
    ( ?thterm:Nodes.ThTerm.t -> Node.t -> (Node.t -> result) env -> unit,
      'b,
      Node.t )
    arg

  val attach_any_repr :
    (?thterm:Nodes.ThTerm.t -> (Node.t -> result) env -> unit, 'b, Node.t) arg
end

module type Simple = sig
  include
    Attach
      with type ('a, 'b, 'filter) arg :=
        ?once:unit ->
        ?filter:(Egraph.rt -> 'filter -> bool) ->
        'b Egraph.t ->
        'a
       and type 'a env := Egraph.wt -> 'a
       and type result := unit

  val schedule_immediately :
    _ Egraph.t -> ?thterm:Nodes.ThTerm.t -> (Egraph.wt -> unit) -> unit
end

module Simple : sig
  include Simple
  module FixingModel : Simple
  module DelayedBy1FixModel : Simple
  module LastEffort : Simple
  module LastEffortUncontextual : Simple
  module LastEffortLate : Simple
  module LastEffortLateUncontextual : Simple
end

module WithFilter :
  Attach
    with type ('a, 'b, _) arg := 'b Egraph.t -> 'a
     and type 'a env := Egraph.rt -> 'a
     and type result := (Egraph.wt -> unit) option

module Key : sig
  type state = Wait | Stop

  module type S = sig
    module Key : Colibri2_popop_lib.Popop_stdlib.Datatype

    val delay : Events.delay
    val run : Egraph.wt -> Key.t -> state
    val name : string
  end

  module Register (S : S) : sig
    include
      Attach
        with type ('a, 'b, _) arg := 'b Egraph.t -> 'a
         and type 'a env := S.Key.t
         and type result := unit
  end
end

type ('a, 'b) generic_wait = {
  attach : 's. 's Egraph.t -> 'a -> (Egraph.rt -> Events.enqueue) -> unit;
  has_value : 's. 's Egraph.t -> 'a -> bool;
  get : 's. 's Egraph.t -> 'a -> 'b option;
}

module type Monad = sig
  type 'a monad
  type sequence

  val nop : sequence
  val getd : ?def:'a -> 'a DomKind.t -> Node.t -> 'a option monad

  val getdf :
    ?def:'a -> (Egraph.rt -> Node.t -> 'a option) -> Node.t -> 'a option monad

  val setd : 'a DomKind.t -> Node.t -> 'a option monad -> sequence
  val exec : (Egraph.wt -> unit) -> bool option monad -> sequence
  val generic_wait : ('a, 'b) generic_wait -> 'a -> 'b option monad

  val updd :
  ?check:(Egraph.rt -> Node.t -> 'a -> bool) ->
    (Egraph.wt -> Node.t -> 'a -> unit) -> Node.t -> 'a option monad -> sequence

  val getv : ('a, _) ValueKind.t -> Node.t -> 'a option monad
  val setv : ('a, _) ValueKind.t -> Node.t -> 'a option monad -> sequence
  val exec_ro : (Egraph.rt -> unit) -> 'a monad -> 'a monad
  val ( let+ ) : 'a option monad -> ('a -> 'c) -> 'c option monad
  val ( let* ) : 'a option monad -> ('a -> 'c option) -> 'c option monad
  val ( and+ ) : 'a option monad -> 'b option monad -> ('a * 'b) option monad
  val ( && ) : sequence -> sequence -> sequence
  val attach : Lexing.position -> _ Egraph.t -> ?thterm:Nodes.ThTerm.t -> sequence -> unit
end

module Monad : Monad
module MonadAlsoInterp : Monad
