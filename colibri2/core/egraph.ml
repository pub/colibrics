(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib
open Popop_stdlib
open Nodes

module StdQueue = Queue

(* A queue that add trace messages when values enter or leave *)
module Queue = struct
  type 'a tagged = Tag of (int * 'a)

  type 'a t = {
    queue : 'a tagged StdQueue.t;
    name : string;
    counter : int ref
  }

  let trace_data (q : 'a t) id () = [
    ("id", (`Int id));
    ("queue", (`String q.name))
  ]

  let is_empty { queue ; _ } = StdQueue.is_empty queue
  let clear { queue ; _ } = StdQueue.clear queue
  let transfer { queue = q1 ; _ } { queue = q2 ; _ } = StdQueue.transfer q1 q2
  let create name = {
    queue = StdQueue.create ();
    name;
    counter = ref 0
  }

  let next_tag ({ counter; _ } as queue)  =
    counter := !counter + 1;
    Trace.message "breadcrumb_push" ~data:(trace_data queue !counter);
    !counter

  let tag q v = Tag (next_tag q, v)
  let add (v : 'a) queue  = StdQueue.add (tag queue v) queue.queue
  let push (v : 'a) queue = StdQueue.push (tag queue v) queue.queue

  (* extract values with these functions *)

  let (let*) v f =
    match v with
    | None -> (f None)
    | Some (queue, Tag (id, item)) ->
      (* Scopes are too noisy here *)
      (* Trace.with_span "breadcrumb"
          ~__FILE__ ~__LINE__
          ~data:(trace_data queue id)
          (fun _ -> (f (Some item))) *)
      Trace.message "breadcrumb_pop"
        ~data:(trace_data queue id);
        (f (Some item))

  let take_opt queue = match StdQueue.take_opt queue.queue with
    | None -> None
    | Some v -> Some (queue, v)

end

exception Contradiction
exception DomainMergingFixpointNotReachedInOneTurn

type ro = < >
type rw = < rw: unit >

let debug = Debug.register_flag
    ~desc:"for the core solver"
    "Egraph.all"

let debug_few = Debug.register_info_flag
    ~desc:"for the core solver"
    "Egraph.few"

let print_decision = Debug.register_info_flag
    ~desc:"Print@ information@ about@ the@ decisions@ made"
    "decision"

let print_contradiction = Debug.register_info_flag
    ~desc:"Print@ information@ about@ the@ contradiction@ found"
    "contradiction"

let stats_set_dom =
  Debug.register_stats_int "Egraph.set_dom/merge"
let stats_set_value =
  Debug.register_stats_int "Egraph.set_value/merge"
let stats_register =
  Debug.register_stats_int "Egraph.register"

module HNode = Context.HashtblWithoutRemove(Node)
module HRNode = Context.Hashtbl(Node)

(** For each kind of domain *)
type ('ro,'a) domtable = {
  (** For each representative node its associated domain if any *)
  table : 'a HRNode.t;
  (** Events: the domain of this node changed *)
  events : ('ro -> Node.t -> Events.enqueue) Bag.t HNode.t;
  (** Events: the domain of any node  changed *)
  reg_events : ('ro -> Node.t -> Events.enqueue) Bag.t Context.Ref.t;
}

module VDomTable = DomKind.MkArray (struct type ('a,'ro) t = ('ro,'a) domtable end)

module VSemTable = ThTermKind.MkArray (struct type (_,'b,'ro) t = ('ro -> 'b -> Events.enqueue) Bag.t end)

type ('ro,'a) valuetable = {
  (** Events: the value of this node changed *)
  events : ('ro -> Node.t -> 'a -> Events.enqueue) Bag.t HNode.t;
  (** Events: the value has been registered *)
  reg_events : ('ro -> 'a -> Events.enqueue) Bag.t Context.Ref.t;
}

module ValueTable = ValueKind.MkArray (struct type (_,'b,'ro) t = ('ro,'b) valuetable end)


(** Environnement *)

(** mutable but only contain persistent structure *)
(** Just for easy qualification *)
module Def = struct

  type saved = {
      saved_event_any_reg : event_node Bag.t;
      saved_event_before_merge : event_merge list;
      saved_event_after_merge : event_merge list;
      saved_event_sem   : ro delayed_t VSemTable.t;
      saved_envs  : Env.Saved.ArrayH.t;
      saved_unknown : bool;
    }

  and event_node = (ro delayed_t -> Node.t -> Events.enqueue)

  and t = {
    (** Union Find *)
      repr  : Node.t HNode.t;
      rang  : int HNode.t;
      (** Events: the representative of this node changed *)
      event_repr : event_node Bag.t HNode.t;
      (** Events: the representative of any node changed *)
      mutable event_any_repr : event_node Bag.t;
      (** Events: the value of this node has been set *)
      event_value : (ro delayed_t -> Node.t -> Value.t -> Events.enqueue) Bag.t HNode.t;
      (** Events: this node have been registered *)
      event_reg : event_node Bag.t HNode.t;
      (** Events: any node have been registered *)
      mutable event_any_reg : event_node  Bag.t;
      (** Events: during the merge of two nodes *)
      mutable event_before_merge : event_merge list;
      (** Events: after the merge of two nodes *)
      mutable event_after_merge : event_merge list;
      (** Events: semantic term of this kind has been registered *)
      event_sem   : ro delayed_t VSemTable.t;
      (** Information on the domains, data is {!'a domtable} *)
      dom   : ro delayed_t VDomTable.t;
      (** Value *)
      value : ro delayed_t ValueTable.t;
      values: Value.t HNode.t;
      (** Environment saved during backtrack point *)
      envs  : Env.Saved.ArrayH.t;
      (** Environment not saved during backtrack point, they should use !{Context} *)
      unsaved_envs: Env.Unsaved.ArrayH.t;
      mutable unknown : bool;
      history : saved Context.history;
      delayed  : rw delayed_t;
      mutable node_info_printer : ( ro delayed_t -> Node.t -> (string * string) list) list;
    }


  (** delayed_t is used *)
  and 'a delayed_t = {
    env: t;
    todo_immediate_dem : action_immediate_dem Queue.t;
    todo_merge_dom : action_merge_dom Queue.t;
    mutable todo_delayed_merge : (Node.t * Node.t * bool) option;
    todo_merge : action_merge Queue.t;
    todo_ext_action : action_ext Queue.t;
    mutable sched_daemon : Events.Wait.daemon_key -> unit;
    mutable sched_decision : choice -> unit;
  }

  and action_immediate_dem =
    | RunDem : Events.Wait.daemon_key -> action_immediate_dem

  and action_merge_dom =
    | SetMergeDomNode  :
        'a DomKind.t * Node.t * Node.t * bool -> action_merge_dom

  and action_merge =
    | Merge of Node.t * Node.t

  and action_ext =
    | ExtDem         : Events.Wait.daemon_key  -> action_ext

  and choice_state =
    | DecNo
    | DecTodo of (rw delayed_t -> unit) list

  and choice = {
    choice: rw delayed_t -> choice_state;
    prio: int;
    print_cho: unit Fmt.t;
    key: Node.t option;
  }

  and event_merge = (rw delayed_t -> repr:Node.t -> Node.t -> unit)

  let nothing_todo t =
    Queue.is_empty t.todo_immediate_dem
    && Queue.is_empty t.todo_merge_dom
    && Option.is_none t.todo_delayed_merge
    && Queue.is_empty t.todo_merge
    && Queue.is_empty t.todo_ext_action

  let emptied delayed =
    Queue.clear delayed.todo_immediate_dem;
    Queue.clear delayed.todo_merge_dom ;
    if Option.is_some delayed.todo_delayed_merge then
      Trace.message "clean_merge";
    delayed.todo_delayed_merge <- None ;
    Queue.clear delayed.todo_merge ;
    Queue.clear delayed.todo_ext_action

end

open Def


type choice = Def.choice = {
  choice: rw delayed_t -> choice_state;
  prio: int;
  print_cho: unit Fmt.t;
  key: Node.t option;
}
type choice_state = Def.choice_state =
  | DecNo
  | DecTodo of (rw delayed_t -> unit) list

(** {2 Define events} *)

module WaitDef = struct
  type delayed = rw delayed_t

end
module Wait : Events.Wait.S with type delayed = rw delayed_t =
  Events.Wait.Make(WaitDef)

let new_pending_daemon (type d) t ?thterm (dem : d Events.Dem.t) runable =
  let module Dem = (val Wait.get_dem dem) in
  let daemonkey = Events.Wait.DaemonKey (dem, runable, thterm) in
  match Dem.delay with
  | Immediate -> Queue.push (RunDem daemonkey) t.todo_immediate_dem
  | _ -> t.sched_daemon daemonkey

(** {2 Define domain registration} *)
module VDom = DomKind.Make(struct type delayed = rw delayed_t
type delayed_ro = ro delayed_t end)
include VDom

module Hidden = Context.Make(struct
    type saved = Def.saved
    type t = Def.t

    let save (t:t) : saved =
      assert (Def.nothing_todo t.delayed); {
        saved_event_any_reg = t.event_any_reg;
        saved_event_before_merge = t.event_before_merge;
        saved_event_after_merge = t.event_after_merge;
        saved_event_sem = VSemTable.copy t.event_sem;
        saved_envs = Env.Saved.ArrayH.copy t.envs;
        saved_unknown = t.unknown;
      }

    let restore (s:saved) (t:t) =
      t.event_any_reg <- s.saved_event_any_reg;
      t.event_before_merge <- s.saved_event_before_merge;
      t.event_after_merge <- s.saved_event_after_merge;
      VSemTable.move ~from:s.saved_event_sem ~to_:t.event_sem;
      Env.Saved.ArrayH.move ~from:s.saved_envs ~to_:t.envs;
      t.unknown <- s.saved_unknown;
      Def.emptied t.delayed

    let get_history t = t.history
  end)

(** {2 Table access in the environment } *)

let get_table_dom t k = VDomTable.get t.dom k

let get_table_sem t k =
  VSemTable.get t.event_sem k

let get_table_value t k =
  ValueTable.get t.value k

  let check_initialization () =
    VDom.is_well_initialized () && Wait.is_well_initialized () &&
    Env.Saved.is_well_initialized () &&
    Env.Unsaved.is_well_initialized ()

  

exception UninitializedEnv of string

exception NotRegistered

(** Just used for being able to qualify these function on t *)
module T = struct
  let rec find t node =
    let node' = HNode.find_exn t.repr NotRegistered node in
    if Node.equal node node' then node else
      let r = find t node' in
      HNode.set t.repr node r;
      r

  let find_def t node =
    let node' = HNode.find_def ~def:node t.repr node  in
    if Node.equal node node' then node else
      let r = find t node' in
      HNode.set t.repr node r;
      r

  let is_repr t node =
    try Node.equal (HNode.find t.repr node) node
    with Not_found -> true

  let is_equal t node1 node2 =
    let node1 = find_def t node1 in
    let node2 = find_def t node2 in
    Node.equal node1 node2

  let get_direct_dom (type a) t (dom : a DomKind.t) node =
    HRNode.find_opt (get_table_dom t dom).table node

  let get_dom t dom node =
    let node = find_def t node in
    get_direct_dom t dom node

  let get_direct_value t node =
    HNode.find_opt t.values node

  let get_value t node =
    let node = find_def t node in
    get_direct_value t node

  let get_env : type a. t -> a Env.Saved.t -> a
    = fun t k ->
        Env.Saved.ArrayH.get t.envs k

  let set_env : type a. t -> a Env.Saved.t -> a -> unit
    = fun t k ->
      Env.Saved.ArrayH.set t.envs k

  let get_unsaved_env : type a. t -> a Env.Unsaved.t -> a
    = fun t k ->
      Env.Unsaved.ArrayH.get t.unsaved_envs k

  let is_registered t node =
    HNode.mem t.repr node

end
open T

(** {2 For debugging and display} *)
let escape_dot = Str.global_replace (Str.regexp "[{}|<>]") "\\\\\\0"

let escape_for_dot pp v =
  let s = Format.to_string pp v in
  let s = escape_dot s in
  s

  module Private = struct
    type t = Def.t
    let get_dotgui_only = ref (fun _ -> failwith "badly initialized module")
    let get_unsaved_env = T.get_unsaved_env
    let get_multiple_merging_round = ref (fun _ -> failwith "badly initialized module")
end
let output_graph filename t =
  let open Graph in
  let show_node =
    match !Private.get_dotgui_only t with
    | [] -> (fun _ -> `Show)
    | l ->
      let only = DStr.S.of_list (List.concat_map (String.split ~by:",") l) in
      let h = Node.H.create 10 in
      HNode.iter ~f:(fun node1 node2 ->
          if DStr.S.mem (Fmt.str "%a" Node.pp node1) only then begin
            Node.H.replace h node1 `Show;
            Node.H.change (function
                | Some `Show -> Some `Show
                | _ -> Some `Repr) h node2;
          end
        ) t.repr;
      Node.H.find_def h `Hide
  in
  let module G = struct
    include Imperative.Digraph.Concrete(Node)
    let graph_attributes _ = []
    let default_vertex_attributes _ = [`Shape `Record]
    let vertex_name node = string_of_int (Node.hash node)

    let pp fmt node =
      let iter_dom dom fmt (domtable: _ domtable) =
        try
          let s = HRNode.find domtable.table node in
          Format.fprintf fmt "| {%a | %s}"
            DomKind.pp dom (escape_for_dot (VDom.print_dom dom) s);
        with Not_found -> ()
      in
      let print_value fmt values =
        try
          let s = HNode.find values node in
          (match Value.kind s with
           | Value.Value (kind, value) ->
             let (module S) = Nodes.get_value kind in
             Format.fprintf fmt "| {%a | %s}"
               ValueKind.pp kind (escape_for_dot S.SD.pp (S.value value)))
        with Not_found -> ()
      in
      let print_sem fmt node =
        match show_node node with
        | `Show ->
          begin match Only_for_solver.thterm node with
          | None -> ()
          | Some thterm ->
            match Only_for_solver.sem_of_node thterm with
            | Only_for_solver.ThTerm(sem,v) ->
              let (module S) = Nodes.get_thterm sem in
              let v = S.sem v in
              Format.fprintf fmt "| {%a | %s}"
                ThTermKind.pp sem (escape_for_dot S.SD.pp v)
          end
        | _ -> ()
      in
      let print_node_info fmt node =
        List.iter (fun f ->
            List.iter (fun (k,v) -> Format.fprintf fmt "| {%s | %s}"
            (escape_dot k) (escape_dot v)
            ) (f (t.delayed :> ro delayed_t) node)
          )
        t.node_info_printer
      in
      Format.fprintf fmt "{%a %a %a %a %a}" (* "{%a | %a | %a}" *)
        Node.pp node
        print_sem node
        print_node_info node
        (if is_repr t node
         then VDomTable.pp Format.silent Format.silent
             {VDomTable.printk=Format.silent}
             {VDomTable.printd=iter_dom}
         else Format.silent)
        t.dom
        (if is_repr t node
         then print_value
         else Format.silent)
        t.values

    let vertex_attributes node =
      let label = Format.to_string pp node in
      [`Label label]
    let default_edge_attributes _ = []
    let edge_attributes _ = []
    let get_subgraph _ = None
  end in
  let g = G.create () in
  HNode.iter ~f:(fun node1 node2 ->
      match show_node node1 with
      | `Repr | `Show ->
        if Node.equal node1 node2
        then G.add_vertex g node1
        else G.add_edge g node1 (find_def t node2)
      | `Hide -> ()) t.repr;
  let cout = open_out filename in
  let module Dot = Graphviz.Dot(G) in
  Dot.output_graph cout g;
  close_out cout

module DotGuiLog = Debug.RegisterDebugFlag(struct
    let options = Debug.defaults
    let tag = "dotgui"
    let desc = format_of_string "Show each step in a gui"
  end)

(* fixme: possible race conditions, in particular with tests.exe which
   must be run with -shards 1 when -debug is true; different tests use
   the same working directory for dot files in parallel. *)
let draw_graph =
  let c = ref 0 in
  let last_md5sum = ref "" in
  fun ?(force:bool=false) t ->
    if force || DotGuiLog.active () then
      let dir = "debug_graph.tmp" in
      if not (Sys.file_exists dir) then Unix.mkdir dir 0o700;
      let filename = Format.sprintf "%s/debug_graph%i.dot" dir !c in
      incr c;
      output_graph filename t;
      let md5sum = Digest.file filename in
      if String.equal md5sum !last_md5sum then Sys.remove filename
      else begin
        DotGuiLog.force_info
          "output dot file: %s" filename;
        last_md5sum := md5sum
      end

let pp fmt t =
  let t = t.env in
  let open Graph in
        let show_node =
          match !Private.get_dotgui_only t with
          | [] -> (fun _ -> `Show)
          | l ->
            let only = DStr.S.of_list (List.concat_map (String.split ~by:",") l) in
            let h = Node.H.create 10 in
            HNode.iter ~f:(fun node1 node2 ->
                if DStr.S.mem (Fmt.str "%a" Node.pp node1) only then begin
                  Node.H.replace h node1 `Show;
                  Node.H.change (function
                      | Some `Show -> Some `Show
                      | _ -> Some `Repr) h node2;
                end
              ) t.repr;
            Node.H.find_def h `Hide
        in
        let module G = struct
          include Imperative.Digraph.Concrete(Node)
          let graph_attributes _ = []
          let default_vertex_attributes _ = [`Shape `Record]
          let vertex_name node = string_of_int (Node.hash node)
      
          let pp fmt node =
            let iter_dom dom fmt (domtable: _ domtable) =
              try
                let s = HRNode.find domtable.table node in
                Format.fprintf fmt "| {%a | %s}"
                  DomKind.pp dom (escape_for_dot (VDom.print_dom dom) s);
              with Not_found -> ()
            in
            let print_value fmt values =
              try
                let s = HNode.find values node in
                (match Value.kind s with
                 | Value.Value (kind, value) ->
                   let (module S) = Nodes.get_value kind in
                   Format.fprintf fmt "| {%a | %s}"
                     ValueKind.pp kind (escape_for_dot S.SD.pp (S.value value)))
              with Not_found -> ()
            in
            let print_sem fmt node =
              match show_node node with
              | `Show ->
                begin match Only_for_solver.thterm node with
                | None -> ()
                | Some thterm ->
                  match Only_for_solver.sem_of_node thterm with
                  | Only_for_solver.ThTerm(sem,v) ->
                    let (module S) = Nodes.get_thterm sem in
                    let v = S.sem v in
                    Format.fprintf fmt "| {%a | %s}"
                      ThTermKind.pp sem (escape_for_dot S.SD.pp v)
                end
              | _ -> ()
            in
            let print_node_info fmt node =
              List.iter (fun f ->
                  List.iter (fun (k,v) -> Format.fprintf fmt "| {%s | %s}"
                  (escape_dot k) (escape_dot v)
                  ) (f (t.delayed :> ro delayed_t) node)
                )
              t.node_info_printer
            in
            Format.fprintf fmt "{%a %a %a %a %a}" (* "{%a | %a | %a}" *)
              Node.pp node
              print_sem node
              print_node_info node
              (if is_repr t node
               then VDomTable.pp Format.silent Format.silent
                   {VDomTable.printk=Format.silent}
                   {VDomTable.printd=iter_dom}
               else Format.silent)
              t.dom
              (if is_repr t node
               then print_value
               else Format.silent)
              t.values
      
          let vertex_attributes node =
            let label = Format.to_string pp node in
            [`Label label]
          let default_edge_attributes _ = []
          let edge_attributes _ = []
          let get_subgraph _ = None
        end in
        let g = G.create () in
        HNode.iter ~f:(fun node1 node2 ->
            match show_node node1 with
            | `Repr | `Show ->
              if Node.equal node1 node2
              then G.add_vertex g node1
              else G.add_edge g node1 (find_def t node2)
            | `Hide -> ()) t.repr;
        let module Dot = Graphviz.Dot(G) in
        Dot.fprint_graph fmt g
      

(** {2 Delayed} *)

module Delayed = struct
  open T
  type 'a t = 'a delayed_t

  let context t = Hidden.creator t.env.history

  let find t node =
    find t.env node

  let find_def t node =
    find_def t.env node

  let is_repr t node =
    is_repr t.env node

  let is_equal t node1 node2 =
    is_equal t.env node1 node2

  let get_dom t dom node =
    get_dom t.env dom node

  let get_value t node =
    get_value t.env node

  let get_env t env =
    get_env t.env env

  let set_env t env v =
    set_env t.env env v

  let get_unsaved_env t env =
    get_unsaved_env t.env env

  let is_registered t node =
    is_registered t.env node

  let run_event t apply e =
    match apply (t:> ro t) e with
    | Events.EnqStopped -> false
    | EnqAlready -> true
    | EnqRun (dem,runable,thterm) ->
      new_pending_daemon t dem runable ?thterm;
      true
    | EnqLast (dem,runable,thterm) ->
      new_pending_daemon t dem runable ?thterm;
      false

  let run_events t apply events =
    Bag.filter_homo (run_event t apply) events

  let run_events_and_update t hevents node =
    (** wakeup events *)
    let woken_events = HNode.find_opt hevents node in
    begin match woken_events with
    | None -> ()
    | Some woken_events ->
      let woken_events_filtered = (run_events t (fun t e -> e t node)) woken_events in
      if not (Base.phys_equal woken_events_filtered woken_events) then
        let woken_events_new = HNode.find hevents node in
        let new_events = Bag.unadd_from woken_events woken_events_new in
        let woken_events = List.fold_right Bag.add new_events woken_events_filtered in
        HNode.set hevents node woken_events
    end
    

  let set_value_direct t node0 new_v =
    Debug.incr stats_set_value;
    let node = find t node0 in
    HNode.set t.env.values node new_v;
    (match Value.kind new_v with
     | Value.Value(valuekind,new_v) ->
       let events =
         let events = (get_table_value t.env valuekind).events  in
         HNode.find_def events ~def:Bag.empty node
       in
       (* deletion of events are not taken into account for that kind *)
       ignore (run_events t (fun t e -> e t node new_v) events)
    );
    let events = HNode.find_def t.env.event_value ~def:Bag.empty node in
    (* deletion of events are not taken into account for that kind *)
    ignore (run_events t (fun t e -> e t node new_v) events)

  let add_pending_merge (t : rw t) node node' =
    Debug.dprintf4 debug_few "[Egraph] @[add_pending_merge for %a and %a@]"
      Node.pp node Node.pp node';
    assert (is_registered t node);
    assert (is_registered t node');
    assert (not (Node.equal (find t node) (find t node')));
    (* Add the actual merge for later *)
    Queue.add (Merge (node,node')) t.todo_merge

  let register_trace node =
    begin
      match Only_for_solver.thterm node with
      | None ->
        begin
          Trace.message "register" ~data:(fun () ->
            let s_node = Format.sprintf "%a" Node.pp node in
            [("node", (`String s_node))]);
          if Debug.test_flag debug_few then
            Debug.dprintf2 debug_few "[Egraph] @[register %a@]" Node.pp node
        end
      | Some thterm ->
        begin
          Trace.message "register"
            ~data:(fun () -> 
              let s_term =  Format.sprintf "%a" ThTerm.pp thterm in
              let s_node = Format.sprintf "%a" Node.pp node in    
              [("node", (`String s_node));
               ("term", (`String s_term))]);
          if Debug.test_flag debug_few then
            Debug.dprintf4 debug_few "[Egraph] @[register %a: %a@]"
              Node.pp node ThTerm.pp thterm
        end
    end

  let register t node =
    if not (is_registered t node) then begin
      Debug.incr stats_register;
      register_trace node;
      (* assert ( check_no_dom t node ); *)
      HNode.set t.env.repr node node;
      (** reg_node *)
      let node_events = HNode.find_opt t.env.event_reg node in
      (* deletion of events are not taken into account for that kind *)
      ignore (Opt.map (run_events t (fun t e -> e t node)) node_events);
      (** reg *)
      (* deletion of events are not taken into account for that kind *)
      ignore (run_events t (fun t e -> e t node) t.env.event_any_reg);
      (** reg_sem *)
      match Only_for_solver.open_node node with
      | Only_for_solver.ThTerm thterm ->
        begin match Only_for_solver.sem_of_node thterm with
          | Only_for_solver.ThTerm(sem,thterm) ->
            let reg_events = get_table_sem t.env sem in
            (* deletion of events are not taken into account for that kind *)
            ignore (run_events t (fun t e -> e t thterm) reg_events);
        end
      | Only_for_solver.Value nodevalue ->
        begin match Value.kind nodevalue with
          | Value(value,nodevalue') ->
            let reg_events = Context.Ref.get (get_table_value t.env value).reg_events in
            (* deletion of events are not taken into account for that kind *)
            ignore (run_events t (fun t e -> e t nodevalue') reg_events);
            set_value_direct t node nodevalue
        end
    end

  let set_semvalue_pending t node0 node0' =
    let node = find t node0 in
    begin
      if not (is_registered t node0') then begin
        register t node0';
        (* Here the important part of this function the representative
           is forced to be node. The goal is to not grow the number of
           classes that can be used for representative for
           termination.
        *)
        HNode.set t.env.repr node0' node;
        (** wakeup the daemons register_node *)
        let other_event = HNode.find_opt t.env.event_repr node0' in
        (* can be always removed *)
        ignore (Opt.map (run_events t (fun t e -> e t node0')) other_event);
        (* deletion of events are not taken into account for that kind *)
        ignore (run_events t (fun t e -> e t node0') t.env.event_any_repr);
        (** set the value if node0' is one *)
        match HNode.find_opt t.env.values node, Nodes.Only_for_solver.nodevalue node0' with
        | _, None -> ()
        | None, Some value ->
          set_value_direct t node0 value
        | Some v, Some v' ->
          if Value.equal v v'
          then
            (* already same value. Does that really happen? *)
            ()
          else begin
            Debug.dprintf8 print_contradiction
              "[Egraph] %a is set to %a which have conflicting values %a and %a"
              Node.pp node0 Node.pp node0' Value.pp v Value.pp v';
            raise Contradiction
          end
      end
      (** node' is already registered *)
      else if Node.equal node (find t node0') then
        (** if node is the representant of node' then we have nothing to do *)
        Debug.dprintf0 debug_few
          "[Egraph] @[    already equal@]"
      else
        (** merge node and node0' *)
        add_pending_merge t node0 node0'
    end

  let set_sem_pending t node0 thterm =
    let node0' = ThTerm.node thterm in
    set_semvalue_pending t node0 node0'

  let set_value_pending t node0 nodevalue =
    let node0' = Value.node nodevalue in
    set_semvalue_pending t node0 node0'

  let set_dom_pending (type a) t (dom : a DomKind.t) node0 new_v =
    Debug.incr stats_set_dom;
    let node = find_def t node0 in
    let domtable = (get_table_dom t.env dom) in
    let old_v = HRNode.find_opt domtable.table node in
    HRNode.set_opt domtable.table node new_v;
    let (module Dom) = VDom.get_dom dom in
    let wakeup = match Dom.wakeup_threshold with
      | None -> true
      | Some f -> f (t:> ro t) node ~old:old_v new_v
    in
    if wakeup then (
      run_events_and_update t domtable.events node;
      (* deletion of events are not taken into account *)
      ignore (run_events t (fun t e -> e t node) (Context.Ref.get domtable.reg_events))) else
        Debug.dprintf4 debug_few "[Egraph] Domain wakeup avoided for %a (%a)" Node.pp node0 Node.pp node


  let flag_choose_repr_no_value =
    Debug.register_flag
      ~desc:"Accept to use value as representative"
      "choose_repr_no_value"

  (** representative is the second returned elements *)
  let choose_repr t ((_,a) as pa) ((_,b) as pb) =
    let heuristic () =
      if Shuffle.is_shuffle () then
        Shuffle.shuffle2 (pa,pb)
      else
        let ra = HNode.find_def t.rang ~def:0 a in
        let rb = HNode.find_def t.rang ~def:0 b in
        if ra = rb then begin
          HNode.set t.rang a (ra+1);
          (pb,pa)
        end else
        if ra < rb then (pa,pb)
        else (pb,pa)
    in
    if Debug.test_noflag flag_choose_repr_no_value then
      let va = Nodes.Only_for_solver.is_value a in
      let vb = Nodes.Only_for_solver.is_value b in
      if va && not vb then (pa,pb)
      else if va && not vb then (pb,pa)
      else heuristic ()
    else
      heuristic ()

  let merge_dom_pending (type a) t (dom : a DomKind.t) node1_0 node2_0 inv =
    let node1 = find t node1_0 in
    let node2  = find t node2_0  in
    let domtable = (get_table_dom t.env dom) in
    let old_other_s = HRNode.find_opt domtable.table node1 in
    let old_repr_s = HRNode.find_opt domtable.table node2 in
    let (module Dom) = VDom.get_dom dom in
    Debug.dprintf15 debug_few
      "[Egraph] @[merge dom %a (%a(%a),%a)@ %s@ (%a(%a),%a)@]"
      DomKind.pp Dom.key
      Node.pp node1 Node.pp node1_0
      (Format.opt Dom.pp) old_other_s
      (if inv then "<-" else "->")
      Node.pp node2 Node.pp node2_0
      (Format.opt Dom.pp) old_repr_s;
    match old_other_s, old_repr_s with
    | None, None   -> ()
    | _ ->
      Dom.merge t
        (old_other_s,node1_0)
        (old_repr_s,node2_0)
        inv

  (** Return if the merging of the domain is already done *)
  let merge_dom ~additional_round t node1_0 node2_0 inv =
    let node1 = find t node1_0 in
    let node2  = find t node2_0  in
    let dom_done = ref true in
    let iteri (type a) (dom : a DomKind.t) (domtable : (ro t,a) domtable) =
      let s1 = HRNode.find_opt domtable.table node1 in
      let s2  = HRNode.find_opt domtable.table node2 in
      let (module Dom) = VDom.get_dom dom in
      if not (Dom.merged s1 s2)
      then begin
        dom_done := false;
        if additional_round then
          Debug.dprintf10 debug_few "[Egraph.few] Domain %a not merged between %a and %a, found (%a) and (%a)"
            DomKind.pp dom Node.pp node1 Node.pp node2 (Fmt.option Dom.pp) s1 (Fmt.option Dom.pp) s2;
        Queue.push (SetMergeDomNode(dom,node1_0,node2_0,inv))
            t.todo_merge_dom
      end
    in
    VDomTable.iteri {VDomTable.iteri} t.env.dom;
    (** run event merge*)
    begin if not additional_round then
      let repr,other = if inv then node1_0,node2_0 else node2_0,node1_0 in
      List.iter (fun (f:event_merge) -> f t ~repr other)
       t.env.event_before_merge
    end;
    !dom_done

  let merge_values t node0 node0' =
    let node  = find t node0 in
    let node' = find t node0'  in
    let old_s   = HNode.find_opt t.env.values node in
    let old_s'  = HNode.find_opt t.env.values node' in
    Debug.dprintf12 debug
      "[Egraph] @[merge value (%a(%a),%a)@ and (%a(%a),%a)@]"
      Node.pp node Node.pp node0
      (Format.opt Value.pp) old_s
      Node.pp node' Node.pp node0'
      (Format.opt Value.pp) old_s';
    match old_s, old_s' with
    | None, None   -> ()
    | Some v, None ->
      set_value_direct t node0' v
    | None, Some v' ->
      set_value_direct t node0  v'
    | Some v, Some v' ->
      if Value.equal v v'
      then
        (* already same value. Does that really happen? *)
        ()
      else begin
        Debug.dprintf8 print_contradiction
          "[Egraph] %a and %a are merged with conflicting values %a and %a"
          Node.pp node0 Node.pp node0' Value.pp v Value.pp v';
        raise Contradiction
      end

  let check_mergeable_values t node node' =
    let old_s   = HNode.find_opt t.env.values node  in
    let old_s'  = HNode.find_opt t.env.values node' in
    match old_s, old_s' with
    | None, None
    | Some _, None
    | None, Some _ -> ()
    | Some v, Some v' ->
      if Value.equal v v'
      then
        (* already same value. Does that really happen? *)
        ()
      else begin
        Debug.dprintf8 print_contradiction
          "[Egraph] %a and %a are merged with conflicting values %a and %a"
          Node.pp node Node.pp node' Value.pp v Value.pp v';
        raise Contradiction
      end

  let finalize_merge t node1_0 node2_0 inv =
    let node1 = find t node1_0 in
    let node2  = find t node2_0  in
    let other_node0,other_node,repr_node0,repr_node =
      if inv
      then node2_0,node2, node1_0, node1
      else node1_0, node1, node2_0, node2 in
    merge_values t node1_0 node2_0;
    HNode.set t.env.repr other_node repr_node;
    Trace.message "finalize_merge" ~data:(fun () ->
        [("other", `String (Format.sprintf "%a" Node.pp other_node));
         ("repr", `String (Format.sprintf "%a" Node.pp repr_node))]);
    Debug.dprintf8 debug_few "[Egraph.few] merge %a(%a) -> %a(%a)"
      Node.pp other_node Node.pp other_node0
      Node.pp repr_node Node.pp repr_node0;

    let other_event = HNode.find_opt t.env.event_repr other_node in

    (** move node events *)
    begin match other_event with
      | None -> ()
      | Some other_event ->
          let other_event =
            (run_events t (fun t e -> e t other_node)) other_event
          in    
          HNode.add_change (fun x -> x) Bag.concat t.env.event_repr repr_node other_event
    end;

    (** move dom events  *)
    let iteri (type a) (_ : a DomKind.t) (domtable: (ro t,a) domtable) =
      match HNode.find_opt domtable.events other_node with
      | None -> ()
      | Some other_events ->
          HNode.add_change (fun x -> x) Bag.concat domtable.events repr_node other_events
    in
    VDomTable.iteri {VDomTable.iteri} t.env.dom;
    (** move value events by valuekind  *)
    let iteri _ (table: _ valuetable) =
          match HNode.find_opt table.events other_node with
            | None -> ()
            | Some other_events ->
              HNode.add_change (fun x -> x) Bag.concat table.events repr_node other_events
    in
    ValueTable.iteri {iteri} t.env.value;
    begin
      let other_event = HNode.find_opt t.env.event_value other_node in

      (** move value events *)
      begin match other_event with
        | None -> ()
        | Some other_event ->
            HNode.add_change (fun x -> x) Bag.concat t.env.event_value repr_node other_event
      end;
    end;
    (** wakeup the daemons *)
    t.env.event_any_repr <-
      run_events t (fun t e -> e t other_node) t.env.event_any_repr;
    List.iter (fun (f:event_merge) -> f t ~repr:repr_node other_node) t.env.event_after_merge

  let set_todo_delayed_merge t new_val =
      match t.todo_delayed_merge with
      | Some (n1,n2,inv) -> finalize_merge t n1 n2 inv;
      | _ -> ();
        t.todo_delayed_merge <- new_val

  let continue_merge t node1_0 node2_0 inv  =
    let dom_not_done = not (merge_dom ~additional_round:false t node1_0 node2_0 inv) in
    if dom_not_done
    then begin
      Debug.dprintf4 debug "[Egraph] @[merge %a %a dom not done@]"
        Node.pp node1_0 Node.pp node2_0;
        set_todo_delayed_merge t (Some (node1_0,node2_0,inv))
    end
    else
      finalize_merge t node1_0 node2_0 inv

  (** merge two pending actions *)
  let start_merge t node1_0 node2_0 =
    let node1 = find t node1_0 in
    let node2 = find t node2_0 in
    if not (Node.equal node1 node2) then begin
      check_mergeable_values t node1 node2;
      let ((other_node0,_),(repr,_)) =
        choose_repr t.env (node1_0,node1) (node2_0,node2) in
      let inv = not (Node.equal node1_0 other_node0) in
      Trace.message "start_merge" ~data:(fun () ->
          [("other", `String (Format.sprintf "%a" Node.pp other_node0));
           ("repr", `String (Format.sprintf "%a" Node.pp repr))]);
      continue_merge t node1_0 node2_0 inv
    end

  (** {2 Internal scheduler} *)

  (**
     - Set dom, set value are done immediately
     - daemon immediate
     - merge domain
     - end merging (the class are really merged)
     - start merging (+ merge value)
     - daemon not immediate
  *)

  let step =
    let counter = ref 0 in
    Trace.counter_int "step" !counter;
    fun () ->
      begin
        incr counter;
        Trace.counter_int "step" !counter;
      end

  let rec do_pending_daemon delayed (Events.Wait.DaemonKey (dem,runable,opt_thterm)) =
    let (module Dem) = Wait.get_dem dem in
    let go () = Dem.run delayed runable in
    match opt_thterm with
    | None ->
      go ()
    | Some thterm ->
      let pp fmt th = Node.pp fmt (ThTerm.node th) in
      Debug.dprintf4 debug_few "[Egraph] run for %a which is %a:" pp thterm ThTerm.pp thterm;
      let data () = [("name", (`String (Format.sprintf "%a" ThTerm.pp thterm)))] in
      Trace.with_span ~__FILE__ ~__LINE__ "do_pending_daemon" ~data:data
      @@ fun _sp ->
      go ()

  and do_pending t =
    draw_graph t.env;
    step();
    let (let*) = Queue.(let*) in
    let take_opt = Queue.take_opt in
    let* action = take_opt t.todo_immediate_dem in
    match action with
    | Some RunDem att ->
      Debug.dprintf0 debug "[Egraph] @[do_pending RunDem immediate@]";
      do_pending_daemon t att;
      do_pending t
    | None ->
      let* action = take_opt t.todo_merge_dom in
      match action with
      | Some SetMergeDomNode(dom,node1,node2,inv) ->
        Debug.dprintf6 debug "[Egraph] @[do_pending SetDomNode %a %a %a@]"
          DomKind.pp dom Node.pp node1 Node.pp node2;
        merge_dom_pending t dom node1 node2 inv;
        do_pending t
      | None ->
        match t.todo_delayed_merge with
        | Some(node1_0,node2_0,inv) ->
          if merge_dom ~additional_round:true t node1_0 node2_0 inv 
          then begin
            t.todo_delayed_merge <- None;
            finalize_merge t node1_0 node2_0 inv
          end
          else begin
          Debug.dprintf4 debug "The domains of %a and %a needs another round of merging" Node.pp node1_0 Node.pp node2_0;
          if not (!Private.get_multiple_merging_round t) then
            raise DomainMergingFixpointNotReachedInOneTurn
          end;
          do_pending t
        | None ->
          let* action = take_opt t.todo_merge in
          match action with
          | Some Merge (node1,node2) ->
            Debug.dprintf4 debug "[Egraph] @[do_pending Merge %a %a@]"
              Node.pp node1 Node.pp node2;
            start_merge t node1 node2;
            do_pending t
          | None ->
            let* action = take_opt t.todo_ext_action in
            match action with
            | None -> Debug.dprintf0 debug "[Egraph] Nothing to do"
            | Some ExtDem att ->
              begin
                Debug.dprintf0 debug "[Egraph] @[do_pending RunDem@]";
                let store_ext_action = Queue.create "store_external_actions" in
                Queue.transfer t.todo_ext_action store_ext_action;
                do_pending_daemon t att;
                Queue.transfer store_ext_action t.todo_ext_action;
                do_pending t
              end

  and flush_internal d =
    Debug.dprintf0 debug "[Egraph] @[flush delayed@]";
    try
      if not (Queue.is_empty d.todo_ext_action) then
        let saved_ext_action = Queue.create "saved_external_actions" in
        Queue.transfer d.todo_ext_action saved_ext_action;
        do_pending d;
        assert (nothing_todo d);
        Queue.transfer saved_ext_action d.todo_ext_action;
      else begin
        do_pending d;
        assert (nothing_todo d);
      end;
      Debug.dprintf0 debug "[Egraph] @[flush delayed end@]"
    with e when Debug.test_flag debug &&
                not (Printexc.backtrace_status ()) ->
      raise e

  (** {2 API} *)

  let merge t node1_0 node2_0 =
    if not (Node.equal
              (find t node1_0)
              (find t node2_0)) then
      add_pending_merge t node1_0 node2_0

  let set_thterm  d node thterm =
    Debug.dprintf4 debug "[Egraph] @[add_pending_set_thterm for %a and %a@]"
      Node.pp node ThTerm.pp thterm;
    set_sem_pending d node thterm

  let set_value d node nodevalue =
    Debug.dprintf4 debug_few
      "[Egraph] @[set_value for %a with %a@]"
      Node.pp node Value.pp nodevalue;
    set_value_pending d node nodevalue

  let set_dom d dom node v =
    Trace.message "set_dom" ~data:(fun () ->
        let s_node = Format.sprintf "%a" Node.pp node in
        let s_dom = Format.sprintf "%a" DomKind.pp dom in
        let s_val = Format.sprintf "%a" (print_dom dom) v in
        [("dom", `String s_dom);
         ("node", `String s_node);
         ("val", `String s_val)]);
    Debug.dprintf6 debug_few
      "[Egraph] @[set_dom %a for %a with %a@]"
      DomKind.pp dom
      Node.pp node (print_dom dom) v;
    set_dom_pending d dom node (Some v)

  let unset_dom d dom node =
    Trace.message "unset_dom"
      ~data:(fun () ->
        let s_node = Format.sprintf "%a" Node.pp node in
        [("node", (`String s_node))]);
    Debug.dprintf2 debug
      "[Egraph] @[unset_dom for %a@]"
      Node.pp node;
    set_dom_pending d dom node None

  let register_decision t chogen =
    t.sched_decision chogen

  (* let register_delayed_event t dem v =
   *   t.sched_daemon (Events.Wait.DaemonKey(dem,v)) *)

  let contradiction () =
    raise Contradiction

  (** {2 API for attaching event} *)

  let attach_before_merge t (f : rw delayed_t -> repr:Node.t -> Node.t -> unit) =
    t.env.event_before_merge <- f::t.env.event_before_merge

  let attach_after_merge t (f : rw delayed_t -> repr:Node.t -> Node.t -> unit) =
    t.env.event_after_merge <- f::t.env.event_after_merge


  let attach_dom (type a) t ?(direct=true) node (dom : a DomKind.t) f =
    let node = find_def t node in
    let domtable = get_table_dom t.env dom in
    let add () = HNode.add_change Bag.elt Bag.add domtable.events node f in
    if direct then
      (match get_dom t dom node with
      | None -> add ()
      | Some _ ->
        if run_event t (fun t f -> f t node) f then add ())
      else
        add ()


  let attach_value (type a b) t ?(direct=true) node (value : (a,b) ValueKind.t) (f: ro t -> Node.t -> b -> Events.enqueue) =
    let node = find_def t node in
    match HNode.find_opt t.env.values node with
    | None ->
      let valuetable = get_table_value t.env value in
      HNode.add_change Bag.elt Bag.add valuetable.events node f
    | Some v ->
      if direct then
        match Nodes.Value.kind v with
        | Value (value',v) ->
          match ValueKind.Eq.eq_type value value' with
          | Neq -> (* Already registered with different value *)
            Debug.dprintf0 debug_few "[Egraph] Strange value wait with different value than already set";
          | Eq ->
            ignore (run_events t (fun t f -> f t node v) (Bag.elt f))

  let attach_repr t node f =
    let node = find_def t node in
    HNode.add_change Bag.elt Bag.add t.env.event_repr node f

  let attach_any_repr t f =
    t.env.event_any_repr <- Bag.add f t.env.event_any_repr

  let attach_any_value t ?(direct=true) node f =
    let node = find_def t node in
    match HNode.find_opt t.env.values node with
    | None ->
      let node = find_def t node in
       HNode.add_change Bag.elt Bag.add t.env.event_value node f
    | Some v ->
      if direct then
        ignore (run_event t (fun t f -> f t node v) f)

  let attach_reg_node t node f =
    match find t node with
    | node -> (** already registered *)
      ignore (run_event t (fun t e -> e t node) f)
    | exception NotRegistered ->
        HNode.add_change Bag.elt Bag.add t.env.event_reg node f
  let attach_reg_any t f =
    (* todo already registered ? *)
    t.env.event_any_reg <- Bag.add f t.env.event_any_reg

  let attach_reg_sem (type a) t (sem : (a,_) ThTermKind.t) f =
    let reg_events = get_table_sem t.env sem in
    let reg_events = Bag.add f reg_events in
    VSemTable.set t.env.event_sem sem reg_events

  let attach_reg_value (type a) t (value : (a,_) ValueKind.t) f =
    let table = get_table_value t.env value in
    Context.Ref.set table.reg_events (Bag.add f (Context.Ref.get table.reg_events))

  let attach_any_dom (type a) t (dom : a DomKind.t) f =
    let dom_table = get_table_dom t.env dom in
    Context.Ref.set dom_table.reg_events (Bag.add f (Context.Ref.get dom_table.reg_events))


end

include Delayed

module Backtrackable = struct

  type delayed = rw t
  type t = Hidden.hidden

  let new_t context =
    (* todo better error message *)
    assert (check_initialization ());
    let dom =
      let init _ = 
    { table = HRNode.create context;
      events = HNode.create context;
      reg_events = Context.Ref.create context Bag.empty }
  in
  VDomTable.create {init}
    in
    let rec t = {
      repr = HNode.create context;
      rang = HNode.create context;
      event_repr = HNode.create context;
      event_any_repr = Bag.empty;
      event_value = HNode.create context;
      event_reg = HNode.create context;
      event_any_reg = Bag.empty;
      event_before_merge = [];
      event_after_merge = [];
      dom ;
      event_sem = VSemTable.create { init = (fun _ -> Bag.empty) };
      value = ValueTable.create { init =  (fun _ -> 
        { events = HNode.create context;
          reg_events = Context.Ref.create context Bag.empty;
        })};
      values = HNode.create context;
      envs = Env.Saved.ArrayH.create { init = (fun k -> Env.Saved.init k) };
      unsaved_envs = Env.Unsaved.ArrayH.create { init = (fun k -> Env.Unsaved.init context k) };
      delayed;
      history = Hidden.create context;
      unknown = true;
      node_info_printer = [];
    }
    and delayed =  { env = t;
           todo_immediate_dem = Queue.create "immediate";
           todo_merge_dom = Queue.create "merge_dom";
           todo_delayed_merge = None;
           todo_merge = Queue.create "merge";
           todo_ext_action = Queue.create "external_action";
                     sched_daemon = (fun _ -> ()) ;
                     sched_decision = (fun _ -> ());
         } in
    Hidden.hide t

  let set_sched ~sched_daemon ~sched_decision t =
    let t = Hidden.rw t in
    t.delayed.sched_daemon <- sched_daemon;
    t.delayed.sched_decision <- sched_decision

  let get_delayed t = (Hidden.rw t).delayed

  let context t =
    let t = Hidden.ro t in
    Hidden.creator t.history

  let has_nothing_todo t =
    let d = (Hidden.rw t).delayed in
    Def.nothing_todo d
  
  let delayed_stop t =
    assert (has_nothing_todo t)

  let flush t =
    let d = (Hidden.rw t).delayed in
    Delayed.do_pending d;
    assert (Def.nothing_todo d)

  let run_daemon d dem =
    Queue.push (ExtDem dem) d.todo_ext_action

  let is_registered t node =
    let t = Hidden.rw t in
    T.is_registered t node

  let is_equal t node1 node2 =
    let t = Hidden.rw t in
    let node1,node2 = Shuffle.shuffle2 (node1,node2) in
    Debug.dprintf4 debug "[Egraph] @[is_equal %a %a@]"
      Node.pp node1 Node.pp node2;
    draw_graph t;
    T.is_equal t node1 node2

  let find t node =
    let t = Hidden.rw t in
    T.find t node

  let get_dom t dom node =
    let t = Hidden.rw t in
    T.get_dom t dom node

  let get_value t node =
    let t = Hidden.rw t in
    T.get_value t node

  let get_env t env =
    let t = Hidden.rw t in
    T.get_env t env

  let set_env t env v =
    let t = Hidden.rw t in
    T.set_env t env v

  let get_unsaved_env t env =
    let t = Hidden.rw t in
    T.get_unsaved_env t env

  let is_repr t node =
    let t = Hidden.rw t in
    T.is_repr t node

  let find_def t node =
    let t = Hidden.rw t in
    T.find_def t node

  let draw_graph ?force t = let t = Hidden.ro t in draw_graph ?force t
  let output_graph s t = let t = Hidden.ro t in output_graph s t

  let get_unknown t =
    let t = Hidden.ro t in
    t.unknown
end


let () = Exn_printer.register (fun fmt exn ->
    match exn with
    | UninitializedEnv env ->
      Format.fprintf fmt "The environnement of %s is not initialized." env
    | exn -> raise exn
  )

type theory = rw t -> unit

let theories = ref []
let add_default_theory th = theories := th::!theories
let default_theories () = !theories


  type wt = rw t

  type rt = ro t

let ro x = (x:> ro t)

let set_unknown d b = d.env.unknown <- b

let register_node_info_printer t f = t.env.node_info_printer <- f::t.env.node_info_printer
