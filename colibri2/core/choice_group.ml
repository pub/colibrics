(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Base
module HThTerm = Datastructure.Hashtbl2 (Nodes.ThTerm)

let debug = Debug.register_info_flag ~desc:"Group of decisions" "choice_group"

type t = {
  activated : bool Context.Ref.t;
  terms : Nodes.ThTerm.t Context.Queue.t;
}

and choosable =
  | Choosable
  | Init
  | Wait of {
      choices : Egraph.choice list;
      terms_added_to_group : Nodes.ThTerm.t list;
    }

let pp_choosable fmt = function
  | Choosable -> Fmt.pf fmt "!"
  | Wait _ -> Fmt.pf fmt "?"
  | Init -> Fmt.pf fmt "??"

let state : choosable HThTerm.t =
  HThTerm.create pp_choosable "Choice_group.cells" (fun _ -> Init)

let register_decision d thterm choice =
  match HThTerm.find state d thterm with
  | Choosable -> Egraph.register_decision d choice
  | Init ->
      (* Some terms are not correctly attached, so currently with consider this case as choosable *)
      if false then
        HThTerm.set state d thterm
          (Wait { choices = [ choice ]; terms_added_to_group = [] })
      else (
        HThTerm.set state d thterm Choosable;
        Egraph.register_decision d choice)
  | Wait l ->
      HThTerm.set state d thterm
        (Wait
           {
             choices = choice :: l.choices;
             terms_added_to_group = l.terms_added_to_group;
           })

let empty_wait = Wait { choices = []; terms_added_to_group = [] }

let add_to_group d thterm group =
  match HThTerm.find state d thterm with
  | Choosable -> ()
  | Init ->
      Context.Queue.enqueue group.terms thterm;
      HThTerm.set state d thterm empty_wait
  | Wait _ -> Context.Queue.enqueue group.terms thterm

let create d =
  (* Fmt.epr "Create_to_group@."; *)
  {
    activated = Context.Ref.create (Egraph.context d) false;
    terms = Context.Queue.create (Egraph.context d);
  }

let rec make_choosable d thterm =
  match HThTerm.find state d thterm with
  | Choosable -> ()
  | Init -> HThTerm.set state d thterm Choosable
  | Wait l ->
      if not (List.is_empty l.choices) then
        Debug.dprintf2 debug "activate choice for %a" Nodes.Node.pp
          (Nodes.ThTerm.node thterm);
      HThTerm.set state d thterm Choosable;
      List.iter l.choices ~f:(Egraph.register_decision d);
      List.iter l.terms_added_to_group ~f:(make_choosable d)

let activate d t =
  (* Fmt.epr "Activate group@."; *)
  if not (Context.Ref.get t.activated) then (
    Context.Ref.set t.activated true;
    Context.Queue.iter (make_choosable d) t.terms)

let add_to_group_of d thterm thterm' =
  let add_group () =
    match HThTerm.find state d thterm' with
    | Choosable -> make_choosable d thterm
    | Init ->
        HThTerm.set state d thterm'
          (Wait { terms_added_to_group = [ thterm ]; choices = [] })
    | Wait l ->
        HThTerm.set state d thterm'
          (Wait
             {
               terms_added_to_group = thterm :: l.terms_added_to_group;
               choices = l.choices;
             })
  in

  match HThTerm.find state d thterm with
  | Choosable -> ()
  | Init -> add_group ()
  | Wait _ -> add_group ()
