(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Nodes

let debug =
  Debug.register_info_flag "demon_loc"
    ~desc:"Show where demon have been attached"

type ('a, 'b) generic_wait = {
  attach : 's. 's Egraph.t -> 'a -> (Egraph.rt -> Events.enqueue) -> unit;
  has_value : 's. 's Egraph.t -> 'a -> bool;
  get : 's. 's Egraph.t -> 'a -> 'b option;
}

module type Attach = sig
  type ('a, 'b, 'filter_arg) arg
  type 'a env
  type result

  val attach_dom :
    ( ?thterm:Nodes.ThTerm.t ->
      ?direct:bool ->
      Node.t ->
      'a DomKind.t ->
      (Node.t -> result) env ->
      unit,
      'b,
      Node.t )
    arg

  val attach_any_dom :
    ( ?thterm:Nodes.ThTerm.t -> 'a DomKind.t -> (Node.t -> result) env -> unit,
      'b,
      Node.t )
    arg

  val attach_value :
    ( ?thterm:Nodes.ThTerm.t ->
      ?direct:bool ->
      Node.t ->
      ('a, 'b) ValueKind.t ->
      (Node.t -> 'b -> result) env ->
      unit,
      'r,
      Node.t )
    arg

  val attach_any_value :
    ( ?thterm:Nodes.ThTerm.t ->
      ?direct:bool ->
      Node.t ->
      (Node.t -> Value.t -> result) env ->
      unit,
      'b,
      Node.t )
    arg

  val attach_reg_any :
    (?thterm:Nodes.ThTerm.t -> (Node.t -> result) env -> unit, 'b, Node.t) arg

  val attach_reg_node :
    ( ?thterm:Nodes.ThTerm.t -> Node.t -> (Node.t -> result) env -> unit,
      'b,
      Node.t )
    arg

  val attach_reg_sem :
    ( ?thterm:Nodes.ThTerm.t ->
      ('a, 'b) ThTermKind.t ->
      ('b -> result) env ->
      unit,
      'r,
      'b )
    arg

  val attach_reg_value :
    ( ?thterm:Nodes.ThTerm.t ->
      ('a, 'b) ValueKind.t ->
      ('b -> result) env ->
      unit,
      'r,
      'b )
    arg

  val attach_repr :
    ( ?thterm:Nodes.ThTerm.t -> Node.t -> (Node.t -> result) env -> unit,
      'b,
      Node.t )
    arg

  val attach_any_repr :
    (?thterm:Nodes.ThTerm.t -> (Node.t -> result) env -> unit, 'b, Node.t) arg
end

module type Simple = sig
  include
    Attach
      with type ('a, 'b, 'filter_arg) arg :=
        ?once:unit ->
        ?filter:(Egraph.rt -> 'filter_arg -> bool) ->
        'b Egraph.t ->
        'a
       and type 'a env := Egraph.wt -> 'a
       and type result := unit

  val schedule_immediately :
    _ Egraph.t -> ?thterm:Nodes.ThTerm.t -> (Egraph.wt -> unit) -> unit
end

module Simple = struct
  module Make (Info : sig
    val name : string
    val delay : Events.delay
  end) =
  struct
    module D = struct
      type runable = Egraph.wt -> unit

      let print_runable = Fmt.nop
      let delay = Info.delay

      let key =
        Events.Dem.create
          (module struct
            type t = Egraph.wt -> unit

            let name = Info.name
          end)

      let run d f = f d
    end

    let () = Egraph.Wait.register_dem (module D)

    let if_once once f thterm =
      if once then Events.EnqLast (D.key, f, thterm)
      else Events.EnqRun (D.key, f, thterm)

    let wrap_node ?once ?filter thterm f =
      let once = Option.is_some once in
      match filter with
      | None -> fun _ n -> Events.EnqRun (D.key, (fun d -> f d n), thterm)
      | Some filter ->
          fun d n ->
            if filter d n then if_once once (fun d -> f d n) thterm
            else Events.EnqAlready

    let wrap_node_and_other ?once ?filter thterm f =
      let once = Option.is_some once in
      match filter with
      | None -> fun _ n v -> Events.EnqRun (D.key, (fun d -> f d n v), thterm)
      | Some filter ->
          fun d n v ->
            if filter d n then if_once once (fun d -> f d n v) thterm
            else Events.EnqAlready

    let wrap_other ?once ?filter thterm f =
      let once = Option.is_some once in
      match filter with
      | None -> fun _ v -> Events.EnqRun (D.key, (fun d -> f d v), thterm)
      | Some filter ->
          fun d v ->
            if filter d v then if_once once (fun d -> f d v) thterm
            else Events.EnqAlready

    let attach_dom ?once ?filter d ?thterm ?direct n dom f =
      Egraph.attach_dom d ?direct n dom (wrap_node ?once ?filter thterm f)

    let attach_any_dom ?once ?filter d ?thterm dom f =
      Egraph.attach_any_dom d dom (wrap_node ?once ?filter thterm f)

    let attach_value ?once ?filter d ?thterm ?direct n v f =
      Egraph.attach_value d ?direct n v
        (wrap_node_and_other ?once ?filter thterm f)

    let attach_any_value ?once ?filter d ?thterm ?direct n f =
      Egraph.attach_any_value d ?direct n
        (wrap_node_and_other ?once ?filter thterm f)

    let attach_reg_any ?once ?filter d ?thterm f =
      Egraph.attach_reg_any d (wrap_node ?once ?filter thterm f)

    let attach_reg_node ?once ?filter d ?thterm n f =
      Egraph.attach_reg_node d n (wrap_node ?once ?filter thterm f)

    let attach_reg_sem ?once ?filter d ?thterm th f =
      Egraph.attach_reg_sem d th (wrap_other ?once ?filter thterm f)

    let attach_reg_value ?once ?filter d ?thterm th f =
      Egraph.attach_reg_value d th (wrap_other ?once ?filter thterm f)

    let attach_repr ?once ?filter d ?thterm n f =
      Egraph.attach_repr d n (wrap_node ?once ?filter thterm f)

    let attach_any_repr ?once ?filter d ?thterm f =
      Egraph.attach_any_repr d (wrap_node ?once ?filter thterm f)

    let schedule_immediately d ?thterm f =
      Egraph.new_pending_daemon d ?thterm D.key (fun d -> f d)
  end

  module FixingModel = Make (struct
    let name = "Demon.Simple.FixingModel"
    let delay = Events.FixingModel
  end)

  module LastEffort = Make (struct
    let name = "Demon.Simple.LastEffort"
    let delay = Events.LastEffort 1
  end)

  module LastEffortUncontextual = Make (struct
    let name = "Demon.Simple.LastEffortUncontextual"
    let delay = Events.LastEffortUncontextual 1
  end)

  module LastEffortLate = Make (struct
    let name = "Demon.Simple.LastEffortLate"
    let delay = Events.LastEffort 4
  end)

  module LastEffortLateUncontextual = Make (struct
    let name = "Demon.Simple.LastEffortLateUncontextual"
    let delay = Events.LastEffortUncontextual 4
  end)

  module DelayedBy1FixModel = Make (struct
    let name = "Demon.Simple.DelayedBy1FixModel"
    let delay = Events.Delayed_by_and_FixingModel 1
  end)

  module DelayedBy1 = Make (struct
    let name = "Demon.Simple.DelayedBy1"
    let delay = Events.Delayed_by 1
  end)

  include DelayedBy1
end

module WithFilter = struct
  module D = struct
    type runable = Egraph.wt -> unit

    let print_runable = Fmt.nop
    let delay = Events.Delayed_by_and_FixingModel 1

    let key =
      Events.Dem.create
        (module struct
          type t = Egraph.wt -> unit

          let name = "Demon.WithFilter"
        end)

    let run d f = f d
  end

  let () = Egraph.Wait.register_dem (module D)

  let wrap_node thterm f d n =
    match f d n with
    | None -> Events.EnqAlready
    | Some g -> Events.EnqRun (D.key, g, thterm)

  let wrap_node_and_other thterm f d n v =
    match f d n v with
    | None -> Events.EnqAlready
    | Some g -> Events.EnqRun (D.key, g, thterm)

  let wrap_other thterm f d v =
    match f d v with
    | None -> Events.EnqAlready
    | Some g -> Events.EnqRun (D.key, g, thterm)

  let attach_dom d ?thterm ?direct n dom f =
    Egraph.attach_dom d ?direct n dom (wrap_node thterm f)

  let attach_any_dom d ?thterm dom f =
    Egraph.attach_any_dom d dom (wrap_node thterm f)

  let attach_value d ?thterm ?direct n v f =
    Egraph.attach_value d ?direct n v (wrap_node_and_other thterm f)

  let attach_any_value d ?thterm ?direct n f =
    Egraph.attach_any_value d ?direct n (wrap_node_and_other thterm f)

  let attach_reg_any d ?thterm f = Egraph.attach_reg_any d (wrap_node thterm f)

  let attach_reg_node d ?thterm n f =
    Egraph.attach_reg_node d n (wrap_node thterm f)

  let attach_reg_sem d ?thterm th f =
    Egraph.attach_reg_sem d th (wrap_other thterm f)

  let attach_reg_value d ?thterm th f =
    Egraph.attach_reg_value d th (wrap_other thterm f)

  let attach_repr d ?thterm n f = Egraph.attach_repr d n (wrap_node thterm f)

  let attach_any_repr d ?thterm f =
    Egraph.attach_any_repr d (wrap_node thterm f)
end

module Key = struct
  type state = Wait | Stop

  module type S = sig
    module Key : Colibri2_popop_lib.Popop_stdlib.Datatype

    val delay : Events.delay
    val run : Egraph.wt -> Key.t -> state
    val name : string
  end

  module Register (S : S) = struct
    type state' = Wait | Scheduled | Stop [@@deriving show]

    module H = Datastructure.Hashtbl2 (S.Key)

    let h = H.create pp_state' S.name (fun _ -> Wait)

    let key =
      Events.Dem.create
        (module struct
          type t = S.Key.t

          let name = S.name
        end)

    let () =
      Egraph.Wait.register_dem
        (module struct
          let key = key
          let delay = S.delay

          let run d k =
            match H.find h d k with
            | Stop -> ()
            | Wait -> raise Impossible
            | Scheduled -> (
                H.set h d k Wait;
                match S.run d k with Wait -> () | Stop -> H.set h d k Stop)

          type runable = S.Key.t

          let print_runable = S.Key.pp
        end)

    let wrap thterm k d =
      match H.find h d k with
      | Stop -> Events.EnqStopped
      | Wait ->
          H.set h d k Scheduled;
          Events.EnqRun (key, k, thterm)
      | Scheduled -> Events.EnqAlready

    let wrap_node thterm k d _ = wrap thterm k d
    let wrap_node_and_other thterm k d _ _ = wrap thterm k d
    let wrap_other thterm k d _ = wrap thterm k d

    let attach_dom d ?thterm ?direct n dom f =
      Egraph.attach_dom d ?direct n dom (wrap_node thterm f)

    let attach_any_dom d ?thterm dom f =
      Egraph.attach_any_dom d dom (wrap_node thterm f)

    let attach_value d ?thterm ?direct n v f =
      Egraph.attach_value d ?direct n v (wrap_node_and_other thterm f)

    let attach_any_value d ?thterm ?direct v f =
      Egraph.attach_any_value d ?direct v (wrap_node_and_other thterm f)

    let attach_reg_any d ?thterm f =
      Egraph.attach_reg_any d (wrap_node thterm f)

    let attach_reg_node d ?thterm n f =
      Egraph.attach_reg_node d n (wrap_node thterm f)

    let attach_reg_sem d ?thterm th f =
      Egraph.attach_reg_sem d th (wrap_other thterm f)

    let attach_reg_value d ?thterm th f =
      Egraph.attach_reg_value d th (wrap_other thterm f)

    let attach_repr d ?thterm n f = Egraph.attach_repr d n (wrap_node thterm f)

    let attach_any_repr d ?thterm f =
      Egraph.attach_any_repr d (wrap_node thterm f)
  end
end

module MakeMonad (I : sig
  val name : string
  val delay : Events.delay
end) =
struct
  (* The idea is [type 'a monad = Egraph.t -> 'a] *)

  type nonrec unit = unit

  type 'a monad =
    | Both : 'a option monad * 'b option monad -> ('a * 'b) option monad
    | Bind : 'a option monad * ('a -> 'b) -> 'b option monad
    | Bindo : 'a option monad * ('a -> 'b option) -> 'b option monad
    | GetV : Node.t * ('b, _) ValueKind.t -> 'b option monad
    | GetD : Node.t * 'a DomKind.t * 'a option -> 'a option monad
    | GetDf :
        Node.t * (Egraph.rt -> Node.t -> 'a option) * 'a option
        -> 'a option monad
    | Exec : (Egraph.rt -> unit) * 'a monad -> 'a monad
    | GenericWait : 'a * ('a, 'b) generic_wait -> 'b option monad

  type sequence =
    | Sequence : sequence list -> sequence
    | SetV : Node.t * ('b, _) ValueKind.t * 'b option monad -> sequence
    | SetD : Node.t * 'a DomKind.t * 'a option monad -> sequence
    | UpdD :
        Node.t
        * (Egraph.rt -> Node.t -> 'a -> bool)
        * (Egraph.wt -> Node.t -> 'a -> unit)
        * 'a option monad
        -> sequence
    | SExec : (Egraph.wt -> unit) * bool option monad -> sequence

  let getv m n = GetV (n, m)
  let setv m n f = SetV (n, m, f)
  let getd ?def m n = GetD (n, m, def)
  let getdf ?def f n = GetDf (n, f, def)
  let generic_wait r a = GenericWait (a, r)
  let exec f m = SExec (f, m)
  let exec_ro f m = Exec (f, m)
  let setd m n f = SetD (n, m, f)
  let updd ?(check = fun _ _ _ -> true) m n f = UpdD (n, check, m, f)
  let ( let+ ) a f = Bind (a, f)
  let ( let* ) a f = Bindo (a, f)
  let ( && ) a b = Sequence [ a; b ]
  let ( and+ ) a b = Both (a, b)
  let nop = Sequence []

  module D = struct
    type runable' =
      | SetV : Node.t * Value.t * runable' -> runable'
      | SetD : Node.t * 'a DomKind.t * 'a * runable' -> runable'
      | UpdD :
          Node.t * (Egraph.wt -> Node.t -> 'a -> unit) * 'a * runable'
          -> runable'
      | SExec : (Egraph.wt -> unit) * runable' -> runable'
      | Nil : runable'

    type runable = runable' * Lexing.position

    let print_runable = Fmt.nop
    let delay = I.delay

    let key =
      Events.Dem.create
        (module struct
          type t = runable

          let name = I.name
        end)

    let rec run d : runable' -> unit = function
      | SetV (n, v, r) ->
          Egraph.set_value d n v;
          run d r
      | SetD (n, dom, b, r) ->
          Egraph.set_dom d dom n b;
          run d r
      | UpdD (n, upd, b, r) ->
          upd d n b;
          run d r
      | SExec (g, r) ->
          g d;
          run d r
      | Nil -> ()

    let run d (r, (loc : Lexing.position)) =
      Debug.dprintf2 debug "Attached at %a:"
        (fun fmt loc ->
          Fmt.string fmt (Base.Source_code_position.to_string loc))
        loc;
      run d r
  end

  let () = Egraph.Wait.register_dem (module D)

  let rec compute : type a. _ Egraph.t -> a monad -> a =
   fun d -> function
    | Both (a, b) -> (
        match compute d a with
        | None -> None
        | Some a -> (
            match compute d b with None -> None | Some b -> Some (a, b)))
    | Bind (a, f) -> (
        match compute d a with None -> None | Some v -> Some (f v))
    | Bindo (a, f) -> ( match compute d a with None -> None | Some v -> f v)
    | GetV (n, m) -> (
        match Egraph.get_value d n with
        | None -> None
        | Some v -> Value.value m v)
    | GetD (n, dom, Some default) ->
        Some (Option.value ~default (Egraph.get_dom d dom n))
    | GetDf (n, f, Some default) ->
        Some (Option.value ~default (f (Egraph.ro d) n))
    | GetDf (n, f, None) -> f (Egraph.ro d) n
    | GetD (n, dom, None) -> Egraph.get_dom d dom n
    | GenericWait (a, r) -> r.get d a
    | Exec (f, m) ->
        f (Egraph.ro d);
        compute d m

  let rec compute_seq d acc : _ -> D.runable' = function
    | Sequence l -> List.fold_left (compute_seq d) acc l
    | SetV (n, m, f) -> (
        let v = compute d f in
        match v with
        | None -> acc
        | Some v ->
            let v = Value.index m v in
            let check () =
              match Egraph.get_value d n with
              | None -> true
              | Some v' -> not (Value.equal v' v)
            in
            if check () then SetV (n, v, acc) else acc)
    | SetD (n, dom, f) -> (
        let v = compute d f in
        match v with None -> acc | Some v -> SetD (n, dom, v, acc))
    | UpdD (n, check, dom, f) -> (
        let v = compute d f in
        match v with
        | None -> acc
        | Some v ->
            if check (Egraph.ro d) n v then UpdD (n, dom, v, acc) else acc)
    | SExec (g, f) -> (
        let v = compute d f in
        match v with None | Some false -> acc | Some true -> SExec (g, acc))

  type footprint = {
    doms : Node.S.t DomKind.Array.t;
    sems : Node.S.t ValueKind.Array.t;
    mutable generic : ((Egraph.rt -> Events.enqueue) -> unit) list;
  }

  let rec attach_aux : type a. _ Egraph.t -> footprint -> a monad -> bool =
   fun d ft -> function
    | Both (a, b) ->
        let b1 = attach_aux d ft a in
        let b2 = attach_aux d ft b in
        Stdlib.(b1 && b2)
    | Bind (a, _) -> attach_aux d ft a
    | Bindo (a, _) -> attach_aux d ft a
    | GetV (n, m) -> (
        match Egraph.get_value d n with
        | None ->
            let s = ValueKind.Array.get ft.sems m in
            ValueKind.Array.set ft.sems m (Node.S.add n s);
            false
        | Some v -> Option.is_some (Value.value m v))
    | GetD (n, dom, default) -> (
        let s = DomKind.Array.get ft.doms dom in
        DomKind.Array.set ft.doms dom (Node.S.add n s);
        match (Egraph.get_dom d dom n, default) with
        | None, None -> false
        | _ -> true)
    | GetDf (n, f, default) -> (
        match (f (Egraph.ro d) n, default) with
        | None, None -> false
        | _ -> true)
    | GenericWait (a, r) ->
        ft.generic <- r.attach d a :: ft.generic;
        r.has_value d a
    | Exec (_, m) -> attach_aux d ft m

  let rec attach_seq compute d = function
    | Sequence l ->
        List.fold_left (fun b e -> attach_seq compute d e || b) false l
    | SetV (_, _, f) -> attach_aux compute d f
    | SetD (_, _, f) -> attach_aux compute d f
    | UpdD (_, _, _, f) -> attach_aux compute d f
    | SExec (_, f) -> attach_aux compute d f

  let attach loc d ?thterm seq =
    let compute d =
      match compute_seq d D.Nil seq with
      | Nil -> Events.EnqAlready
      | r -> Events.EnqRun (D.key, (r, loc), thterm)
    in
    let ft =
      {
        doms = DomKind.Array.create { init = (fun _ -> Node.S.empty) };
        sems = ValueKind.Array.create { init = (fun _ -> Node.S.empty) };
        generic = [];
      }
    in
    let b = attach_seq d ft seq in
    let iteri dom s =
      Node.S.iter
        (fun n ->
          Egraph.attach_dom d ~direct:false n dom (fun d _ -> compute d))
        s
    in
    DomKind.Array.iteri { iteri } ft.doms;
    List.iter (fun f -> f compute) ft.generic;
    let iteri m s =
      Node.S.iter
        (fun n ->
          Egraph.attach_value d ~direct:false n m (fun d _ _ -> compute d))
        s
    in
    ValueKind.Array.iteri { iteri } ft.sems;
    if b then
      match compute_seq d D.Nil seq with
      | Nil -> ()
      | r -> Egraph.new_pending_daemon d D.key (r, loc)
end

module type Monad = sig
  type 'a monad
  type sequence

  val nop : sequence
  val getd : ?def:'a -> 'a DomKind.t -> Node.t -> 'a option monad

  val getdf :
    ?def:'a -> (Egraph.rt -> Node.t -> 'a option) -> Node.t -> 'a option monad

  val setd : 'a DomKind.t -> Node.t -> 'a option monad -> sequence
  val exec : (Egraph.wt -> unit) -> bool option monad -> sequence
  val generic_wait : ('a, 'b) generic_wait -> 'a -> 'b option monad

  val updd :
    ?check:(Egraph.rt -> Node.t -> 'a -> bool) ->
    (Egraph.wt -> Node.t -> 'a -> unit) ->
    Node.t ->
    'a option monad ->
    sequence

  val getv : ('a, _) ValueKind.t -> Node.t -> 'a option monad
  val setv : ('a, _) ValueKind.t -> Node.t -> 'a option monad -> sequence
  val exec_ro : (Egraph.rt -> unit) -> 'a monad -> 'a monad
  val ( let+ ) : 'a option monad -> ('a -> 'c) -> 'c option monad
  val ( let* ) : 'a option monad -> ('a -> 'c option) -> 'c option monad
  val ( and+ ) : 'a option monad -> 'b option monad -> ('a * 'b) option monad
  val ( && ) : sequence -> sequence -> sequence

  val attach :
    Lexing.position -> _ Egraph.t -> ?thterm:Nodes.ThTerm.t -> sequence -> unit
end

module Monad = MakeMonad (struct
  let name = "Demon.Monad"
  let delay = Events.Delayed_by 1
end)

module MonadAlsoInterp = MakeMonad (struct
  let name = "Demon.MonadAlsoInterp"
  let delay = Events.Delayed_by_and_FixingModel 1
end)
