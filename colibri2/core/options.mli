(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

type 'a t

val register : ?pp:'a Fmt.t -> string -> 'a Cmdliner.Term.t -> 'a t
val get : _ Egraph.t -> 'a t -> 'a
val register_flag : ?doc:string -> string -> bool t
val register_int : ?doc:string -> string -> int -> int t
(** [register_int name def] Add an int option with default [def] *)

type options

val set : options -> 'a t -> 'a -> options
val parser : unit -> options Cmdliner.Term.t
val set_egraph : _ Egraph.t -> options -> unit
val default_options : unit -> options
val get_from_options : options -> 'a t -> 'a

