(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

(** {2 An hashtable context aware } *)

module type Sig =  sig
  type 'a t
  type key

  val create : 'a Format.printer -> string -> 'a t

  val remove : 'a t -> _ Egraph.t -> key -> unit
  val set : 'a t -> _ Egraph.t -> key -> 'a -> unit
  val find : 'a t -> _ Egraph.t -> key -> 'a
  val find_opt : 'a t -> _ Egraph.t -> key -> 'a option
  val find_exn : 'a t -> _ Egraph.t -> exn -> key -> 'a
  val mem : 'a t -> _ Egraph.t -> key -> bool
  val change : ('a option -> 'a option) -> 'a t -> _ Egraph.t -> key -> unit
  val iter :  f:(key -> 'a -> unit) -> 'a t -> _ Egraph.t -> unit
  val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> _ Egraph.t -> 'b -> 'b
  val filter_map_inplace :
    (key -> 'a -> 'a option) -> 'a t -> _ Egraph.t -> unit
  val clear : 'a t -> _ Egraph.t -> unit
  val length : 'a t -> _ Egraph.t -> int
end

module Hashtbl (S:Colibri2_popop_lib.Popop_stdlib.Datatype) : Sig with type key := S.t

(** {2 An hashtable context aware, where data are automatically initialized } *)

module type Sig2 =  sig
  type 'a t
  type key

  val create: 'a Format.printer -> string -> (Context.creator -> 'a) -> 'a t

  val set : 'a t -> _ Egraph.t -> key -> 'a -> unit
  val find : 'a t -> _ Egraph.t -> key -> 'a
  val change : ('a -> 'a) -> 'a t -> _ Egraph.t -> key -> unit
end

module Hashtbl2 (S:Colibri2_popop_lib.Popop_stdlib.Datatype) : Sig2 with type key := S.t

(** {2 A table where data are initialized on the fly, and can't be modified. It
   is used with other context aware mutable data-structure as data } *)

module type Memo =  sig
  type 'a t
  type key

  val create: 'a Format.printer -> string -> (Context.creator -> key -> 'a) -> 'a t
  val find : 'a t -> _ Egraph.t -> key -> 'a
  val iter : (key -> 'a -> unit) -> 'a t -> _ Egraph.t -> unit
  val fold : (key -> 'a -> 'acc -> 'acc) -> 'a t -> _ Egraph.t -> 'acc -> 'acc
  val length : 'a t -> _ Egraph.t-> int

end

module Memo (S:Colibri2_popop_lib.Popop_stdlib.Datatype) : Memo with type key := S.t


module type Memo2 =  sig
  type ('a, 'b) t
  type key

  val create: 'a Format.printer -> string -> ('b Egraph.t -> key -> 'a) -> ('a, 'b) t
  val find : ('a, 'b) t -> 'b Egraph.t -> key -> 'a
  val iter : (key -> 'a -> unit) -> ('a, 'b) t -> 'b Egraph.t -> unit
  val fold : (key -> 'a -> 'acc -> 'acc) -> ('a, 'b) t -> 'b Egraph.t -> 'acc -> 'acc
end

module Memo2 (S:Colibri2_popop_lib.Popop_stdlib.Datatype) : Memo2 with type key := S.t

module HNode : Sig with type key := Nodes.Node.t

module Push: sig
  type 'a t

  val create: 'a Format.printer -> string -> 'a t
  val push: 'a t -> _ Egraph.t -> 'a -> unit
  val iter: f:('a -> unit) -> 'a t -> _ Egraph.t -> unit
  val fold: f:('acc -> 'a -> 'acc) ->  init:'acc -> 'a t -> _ Egraph.t -> 'acc
  val exists : f:('a -> bool) -> 'a t -> _ Egraph.t -> bool
  val length: 'a t -> _ Egraph.t -> int
  val get: 'a t -> _ Egraph.t -> int -> 'a

end

module Ref: sig
  type 'a t
  val create: 'a Fmt.t -> string -> 'a -> 'a t
  val get: 'a t -> _ Egraph.t -> 'a
  val set: 'a t -> _ Egraph.t -> 'a -> unit
  val incr: ?trace:string -> int t -> _ Egraph.t  -> unit

  val get_private: 'a t -> Egraph.Private.t -> 'a
end

module type Trie = sig
  type 'a t
  type key

  val create: 'a Format.printer -> string -> 'a t

  module List : sig
    val set : 'a t ->  _ Egraph.t ->key list -> 'a -> unit
    val find_def : default:(Context.creator -> 'a) -> 'a t ->  _ Egraph.t ->key list -> 'a
  end

  module Set : sig
    type set

    val set : 'a t -> _ Egraph.t -> set -> 'a -> unit
    val find_def : default:(Context.creator -> 'a) -> 'a t -> _ Egraph.t -> set -> 'a
  end
end

module Trie (S:Colibri2_popop_lib.Popop_stdlib.Datatype) : Trie
 with type key := S.t and type Set.set := S.S.t
