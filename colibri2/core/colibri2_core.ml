(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

module Egraph = Egraph
module Ground = Ground
module Builtin = Dolmen_std.Builtin
module Expr = Expr

module Node = struct
  include Nodes.Node
  module HC = Datastructure.HNode
end

module ThTerm = struct
  module Kind : Keys.Key2 with type ('a, 'b) t = ('a, 'b) Nodes.ThTermKind.t =
    Nodes.ThTermKind
  (** Unique keys associated to each kind of ThTerm *)

  module type S = Nodes.RegisteredThTerm

  module Register (T : sig
    include Colibri2_popop_lib.Popop_stdlib.NamedDatatype

    val ty : t -> Ground.Ty.t
  end) : S with type s = T.t =
    Nodes.RegisterThTerm (T)

  (** {3 Generic value which unit all the theory terms} *)

  include Nodes.ThTerm
end

(** Another kind of nodes are values. They are disjoint but similar to theory
    terms. The goal of the solver is to find one {!Value.t} for each expression
    of the input problem. They are registered using {!Value.Register}. *)

(** One {!Value.t} can be associated to an equivalence class and a {!Value.t} is
    associated to an uniq {!Node.t} *)
module Value = struct
  module Kind : Keys.Key2 with type ('a, 'b) t = ('a, 'b) Nodes.ValueKind.t =
    Nodes.ValueKind

  module type S = Nodes.RegisteredValue

  module Register (T : Colibri2_popop_lib.Popop_stdlib.NamedDatatype) :
    S with type s = T.t =
    Nodes.RegisterValue (T)

  (** {3 Generic value which unit all the values} *)

  include Nodes.Value
end

(** Domains are additional informations associated to each equivalence class
    inside the {!Egraph.t}. They can be registered using {!Dom.register} or when
    the domain is simple by {!Dom.Lattice} *)

module Dom = struct
  module Kind : Keys.Key with type 'a t = 'a DomKind.t = DomKind

  module type S = sig
    type t

    val merged : t option -> t option -> bool
    (** [merged d1 d2] indicates if the [d1] and [d2] are the same and doesn't
        need to be merged *)

    val merge :
      Egraph.wt -> t option * Node.t -> t option * Node.t -> bool -> unit
    (** [merge d (dom1,cl1) (dom2,cl2) inv] called when [cl1] and [cl2] are
        going to be merged in the same equivalence class. - if inv is false, cl2
        will be the new representative - if inv is true, cl1 will be the new
        representative *)

    val wakeup_threshold :
      (Egraph.rt -> Node.t -> old:t option -> t option -> bool) option
    (** Return false if the waiting daemon should not be wakeup *)

    val pp : Format.formatter -> t -> unit
    val key : t Kind.t
  end

  module type Lattice = sig
    type t

    val equal : t -> t -> bool
    val pp : Format.formatter -> t -> unit
    val key : t Kind.t

    val inter : Egraph.wt -> t -> t -> t option
    (** [inter d d1 d2] compute the intersection of [d1] and [d2] return [None]
        if it is empty. In this last case a contradiction is reported *)

    val is_singleton : Egraph.wt -> Node.t -> t -> Value.t option
    (** [is_singleton _ _ d] if [d] is restricted to a singleton return the
        corresponding value *)

    val wakeup_threshold : (Egraph.rt -> Node.t -> old:t -> t -> bool) option
    (** Return false if the waiting daemons should not be wakeup *)
  end

  module type LS = sig
    type t

    val set_dom : Egraph.wt -> Node.t -> t -> unit
    (** [set_dom d n l] Set the domain of [n] with [l] and set its value if it
        is a singleton *)

    val upd_dom : Egraph.wt -> Node.t -> t -> unit
    (** [upd_dom d n l] Same than {!set_dom} but set with the intersection of
        [l] with the current value of the domain. *)
  end

  module type DS = sig
    type t

    include S with type t := t
    include LS with type t := t
  end

  module Make (L : Lattice) : DS with type t = L.t = struct
    type t = L.t

    let pp = L.pp
    let key = L.key

    let set_dom d node v =
      Egraph.set_dom d L.key node v;
      match L.is_singleton d node v with
      | Some cst -> Egraph.set_value d node cst
      | None -> ()

    let merged i1 i2 =
      match (i1, i2) with
      | None, None -> true
      | Some i1, Some i2 -> L.equal i1 i2
      | _ -> false

    let merge d (i1, cl1) (i2, cl2) _ =
      assert (not (Egraph.is_equal d cl1 cl2));
      match (i1, cl1, i2, cl2) with
      | Some i1, _, Some i2, _ -> (
          match L.inter d i1 i2 with
          | None ->
              Colibri2_stdlib.Debug.dprintf10 Egraph.print_contradiction
                "[%a] The intersection of %a and %a is empty when\n\
                \ merging %a and %a"
                Kind.pp L.key L.pp i1 L.pp i2 Node.pp cl1 Node.pp cl2;
              Egraph.contradiction ()
          | Some i ->
              if not (L.equal i i1) then set_dom d cl1 i;
              if not (L.equal i i2) then set_dom d cl2 i)
      | Some i1, _, _, cl2 | _, cl2, Some i1, _ -> set_dom d cl2 i1
      | None, _, None, _ -> raise Impossible

    let upd_dom d n' v' =
      match Egraph.get_dom d L.key n' with
      | None -> set_dom d n' v'
      | Some old -> (
          match L.inter d old v' with
          | None ->
              Colibri2_stdlib.Debug.dprintf8 Egraph.print_contradiction
                "[%a] The intersection of %a with %a is empty when updating %a"
                Kind.pp L.key L.pp old L.pp v' Node.pp n';
              Egraph.contradiction ()
          | Some d' -> if not (L.equal old d') then set_dom d n' d')

    let wakeup_threshold =
      match L.wakeup_threshold with
      | None -> None
      | Some wakeup_threshold ->
          Some
            (fun t n ~old new_v ->
              match (old, new_v) with
              | Some old, Some new_v -> wakeup_threshold t n ~old new_v
              | _ -> true)
  end

  let register (s : (module S)) =
    let module S = (val s) in
    Egraph.register_dom (module S)

  (** [Lattice(L)] register a domain as a lattice. It returns useful function
      for setting and updating the domain *)
  module Lattice (L : Lattice) : LS with type t = L.t = struct
    module S = Make (L)

    let () = register (module S)

    include S
  end

  module type LatticeLimitRepeat = sig
    include Lattice

    val repeat_limit : _ Egraph.t -> int
  end

  module type LLRS = sig
    include LS

    val nodes_reach_repeat_limit : _ Egraph.t -> Node.t Vector.vector
  end

  module LatticeLimitRepeat (L : LatticeLimitRepeat) : LLRS with type t = L.t =
  struct
    type limits = { limits : int Node.H.t; limited : Node.t Vector.vector }

    let env =
      Env.Unsaved.create
        (module struct
          type t = limits

          let name = "Lattice limit"
        end)

    module L = struct
      include L

      let () =
        Env.Unsaved.register
          ~init:(fun context ->
            let h = { limits = Node.H.create 10; limited = Vector.create () } in
            Context.always_do_when_pushing context (fun () ->
                Node.H.clear h.limits;
                Vector.clear h.limited);
            h)
          ~pp:Fmt.nop env

      let limit_wakeup d n =
        let h = Egraph.get_unsaved_env d env in
        let times = Node.H.find_def h.limits 0 n in

        if L.repeat_limit d < times then false
        else (
          (* Pushed at the last update so that the simplex is warned *)
          if L.repeat_limit d = times then Vector.push h.limited n;
          Node.H.replace h.limits n (times + 1);
          true)

      let wakeup_threshold =
        match L.wakeup_threshold with
        | None -> Some (fun d n ~old:_ _ -> limit_wakeup d n)
        | Some f -> Some (fun d n ~old v -> limit_wakeup d n && f d n ~old v)
    end

    include Lattice (L)

    let nodes_reach_repeat_limit d =
      let h = Egraph.get_unsaved_env d env in
      h.limited
  end
end

module Interp = Interp
module Env = Env
module Datastructure = Datastructure

module type Daemon = Demon.Simple

module DaemonOnlyPropa = Demon.Simple
module DaemonFixingModel = Demon.Simple.FixingModel
module DaemonLastEffort = Demon.Simple.LastEffort
module DaemonLastEffortUncontextual = Demon.Simple.LastEffortUncontextual
module DaemonLastEffortLate = Demon.Simple.LastEffortLate

module DaemonLastEffortLateUncontextual =
  Demon.Simple.LastEffortLateUncontextual

module DaemonAlsoInterp = Demon.Simple.DelayedBy1FixModel
module DaemonWithFilter = Demon.WithFilter
module DaemonWithKey = Demon.Key

module type Monad = Demon.Monad

module MonadOnlyPropa = Demon.Monad
module MonadAlsoInterp = Demon.MonadAlsoInterp

type ('a, 'b) generic_wait = ('a, 'b) Demon.generic_wait = {
  attach : 's. 's Egraph.t -> 'a -> (Egraph.rt -> Events.enqueue) -> unit;
  has_value : 's. 's Egraph.t -> 'a -> bool;
  get : 's. 's Egraph.t -> 'a -> 'b option;
}

module Events = struct
  include Egraph
  module Dem = Events.Dem

  type delay = Events.delay =
    | Immediate
    | Delayed_by of int
    | LastEffort of int
    | LastEffortUncontextual of int
    | FixingModel
    | Delayed_by_and_FixingModel of int

  type enqueue = Events.enqueue =
    | EnqRun : 'r Dem.t * 'r * ThTerm.t option -> enqueue
        (** Schedule a daemon run *)
    | EnqLast : 'r Dem.t * 'r * ThTerm.t option -> enqueue
        (** Same as EnqRun but remove the waiting event *)
    | EnqAlready : enqueue  (** Don't run but keep the waiting event *)
    | EnqStopped : enqueue  (** Stop and don't run *)

  type daemon_key = Events.Wait.daemon_key =
    | DaemonKey : 'runable Dem.t * 'runable * ThTerm.t option -> daemon_key

  let pp_daemon_key fmt = function
    | DaemonKey (dem, run, _) ->
        Fmt.pf fmt "%a-%a" Dem.pp dem (Wait.print_dem_runable dem) run

  module type T = Egraph.Wait.Dem
  (** Basic daemons *)

  module type WithDatatype = sig
    type runable [@@deriving eq, hash]
  end

  let register :
      'a.
      ?datatype:(module WithDatatype with type runable = 'a) ->
      (module T with type runable = 'a) ->
      unit =
   fun ?datatype s -> Egraph.Wait.register_dem ?datatype s

  let get_datatype = Egraph.Wait.get_dem_datatype
  let new_pending_daemon = Egraph.new_pending_daemon
end

module Init = struct
  let add_default_theory = Egraph.add_default_theory
end

module Debug = struct
  include Colibri2_stdlib.Debug

  let decision = Egraph.print_decision
  let contradiction = Egraph.print_contradiction
end

module ForSchedulers = struct
  module Backtrackable = Egraph.Backtrackable

  let default_theories = Egraph.default_theories
  let ground_init = Ground.init
  let interp_init = Interp.init

  module Fix_model = Interp.Fix_model

  let get_event_priority = Events.Wait.get_priority
  let flush_internal = Egraph.flush_internal

  module Options = Options
end

module Choice = struct
  type choice_state = Egraph.choice_state =
    | DecNo
    | DecTodo of (Egraph.wt -> unit) list

  and t = Egraph.choice = {
    choice : Egraph.wt -> choice_state;
    prio : int;
    print_cho : unit Fmt.t;
    key : Node.t option;
  }

  let register d g dec = Choice_group.register_decision d (Ground.thterm g) dec
  let register_thterm = Choice_group.register_decision
  let register_global = Egraph.register_decision

  module Group = Choice_group
end

module Options = Options
