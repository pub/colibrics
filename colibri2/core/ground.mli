(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib
open Nodes

module All : sig
  type ty = Nodes.ty = { app : Expr.Ty.Const.t; args : ty list }

  type term = {
    app : Expr.Term.Const.t;
    tyargs : ty list;
    args : Node.t IArray.t;
    ty : ty;
  }
end

module Subst : sig
  type ty = All.ty Expr.Ty.Var.M.t [@@deriving show, hash, ord, eq]
  type t = { term : Node.t Expr.Term.Var.M.t; ty : ty }

  include Colibri2_popop_lib.Popop_stdlib.Datatype with type t := t

  val empty : t
  val distinct_union : t -> t -> t
  val map_repr : _ Egraph.t -> t -> t

  val mk :
    (Dolmen_std.Expr.term_var * Node.t) list ->
    (Dolmen_std.Expr.ty_var * All.ty) list ->
    t
end

module Ty : sig
  type t = All.ty = { app : Expr.Ty.Const.t; args : t list }

  include Colibri2_popop_lib.Popop_stdlib.Datatype with type t := t

  val convert : Subst.ty -> Expr.Ty.t -> t
  val apply : Dolmen_std.Expr.ty_cst -> t list -> t

  val prop : t
  (** The type of propositions *)

  val bool : t
  (** Alias for {!prop}. *)

  val unit : t
  (** The unit type. *)

  val base : t
  (** An arbitrary type. *)

  val int : t
  (** The type of integers *)

  val rat : t
  (** The type of rationals *)

  val real : t
  (** The type of reals. *)

  val array : t -> t -> t
  (** The type of strings *)

  val string : t
  (** The type of strings *)

  val string_reg_lang : t
  (** The type of regular language over strings. *)

  val definition : Expr.Ty.Const.t -> Expr.Ty.def

  val get_arrow : t -> (t * t) option
  (** return Some (arg,ret) if the type is an arrow *)
end

module Term : sig
  type t = All.term = {
    app : Expr.Term.Const.t;
    tyargs : Ty.t list;
    args : Node.t IArray.t;
    ty : Ty.t;
  }

  include Colibri2_popop_lib.Popop_stdlib.Datatype with type t := t
end

type s = All.term = {
  app : Expr.Term.Const.t;
  tyargs : Ty.t list;
  args : Node.t IArray.t;
  ty : Ty.t;
}

include RegisteredThTerm with type s := Term.t

val convert : ?subst:Subst.t -> _ Egraph.t -> Expr.Term.t -> Node.t
val convert_not_choosable : ?subst:Subst.t -> Expr.Term.t -> Node.t

val apply :
  _ Egraph.t -> Expr.Term.Const.t -> Ty.t list -> Node.t IArray.t -> Term.t

val apply' :
  _ Egraph.t -> Expr.Term.Const.t -> Ty.t list -> Node.t list -> Node.t

val init : Egraph.wt -> unit

val register_converter : Egraph.wt -> (Egraph.wt -> t -> unit) -> unit
(** register callback called for each new ground term registered *)

val tys : _ Egraph.t -> Node.t -> Ty.S.t
val add_ty : Egraph.wt -> Node.t -> Ty.t -> unit

module Defs : sig
  val add :
    Egraph.wt ->
    Dolmen_std.Expr.Term.Const.t ->
    Dolmen_std.Expr.Ty.Var.t list ->
    Dolmen_std.Expr.Term.Var.t list ->
    Expr.Term.t ->
    unit
  (** [add d sym tys vars def] add the definition [def] for the symbol [sym] *)

  val add_handler :
    Egraph.wt ->
    (Egraph.wt ->
    Dolmen_std.Expr.Term.Const.t ->
    Dolmen_std.Expr.Ty.Var.t list ->
    Dolmen_std.Expr.Term.Var.t list ->
    Expr.Term.t ->
    unit) ->
    unit
  (** Called every time a definition is registered *)
end

module ClosedQuantifier : sig
  type binder = Forall | Exists

  type s = {
    binder : binder;
    subst : Subst.t;
    ty_vars : Expr.Ty.Var.t list;
    term_vars : Expr.Term.Var.t list;
    body : Expr.Term.t;
  }

  include RegisteredThTerm with type s := s
end

module NotTotallyApplied : sig
  type s =
    | Lambda of {
        subst : Subst.t;
        ty_vars : Expr.Ty.Var.t list;
        term_vars : Expr.Term.Var.t list;
        body : Expr.Term.t;
        ty : Ty.t;
      }
    | Cst of { tc : Expr.Term.Const.t; ty : Ty.t }
    | App of {
        app : Node.t;
        tyargs : Ty.t list;
        args : Node.t IArray.t;
        ty : Ty.t;
      }

  include RegisteredThTerm with type s := s

  val ty : t -> Ty.t
end

val register_cst_normalization :
  Expr.Term.Const.t -> ty -> (ty list -> Node.t IArray.t -> ty -> s) -> unit

val convert_and_iter :
  ('a -> t -> unit) ->
  ('a -> ClosedQuantifier.t -> unit) ->
  ('a -> NotTotallyApplied.t -> unit) ->
  'a ->
  Subst.t ->
  Expr.Term.t ->
  Node.t
(** Iter on the new ground terms when converting, bottom_up *)

val convert_one_app :
  Subst.t ->
  'a Egraph.t ->
  Expr.Term.t ->
  Expr.Ty.t list ->
  Node.t IArray.t ->
  Expr.Ty.t ->
  Node.t

val convert_one_cst :
  Subst.t -> 'a Egraph.t -> Expr.Term.Const.t -> Expr.Ty.t -> Node.t

val convert_one_binder :
  Subst.t -> 'a Egraph.t -> Expr.binder -> Expr.Term.t -> Expr.Ty.t -> Node.t

val convert_one_app_and_iter :
  'a ->
  Expr.term ->
  Expr.ty list ->
  Node.t Colibri2_popop_lib.IArray.t ->
  Expr.ty ->
  Subst.t ->
  ('a -> t -> unit) ->
  ('a -> ClosedQuantifier.t -> unit) ->
  ('a -> NotTotallyApplied.t -> unit) ->
  Node.t

val convert_one_cst_and_iter :
  'a ->
  Dolmen_std.Expr.term_cst ->
  Expr.Ty.t ->
  Subst.t ->
  ('a -> t -> unit) ->
  ('a -> NotTotallyApplied.t -> unit) ->
  Node.t

val convert_one_binder_and_iter :
  'a ->
  Expr.binder ->
  Expr.term ->
  Expr.ty ->
  Subst.t ->
  ('a -> ClosedQuantifier.t -> unit) ->
  ('a -> NotTotallyApplied.t -> unit) ->
  Node.t

val convert_let_seq :
  Subst.t ->
  'a ->
  (Dolmen_std.Expr.Term.Var.t * Expr.Term.t) list ->
  Expr.Term.t ->
  ('a -> Subst.t -> Expr.Term.t -> Node.t) ->
  Node.t

val convert_let_par :
  Subst.t ->
  'a ->
  (Dolmen_std.Expr.Term.Var.t * Expr.Term.t) list ->
  Expr.Term.t ->
  ('a -> Subst.t -> Expr.Term.t -> Node.t) ->
  Node.t
