(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

module K = Keys.Make_key (struct end)

module Data = struct
  type 'a data = { cmd : 'a Cmdliner.Term.t; id : 'a K.t; pp : 'a Fmt.t }

  let pp d = d.pp
  let key d = d.id
end

module Reg = K.Make_Registry (Data)

type 'a t = 'a K.t

let register (type a) ?(pp = Fmt.nop) name (cmd : a Cmdliner.Term.t) =
  let id =
    K.create
      (module struct
        type t = a

        let name = name
      end)
  in
  Reg.register { cmd; id; pp };
  id

let register_flag ?doc name =
  register ~pp:Fmt.bool ("Options." ^ name)
    Cmdliner.Arg.(value & flag & info [ name ] ?doc)

let register_int ?doc name default =
  register ~pp:Fmt.int ("Options." ^ name)
    Cmdliner.Arg.(value & opt int default & info [ name ] ?doc)

module KM = K.MkMap (struct
  type ('a, 'b) t = 'a
end)

let env = Datastructure.Ref.create Fmt.nop "Core.Options" KM.empty
let get_from_options options k = KM.find k options
let get d k = get_from_options (Datastructure.Ref.get env d) k

type options = unit KM.t

let set m k v = KM.add k v m

let parser () : options Cmdliner.Term.t =
  let fold acc data =
    Cmdliner.Term.(
      const (fun acc v -> set acc data.Data.id v) $ acc $ data.Data.cmd)
  in
  Reg.fold_initialized { fold } (Cmdliner.Term.const KM.empty)

let set_egraph d m = Datastructure.Ref.set env d m

let default_options () =
  match Cmdliner.Cmd.eval_peek_opts ~argv:[| "colibri2" |] (parser ()) with
  | Some r, _ -> r
  | None, _ -> invalid_arg "Some options are not optional"

let dotgui_only =
  let doc =
    "Display only the given node in the dotgui output --debug-flag dotgui"
  in
  register
    ~pp:Fmt.(list ~sep:comma string)
    "dotgui_only"
    Cmdliner.Arg.(
      value & opt_all string []
      & info [ "debug-dotgui-only" ] ~doc
          ~env:(Cmdliner.Cmd.Env.info "COLIBRI2_DOTGUI_ONLY"))

let multiple_merging_round =
  register "egraph-merging-rounds"
    Cmdliner.Arg.(
      value & flag
      & info
          [ "egraph-merging-rounds" ]
          ~docs:
            "Accept more that one merging round between two congruence class")

let () =
  (Egraph.Private.get_dotgui_only :=
     fun d -> KM.find dotgui_only (Datastructure.Ref.get_private env d));

  Egraph.Private.get_multiple_merging_round :=
    fun d -> KM.find multiple_merging_round (Datastructure.Ref.get env d)
