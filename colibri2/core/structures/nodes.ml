(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib
open Popop_stdlib

exception BrokenInvariant of string
exception SolveSameRepr
exception UnwaitedEvent
exception AlreadyDead
exception AlreadyRedirected

let debug_create =
  Debug.register_info_flag
    ~desc:"for the core solver class creation information" "index"

module ThTermKind = Keys.Make_key2 (struct end)
module ValueKind = Keys.Make_key2 (struct end)

module Node = struct
  type 'a r =
    | ThTerm : int * ('a, _) ThTermKind.t * 'a -> [ `ThTerm ] r
    | Value : int * ('a, _) ValueKind.t * 'a -> [ `Value ] r

  type t' = All : _ r -> t' [@@unboxed]
  type thterm = [ `ThTerm ] r
  type nodevalue = [ `Value ] r

  let tag : t' -> int = function
    | All (ThTerm (tag, _, _)) -> tag
    | All (Value (tag, _, _)) -> tag

  let names = Simple_vector.create 100
  let used_names : (* next id to use *) int DStr.H.t = DStr.H.create 100

  (** remove the empty string, and given in COLIBRI2_FORBID_NAMES *)
  let () =
    let forbid_names = List.iter (fun x -> DStr.H.add used_names x 0) in
    forbid_names [ "" ];
    let names =
      Option.get_or ~default:"" (Sys.getenv_opt "COLIBRI2_FORBID_NAMES")
    in
    forbid_names (String.split_on_char ':' names)

  let pp fmt x =
    Format.pp_print_char fmt '@';
    Format.pp_print_string fmt (Simple_vector.get names (tag x))

  module T = Popop_stdlib.MakeMSH (struct
    type t = t'

    let tag = tag
    let pp = pp
  end)

  include T

  let next_tag, incr_tag = Util.get_counter ()

  let rename node s =
    let s = Strings.find_new_name used_names s in
    Simple_vector.set names (tag node) s

  module ThTermIndex = ThTermKind.MkHashtbl (struct
    type ('a, _, _) t = 'a -> thterm
  end)

  let semindex : unit ThTermIndex.t = ThTermIndex.create 8
  let thterm sem v : thterm = ThTermIndex.get semindex sem v

  module ValueIndex = ValueKind.MkHashtbl (struct
    type ('a, 'unedeed, _) t = 'a -> nodevalue
  end)

  let valueindex : unit ValueIndex.t = ValueIndex.create 8
  let nodevalue value v : nodevalue = ValueIndex.get valueindex value v
  let of_thterm : thterm -> t = fun t -> All t
  let of_nodevalue : nodevalue -> t = fun t -> All t
  let index_sem sem v = of_thterm (thterm sem v)
  let index_value sem v = of_nodevalue (nodevalue sem v)
end

module type NamedDatatype = sig
  include Popop_stdlib.Datatype

  val name : string
end

type ty = { app : Dolmen_std.Expr.ty_cst; args : ty list }

module type RegisteredThTerm = sig
  type s

  module SD : sig
    include NamedDatatype with type t = s

    val ty : t -> ty
  end

  include Datatype
  (** thterm *)

  val key : (s, t) ThTermKind.t

  val index : s -> t
  (** Return a thterm from a semantical value *)

  val node : t -> Node.t
  (** Return a node from a thterm *)

  val sem : t -> s
  (** Return the sem from a thterm *)

  val ty : t -> ty
  (** The type of the thterm *)

  val thterm : t -> Node.thterm
  val of_thterm : Node.thterm -> t option
  val coerce_thterm : Node.thterm -> t
  val iter : (t -> unit) -> unit
end

module ThTermRegistry = ThTermKind.Make_Registry (struct
  type ('k, 'd) data =
    (module RegisteredThTerm with type s = 'k and type t = 'd)

  let ppk (type k d) ((module ThTerm) : (k, d) data) = ThTerm.SD.pp
  let ppd (type k d) ((module ThTerm) : (k, d) data) = ThTerm.pp
  let key (type k d) ((module ThTerm) : (k, d) data) = ThTerm.key
end)

let check_thterm_registered = ThTermRegistry.check_is_registered
let print_thterm = ThTermRegistry.printk
let get_thterm = ThTermRegistry.get

module type RegisteredValue = sig
  type s

  module SD : NamedDatatype with type t = s

  include Datatype
  (** nodevalue *)

  val key : (s, t) ValueKind.t

  val index : ?basename:string -> s -> t
  (** Return a nodevalue from a valueantical value *)

  val node : t -> Node.t
  (** Return a class from a nodevalue *)

  val value : t -> s
  (** Return the value from a nodevalue *)

  val nodevalue : t -> Node.nodevalue
  val of_nodevalue : Node.nodevalue -> t option
  val of_value : s -> Node.nodevalue
  val coerce_nodevalue : Node.nodevalue -> t
  val coerce_value : Node.nodevalue -> s
end

module ValueRegistry = ValueKind.Make_Registry (struct
  type ('k, 'd) data = (module RegisteredValue with type s = 'k and type t = 'd)

  let ppk (type k d) ((module Value) : (k, d) data) = Value.SD.pp
  let ppd (type k d) ((module Value) : (k, d) data) = Value.pp
  let key (type k d) ((module Value) : (k, d) data) = Value.key
end)

let check_value_registered = ValueRegistry.check_is_registered
let print_value = ValueRegistry.printk
let get_value = ValueRegistry.get

module ThTerm = struct
  include Popop_stdlib.MakeMSH (struct
    type t = Node.thterm

    let tag : t -> int = function Node.ThTerm (tag, _, _) -> tag

    let pp fmt : t -> unit = function
      | Node.ThTerm (_, sem, v) -> print_thterm sem fmt v
  end)

  let index = Node.thterm
  let node = Node.of_thterm

  let ty : t -> ty = function
    | Node.ThTerm (_, sem, v) ->
        let f : type a b. (a, b) ThTermKind.t -> a -> ty =
         fun sem v ->
          let module M = (val get_thterm sem) in
          M.SD.ty v
        in
        f sem v
end

module RegisterThTerm (D : sig
  include NamedDatatype

  val ty : t -> ty
end) : RegisteredThTerm with type s = D.t = struct
  module All = struct
    module SD = D

    let key =
      ThTermKind.create
        (module struct
          let name = D.name

          type t = D.t
          type d = Node.thterm
        end)

    module HC = Hashcons.MakeTag (struct
      open Node

      type t = thterm

      let next_tag = Node.next_tag
      let incr_tag = Node.incr_tag

      let equal : t -> t -> bool =
       fun a b ->
        match (a, b) with
        | ThTerm (_, sema, va), ThTerm (_, semb, vb) -> (
            match
              ( ThTermKind.Eq.coerce_type sema key,
                ThTermKind.Eq.coerce_type semb key )
            with
            | Poly.Eq, Poly.Eq -> D.equal va vb)

      let hash : t -> int =
       fun a ->
        match a with
        | ThTerm (_, sema, va) ->
            let Poly.Eq = ThTermKind.Eq.coerce_type sema key in
            D.hash va

      let set_tag : int -> t -> t =
       fun tag (ThTerm (_, sem, v)) -> ThTerm (tag, sem, v)

      let tag : t -> int = fun (ThTerm (tag, _, _)) -> tag

      let pp fmt x =
        Format.pp_print_char fmt '@';
        Format.pp_print_string fmt (Simple_vector.get names (tag x))
    end)

    include HC

    type s = D.t

    let tag : t -> int = function Node.ThTerm (tag, _, _) -> tag

    let index v =
      let node =
        HC.hashcons2 (fun tag sem v -> Node.ThTerm (tag, sem, v)) key v
      in
      let i = tag node in
      Simple_vector.inc_size (i + 1) Node.names;
      if Simple_vector.is_uninitialized Node.names i then (
        let s =
          Strings.find_new_name Node.used_names ""
          (* TODO use ThTerm.pp or Sem.print_debug *)
        in
        Trace.message "index" ~data:(fun () ->
            [
              ("id", `String s); ("node", `String (Format.sprintf "%a" D.pp v));
            ]);
        Debug.dprintf3 debug_create "[Egraph] @[@@%s is %a@]" s D.pp v;
        Simple_vector.set Node.names i s);
      node

    let node = Node.of_thterm

    let sem : t -> D.t =
     fun (Node.ThTerm (_, sem, v)) ->
      let Poly.Eq = ThTermKind.Eq.coerce_type sem key in
      v

    let ty : t -> ty =
     fun (Node.ThTerm (_, sem, v)) ->
      let Poly.Eq = ThTermKind.Eq.coerce_type sem key in
      D.ty v

    let thterm : t -> ThTerm.t = fun x -> x

    let of_thterm : ThTerm.t -> t option = function
      | Node.ThTerm (_, sem', _) as v when ThTermKind.equal sem' key -> Some v
      | _ -> None

    let coerce_thterm : ThTerm.t -> t =
     fun (Node.ThTerm (_, sem', _) as v) ->
      assert (ThTermKind.equal sem' key);
      v
  end

  let () =
    ThTermRegistry.register
      (module All : RegisteredThTerm with type s = D.t and type t = All.t);
    Node.ThTermIndex.set Node.semindex All.key All.index

  include All
end

module Value = struct
  include Popop_stdlib.MakeMSH (struct
    type t = Node.nodevalue

    let tag : t -> int = function Node.Value (tag, _, _) -> tag

    let pp fmt : t -> unit = function
      | Node.Value (_, value, v) -> print_value value fmt v
  end)

  let index = Node.nodevalue
  let node = Node.of_nodevalue

  let value : type a b. (a, b) ValueKind.t -> t -> a option =
   fun value (Node.Value (_, value', v)) ->
    match ValueKind.Eq.eq_type value value' with
    | Poly.Neq -> None
    | Poly.Eq -> Some v

  type kind = Value : (_, 'b) ValueKind.t * 'b -> kind

  let kind : t -> kind = function
    | Node.Value (_, value, _) as x -> Value (value, Obj.magic x)

  let nodevalue : (_, 'b) ValueKind.t -> 'b -> t =
   fun _ v -> (Obj.magic (v : 'b) : t)
end

module RegisterValue (D : NamedDatatype) : RegisteredValue with type s = D.t =
struct
  module All = struct
    let key =
      ValueKind.create
        (module struct
          let name = D.name

          type t = D.t
          type d = Node.nodevalue
        end)

    module SD = D

    module HC = Hashcons.MakeTag (struct
      open Node

      type t = nodevalue

      let next_tag = Node.next_tag
      let incr_tag = Node.incr_tag

      let equal : t -> t -> bool =
       fun a b ->
        match (a, b) with
        | Value (_, valuea, va), Value (_, valueb, vb) -> (
            match
              ( ValueKind.Eq.coerce_type valuea key,
                ValueKind.Eq.coerce_type valueb key )
            with
            | Poly.Eq, Poly.Eq -> D.equal va vb)

      let hash : t -> int =
       fun a ->
        match a with
        | Value (_, valuea, va) ->
            let Poly.Eq = ValueKind.Eq.coerce_type valuea key in
            D.hash va

      let set_tag : int -> t -> t =
       fun tag x -> match x with Value (_, value, v) -> Value (tag, value, v)

      let tag : t -> int = function Value (tag, _, _) -> tag

      let pp fmt x =
        Format.pp_print_char fmt '@';
        Format.pp_print_string fmt (Simple_vector.get names (tag x))
    end)

    include HC

    type s = D.t

    let tag : t -> int = function Node.Value (tag, _, _) -> tag

    let index ?(basename = "") v =
      let node =
        HC.hashcons2 (fun tag value v -> Node.Value (tag, value, v)) key v
      in
      let i = tag node in
      Simple_vector.inc_size (i + 1) Node.names;
      if Simple_vector.is_uninitialized Node.names i then (
        let s = Strings.find_new_name Node.used_names basename in
        Debug.dprintf3 debug_create "[Egraph] @[@@%s is %a@]" s D.pp v;
        Simple_vector.set Node.names i s);
      node

    let node = Node.of_nodevalue

    let value : t -> D.t = function
      | Node.Value (_, value, v) ->
          let Poly.Eq = ValueKind.Eq.coerce_type value key in
          v

    let nodevalue : t -> Value.t = fun x -> x

    let of_nodevalue : Value.t -> t option = function
      | Node.Value (_, value', _) as v when ValueKind.equal value' key -> Some v
      | _ -> None

    let of_value v = nodevalue (index v)

    let coerce_nodevalue : Value.t -> t = function
      | Node.Value (_, value', _) as v ->
          assert (ValueKind.equal value' key);
          v

    let coerce_value x = value (coerce_nodevalue x)
  end

  include All

  let () =
    ValueRegistry.register
      (module All : RegisteredValue with type s = D.t and type t = All.t);
    Node.ValueIndex.set Node.valueindex All.key index
end

module Only_for_solver = struct
  type sem_of_node = ThTerm : (_, 'b) ThTermKind.t * 'b -> sem_of_node

  let thterm : Node.t -> ThTerm.t option = function
    | Node.All (Node.Value _) -> None
    | Node.All (Node.ThTerm _ as x) -> Some x

  let sem_of_node : ThTerm.t -> sem_of_node = function
    | Node.ThTerm (_, sem, _) as v -> ThTerm (sem, Obj.magic v)

  let nodevalue : Node.t -> Value.t option = function
    | Node.All (Node.ThTerm _) -> None
    | Node.All (Node.Value _ as x) -> Some x

  let node_of_thterm : ThTerm.t -> Node.t = ThTerm.node
  let node_of_nodevalue : Value.t -> Node.t = Value.node

  type opened_node =
    | ThTerm : ThTerm.t -> opened_node
    | Value : Value.t -> opened_node

  let open_node : Node.t -> opened_node = function
    | Node.All (Node.ThTerm _ as x) -> ThTerm x
    | Node.All (Node.Value _ as x) -> Value x

  let is_value : Node.t -> bool = function
    | Node.All (Node.ThTerm _) -> false
    | Node.All (Node.Value _) -> true
end

let check_initialization () =
  ThTermRegistry.is_well_initialized () && ValueRegistry.is_well_initialized ()
