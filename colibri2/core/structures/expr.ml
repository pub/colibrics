(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

include Dolmen_std.Expr
include Dolmen_std.Builtin

module Ty = struct
  include Dolmen_std.Expr.Ty

  let pp = print

  include Colibri2_popop_lib.Popop_stdlib.MkDatatype (struct
    type nonrec t = t

    let equal = equal
    let compare = compare
    let hash = hash
    let pp = pp
    let hash_fold_t s t = Base.Hash.fold_int s (hash t)
  end)

  module Var = struct
    include Dolmen_std.Expr.Ty.Var

    let pp = Dolmen_std.Expr.Print.ty_var

    include Colibri2_popop_lib.Popop_stdlib.MakeMSH (struct
      type nonrec t = t

      let tag x = (x.index :> int)
      let pp = pp
    end)
  end

  let rec free_vars acc (t : t) =
    match t.ty_descr with
    | TyVar v -> Var.S.add v acc
    | TyApp (_, l) -> List.fold_left free_vars acc l
    | Arrow (args, ret) -> List.fold_left free_vars (free_vars acc ret) args
    | Pi (vars, body) ->
        let fv = free_vars Var.S.empty body in
        let fv = List.fold_left (fun acc v -> Var.S.remove v acc) fv vars in
        Var.S.union fv acc

  let rec has_no_vars (t : t) =
    match t.ty_descr with
    | TyVar _ -> false
    | TyApp (_, l) -> List.for_all has_no_vars l
    | Arrow (args, ret) -> has_no_vars ret && List.for_all has_no_vars args
    | Pi _ -> false

  module Const = struct
    include Dolmen_std.Expr.Ty.Const

    let pp fmt (t : t) = Dolmen_std.Expr.Id.print fmt t

    include Colibri2_popop_lib.Popop_stdlib.MakeMSH (struct
      type nonrec t = t

      let tag x = (x.index :> int)
      let pp = pp
    end)
  end
end

module Term = struct
  include Dolmen_std.Expr.Term

  let pp = print

  let rec pp_smtlib fmt t =
    match t.term_descr with
    | Var _ -> Fmt.pf fmt "<var>"
    | Cst c -> Dolmen_std.Expr.Id.print fmt c
    | App (f, [], []) -> pp_smtlib fmt f
    | App (f, _, []) -> Fmt.pf fmt "(as %a %a)" pp f Ty.pp t.term_ty
    | App (f, [], args) -> Fmt.pf fmt "(%a %a)" pp f Fmt.(list ~sep:sp pp) args
    | App (f, _, args) ->
        Fmt.pf fmt "((as %a %a) %a)" pp f Ty.pp t.term_ty
          Fmt.(list ~sep:sp pp)
          args
    | Binder (_, _) -> Fmt.pf fmt "<binder>"
    | Match (_, _) -> Fmt.pf fmt "<match>"

  include Colibri2_popop_lib.Popop_stdlib.MkDatatype (struct
    type nonrec t = t

    let equal = equal
    let compare = compare
    let hash = hash
    let pp = pp
    let hash_fold_t s t = Base.Hash.fold_int s (hash t)
  end)

  module Const = struct
    module D = struct
      include Dolmen_std.Expr.Term.Const

      let pp fmt (t : t) = Dolmen_std.Expr.Id.print fmt t

      include Colibri2_popop_lib.Popop_stdlib.MakeMSH (struct
        type nonrec t = t

        let tag x = (x.index :> int)
        let pp = pp
      end)
    end

    include D
    module HC = Datastructure.Hashtbl (D)
  end

  module Var = struct
    include Dolmen_std.Expr.Term.Var

    let pp fmt (t : t) = Dolmen_std.Expr.Id.print fmt t

    include Colibri2_popop_lib.Popop_stdlib.MakeMSH (struct
      type nonrec t = t

      let tag x = (x.index :> int)
      let pp = pp
    end)
  end

  let rec free_vars ((acc_ty, acc_term) as acc) (t : t) =
    match t.term_descr with
    | Var v -> (Ty.free_vars acc_ty v.id_ty, Var.S.add v acc_term)
    | Cst _ -> acc
    | App (f, tys, ts) ->
        let acc_ty, acc_term = free_vars acc f in
        List.fold_left free_vars
          (List.fold_left Ty.free_vars acc_ty tys, acc_term)
          ts
    | Binder ((Lambda (tys, ts) | Exists (tys, ts) | Forall (tys, ts)), body) ->
        let fty, fv = free_vars (Ty.Var.S.empty, Var.S.empty) body in
        let fty =
          List.fold_left (fun acc ty -> Ty.Var.S.remove ty acc) fty tys
        in
        let fv = List.fold_left (fun acc t -> Var.S.remove t acc) fv ts in
        (Ty.Var.S.union acc_ty fty, Var.S.union acc_term fv)
    | Binder (Let_seq l, body) ->
        let acc_ty, fv = free_vars (acc_ty, Var.S.empty) body in
        let acc_ty, fv =
          List.fold_right
            (fun (v, t) (acc_ty, fv) ->
              let fv = Var.S.remove v fv in
              let acc_ty, fv = free_vars (acc_ty, fv) t in
              let acc_ty = Ty.free_vars acc_ty v.id_ty in
              (acc_ty, fv))
            l (acc_ty, fv)
        in
        (acc_ty, Var.S.union acc_term fv)
    | Binder (Let_par l, body) ->
        let acc_ty, fv = free_vars (acc_ty, Var.S.empty) body in
        let fv =
          List.fold_right
            (fun (v, _) fv ->
              let fv = Var.S.remove v fv in
              fv)
            l fv
        in
        let acc_ty, fv =
          List.fold_right
            (fun (v, t) (acc_ty, fv) ->
              let acc_ty, fv = free_vars (acc_ty, fv) t in
              let acc_ty = Ty.free_vars acc_ty v.id_ty in
              (acc_ty, fv))
            l (acc_ty, fv)
        in
        (acc_ty, Var.S.union acc_term fv)
    | Match (scrutinee, branches) ->
        let acc = free_vars (acc_ty, acc_term) scrutinee in
        List.fold_left
          (fun (acc_ty, acc_term) (pat, body) ->
            let acc_ty, freet = free_vars (acc_ty, Var.S.empty) body in
            let acc_ty, boundt = free_vars (acc_ty, Var.S.empty) pat in
            let bound = Var.S.diff freet boundt in
            (acc_ty, Var.S.union acc_term bound))
          acc branches

  module ForSmallTerm : sig
    val remove_lets : t -> t
  end = struct
    let rec ty_var_list_subst ty_var_map = function
      | [] -> ty_var_map
      | (v :: r : ty_var list) ->
          ty_var_list_subst (Subst.Var.remove v ty_var_map) r

    let rec term_var_list_subst ty_var_map t_var_map acc = function
      | [] -> (List.rev acc, t_var_map)
      | (v :: r : term_var list) ->
          let ty = Ty.subst ty_var_map v.id_ty in
          if not (Ty.equal ty v.id_ty) then
            let nv = Id.mk v.path ty in
            term_var_list_subst ty_var_map
              (Subst.Var.bind t_var_map v (of_var nv))
              (nv :: acc) r
          else
            term_var_list_subst ty_var_map
              (Subst.Var.remove v t_var_map)
              (v :: acc) r

    (** From dolmen *)
    let rec remove_lets_aux ~fix ty_var_map t_var_map (t : t) =
      match t.term_descr with
      | Var v -> (
          match Subst.Var.get v t_var_map with
          | exception Not_found -> t
          | term ->
              if fix then remove_lets_aux ~fix ty_var_map t_var_map term
              else term)
      | Cst _ -> t
      | App (f, tys, args) ->
          let new_f = remove_lets_aux ~fix ty_var_map t_var_map f in
          let new_tys = List.map (Ty.subst ~fix ty_var_map) tys in
          let new_args =
            List.map (remove_lets_aux ~fix ty_var_map t_var_map) args
          in
          if
            Base.phys_equal new_f f
            && List.for_all2 Base.phys_equal new_tys tys
            && List.for_all2 Base.phys_equal new_args args
          then t
          else apply new_f new_tys new_args
      | Binder (b, body) ->
          let mk_bind b body =
            match b with
            | Let_seq _ -> body (* already subtituted *)
            | Let_par _ -> body (* already subtituted *)
            | Forall (l, l') -> all (l, l') body
            | Exists (l, l') -> ex (l, l') body
            | Lambda (tys, ts) -> lam (tys, ts) body
          in
          let b', ty_var_map, t_var_map =
            binder_subst ~fix ty_var_map t_var_map b
          in
          mk_bind b' (remove_lets_aux ~fix ty_var_map t_var_map body)
      | Match (scrutinee, branches) ->
          let scrutinee = remove_lets_aux ~fix ty_var_map t_var_map scrutinee in
          let branches =
            List.map (branch_subst ~fix ty_var_map t_var_map) branches
          in
          pattern_match scrutinee branches

    and binder_subst ~fix ty_var_map t_var_map = function
      | Let_seq l ->
          let l, t_var_map =
            binding_list_subst ~fix ty_var_map t_var_map [] l
          in
          let t_var_map =
            List.fold_left
              (fun acc (v, t) -> Subst.Var.bind acc v t)
              t_var_map l
          in
          (Let_seq l, ty_var_map, t_var_map)
      | Let_par l ->
          let l, t_var_map =
            binding_list_subst ~fix ty_var_map t_var_map [] l
          in
          let t_var_map =
            List.fold_left
              (fun acc (v, t) ->
                Subst.Var.bind acc v
                  (Dolmen_std.Expr.Term.subst ty_var_map acc t))
              t_var_map l
          in
          (Let_par l, ty_var_map, t_var_map)
      | Lambda (tys, ts) ->
          (* term variables in ts may have their types changed by the subst *)
          let ty_var_map = ty_var_list_subst ty_var_map tys in
          let ts, t_var_map = term_var_list_subst ty_var_map t_var_map [] ts in
          (Lambda (tys, ts), ty_var_map, t_var_map)
      | Exists (tys, ts) ->
          (* term variables in ts may have their types changed by the subst *)
          let ty_var_map = ty_var_list_subst ty_var_map tys in
          let ts, t_var_map = term_var_list_subst ty_var_map t_var_map [] ts in
          (Exists (tys, ts), ty_var_map, t_var_map)
      | Forall (tys, ts) ->
          (* term variables in ts may have their types changed by the subst *)
          let ty_var_map = ty_var_list_subst ty_var_map tys in
          let ts, t_var_map = term_var_list_subst ty_var_map t_var_map [] ts in
          (Forall (tys, ts), ty_var_map, t_var_map)

    and binding_list_subst ~fix ty_var_map t_var_map acc = function
      | [] -> (List.rev acc, t_var_map)
      | ((v, t) :: r : (term_var * term) list) ->
          let t = remove_lets_aux ~fix ty_var_map t_var_map t in
          if Ty.equal (ty t) v.id_ty then
            let t_var_map = Subst.Var.remove v t_var_map in
            let acc = (v, t) :: acc in
            binding_list_subst ~fix ty_var_map t_var_map acc r
          else
            let nv = Id.mk v.path (ty t) in
            let t_var_map = Subst.Var.bind t_var_map v (of_var nv) in
            let acc = (nv, t) :: acc in
            binding_list_subst ~fix ty_var_map t_var_map acc r

    and branch_subst ~fix ty_var_map t_var_map (pattern, body) =
      let _, l = fv pattern in
      let _, t_var_map = term_var_list_subst ty_var_map t_var_map [] l in
      ( remove_lets_aux ~fix ty_var_map t_var_map pattern,
        remove_lets_aux ~fix ty_var_map t_var_map body )

    and remove_lets t = remove_lets_aux ~fix:true Subst.empty Subst.empty t
  end
end

let add_builtins, additional_builtins =
  let q : Dolmen_loop.Typer.T.builtin_symbols Base.Queue.t =
    Base.Queue.create ()
  in
  ( Base.Queue.enqueue q,
    fun e t ->
      let o =
        Base.Queue.find_map q ~f:(fun f ->
            match f e t with `Not_found -> None | x -> Some x)
      in
      Option.value ~default:`Not_found o )
