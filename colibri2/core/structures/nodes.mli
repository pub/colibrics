(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

(** Define the Node, and the related types semantic terms and value *)

open Colibri2_popop_lib
open Popop_stdlib

(** {2 General exceptions (to move away)} *)

exception BrokenInvariant of string
exception SolveSameRepr
exception UnwaitedEvent
exception AlreadyDead
exception AlreadyRedirected

module ThTermKind : Keys.Key2
module ValueKind : Keys.Key2

(** Node *)

module Node : sig
  include Datatype

  val rename : t -> string -> unit
  (** Change the pretty printed string for this node, to use with care
      preferably at the start *)

  val index_sem : ('a, _) ThTermKind.t -> 'a -> t
  (** Return the corresponding node from a theory term *)

  val index_value : ('a, _) ValueKind.t -> 'a -> t
  (** Return the corresponding node from a value *)
end

(** {2 Theory terms} *)

(** Basically a theory term type is just something with an ordering. For each
    theory terms a unique node is associated. *)
val check_thterm_registered : _ ThTermKind.t -> unit
(** {3 Generic Handling of theory terms} *)

val print_thterm : ('a, _) ThTermKind.t -> 'a Format.printer

type ty = { app : Dolmen_std.Expr.ty_cst; args : ty list }
(** Backward definition for Ground.Ty.t *)

module ThTerm : sig
  include Datatype

  val index : ('a, _) ThTermKind.t -> 'a -> t
  (** Return the corresponding node from a theory term *)

  val node : t -> Node.t
  (** Returns the node associated to this theory term *)

  val ty : t -> ty
end

(** {3 Construction of a theory terms} *)

(** Result of the registration of a theory term *)
module type RegisteredThTerm = sig
  type s
  (** the user given type *)

  module SD : sig
    include NamedDatatype with type t = s

    val ty : t -> ty
  end

  include Datatype
  (** thterm *)

  val key : (s, t) ThTermKind.t

  val index : s -> t
  (** Return a theory term from the user type *)

  val node : t -> Node.t
  (** Return a class from a thterm *)

  val sem : t -> s
  (** Return the sem from a thterm *)

  val ty : t -> ty
  (** The type of the thterm *)

  val thterm : t -> ThTerm.t

  val of_thterm : ThTerm.t -> t option
  (** Return the user type if the ThTerm belongs to this module *)

  val coerce_thterm : ThTerm.t -> t
  (** Return the user type. Raise if the ThTerm does not belong to this module
  *)

  val iter : (t -> unit) -> unit
  (** iter all the indexed values. Normally not useful *)
end

module RegisterThTerm (D : sig
  include NamedDatatype

  val ty : t -> ty
end) : RegisteredThTerm with type s = D.t

val get_thterm :
  ('a, 'b) ThTermKind.t ->
  (module RegisteredThTerm with type s = 'a and type t = 'b)

(** {2 Values} *)

(** Basically a value is just something with an ordering. For each value a
    unique node is associated. The difference with theory terms is that only one
    value of a kind can be in an equivalence class. The solver keep track of
    which value is associated to an equivalence class (like it does for domains)
*)

val print_value : ('a, _) ValueKind.t -> 'a Format.printer
val check_value_registered : _ ValueKind.t -> unit

(** {3 Module for handling generically values} *)

module Value : sig
  include Datatype

  val index : ('a, _) ValueKind.t -> 'a -> t
  (** Return the corresponding node from a value *)

  val node : t -> Node.t
  val value : ('a, _) ValueKind.t -> t -> 'a option
  val nodevalue: (_, 'b) ValueKind.t -> 'b -> t

  type kind = Value : (_, 'b) ValueKind.t * 'b -> kind

  val kind : t -> kind
  (** unpack a value *)
end

(** {3 For building a particular value} *)

module type RegisteredValue = sig
  type s

  module SD : NamedDatatype with type t = s

  include Datatype
  (** nodevalue *)

  val key : (s, t) ValueKind.t

  val index : ?basename:string -> s -> t
  (** Return a nodevalue from a valueantical term. Basename is used only for
      debug *)

  val node : t -> Node.t
  (** Return a class from a nodevalue *)

  val value : t -> s
  (** Return the value from a nodevalue *)

  val nodevalue : t -> Value.t
  val of_nodevalue : Value.t -> t option
  val of_value : s -> Value.t
  val coerce_nodevalue : Value.t -> t
  val coerce_value : Value.t -> s
end

module RegisterValue (D : NamedDatatype) : RegisteredValue with type s = D.t

val get_value :
  ('a, 'b) ValueKind.t ->
  (module RegisteredValue with type t = 'b and type s = 'a)

val check_initialization : unit -> bool
(** Check if the initialization of all the sem and value have been done *)

(** {2 Only for the solver} *)
module Only_for_solver : sig
  type sem_of_node = ThTerm : (_, 'b) ThTermKind.t * 'b -> sem_of_node

  val thterm : Node.t -> ThTerm.t option
  (** give the sem associated with a node, make sense only for not merged class.
      So only the module solver can use it *)

  val sem_of_node : ThTerm.t -> sem_of_node
  (** give the sem associated with a node, make sense only for not merged class.
      So only the module solver can use it *)

  val nodevalue : Node.t -> Value.t option
  (** give the value associated with a node, make sense only for not merged
      class. So only the module solver can use it *)

  val node_of_thterm : ThTerm.t -> Node.t
  val node_of_nodevalue : Value.t -> Node.t

  type opened_node =
    | ThTerm : ThTerm.t -> opened_node
    | Value : Value.t -> opened_node

  val open_node : Node.t -> opened_node
  val is_value : Node.t -> bool
end
