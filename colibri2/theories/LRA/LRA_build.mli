(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

module LastEffort : sig
  val add' : _ Egraph.t -> A.t -> Node.t -> A.t -> Node.t -> Node.t
  val add : _ Egraph.t -> Node.t -> Node.t -> Node.t
  val sub : _ Egraph.t -> Node.t -> Node.t -> Node.t
  val mult_cst : _ Egraph.t -> A.t -> Node.t -> Node.t
  val mult : _ Egraph.t -> Node.t -> Node.t -> Node.t
  val gt_zero : _ Egraph.t -> Node.t -> Node.t
  val ge_zero : _ Egraph.t -> Node.t -> Node.t
  val lt : _ Egraph.t -> Node.t -> Node.t -> Node.t
  val le : _ Egraph.t -> Node.t -> Node.t -> Node.t
  val gt : _ Egraph.t -> Node.t -> Node.t -> Node.t
  val ge : _ Egraph.t -> Node.t -> Node.t -> Node.t
end
