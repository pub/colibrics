(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib

open Colibri2_core
(** Polynome *)

(* Poly.t is a map from Node.t to A.t and represents an addition of
   scaled variables, by associating an algebraic value to each node:
        a0.v0 + ... + aN.vN
*)
module Poly : Popop_stdlib.Datatype with type t = A.t Node.M.t

(* A polynom a0.v0 + ... + aN.vN = cst *)
type t = private { cst : A.t; poly : Poly.t }

include Popop_stdlib.Datatype with type t := t

val invariant : t -> bool
val zero : t
val is_zero : t -> bool
val cst : A.t -> t
val is_cst : t -> A.t option
val get_cst : t -> A.t
val monome : A.t -> Node.t -> t
val is_one_node : t -> Node.t option
val nodes : t -> Node.S.t

type extract =
  | Zero  (** p = 0 *)
  | Cst of A.t  (** p = q *)
  | Var of A.t * Node.t * t  (** p = qx + p' *)

val extract : t -> extract

type kind = ZERO | CST | VAR

val classify : t -> kind
val sub_cst : t -> A.t -> t
val add_cst : t -> A.t -> t
val mult_cst : A.t -> t -> t
val add : t -> t -> t
val sub : t -> t -> t
val neg : t -> t
val of_list : A.t -> (Node.t * A.t) list -> t
val of_map : A.t -> A.t Node.M.t -> t
val x_p_cy : t -> A.t -> t -> t
val cx_p_cy : A.t -> t -> A.t -> t -> t
val subst : t -> Node.t -> t -> t * A.t
val subst_node : t -> Node.t -> Node.t -> t * A.t
val fold : ('a -> Node.t -> A.t -> 'a) -> 'a -> t -> 'a
val iter : (Node.t -> A.t -> unit) -> t -> unit
val compute : (Node.t -> A.t) -> t -> A.t

type 'a tree = Empty | Node of 'a tree * Node.t * 'a * 'a tree * int

val get_tree : t -> A.t tree * A.t

val normalize : t -> t
(** divide by the pgcd *)

val print_poly : A.t Node.M.t Fmt.t
