(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

module T = struct
  type t = { cst : A.t; poly : Q.t Node.M.t } [@@deriving eq, ord, hash]

  let pp_exposant fmt q =
    if Z.equal q.Q.den Z.one && Z.fits_int q.Q.num then
      match Z.to_int q.Q.num with
      | 1 -> ()
      | 2 -> Fmt.string fmt "²"
      | 3 -> Fmt.string fmt "³"
      | 4 -> Fmt.string fmt "⁴"
      | 5 -> Fmt.string fmt "⁵"
      | 6 -> Fmt.string fmt "⁶"
      | 7 -> Fmt.string fmt "⁷"
      | 8 -> Fmt.string fmt "⁸"
      | 9 -> Fmt.string fmt "⁹"
      | i -> Fmt.pf fmt "^%i" i
    else Fmt.pf fmt "^%a" Q.pp q

  let pp fmt v =
    let print_not_one fmt q = if not (A.equal q A.one) then A.pp fmt q in
    if Node.M.is_empty v.poly then A.pp fmt v.cst
    else
      Fmt.pf fmt "@[%a|%a|@]" print_not_one v.cst
        Fmt.(iter_bindings Node.M.iter (pair ~sep:nop Node.pp pp_exposant))
        v.poly
end

include T
include Popop_stdlib.MkDatatype (T)

(** different invariant *)

let invariant p =
  A.sign p.cst > 0 && Node.M.for_all (fun _ q -> not (Q.equal q Q.zero)) p.poly

let cst q = { cst = q; poly = Node.M.empty }
let zero = cst A.zero
let is_zero p = A.is_zero p.cst
let is_not_zero p = A.is_not_zero p.cst

(** constructor *)
let one = { cst = A.one; poly = Node.M.empty }
(*
 * let is_cst p = if Node.M.is_empty p.poly then Some p.cst else None
 * (\* let is_zero p = Q.equal p.cst Q.zero && Node.M.is_empty p.poly *\)
 * let is_one p = Q.equal p.cst Q.one && Node.M.is_empty p.poly *)

type extract = One | Cst of A.t | Var of Q.t * Node.t * t

let extract p =
  if Node.M.is_empty p.poly then if A.equal p.cst A.one then One else Cst p.cst
  else
    let x, q = Shuffle.chooseb Node.M.choose Node.M.choose_rnd p.poly in
    let p' = { p with poly = Node.M.remove x p.poly } in
    Var (q, x, p')

let remove n p = { p with poly = Node.M.remove n p.poly }

type kind = CST of A.t | NODE of A.t * Node.t | PRODUCT

let classify p =
  if Node.M.is_empty p.poly then CST p.cst
  else if Node.M.is_num_elt 1 p.poly then
    let node, k = Node.M.choose p.poly in
    if Q.equal Q.one k then NODE (p.cst, node) else PRODUCT
  else PRODUCT

let monome x c =
  if Q.equal Q.zero c then one else { cst = A.one; poly = Node.M.singleton x c }

let is_one_node p =
  (* cst = 0 and one empty monome *)
  if A.equal A.one p.cst && Node.M.is_num_elt 1 p.poly then
    let node, k = Node.M.choose p.poly in
    if Q.equal Q.one k then Some node else None
  else None

let terms p = Node.M.bindings p.poly

let mult_cst c p1 =
  if A.is_zero c then zero else { p1 with cst = A.mul p1.cst c }

let power_cst c p1 =
  if A.is_zero p1.cst then zero
  else if Q.equal Q.zero c then one
  else if Q.equal Q.one c then p1
  else
    let poly_mult c m = Node.M.map (fun c1 -> Q.mul c c1) m in
    { cst = A.positive_pow p1.cst c; poly = poly_mult c p1.poly }

(** Warning Node.M.union can be used only for defining an operation [op] that
    verifies [op 0 p = p] and [op p 0 = p] *)
let mul p1 p2 =
  if A.is_zero p1.cst || A.is_zero p2.cst then zero
  else
    let poly_add m1 m2 =
      Node.M.union (fun _ c1 c2 -> Q.none_zero (Q.add c1 c2)) m1 m2
    in
    { cst = A.mul p1.cst p2.cst; poly = poly_add p1.poly p2.poly }

let div p1 p2 =
  assert (is_not_zero p2);
  let poly_sub m1 m2 =
    Node.M.union_merge
      (fun _ c1 c2 ->
        match c1 with
        | None -> Some (Q.neg c2)
        | Some c1 -> Q.none_zero (Q.sub c1 c2))
      m1 m2
  in
  { cst = A.div p1.cst p2.cst; poly = poly_sub p1.poly p2.poly }

(** x * y^c *)
let x_m_yc p1 p2 c =
  if Q.equal c Q.zero then p1
  else if Q.equal c Q.one then mul p1 p2
  else
    let f a b = Q.add a (Q.mul c b) in
    let poly m1 m2 =
      Node.M.union_merge
        (fun _ c1 c2 ->
          match c1 with
          | None -> Some (Q.mul c c2)
          | Some c1 -> Q.none_zero (f c1 c2))
        m1 m2
    in
    {
      cst = A.mul p1.cst (A.positive_pow p2.cst c);
      poly = poly p1.poly p2.poly;
    }

let xc_m_yc p1 c1 p2 c2 =
  let c1_iszero = Q.equal c1 Q.zero in
  let c2_iszero = Q.equal c2 Q.zero in
  if c1_iszero && c2_iszero then one
  else if c1_iszero then p2
  else if c2_iszero then p1
  else
    let f e1 e2 = Q.add (Q.mul c1 e1) (Q.mul c2 e2) in
    let poly m1 m2 =
      Node.M.merge
        (fun _ e1 e2 ->
          match (e1, e2) with
          | None, None -> assert false
          | None, Some e2 -> Some (Q.mul c2 e2)
          | Some e1, None -> Some (Q.mul c1 e1)
          | Some e1, Some e2 -> Q.none_zero (f e1 e2))
        m1 m2
    in
    {
      cst = A.mul (A.positive_pow p1.cst c1) (A.positive_pow p2.cst c2);
      poly = poly p1.poly p2.poly;
    }

let subst_node p x y =
  let poly, qo = Node.M.find_remove x p.poly in
  match qo with
  | None -> (p, Q.zero)
  | Some q ->
      let poly =
        Node.M.change
          (function None -> qo | Some q' -> Q.none_zero (Q.add q q'))
          y poly
      in
      ({ p with poly }, q)

exception Div_zero

let subst_exn p x s =
  let poly, q = Node.M.find_remove x p.poly in
  match q with
  | None -> (p, Q.zero)
  | Some q ->
      if is_zero s then (
        if Q.sign q < 0 then raise Div_zero;
        (zero, q))
      else (x_m_yc { p with poly } s q, q)

let fold f acc p = Node.M.fold_left f acc p.poly
let iter f p = Node.M.iter f p.poly

let of_list cst l =
  let fold acc (node, q) =
    Node.M.change
      (function None -> Some q | Some q' -> Q.none_zero (Q.add q q'))
      node acc
  in
  { cst; poly = List.fold_left fold Node.M.empty l }

let of_map cst m =
  assert (Node.M.for_all (fun _ v -> Q.sign v <> 0) m);
  { cst; poly = m }

let common pa pb =
  {
    cst = A.one;
    poly =
      Node.M.inter
        (fun _ a b -> Q.none_zero (Q.min (Q.floor a) (Q.floor b)))
        pa.poly pb.poly;
  }
