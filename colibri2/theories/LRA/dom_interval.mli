(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

val init : Egraph.wt -> unit

module D : sig
  include Colibri2_theories_LRA_stages_def.Interval_sig.S

  val get_convexe_hull : t -> (A.t * Bound.t) option * (A.t * Bound.t) option

  val from_convexe_hull :
    (A.t * Bound.t) option * (A.t * Bound.t) option -> t option

  val is_zero_or_positive : t -> bool
  val mk_exn : A.t -> Bound.t -> Bound.t -> A.t -> t

  val le_zero_lt_one : t
  (** 0 <= x < 1 *)
end

val get_dom : _ Egraph.t -> Node.t -> D.t option
val upd_dom : Egraph.wt -> Node.t -> D.t -> unit
val is_zero_or_positive : _ Egraph.t -> Node.t -> bool
val is_not_zero : _ Egraph.t -> Node.t -> bool
val is_strictly_positive : _ Egraph.t -> Node.t -> bool
val is_strictly_negative : _ Egraph.t -> Node.t -> bool
val is_integer : _ Egraph.t -> Node.t -> bool
val zero_is : _ Egraph.t -> Node.t -> D.is_comparable
val assume_positive : Egraph.wt -> Node.t -> unit
val assume_negative : Egraph.wt -> Node.t -> unit

module Propagate : sig
  val sub :
    Lexing.position ->
    ?thterm:ThTerm.t ->
    'a Egraph.t ->
    r:Node.t ->
    a:Node.t ->
    b:Node.t ->
    unit

  val cmp :
    Lexing.position ->
    ?thterm:ThTerm.t ->
    'a Egraph.t ->
    [ `Ge | `Gt | `Le | `Lt ] ->
    r:Node.t ->
    a:Node.t ->
    b:Node.t ->
    unit

  val bool_equiv_in_dom :
    Lexing.position ->
    ?thterm:ThTerm.t ->
    'a Egraph.t ->
    r:Node.t ->
    a:Node.t ->
    D.t ->
    unit
  (** a ∈ u <=> r *)

  val setb :
    Node.t -> bool option MonadAlsoInterp.monad -> MonadAlsoInterp.sequence

  val getb : Node.t -> bool option MonadAlsoInterp.monad

  val set :
    Node.t -> D.t option MonadAlsoInterp.monad -> MonadAlsoInterp.sequence

  val get : Node.t -> D.t option MonadAlsoInterp.monad

  val unop :
    Lexing.position ->
    ?thterm:ThTerm.t ->
    ?tores:(D.t -> D.t) ->
    ?toarg:(D.t -> D.t) ->
    'a Egraph.t ->
    Node.t ->
    Node.t IArray.t ->
    unit

  val binop :
    Lexing.position ->
    ?thterm:ThTerm.t ->
    ?tores:(D.t -> D.t -> D.t) ->
    ?tofst:(D.t -> D.t -> D.t) ->
    ?tosnd:(D.t -> D.t -> D.t) ->
    'a Egraph.t ->
    Node.t ->
    Node.t IArray.t ->
    unit

  val gen_ceil_truncate_floor :
    Egraph.rw Egraph.t ->
    Node.t ->
    Node.t IArray.t ->
    forward:(D.t -> D.t) ->
    unit
  (** forward propagation, and merge if argument is an integer *)
end

val wait_dom : (Node.t, D.t) generic_wait
val nodes_reach_repeat_limit : _ Egraph.t -> Node.t CCVector.vector

val events_attach_dom :
  _ Egraph.t ->
  ?direct:bool ->
  Node.t ->
  (Egraph.rt -> Node.t -> Events.enqueue) ->
  unit

val events_attach_any_dom :
  _ Egraph.t -> (Egraph.rt -> Node.t -> Events.enqueue) -> unit

val daemon_attach_dom :
  _ Egraph.t ->
  ?once:unit ->
  ?filter:(Egraph.ro Egraph.t -> Node.t -> bool) ->
  ?direct:bool ->
  Node.t ->
  (Egraph.rw Egraph.t -> Node.t -> unit) ->
  unit

val daemon_attach_any_dom :
  _ Egraph.t ->
  ?once:unit ->
  ?filter:(Egraph.ro Egraph.t -> Node.t -> bool) ->
  (Egraph.rw Egraph.t -> Node.t -> unit) ->
  unit

val daemonwithfilter_attach_any_dom :
  _ Egraph.t ->
  ?thterm:ThTerm.t ->
  (Egraph.rt -> Node.t -> (Egraph.wt -> unit) option) ->
  unit
