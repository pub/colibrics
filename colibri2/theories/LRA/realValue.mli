(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

module Builtin : sig
  type 'a Builtin.t +=
    | BV2Nat of int
    | Pow_int_int
    | Pow_real_int
    | Int2BV of int
    | Sqrt

  val abs_real : Expr.Term.Const.t
  val abs_int : Expr.Term.Const.t
  val colibri_ceil : Expr.Term.Const.t
  val colibri_floor : Expr.Term.Const.t
  val colibri_truncate : Expr.Term.Const.t
  val colibri_truncate_to_int : Expr.Term.Const.t
  val colibri_sqrt : Expr.Term.Const.t
  val colibri_pow_int_int : Expr.Term.Const.t
  val colibri_pow_real_int : Expr.Term.Const.t
  val colibri_cdiv : Expr.Term.Const.t
  val colibri_crem : Expr.Term.Const.t
  val int2bv : int -> Expr.Term.Const.t
  val bv2nat : int -> Expr.Term.Const.t
end

include Value.S with type s = A.t

val set : Egraph.wt -> Node.t -> A.t -> unit
val get : Egraph.wt -> Node.t -> A.t option
val cst' : A.t -> t
val cst : A.t -> Node.t
val zero : Node.t
val one : Node.t
val minus_one : Node.t
val init : Egraph.wt -> unit
val positive_int_sequence : Z.t Base.Sequence.t
val int_sequence : Z.t Base.Sequence.t
val q_sequence : _ Egraph.t -> A.t Base.Sequence.t
val unsigned_bitv : int -> A.t -> Z.t
val signed_bitv : int -> A.t -> Z.t
