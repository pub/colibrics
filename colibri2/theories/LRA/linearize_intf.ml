(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2024                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

module T = struct
  open Base

  type t = {
    id : int;
    x : Node.t;
    (* y may be the same node as x *)
    y : Node.t;
    (* result node *)
    z : Node.t;
  }
  [@@deriving eq, ord, hash, show { with_path = false }]
  (** sign(node) = sign and |node| = |xy| *)
end

include T
include Popop_stdlib.MkDatatype (T)

module Log = Debug.RegisterDebugFlag (struct
  let options = { Debug.defaults with is_info = true }
  let tag = "linearize"

  let desc =
    format_of_string
      "Approximate non-linear products by boundaries of linear equations"
end)
