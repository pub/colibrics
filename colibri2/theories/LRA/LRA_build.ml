(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

(** Helpers to remove *)

module LastEffort = struct
  let a = Expr.Term.Var.mk "a" Expr.Ty.real
  let b = Expr.Term.Var.mk "b" Expr.Ty.real
  let ta = Expr.Term.of_var a
  let tb = Expr.Term.of_var b
  let c = Expr.Term.Var.mk "c" Expr.Ty.real
  let d = Expr.Term.Var.mk "d" Expr.Ty.real
  let tc = Expr.Term.of_var c
  let td = Expr.Term.of_var d
  let tadd = Expr.Term.Real.add ta tb

  let add d na nb =
    let subst =
      {
        Ground.Subst.empty with
        term = Expr.Term.Var.M.of_list [ (a, na); (b, nb) ];
      }
    in
    Ground.convert ~subst d tadd

  let tadd' = Expr.Term.Real.(add (mul tc ta) (mul td tb))

  let add' env qa na qb nb =
    let qa = RealValue.cst qa in
    let qb = RealValue.cst qb in
    let subst =
      {
        Ground.Subst.empty with
        term = Expr.Term.Var.M.of_list [ (a, na); (b, nb); (c, qa); (d, qb) ];
      }
    in
    Ground.convert ~subst env tadd'

  let tsub = Expr.Term.Real.sub ta tb

  let sub env na nb =
    let subst =
      {
        Ground.Subst.empty with
        term = Expr.Term.Var.M.of_list [ (a, na); (b, nb) ];
      }
    in
    Ground.convert ~subst env tsub

  let tmult_cst = Expr.Term.Real.(mul tc ta)

  let mult_cst env qa na =
    let qa = RealValue.cst qa in
    let subst =
      {
        Ground.Subst.empty with
        term = Expr.Term.Var.M.of_list [ (a, na); (c, qa) ];
      }
    in
    Ground.convert ~subst env tmult_cst

  let tmult = Expr.Term.Real.(mul ta tb)

  let mult env na nb =
    let subst =
      {
        Ground.Subst.empty with
        term = Expr.Term.Var.M.of_list [ (a, na); (b, nb) ];
      }
    in
    Ground.convert ~subst env tmult

  let tgt_zero = Expr.Term.Real.(gt ta tb)

  let gt_zero env na =
    let subst =
      {
        Ground.Subst.empty with
        term = Expr.Term.Var.M.of_list [ (a, na); (b, RealValue.zero) ];
      }
    in
    Ground.convert ~subst env tgt_zero

  let tge_zero = Expr.Term.Real.(ge ta tb)

  let ge_zero env na =
    let subst =
      {
        Ground.Subst.empty with
        term = Expr.Term.Var.M.of_list [ (a, na); (b, RealValue.zero) ];
      }
    in
    Ground.convert ~subst env tge_zero

  let cmp tcmp env na nb =
    let subst =
      {
        Ground.Subst.empty with
        term = Expr.Term.Var.M.of_list [ (a, na); (b, nb) ];
      }
    in
    Ground.convert ~subst env tcmp

  let tlt = Expr.Term.Real.lt ta tb
  let tle = Expr.Term.Real.le ta tb
  let tgt = Expr.Term.Real.gt ta tb
  let tge = Expr.Term.Real.ge ta tb
  let lt env na nb = cmp tlt env na nb
  let le env na nb = cmp tle env na nb
  let gt env na nb = cmp tgt env na nb
  let ge env na nb = cmp tge env na nb
end
