(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

let debug =
  Debug.register_info_flag ~desc:"Reasoning about <= < in LRA" "simplex"

module LinearizeLog = Linearize_intf.Log

let linearize_flag =
  Options.register "linearize"
    Cmdliner.Arg.(
      value & flag
      & info [ "linearize" ]
          ~docs:"Allow simplex to add linear approximations in some cases")

let simplex_improve_distance =
  Options.register_flag "simplex-improve-distance"
    ~doc:"Use the simplex for refining distance between variables"

let no_fm_simplex =
  Options.register_flag "no-fm-simplex"
    ~doc:"Don't use the simplex for simulating fourier-motskin"

let stats_maximization = Debug.register_stats_int "Simplex.maximization"

(** Compute the convexe hull of simplex variable [v], as a pair of lower and
    upper bounds, based on the value and offset of [v] *)
let convexe_hull (v : Sim.Core.var_info) =
  let bound cmp Sim.Core.{ bvalue = { v; offset }; _ } =
    let b =
      match A.sign offset with
      | 0 -> Bound.Large
      | s when cmp s 0 -> Bound.Strict
      | _ -> assert false
    in
    (v, b)
  in
  ( Base.Option.map ~f:(bound ( > )) v.mini,
    Base.Option.map ~f:(bound ( < )) v.maxi )

type eq = Fm_simplex.eq = {
  inf : Boundary.t;
  p : A.t Node.M.t;
  sup : Boundary.t;
}
(** An equation used for the simplex is a sum of scaled variables possibly
    bounded by inferior and superior bounds *)

(** Compute a pair of simplex bounds from [equation] *)
let equation_simplex_bounds equation =
  (Sim.of_bound_inf equation.inf, Sim.of_bound_sup equation.sup)

(** Pretty-printer of an equation (e.g. inf < poly < sup) *)
let pp_eq fmt { inf; p; sup } =
  Fmt.pf fmt "%a %a %a" Boundary.pp_bound_inf inf Polynome.print_poly p
    Boundary.pp_bound_sup sup

(** Add variable to simplex, given a node *)
let add_variable egraph simplex node =
  let log_debug (min, max) =
    Debug.dprintf6 debug "add_variable %a %a %a" Boundary.pp_bound_inf min
      Node.pp node Boundary.pp_bound_sup max;
    (min, max)
  and add_simplex_var (min, max) = Sim.Assert.var simplex node ?min ?max in
  node
  |> Boundaries.node_bounds egraph
  |> log_debug |> Sim.to_simplex_bounds |> add_simplex_var |> fst

(** Add equation to simplex (either as a polynom or a single variable) *)
let add_equation egraph simplex equation =
  assert (not (Node.M.is_empty equation.p));
  Debug.dprintf2 debug "[Simplex] %a" pp_eq equation;
  let min, max = equation_simplex_bounds equation in
  if Node.M.is_num_elt 1 equation.p then (
    let n, q = Node.M.choose equation.p in
    (* this was normalized earlier to a unit term *)
    assert (A.equal q A.one);
    fst @@ Sim.Assert.var simplex n ?min ?max)
  else
    let p' = Polynome.of_map A.zero equation.p in
    let n = Dom_polynome.node_of_polynome egraph p' in
    fst @@ Sim.Assert.poly simplex (Sim.of_poly equation.p) n ?min ?max

(** Associate a polynom E = 0 to equation (E,inf,sup) in equations. If there is
    already such a mapping, update it to intersect the boundaries of the new and
    existing equations. *)
let bind_polynom_to_equation equations equation =
  let key = Polynome.of_map A.zero equation.p
  and update = function
    | None -> Some equation
    | Some current ->
        Some
          {
            inf = Boundary.inter_inf current.inf equation.inf;
            sup = Boundary.inter_sup current.sup equation.sup;
            p = equation.p;
          }
  in
  Polynome.H.change update equations key;
  equations

(** Get the polynome associated with a node *)
let node_polynome egraph node =
  match Dom_polynome.get_repr egraph node with
  | None -> Polynome.monome A.one (Egraph.find_def egraph node)
  | Some p -> p

(** Build a normalized equation (E',inf',sup') given a polynom E = C and
    boundaries (inf, sup). One term from the polynom is chosen to divide the
    polynom by its factor. The constant C associated with the polynom is made
    equal to zero by moving the boundaries. *)
let polynom_equation polynom (inf, sup) =
  assert (not (Node.M.is_empty polynom.Polynome.poly));
  let _var, factor = Node.M.choose polynom.poly in
  assert (not (A.equal factor A.zero));
  let inverse = A.inv factor in
  let scaled_polynom = Polynome.mult_cst inverse polynom in
  let inf, sup =
    (inf, sup) |> Boundaries.scale inverse
    |> Boundaries.translate (A.neg scaled_polynom.cst)
  in
  { inf; p = scaled_polynom.poly; sup }

(** Get sign in Dom_product domain of node *)
let node_sign egraph node = Dom_product.SolveSign.get_repr egraph node

(** Get abs in Dom_product domain of node *)
let node_abs egraph node = Dom_product.SolveAbs.get_repr egraph node

(** True if [node] is known to be positive according to its sign domain *)
let is_node_positive egraph node =
  Base.Option.exists ~f:Sign_product.(equal one) (node_sign egraph node)

(** True if all nodes in polynom are positively signed *)
let is_positive egraph polynom =
  Node.M.for_all (fun n _ -> is_node_positive egraph n) polynom

(** Merge variables from polynom with the ones in vars *)
let merge_polynom_variables vars polynom =
  Node.M.union_merge (fun _ _ _ -> Some ()) vars polynom.Polynome.poly

(** Update accumulator of equations and variables given a polynom *)
let polynom_equation_fold polynom equation (equations, variables) =
  ( bind_polynom_to_equation equations equation,
    merge_polynom_variables variables polynom )

(** If [node] is associated with a non-empty linear polynom, update accumulator
    [acc] to store its equation and variables. Otherwise leave the accumulator
    unmodified. *)
let maybe_add_node_equation egraph node acc =
  match Dom_interval.get_dom egraph node with
  | None -> acc
  | Some domain ->
      let polynom = node_polynome egraph node in
      if Node.M.is_empty polynom.poly then acc
      else
        let equation =
          domain |> Boundary.domain_bounds |> polynom_equation polynom
        in
        Debug.dprintf6 debug "[Simplex] %a ∈ %a -> %a" Polynome.pp polynom
          Dom_interval.D.pp domain pp_eq equation;
        polynom_equation_fold polynom equation acc

(** When [node] is associated with a positive product of positive terms, update
    [acc] to add each node in the term to the set of simplex equations *)
let maybe_add_node_product_equations egraph node acc =
  match (node_abs egraph node, node_sign egraph node) with
  | Some abs, Some sign
    when Sign_product.(equal one sign)
         && (not (Node.M.is_empty abs.poly))
         && is_positive egraph abs.poly ->
      (* The condition in the when is not needed for soundness, any node
       could be passed down to {!maybe_add_node_equation}; the case is
       restricted for other, mysterious reasons *)
      (* see tests/solve/smt_nra/unsat/div_pos_lt_to_real.smt2 *)
      Debug.dprintf6 debug "[Simplex] %a: %a --- %a" Node.pp node Product.pp abs
        Sign_product.pp sign;
      let fold acc n _ = maybe_add_node_equation egraph n acc in
      Node.M.fold_left fold acc abs.poly
  | _ -> acc

(** Possibly accumulate equations and variables in acc for a node n *)
let equations_fold d acc n =
  Debug.dprintf2 debug "[Simplex] For equation %a:" Node.pp n;
  acc |> maybe_add_node_equation d n |> maybe_add_node_product_equations d n

(** GLOBAL LIST OF COMPARISON NODES FOR SIMPLEX *)

let comparisons = Datastructure.Push.create Node.pp "LRA.simplex.comparisons"
let add_comparison_node n = Datastructure.Push.push comparisons n

let extract_comparisons_equations_and_variables egraph =
  (* Build equations and variables from the {!comparisons} list of nodes *)
  Datastructure.Push.fold comparisons egraph ~f:(equations_fold egraph)
    ~init:(Polynome.H.create 10, Node.S.empty)

let val_of_sol s =
  let (s : Sim.Core.solution) = Lazy.force s in
  let h = Node.H.create 10 in
  Base.List.iter ~f:(fun (n, q) -> Node.H.replace h n q) s.main_vars;
  Base.List.iter ~f:(fun (n, q) -> Node.H.replace h n q) s.slake_vars;
  h

(** Given a simplex environment [env] that is sat and a solution [s], merge
    nodes in [vars] that are equal to each other *)
let find_equalities d vars (env : Sim.Core.t) (s : Sim.Core.solution Lazy.t) =
  (* Env must be sat *)
  let under_approx = Edge.H.create 10 in
  let _ =
    (* initialize graph with (undirected) edges from all distinct node
       pairs (n1, n2), labeling the edges with the following
       information: two flags indicating if the source and target
       nodes are integers, and two algebraic values that represent an
       interval for the signed distance between the linked values. *)
    Node.S.fold_left
      (fun acc from ->
        let is_int_from = Dom_interval.is_integer d from in
        let acc = Node.S.remove from acc in
        Node.S.iter
          (fun to_ ->
            let is_int_to = Dom_interval.is_integer d to_ in
            Edge.H.add_new Impossible under_approx { Edge.from; to_ }
              (is_int_from, is_int_to, A.inf, A.minus_inf))
          acc;
        acc)
      vars vars
  in
  let update_under_approx s =
    let h = val_of_sol s in
    Edge.H.filter_mapi
      (fun { from; to_ } (is_int_from, is_int_to, inf0, sup0) ->
        let dist = A.sub (Node.H.find h to_) (Node.H.find h from) in
        let inf = A.min inf0 dist in
        let sup = A.max sup0 dist in
        if is_int_from || is_int_to then
          if A.le inf A.minus_one || A.le A.one sup then None
          else Some (is_int_from, is_int_to, inf, sup)
        else if A.lt inf A.zero || A.lt A.zero sup then None
        else Some (is_int_from, is_int_to, inf, sup))
      under_approx
  in
  let one_dist env is_int a b =
    Debug.incr stats_maximization;
    Debug.dprintf4 debug "[Simplex] Check max %a - %a" Node.pp a Node.pp b;
    let p' = Polynome.of_list A.zero [ (a, A.one); (b, A.minus_one) ] in
    let p' = Dom_polynome.normalize d p' in
    let p = Sim.of_poly p'.poly in
    let env, opt = Sim.Solve.maximize env p in
    match Sim.Result.get opt env with
    | Unknown -> assert false
    | Unsat _ -> assert false
    | Sat _ -> assert false
    | Unbounded s ->
        Debug.dprintf0 debug "[Simplex] => unbounded";
        update_under_approx s;
        (* s is often not interesting for the update; all at zero (equality), so the
         distance is pushed a little further to get the case of disequality *)
        let eq = polynom_equation p' (Large (A.of_int 2), No) in
        let env' = add_equation d env eq in
        let env' = Sim.Solve.solve env' in
        let s =
          match Sim.Result.get opt env' with
          | Sat s -> s
          | Unbounded s -> s
          | _ -> assert false
        in
        (`Distinct, env, s)
    | Max (m, s) ->
        let m = Lazy.force m in
        let m_v = A.add p'.cst m.max_v.bvalue.v in
        Debug.dprintf2 debug "[Simplex] => Max %a" Sim.Core.R2.print
          m.max_v.bvalue;
        (if m.is_le then Delta.add_le else Delta.add_lt) d a m_v b;
        let notdistinct =
          if is_int then A.lt (A.abs m.max_v.bvalue.v) A.one
          else A.equal m_v A.zero
        in
        if notdistinct then (`NotDistinct, env, s) else (`Distinct, env, s)
  in
  (* test equality and propagate small distance *)
  let rec test_equality env =
    match Edge.H.choose under_approx with
    | None -> env
    | Some (({ from; to_ } as e), (is_int_from, is_int_to, _, _)) -> (
        Edge.H.remove under_approx e;
        let is_int = is_int_from && is_int_to in
        match one_dist env is_int from to_ with
        | `Distinct, env, s ->
            update_under_approx s;
            let env =
              if is_int_from || is_int_to then (
                let _, env, s = one_dist env is_int to_ from in
                update_under_approx s;
                env)
              else env
            in
            test_equality env
        | `NotDistinct, env, s -> (
            update_under_approx s;
            match one_dist env is_int to_ from with
            | `Distinct, env, s ->
                update_under_approx s;
                test_equality env
            | `NotDistinct, env, s ->
                (* "polypaver-bench-exp-3d-chunk-0022.smt2" *)
                update_under_approx s;
                Debug.dprintf4 debug "[Simplex] Found %a = %a" Node.pp from
                  Node.pp to_;
                Egraph.register d from;
                Egraph.register d to_;
                Egraph.merge d from to_;
                test_equality env))
  in
  update_under_approx s;
  test_equality env

(** For each variable in a simplex system, update the integer interval domains
    of its associated node. *)
let update_domains egraph simplex =
  let update_domain node ((var_info : Sim.Core.var_info), _) =
    if Egraph.is_registered egraph node then
      let domain =
        var_info |> convexe_hull |> Dom_interval.D.from_convexe_hull
        |> Option.get
      in
      if not (Dom_interval.D.is_reals domain) then (
        Debug.dprintf4 debug "Propagate %a: %a" Node.pp node Dom_interval.D.pp
          domain;
        Dom_interval.upd_dom egraph node domain)
  in
  if true then Sim.Core.MX.iter update_domain Sim.Core.(simplex.basic);
  if true then Sim.Core.MX.iter update_domain Sim.Core.(simplex.non_basic)

let repeated_nodes d =
  let v = Dom_interval.nodes_reach_repeat_limit d in
  let vars =
    CCVector.fold
      (fun acc n -> Node.S.add (Egraph.find_def d n) acc)
      Node.S.empty v
  in
  CCVector.clear v;
  vars

let bound_repeated_node d env n =
  let find_bound ~max_bound n =
    let p' =
      Polynome.of_list A.zero [ (n, if max_bound then A.one else A.minus_one) ]
    in
    let p' = Dom_polynome.normalize d p' in
    Debug.dprintf4 debug "[Simplex] Find bounds for repeated %a (%a)" Node.pp n
      Polynome.pp p';
    let p = Sim.of_poly p'.poly in
    let env, opt = Sim.Solve.maximize env p in
    match Sim.Result.get opt env with
    | Sim.Core.Unknown -> assert false
    | Sim.Core.Unsat _ -> assert false
    | Sim.Core.Sat _ -> assert false
    | Sim.Core.Unbounded _ -> Debug.dprintf0 debug "      Unbounded"
    | Sim.Core.Max (m, _) ->
        let m = Lazy.force m in
        let m_v = A.add p'.cst m.max_v.bvalue.v in
        let m_v = if max_bound then m_v else A.neg m_v in
        let interval =
          (if max_bound then
             if m.is_le then Dom_interval.D.le else Dom_interval.D.lt
           else if m.is_le then Dom_interval.D.ge
           else Dom_interval.D.gt)
            m_v
        in
        Debug.dprintf2 debug "      Bounded %a" Dom_interval.D.pp interval;
        Dom_interval.upd_dom d n interval
  in
  find_bound ~max_bound:true n;
  find_bound ~max_bound:false n

let add_linearizations egraph acc =
  let add_variable v (eq, vars) = (eq, Node.S.add v vars)
  and add_equation eq (eqs, vars) = (bind_polynom_to_equation eqs eq, vars) in
  let fold acc v =
    let Linearize_intf.{ id; x; y; z } = v in
    LinearizeLog.info "add linearization %a" Linearize_intf.pp v;
    let classify n = Option.map Sign_product.classify (node_sign egraph n) in
    match (classify z, classify x, classify y) with
    | Some PLUS_ONE, Some PLUS_ONE, Some PLUS_ONE
    | Some MINUS_ONE, Some MINUS_ONE, Some MINUS_ONE -> (
        LinearizeLog.debug "(id=%d) same sign for all terms" id;
        let x0, x1 = Boundaries.node_bounds egraph x
        and y0, y1 = Boundaries.node_bounds egraph y
        (* starting from "z = x * y" we add approximations in the
         simplex system with the following pattern:
           k <= kx.x + ky.y + kz.z
      *)
        and add_approximation ~inf ~kx ~ky ~kz =
          LinearizeLog.info
            "(id=%d) add approximation: %a ≤ (%a%a) + (%a%a) + (%a%a)" id A.pp
            inf A.pp kx Node.pp x A.pp ky Node.pp y A.pp kz Node.pp z;
          add_equation
            {
              inf = Large inf;
              sup = No;
              p = Node.M.of_list [ (x, kx); (y, ky); (z, kz) ];
            }
        in
        match (x0, x1, y0, y1) with
        | Large x0, Large x1, Large y0, Large y1 ->
            let open A in
            acc |> add_variable x |> add_variable y |> add_variable z
            |> add_approximation (* -x0.y0 <= −y0.x − x0.y + z *)
                 ~inf:(neg (mul x0 y0))
                 ~kx:(neg y0) ~ky:(neg x0) ~kz:one
            |> add_approximation (* + x0.y1 <= +y1.x + x0.y − z*)
                 ~inf:(mul x0 y1) ~kx:y1 ~ky:x0 ~kz:minus_one
            |> add_approximation (* + x1.y0 <= +y0.x + x1.y − z *)
                 ~inf:(mul x1 y0) ~kx:y0 ~ky:x1 ~kz:minus_one
            |> add_approximation (* - x1.y1 <= −y1.x − x1.y + z *)
                 ~inf:(neg (mul x1 y1))
                 ~kx:(neg y1) ~ky:(neg x1) ~kz:one
        | _ ->
            LinearizeLog.debug "ignore unbounded or strictly bounded nodes";
            acc)
    | _, _, _ ->
        LinearizeLog.error "(id=%d) unexpected case" id;
        acc
  in
  Linearize.fold fold acc

(** Bounds merged are more precise than in Dom_interval *)
let propagate_eqs d eqs =
  Polynome.H.iter
    (fun _ eq ->
      let p = Polynome.of_map A.zero eq.p in
      let node = Dom_polynome.node_of_polynome d p in
      let domain = Boundary.bounds_domain (eq.inf, eq.sup) in
      match domain with
      | None ->
          Debug.dprintf2 debug "contradiction propagate %a" pp_eq eq;
          Egraph.contradiction ()
      | Some domain ->
          if not (Dom_interval.D.is_reals domain) then (
            Debug.dprintf4 debug "Propagate %a: %a" Node.pp node
              Dom_interval.D.pp domain;
            Dom_interval.upd_dom d node domain))
    eqs

(** Build simplex environmenet from {!comparisons} nodes *)

let extract_equations d =
  let eqs_vars = extract_comparisons_equations_and_variables d in
  let eqs, vars = add_linearizations d eqs_vars in
  propagate_eqs d eqs;
  (eqs, vars)

let prepare_simplex_env d (eqs, vars) =
  let add_variable env v = add_variable d env v
  and add_equation _ eq env = add_equation d env eq in
  let env = Sim.Core.empty ~is_int:false ~check_invs:false in
  let env =
    Node.S.fold_left add_variable env vars |> Polynome.H.fold add_equation eqs
  in
  env

let check_unsat env =
  match Sim.Result.get None (Sim.Solve.solve env) with
  | Unknown -> assert false
  | Unbounded _ -> assert false
  | Max (_, _) -> assert false
  | Unsat _ -> Egraph.contradiction ()
  | Sat s -> (env, s)

(** Build and solve simplex environment from {!comparisons} nodes *)
let simplex_normal (d : Egraph.wt) repeated_nodes ((_, vars) as eqs_vars) =
  let env = prepare_simplex_env d eqs_vars in
  let env, s = check_unsat env in
  let env =
    if Options.get d simplex_improve_distance then find_equalities d vars env s
    else env
  in
  if false then update_domains d env;
  Node.S.iter (bound_repeated_node d env) repeated_nodes

let simplex d =
  let eqs_vars = extract_equations d in
  let no_fm_simplex = Options.get d no_fm_simplex in
  if not no_fm_simplex then
    Fm_simplex.fm_simul d ~bound_repeated_nodes:Node.S.empty eqs_vars;
  let repeated_nodes = repeated_nodes d in
  if no_fm_simplex || not (Node.S.is_empty repeated_nodes) then
    simplex_normal d repeated_nodes eqs_vars

let rec simplex_last_effort d =
  let eqs_vars = extract_equations d in
  let env = prepare_simplex_env d eqs_vars in
  let _, s = check_unsat env in
  check_disequalities d s

and check_disequalities d s =
  let h = val_of_sol s in
  Debug.dprintf2 debug "check disequalities :%a"
    (Fmt.iter_bindings ~sep:Fmt.comma Node.H.iter
       (Fmt.pair ~sep:(Fmt.any ":") Node.pp A.pp))
    h;
  let check_node_equation node =
    match Dom_interval.get_dom d node with
    | None -> ()
    | Some domain -> (
        let polynom = node_polynome d node in
        if Node.M.is_empty polynom.poly then ()
        else
          match Polynome.compute (fun n -> Node.H.find h n) polynom with
          | exception Not_found -> ()
          | c ->
              if not (Dom_interval.D.mem c domain) then (
                Debug.dprintf8 debug "Pb found :%a = %a and %a <> %a" Node.pp
                  node Polynome.pp polynom A.pp c Dom_interval.D.pp domain;
                let c_v = RealValue.cst c in
                let lt =
                  Ground.apply' d Expr.Term.Const.Real.lt [] [ node; c_v ]
                in
                Egraph.register d lt;
                Choice.register_global d (Boolean.chobool lt)
                (* DaemonLastEffortLate.schedule_immediately d simplex_last_effort *))
        )
  in
  Datastructure.Push.iter comparisons d ~f:check_node_equation

(** Interface between Simplex and daemon *)
module type SimplexDaemonInterface = sig
  val event_handler : _ Egraph.t -> _ -> Events.enqueue
  (** React to other events by possibly scheduling a call to simplex *)

  val schedule : _ Egraph.t -> unit
  (** Schedule the simplex at a later point in time if not already scheduled *)
end

(** Module that links simplex and event framework *)
module SimplexDaemon : sig
  include Events.T
  include SimplexDaemonInterface
end = struct
  let delay = Events.Delayed_by_and_FixingModel 8

  type runable = unit

  let print_runable = Unit.pp

  let scheduled =
    Datastructure.Ref.create Base.Bool.pp "LRA.simplex.scheduled" false

  let stats_time = Debug.register_stats_time "Simplex.time"
  let stats_run = Debug.register_stats_int "Simplex.run"

  let run d () =
    Debug.dprintf0 debug "[Simplex]";
    Datastructure.Ref.set scheduled d false;
    Debug.incr stats_run;
    Debug.add_time_during stats_time (fun _ -> simplex d)

  let key =
    Events.Dem.create
      (module struct
        type t = unit

        let name = "LRA.simplex"
      end)

  let enqueue_aux d =
    if Datastructure.Ref.get scheduled d then false
    else (
      Datastructure.Ref.set scheduled d true;
      true)

  let event_handler d _ =
    if enqueue_aux d then Events.EnqRun (key, (), None) else Events.EnqAlready

  let schedule d = if enqueue_aux d then Events.new_pending_daemon d key ()
end

module Daemon : SimplexDaemonInterface = SimplexDaemon
(** Daemon as seen by the Simplex *)

(** For a comparison [a op b], return either [a op b] or [b op' a], whichever
    makes the left-hand term the one with the smallest tag; Operation [op'] is
    the symetrical operation of [op] (e.g. xj ≤ xi becomes xi ≥ xj when i < j)
*)
let reorder_terms a b op =
  if Node.compare a b < 0 then (a, b, op) else (b, a, Dom_interval.D.neg op)

(** Keep track of registered nodes (see [register]) *)
let seen = Node.HC.create Fmt.nop "Simplex.seen"

(** If [node] wasn't seen before, register it for simplex computations and
    return true; otherwise, return false *)
let register egraph node =
  if Node.HC.mem seen egraph node then
    (* already exists *)
    false
  else (
    Node.HC.set seen egraph node ();
    Egraph.register egraph node;
    (* newly added *)
    true)

(** {2 Initialization} *)
module Propagate = struct
  let maybe_linearize d =
    if Options.get d linearize_flag then fun list ->
      List.iter (fun n -> Linearize.check d n) list
    else fun _ -> ()

  let maybe_register_node d node to_linearize =
    if register d node then (
      add_comparison_node d node;
      (* register event handlers *)
      maybe_linearize d to_linearize;
      Dom_polynome.attach_repr_change d ~node Daemon.schedule;
      Events.attach_repr d node Daemon.event_handler;
      Dom_interval.events_attach_dom d node Daemon.event_handler;
      (* schedule simplex *)
      Daemon.schedule d)

  module NodePair = struct
    module T = struct
      type t = { a : Node.t; b : Node.t } [@@deriving hash, eq, ord, show]
    end

    include T
    include Popop_stdlib.MkDatatype (T)
  end

  module HNodePair = Datastructure.Hashtbl2 (NodePair)

  let pair_seen = HNodePair.create Fmt.nop "Simplex.pair_seen" (fun _ -> false)

  (** Propagation: a - b \in u <=> res*)
  let cmp d loc ?thterm a b ?r u =
    let n, u =
      if Node.equal a RealValue.zero then (
        maybe_register_node d b [ b ];
        (b, Dom_interval.D.neg u))
      else if Node.equal b RealValue.zero then (
        maybe_register_node d a [ a ];
        (a, u))
      else
        let a, b, u = reorder_terms a b u in
        let poly = Polynome.of_list A.zero [ (a, A.one); (b, A.minus_one) ] in
        let node = Dom_polynome.node_of_polynome d poly in
        maybe_register_node d node [ a; b ];
        let p = NodePair.{ a; b } in
        if not (HNodePair.find pair_seen d p) then (
          HNodePair.set pair_seen d p true;
          Dom_interval.Propagate.sub loc ?thterm d ~a ~b ~r:node;
          Dom_product.propagate_a_cb d ~a ~b ~c:A.minus_one ~res:node);
        (node, u)
    in
    match r with
    | Some r -> Dom_interval.Propagate.bool_equiv_in_dom loc ?thterm d ~r ~a:n u
    | None -> Dom_interval.upd_dom d n u
end

let () = Events.register (module SimplexDaemon)

let init env =
  (* For now a simple last effort scheduling (once by branch) *)
  DaemonLastEffortLate.schedule_immediately env simplex_last_effort;
  let tys_lra = Ground.Ty.S.of_list [ Ground.Ty.real; Ground.Ty.int ] in
  Colibri2_theories_bool.Equality.register_hook_new_disequality env (fun d ns ->
      let n = Node.S.choose ns in
      if not (Ground.Ty.S.disjoint tys_lra (Ground.tys d n)) then
        Node.S.iter
          (fun a ->
            Node.S.iter
              (fun b ->
                if Node.compare a b < 0 then (
                  let poly =
                    Polynome.of_list A.zero [ (a, A.one); (b, A.minus_one) ]
                  in
                  let node = Dom_polynome.node_of_polynome d poly in
                  Egraph.register d node;
                  Dom_interval.upd_dom d node
                    (Option.get
                       (Dom_interval.D.except Dom_interval.D.reals A.zero));
                  Dom_interval.Propagate.sub [%here] d ~r:node ~a ~b;
                  add_comparison_node d node))
              ns)
          ns)
