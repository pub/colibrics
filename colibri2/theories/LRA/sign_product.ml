(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

(** Product of node sign with one constant 1 or -1 *)

module T = struct
  type cst = Pos | Neg [@@deriving eq, ord, hash]

  type power =
    | Odd  (** odd possibly nul *)
    | Even  (** even *)
    | OddNonZero  (** the node can't be zero*)
  [@@deriving eq, ord, hash]

  type t = P of { poly : power Node.M.t; cst : cst } | Zero
  [@@deriving eq, ord, hash]

  let pp_exposant fmt q =
    match q with
    | OddNonZero -> ()
    | Even -> Fmt.string fmt "²"
    | Odd -> Fmt.string fmt "¹"

  let pp fmt = function
    | Zero -> Fmt.pf fmt "0"
    | P v ->
        Fmt.pf fmt "@[%s%a@]"
          (match v.cst with Pos -> "+" | Neg -> "-")
          (* print_not_one v.cst *)
          Fmt.(iter_bindings Node.M.iter (pair Node.pp pp_exposant))
          v.poly
end

include T
include Colibri2_popop_lib.Popop_stdlib.MkDatatype (T)

let of_one_node cl = P { poly = Node.M.singleton cl Odd; cst = Pos }

let of_one_node_non_zero cl =
  P { poly = Node.M.singleton cl OddNonZero; cst = Pos }

let abs_of_one_node cl = P { poly = Node.M.singleton cl Even; cst = Pos }

let is_one_node = function
  | Zero -> None
  | P t ->
      if Node.M.is_num_elt 1 t.poly && T.equal_cst t.cst Pos then
        match Node.M.choose t.poly with
        | _, Even -> None
        | n, (Odd | OddNonZero) -> Some n
      else None

let one = P { cst = Pos; poly = Node.M.empty }
let minus_one = P { cst = Neg; poly = Node.M.empty }
let zero = Zero

let cst a =
  let sgn = A.sign a in
  if sgn = 0 then zero else if sgn > 0 then one else minus_one

let mul_cst c1 c2 =
  match (c1, c2) with Pos, Pos | Neg, Neg -> Pos | Neg, Pos | Pos, Neg -> Neg

let mul p1 p2 =
  match (p1, p2) with
  | Zero, _ | _, Zero -> Zero
  | P p1, P p2 ->
      let poly m1 m2 =
        Node.M.union
          (fun _ c1 c2 ->
            match (c1, c2) with
            | Odd, Odd -> Some Even
            | (OddNonZero | Odd), (OddNonZero | Odd) -> None
            | Even, v | v, Even -> Some v)
          m1 m2
      in
      P { cst = mul_cst p1.cst p2.cst; poly = poly p1.poly p2.poly }

exception Div_zero

let subst_exn p x s =
  match p with
  | Zero -> None
  | P p -> (
      let poly, q = Node.M.find_remove x p.poly in
      match (q, s) with
      | None, _ -> None
      | Some OddNonZero, Zero -> raise Div_zero
      | Some (Odd | OddNonZero), _ -> Some (mul (P { cst = p.cst; poly }) s)
      | Some Even, _ -> Some (mul (mul (P { cst = p.cst; poly }) s) s))

let normalize p ~f =
  match p with
  | Zero -> Zero
  | P p ->
      let add acc cl v =
        let q = f cl in
        match v with Odd | OddNonZero -> mul acc q | Even -> mul (mul acc q) q
      in
      Node.M.fold_left add (P { cst = p.cst; poly = Node.M.empty }) p.poly

type data = power

let nodes = function Zero -> Node.M.empty | P p -> p.poly

type extracted = Plus_one | Minus_one | Zero | Var of Node.t * t | NoNonZero

let extract : t -> extracted = function
  | Zero -> Zero
  | P p -> (
      if Node.M.is_empty p.poly then
        match p.cst with Pos -> Plus_one | Neg -> Minus_one
      else
        match
          Node.M.fold_left
            (fun acc c v ->
              match acc with
              | Some _ -> acc
              | None -> (
                  match v with Odd | Even -> None | OddNonZero -> Some c))
            None p.poly
        with
        | None -> NoNonZero
        | Some x ->
            let p' = P { p with poly = Node.M.remove x p.poly } in
            Var (x, p'))

type presolve = Zero | NonZero | OnePossibleZero of Node.t | Other

let presolve : t -> presolve = function
  | Zero -> Zero
  | P p ->
      Node.M.fold_left
        (fun acc c -> function
          | Even -> Other
          | Odd -> (
              match acc with
              | NonZero -> OnePossibleZero c
              | OnePossibleZero _ | Zero | Other -> Other)
          | OddNonZero -> acc)
        NonZero p.poly

type extract_cst = Pos | Neg | Zero

let extract_cst : t -> extract_cst * t = function
  | Zero -> (Zero, Zero)
  | P t as t' -> (
      match t.cst with Pos -> (Pos, t') | Neg -> (Neg, P { t with cst = Pos }))

let remove n = function
  | T.Zero -> T.Zero
  | P p -> P { p with poly = Node.M.remove n p.poly }

let common pa pb =
  match (pa, pb) with
  | T.Zero, T.Zero -> T.Zero
  | Zero, p | p, Zero -> p
  | P pa, P pb ->
      P
        {
          poly =
            Node.M.inter
              (fun _ v1 v2 ->
                match (v1, v2) with
                | Odd, Odd -> Some Odd
                | (Odd | OddNonZero), (Odd | OddNonZero) -> Some OddNonZero
                | Odd, Even | Even, Odd -> Some Odd
                | OddNonZero, Even | Even, OddNonZero -> Some OddNonZero
                | Even, Even -> Some Even)
              pa.poly pb.poly;
          cst =
            (match (pa.cst, pb.cst) with
            | Neg, Neg -> Neg
            | Pos, Pos | Neg, Pos | Pos, Neg -> Pos);
        }

let mul_cst (pos : cst) a = match pos with Pos -> a | Neg -> A.neg a

type kind = PLUS_ONE | MINUS_ONE | ZERO | NODE of (cst * Node.t) | PRODUCT

let classify = function
  | T.Zero -> ZERO
  | T.P p ->
      if Node.M.is_empty p.poly then
        match p.cst with Pos -> PLUS_ONE | Neg -> MINUS_ONE
      else if Node.M.is_num_elt 1 p.poly then
        let node, v = Node.M.choose p.poly in
        match v with Even -> PRODUCT | Odd | OddNonZero -> NODE (p.cst, node)
      else PRODUCT

let pp_kind fmt = function
  | ZERO -> Format.fprintf fmt "=0"
  | PLUS_ONE -> Format.fprintf fmt "+"
  | MINUS_ONE -> Format.fprintf fmt "-"
  | NODE _ -> Format.fprintf fmt "N"
  | PRODUCT -> Format.fprintf fmt "P"
