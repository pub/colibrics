(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

let debug =
  Debug.register_info_flag ~desc:"for the normalization by pivoting"
    "LRA.pivotWithCst"

let debug_full =
  Debug.register_flag ~desc:"for the normalization by pivoting"
    "LRA.pivotWithCst.full"

module TotalWithCst (P : sig
  module Base : sig
    include Colibri2_popop_lib.Popop_stdlib.Datatype

    val is_empty : t -> bool
  end

  module Cst : sig
    include Colibri2_popop_lib.Popop_stdlib.Datatype

    val add : t -> t -> t
    val sub : t -> t -> t
    val zero : t
  end

  type t

  val dt : t -> Base.t * Cst.t
  val combine : t -> Cst.t -> t
  val name : string

  include Colibri2_popop_lib.Popop_stdlib.Datatype with type t := t

  val of_one_node : Node.t -> t
  val subst : t -> Node.t -> t -> t
  val normalize : t -> f:(Node.t -> t) -> t

  type data

  val nodes : t -> data Node.M.t
  val solve : t -> t -> t Pivot.solve_total
  val new_base : Egraph.wt -> Node.t -> t -> unit
  val empty_base : Egraph.wt -> Node.t -> Cst.t -> unit
end) : sig
  val assume_equality : Egraph.wt -> Node.t -> P.t -> unit
  val get_deltas : _ Egraph.t -> Node.t -> Node.t P.Cst.M.t
  val get_repr : _ Egraph.t -> Node.t -> P.t option
  val get_proxy : _ Egraph.t -> Node.t -> Node.t * P.Cst.t
  val get_node : _ Egraph.t -> P.t -> Node.t option
  val normalize : _ Egraph.t -> P.t -> P.t
  val init : Egraph.wt -> unit

  val attach_repr_change :
    _ Egraph.t -> node:Node.t -> (Egraph.wt -> unit) -> unit

  val attach_any_repr_change :
    _ Egraph.t ->
    (Egraph.wt -> old_repr:Node.t -> repr:Node.t -> delta:P.Cst.t -> unit) ->
    unit
end = struct
  (** The classes defined here are unions of classes in the e-graph. The
      representative in the e-graph is not necessarily the representative here.

      three datastructures:
      - HRepr: (node -> proxy)
      - HUsed: (node -> node* )
      - Hbase: (base -> (node,delta))

      proxy: Representative or Delta to another node

      delta-class , equivalence class x == y if exists c. x == y + c

      invariant: * only one repr by delta-class * if present in used, must be in
      dom as a Repr *)

  open Colibri2_popop_lib

  let any_repr_change :
      (Egraph.wt -> old_repr:Node.t -> repr:Node.t -> delta:P.Cst.t -> unit)
      Datastructure.Push.t =
    Datastructure.Push.create Fmt.nop (P.name ^ ".any_repr_change")

  let attach_any_repr_change d f = Datastructure.Push.push any_repr_change d f

  module Internal = struct
    (** The node given in argument must be already in an equivalence class.
        Except get_repr_or_add which add one to the equivalence class *)

    type events = (Egraph.wt -> unit) Bag.t

    let pp_events = Fmt.using Bag.length Fmt.int

    type proxy =
      | Repr of { p : P.t; deltas : Node.t P.Cst.M.t; events : events }
          (** [proxy = p, proxy = deltas]
              [ forall c,n in deltas, deltas = c + n ] *)
      | Delta of { base : Node.t; delta : P.Cst.t }
          (** [proxy = base  + delta] *)
    [@@deriving show]

    module HRepr : sig
      val get : 'a Egraph.t -> Node.t -> proxy option
      (** [n = proxy] *)

      val get_repr : 'a Egraph.t -> Node.t -> P.t option
      (** [n = proxy] *)

      val uf_find : 'a Egraph.t -> Node.t -> Node.t * P.Cst.t
      (** [uf_find d n] does [n = n' + c] *)

      val is_repr_one_node : _ Egraph.t -> Node.t -> bool

      (** Modifying *)

      val set : 'a Egraph.t -> Node.t -> proxy -> unit
      (** n = proxy *)

      val add_deltas :
        Egraph.rw Egraph.t ->
        base:Node.t ->
        delta:P.Cst.t ->
        deltas:Node.t P.Cst.M.t ->
        events:(Egraph.wt -> unit) Bag.t ->
        unit
      (** base + delta = deltas *)
    end = struct
      (** n = proxy *)
      let dom = Node.HC.create pp_proxy P.name

      let get d n = Node.HC.find_opt dom d n

      let is_repr_one_node d cl =
        match get d cl with
        | None -> true
        | Some (Repr { p; _ }) -> P.equal (P.of_one_node cl) p
        | Some (Delta _) -> false

      let set d n p =
        Debug.dprintf4 debug "[Arith] set_dom %a: %a" Node.pp n pp_proxy p;
        Node.HC.set dom d n p

      (** base is a repr. base + delta = deltas

          We add (delta + deltas) to its equivalence class *)
      let add_deltas d ~base ~delta ~deltas:n_deltas ~events =
        match get d base with
        | None | Some (Delta _) -> assert false
        | Some (Repr { p; deltas; events = events0 }) ->
            let deltas =
              P.Cst.M.union
                (fun _ n n' ->
                  (* Should not have to register here *)
                  Egraph.register d n;
                  Egraph.register d n';
                  Egraph.merge d n n';
                  Some n) (* Keep the old value which is the representative *)
                deltas
                (P.Cst.M.translate (fun x -> P.Cst.sub x delta) n_deltas)
            in
            set d base (Repr { p; deltas; events = Bag.concat events events0 })

      let rec uf_find d cl1 =
        match get d cl1 with
        | None | Some (Repr _) -> (cl1, P.Cst.zero)
        | Some (Delta { base; delta }) ->
            assert (not (Node.equal cl1 base));
            let base', delta' = uf_find d base in
            let delta' = P.Cst.add delta' delta in
            (base', delta')

      let get_repr d cl : P.t option =
        let base, delta = uf_find d cl in
        (* cl = base + delta *)
        let p = get d base in
        match p with
        | None -> None
        | Some (Repr { p; _ }) ->
            assert (
              Node.M.for_all
                (fun n _ ->
                  match get d n with
                  | Some (Repr { p; _ }) -> P.equal (P.of_one_node n) p
                  | _ -> false)
                (P.nodes p));
            Some (P.combine p delta)
        | Some (Delta _) -> assert false
    end

    module HBase : sig
      val find_base : _ Egraph.t -> P.Base.t -> (Node.t * P.Cst.t) option
      (** Find to which node and with which delta the base is associated
          [base = node + cst] *)

      val set_base : _ Egraph.t -> P.Base.t -> Node.t * P.Cst.t -> unit
      (** Set the node and delta associated to a node. Can be set only once
          [base = node + cst] *)
    end = struct
      module H = Datastructure.Memo (P.Base)

      let h =
        H.create Fmt.nop ("H" ^ P.name) (fun c _ -> Context.RefOpt.create c)

      let find_base d b =
        let r = H.find h d b in
        Context.RefOpt.get r

      let set_base d b base_delta =
        let r = H.find h d b in
        assert (Option.is_none (Context.RefOpt.get r));
        Context.RefOpt.set r base_delta
    end

    module HUsed : sig
      val add : _ Egraph.t -> Node.t -> _ Node.M.t -> unit
      (** [add _ n m] node in [m] are used in [n] *)

      val find : _ Egraph.t -> Node.t -> Node.t Bag.t
      (** [find _ n] give in which node [n] is used *)
    end = struct
      let used_in_poly : Node.t Bag.t Node.HC.t =
        Node.HC.create (Bag.pp Node.pp) "used_in_poly"

      let add d cl' new_cl =
        Node.M.iter
          (fun used _ ->
            Node.HC.change
              (function
                | Some b -> Some (Bag.append b cl') | None -> Some (Bag.elt cl'))
              used_in_poly d used)
          new_cl

      let find d n =
        match Node.HC.find used_in_poly d n with
        | exception Not_found -> Bag.empty
        | b -> b
    end

    (** b = base' + delta' *)
    let find_base_uf_find d b =
      match HBase.find_base d b with
      | Some (base, delta) ->
          let base', delta' = HRepr.uf_find d base in
          (* Seems to be the case but why? assert (Node.equal base base'); *)
          Some (base', P.Cst.add delta delta')
      | None -> None

    let init_one d n ~events =
      let p = P.of_one_node n in
      let b, c = P.dt p in
      HRepr.set d n
        (Repr { p; deltas = P.Cst.M.singleton P.Cst.zero n; events });
      HBase.set_base d b (n, P.Cst.(sub zero c));
      p

    let get_repr_or_add d n =
      match HRepr.get_repr d n with
      | Some p -> p
      | None -> init_one d n ~events:Bag.empty

    let set d n new_ ~deltas ~events =
      (* n = deltas *)
      (* n = new_ *)
      Debug.dprintf4 debug_full "[Arith] set %a %a" Node.pp n P.pp new_;
      let b, c = P.dt new_ in
      (* new_ = b + c *)
      (match find_base_uf_find d b with
      | Some (base, delta) ->
          (* b = base + delta
             deltas = n = b + c = base + (delta + c)
          *)
          assert (not (Node.equal n base));
          let delta = P.Cst.add delta c in
          Datastructure.Push.iter any_repr_change d ~f:(fun f ->
              f d ~old_repr:n ~repr:base ~delta);
          HRepr.add_deltas d ~base ~delta ~deltas ~events;
          assert (
            match HRepr.get d n with
            | None | Some (Repr _) -> true
            | Some (Delta _) -> false);
          HRepr.set d n (Delta { base; delta })
      | None ->
          P.new_base d n new_;
          HUsed.add d n (P.nodes new_);
          HRepr.set d n (Repr { p = new_; deltas; events });
          HBase.set_base d b (n, P.Cst.(sub zero c)));
      if P.Base.is_empty b then
        (* n + k = deltas = b + c = 0 + c *)
        P.Cst.M.iter (fun k n -> P.empty_base d n (P.Cst.sub c k)) deltas;
      (* call event handlers *)
      Bag.iter (fun f -> f d) events

    (** [cl = p] *)
    let subst_doms d cl (p : P.t) =
      assert (HRepr.is_repr_one_node d cl);
      Bag.iter
        (fun cl' ->
          match HRepr.get d cl' with
          | None -> assert false (* absurd: can't be used and absent *)
          | Some (Repr { p = q; events; deltas }) ->
              let q_new = P.subst q cl p in
              if not (P.equal q_new q) then
                (* let new_cl = Node.M.set_diff (P.nodes p) (P.nodes q) in
                   add_used d cl' new_cl; *)
                set d cl' q_new ~events ~deltas
          | Some (Delta _) -> (* Old used *) ())
        (HUsed.find d cl);
      match HRepr.get d cl with
      | None ->
          set d cl p ~events:Bag.empty ~deltas:(P.Cst.M.singleton P.Cst.zero cl)
      | Some (Repr { p = q; events; deltas }) ->
          assert (P.equal (P.of_one_node cl) q);
          set d cl p ~events ~deltas
      | Some (Delta _) -> assert false

    let merge d ~repr other =
      let pother = HRepr.get d other in
      let prepr = HRepr.get d repr in
      if Option.is_some pother || Option.is_some prepr then (
        let p1 = get_repr_or_add d other in
        let p2 = get_repr_or_add d repr in
        Debug.dprintf8 debug "[Arith] @[merge %a (%a) -> %a (%a)@]" Node.pp
          other P.pp p1 Node.pp repr P.pp p2;
        match P.solve p1 p2 with
        | AlreadyEqual -> ()
        | Contradiction -> Egraph.contradiction ()
        | Subst (x, p) ->
            Debug.dprintf4 debug_full "[Arith] @[pivot %a=%a@]" Node.pp x P.pp p;
            subst_doms d x p)
  end

  let normalize_add_used d (p : P.t) =
    P.normalize p ~f:(fun cl ->
        let cl = Egraph.find_def d cl in
        Internal.get_repr_or_add d cl)

  let merge d ~repr other =
    assert (not (Egraph.is_equal d repr other));
    let other = Egraph.find d other in
    let repr = Egraph.find d repr in
    Internal.merge d ~repr other

  let print_info_node th n =
    match Internal.HRepr.get th n with
    | None -> []
    | Some (Repr { p }) -> [ (P.name, Fmt.str "%a" P.pp p) ]
    | Some (Delta { base; delta }) ->
        [ (P.name ^ ":pxy", Fmt.str "%a + %a" Node.pp base P.Cst.pp delta) ]

  let init th =
    Egraph.attach_before_merge th merge;
    Egraph.register_node_info_printer th print_info_node

  let normalize d (p : P.t) =
    P.normalize p ~f:(fun cl ->
        let cl = Egraph.find_def d cl in
        match Internal.HRepr.get_repr d cl with
        | None -> P.of_one_node cl
        | Some p -> p)

  let get_node d p =
    let p = normalize d p in
    let b, c = P.dt p in
    match Internal.find_base_uf_find d b with
    | None -> None
    | Some (base, delta) -> (
        match Internal.HRepr.get d base with
        | None | Some (Delta _) -> assert false
        | Some (Repr { deltas; _ }) ->
            (* p = b + c = base + delta + c; base = deltas = res + d (res?, d?)
               p = res + d + delta + c
            *)
            P.Cst.M.find_opt P.Cst.(sub zero (add c delta)) deltas)

  let assume_equality d cl (p1 : P.t) =
    Debug.dprintf4 debug "[PivotWC] solve_one %a = %a" Node.pp cl P.pp p1;
    let cl = Egraph.find_def d cl in
    let p1 = normalize_add_used d p1 in
    let p2 = Internal.HRepr.get_repr d cl in
    if Option.is_some p2 || Node.M.mem cl (P.nodes p1) then (
      let p2 = Internal.get_repr_or_add d cl in
      match P.solve p1 p2 with
      | AlreadyEqual -> ()
      | Contradiction -> Egraph.contradiction ()
      | Subst (x, p) ->
          Debug.dprintf4 debug_full "[Arith] @[pivot %a=%a@]" Node.pp x P.pp p;
          Internal.subst_doms d x p)
    else
      (* This case allows to not substitute when not needed *)
      Internal.subst_doms d cl p1

  let get_deltas d cl =
    let cl = Egraph.find_def d cl in
    let base, _ = Internal.HRepr.uf_find d cl in
    match Internal.HRepr.get d base with
    | None -> P.Cst.M.singleton P.Cst.zero cl
    | Some (Delta _) -> assert false
    | Some (Repr { deltas }) -> deltas

  let get_repr d cl = Internal.HRepr.get_repr d (Egraph.find_def d cl)

  let get_proxy d cl : Node.t * P.Cst.t =
    let cl = Egraph.find_def d cl in
    Internal.HRepr.uf_find d cl

  let attach_repr_change d ~node (f : Egraph.wt -> unit) =
    let node = Egraph.find_def d node in
    let base, _ = Internal.HRepr.uf_find d node in
    let p = Internal.HRepr.get d base in
    match p with
    | None -> ignore (Internal.init_one d node ~events:(Bag.elt f))
    | Some (Repr { p; deltas; events }) ->
        Internal.HRepr.set d base
          (Repr { p; deltas; events = Bag.append events f })
    | Some (Delta _) -> assert false
end
