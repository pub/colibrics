(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

val assume_equality : Egraph.wt -> Node.t -> Polynome.t -> unit
val get_repr : _ Egraph.t -> Node.t -> Polynome.t option

val attach_new_monome :
  _ Egraph.t ->
  (Egraph.wt -> this_node_is_equal_to:Node.t -> A.t -> Node.t -> unit) ->
  unit
(** Warn when a node n is equal to a*n' with a different from 1 *)

val attach_repr_change :
  _ Egraph.t -> node:Node.t -> (Egraph.wt -> unit) -> unit

val attach_any_repr_change :
  _ Egraph.t ->
  (Egraph.wt -> old_repr:Node.t -> repr:Node.t -> delta:A.t -> unit) ->
  unit
(** old_repr = repr + delta *)

val get_deltas : _ Egraph.t -> Node.t -> Node.t A.M.t
val get_proxy : _ Egraph.t -> Node.t -> Node.t * A.t
val node_of_polynome : Egraph.wt -> Polynome.t -> Node.t
val normalize : _ Egraph.t -> Polynome.t -> Polynome.t
val init : Egraph.wt -> unit
