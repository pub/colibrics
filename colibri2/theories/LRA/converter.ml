open Colibri2_core
open MonadAlsoInterp
module DU = Dom_interval.Propagate
module U = Dom_interval.D
module DP = Dom_polynome
module DX = Dom_product

let converter d (f : Ground.t) =
  let thterm = Ground.thterm f in
  let r = Ground.node f in
  let reg n = Egraph.register d n in
  match Ground.sem f with
  (*
       Uninterpreted function and constants
  *)
  | { app = { builtin = Expr.Base; _ }; tyargs = _; args = _; ty }
    when Ground.Ty.equal ty Ground.Ty.real ->
      ()
  | { app = { builtin = Expr.Base; _ }; tyargs = _; args = _; ty }
    when Ground.Ty.equal ty Ground.Ty.int ->
      Dom_interval.upd_dom d r U.integers
  (*
       Simple Arithmetic  
  *)
  | { app = { builtin = Expr.Add _ }; tyargs = []; args; _ } ->
      let minus_neg a b = U.minus b a in
      DU.binop [%here] ~thterm d r args ~tores:U.add ~tofst:minus_neg
        ~tosnd:minus_neg;
      let a, b = IArray.extract2_exn args in
      DP.assume_equality d r
        (Polynome.of_list A.zero [ (a, A.one); (b, A.one) ]);
      DX.propagate_a_cb d ~res:r ~a ~b ~c:A.one
  | { app = { builtin = Expr.Sub _ }; tyargs = []; args; _ } ->
      DU.binop [%here] ~thterm d r args ~tores:U.minus ~tosnd:U.minus
        ~tofst:U.add;
      let a, b = IArray.extract2_exn args in
      DP.assume_equality d r
        (Polynome.of_list A.zero [ (a, A.one); (b, A.minus_one) ]);
      DX.propagate_a_cb d ~res:r ~a ~b ~c:A.minus_one
  | { app = { builtin = Expr.Minus _ }; tyargs = []; args; _ } ->
      let neg = U.mult_cst A.minus_one in
      DU.unop [%here] ~thterm d r args ~tores:neg ~toarg:neg;
      let a = IArray.extract1_exn args in
      DP.assume_equality d r (Polynome.of_list A.zero [ (a, A.minus_one) ])
  | { app = { builtin = Expr.Mul _ }; tyargs = []; args; _ } ->
      let a, b = IArray.extract2_exn args in
      reg a;
      reg b;
      attach [%here] d
        (DU.set r
           (let+ va = DU.get a and+ vb = DU.get b in
            U.mul va vb)
        && DU.set a
             (let* vr = DU.get r and+ vb = DU.get b in
              if U.mem A.zero vb then None else Some (U.div vr vb))
        &&
        (* if va is 0, and vr is not 0, b can't be not zero because vr would be 0. So b is 0. *)
        DU.set b
          (let* va = DU.get a and+ vr = DU.get r in
           if U.mem A.zero va then None else Some (U.div vr va)));
      DX.SolveAbs.assume_equality d r
        (Product.of_list A.one [ (a, Q.one); (b, Q.one) ]);
      DX.SolveSign.assume_equality d r
        (Sign_product.mul
           (Sign_product.of_one_node a)
           (Sign_product.of_one_node b));
      let become_linear d r n other =
        DaemonAlsoInterp.attach_value d n RealValue.key (fun d _ q ->
            Dom_polynome.assume_equality d r
              (Polynome.monome (RealValue.value q) other))
      in
      become_linear d r a b;
      become_linear d r b a
  (*
     Unary function  
  *)
  | { app = { builtin = RealValue.Builtin.Sqrt }; tyargs = []; args; _ } ->
      let a = IArray.extract1_exn args in
      reg a;
      attach [%here] d
        (DU.set r
           (let* da = DU.get a in
            if U.is_zero_or_positive da then
              let ma, mb = U.get_convexe_hull da in
              let ma =
                match ma with
                | None -> assert false
                | Some (q, b) -> Some (A.positive_root q 2, b)
              in
              let mb =
                match mb with
                | None -> None
                | Some (q, b) -> Some (A.positive_root q 2, b)
              in
              U.from_convexe_hull (ma, mb)
            else None));
      Dom_interval.daemon_attach_dom d a ~once:()
        ~filter:Dom_interval.is_zero_or_positive (fun d _ ->
          (* a = res ^ 2 *)
          DX.SolveAbs.assume_equality d a (Product.of_list A.one [ (r, Q.two) ]);
          (* sign res = sign a (don't forget 0 case) *)
          DX.SolveSign.assume_equality d r (Sign_product.of_one_node a))
  | { app = { builtin = Expr.Abs }; tyargs = []; args; _ } ->
      let a = IArray.extract1_exn args in
      reg a;
      attach [%here] d
        (DU.set r
           (let+ da = DU.get a in
            let uniono a b =
              match (a, b) with
              | None, None -> assert false (* da is not empty *)
              | None, Some c | Some c, None -> c
              | Some a, Some b -> U.union a b
            in
            let da_neg = Option.map U.neg @@ U.inter da (U.lt A.zero) in
            let da_pos = U.inter da (U.ge A.zero) in
            uniono da_pos da_neg)
        && DU.set a
             (let+ dr = DU.get r in
              U.union dr (U.neg dr)));
      DX.SolveAbs.assume_equality d r (Product.of_list A.one [ (a, Q.one) ]);
      DX.SolveSign.assume_equality d r (Sign_product.abs_of_one_node a);
      (* a <= abs a *)
      Simplex.Propagate.cmp d [%here] ~thterm a r (U.le A.zero)
      (* TODO? 0 <= abs a + a *)
  | {
   app = { builtin = Expr.Floor _ | Expr.Floor_to_int _ };
   tyargs = [];
   args;
   _;
  } ->
      (* a-1 < floor a <= a ==> -1 < floor a - a <= 0  => 0 <= a - floor a < 1 *)
      let a = IArray.extract1_exn args in
      Egraph.register d a;
      DU.gen_ceil_truncate_floor d r args ~forward:U.floor;
      Simplex.Propagate.cmp d [%here] ~thterm a r U.le_zero_lt_one
  | { app = { builtin = Expr.Ceiling _ }; tyargs = []; args; _ } ->
      (* a <= ceil a < a+1 ==> 0 <= ceil a - a < 1 *)
      let a = IArray.extract1_exn args in
      Egraph.register d a;
      DU.gen_ceil_truncate_floor d r args ~forward:U.ceil;
      Simplex.Propagate.cmp d [%here] ~thterm r a U.le_zero_lt_one
  | { app = { builtin = Expr.Truncate _ }; tyargs = []; args; _ } ->
      (* a-1 < truncate a < a+1*)
      let a = IArray.extract1_exn args in
      reg a;
      DU.gen_ceil_truncate_floor d r args ~forward:U.truncate;
      let u = U.mk_exn A.minus_one Strict Strict A.one in
      Simplex.Propagate.cmp d [%here] ~thterm r a u
  (*
     Modulo/Division  
  *)
  | { app = { builtin = Expr.Div _ }; tyargs = []; args; _ } ->
      let num, den = IArray.extract2_exn args in
      reg num;
      reg den;
      Dom_interval.daemon_attach_dom d den ~once:()
        ~filter:Dom_interval.is_not_zero (fun d _ ->
          attach [%here] d
            (DU.set r
               (let+ va = DU.get num and+ vb = DU.get den in
                U.div va vb)
            && DU.set num
                 (let* vb = DU.get den and+ vr = DU.get r in
                  if U.mem A.zero vb then None else Some (U.mul vr vb))
            &&
            (* if va is 0, and vr is not 0, den can't be not zero because vr would be 0. So den is 0. *)
            DU.set den
              (let* va = DU.get num and+ vr = DU.get r in
               if U.mem A.zero vr then None else Some (U.div va vr)));
          (* num = res * den *)
          DX.SolveAbs.assume_equality d num
            (Product.of_list A.one [ (r, Q.one); (den, Q.one) ]);
          DX.SolveSign.assume_equality d num
            (Sign_product.mul
               (Sign_product.of_one_node r)
               (Sign_product.of_one_node den)))
  | { app = { builtin = Expr.Modulo_t `Int }; tyargs = []; args; _ } ->
      let num, den = IArray.extract2_exn args in
      reg num;
      reg den;
      Dom_interval.daemon_attach_dom d den ~once:()
        ~filter:Dom_interval.is_not_zero (fun d _ -> DX.div_rem_t d num den)
      (* sign(res) <> sign(num) because of the case 0 *)
  | { app = { builtin = Expr.Modulo_f `Int }; tyargs = []; args; _ } ->
      let num, den = IArray.extract2_exn args in
      reg num;
      reg den;
      Dom_interval.daemon_attach_dom d den ~once:()
        ~filter:Dom_interval.is_not_zero (fun d _ ->
          (* sign(res) = sign(den) *)
          DX.SolveSign.assume_equality d r (Sign_product.of_one_node den))
  (*
     Comparison  
  *)
  | {
   app = { builtin = Expr.(Lt ty | Leq ty | Geq ty | Gt ty) as op };
   tyargs = [];
   args;
   _;
  } ->
      let a, b = IArray.extract2_exn args in
      reg a;
      reg b;
      if Boolean.dec_at_literal d then
        Choice.register d f (Boolean.chobool (Ground.node f));
      let of_builtin_op = function
        | Expr.Lt _ -> `Lt
        | Expr.Leq _ -> `Le
        | Expr.Gt _ -> `Gt
        | Expr.Geq _ -> `Ge
        | _ -> assert false
      in
      DU.cmp [%here] ~thterm d (of_builtin_op op) ~r ~a ~b;
      let u =
        match op with
        | Expr.Lt _ -> U.lt A.zero
        | Expr.Leq _ -> U.le A.zero
        | Expr.Gt _ -> U.gt A.zero
        | Expr.Geq _ -> U.ge A.zero
        | _ -> assert false
      in
      let u =
        match ty with `Int -> Opt.get @@ U.inter U.integers u | _ -> u
      in
      Simplex.Propagate.cmp d [%here] ~thterm a b ~r u
  | {
   app =
     {
       builtin =
         Expr.(
           ( Bitv_ult _ | Bitv_ule _ | Bitv_ugt _ | Bitv_uge _ | Bitv_slt _
           | Bitv_sle _ | Bitv_sgt _ | Bitv_sge _ ));
     };
   tyargs = [];
   args = _;
   _;
  } ->
      if Boolean.dec_at_literal d then
        Choice.register d f (Boolean.chobool (Ground.node f))
  | { app = { builtin = Expr.Equal }; tyargs = _; args; _ } ->
      if IArray.length args = 2 then (
        let a, b = IArray.extract2_exn args in
        reg a;
        reg b;
        attach [%here] d
          (DU.setb r
             (let* va = DU.get a and+ vb = DU.get b in
              if U.is_distinct va vb then Some false else None)))
  | { app = { builtin = Expr.Distinct }; tyargs = [ ty ]; args; ty = _ } ->
      if IArray.length args = 2 then (
        let a, b = IArray.extract2_exn args in
        reg a;
        reg b;
        if
          List.exists (Ground.Ty.equal ty)
            [ Ground.Ty.real; Ground.Ty.int; Ground.Ty.rat ]
        then
          DaemonAlsoInterp.attach_value d r Boolean.dom ~once:() (fun d _ v ->
              if Boolean.(BoolValue.equal v value_true) then
                let diff =
                  DP.node_of_polynome d
                    (Polynome.of_list A.zero [ (a, A.one); (b, A.minus_one) ])
                in
                let get_dom d v =
                  Option.value (Dom_interval.get_dom d v) ~default:U.reals
                in
                let d_diff = U.minus (get_dom d a) (get_dom d b) in
                let d_diff = U.except d_diff A.zero in
                match d_diff with
                | None -> raise Egraph.Contradiction
                | Some d_diff -> Dom_interval.upd_dom d diff d_diff))
  (* Others of type int: It should be logged it should not happen *)
  | { app = _; tyargs = _; args = _; ty } when Ground.Ty.equal ty Ground.Ty.int
    ->
      Dom_interval.upd_dom d r U.integers
  | _ -> ()

let init d = Ground.register_converter d converter
