(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

module Builtin = struct
  type _ Expr.t +=
    | BV2Nat of int
    | Pow_int_int
    | Pow_real_int
    | Int2BV of int
    | Sqrt

  let parse_int _env _ast s =
    match int_of_string s with
    | i when i >= 0 -> i
    | _ ->
        invalid_arg "a positive integer"
        (* Type._error env (Ast ast)
         *   (Type.Expected ("a positive integer", None)) *)
    | exception Failure _ -> invalid_arg "a positive integer"
  (* Type._error env (Ast ast) (Type.Expected ("a positive integer", None)) *)

  let abs_real =
    Expr.Id.mk ~name:"colibri_abs_real" ~builtin:Expr.Abs
      (Dolmen_std.Path.global "colibri_abs_real")
      (Expr.Ty.arrow [ Expr.Ty.real ] Expr.Ty.real)

  let abs_int =
    Expr.Id.mk ~name:"colibri_abs_int" ~builtin:Expr.Abs
      (Dolmen_std.Path.global "colibri_abs_int")
      (Expr.Ty.arrow [ Expr.Ty.int ] Expr.Ty.int)

  let colibri_ceil =
    Expr.Id.mk ~name:"colibri_ceil" ~builtin:(Expr.Ceiling `Real)
      (Dolmen_std.Path.global "colibri_ceil")
      (Expr.Ty.arrow [ Expr.Ty.real ] Expr.Ty.real)

  let colibri_floor =
    Expr.Id.mk ~name:"colibri_floor" ~builtin:(Expr.Floor `Real)
      (Dolmen_std.Path.global "colibri_floor")
      (Expr.Ty.arrow [ Expr.Ty.real ] Expr.Ty.real)

  let colibri_truncate =
    Expr.Id.mk ~name:"colibri_truncate" ~builtin:(Expr.Truncate `Real)
      (Dolmen_std.Path.global "colibri_truncate")
      (Expr.Ty.arrow [ Expr.Ty.real ] Expr.Ty.real)

  let colibri_truncate_to_int =
    Expr.Id.mk ~name:"colibri_truncate_to_int" ~builtin:(Expr.Truncate `Real)
      (Dolmen_std.Path.global "colibri_truncate_to_int")
      (Expr.Ty.arrow [ Expr.Ty.real ] Expr.Ty.int)

  let colibri_sqrt =
    Expr.Id.mk ~name:"colibri_sqrt" ~builtin:Sqrt
      (Dolmen_std.Path.global "colibri_sqrt")
      (Expr.Ty.arrow [ Expr.Ty.real ] Expr.Ty.real)

  let colibri_pow_int_int =
    Expr.Id.mk ~name:"colibri_pow_int_int" ~builtin:Pow_int_int
      (Dolmen_std.Path.global "colibri_pow_int_int")
      (Expr.Ty.arrow [ Expr.Ty.int; Expr.Ty.int ] Expr.Ty.int)

  let colibri_pow_real_int =
    Expr.Id.mk ~name:"colibri_pow_real_int" ~builtin:Pow_real_int
      (Dolmen_std.Path.global "colibri_pow_real_int")
      (Expr.Ty.arrow [ Expr.Ty.real; Expr.Ty.int ] Expr.Ty.real)

  let colibri_cdiv =
    Expr.Id.mk ~name:"colibri_cdiv" ~builtin:(Expr.Div_t `Int)
      (Dolmen_std.Path.global "colibri_cdiv")
      (Expr.Ty.arrow [ Expr.Ty.int; Expr.Ty.int ] Expr.Ty.int)

  let colibri_crem =
    Expr.Id.mk ~name:"colibri_crem" ~builtin:(Expr.Modulo_t `Int)
      (Dolmen_std.Path.global "colibri_crem")
      (Expr.Ty.arrow [ Expr.Ty.int; Expr.Ty.int ] Expr.Ty.int)

  let int2bv n =
    Expr.Id.mk ~name:"int2bv" ~builtin:(Int2BV n)
      (Dolmen_std.Path.global "int2bv")
      (Expr.Ty.arrow [ Expr.Ty.int ] (Expr.Ty.bitv n))

  let bv2nat =
    Popop_stdlib.DInt.H.memo
      (fun n ->
        Expr.Id.mk ~name:"bv2nat" ~builtin:(BV2Nat n)
          (Dolmen_std.Path.global "bv2nat")
          (Expr.Ty.arrow [ Expr.Ty.bitv n ] Expr.Ty.int))
      (Popop_stdlib.DInt.H.create 10)

  let () =
    let match_bitv_type t =
      match Expr.Ty.expand_head (Expr.Term.ty t) with
      | { ty_descr = Expr.TyApp ({ builtin = Builtin.Bitv n; _ }, _) } -> n
      | _ -> raise (Expr.Term.Wrong_type (t, Expr.Ty.bitv 0))
    in
    let app1 env s f =
      Dolmen_loop.Typer.T.builtin_term
        (Dolmen_type.Base.term_app1
           (module Dolmen_loop.Typer.T)
           env s
           (fun a -> Expr.Term.apply_cst f [] [ a ]))
    in
    let app2 env s f =
      Dolmen_loop.Typer.T.builtin_term
        (Dolmen_type.Base.term_app2
           (module Dolmen_loop.Typer.T)
           env s
           (fun a b -> Expr.Term.apply_cst f [] [ a; b ]))
    in
    Expr.add_builtins (fun env s ->
        match s with
        | Dolmen_loop.Typer.T.Id { ns = Term; name = Simple "colibri_abs_real" }
          ->
            app1 env s abs_real
        | Dolmen_loop.Typer.T.Id { ns = Term; name = Simple "colibri_abs_int" }
          ->
            app1 env s abs_int
        | Dolmen_loop.Typer.T.Id
            { ns = Term; name = Simple "colibri_pow_int_int" } ->
            app2 env s colibri_pow_int_int
        | Dolmen_loop.Typer.T.Id
            { ns = Term; name = Simple "colibri_pow_real_int" } ->
            app2 env s colibri_pow_real_int
        | Dolmen_loop.Typer.T.Id { ns = Term; name = Simple "colibri_cdiv" } ->
            app2 env s colibri_cdiv
        | Dolmen_loop.Typer.T.Id { ns = Term; name = Simple "colibri_ceil" } ->
            app1 env s colibri_ceil
        | Dolmen_loop.Typer.T.Id { ns = Term; name = Simple "colibri_floor" } ->
            app1 env s colibri_floor
        | Dolmen_loop.Typer.T.Id { ns = Term; name = Simple "colibri_truncate" }
          ->
            app1 env s colibri_truncate
        | Dolmen_loop.Typer.T.Id
            { ns = Term; name = Simple "colibri_truncate_to_int" } ->
            app1 env s colibri_truncate_to_int
        | Dolmen_loop.Typer.T.Id { ns = Term; name = Simple "colibri_sqrt" } ->
            app1 env s colibri_sqrt
        | Dolmen_loop.Typer.T.Id { ns = Term; name = Simple "colibri_crem" } ->
            app2 env s colibri_crem
        | Dolmen_loop.Typer.T.Id { ns = Term; name = Simple "bv2nat" } ->
            Dolmen_loop.Typer.T.builtin_term
              (Dolmen_type.Base.term_app1
                 (module Dolmen_loop.Typer.T)
                 env s
                 (fun a ->
                   Expr.Term.apply_cst (bv2nat (match_bitv_type a)) [] [ a ]))
        | Dolmen_loop.Typer.T.Id
            {
              ns = Term;
              name = Indexed { basename = "int2bv"; indexes = [ n ] };
            } ->
            let n = parse_int env s n in
            app1 env s (int2bv n)
        | _ -> `Not_found)
end

module RealValue = Value.Register (struct
  include A

  let name = "A"
end)

include RealValue

let () =
  Interp.Register.print_value_smt RealValue.key (fun _ _ty fmt v ->
      (* TODO print int as int and real as real using ty *)
      let v = RealValue.value v in
      match v with
      | Q v ->
          if Z.equal v.den Z.one then Z.pp_print fmt v.num
          else Fmt.pf fmt "(/ %a %a)" Z.pp_print v.num Z.pp_print v.den
      | A v ->
          let qqbar = Calcium.CA.to_qqbar ~ctx:A.ctx v in
          let poly = Calcium.QQBAR.poly qqbar in
          let max_deg = Flint.FMPZ_poly.length poly - 1 in
          (* (root-of-with-interval (coeffs p_0 p_1 ... p_n) min max) *)
          Fmt.pf fmt "(@[root-of-with-interval@ (@[coeffs@ ";
          for i = 0 to max_deg do
            Fmt.pf fmt "%a@ " Z.pp_print (Flint.FMPZ_poly.get_coef poly i)
          done;
          let enclosure = Arb.ACB.real @@ Calcium.QQBAR.enclosure qqbar in
          let mid = Arb.ARB.mid enclosure in
          let mag = Arb.ARB.rad enclosure in
          Fmt.pf fmt "@]) (%a-%a) (%a+%a)@]" Arb.ARF.pp mid Arb.MAG.pp mag
            Arb.ARF.pp mid Arb.MAG.pp mag)

let cst' c = index ~basename:(Format.asprintf "%aR" A.pp c) c
let cst c = node (cst' c)
let zero = cst A.zero
let one = cst A.one
let minus_one = cst A.minus_one
let set d n a = Egraph.set_value d n (nodevalue (index a))

let get env n =
  match Egraph.get_value env n with
  | None -> None
  | Some v -> (
      match of_nodevalue v with None -> None | Some v' -> Some (value v'))

let positive_int_sequence =
  let open Sequence.Generator in
  let rec loop i = yield i >>= fun () -> loop (Z.succ i) in
  run (loop Z.zero)

let strictly_positive_int_sequence =
  let open Sequence.Generator in
  let rec loop i = yield i >>= fun () -> loop (Z.succ i) in
  run (loop Z.one)

let int_sequence_without_zero =
  let open Sequence.Generator in
  let rec loop i =
    yield i >>= fun () ->
    yield (Z.neg i) >>= fun () -> loop (Z.succ i)
  in
  run (loop Z.one)

let int_sequence = Sequence.shift_right int_sequence_without_zero Z.zero

let q_sequence _ =
  let open Std.Sequence in
  let+ num = int_sequence and* den = strictly_positive_int_sequence in
  (* algebraic non rational are not generated *)
  A.of_q (Q.make num den)

let init_ty d =
  Interp.Register.ty d (fun d ty ->
      match ty with
      | { app = { builtin = Expr.Int; _ }; _ } ->
          Some
            (Sequence.map int_sequence ~f:(fun i ->
                 nodevalue (cst' (A.of_bigint i))))
      | { app = { builtin = Expr.Real; _ }; _ } ->
          let seq = q_sequence d in
          let seq =
            Sequence.unfold_with seq ~init:A.S.empty ~f:(fun s v ->
                if A.S.mem v s then Skip { state = s }
                else Yield { value = nodevalue (cst' v); state = A.S.add v s })
          in
          Some seq
      | { app = { builtin = Expr.Bitv n; _ }; _ } ->
          let open Sequence.Generator in
          let rec loop i =
            if Z.numbits i <= n then
              yield (nodevalue (cst' (A.of_bigint i))) >>= fun () ->
              loop (Z.succ i)
            else return ()
          in
          let seq = run (loop Z.zero) in
          Some seq
      | _ -> None)

exception Wrong_arg

let unsigned_bitv n t =
  if not (A.is_unsigned_integer n t) then raise Wrong_arg;
  A.to_z t

let signed_bitv n t = Z.signed_extract (unsigned_bitv n t) 0 n

module Check = struct
  exception ValueNeeded

  let interp d n = Opt.get_exn ValueNeeded (Egraph.get_value d n)

  let compute_ground d t =
    let ( !> ) t = RealValue.value (RealValue.coerce_nodevalue (interp d t)) in
    let bitv n t = unsigned_bitv n !>t in
    let signed_bitv n t = signed_bitv n !>t in
    let ( !< ) v = `Some (RealValue.nodevalue (RealValue.index v)) in
    let ( !<< ) b =
      let r = if b then Boolean.values_true else Boolean.values_false in
      `Some r
    in
    let extract n t = !<(A.of_bigint (Z.extract t 0 n)) in
    let from_bitv n t =
      let t' = A.of_bigint t in
      if not (A.is_unsigned_integer n t') then
        Fmt.epr "@[[BV] %s(%a) is not of size %i@]@."
          (Z.format (Fmt.str "%%0+#%ib" n) t)
          Z.pp_print t n;
      assert (A.is_unsigned_integer n t');
      !<t'
    in
    match Ground.sem t with
    | {
     app = { builtin = Expr.Coercion };
     tyargs =
       ( [
           { app = { builtin = Expr.Int | Expr.Rat; _ }; _ };
           { app = { builtin = Expr.Real; _ }; _ };
         ]
       | [
           { app = { builtin = Expr.Int; _ }; _ };
           { app = { builtin = Expr.Rat; _ }; _ };
         ] );
     args;
    } ->
        let a = IArray.extract1_exn args in
        !<(!>a)
    | { app = { builtin = Expr.Integer s }; tyargs = []; args; ty = _ } ->
        assert (IArray.is_empty args);
        !<(A.of_string s)
    | { app = { builtin = Expr.Decimal s }; tyargs = []; args; ty = _ } ->
        assert (IArray.is_empty args);
        !<(A.of_string_decimal s)
    | { app = { builtin = Expr.Rational s }; tyargs = []; args; ty = _ } ->
        assert (IArray.is_empty args);
        !<(A.of_string_decimal s)
    | { app = { builtin = Expr.Is_int _ }; tyargs = []; args; ty = _ } ->
        let a = IArray.extract1_exn args in
        !<<(A.is_integer !>a)
    | { app = { builtin = Expr.Add _ }; tyargs = []; args; ty = _ } ->
        let a, b = IArray.extract2_exn args in
        !<(A.add !>a !>b)
    | { app = { builtin = Expr.Sub _ }; tyargs = []; args; ty = _ } ->
        let a, b = IArray.extract2_exn args in
        !<(A.sub !>a !>b)
    | { app = { builtin = Expr.Minus _ }; tyargs = []; args; ty = _ } ->
        let a = IArray.extract1_exn args in
        !<(A.neg !>a)
    | { app = { builtin = Expr.Mul _ }; tyargs = []; args; ty = _ } ->
        let a, b = IArray.extract2_exn args in
        !<(A.mul !>a !>b)
    | { app = { builtin = Expr.Div _ }; tyargs = []; args; ty = _ } ->
        let a, b = IArray.extract2_exn args in
        if A.is_zero !>b then `Uninterp else !<(A.div !>a !>b)
    | { app = { builtin = Expr.Div_e _ }; tyargs = []; args; ty = _ } ->
        let a, b = IArray.extract2_exn args in
        let b = !>b in
        let s = A.sign b in
        if s = 0 then `Uninterp else !<(A.div_e !>a b)
    | { app = { builtin = Expr.Div_f _ }; tyargs = []; args; ty = _ } ->
        let a, b = IArray.extract2_exn args in
        let b = !>b in
        let s = A.sign b in
        if s = 0 then `Uninterp else !<(A.div_f !>a b)
    | { app = { builtin = Expr.Div_t _ }; tyargs = []; args; ty = _ } ->
        let a, b = IArray.extract2_exn args in
        let b = !>b in
        let s = A.sign b in
        if s = 0 then `Uninterp else !<(A.div_t !>a b)
    | { app = { builtin = Expr.Modulo_e _ }; tyargs = []; args; ty = _ } ->
        let a, b = IArray.extract2_exn args in
        let b = !>b in
        let s = A.sign b in
        if s = 0 then `Uninterp else !<(A.mod_e !>a b)
    | { app = { builtin = Expr.Modulo_f _ }; tyargs = []; args; ty = _ } ->
        let a, b = IArray.extract2_exn args in
        let b = !>b in
        let s = A.sign b in
        if s = 0 then `Uninterp else !<(A.mod_f !>a b)
    | { app = { builtin = Expr.Modulo_t _ }; tyargs = []; args; ty = _ } ->
        let a, b = IArray.extract2_exn args in
        let b = !>b in
        let s = A.sign b in
        if s = 0 then `Uninterp else !<(A.mod_t !>a b)
    | { app = { builtin = Expr.Divisible }; tyargs = []; args; ty = _ } ->
        let b, a = IArray.extract2_exn args in
        let b = !>b in
        let s = A.sign b in
        if s = 0 then `Uninterp else !<<(A.is_zero (A.mod_t !>a b))
    | { app = { builtin = Builtin.Pow_int_int }; tyargs = []; args; ty = _ } ->
        let a, b = IArray.extract2_exn args in
        let a = !>a in
        let b = !>b in
        if A.sign b < 0 then `Uninterp else !<(A.pow a (A.to_int b))
    | { app = { builtin = Builtin.Pow_real_int }; tyargs = []; args; ty = _ } ->
        let a, b = IArray.extract2_exn args in
        let a = !>a in
        let b = !>b in
        if A.sign b < 0 then `Uninterp else !<(A.pow a (A.to_int b))
    | { app = { builtin = Expr.Lt _ }; tyargs = []; args; ty = _ } ->
        let a, b = IArray.extract2_exn args in
        !<<(A.lt !>a !>b)
    | { app = { builtin = Expr.Leq _ }; tyargs = []; args; ty = _ } ->
        let a, b = IArray.extract2_exn args in
        !<<(A.le !>a !>b)
    | { app = { builtin = Expr.Gt _ }; tyargs = []; args; ty = _ } ->
        let a, b = IArray.extract2_exn args in
        !<<(A.gt !>a !>b)
    | { app = { builtin = Expr.Geq _ }; tyargs = []; args; ty = _ } ->
        let a, b = IArray.extract2_exn args in
        !<<(A.ge !>a !>b)
    | { app = { builtin = Expr.Floor_to_int _ }; tyargs = []; args; _ } ->
        let a = IArray.extract1_exn args in
        !<(A.floor !>a)
    | { app = { builtin = Expr.Abs }; tyargs = []; args; _ } ->
        let a = IArray.extract1_exn args in
        !<(A.abs !>a)
    | { app = { builtin = Expr.Ceiling _ }; tyargs = []; args; _ } ->
        let a = IArray.extract1_exn args in
        !<(A.ceil !>a)
    | { app = { builtin = Expr.Floor _ }; tyargs = []; args; _ } ->
        let a = IArray.extract1_exn args in
        !<(A.floor !>a)
    | { app = { builtin = Expr.Truncate _ }; tyargs = []; args; _ } ->
        let a = IArray.extract1_exn args in
        !<(A.truncate !>a)
    | { app = { builtin = Builtin.Sqrt }; tyargs = []; args; _ } ->
        let a = IArray.extract1_exn args in
        let a = !>a in
        if A.sign a < 0 then `Uninterp else !<(A.positive_root a 2)
    | { app = { builtin = Expr.Bitvec s }; tyargs = []; args; ty = _ } ->
        assert (IArray.is_empty args);
        from_bitv (String.length s) (Z.of_string_base 2 s)
    | { app = { builtin = Expr.Bitv_concat { n; m } }; tyargs = []; args; _ } ->
        let a, b = IArray.extract2_exn args in
        from_bitv (n + m) (Z.logor (Z.shift_left (bitv n a) m) (bitv m b))
    | {
     app = { builtin = Expr.Bitv_extract { n; i; j } };
     tyargs = [];
     args;
     _;
    } ->
        let a = IArray.extract1_exn args in
        from_bitv (i - j + 1) (Z.extract (bitv n a) j (i - j + 1))
    | { app = { builtin = Expr.Bitv_repeat { n; k } }; tyargs = []; args; _ } ->
        let a = IArray.extract1_exn args in
        let rec loop n k acc z =
          if k = 0 then acc
          else
            let acc = Z.logor (Z.shift_left acc n) z in
            loop n (k - 1) acc z
        in
        let a = bitv n a in
        from_bitv (n * k) (loop n k Z.zero a)
    | {
     app = { builtin = Expr.Bitv_zero_extend { n; k } };
     tyargs = [];
     args;
     _;
    } ->
        let a = IArray.extract1_exn args in
        from_bitv (n + k) (bitv n a)
    | {
     app = { builtin = Expr.Bitv_sign_extend { n; k } };
     tyargs = [];
     args;
     _;
    } ->
        let a = IArray.extract1_exn args in
        let a = bitv n a in
        if Z.testbit a (n - 1) then
          extract (n + k) (Z.logor (Z.shift_left Z.minus_one n) a)
        else from_bitv (n + k) a
    | {
     app = { builtin = Expr.Bitv_rotate_left { n; i } };
     tyargs = [];
     args;
     _;
    } ->
        let a = IArray.extract1_exn args in
        let a = bitv n a in
        let k = i mod n in
        extract n (Z.logor (Z.shift_left a k) (Z.extract a (n - k) k))
    | {
     app = { builtin = Expr.Bitv_rotate_right { n; i } };
     tyargs = [];
     args;
     _;
    } ->
        let a = IArray.extract1_exn args in
        let a = bitv n a in
        let k = i mod n in
        extract n (Z.logor (Z.shift_left a (n - k)) (Z.shift_right a k))
    | { app = { builtin = Expr.Bitv_not n }; tyargs = []; args; _ } ->
        let a = IArray.extract1_exn args in
        extract n (Z.lognot (bitv n a))
    | { app = { builtin = Expr.Bitv_and n }; tyargs = []; args; _ } ->
        let a, b = IArray.extract2_exn args in
        from_bitv n (Z.logand (bitv n a) (bitv n b))
    | { app = { builtin = Expr.Bitv_or n }; tyargs = []; args; _ } ->
        let a, b = IArray.extract2_exn args in
        from_bitv n (Z.logor (bitv n a) (bitv n b))
    | { app = { builtin = Expr.Bitv_nand n }; tyargs = []; args; _ } ->
        let a, b = IArray.extract2_exn args in
        extract n (Z.lognot (Z.logand (bitv n a) (bitv n b)))
    | { app = { builtin = Expr.Bitv_nor n }; tyargs = []; args; _ } ->
        let a, b = IArray.extract2_exn args in
        extract n (Z.lognot (Z.logor (bitv n a) (bitv n b)))
    | { app = { builtin = Expr.Bitv_xor n }; tyargs = []; args; _ } ->
        let a, b = IArray.extract2_exn args in
        extract n (Z.logxor (bitv n a) (bitv n b))
    | { app = { builtin = Expr.Bitv_xnor n }; tyargs = []; args; _ } ->
        let a, b = IArray.extract2_exn args in
        extract n (Z.logxor (bitv n a) (Z.lognot (bitv n b)))
    | { app = { builtin = Expr.Bitv_comp n }; tyargs = []; args; _ } ->
        let a, b = IArray.extract2_exn args in
        if Z.equal (bitv n a) (bitv n b) then extract 1 Z.minus_one
        else from_bitv 1 Z.zero
    | { app = { builtin = Expr.Bitv_neg n }; tyargs = []; args; _ } ->
        let a = IArray.extract1_exn args in
        from_bitv n (Z.sub (Z.shift_left Z.one n) (bitv n a))
    | { app = { builtin = Expr.Bitv_add n }; tyargs = []; args; _ } ->
        let a, b = IArray.extract2_exn args in
        extract n (Z.add (bitv n a) (bitv n b))
    | { app = { builtin = Expr.Bitv_sub n }; tyargs = []; args; _ } ->
        let a, b = IArray.extract2_exn args in
        extract n (Z.sub (bitv n a) (bitv n b))
    | { app = { builtin = Expr.Bitv_mul n }; tyargs = []; args; _ } ->
        let a, b = IArray.extract2_exn args in
        extract n (Z.mul (bitv n a) (bitv n b))
    | { app = { builtin = Expr.Bitv_udiv n }; tyargs = []; args; _ } ->
        let a, b = IArray.extract2_exn args in
        let b = bitv n b in
        if Z.equal b Z.zero then extract n Z.minus_one
        else extract n (Z.div (bitv n a) b)
    | { app = { builtin = Expr.Bitv_urem n }; tyargs = []; args; _ } ->
        let a, b = IArray.extract2_exn args in
        let b = bitv n b in
        if Z.equal b Z.zero then from_bitv n (bitv n a)
        else extract n (Z.rem (bitv n a) b)
    | { app = { builtin = Expr.Bitv_sdiv n }; tyargs = []; args; _ } ->
        let a, b = IArray.extract2_exn args in
        let b = signed_bitv n b in
        if Z.equal b Z.zero then extract n Z.one
        else extract n (Z.div (signed_bitv n a) b)
    | { app = { builtin = Expr.Bitv_srem n }; tyargs = []; args; _ } ->
        let a, b = IArray.extract2_exn args in
        let b = signed_bitv n b in
        if Z.equal b Z.zero then from_bitv n (bitv n a)
        else extract n (Z.rem (signed_bitv n a) b)
    | { app = { builtin = Expr.Bitv_smod n }; tyargs = []; args; _ } ->
        let a, b = IArray.extract2_exn args in
        let b = signed_bitv n b in
        let a = signed_bitv n a in
        if Z.equal b Z.zero then `Uninterp
        else extract n (Z.sub a (Z.mul (Z.fdiv a b) b))
    | { app = { builtin = Expr.Bitv_shl n }; tyargs = []; args; _ } ->
        let a, b = IArray.extract2_exn args in
        let b = bitv n b in
        if Z.leq (Z.of_int n) b then from_bitv n Z.zero
        else extract n (Z.shift_left (bitv n a) (Z.to_int b))
    | { app = { builtin = Expr.Bitv_lshr n }; tyargs = []; args; _ } ->
        let a, b = IArray.extract2_exn args in
        let b = bitv n b in
        if Z.leq (Z.of_int n) b then from_bitv n Z.zero
        else extract n (Z.shift_right (bitv n a) (Z.to_int b))
    | { app = { builtin = Expr.Bitv_ashr n }; tyargs = []; args; _ } ->
        let a, b = IArray.extract2_exn args in
        let b = bitv n b in
        let b = if Z.leq (Z.of_int n) b then n else Z.to_int b in
        extract n (Z.shift_right (signed_bitv n a) b)
    | { app = { builtin = Expr.Bitv_ult n }; tyargs = []; args; _ } ->
        let a, b = IArray.extract2_exn args in
        !<<(Z.lt (bitv n a) (bitv n b))
    | { app = { builtin = Expr.Bitv_ule n }; tyargs = []; args; _ } ->
        let a, b = IArray.extract2_exn args in
        !<<(Z.leq (bitv n a) (bitv n b))
    | { app = { builtin = Expr.Bitv_ugt n }; tyargs = []; args; _ } ->
        let a, b = IArray.extract2_exn args in
        !<<(Z.gt (bitv n a) (bitv n b))
    | { app = { builtin = Expr.Bitv_uge n }; tyargs = []; args; _ } ->
        let a, b = IArray.extract2_exn args in
        !<<(Z.geq (bitv n a) (bitv n b))
    | { app = { builtin = Expr.Bitv_slt n }; tyargs = []; args; _ } ->
        let a, b = IArray.extract2_exn args in
        !<<(Z.lt (signed_bitv n a) (signed_bitv n b))
    | { app = { builtin = Expr.Bitv_sle n }; tyargs = []; args; _ } ->
        let a, b = IArray.extract2_exn args in
        !<<(Z.leq (signed_bitv n a) (signed_bitv n b))
    | { app = { builtin = Expr.Bitv_sgt n }; tyargs = []; args; _ } ->
        let a, b = IArray.extract2_exn args in
        !<<(Z.gt (signed_bitv n a) (signed_bitv n b))
    | { app = { builtin = Expr.Bitv_sge n }; tyargs = []; args; _ } ->
        let a, b = IArray.extract2_exn args in
        !<<(Z.geq (signed_bitv n a) (signed_bitv n b))
    | { app = { builtin = Builtin.BV2Nat n }; tyargs = []; args; _ } ->
        let a = IArray.extract1_exn args in
        !<(A.of_bigint (bitv n a))
    | { app = { builtin = Builtin.Int2BV n }; tyargs = []; args; _ } ->
        let a = IArray.extract1_exn args in
        extract n (A.to_z !>a)
    | _ -> `None

  let init d =
    Interp.Register.check d (fun d t ->
        match
          ( (Ground.sem t).ty.app,
            RealValue.of_nodevalue (interp d (Ground.node t)) )
        with
        | { builtin = Expr.Int; _ }, None -> Wrong
        | { builtin = Expr.Int; _ }, Some v
          when not (A.is_integer (RealValue.value v)) ->
            Wrong
        | { builtin = Expr.Bitv _; _ }, None -> Wrong
        | { builtin = Expr.Bitv n; _ }, Some v
          when not (A.is_unsigned_integer n (RealValue.value v)) ->
            Wrong
        | _ -> (
            let check r = Value.equal r (interp d (Ground.node t)) in
            match compute_ground d t with
            | exception Wrong_arg -> Wrong
            | `None -> NA
            | `Some v -> Interp.check_of_bool (check v)
            | `Uninterp ->
                Interp.check_of_bool
                  (Colibri2_theories_quantifiers.Uninterp
                   .On_uninterpreted_domain
                   .check d t)));
    Interp.Register.compute d (fun d t ->
        match compute_ground d t with
        | exception Wrong_arg -> raise Impossible
        | `None -> NA
        | `Some v -> Value v
        | `Uninterp ->
            Colibri2_theories_quantifiers.Uninterp.On_uninterpreted_domain
            .compute d t)

  let attach d g =
    let f d g =
      match compute_ground d g with
      | exception Wrong_arg -> Egraph.contradiction ()
      | `None -> raise Impossible
      | `Some v -> Egraph.set_value d (Ground.node g) v
      | `Uninterp ->
          Colibri2_theories_quantifiers.Uninterp.On_uninterpreted_domain
          .propagate d g
    in
    Interp.WatchArgs.create d f g
end

(** {2 Initialization} *)
let converter d (f : Ground.t) =
  let r = Ground.node f in
  let merge n =
    Egraph.register d n;
    Egraph.merge d r n
  in
  match Ground.sem f with
  | {
   app = { builtin = Expr.Coercion };
   tyargs =
     ( [
         { app = { builtin = Expr.Int | Expr.Rat; _ }; _ };
         { app = { builtin = Expr.Real; _ }; _ };
       ]
     | [
         { app = { builtin = Expr.Int; _ }; _ };
         { app = { builtin = Expr.Rat; _ }; _ };
       ] );
   args;
  } ->
      let a = IArray.extract1_exn args in
      merge a;
      Check.attach d f
  | _ -> (
      match Check.compute_ground d f with
      | exception Check.ValueNeeded ->
          IArray.iter (Ground.sem f).args ~f:(Egraph.register d);
          Check.attach d f
      | `None -> ()
      | `Some v -> Egraph.set_value d (Ground.node f) v
      | `Uninterp ->
          IArray.iter (Ground.sem f).args ~f:(Egraph.register d);
          Check.attach d f)

module Quantifier_skipped = struct
  open Colibri2_theories_quantifiers.Quantifier.Quantifier_skipped

  let coercion a = App (Expr.Coercion, [ TyAny; TyAny ], [ a ])
  let to_int a = App (Expr.Floor_to_int `Real, [], [ a ])
  let equal a b = App (Expr.Equal, [ TyAny ], [ a; b ])
  let le x a b = App (Expr.Leq x, [], [ a; b ])
  let imply a b = App (Expr.Imply, [], [ a; b ])
  let op op a b = App (op, [], [ a; b ])
  let minus x a = App (Expr.Minus x, [], [ a ])
  let x = 1
  let y = 2

  let filters : filter_quant list =
    [
      {
        vars = [ x; y ];
        filter =
          (let x = Var x and y = Var y in
           let morphism f =
             equal
               (coercion (op (f `Int) x y))
               (op (f `Real) (coercion x) (coercion y))
           in
           Choice
             [
               imply (equal (coercion x) (coercion y)) (equal x y);
               imply (le `Int x y) (le `Real (coercion x) (coercion y));
               (* imply (le x y) (le (to_int x) (to_int y)); *)
               morphism (fun x -> Expr.Mul x);
               morphism (fun x -> Expr.Sub x);
               morphism (fun x -> Expr.Add x);
             ]);
      };
      {
        vars = [ x ];
        filter =
          (let x = Var x in
           Choice
             [
               equal (coercion (minus `Int x)) (minus `Real (coercion x));
               equal (to_int (coercion x)) x;
             ]);
      };
    ]

  let register (q : Ground.ClosedQuantifier.s) = List.exists (match_ q) filters
end

let keep_relations =
  Options.register ~pp:Fmt.bool "LRA.keep_relations"
    Cmdliner.Arg.(
      value & flag & info [ "lra-keep-relations-in-patterns" ] ~doc:"")

let init env =
  Ground.register_converter env converter;
  init_ty env;
  Check.init env;
  Colibri2_theories_quantifiers.Info.Builtin_skipped_for_trigger.register env
    (function
    | Expr.Leq _ | Expr.Geq _ | Expr.Lt _ | Expr.Gt _ | Expr.Equal | Expr.Abs ->
        not (Options.get env keep_relations)
    | _ -> false);
  Colibri2_theories_quantifiers.Quantifier.Quantifier_skipped.register env
    Quantifier_skipped.register
