(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

(* val assume_equality : Egraph.wt -> Node.t -> Product.t -> unit *)

val node_of_product : _ Egraph.t -> Product.t -> Sign_product.t -> Node.t

module SolveAbs : sig
  val get_repr : _ Egraph.t -> Node.t -> Product.t option
  val assume_equality : Egraph.wt -> Node.t -> Product.t -> unit
end

module SolveSign : sig
  val get_repr : _ Egraph.t -> Node.t -> Sign_product.t option
  val assume_equality : Egraph.wt -> Node.t -> Sign_product.t -> unit
end

val init : Egraph.wt -> unit

val propagate_a_cb :
  _ Egraph.t -> res:Node.t -> a:Node.t -> c:A.t -> b:Node.t -> unit

val div_rem_t : Egraph.rw Egraph.t -> Node.t -> Node.t -> unit
