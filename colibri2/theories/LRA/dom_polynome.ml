(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

let debug =
  Debug.register_info_flag ~desc:"for the arithmetic theory of polynome"
    "LRA.polynome"

module T = struct
  include Polynome

  let ty _ = Ground.Ty.real
  let name = "SARITH_POLY"
end

module ThE = ThTerm.Register (T)

let attach_new_monome _ _ = ()

include PivotWithCst.TotalWithCst (struct
  module Base = struct
    include Polynome.Poly

    let is_empty = Node.M.is_empty
  end

  module Cst = A
  include Polynome

  let dt p = (p.poly, p.cst)
  let combine = Polynome.add_cst
  let name = "LRA.polynome"

  type data = A.t

  let nodes p = p.poly
  let of_one_node n = monome A.one n
  let subst p n q = fst (subst p n q)

  let normalize p ~f =
    let add acc cl c = Polynome.x_p_cy acc c (f cl) in
    Polynome.fold add (Polynome.cst p.cst) p

  let solve p1 p2 : _ Pivot.solve_total =
    let diff = sub p1 p2 in
    (* 0 = other - repr = p1 - p2 = diff *)
    Debug.dprintf2 debug "[Arith] @[solve 0=%a@]" pp diff;
    match Polynome.extract diff with
    | Zero -> Pivot.AlreadyEqual
    | Cst c ->
        (* 0 = cst <> 0 *)
        Debug.dprintf2 Colibri2_core.Debug.contradiction
          "[LRA/Poly] Found 0 = %a when merging" A.pp c;
        Contradiction
    | Var (q, x, p') ->
        assert (not (A.equal A.zero q));
        (* diff = qx + p' *)
        Subst (x, Polynome.mult_cst (A.div A.one (A.neg q)) p')

  let new_base _ _ _ = ()
  let empty_base = RealValue.set
end)

let node_of_polynome d p =
  match Polynome.is_cst p with
  | None -> (
      match get_node d p with
      | Some n -> n
      | None ->
          let n = ThE.node (ThE.index p) in
          Egraph.register d n;
          assume_equality d n p;
          n)
  | Some q -> RealValue.node (RealValue.index q)

(** {2 Initialization} *)

let init env =
  init env;
  DaemonAlsoInterp.attach_reg_value env RealValue.key (fun d value ->
      let v = RealValue.value value in
      let cl = RealValue.node value in
      let p1 = Polynome.of_list v [] in
      assume_equality d cl p1);
  Disequality.register_disequal env (fun d n1 n2 ->
      match (get_repr d n1, get_repr d n2) with
      | Some p1, Some p2 -> (
          match Polynome.is_cst (Polynome.sub p1 p2) with
          | Some c when A.is_not_zero c -> true
          | _ -> false)
      | _ -> false)
