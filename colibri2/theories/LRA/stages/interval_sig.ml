(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib
open Colibri2_stdlib.Std

module type Union = sig
  type t'' = Sin of A.t * t'' | Chg of A.t * Bound.t * t'' | Inf
  [@@deriving eq, ord, hash]

  type t' = On of t'' | Off of t'' [@@deriving eq, ord, hash]
  type integrability = Integer | MaybeReal [@@deriving eq, ord, hash]
  type t = { integrability : integrability; u : t' } [@@deriving eq, ord, hash]
end

module type S = sig
  include Popop_stdlib.Datatype

  val is_distinct : t -> t -> bool

  type is_comparable = Gt | Lt | Ge | Le | Eq | Uncomparable

  val is_comparable : t -> t -> is_comparable
  val is_included : t -> t -> bool
  val mult_cst : A.t -> t -> t
  val add_cst : A.t -> t -> t
  val add : t -> t -> t
  val mul : t -> t -> t
  val div : t -> t -> t
  val inv : t -> t
  val minus : t -> t -> t
  val neg : t -> t
  val complement : t -> t option
  val floor : t -> t
  val ceil : t -> t
  val truncate : t -> t
  val relu : t -> t
  val mem : A.t -> t -> bool

  val singleton : A.t -> t
  (** from A.t *)

  val is_singleton : t -> A.t option
  val except : t -> A.t -> t option

  type split_heuristic = Singleton of A.t | Splitted of t * t | NotSplitted

  val split_heuristic : t -> split_heuristic
  val absent : A.t -> t -> bool
  val is_integer : t -> bool
  val integers : t
  val gt : A.t -> t
  val ge : A.t -> t
  val lt : A.t -> t

  val le : A.t -> t
  (** > q, >= q, < q, <= q *)

  val le' : t -> t
  val lt' : t -> t
  val ge' : t -> t
  val gt' : t -> t

  val union : t -> t -> t
  (** union set *)

  val inter : t -> t -> t option
  (** intersection set. if the two arguments are equals, return the second *)

  val zero : t
  val reals : t

  val is_reals : t -> bool
  (** R *)

  val choose : t -> A.t
  (** Nothing smart in this choosing *)

  val invariant : t -> bool

  (* val choose_rnd : (int -> int) -> t -> A.t
   * (\** choose an element randomly (but non-uniformly), the given function is
   *       the random generator *\)
   *)

  val get_convexe_hull : t -> (A.t * Bound.t) option * (A.t * Bound.t) option

  val from_convexe_hull :
    (A.t * Bound.t) option * (A.t * Bound.t) option -> t option

  module Union : sig
    type t'' = private Sin of A.t * t'' | Chg of A.t * Bound.t * t'' | Inf
    [@@deriving eq, ord, hash]

    val gen_t'' : t'' QCheck2.Gen.t

    type t' = private On of t'' | Off of t'' [@@deriving eq, ord, hash]

    val gen_t' : t' QCheck2.Gen.t

    type integrability = Integer | MaybeReal [@@deriving eq, ord, hash]

    val gen_integrability : integrability QCheck2.Gen.t

    type t = { integrability : integrability; u : t' }
    [@@deriving eq, ord, hash]

    val gen : t QCheck2.Gen.t
  end

  val get_union : t -> Union.t
  val reduce_integers : t -> t option
end

module Union_Helpers (U : Union) = struct
  open Base
  open Bound
  open U

  let gen_q = A.gen

  let gen_bound =
    QCheck2.Gen.frequency
      [ (1, QCheck2.Gen.pure Strict); (1, QCheck2.Gen.pure Large) ]

  let gen_t'' =
    let open QCheck2.Gen in
    let elm =
      QCheck2.Gen.frequency
        [
          (1, QCheck2.Gen.map (fun gen0 -> `Sin gen0) gen_q);
          ( 1,
            QCheck2.Gen.map
              (fun (gen0, gen1) -> `Chg (gen0, gen1))
              (QCheck2.Gen.pair gen_q gen_bound) );
        ]
    in
    let+ l = small_list elm in
    let l =
      List.dedup_and_sort l ~compare:(fun a b ->
          let extract = function `Sin q -> q | `Chg (q, _) -> q in
          -A.compare (extract a) (extract b))
    in
    let rec convert acc = function
      | [] -> acc
      | `Sin q :: l -> convert (Sin (q, acc)) l
      | `Chg (q, b) :: l -> convert (Chg (q, b, acc)) l
    in
    convert Inf l

  let gen_t' =
    QCheck2.Gen.frequency
      [
        (1, QCheck2.Gen.map (fun gen0 -> On gen0) gen_t'');
        (1, QCheck2.Gen.map (fun gen0 -> Off gen0) gen_t'');
      ]

  let gen_integrability =
    QCheck2.Gen.frequency
      [ (1, QCheck2.Gen.pure Integer); (1, QCheck2.Gen.pure MaybeReal) ]

  let gen =
    QCheck2.Gen.map
      (fun (gen0, gen1) -> { integrability = gen0; u = gen1 })
      (QCheck2.Gen.pair gen_integrability gen_t')
end
