(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib
open Colibri2_stdlib.Std

let compare_ord a b : Colibrilib.Ord.t =
  let c = A.compare a b in
  Int.(if c = 0 then Eq else if c <= 0 then Lt else Gt)

module P = Colibrilib.Interval.Union.Make (struct
  include A

  let infix_eq = equal
  let infix_lseq = le
  let infix_ls = lt
  let infix_pl = add
  let infix_mn = sub
  let infix_as = mul
  let infix_sl = div
  let compare = compare_ord
  let of_int = A.of_bigint
  let truncate a = A.to_z (A.truncate a)
  let floor a = A.to_z (A.floor a)
  let ceil a = A.to_z (A.ceil a)
end)

open P
open P.Increasing.Unary
open P.Increasing.Binary
open P.NonIncreasing

module Union = struct
  module U = struct
    type q = A.t [@@deriving eq, ord, hash]

    type bound = Colibrilib.Interval.Bound.t = Strict | Large
    [@@deriving eq, ord, hash]

    type t'' = P.t'' = Sin of q * t'' | Chg of q * bound * t'' | Inf
    [@@deriving eq, ord, hash]

    type t' = P.t' = On of t'' | Off of t'' [@@deriving eq, ord, hash]
    type integrability = Integer | MaybeReal [@@deriving eq, ord, hash]

    type t = { integrability : integrability; u : t' }
    [@@deriving eq, ord, hash]
  end

  include U
  include Colibri2_theories_LRA_stages_def.Interval_sig.Union_Helpers (U)
end

include Union

let get_union x = x

let print_bound fmt = function
  | Large -> Format.fprintf fmt "["
  | Strict -> Format.fprintf fmt "]"

let rec pp_on fmt = function
  | Sin (q, on) ->
      Format.fprintf fmt "%a[∪]%a;" A.pp q A.pp q;
      pp_on fmt on
  | Chg (q, b, off) ->
      Format.fprintf fmt "%a%a" A.pp q print_bound b;
      pp_off ~first:false fmt off
  | Inf -> Format.fprintf fmt "+∞["

and pp_off ~first fmt = function
  | Sin (q, off) ->
      if not first then Format.fprintf fmt "∪";
      Format.fprintf fmt "{%a}" A.pp q;
      pp_off ~first:false fmt off
  | Chg (q, b, on) ->
      if not first then Format.fprintf fmt "∪";
      Format.fprintf fmt "%a%a;" print_bound b A.pp q;
      pp_on fmt on
  | Inf -> ()

let pp_t' fmt t =
  match t with
  | On on ->
      Format.fprintf fmt "]-∞;";
      pp_on fmt on
  | Off off -> pp_off ~first:true fmt off

let pp fmt t =
  (match t.integrability with Integer -> Fmt.pf fmt "ℕ" | MaybeReal -> ());
  pp_t' fmt t.u

include Popop_stdlib.MkDatatype (struct
  type nonrec t = t

  let equal = equal
  let compare = compare
  let hash = hash
  let hash_fold_t = hash_fold_t
  let pp = pp
end)

let invariant _ = true (* proved in why3 *)
let inv : Bound.t -> Bound.t = function Strict -> Large | Large -> Strict

let of_bound : Colibrilib.Interval.Bound.t -> Bound.t = function
  | Strict -> Strict
  | Large -> Large

let to_bound : Bound.t -> Colibrilib.Interval.Bound.t = function
  | Strict -> Strict
  | Large -> Large

(** type invariant *)
let get_convexe_hull : t -> (A.t * Bound.t) option * (A.t * Bound.t) option =
 fun t ->
  let inf, sup = get_convexe_hull t.u in
  let conv c = Base.Option.map ~f:(fun (q, b) -> (q, of_bound b)) c in
  let checkint dir b =
    match (b, t.integrability) with
    | x, MaybeReal -> x
    | None, _ -> None
    | Some (z, Bound.Strict), _ when A.is_integer z ->
        Some (A.add (if dir then A.one else A.minus_one) z, Large)
    | Some (z, Bound.Large), _ when A.is_integer z -> b
    | Some (z, _), _ -> Some ((if dir then A.ceil else A.floor) z, Large)
  in
  (checkint true (conv inf), checkint false (conv sup))

let from_convexe_hull (c1, c2) : t option =
  let conv c = Base.Option.map ~f:(fun (q, b) -> (q, to_bound b)) c in
  let ch = (conv c1, conv c2) in
  match from_convexe_hull ch with
  | Some u -> Some { u; integrability = MaybeReal }
  | None -> None

type split_heuristic = Singleton of A.t | Splitted of t * t | NotSplitted

let split_heuristic _ = NotSplitted

let choose : t -> A.t =
  let get_first_on = function
    | Sin (q, _) | Chg (q, Strict, _) -> A.sub q A.one
    | Chg (q, Large, off) -> q
    | Inf -> A.zero
  in
  let get_after_on q' = function
    | Sin (q, _) | Chg (q, Strict, _) ->
        let p = A.sub q A.one in
        if A.lt q' p then p else A.div (A.add q' q) A.two
    | Chg (q, Large, _) -> q
    | Inf -> A.add q' A.one
  in
  let get_first_off = function
    | Sin (q, _) | Chg (q, Large, _) -> q
    | Chg (q, Strict, on) -> get_after_on q on
    | Inf -> assert false
  in
  function
  | { u = On on } -> get_first_on on
  | { u = Off off } -> get_first_off off

let is_reals = function
  | { u = On Inf; integrability = MaybeReal } -> true
  | _ -> false

let is_integer = function
  | { u = On Inf; integrability = Integer } -> true
  | _ -> false

let ceilb q b =
  match b with
  | Large -> A.ceil q
  | Strict -> if A.is_integer q then A.add q A.one else A.ceil q

let floorb q b =
  match b with
  | Large -> A.floor q
  | Strict -> if A.is_integer q then A.sub q A.one else A.floor q

let reduce_integers_aux lu =
  let add_on start q l =
    if A.lt q start then l
    else if A.equal start q then (* off *) Sin (start, l)
    else Chg (start, Large, Chg (q, Strict, l))
  in
  let rec aux_on start = function
    | Sin (q, l) ->
        let qf = floorb q Strict in
        let qc = ceilb q Strict in
        add_on start qf (aux_on qc l)
    | Chg (q, b, l) ->
        let qf = floorb q (Colibrilib.Interval.Bound.inv_bound b) in
        add_on start qf (aux_off l)
    | Inf -> Chg (start, Large, Inf)
  and aux_off = function
    | Sin (q, l) when A.is_integer q -> Sin (q, aux_off l)
    | Sin (q, l) -> aux_off l
    | Chg (q, b, l) ->
        let q = ceilb q b in
        aux_on q l
    | Inf -> Inf
  in
  let rec aux_on_inf = function
    | Sin (q, l) ->
        let qf = floorb q Strict in
        let qc = ceilb q Strict in
        Chg (qf, Strict, aux_on qc l)
    | Chg (q, b, l) ->
        let qf = floorb q (Colibrilib.Interval.Bound.inv_bound b) in
        Chg (qf, Strict, aux_off l)
    | Inf -> Inf
  in
  match lu with
  | On u -> Some (On (aux_on_inf u))
  | Off u -> ( match aux_off u with Inf -> None | u -> Some (Off u))

let reduce_integers l =
  Option.map (fun u -> { integrability = Integer; u }) (reduce_integers_aux l.u)

let reals = { u = On Inf; integrability = MaybeReal }
let integers = { u = On Inf; integrability = Integer }
let zero = { u = Off (Sin (A.zero, Inf)); integrability = Integer }
let gt q = { u = gt q; integrability = MaybeReal }
let ge q = { u = ge q; integrability = MaybeReal }
let lt q = { u = lt q; integrability = MaybeReal }
let le q = { u = le q; integrability = MaybeReal }
let gt' q = { u = gt' q.u; integrability = MaybeReal }
let ge' q = { u = ge' q.u; integrability = MaybeReal }
let lt' q = { u = lt' q.u; integrability = MaybeReal }
let le' q = { u = le' q.u; integrability = MaybeReal }

let union t1 t2 =
  {
    integrability =
      (match (t1.integrability, t2.integrability) with
      | Integer, Integer -> Integer
      | _ -> MaybeReal);
    u = union t1.u t2.u;
  }

let inter t1 t2 =
  let inter_aux integrability u1 u2 =
    Option.map (fun u -> { integrability; u }) (inter u1 u2)
  in
  match (t1.integrability, t2.integrability, t1.u, t2.u) with
  | Integer, MaybeReal, u1, u2 | MaybeReal, Integer, u2, u1 ->
      Option.bind (reduce_integers_aux u2) (fun u2 -> inter_aux Integer u1 u2)
  | MaybeReal, MaybeReal, u1, u2 ->
      Option.map
        (fun { integrability; u } ->
          match u with
          | Off (Sin (q, Inf)) when A.is_integer q ->
              { integrability = Integer; u }
          | _ -> { integrability; u })
        (inter_aux MaybeReal u1 u2)
  | Integer, Integer, u1, u2 -> inter_aux Integer u1 u2

let add t1 t2 =
  {
    integrability =
      (match (t1.integrability, t2.integrability) with
      | Integer, Integer -> Integer
      | _ -> MaybeReal);
    u = add t1.u t2.u;
  }

let mul t1 t2 =
  {
    integrability =
      (match (t1.integrability, t2.integrability) with
      | Integer, Integer -> Integer
      | _ -> MaybeReal);
    u = mul t1.u t2.u;
  }

let inv t = { integrability = MaybeReal; u = P.Increasing.Binary.inv t.u }
let div t1 t2 = { integrability = MaybeReal; u = div t1.u t2.u }

let mem q t =
  (match t.integrability with Integer -> A.is_integer q | MaybeReal -> true)
  && mem q t.u

let absent q t = not (mem q t)

let except t q =
  match t.integrability with
  | Integer when not (A.is_integer q) -> Some t
  | _ -> (
      match except t.u q with
      | Some u -> (
          match t.integrability with
          | Integer -> (
              match reduce_integers_aux u with
              | Some u -> Some { integrability = t.integrability; u }
              | None -> None)
          | _ -> Some { integrability = t.integrability; u })
      | None -> None)

let add_cst q t =
  {
    u = add_cst t.u q;
    integrability =
      (match t.integrability with
      | Integer when A.is_integer q -> Integer
      | _ -> MaybeReal);
  }

let singleton q =
  {
    u = singleton q;
    integrability = (if A.is_integer q then Integer else MaybeReal);
  }

let is_singleton t = is_singleton t.u
let count = 20

let%test_unit "singleton" =
  (* Ocaml_poly.PolyUtils.trace_enable "algebraic_number"; *)
  let test =
    QCheck2.Test.make_cell ~count ~print:A.to_string A.gen (fun q ->
        Option.is_some (is_singleton (singleton q)))
  in
  QCheck2.Test.check_cell_exn test

let interesting_values acc =
  let add q acc = q :: acc in
  let rec fon acc = function
    | Sin (q, on) -> fon (add q acc) on
    | Chg (q, _, off) -> foff (add q acc) off
    | Inf -> acc
  and foff acc = function
    | Sin (q, off) -> foff (add q acc) off
    | Chg (q, _, on) -> fon (add q acc) on
    | Inf -> acc
  in
  function On on -> fon acc on | Off off -> foff acc off

let%test_unit "gen_compare" =
  let gen =
    let open QCheck2.Gen in
    let* true_true = bool
    and* false_true = bool
    and* true_false = bool
    and* false_false = bool
    and* t1 = gen_t'
    and* t2 = gen_t' in
    let+ q =
      oneofl
        (interesting_values
           (interesting_values [ A.zero; A.one; A.minus_one ] t2)
           t1)
    in
    (true_true, false_true, true_false, false_false, t1, t2, q)
  in
  let pp fmt (true_true, false_true, true_false, false_false, t1, t2, q) =
    Fmt.pf fmt "%b %b %b %b: %a and %a at %a" true_true false_true true_false
      false_false pp_t' t1 pp_t' t2 A.pp q
  in
  let print x = Fmt.str "%a" pp x in
  let test =
    QCheck2.Test.make_cell ~name:"on interesting values" ~count ~print gen
      ~max_fail:10
      (fun (true_true, false_true, true_false, false_false, t1, t2, q) ->
        let b1 = P.mem q t1 in
        let b2 = P.mem q t2 in
        let test a b =
          match (a, b) with
          | true, true -> true_true
          | false, true -> false_true
          | true, false -> true_false
          | false, false -> false_false
        in
        let b = for_all test t1 t2 in
        let b' = test b1 b2 in
        (* Format.printf "b = %b; b' = %b; b1 = %b; b2 = %b; %b; r=%a@." b b' b1
         *   b2
         *   (A.lt (A.of_int 4) q)
         *   pp r; *)
        (not b) || b')
  in
  QCheck2.Test.check_cell_exn test

let is_distinct t1 t2 = for_all (fun a b -> (not a) || not b) t1.u t2.u

let is_included t1 t2 =
  (match (t1.integrability, t2.integrability) with
  | _, MaybeReal | Integer, _ -> true
  | MaybeReal, Integer -> false)
  && for_all
       (fun a b -> match (a, b) with true, false -> false | _ -> true)
       t1.u t2.u

type is_comparable = IsComparable.is_comparable =
  | Gt
  | Lt
  | Ge
  | Le
  | Eq
  | Uncomparable

let is_comparable t1 t2 =
  match IsComparable.is_comparable t1.u t2.u with
  | (Lt | Gt | Le | Ge | Uncomparable) as r -> r
  | Eq ->
      if equal_integrability t1.integrability t2.integrability then Eq
      else Uncomparable

let mult_cst x l =
  let integrability =
    match l.integrability with
    | Integer -> if A.is_integer x then Integer else MaybeReal
    | MaybeReal -> MaybeReal
  in
  { u = mult_cst x l.u; integrability }

let neg a = { integrability = a.integrability; u = neg a.u }
let minus a b = add a (neg b)

let%test_unit "mult_cst" =
  let gen =
    let open QCheck2.Gen in
    let* t1 = gen and* cst = A.gen in
    let+ q =
      oneofl (interesting_values [ A.zero; A.one; A.minus_one; cst ] t1.u)
    in
    (t1, cst, q)
  in
  let print' fmt (t1, cst, q) =
    Fmt.pf fmt "%a and %a at %a" pp t1 A.pp cst A.pp q
  in
  let print x = Fmt.str "%a" print' x in
  let test =
    QCheck2.Test.make_cell ~name:"on interesting values" ~count ~print gen
      ~max_fail:1 (fun (t1, cst, q) ->
        let b1 = mem q t1 in
        let t2 = mult_cst cst t1 in
        let b2 = mem (A.mul cst q) t2 in
        (not b1) || b2)
  in
  QCheck2.Test.check_cell_exn test

let ceil a = { integrability = Integer; u = ceil a.u }
let floor a = { integrability = Integer; u = floor a.u }
let truncate a = { integrability = Integer; u = truncate a.u }
let relu a = { integrability = a.integrability; u = relu a.u }

let complement a =
  Base.Option.map (complement a.u) ~f:(fun u ->
      { integrability = MaybeReal; u })
