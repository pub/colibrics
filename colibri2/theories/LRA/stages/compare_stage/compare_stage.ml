(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib
open Colibri2_stdlib.Std

module type S = sig
  include Interval_sig.S

  type split_heuristic = Singleton of A.t | Splitted of t * t | NotSplitted

  val split_heuristic : t -> split_heuristic
end

let debug = Debug.register_info_flag ~desc:"compare stage" "LRA.compare_stage"

module Cmp (S1 : S) (S2 : S) : S = struct
  module T = struct
    type t = { s1 : S1.t; s2 : S2.t } [@@deriving eq, ord, show, hash]
  end

  include T
  include Popop_stdlib.MkDatatype (T)

  let mk s1 s2 = { s1; s2 }

  let print_if1 b pps s msg =
    if b then Debug.dprintf3 debug "[CMP stage] %s with %a" msg pps s

  let print_if2 b pps s ppu u msg =
    if b then
      Debug.dprintf5 debug "[CMP stage] %s with %a and %a" msg pps s ppu u

  let is_included s u =
    let b1 = S1.is_included s.s1 u.s1 in
    let b2 = S2.is_included s.s2 u.s2 in
    print_if2 (b1 && not b2) pp s pp u "is_included";
    b2

  let is_distinct s u =
    let b1 = S1.is_distinct s.s1 u.s1 in
    let b2 = S2.is_distinct s.s2 u.s2 in
    print_if2 (b1 && not b2) pp s pp u "is_distinct";
    b2

  let is_singleton u =
    let b1 = S1.is_singleton u.s1 in
    let b2 = S2.is_singleton u.s2 in
    print_if1 (Option.is_some b1 && Option.is_none b2) pp u "is_singleton";
    b2

  let is_reals u =
    let b1 = S1.is_reals u.s1 in
    let b2 = S2.is_reals u.s2 in
    print_if1 ((not b1) && b2) pp u "is_real";
    b2

  let is_integer u =
    let b1 = S1.is_integer u.s1 in
    let b2 = S2.is_integer u.s2 in
    print_if1 ((not b1) && b2) pp u "is_real";
    b2

  let mem q u =
    let b1 = S1.mem q u.s1 in
    let b2 = S2.mem q u.s2 in
    print_if2 ((not b1) && b2) A.pp q pp u "mem";
    b2

  let absent q u =
    let b1 = S1.absent q u.s1 in
    let b2 = S2.absent q u.s2 in
    print_if2 (b1 && not b2) A.pp q pp u "absent";
    b2

  type is_comparable = S2.is_comparable =
    | Gt
    | Lt
    | Ge
    | Le
    | Eq
    | Uncomparable
  [@@deriving show]

  let is_comparable s u =
    let b1 = S1.is_comparable s.s1 u.s1 in
    let b2 = S2.is_comparable s.s2 u.s2 in
    let c =
      match (b1, b2) with
      | Gt, Ge | Lt, Le | Eq, (Le | Ge) ->
          Debug.dprintf2 debug "res: %a" pp_is_comparable b2;
          true
      | Eq, (Gt | Lt) | (Gt | Lt), Eq -> assert false
      | _ -> false
    in
    print_if2 c pp s pp u "is_comparable";
    b2

  let reals = mk S1.reals S2.reals
  let integers = mk S1.integers S2.integers
  let zero = mk S1.zero S2.zero
  let mult_cst q s = { s1 = S1.mult_cst q s.s1; s2 = S2.mult_cst q s.s2 }
  let add_cst q s = { s1 = S1.add_cst q s.s1; s2 = S2.add_cst q s.s2 }
  let add s u = { s1 = S1.add s.s1 u.s1; s2 = S2.add s.s2 u.s2 }
  let mul s u = { s1 = S1.mul s.s1 u.s1; s2 = S2.mul s.s2 u.s2 }
  let div s u = { s1 = S1.div s.s1 u.s1; s2 = S2.div s.s2 u.s2 }
  let inv s = { s1 = S1.inv s.s1; s2 = S2.inv s.s2 }
  let relu s = { s1 = S1.relu s.s1; s2 = S2.relu s.s2 }
  let neg s = { s1 = S1.neg s.s1; s2 = S2.neg s.s2 }
  let floor s = { s1 = S1.floor s.s1; s2 = S2.floor s.s2 }
  let ceil s = { s1 = S1.ceil s.s1; s2 = S2.ceil s.s2 }
  let truncate s = { s1 = S1.truncate s.s1; s2 = S2.truncate s.s2 }
  let minus s u = { s1 = S1.minus s.s1 u.s1; s2 = S2.minus s.s2 u.s2 }

  let inter s u =
    let b1 = S1.inter s.s1 u.s1 in
    let b2 = S2.inter s.s2 u.s2 in
    match (b1, b2) with
    | _, None -> None
    | None, Some _ ->
        print_if2 true pp s pp u "inter";
        None
    | Some s1, Some s2 -> Some { s1; s2 }

  let except s u =
    let b1 = S1.except s.s1 u in
    let b2 = S2.except s.s2 u in
    match (b1, b2) with
    | _, None -> None
    | None, Some _ ->
        print_if2 true pp s A.pp u "except";
        None
    | Some s1, Some s2 -> Some { s1; s2 }

  let complement s =
    let b1 = S1.complement s.s1 in
    let b2 = S2.complement s.s2 in
    match (b1, b2) with
    | _, None -> None
    | None, Some _ ->
        print_if1 true pp s "complement";
        None
    | Some s1, Some s2 -> Some { s1; s2 }

  let union s u = { s1 = S1.union s.s1 u.s1; s2 = S2.union s.s2 u.s2 }
  let singleton q = { s1 = S1.singleton q; s2 = S2.singleton q }

  let from_convexe_hull b1 =
    match (S1.from_convexe_hull b1, S2.from_convexe_hull b1) with
    | None, None -> None
    | Some s1, Some s2 -> Some { s1; s2 }
    | _, _ ->
        print_if1 true Fmt.nop () "from_convexe_hull";
        None

  type split_heuristic = Singleton of A.t | Splitted of t * t | NotSplitted

  let split_heuristic _ = assert false
  let gt q = { s1 = S1.gt q; s2 = S2.gt q }
  let lt q = { s1 = S1.lt q; s2 = S2.lt q }
  let le q = { s1 = S1.le q; s2 = S2.le q }
  let ge q = { s1 = S1.ge q; s2 = S2.ge q }
  let gt' q = { s1 = S1.gt' q.s1; s2 = S2.gt' q.s2 }
  let lt' q = { s1 = S1.lt' q.s1; s2 = S2.lt' q.s2 }
  let le' q = { s1 = S1.le' q.s1; s2 = S2.le' q.s2 }
  let ge' q = { s1 = S1.ge' q.s1; s2 = S2.ge' q.s2 }
  let choose s = S2.choose s.s2
  let invariant s = S1.invariant s.s1 && S2.invariant s.s2

  let get_convexe_hull s =
    let a1, b1 = S1.get_convexe_hull s.s1 in
    let a2, b2 = S2.get_convexe_hull s.s2 in
    let c1 = match (a1, a2) with Some _, None -> true | _ -> false in
    print_if1 c1 pp s "get_convexe_hull_min";
    let c2 = match (a1, a2) with Some _, None -> true | _ -> false in
    print_if1 c2 pp s "get_convexe_hull_max";
    (a2, b2)

  module Union = S1.Union

  let get_union x = S1.get_union x.s1

  let reduce_integers { s1; s2 } =
    let open Base.Option in
    S1.reduce_integers s1 >>= fun s1 ->
    S2.reduce_integers s2 >>| fun s2 -> { s1; s2 }

  let neg { s1; s2 } = { s1 = S1.neg s1; s2 = S2.neg s2 }
  let floor { s1; s2 } = { s1 = S1.floor s1; s2 = S2.floor s2 }
  let ceil { s1; s2 } = { s1 = S1.ceil s1; s2 = S2.ceil s2 }
  let truncate { s1; s2 } = { s1 = S1.truncate s1; s2 = S2.truncate s2 }
  let relu { s1; s2 } = { s1 = S1.relu s1; s2 = S2.relu s2 }
end
