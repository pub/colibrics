module T = struct
  type t = Colibrilib.Interval.Bound.t = Strict | Large
  [@@deriving eq, ord, hash]

  let pp fmt = function
    | Strict -> CCFormat.string fmt "<"
    | Large -> CCFormat.string fmt "≤"
end

include T
include Colibri2_popop_lib.Popop_stdlib.MkDatatype (T)

let compare_inf b1 b2 =
  match (b1, b2) with
  | Large, Strict -> -1
  | Strict, Large -> 1
  | Large, Large | Strict, Strict -> 0

let compare_inf_sup b1 b2 =
  match (b1, b2) with
  | Large, Strict -> 1
  | Strict, Large -> 1
  | Large, Large -> 0
  | Strict, Strict -> 1

let compare_sup b1 b2 = -compare_inf b1 b2
let compare_bounds_inf = CCOrd.pair Q.compare compare_inf
let compare_bounds_sup = CCOrd.pair Q.compare compare_sup
let compare_bounds_inf_sup = CCOrd.pair Q.compare compare_inf_sup
let neg = function Large -> Strict | Strict -> Large
