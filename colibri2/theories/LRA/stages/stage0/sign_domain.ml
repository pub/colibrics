(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

(** Sign domain: abstraction of integer numerical values by their signs. *)

(** From frama-c *)

open Base

module T = struct
  type t = {
    pos : bool;  (** true: maybe positive, false: never positive *)
    zero : bool;  (** true: maybe zero, false: never zero *)
    neg : bool;  (** true: maybe negative, false: never negative *)
  }
  [@@deriving ord, eq, hash]

  let pp fmt t =
    Fmt.string fmt
    @@
    match (t.neg, t.zero, t.pos) with
    | true, true, true -> "ℝ"
    | true, false, true -> "≠0"
    | false, true, false -> "=0"
    | true, true, false -> "≤0"
    | true, false, false -> "<0"
    | false, false, false -> "∅"
    | false, false, true -> ">0"
    | false, true, true -> "≥0"
end

include T
include Colibri2_popop_lib.Popop_stdlib.MkDatatype (T)

let invariant t = t.neg || t.zero || t.pos
let top = { pos = true; zero = true; neg = true }
let pos_or_zero = { pos = true; zero = true; neg = false }
let pos = { pos = true; zero = false; neg = false }
let neg_or_zero = { pos = false; zero = true; neg = true }
let dneg = { pos = false; zero = false; neg = true }
let zero = { pos = false; zero = true; neg = false }
let one = { pos = true; zero = false; neg = false }
let non_zero = { pos = true; zero = false; neg = true }
let ge_zero v = not v.neg
let le_zero v = not v.pos

(* Bottom is a special value (`Bottom) in Eva, and need not be part of
   the lattice. Here, we have a value which is equivalent to it, defined
   there only for commodity. *)
let empty = { pos = false; zero = false; neg = false }
let is_empty t = equal t empty
let is_reals t = equal t top

(* Inclusion: test inclusion of each field. *)
let is_included v1 v2 =
  let bincl b1 b2 = (not b1) || b2 in
  bincl v1.pos v2.pos && bincl v1.zero v2.zero && bincl v1.neg v2.neg

(* Join of the lattice: pointwise logical or. *)
let join v1 v2 =
  { pos = v1.pos || v2.pos; zero = v1.zero || v2.zero; neg = v1.neg || v2.neg }

(* Meet of the lattice (called 'narrow' in Eva for historical reasons).
   We detect the case where the values have incompatible concretization,
   and report this as `Bottom. *)
let narrow v1 v2 =
  let r =
    {
      pos = v1.pos && v2.pos;
      zero = v1.zero && v2.zero;
      neg = v1.neg && v2.neg;
    }
  in
  if is_empty r then None else Some r

let mem q v =
  let c = A.sign q in
  (c = 0 && v.zero) || (c < 0 && v.neg) || (c > 0 && v.pos)

let absent q v =
  let c = A.sign q in
  (c = 0 && not v.zero) || (c < 0 && not v.neg) || (c > 0 && not v.pos)

let reals = top

(* [singleton] creates an abstract value corresponding to the singleton [i]. *)
let singleton i =
  if A.lt i A.zero then dneg else if A.gt i A.zero then pos else zero

let is_singleton v = if equal v zero then Some A.zero else None

(** {2 Forward transfer functions} *)

(* Functions [neg_unop], [plus], [mul] and [div] below are forward transformers
   for the mathematical operations -, +, *, /. The potential overflows and
   wrappings for the operations on machine integers are taken into account by
   the functions [truncate_integer] and [rewrap_integer]. *)

let neg_unop v = { v with neg = v.pos; pos = v.neg }
let logical_not v = { pos = v.zero; neg = false; zero = v.pos || v.neg }

let add v1 v2 =
  let neg = v1.neg || v2.neg in
  let pos = v1.pos || v2.pos in
  let same_sign v1 v2 =
    (le_zero v1 && le_zero v2) || (ge_zero v1 && ge_zero v2)
  in
  let zero = (not (same_sign v1 v2)) || (v1.zero && v2.zero) in
  { neg; pos; zero }

let add_cst q v =
  let c = A.sign q in
  if c = 0 then v
  else if c > 0 then
    { neg = v.neg; zero = v.neg; pos = v.pos || v.zero || v.neg }
  else { neg = v.neg || v.zero || v.pos; zero = v.pos; pos = v.pos }

let mul v1 v2 =
  let pos = (v1.pos && v2.pos) || (v1.neg && v2.neg) in
  let neg = (v1.pos && v2.neg) || (v1.neg && v2.pos) in
  let zero = v1.zero || v2.zero in
  { neg; pos; zero }

let div v1 v2 = if v2.zero then top else mul v1 v2

let inv v1 =
  if v1.zero then top else { neg = v1.neg; zero = false; pos = v1.pos }

let neg = neg_unop
let minus v1 v2 = add v1 (neg v2)
let complement v1 = Some { neg = true; zero = not v1.zero; pos = true }

let mult_cst q v =
  let c = A.sign q in
  if c = 0 then zero
  else if c > 0 then v
  else { neg = v.pos; zero = v.zero; pos = v.neg }

let div v1 v2 =
  let pos = (v1.pos && v2.pos) || (v1.neg && v2.neg) in
  let neg = (v1.pos && v2.neg) || (v1.neg && v2.pos) in
  let zero = true in
  (* zero can appear with large enough v2 *)
  { neg; pos; zero }

(* The implementation of the bitwise operators below relies on this table
   giving the sign of the result according to the sign of both operands.

       v1  v2   v1&v2   v1|v2   v1^v2
   -----------------------------------
   |   +   +      +0      +       +0
   |   +   0      0       +       +
   |   +   -      +0      -       -
   |   0   +      0       +       +
   |   0   0      0       0       0
   |   0   -      0       -       -
   |   -   +      +0      -       -
   |   -   0      0       -       -
   |   -   -      -       -       +0
*)

let bitwise_and v1 v2 =
  let pos = (v1.pos && (v2.pos || v2.neg)) || (v2.pos && v1.neg) in
  let neg = v1.neg && v2.neg in
  let zero = v1.zero || v1.pos || v2.zero || v2.pos in
  { neg; pos; zero }

let bitwise_or v1 v2 =
  let pos = (v1.pos && (v2.pos || v2.zero)) || (v1.zero && v2.pos) in
  let neg = v1.neg || v2.neg in
  let zero = v1.zero && v2.zero in
  { neg; pos; zero }

let bitwise_xor v1 v2 =
  let pos =
    (v1.pos && v2.pos) || (v1.pos && v2.zero) || (v1.zero && v2.pos)
    || (v1.neg && v2.neg)
  in
  let neg =
    (v1.neg && (v2.pos || v2.zero)) || (v2.neg && (v1.pos || v1.zero))
  in
  let zero = (v1.zero && v2.zero) || (v1.pos && v2.pos) || (v1.neg && v2.neg) in
  { neg; pos; zero }

let logical_and v1 v2 =
  let pos = (v1.pos || v1.neg) && (v2.pos || v2.neg) in
  let neg = false in
  let zero = v1.zero || v2.zero in
  { pos; neg; zero }

let logical_or v1 v2 =
  let pos = v1.pos || v1.neg || v2.pos || v2.neg in
  let neg = false in
  let zero = v1.zero && v2.zero in
  { pos; neg; zero }

(*
(* This function must reduce the value [right] assuming that the
   comparison [left op right] holds. *)
let backward_comp_right op ~left ~right =
  let open Abstract_interp.Comp in
  match op with
  | Eq ->
    narrow left right >>- reduced
  | Ne ->
    if equal left zero then
      narrow right non_zero >>- reduced
    else unreduced
  | Le ->
    if ge_zero left then
      (* [left] is positive or zero. Hence, [right] is at least also positive
         or zero. *)
      if left.zero then
        (* [left] may be zero, [right] is positive or zero *)
        narrow right pos_or_zero >>- reduced
      else
        (* [left] is strictly positive, hence so is [right] *)
        narrow right pos >>- reduced
    else unreduced
  | Lt ->
    if ge_zero left then
      narrow right pos >>- reduced
    else unreduced
  | Ge ->
    if le_zero left then
      if left.zero then
        narrow right neg_or_zero >>- reduced
      else
        narrow right neg >>- reduced
    else unreduced
  | Gt ->
    if le_zero left then
      narrow right neg >>- reduced
    else unreduced

(* This functions must reduce the values [left] and [right], assuming
   that [left op right == result] holds. Currently, it is only implemented
   for comparison operators. *)
let backward_binop ~input_type:_ ~resulting_type:_ op ~left ~right ~result =
  match op with
  | Ne | Eq | Le | Lt | Ge | Gt ->
    let op = Value_util.conv_comp op in
    if equal zero result then
      (* The comparison is false, as it always evaluate to false. Reduce by the
         fact that the inverse comparison is true.  *)
      let op = Comp.inv op in
      backward_comp_right op ~left ~right >>- fun right' ->
      backward_comp_right (Comp.sym op) ~left:right ~right:left >>- fun left' ->
      `Value (left', right')
    else if not result.zero then
      (* The comparison always hold, as it never evaluates to false. *)
      backward_comp_right op ~left ~right >>- fun right' ->
      backward_comp_right (Comp.sym op) ~left:right ~right:left >>- fun left' ->
      `Value (left', right')
    else
      (* The comparison may or may not hold, it is not possible to reduce *)
      `Value (None, None)
  | _ -> `Value (None, None)

(* Not implemented precisely *)
let backward_unop ~typ_arg:_ _op ~arg:_ ~res:_ = `Value None
(* Not implemented precisely *)
let backward_cast ~src_typ:_ ~dst_typ:_ ~src_val:_ ~dst_val:_ = `Value None
*)

let is_distinct a b =
  not (Bool.(a.neg && b.neg) || Bool.(a.zero && b.zero) || Bool.(a.pos && b.pos))

type is_comparable = Gt | Lt | Ge | Le | Eq | Uncomparable

let is_comparable x y =
  match (x, y) with
  | { pos = false; zero = false; neg = false }, _
  | _, { pos = false; zero = false; neg = false }
  | { pos = true; neg = true; _ }, _
  | _, { pos = true; neg = true; _ } ->
      Uncomparable
  | { pos = true; _ }, { pos = true; _ } | { neg = true; _ }, { neg = true; _ }
    ->
      Uncomparable
  | ( { neg = false; zero = true; pos = false },
      { neg = false; zero = true; pos = false } ) ->
      Eq
  | ( { neg = true; zero = false; pos = false },
      { neg = false; zero = _; pos = _ } )
  | ( { neg = true; zero = true; pos = false },
      { neg = false; zero = false; pos = true } )
  | ( { neg = false; zero = true; pos = false },
      { neg = false; zero = false; pos = true } ) ->
      Lt
  | ( { neg = true; zero = true; pos = false },
      { neg = false; zero = true; pos = _ } )
  | ( { neg = false; zero = true; pos = false },
      { neg = false; zero = true; pos = _ } ) ->
      Le
  | ( { neg = false; zero = _; pos = _ },
      { neg = true; zero = false; pos = false } )
  | ( { neg = false; zero = false; pos = true },
      { neg = true; zero = true; pos = false } )
  | ( { neg = false; zero = false; pos = true },
      { neg = false; zero = true; pos = false } ) ->
      Gt
  | ( { neg = false; zero = true; pos = _ },
      { neg = true; zero = true; pos = false } )
  | ( { neg = false; zero = true; pos = true },
      { neg = false; zero = true; pos = false } ) ->
      Ge

let is_included x y =
  (* <= is like implication on booleans *)
  Bool.(x.neg <= y.neg && x.zero <= y.zero && x.pos <= y.pos)

let union = join
let inter = narrow

type split_heuristic = Singleton of A.t | Splitted of t * t | NotSplitted

let split_heuristic _ = NotSplitted

let except t x =
  if A.sign x = 0 && t.zero then
    let t = { t with zero = false } in
    if is_empty t then None else Some t
  else Some t

let choose t = if t.zero then A.zero else if t.pos then A.one else A.minus_one

let le q =
  let c = A.sign q in
  if c < 0 then dneg else if c = 0 then neg_or_zero else top

let lt q =
  let c = A.sign q in
  if c <= 0 then dneg else top

let ge q =
  let c = A.sign q in
  if c < 0 then top else if c = 0 then pos_or_zero else pos

let gt q =
  let c = A.sign q in
  if c >= 0 then pos else top

let le' v =
  { neg = v.neg || v.zero || v.pos; zero = v.zero || v.pos; pos = v.pos }

let lt' v = { neg = v.neg || v.zero || v.pos; zero = v.pos; pos = v.pos }

let ge' v =
  { neg = v.neg; zero = v.zero || v.neg; pos = v.pos || v.zero || v.neg }

let gt' v = { neg = v.neg; zero = v.neg; pos = v.pos || v.zero || v.neg }
let integers = reals
let is_integer _ = false

let get_convexe_hull v =
  let inf =
    match (v.neg, v.zero, v.pos) with
    | true, _, _ -> None
    | false, true, _ -> Some (A.zero, Bound.Large)
    | false, false, true -> Some (A.zero, Bound.Strict)
    | false, false, false -> assert false
    (* absurd: bottom *)
  in
  let sup =
    match (v.neg, v.zero, v.pos) with
    | _, _, true -> None
    | _, true, false -> Some (A.zero, Bound.Large)
    | true, false, false -> Some (A.zero, Bound.Strict)
    | false, false, false -> assert false
    (* absurd: bottom *)
  in
  (inf, sup)

let from_convexe_hull (inf, sup) =
  let d1 =
    match inf with
    | None -> reals
    | Some (q, b) ->
        let s = A.sign q in
        if s > 0 then pos
        else if s = 0 then
          match b with Bound.Large -> pos_or_zero | Strict -> pos
        else reals
  in
  let d2 =
    match sup with
    | None -> reals
    | Some (q, b) ->
        let s = A.sign q in
        if s < 0 then dneg
        else if s = 0 then
          match b with Bound.Large -> neg_or_zero | Strict -> dneg
        else reals
  in
  inter d1 d2

module Union = struct
  module U = struct
    type t'' = Sin of A.t * t'' | Chg of A.t * Bound.t * t'' | Inf
    [@@deriving eq, ord, hash]

    type t' = On of t'' | Off of t'' [@@deriving eq, ord, hash]
    type integrability = Integer | MaybeReal [@@deriving eq, ord, hash]

    type t = { integrability : integrability; u : t' }
    [@@deriving eq, ord, hash]
  end

  include U
  include Colibri2_theories_LRA_stages_def.Interval_sig.Union_Helpers (U)
end

let get_union t : Union.t =
  let u : Union.t' =
    match (t.neg, t.zero, t.pos) with
    | false, false, false -> Off Inf
    | false, false, true -> Off (Chg (A.zero, Bound.Strict, Inf))
    | false, true, false -> Off (Sin (A.zero, Inf))
    | false, true, true -> Off (Chg (A.zero, Bound.Large, Inf))
    | true, false, false -> On (Chg (A.zero, Bound.Large, Inf))
    | true, false, true -> On (Sin (A.zero, Inf))
    | true, true, false -> On (Chg (A.zero, Bound.Strict, Inf))
    | true, true, true -> On Inf
  in
  { integrability = MaybeReal; u }

let reduce_integers t = Some t
let ceil a = a
let floor a = a
let truncate a = a
let relu a = { neg = false; zero = a.zero || a.neg; pos = a.pos }
