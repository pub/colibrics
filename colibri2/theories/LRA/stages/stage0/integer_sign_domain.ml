(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

(** Sign domain with integer. *)

open Base

module T = struct
  type t = {
    noninteger : bool;  (** true: maybe real, false: never real *)
    sign : Sign_domain.t;
  }
  [@@deriving ord, eq, hash]

  let pp fmt t =
    let prefix s = if t.noninteger then s else "ℕ" ^ s in
    Fmt.string fmt
    @@
    match (t.sign.neg, t.sign.zero, t.sign.pos) with
    | true, true, true -> if t.noninteger then "ℝ" else "ℕ"
    | true, false, true -> prefix "≠0"
    | false, true, false -> prefix "=0"
    | true, true, false -> prefix "≤0"
    | true, false, false -> prefix "<0"
    | false, false, false -> prefix "∅"
    | false, false, true -> prefix ">0"
    | false, true, true -> prefix "≥0"
end

include T
include Colibri2_popop_lib.Popop_stdlib.MkDatatype (T)

let invariant t = Sign_domain.invariant t.sign

type split_heuristic = Singleton of A.t | Splitted of t * t | NotSplitted

let split_heuristic t =
  match Sign_domain.split_heuristic t.sign with
  | Singleton q -> Singleton q
  | Splitted (t1, t2) ->
      Splitted
        ( { noninteger = t.noninteger; sign = t1 },
          { noninteger = t.noninteger; sign = t2 } )
  | NotSplitted -> NotSplitted

let absent q t =
  ((not t.noninteger) && not (A.is_integer q)) || Sign_domain.absent q t.sign

let le' t = { noninteger = true; sign = Sign_domain.le' t.sign }
let lt' t = { noninteger = true; sign = Sign_domain.lt' t.sign }
let ge' t = { noninteger = true; sign = Sign_domain.ge' t.sign }
let gt' t = { noninteger = true; sign = Sign_domain.gt' t.sign }
let is_distinct t1 t2 = Sign_domain.is_distinct t1.sign t2.sign

type is_comparable = Sign_domain.is_comparable =
  | Gt
  | Lt
  | Ge
  | Le
  | Eq
  | Uncomparable

let is_comparable t1 t2 = Sign_domain.is_comparable t1.sign t2.sign

let is_included t1 t2 =
  ((not t1.noninteger) || t2.noninteger)
  && Sign_domain.is_included t1.sign t2.sign

let mult_cst q t =
  {
    noninteger = t.noninteger && A.is_not_zero q;
    sign = Sign_domain.mult_cst q t.sign;
  }

let add_cst q t =
  { noninteger = t.noninteger; sign = Sign_domain.add_cst q t.sign }

let add t1 t2 =
  {
    noninteger = t1.noninteger || t2.noninteger;
    sign = Sign_domain.add t1.sign t2.sign;
  }

let mul t1 t2 =
  {
    noninteger = t1.noninteger || t2.noninteger;
    sign = Sign_domain.mul t1.sign t2.sign;
  }

let div t1 t2 = { noninteger = true; sign = Sign_domain.div t1.sign t2.sign }
let inv t = { noninteger = true; sign = Sign_domain.inv t.sign }

let minus t1 t2 =
  {
    noninteger = t1.noninteger || t2.noninteger;
    sign = Sign_domain.minus t1.sign t2.sign;
  }

let neg t1 = { noninteger = t1.noninteger; sign = Sign_domain.neg t1.sign }
let mem q t = (A.is_integer q || t.noninteger) && Sign_domain.mem q t.sign

let complement t1 =
  Base.Option.map (Sign_domain.complement t1.sign) ~f:(fun sign ->
      { noninteger = true; sign })

let singleton q =
  { noninteger = not (A.is_integer q); sign = Sign_domain.singleton q }

let is_singleton t = Sign_domain.is_singleton t.sign

let except t q =
  match Sign_domain.except t.sign q with
  | None -> None
  | Some sign -> Some { noninteger = t.noninteger; sign }

let gt q = { noninteger = true; sign = Sign_domain.gt q }
let ge q = { noninteger = true; sign = Sign_domain.ge q }
let lt q = { noninteger = true; sign = Sign_domain.lt q }
let le q = { noninteger = true; sign = Sign_domain.le q }

let union t1 t2 =
  {
    noninteger = t1.noninteger || t2.noninteger;
    sign = Sign_domain.union t1.sign t2.sign;
  }

let inter t1 t2 =
  match Sign_domain.inter t1.sign t2.sign with
  | Some sign -> Some { noninteger = t1.noninteger && t2.noninteger; sign }
  | None -> None

let zero = { noninteger = false; sign = Sign_domain.zero }
let reals = { noninteger = true; sign = Sign_domain.reals }
let integers = { noninteger = false; sign = Sign_domain.reals }
let is_reals t = t.noninteger && Sign_domain.is_reals t.sign
let choose t = Sign_domain.choose t.sign
let is_integer t = not t.noninteger

let get_convexe_hull t =
  let inf, sup = Sign_domain.get_convexe_hull t.sign in
  let checkint dir = function
    | x when t.noninteger -> x
    | None -> None
    | Some (z, Bound.Strict) when A.is_zero z -> Some (dir, Large)
    | x -> x
  in
  (checkint A.one inf, checkint A.minus_one sup)

let from_convexe_hull ch =
  Option.map
    ~f:(fun sign -> { sign; noninteger = true })
    (Sign_domain.from_convexe_hull ch)

module Union = struct
  module U = struct
    type t'' = Sin of A.t * t'' | Chg of A.t * Bound.t * t'' | Inf
    [@@deriving eq, ord, hash]

    type t' = On of t'' | Off of t'' [@@deriving eq, ord, hash]
    type integrability = Integer | MaybeReal [@@deriving eq, ord, hash]

    type t = { integrability : integrability; u : t' }
    [@@deriving eq, ord, hash]
  end

  include U
  include Colibri2_theories_LRA_stages_def.Interval_sig.Union_Helpers (U)
end

let get_union t : Union.t =
  let u : Union.t' =
    match (t.sign.neg, t.sign.zero, t.sign.pos) with
    | false, false, false -> Off Inf
    | false, false, true when t.noninteger ->
        Off (Chg (A.zero, Bound.Strict, Inf))
    | false, false, true -> Off (Chg (A.one, Bound.Large, Inf))
    | false, true, false -> Off (Sin (A.zero, Inf))
    | false, true, true -> Off (Chg (A.zero, Bound.Large, Inf))
    | true, false, false when t.noninteger ->
        On (Chg (A.zero, Bound.Large, Inf))
    | true, false, false -> On (Chg (A.minus_one, Bound.Strict, Inf))
    | true, false, true when t.noninteger -> On (Sin (A.zero, Inf))
    | true, false, true ->
        On (Chg (A.minus_one, Bound.Strict, Chg (A.one, Bound.Large, Inf)))
    | true, true, false -> On (Chg (A.zero, Bound.Strict, Inf))
    | true, true, true -> On Inf
  in
  let integrability : Union.integrability =
    if t.noninteger then MaybeReal else Integer
  in
  { integrability; u }

let reduce_integers t = Some { noninteger = false; sign = t.sign }
let ceil a = { noninteger = false; sign = Sign_domain.ceil a.sign }
let floor a = { noninteger = false; sign = Sign_domain.floor a.sign }
let truncate a = { noninteger = false; sign = Sign_domain.truncate a.sign }
let relu a = { noninteger = a.noninteger; sign = Sign_domain.relu a.sign }
