type t = Colibrilib.Interval.Bound.t = Strict | Large

val compare_bounds_inf : Q.t * t -> Q.t * t -> int
val compare_bounds_sup : Q.t * t -> Q.t * t -> int
val compare_bounds_inf_sup : Q.t * t -> Q.t * t -> int
val neg : t -> t

include Colibri2_popop_lib.Popop_stdlib.Datatype with type t := t
