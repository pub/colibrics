(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

let debug =
  Debug.register_info_flag
    ~desc:"for the arithmetic theory of product of variable" "LRA.product"

module T = struct
  module T = struct
    type t = { abs : Product.t; sign : Sign_product.t }
    [@@deriving eq, ord, hash, show]

    let ty _ = Ground.Ty.real
    let name = "SARITH_PROD"
  end

  include T
  include Colibri2_popop_lib.Popop_stdlib.MkDatatype (T)
end

module ThE = ThTerm.Register (T)

module Table = struct
  module H = Datastructure.Hashtbl (T)

  let h = H.create Node.pp "HARITH_PRODUCT"

  let set ~old_ ~new_ d n =
    Debug.dprintf6 debug "set ~old:%a ~new:%a %a" T.pp old_ T.pp new_ Node.pp n;
    (* if not (T.equal old_ new_) then H.remove h d old_; *)
    H.change
      (function
        | None -> Some n
        | Some n' as s ->
            Egraph.merge d n n';
            s)
      h d new_

  let new_ d p =
    match H.find_opt h d p with
    | Some n -> n
    | None ->
        let n = ThE.node (ThE.index p) in
        H.set h d p n;
        n
end

let set d cl ~old_abs ~old_sign abs sign =
  if Sign_product.equal Sign_product.zero sign then
    Egraph.merge d cl RealValue.zero
  else
    let aux () =
      Table.set d cl
        ~old_:{ T.abs = old_abs; sign = old_sign }
        ~new_:{ T.abs; sign }
    in
    match Product.is_one_node abs with
    | None -> aux ()
    | Some cl' -> (
        match Sign_product.is_one_node sign with
        | Some cl'' when Egraph.is_equal d cl' cl'' -> Egraph.merge d cl cl'
        | _ -> aux ())

module rec SolveAbs : sig
  val assume_equality : Egraph.wt -> Node.t -> Product.t -> unit
  val init : Egraph.wt -> unit
  val get_repr : _ Egraph.t -> Node.t -> Product.t option
  val iter_eqs : _ Egraph.t -> Node.t -> f:(Product.t -> unit) -> unit

  val attach_repr_change :
    _ Egraph.t -> ?node:Node.t -> (Egraph.wt -> Node.t -> unit) -> unit

  val attach_eqs_change :
    _ Egraph.t -> ?node:Node.t -> (Egraph.wt -> Node.t -> unit) -> unit
end = Pivot.WithUnsolved (struct
  let name = "ARITH_ABS"

  include Product

  let of_one_node _ n = Product.monome n Q.one

  let subst p n q =
    match subst_exn p n q with
    | exception Div_zero -> raise Egraph.Contradiction
    | p', c ->
        Debug.dprintf10 debug "subst: %a %a %a -> %a %a" Product.pp p Node.pp n
          Product.pp q Product.pp p' Q.pp c;
        if Q.is_zero c then None else Some p'

  let set d cl ~old_ ~new_ =
    SolveSign.iter_eqs d cl ~f:(fun sign ->
        set d cl ~old_abs:old_ ~old_sign:sign new_ sign)

  type data = Q.t

  let nodes p = p.Product.poly

  let normalize p ~f =
    let add acc cl c =
      let q = f cl in
      Product.x_m_yc acc q c
    in
    Product.fold add (Product.cst p.cst) p

  type info = data Node.M.t * data Node.M.t * data Node.M.t * Product.t

  let info d m =
    let spos, other =
      Node.M.partition
        (fun k _ -> Dom_interval.is_strictly_positive d k)
        m.Product.poly
    in
    let notz, other =
      Node.M.partition (fun k _ -> Dom_interval.is_not_zero d k) other
    in
    (spos, notz, other, m)

  let attach_info_change d f = Dom_interval.events_attach_any_dom d f

  let solve (_, notz1, unk1, p1) (_, notz2, unk2, p2) :
      _ Pivot.solve_with_unsolved =
    let exception Solved of Node.t * Product.t in
    let some_zero unk (p : Product.t) : _ Pivot.solve_with_unsolved =
      (* Use the one unknown to not be null *)
      if Node.M.is_empty unk then
        if A.is_zero p.cst then AlreadyEqual else Contradiction
      else if Node.M.is_num_elt 1 unk then
        Subst (Node.M.map (fun _ -> Product.zero) unk)
      else Unsolved
    in
    if Product.is_zero p1 then some_zero unk2 p2
    else if Product.is_zero p2 then some_zero unk1 p1
    else if
      Node.M.is_empty notz1 && Node.M.is_empty unk1 && Node.M.is_empty notz2
      && Node.M.is_empty unk2
    then
      (* all (strictly) positive: can use root *)
      let p = Product.div p1 p2 in
      match Product.extract p with
      | One -> AlreadyEqual
      | Cst _ -> Contradiction
      | Var (q, n, p) ->
          let p = Product.power_cst (Q.inv (Q.neg q)) p in
          Subst (Node.M.singleton n p)
    else if Node.M.is_empty unk1 then
      (* side 1 with all different from zero *)
      try
        Node.M.iter
          (fun n q1 ->
            (* look for n^1 in p1/p2 with n in p1 *)
            let q2 = Node.M.find_def Q.zero n p2.Product.poly in
            if Q.equal (Q.sub q1 q2) Q.one then (
              let p = Product.div p2 p1 in
              assert (Q.equal (Node.M.find n p.poly) Q.minus_one);
              let p = Product.remove n p in
              raise (Solved (n, p))))
          p1.Product.poly;
        Unsolved
      with Solved (n, p) -> Subst (Node.M.singleton n p)
    else if Node.M.is_num_elt 1 unk1 then
      let n, q = Node.M.choose unk1 in
      if not (Node.M.mem n unk2) then
        let p = Product.div p2 (Product.remove n p1) in
        let p = power_cst (Q.inv q) p in
        Subst (Node.M.singleton n p)
      else Unsolved
    else Unsolved
end)

and SolveSign : sig
  val assume_equality : Egraph.wt -> Node.t -> Sign_product.t -> unit
  val init : Egraph.wt -> unit
  val get_repr : _ Egraph.t -> Node.t -> Sign_product.t option
  val iter_eqs : _ Egraph.t -> Node.t -> f:(Sign_product.t -> unit) -> unit

  val attach_repr_change :
    _ Egraph.t -> ?node:Node.t -> (Egraph.wt -> Node.t -> unit) -> unit

  val attach_eqs_change :
    _ Egraph.t -> ?node:Node.t -> (Egraph.wt -> Node.t -> unit) -> unit

  val reshape :
    Egraph.wt -> Node.t -> f:(Sign_product.t -> Sign_product.t option) -> unit
end = Pivot.WithUnsolved (struct
  let name = "ARITH_SIGN"

  include Sign_product

  let subst p n q =
    match subst_exn p n q with
    | exception Div_zero -> raise Egraph.Contradiction
    | r -> r

  let of_one_node d n =
    if Dom_interval.is_not_zero d n then Sign_product.of_one_node_non_zero n
    else Sign_product.of_one_node n

  let attach_info_change d f = Dom_interval.events_attach_any_dom d f

  let set d cl ~old_ ~new_ =
    SolveAbs.iter_eqs d cl ~f:(fun abs ->
        set d cl ~old_abs:abs ~old_sign:old_ abs new_)

  type info = Sign_product.presolve * Sign_product.t

  let info _ m : info = (Sign_product.presolve m, m)

  let solve ((info1, p1) : info) ((_, p2) : info) : _ Pivot.solve_with_unsolved
      =
    match info1 with
    | Zero -> Unsolved
    | NonZero -> (
        (* all non_zero *)
        let p = Sign_product.mul p2 p1 in
        match Sign_product.extract p with
        | Plus_one -> AlreadyEqual
        | Minus_one -> Contradiction
        | Zero -> Contradiction
        | Var (n, p) -> Subst (Node.M.singleton n p)
        | NoNonZero -> Unsolved)
    | OnePossibleZero n ->
        if not (Node.M.mem n (Sign_product.nodes p2)) then
          let p = Sign_product.mul p2 (Sign_product.remove n p1) in
          Subst (Node.M.singleton n p)
        else Unsolved
    | Other -> Unsolved
end)

let node_of_product d abs sign =
  let t = { T.abs; sign } in
  Table.new_ d t

(* res = a + coef*b *)
let factorize res a coef b d _ =
  if
    (not (Egraph.is_equal d RealValue.zero res))
    && (not (Egraph.is_equal d RealValue.zero a))
    && not (Egraph.is_equal d RealValue.zero b)
  then
    match
      ( SolveAbs.get_repr d a,
        SolveAbs.get_repr d b,
        SolveSign.get_repr d a,
        SolveSign.get_repr d b )
    with
    | Some pa, Some pb, Some sa, Some sb ->
        Debug.dprintfn debug "Factorize? %a = %a + %a*%a; %a=%a*%a; %a=%a*%a"
          Node.pp res Node.pp a A.pp coef Node.pp b Node.pp a Product.pp pa
          Sign_product.pp sa Node.pp b Product.pp pb Sign_product.pp sb;
        let common_abs = Product.common pa pb in
        let common_sign = Sign_product.common sa sb in
        if
          not
            (Product.equal Product.one common_abs
            && Sign_product.equal Sign_product.one common_sign)
        then (
          let cst, l =
            List.fold_left
              (fun (cst, acc) (abs, sign, v) ->
                let abs = Product.div abs common_abs in
                let sign = Sign_product.mul sign common_sign in
                match (Product.classify abs, Sign_product.classify sign) with
                | CST cst', PLUS_ONE -> (A.add cst (A.mul cst' v), acc)
                | CST cst', MINUS_ONE -> (A.sub cst (A.mul cst' v), acc)
                | NODE (cst', n), NODE (cst'', n') when Egraph.is_equal d n n'
                  ->
                    (cst, (n, A.mul (Sign_product.mul_cst cst'' cst') v) :: acc)
                | _ ->
                    let n = node_of_product d abs sign in
                    Egraph.register d n;
                    (cst, (n, v) :: acc))
              (A.zero, [])
              [ (pa, sa, A.one); (pb, sb, coef) ]
          in
          let p = Polynome.of_list cst l in
          let n = Dom_polynome.node_of_polynome d p in
          let pp_coef fmt coef =
            if A.equal A.one coef then Fmt.pf fmt "+"
            else if A.equal A.minus_one coef then Fmt.pf fmt "-"
            else Fmt.pf fmt "(%a)" A.pp coef
          in
          Debug.dprintf14 debug "[Factorize] %a = %a %a %a into %a * %a * %a"
            Node.pp res Node.pp a pp_coef coef Node.pp b Polynome.pp p
            Product.pp common_abs Sign_product.pp common_sign;
          Egraph.register d n;
          Dom_polynome.assume_equality d n p;
          let abs = Product.mul (Product.monome n Q.one) common_abs in
          let sign =
            Sign_product.mul (Sign_product.of_one_node n) common_sign
          in
          SolveAbs.assume_equality d res abs;
          SolveSign.assume_equality d res sign)
    | _ -> ()

(** {2 Initialization} *)
let propagate_a_cb d ~res ~a ~c ~b =
  List.iter
    (fun node ->
      SolveAbs.attach_repr_change d ~node (factorize res a c b);
      SolveSign.attach_repr_change d ~node (factorize res a c b))
    [ a; b ]

let div_rem_t =
  let x = Expr.Term.Var.mk "x" Expr.Ty.int in
  let y = Expr.Term.Var.mk "y" Expr.Ty.int in
  let tx = Expr.Term.of_var x in
  let ty = Expr.Term.of_var y in
  let d = Expr.Term.apply_cst RealValue.Builtin.colibri_cdiv [] [ tx; ty ] in
  let m = Expr.Term.apply_cst RealValue.Builtin.colibri_crem [] [ tx; ty ] in
  let def = Expr.Term.Int.add (Expr.Term.Int.mul d ty) m in
  fun d nx ny ->
    Debug.dprintf4 debug
      "[Arith] add link between colibri_cdiv(x,y) and colibri_crem(x,y) with x \
       = %a and y = %a"
      Node.pp nx Node.pp ny;
    let ndef =
      Colibri2_theories_quantifiers.Subst.convert d
        ~subst_new:
          {
            ty = Expr.Ty.Var.M.empty;
            term = Expr.Term.Var.M.of_list [ (x, nx); (y, ny) ];
          }
        def
    in
    Egraph.register d ndef;
    Egraph.merge d nx ndef

let reshape_non_zero d n =
  let q = Sign_product.of_one_node_non_zero n in
  SolveSign.reshape d n ~f:(fun p -> Sign_product.subst_exn p n q)

let init env =
  SolveAbs.init env;
  SolveSign.init env;
  DaemonAlsoInterp.attach_reg_value env RealValue.key (fun d value ->
      let v = RealValue.value value in
      let cl = RealValue.node value in
      let p1 = Product.of_list (A.abs v) [] in
      SolveAbs.assume_equality d cl p1;
      let sp = Sign_product.(cst v) in
      SolveSign.assume_equality d cl sp);
  Dom_polynome.attach_new_monome env (fun d ~this_node_is_equal_to:n q cl ->
      (* n = q * cl^1 *)
      Debug.dprintf6 debug "[Polynome->Product] propagate %a is %a%a" Node.pp n
        A.pp q Node.pp cl;
      let p1 = Product.of_list (A.abs q) [ (cl, Q.one) ] in
      SolveAbs.assume_equality d n p1;
      let sp = Sign_product.(mul (of_one_node cl) (cst q)) in
      SolveSign.assume_equality d n sp);
  let product_polynome d n =
    (* todo more efficient *)
    SolveAbs.iter_eqs d n ~f:(fun p ->
        (* Look for the two following cases: *)
        match Product.classify p with
        | CST cst when A.is_zero cst -> Egraph.merge d n RealValue.zero
        | CST cst ->
            (* - |n| = cst  /\  sgn(n)= ±1 *)
            SolveSign.iter_eqs d n ~f:(fun p ->
                match Sign_product.classify p with
                | PLUS_ONE -> RealValue.set d n cst
                | MINUS_ONE -> RealValue.set d n (A.neg cst)
                | ZERO ->
                    (* |n| <> 0 and sgn(n) = 0 *)
                    Egraph.contradiction ()
                | NODE _ | PRODUCT ->
                    Dom_interval.upd_dom d n
                      Dom_interval.D.(
                        union (singleton (A.neg cst)) (singleton cst)))
        | NODE (cst, cl) ->
            (* |n| = cst*|cl| /\ sgn(n) = ±sgn(cl) *)
            SolveSign.iter_eqs d n ~f:(fun p ->
                match Sign_product.classify p with
                | NODE (cst', cl') when Egraph.is_equal d cl cl' ->
                    let cst = Sign_product.mul_cst cst' cst in
                    let p1 = Polynome.of_list A.zero [ (cl, cst) ] in
                    Debug.dprintf4 debug
                      "[Product->Polynome] propagate %a is %a" Node.pp n Node.pp
                      cl;
                    Dom_polynome.assume_equality d n p1
                | _ -> ())
        | PRODUCT -> ())
  in
  SolveAbs.attach_eqs_change env product_polynome;
  SolveSign.attach_eqs_change env product_polynome;
  Dom_interval.daemonwithfilter_attach_any_dom env (fun d n ->
      match Dom_interval.zero_is d n with
      | Eq -> Some (fun d -> SolveSign.assume_equality d n Sign_product.zero)
      | Lt -> Some (fun d -> SolveSign.assume_equality d n Sign_product.one)
      | Gt ->
          Some (fun d -> SolveSign.assume_equality d n Sign_product.minus_one)
      | Le | Ge | Uncomparable ->
          if Dom_interval.is_not_zero d n then
            Some (fun d -> reshape_non_zero d n)
          else None);
  SolveSign.attach_eqs_change env (fun d n ->
      SolveSign.iter_eqs d n ~f:(fun s ->
          match Sign_product.classify s with
          | Sign_product.PLUS_ONE -> Dom_interval.assume_positive d n
          | Sign_product.MINUS_ONE -> Dom_interval.assume_negative d n
          | Sign_product.ZERO -> Egraph.merge d n RealValue.zero
          | _ -> ()))
