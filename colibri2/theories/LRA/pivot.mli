(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

val debug : Debug.flag

(** Normalization of equations generic in the shape of the terms *)

type 'a solve_with_unsolved =
  | AlreadyEqual
  | Contradiction
  | Unsolved
  | Subst of 'a Node.M.t

module WithUnsolved (P : sig
  type t
  (** Normalizable terms *)

  val name : string

  include Colibri2_popop_lib.Popop_stdlib.Datatype with type t := t

  val of_one_node : _ Egraph.t -> Node.t -> t
  (** Build a value that represent one node *)

  val is_one_node : t -> Node.t option
  (** Test if a value represents one node *)

  val subst : t -> Node.t -> t -> t option
  (** [subst p n q] substitute [n] by [q] in [p], if [n] is not in [p] None is
      returned, otherwise the substitution is returned *)

  val normalize : t -> f:(Node.t -> t) -> t
  (** [norm p ~f] normalize [p] using [f] *)

  type data
  (** An abstract type to avoid translating the map to sets in {!nodes} *)

  val nodes : t -> data Node.M.t
  (** [nodes t] returns the node which are present in [t] *)

  type info
  (** Additional information useful for solving the equation *)

  val info : _ Egraph.t -> t -> info
  (** [info d t] returns the information for the given normalized term *)

  val attach_info_change :
    Egraph.wt -> (Egraph.rt -> Node.t -> Events.enqueue) -> unit
  (** [attach_info_change f] should call the given function when information
      used for the computation of [info] changed. All unsolved equation which
      contains, [nodes t], the given node will be looked again. *)

  val solve : info -> info -> t solve_with_unsolved
  (** [solve t1 t2] solve the equation [t1 = t2] by returning a substitution.
      Return Unsolved if the equation can't be yet solved *)

  val set : Egraph.wt -> Node.t -> old_:t -> new_:t -> unit
  (** Called a new term equal to this node is found *)
end) : sig
  val assume_equality : Egraph.wt -> Node.t -> P.t -> unit
  (** [assume_equality d n p] assumes the equality [n = p] *)

  val init : Egraph.wt -> unit
  (** Initialize the data-structure needed. *)

  val get_repr : _ Egraph.t -> Node.t -> P.t option
  val iter_eqs : _ Egraph.t -> Node.t -> f:(P.t -> unit) -> unit

  val attach_repr_change :
    _ Egraph.t -> ?node:Node.t -> (Egraph.wt -> Node.t -> unit) -> unit

  val attach_eqs_change :
    _ Egraph.t -> ?node:Node.t -> (Egraph.wt -> Node.t -> unit) -> unit

  val reshape : Egraph.wt -> Node.t -> f:(P.t -> P.t option) -> unit
  (** Apply the given function to all the normalized form which contains this
      node. The resulting normalized form should contain the same or less nodes
      than before. *)
end

type 'a solve_total = AlreadyEqual | Contradiction | Subst of Node.t * 'a

module Total (P : sig
  type t
  (** Normalizable terms *)

  val name : string

  include Colibri2_popop_lib.Popop_stdlib.Datatype with type t := t

  val of_one_node : Node.t -> t
  (** Build a value that represent one node *)

  val subst : t -> Node.t -> t -> t
  (** [subst p n q] return the result of the substitution of [n] by [q] in [p]
  *)

  val normalize : t -> f:(Node.t -> t) -> t
  (** [norm p ~f] normalize [p] using [f] *)

  type data
  (** An abstract type to avoid translating the map to sets in {!nodes} *)

  val nodes : t -> data Node.M.t
  (** [nodes t] returns the node which are present in [t] *)

  val solve : t -> t -> t solve_total
  (** [solve t1 t2] solve the equation [t1 = t2] by returning a substitution. *)

  val set : Egraph.wt -> Node.t -> old_:t option -> new_:t -> unit
  (** Called a new term equal to this node is found *)
end) : sig
  val assume_equality : Egraph.wt -> Node.t -> P.t -> unit
  (** [assume_equality d n p] assumes the equality [n = p] *)

  val get_repr : _ Egraph.t -> Node.t -> P.t option

  val attach_repr_change :
    _ Egraph.t -> ?node:Node.t -> (Egraph.wt -> Node.t -> unit) -> unit

  val events_repr_change :
    _ Egraph.t ->
    ?node:Node.t ->
    (Egraph.rt -> Node.t -> Events.enqueue) ->
    unit

  val normalize : _ Egraph.t -> P.t -> P.t
end

(*  LocalWords:  Normalizable
 *)
