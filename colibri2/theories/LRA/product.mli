(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib

open Colibri2_core
(** Product of absolute values, the node with a negative power are assumed to be
    different from zero *)

type t = private { cst : A.t; poly : Q.t Node.M.t }

include Popop_stdlib.Datatype with type t := t

val invariant : t -> bool
val cst : A.t -> t
val zero : t
val one : t
val monome : Node.t -> Q.t -> t
val is_one_node : t -> Node.t option
val terms : t -> (Node.t * Q.t) list
val is_zero : t -> bool
val remove : Node.t -> t -> t

type extract =
  | One  (** p = 1 *)
  | Cst of A.t  (** p = cst <> 1 *)
  | Var of Q.t * Node.t * t  (** p = x^q * p' *)

val extract : t -> extract

type kind = CST of A.t | NODE of A.t * Node.t | PRODUCT

val classify : t -> kind
val power_cst : Q.t -> t -> t
val of_list : A.t -> (Node.t * Q.t) list -> t
val mul : t -> t -> t
val mult_cst : A.t -> t -> t
val div : t -> t -> t
val x_m_yc : t -> t -> Q.t -> t
val xc_m_yc : t -> Q.t -> t -> Q.t -> t

exception Div_zero

val subst_exn : t -> Node.t -> t -> t * Q.t
(** raise Div_zero if the substitution create a division by zero *)

val subst_node : t -> Node.t -> Node.t -> t * Q.t
val fold : ('a -> Node.t -> Q.t -> 'a) -> 'a -> t -> 'a
val iter : (Node.t -> Q.t -> unit) -> t -> unit
val of_map : A.t -> Q.t Node.M.t -> t

val common : t -> t -> t
(** Return the common part *)
