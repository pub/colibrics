(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

let print_poly fmt poly =
  let print_not_1 first fmt q =
    if (not first) && A.compare q A.zero >= 0 then
      Format.pp_print_string fmt "+";
    if A.equal q A.zero then Format.pp_print_string fmt "!0!"
    else if A.equal A.minus_one q then Format.pp_print_string fmt "-"
    else if not (A.equal A.one q) then A.pp fmt q
  in
  let print_mono k v (fmt, first) =
    Format.fprintf fmt "@[%a%a@]@," (print_not_1 first) v Node.pp k;
    (fmt, false)
  in
  let _, first = Node.M.fold print_mono poly (fmt, true) in
  first

module Poly = struct
  module T = struct
    include Node.M

    type t = A.t Node.M.t [@@deriving eq, ord, hash]

    let pp fmt poly = ignore (print_poly fmt poly : bool)
  end

  include T
  include Popop_stdlib.MkDatatype (T)
end

module T = struct
  type t = { cst : A.t; poly : Poly.t } [@@deriving eq, ord, hash]

  let pp fmt v =
    let print_not_0 first fmt q =
      if first then A.pp fmt q
      else if not (A.equal A.zero q) then (
        if A.compare q A.zero > 0 then Format.pp_print_string fmt "+";
        A.pp fmt q)
    in

    Format.fprintf fmt "@[";
    let first = print_poly fmt v.poly in
    Format.fprintf fmt "%a@]" (print_not_0 first) v.cst
end

include T
include Popop_stdlib.MkDatatype (T)

(** different invariant *)

let invariant p = Node.M.for_all (fun _ q -> not (A.equal q A.zero)) p.poly

(** constructor *)
let cst q = { cst = q; poly = Node.M.empty }

let is_cst p = if Node.M.is_empty p.poly then Some p.cst else None
let get_cst p = p.cst
let zero = cst A.zero
let is_zero p = A.equal p.cst A.zero && Node.M.is_empty p.poly

type extract = Zero | Cst of A.t | Var of A.t * Node.t * t

let extract p =
  if Node.M.is_empty p.poly then
    if A.equal p.cst A.zero then Zero else Cst p.cst
  else
    (* max binding is more interesting than choose (min binding) because it
       force results to be choosen as pivot*)
    let x, q = Node.M.max_binding p.poly in
    let p' = { p with poly = Node.M.remove x p.poly } in
    Var (q, x, p')

type kind = ZERO | CST | VAR

let classify p =
  if Node.M.is_empty p.poly then if A.equal p.cst A.zero then ZERO else CST
  else VAR

let monome c x =
  if A.equal A.zero c then cst A.zero
  else { cst = A.zero; poly = Node.M.singleton x c }

let is_one_node p =
  (* cst = 0 and one empty monome *)
  if A.equal A.zero p.cst && Node.M.is_num_elt 1 p.poly then
    let node, k = Node.M.choose p.poly in
    if A.equal A.one k then Some node else None
  else None

let nodes p : Node.S.t = Node.M.map (fun _ -> ()) p.poly
let sub_cst p q = { p with cst = A.sub p.cst q }
let add_cst p q = { p with cst = A.add p.cst q }

let mult_cst c p1 =
  if A.equal A.one c then p1
  else
    let poly_mult c m = Node.M.map (fun c1 -> A.mul c c1) m in
    if A.equal A.zero c then cst A.zero
    else { cst = A.mul c p1.cst; poly = poly_mult c p1.poly }

let none_zero c = if A.equal A.zero c then None else Some c

(** Warning Node.M.union can be used only for defining an operation [op] that
    verifies [op 0 p = p] and [op p 0 = p] *)
let add p1 p2 =
  let poly_add m1 m2 =
    Node.M.union (fun _ c1 c2 -> none_zero (A.add c1 c2)) m1 m2
  in
  { cst = A.add p1.cst p2.cst; poly = poly_add p1.poly p2.poly }

let sub p1 p2 =
  let poly_sub m1 m2 =
    Node.M.union_merge
      (fun _ c1 c2 ->
        match c1 with
        | None -> Some (A.neg c2)
        | Some c1 -> none_zero (A.sub c1 c2))
      m1 m2
  in
  { cst = A.sub p1.cst p2.cst; poly = poly_sub p1.poly p2.poly }

let neg p =
  let poly_neg m =
    Node.M.fold (fun n c acc -> Node.M.add n (A.neg c) acc) m Node.M.empty
  in
  { cst = A.neg p.cst; poly = poly_neg p.poly }

let x_p_cy p1 c p2 =
  assert (not (A.equal c A.zero));
  let f a b = A.add a (A.mul c b) in
  let poly m1 m2 =
    Node.M.union_merge
      (fun _ c1 c2 ->
        match c1 with
        | None -> Some (A.mul c c2)
        | Some c1 -> none_zero (f c1 c2))
      m1 m2
  in
  { cst = f p1.cst p2.cst; poly = poly p1.poly p2.poly }

let cx_p_cy c1 p1 c2 p2 =
  let c1_iszero = A.equal c1 A.zero in
  let c2_iszero = A.equal c2 A.zero in
  if c1_iszero && c2_iszero then zero
  else if c1_iszero then p2
  else if c2_iszero then p1
  else
    let f e1 e2 = A.add (A.mul c1 e1) (A.mul c2 e2) in
    let poly m1 m2 =
      Node.M.merge
        (fun _ e1 e2 ->
          match (e1, e2) with
          | None, None -> assert false
          | None, Some e2 -> Some (A.mul c2 e2)
          | Some e1, None -> Some (A.mul c1 e1)
          | Some e1, Some e2 -> none_zero (f e1 e2))
        m1 m2
    in
    { cst = f p1.cst p2.cst; poly = poly p1.poly p2.poly }

let subst_node p x y =
  let poly, qo = Node.M.find_remove x p.poly in
  match qo with
  | None -> (p, A.zero)
  | Some q ->
      let poly =
        Node.M.change
          (function None -> qo | Some q' -> none_zero (A.add q q'))
          y poly
      in
      ({ p with poly }, q)

let subst p x s =
  let poly, q = Node.M.find_remove x p.poly in
  match q with None -> (p, A.zero) | Some q -> (x_p_cy { p with poly } q s, q)

let fold f acc p = Node.M.fold_left f acc p.poly
let iter f p = Node.M.iter f p.poly

let of_list cst l =
  let fold acc (node, q) =
    Node.M.change
      (function None -> Some q | Some q' -> none_zero (A.add q q'))
      node acc
  in
  { cst; poly = List.fold_left fold Node.M.empty l }

let of_map cst poly =
  assert (Node.M.for_all (fun _ q -> not (A.equal A.zero q)) poly);
  { cst; poly }

let compute interp t =
  Node.M.fold_left (fun acc n k -> A.add acc (A.mul k (interp n))) t.cst t.poly

module ClM = Extmap.Make (Node)

type 'a tree = 'a ClM.view =
  | Empty
  | Node of 'a tree * Node.t * 'a * 'a tree * int

let get_tree p =
  ( ClM.view
      (Node.M.fold_left (fun acc node q -> ClM.add node q acc) ClM.empty p.poly),
    p.cst )

let normalize p =
  if Node.M.is_empty p.poly then cst (A.of_int (A.sign p.cst))
  else
    let init =
      if A.equal A.zero p.cst then snd (Node.M.choose p.poly) else p.cst
    in
    let num, den =
      Node.M.fold_left
        (fun (num, den) _ (q : A.t) ->
          ( (match q with Q q -> Z.gcd num q.num | A _ -> num),
            match q with Q q -> Z.gcd den q.den | A _ -> den ))
        ( (match init with Q q -> q.num | A _ -> Z.one),
          match init with Q q -> q.den | A _ -> Z.one )
        p.poly
    in
    if Z.equal Z.one num && Z.equal Z.one den then p
    else
      let conv q = A.mul (A.of_z den) (A.div (A.of_z num) q) in
      { poly = Node.M.map conv p.poly; cst = conv p.cst }

let print_poly fmt poly =
  let _ = print_poly fmt poly in
  ()
