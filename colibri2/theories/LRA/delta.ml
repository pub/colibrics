(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

(** Currently the distance graph only store information and reacts to change, it
    doesn't compute distances *)

module Edge = struct
  module T = struct
    type t = { src : Node.t; dst : Node.t } [@@deriving ord, eq, hash]

    let pp fmt t = Fmt.pf fmt "%a -> %a" Node.pp t.src Node.pp t.dst
  end

  include T
  include Colibri2_popop_lib.Popop_stdlib.MkDatatype (T)
end

module Dist = struct
  type t = { q : A.t; bound : Bound.t } [@@deriving show]

  let min d1 d2 =
    let c = A.compare d1.q d2.q in
    if c = 0 then
      let bound =
        match (d1.bound, d2.bound) with
        | Large, Large -> Bound.Large
        | _ -> Strict
      in
      { q = d1.q; bound }
    else if c < 0 then d1
    else d2

  let included d1 d2 =
    let c = A.compare d1.q d2.q in
    if c = 0 then
      match (d1.bound, d2.bound) with Large, Strict -> false | _ -> true
    else c < 0
end

module HEdge = Datastructure.Hashtbl (Edge)

let edges = HEdge.create Dist.pp "Delta.edges"
(* src - dst <= q *)

let a = Expr.Term.Var.mk "a" Expr.Ty.real
let ta = Expr.Term.of_var a

let floor_pattern =
  (* Other floor functions? *)
  Colibri2_theories_quantifiers.Pattern.of_term_exn ~subst:Ground.Subst.empty
    (Expr.Term.Real.floor_to_int ta)

(* let ceiling_patterns =
 *   Colibri2_theories_quantifiers.Pattern.of_term
 *     (Expr.Term.Real.ceiling ta) *)

let ceiling_pattern =
  Colibri2_theories_quantifiers.Pattern.of_term_exn ~subst:Ground.Subst.empty
    (Expr.Term.Int.minus
       (Expr.Term.Real.floor_to_int (Expr.Term.Real.minus ta)))

let event d src dst =
  match
    (HEdge.find_opt edges d { src; dst }, HEdge.find_opt edges d { dst; src })
  with
  | Some d1, Some d2 ->
      let aux src dst d1 d2 =
        if
          Dist.included d1 { q = A.one; bound = Strict }
          && Dist.included d2 { q = A.zero; bound = Large }
        then
          if Dom_interval.is_integer d src then
            (* 0 <= src - dst < 1 => src:Int => src = ceil(dst) *)
            let s =
              Colibri2_theories_quantifiers.Pattern.check_term_exists d
                {
                  Ground.Subst.empty with
                  term = Expr.Term.Var.M.singleton a dst;
                }
                ceiling_pattern
            in
            Node.S.iter (Egraph.merge d src) s
          else if Dom_interval.is_integer d dst then
            (* 0 <= src - dst < 1 => dst:Int => dst = floor(src) *)
            let s =
              Colibri2_theories_quantifiers.Pattern.check_term_exists d
                {
                  Ground.Subst.empty with
                  term = Expr.Term.Var.M.singleton a src;
                }
                floor_pattern
            in
            Node.S.iter (Egraph.merge d dst) s
      in
      aux src dst d1 d2;
      aux dst src d2 d1
  | _ -> ()

let add d src q bound dst =
  let d' = { Dist.q; bound } in
  HEdge.change
    (function Some d -> Some (Dist.min d d') | None -> Some d')
    edges d { src; dst };
  (* Could be only when a change append *)
  event d src dst

let add_le d src q dst = add d src q Large dst
let add_lt d src q dst = add d src q Strict dst
let add_ge d src q dst = add d dst q Large src
let add_gt d src q dst = add d dst q Strict src
let th_register _ = ()
