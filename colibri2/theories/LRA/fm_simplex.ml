(** Simulate fourier motskin using simplex IJCAR2012 *)

open Base
open Colibri2_popop_lib.Popop_stdlib
open Colibri2_core

(** Similar to the dual simplex *)

let debug =
  Debug.register_info_flag ~desc:"FM simulation with simplex <= < in LRA"
    "fm_simplex"

type eq = { inf : Boundary.t; p : A.t Node.M.t; sup : Boundary.t }
[@@deriving show]
(** An equation used for the simplex is a sum of scaled variables possibly
    bounded by inferior and superior bounds *)

module Var = struct
  include Int

  let is_int _ = false
  let print = Fmt.int
end

module S = OcplibSimplex.Basic.Make (Var) (Sim.Rat) (Sim.Ex)

type origin =
  | Var of { sup : bool; node : Node.t; bnd : Boundary.t }
  | Eq of { sup : bool; node : Node.t; eq : eq }
[@@deriving show]

let val_of_sol s =
  let (s : S.Core.solution) = Lazy.force s in
  let h = DInt.H.create 10 in
  Base.List.iter ~f:(fun (n, q) -> DInt.H.replace h n q) s.main_vars;
  Base.List.iter ~f:(fun (n, q) -> DInt.H.replace h n q) s.slake_vars;
  h

type combined_strict = ManyStrict | NoStrict | OneStrict of int
type ret = NoBound | Bounded of S.Core.t

let compute_combined_strict sol origins =
  let fold id origin acc =
    if A.is_zero (DInt.H.find sol id) then acc
    else
      match origin with
      | Var { bnd = Large _ } -> acc
      | Eq { sup = false; eq = { inf = Large _ } }
      | Eq { sup = true; eq = { sup = Large _ } } ->
          acc
      | Var { bnd = Strict _ }
      | Eq { sup = false; eq = { inf = Strict _ } }
      | Eq { sup = true; eq = { sup = Strict _ } } -> (
          match acc with
          | NoStrict -> OneStrict id
          | OneStrict _ | ManyStrict -> ManyStrict)
      | Eq { sup = false; eq = { inf = No } }
      | Eq { sup = true; eq = { sup = No } }
      | Var { bnd = No } ->
          assert false
  in
  DInt.H.fold fold origins NoStrict

let zero_bound = S.Core.{ bvalue = R2.of_r A.zero; explanation = () }
let one_bound = S.Core.{ bvalue = R2.of_r A.one; explanation = () }

let fm_simul d ~bound_repeated_nodes ((eqs, vars) : eq Polynome.H.t * Node.S.t)
    =
  let next = Util.next () in
  (* Associate original constraint to each simplex variable *)
  let origins = DInt.H.create 10 in
  (* Equations that insure that every variable disappear from linear combination *)
  let nulls = Node.H.create 10 in
  (* resulting constant of the linear combination *)
  let lincomb = ref S.Core.P.empty in
  (* 0 <= p + cst *)
  let add_cstr origin p cst =
    let id = next () in
    DInt.H.add origins id origin;
    (* negation because we want to minimize using maximize *)
    lincomb := fst (S.Core.P.accumulate id (A.neg cst) !lincomb);
    Node.M.iter
      (fun n c ->
        Node.H.change
          (function
            | None -> Some (`Sin (id, c))
            | Some (`Sin (id', c')) ->
                Some (`Poly (S.Core.P.from_list [ (id, c); (id', c') ]))
            | Some (`Poly acc) ->
                Some (`Poly (fst (S.Core.P.accumulate id c acc))))
          nulls n)
      p
  in
  let add_var node =
    let min, max = Boundaries.node_bounds d node in
    let add_bound ~sup p_cst cst_cst : Boundary.t -> _ = function
      | No -> ()
      | (Large q | Strict q) as bnd ->
          (* q <|<= node --> 0 <= node - q *)
          let origin = Var { sup; node; bnd } in
          let p = Node.M.singleton node p_cst in
          add_cstr origin p (cst_cst q)
    in
    (* q <|<= node --> 0 <= node - q *)
    add_bound ~sup:false A.one A.neg min;
    (* node <|<= q --> 0 <= - node + q *)
    add_bound ~sup:true A.minus_one Fn.id max
  in
  let add_eqs _ eq =
    let add_bound ~sup p_cst cst_cst : Boundary.t -> _ = function
      | No -> ()
      | Large q | Strict q ->
          (* q <|<= node --> 0 <= node - q *)
          let p = Polynome.of_map A.zero eq.p in
          let node = Dom_polynome.node_of_polynome d p in
          let origin = Eq { sup; eq; node } in
          let p = Node.M.map p_cst eq.p in
          add_cstr origin p (cst_cst q)
    in
    (* q <|<= node --> 0 <= p - q *)
    add_bound ~sup:false Fn.id A.neg eq.inf;
    (* node <|<= q --> 0 <= - p + q *)
    add_bound ~sup:true A.neg Fn.id eq.sup
  in
  Node.S.iter add_var vars;
  Polynome.H.iter add_eqs eqs;
  if DInt.H.length origins <= 1 then Debug.dprintf0 debug "[Simplex] FM:empty"
  else
    let simplex = S.Core.empty ~is_int:false ~check_invs:false in
    let add_null _ eq simplex =
      let id = next () in
      match eq with
      | `Sin (n, _) ->
          fst @@ S.Assert.var simplex n ~min:zero_bound ~max:zero_bound
      | `Poly p ->
          fst @@ S.Assert.poly simplex p id ~min:zero_bound ~max:zero_bound
    in
    let bound_coefs id _ simplex =
      fst @@ S.Assert.var simplex id ~min:zero_bound ?max:None
    in
    (* All the coefficient are positive *)
    let simplex = DInt.H.fold bound_coefs origins simplex in
    (* All the variable are eliminated, sum null *)
    let simplex = Node.H.fold add_null nulls simplex in
    let rec check_equalities simplex =
      (* The combination is not trivial (the sum is positive )*)
      let sum =
        DInt.H.fold
          (fun id _ acc -> fst (S.Core.P.accumulate id A.one acc))
          origins S.Core.P.empty
      in
      let simplex =
        let id = next () in
        fst @@ S.Assert.poly simplex sum id ~min:one_bound ?max:None
      in
      run_simplex simplex
    and run_simplex simplex =
      (* Minimize the result of the linear combination *)
      let simplex, opt = S.Solve.maximize simplex !lincomb in
      match S.Result.get opt simplex with
      | S.Core.Unknown -> assert false
      | S.Core.Unsat _ ->
          Debug.dprintf0 debug "[Simplex] FM:nobound";
          NoBound
      | S.Core.Unbounded _ ->
          Debug.dprintf0 debug "[Simplex] FM:unsat";
          Egraph.contradiction ()
      | S.Core.Sat _ -> assert false
      | S.Core.Max (m, sol) ->
          let m = Lazy.force m in
          assert m.is_le;
          assert (A.is_zero m.max_v.bvalue.offset);
          let m = A.neg m.max_v.bvalue.v in
          let sol = val_of_sol sol in
          (* Debug.dprintf4 debug "[Simplex] FM:@ @[sol:%a@]@ @[origins:%a@]"
            (DInt.H.pp A.pp) sol (DInt.H.pp pp_origin) origins; *)
          if A.is_zero m then (
            Debug.dprintf0 debug "[Simplex] FM:equalities";
            let handle_equalities id origin =
              if A.is_zero (DInt.H.find sol id) then true
              else
                match origin with
                | Var { bnd = Large q; node } ->
                    RealValue.set d node q;
                    false
                | Eq { sup = false; eq = { inf = Large q; p } }
                | Eq { sup = true; eq = { sup = Large q; p } } ->
                    let p = Polynome.of_map A.zero p in
                    let node = Dom_polynome.node_of_polynome d p in
                    RealValue.set d node q;
                    false
                | Var { bnd = Strict _ }
                | Eq { sup = false; eq = { inf = Strict _ } }
                | Eq { sup = true; eq = { sup = Strict _ } } ->
                    Debug.dprintf3 debug
                      "[Simplex] FM: unsat 0 > 0 for id:%i with %a" id pp_origin
                      origin;
                    Egraph.contradiction ()
                | Eq { sup = false; eq = { inf = No } }
                | Eq { sup = true; eq = { sup = No } }
                | Var { bnd = No } ->
                    assert false
            in
            DInt.H.filter handle_equalities origins;
            if DInt.H.length origins <= 1 then (
              Debug.dprintf0 debug "[Simplex] FM:empty";
              NoBound)
            else check_equalities simplex)
          else (
            Debug.dprintf0 debug "[Simplex] FM: bounds";
            let cstrict = compute_combined_strict sol origins in
            let handle_inequalities id origin =
              let lam = DInt.H.find sol id in
              if A.is_zero lam then ()
              else
                let sup_inf ~sup node q =
                  let m_lam = A.div m lam in
                  let q = if sup then A.sub q m_lam else A.add q m_lam in
                  let u =
                    if
                      match cstrict with
                      | NoStrict -> true
                      | OneStrict id' when id = id' -> true
                      | _ -> false
                    then
                      if sup then Dom_interval.D.ge q else Dom_interval.D.le q
                    else if sup then Dom_interval.D.gt q
                    else Dom_interval.D.lt q
                  in
                  Debug.dprintf4 debug "Propagate %a for %a" Dom_interval.D.pp u
                    Node.pp node;
                  Dom_interval.upd_dom d node u
                in
                match origin with
                | Var { bnd = Large q | Strict q; node; sup } ->
                    sup_inf ~sup node q
                | Eq
                    {
                      sup = false as sup;
                      node;
                      eq = { inf = Large q | Strict q };
                    }
                | Eq
                    {
                      sup = true as sup;
                      node;
                      eq = { sup = Large q | Strict q };
                    } ->
                    sup_inf ~sup node q
                | Eq { sup = false; eq = { inf = No } }
                | Eq { sup = true; eq = { sup = No } }
                | Var { bnd = No } ->
                    assert false
            in
            DInt.H.iter handle_inequalities origins;
            Bounded simplex)
    in
    match check_equalities simplex with
    | NoBound -> ()
    | Bounded simplex ->
        let l =
          DInt.H.fold
            (fun id origin acc ->
              match origin with
              | Var { node } | Eq { node } ->
                  if Node.S.mem node bound_repeated_nodes then (id, node) :: acc
                  else acc)
            origins []
        in
        let bound_repeated_node (id, node) =
          let simplex =
            fst @@ S.Assert.var simplex id ~min:one_bound ~max:one_bound
          in
          Debug.dprintf3 debug "[Simplex] run fm for %i (%a)" id Node.pp node;
          ignore (run_simplex simplex)
        in
        List.iter ~f:bound_repeated_node l

(* a*x - y *)
