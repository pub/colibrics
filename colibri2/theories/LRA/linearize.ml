(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2024                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

(* Define a Datatype type wrapping a Node with extra information
   related to linearization and scheduling. FIXME? this looks a lot
   like a domain attached to a node, should it be refactored as such? *)

module R = Context.Ref

module T = struct
  open Base

  type t = {
    (* associated node *)
    node : Node.t;
    (* set to true to prevent multiple scheduling *)
    scheduled : bool R.t; [@hash.ignore]
    (* set to true when the node domain should no longer be observed *)
    rejected : bool R.t; [@hash.ignore]
    (* necessary information for simplex to make a linear approximation *)
    mutable parameters : Linearize_intf.t option; [@hash.ignore]
  }
  [@@deriving hash, show]

  type map = t Node.H.t

  let make_map () : map = Node.H.create 10

  let fold (m : map) (f : 'a -> Linearize_intf.t -> 'a) (init : 'a) =
    let g _ v a =
      match (R.get v.rejected, v.parameters) with
      | false, Some p -> f a p
      | _ -> a
    in
    Node.H.fold g m init

  let intern ctx mapping node =
    match Node.H.find_opt mapping node with
    | Some n -> n
    | None ->
        let result =
          {
            node;
            scheduled = R.create ctx false;
            rejected = R.create ctx false;
            parameters = None;
          }
        in
        Node.H.add mapping node result;
        result
end

module Log = Linearize_intf.Log

let ids = ref 0

let fresh_id () =
  let res = !ids in
  ids := !ids + 1;
  res

(** FIXME: node_abs/node_sign are duplicated *)

(** Get sign in Dom_product domain of node *)
let node_sign egraph node = Dom_product.SolveSign.get_repr egraph node

(** Get abs in Dom_product domain of node *)
let node_abs egraph node = Dom_product.SolveAbs.get_repr egraph node

let opt_pp default pp fmt = function
  | None -> Format.fprintf fmt "%s" default
  | Some v -> Format.fprintf fmt "%a" pp v

module Daemon = struct
  let key =
    Events.Dem.create
      (module struct
        type t = T.t

        let name = "LRA.linearize"
      end)

  type runable = T.t

  (* run immediately (on domain change) because (i) the possible
     linear approximations should be known before the simplex runs and
     (ii) the handler doesn't do a lot of work anyway here *)
  let delay = Events.Immediate
  let print_runable = T.pp
  let mapping = T.make_map ()
  let fold (f : 'a -> Linearize_intf.t -> 'a) init = T.fold mapping f init

  let check egraph (T.{ node } as t) =
    let is_sign_known s =
      match Sign_product.classify s with
      | Sign_product.PLUS_ONE | MINUS_ONE | NODE _ -> true
      (* unexpected because the simplex avoids this case *)
      | Sign_product.ZERO ->
          Log.error "unexpected zero sign";
          false
      | _ -> false
    in
    let has_sign node =
      node |> node_sign egraph |> Option.map is_sign_known
      |> Option.value ~default:false
    in
    let s, a = (node_sign egraph node, node_abs egraph node) in
    Log.debug "check node=%a sign=%a abs=%a" Node.pp node
      (opt_pp "/" Sign_product.pp)
      s (opt_pp "/" Product.pp) a;
    let keep x y sign abs =
      Log.info "add/update node=%a sign=%a abs=%a" Node.pp node Sign_product.pp
        sign Product.pp abs;
      t.parameters <- Some { id = fresh_id (); x; y; z = node }
    in
    match (s, a) with
    (* FIXME: handle sign = x^1.y^1 && abs = |x.y| *)
    (* FIXME: handle sign = x^1 && abs = |x| *)
    (* FIXME: handle sign = +|- && abs = k *)

    (* both sign and abs are known *)
    | Some sign, Some abs when A.equal A.one abs.cst && is_sign_known sign -> (
        match Product.terms abs with
        | [ (x, p) ] when Q.(equal two p) ->
            (* FIXME: check sign for square? *)
            (* keep x x sign abs *)
            ignore x;
            Log.warn "|n| = x² is not implemented";
            R.set t.rejected true
        | [ (x, p); (y, q) ] when Q.(equal one p && equal one q) ->
            if has_sign x && has_sign y then keep x y sign abs
        | _ -> R.set t.rejected true)
    (* missing information, do not reject yet *)
    | _ -> ()

  let run egraph (t : T.t) =
    R.set t.scheduled false;
    check egraph t

  let handler egraph node =
    let t = T.intern (Egraph.context egraph) mapping node in
    if R.get t.scheduled then Events.EnqAlready
    else if R.get t.rejected then Events.EnqStopped
    else (
      R.set t.scheduled true;
      Events.EnqRun (key, t, None))
end

(* interface implementation *)

let check egraph (node : Node.t) =
  Dom_interval.events_attach_dom egraph node Daemon.handler

let fold = Daemon.fold
let () = Events.register (module Daemon)
