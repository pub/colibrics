(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

let debug =
  Debug.register_info_flag ~desc:"for intervals for the arithmetic theory"
    "LRA.interval"

let threshold_max_repeat =
  Options.register ~pp:Fmt.int "threshold_max_repeat"
    Cmdliner.(
      Arg.(
        value & opt int 50
        & info [ "threshold-max-repeat" ]
            ~docs:"Number of maximum modification of a domain between decisions"))

module D = struct
  include Colibri2_theories_LRA_stages.Interval_domain

  let is_zero_or_positive p =
    match is_comparable zero p with
    | Le | Lt | Eq -> true
    | Ge | Gt | Uncomparable -> false

  let none_is_contradiction = function
    | None -> Egraph.contradiction ()
    | Some (d : t) -> d

  let mk_exn a ba bb b =
    Option.get (from_convexe_hull (Some (a, ba), Some (b, bb)))

  let le_zero_lt_one = mk_exn A.zero Large Strict A.one
end

let dom =
  Dom.Kind.create
    (module struct
      type t = D.t

      let name = "ARITH"
    end)

module Dom = Dom.LatticeLimitRepeat (struct
  include D

  let key = dom
  let inter _ d1 d2 = inter d1 d2

  let is_singleton _ _ d =
    Option.map
      (fun x -> RealValue.nodevalue @@ RealValue.index x)
      (is_singleton d)

  let repeat_limit d = Options.get d threshold_max_repeat
  let wakeup_threshold = None
end)

let get_dom d = Egraph.get_dom d dom
let upd_dom = Dom.upd_dom

let wait_dom =
  let attach d n f = Events.attach_dom d ~direct:false n dom (fun d _ -> f d) in
  let has_value d n = Option.is_some @@ get_dom d n in
  { attach; get = get_dom; has_value }

let is_zero_or_positive d n =
  match get_dom d n with None -> false | Some p -> D.is_zero_or_positive p

let is_not_zero d n =
  match get_dom d n with None -> false | Some p -> D.absent A.zero p

let is_strictly_positive d n =
  match get_dom d n with
  | None -> false
  | Some p -> (
      match D.is_comparable D.zero p with
      | D.Lt -> true
      | D.Eq | D.Le | D.Ge | D.Gt | D.Uncomparable -> false)

let is_strictly_negative d n =
  match get_dom d n with
  | None -> false
  | Some p -> (
      match D.is_comparable D.zero p with
      | D.Gt -> true
      | D.Eq | D.Le | D.Ge | D.Lt | D.Uncomparable -> false)

let assume_negative d n = upd_dom d n (D.lt A.zero)
let assume_positive d n = upd_dom d n (D.gt A.zero)

let zero_is d n =
  let r =
    match get_dom d n with
    | None -> D.Uncomparable
    | Some p -> D.is_comparable D.zero p
  in
  Debug.dprintf3 debug "zero_is: %a %s" Node.pp n
    (match r with
    | Lt -> "lt"
    | Le -> "le"
    | Ge -> "ge"
    | Eq -> "eq"
    | Gt -> "gt"
    | Uncomparable -> "unc");
  r

let is_integer d n =
  match get_dom d n with None -> false | Some p -> D.is_integer p

let neg_cmp = function `Lt -> `Ge | `Le -> `Gt | `Ge -> `Lt | `Gt -> `Le
let com_cmp = function `Lt -> `Gt | `Le -> `Ge | `Ge -> `Le | `Gt -> `Lt

let backward = function
  | `Lt -> D.lt'
  | `Gt -> D.gt'
  | `Le -> D.le'
  | `Ge -> D.ge'

(** {2 Initialization} *)
module Propagate = struct
  open MonadAlsoInterp

  let setb = setv Boolean.dom
  let getb = getv Boolean.dom

  let set =
    updd
      ~check:(fun d n v ->
        match get_dom d n with
        | None -> true
        | Some v' -> not (D.is_included v' v))
      upd_dom

  let get = generic_wait wait_dom

  let unop loc ?thterm ?tores ?toarg d r args =
    let nopif a b = function
      | None -> nop
      | Some f ->
          set b
            (let+ va = get a in
             f va)
    in
    let a = IArray.extract1_exn args in
    Egraph.register d a;
    attach loc ?thterm d (nopif a r tores && nopif r a toarg)

  let binop loc ?thterm ?tores ?tofst ?tosnd d r args =
    let nopif a b c = function
      | None -> nop
      | Some f ->
          set c
            (let+ va = get a and+ vb = get b in
             f va vb)
    in
    let a, b = IArray.extract2_exn args in
    Egraph.register d a;
    Egraph.register d b;
    attach loc ?thterm d
      (nopif a b r tores && nopif b r a tofst && nopif a r b tosnd)

  let sub loc ?thterm d ~r ~a ~b =
    let args = IArray.of_array [| a; b |] in
    binop loc ?thterm d r args ~tores:D.minus ~tosnd:D.minus ~tofst:D.add

  let cmp loc ?thterm d cmp ~r ~a ~b =
    let reg n = Egraph.register d n in
    let test c =
      match c with
      | D.Uncomparable -> None
      | Le -> (
          match cmp with
          | `Le -> Some true
          | `Gt -> Some false
          | `Lt | `Ge -> None)
      | Lt -> (
          match cmp with `Le | `Lt -> Some true | `Ge | `Gt -> Some false)
      | Eq -> (
          match cmp with `Le | `Ge -> Some true | `Lt | `Gt -> Some false)
      | Ge -> (
          match cmp with
          | `Ge -> Some true
          | `Lt -> Some false
          | `Gt | `Le -> None)
      | Gt -> (
          match cmp with `Gt | `Ge -> Some true | `Lt | `Le -> Some false)
    in
    reg a;
    reg b;
    attach loc ?thterm d
      (setb r
         (let* va = get a and+ vb = get b in
          test (D.is_comparable va vb))
      && set b
           (let+ va = get a and+ vr = getb r in
            backward (if vr then com_cmp cmp else com_cmp (neg_cmp cmp)) va)
      && set a
           (let+ vb = get b and+ vr = getb r in
            backward (if vr then cmp else neg_cmp cmp) vb))

  (** a in u <=> r *)
  let bool_equiv_in_dom loc ?thterm d ~r ~a u =
    attach loc ?thterm d
      (setb r
         (let* ua = get a in
          if D.is_included ua u then Some true
          else if D.is_distinct ua u then Some false
          else None)
      && set a
           (let+ vr = getb r in
            if vr then u else D.none_is_contradiction @@ D.complement u))

  let gen_ceil_truncate_floor d r args ~forward =
    let a = IArray.extract1_exn args in
    Egraph.register d a;
    upd_dom d r D.integers;
    attach [%here] d
      (exec
         (fun d -> Egraph.merge d a r)
         (let+ va = get a in
          D.is_integer va)
      && set r
           (let+ da = get a in
            forward da))
end

let positive_int_sequence_with_zero =
  let open Sequence.Generator in
  let rec loop i = yield i >>= fun () -> loop (Z.succ i) in
  run (yield Z.zero >>= fun () -> loop Z.one)

let interval_sequence_decreasing start end_ =
  positive_int_sequence_with_zero
  |> Sequence.map ~f:(fun z -> Z.sub end_ z)
  |> Sequence.take_while ~f:(fun z -> Z.leq start z)

let interval_sequence_increasing start end_ =
  positive_int_sequence_with_zero
  |> Sequence.map ~f:(fun z -> Z.add start z)
  |> Sequence.take_while ~f:(fun z -> Z.leq z end_)

let iterate_domain_integer i =
  let i = D.reduce_integers i in
  match i with
  | None -> Sequence.empty
  | Some i -> (
      let i = D.get_union i in
      let rec aux_on_neg start acc = function
        | D.Union.Sin _ -> assert false (* absurd: included in integer *)
        | Chg (q, _, l) ->
            assert (Z.sign start <= 0);
            let q = A.to_z q in
            if Z.sign q <= 0 then
              aux_off_neg
                (Sequence.append (interval_sequence_decreasing start q) acc)
                l
            else
              let neg_acc =
                Sequence.append (interval_sequence_decreasing start Z.zero) acc
              in
              let pos_acc =
                Sequence.append
                  (interval_sequence_increasing Z.one q)
                  (aux_off_pos l)
              in
              Sequence.round_robin [ neg_acc; pos_acc ]
        | Inf ->
            assert (Z.sign start <= 0);
            let neg_acc =
              Sequence.append (interval_sequence_decreasing start Z.zero) acc
            in
            let pos_acc =
              positive_int_sequence_with_zero
              |> Sequence.map ~f:(fun z -> Z.add start z)
            in
            Sequence.round_robin [ neg_acc; pos_acc ]
      and aux_off_neg acc = function
        | Sin (q, l) -> aux_off_neg (Sequence.shift_right acc (A.to_z q)) l
        | Chg (q, _, l) ->
            let q = A.to_z q in
            if Z.sign q <= 0 then aux_on_neg q acc l
            else Sequence.round_robin [ acc; aux_on_pos q l ]
        | Inf -> acc
      and aux_on_pos start = function
        | Sin _ -> assert false
        | Chg (q, _, l) ->
            let q = A.to_z q in
            Sequence.append
              (interval_sequence_increasing start q)
              (aux_off_pos l)
        | Inf ->
            positive_int_sequence_with_zero
            |> Sequence.map ~f:(fun z -> Z.add start z)
      and aux_off_pos = function
        | Sin (q, l) -> Sequence.shift_right (aux_off_pos l) (A.to_z q)
        | Chg (q, _, l) ->
            let q = A.to_z q in
            aux_on_pos q l
        | Inf -> Sequence.empty
      in
      match i.u with
      | On (Sin _) -> assert false
      | On Inf -> RealValue.int_sequence
      | On (Chg (q, _, l)) ->
          let q = A.to_z q in
          aux_off_neg
            (positive_int_sequence_with_zero
            |> Sequence.map ~f:(fun z -> Z.sub q z))
            l
      | Off u -> aux_off_neg Sequence.empty u)

(** enumeration without repetition of positive rational *)
let calkin_wilf_step q = Q.(inv (Q.mul_2exp (Q.floor q) 1 - q + Q.one))

(** 1/2, 2, 1/3, ... *)
let calkin_wilf_sequence_w_1 =
  Sequence.unfold ~init:Q.one ~f:(fun q ->
      let q = calkin_wilf_step q in
      Some (q, q))

let calkin_wilf_sequence = Sequence.shift_right calkin_wilf_sequence_w_1 Q.one

(** only between stricly 0 and 1 *)
let calkin_wilf_sequence_0_1 =
  let half = Q.inv Q.two in
  let s =
    Sequence.unfold ~init:half ~f:(fun q ->
        let q = calkin_wilf_step q in
        let q = calkin_wilf_step q in
        Some (q, q))
  in
  Sequence.shift_right s half

(** Singletons are not taken into account *)
let positive_sequence_of_union (u : D.t) =
  let u = D.get_union u in
  let sequence_of_interval q1 b1 q2 b2 =
    let append_bound q (b : Colibrilib.Interval.Bound.t) e =
      match b with Large -> Sequence.shift_right e q | Strict -> e
    in
    let d = A.sub q2 q1 in
    calkin_wilf_sequence_0_1
    |> Sequence.map ~f:(fun q -> A.add q1 (A.mul d (A.of_q q)))
    |> append_bound q1 b1 |> append_bound q2 b2
  in
  let rec aux_on start bound acc = function
    | D.Union.Sin (q, l) ->
        let s = sequence_of_interval start bound q Strict in
        aux_on q Strict (s :: acc) l
    | Chg (q, b, l) ->
        let s =
          sequence_of_interval start bound q
            (Colibrilib.Interval.Bound.inv_bound b)
        in
        aux_off (s :: acc) l
    | Inf ->
        let s =
          Sequence.map calkin_wilf_sequence ~f:(fun q -> A.add start (A.of_q q))
        in
        let s =
          match bound with Large -> Sequence.shift_right s start | Strict -> s
        in
        s :: acc
  and aux_off acc = function
    | D.Union.Sin (q, l) -> aux_off (Sequence.singleton q :: acc) l
    | Chg (q, b, l) -> aux_on q b acc l
    | Inf -> acc
  in
  match u.u with
  | On _ -> assert false (* only positive *)
  | Off u -> Sequence.round_robin (aux_off [] u)

let iterate_domain_rational t =
  let pos = D.(inter (ge A.zero) t) in
  let neg =
    Base.Option.map ~f:(D.mult_cst A.minus_one) D.(inter (lt A.zero) t)
  in
  let aux = function
    | None -> Sequence.empty
    | Some u -> positive_sequence_of_union u
  in
  Sequence.round_robin [ aux pos; Sequence.map (aux neg) ~f:A.neg ]

let iterate_domain (i : D.t) =
  match (D.get_union i).integrability with
  | MaybeReal -> iterate_domain_rational i
  | Integer -> Sequence.map ~f:A.of_z (iterate_domain_integer i)

let nodes_reach_repeat_limit = Dom.nodes_reach_repeat_limit
let events_attach_dom env ?direct n f = Events.attach_dom env ?direct n dom f
let events_attach_any_dom env f = Events.attach_any_dom env dom f

let daemon_attach_dom env ?once ?filter ?direct n f =
  DaemonAlsoInterp.attach_dom env ?once ?filter ?direct n dom f

let daemon_attach_any_dom env ?once ?filter f =
  DaemonAlsoInterp.attach_any_dom env ?once ?filter dom f

let daemonwithfilter_attach_any_dom env ?thterm f =
  DaemonWithFilter.attach_any_dom env ?thterm dom f

let init env =
  DaemonAlsoInterp.attach_reg_value env RealValue.key (fun d value ->
      let v = RealValue.value value in
      let s = D.singleton v in
      upd_dom d (RealValue.node value) s);
  Disequality.register_disequal env (fun env n1 n2 ->
      match (get_dom env n1, get_dom env n2) with
      | None, None | Some _, None | None, Some _ -> false
      | Some d1, Some d2 -> D.is_distinct d1 d2);
  Interp.Register.node env (fun _ d n ->
      match get_dom d n with
      | None -> None
      | Some inter ->
          Some
            (Interp.Seq.of_seq
            @@ (iterate_domain inter
               |> Sequence.map ~f:(fun x ->
                      RealValue.nodevalue @@ RealValue.index x))));
  Disequality.register_disequality_to_value env (fun _ v f ->
      match RealValue.of_nodevalue v with
      | None -> ()
      | Some rv ->
          let v = RealValue.value rv in
          f (fun env n ->
              get_dom env n
              |> Option.value ~default:D.reals
              |> (fun i -> D.none_is_contradiction @@ D.except i v)
              |> upd_dom env n))
