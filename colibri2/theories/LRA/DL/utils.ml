(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

(*
  The graph represents difference constraints of the form:
    x - y <= c: y --{c}--> x
  if x and y are:
  - reals or rationals:
    x - y < c ---> x - y <= c - k*delta
  - integers:
    x - y < c ---> x - y <= c - 1
*)
let dl_debug =
  Debug.register_flag ~desc:"Debugging messages for the difference logic engin"
    "DL"

type _ Expr.t += Rat_DL_Delta | Real_DL_Delta
(* Delta: smallest non-zero positive real or rational value. *)

let dl_delta_rat =
  Expr.Id.mk ~name:"colibri2_dl_delta_rat" ~builtin:Rat_DL_Delta
    (Dolmen_std.Path.global "colibri2_dl_delta_rat")
    Expr.Ty.rat

let dl_delta_real =
  Expr.Id.mk ~name:"colibri2_dl_delta_real" ~builtin:Real_DL_Delta
    (Dolmen_std.Path.global "colibri2_dl_delta_real")
    Expr.Ty.real

type ty = Int | Rat | Real [@@deriving hash]
type op = Lt | Leq | Gt | Geq | Eq | Neq [@@deriving hash]

let pp_op fmt = function
  | Leq -> Fmt.pf fmt "<="
  | Geq -> Fmt.pf fmt ">="
  | Lt -> Fmt.pf fmt "<"
  | Gt -> Fmt.pf fmt ">"
  | Eq -> Fmt.pf fmt "="
  | Neq -> Fmt.pf fmt "=/="

let neg_op = function
  | Leq -> Gt
  | Geq -> Lt
  | Lt -> Geq
  | Gt -> Leq
  | Eq -> Neq
  | Neq -> Eq

let inv_op = function
  | Leq -> Geq
  | Geq -> Leq
  | Lt -> Gt
  | Gt -> Lt
  | Eq -> Eq
  | Neq -> Neq

let conv_op = function
  | Expr.Leq _ -> Leq
  | Expr.Geq _ -> Geq
  | Expr.Lt _ -> Lt
  | Expr.Gt _ -> Gt
  | Expr.Equal -> Eq
  | Expr.Distinct -> Neq
  | _ -> assert false

let conv_ty = function `Int -> Int | `Rat -> Rat | `Real -> Real

module Z = struct
  include Z

  let hash_fold_t s z = Base.Int.hash_fold_t s (Z.to_int z)
end

module Dist = struct
  module C = struct
    include A

    let succ a = A.add a A.one
    let pred a = A.sub a A.one
    let node = RealValue.cst
  end

  type t =
    | Cst of { c : C.t; ty : ty }
    | Delta of { c' : C.t; k : Z.t; ty' : ty }
  [@@deriving hash]

  let cst c ty = Cst { c; ty }
  let delta c' k ty' = Delta { c'; k; ty' }

  let pp fmt = function
    | Cst { c; _ } -> A.pp fmt c
    | Delta { c'; k; _ } ->
        Format.fprintf fmt "(%a + %s*Delta)" A.pp c' (Z.to_string k)

  let succ = function
    | Cst { c; ty = Int } -> Cst { c = A.add c A.one; ty = Int }
    | Delta { ty' = Int; _ } -> assert false
    | Cst { c = c'; ty = ty' } -> Delta { c'; k = Z.one; ty' }
    | Delta delta -> Delta { delta with k = Z.succ delta.k }
  [@@warning "-32"]

  let pred = function
    | Cst { c; ty = Int } -> Cst { c = A.sub c A.one; ty = Int }
    | Delta { ty' = Int; _ } -> assert false
    | Cst { c = c'; ty = ty' } -> Delta { c'; k = Z.minus_one; ty' }
    | Delta ({ c' = c; k = k'; ty' = ty } as delta) ->
        let k = Z.pred k' in
        if Z.equal k Z.zero then Cst { c; ty } else Delta { delta with k }

  let mk_pred c ty =
    match ty with Int -> cst (A.sub c A.one) ty | _ -> delta c Z.minus_one ty

  let add_c a c =
    match a with
    | Cst ({ c = c' } as b) -> Cst { b with c = A.add c' c }
    | Delta ({ c' } as b) -> Delta { b with c' = A.add c' c }

  let sub_c a c =
    match a with
    | Cst ({ c = c' } as b) -> Cst { b with c = A.sub c' c }
    | Delta ({ c' } as b) -> Delta { b with c' = A.sub c' c }

  let add a b =
    match (a, b) with
    | Cst { c = a'; ty }, Cst { c = b'; _ } -> cst (A.add a' b') ty
    | Cst { c = a'; _ }, Delta ({ c' = b'; _ } as delta)
    | Delta ({ c' = b'; _ } as delta), Cst { c = a'; _ } ->
        Delta { delta with c' = A.add a' b' }
    | Delta a', Delta b' ->
        let nc = A.add a'.c' b'.c' in
        let nk = Z.add a'.k b'.k in
        let ty = a'.ty' in
        if Z.equal nk Z.zero then cst nc ty else delta nc nk ty

  let sub a b =
    match (a, b) with
    | Cst { c = a'; ty }, Cst { c = b'; _ } -> cst (A.sub a' b') ty
    | Cst { c = a'; _ }, Delta ({ c' = b'; k; _ } as delta) ->
        Delta { delta with c' = A.sub a' b'; k = Z.neg k }
    | Delta ({ c' = b'; _ } as delta), Cst { c = a'; _ } ->
        Delta { delta with c' = A.sub b' a' }
    | Delta a', Delta b' ->
        let nc = A.sub a'.c' b'.c' in
        let nk = Z.sub a'.k b'.k in
        let ty = a'.ty' in
        if Z.equal nk Z.zero then cst nc ty else delta nc nk ty

  let neg = function
    | Cst cst -> Cst { cst with c = A.neg cst.c }
    | Delta d -> Delta { d with c' = A.neg d.c'; k = Z.neg d.k }

  let delta_le acst ak bcst bk =
    A.lt acst bcst || (A.equal acst bcst && Z.leq ak bk)

  let delta_lt acst ak bcst bk =
    A.lt acst bcst || (A.equal acst bcst && Z.lt ak bk)

  let zero ty = cst A.zero ty

  let is_zero = function
    | Cst a -> A.equal a.c A.zero
    | Delta { c'; k; _ } -> A.equal c' A.zero && Z.equal k Z.zero

  let le a b =
    match (a, b) with
    | Cst { c = a' }, Cst { c = b' } -> A.le a' b'
    | Cst { c = a' }, Delta { c' = bcst; k } -> delta_le a' Z.zero bcst k
    | Delta { c' = bcst; k }, Cst { c = a' } -> delta_le bcst k a' Z.zero
    | Delta { c' = acst; k = ak }, Delta { c' = bcst; k = bk } ->
        delta_le acst ak bcst bk

  let lt a b =
    match (a, b) with
    | Cst { c = a' }, Cst { c = b' } -> A.lt a' b'
    | Cst { c = a' }, Delta { c' = bcst; k } -> delta_lt a' Z.zero bcst k
    | Delta { c' = bcst; k }, Cst { c = a' } -> delta_lt bcst k a' Z.zero
    | Delta { c' = acst; k = ak }, Delta { c' = bcst; k = bk } ->
        delta_lt acst ak bcst bk

  let equal a b =
    match (a, b) with
    | Cst { c = a' }, Cst { c = b' } -> A.equal a' b'
    | Cst { c = a' }, Delta { c' = bcst; k }
    | Delta { c' = bcst; k }, Cst { c = a' } ->
        Z.equal k Z.zero && A.equal a' bcst
    | Delta { c' = acst; k = ak }, Delta { c' = bcst; k = bk } ->
        Z.equal ak bk && A.equal acst bcst

  let compare a b = if equal a b then 0 else if le a b then -1 else 1
  let min a b = if le a b then a else b

  let node =
    let of_ty ty =
      match ty with
      | Rat ->
          ( Expr.Ty.rat,
            Expr.Term.Const.Rat.add,
            Expr.Term.Const.Rat.mul,
            dl_delta_rat )
      | Real ->
          ( Expr.Ty.real,
            Expr.Term.Const.Real.add,
            Expr.Term.Const.Real.mul,
            dl_delta_real )
      | Int ->
          (* unreachable *)
          assert false
    in
    fun env -> function
      | Cst { c } -> RealValue.cst c
      | Delta { c'; k; ty' } ->
          let ty, add, mul, delta = of_ty ty' in
          let kn = RealValue.cst (A.of_z k) in
          let dn = Ground.convert_one_cst Ground.Subst.empty env delta ty in
          let mulres = Ground.apply' env mul [] [ kn; dn ] in
          let cn = RealValue.cst c' in
          let addres = Ground.apply' env add [] [ cn; mulres ] in
          addres
  [@@warning "-32"]

  let ty = function Cst { ty } -> ty | Delta { ty' } -> ty'
end

module NHT = Datastructure.Hashtbl (Node)
