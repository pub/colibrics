(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Utils

module type DistSig = sig
  module C : sig
    type t

    val pp : t Fmt.t
    val equal : t -> t -> bool
    val compare : t -> t -> int

    val hash_fold_t :
      Base_internalhash_types.state -> t -> Base_internalhash_types.state

    val add : t -> t -> t
    val sub : t -> t -> t
    val neg : t -> t
    val succ : t -> t
    val pred : t -> t
    val node : t -> Node.t
  end

  type t

  val pp : t Fmt.t
  val equal : t -> t -> bool
  val compare : t -> t -> int

  val hash_fold_t :
    Base_internalhash_types.state -> t -> Base_internalhash_types.state

  val zero : ty -> t
  val cst : C.t -> ty -> t
  val delta : C.t -> Z.t -> ty -> t
  val mk_pred : C.t -> ty -> t
  val pred : t -> t
  val add : t -> t -> t
  val sub : t -> t -> t
  val add_c : t -> C.t -> t
  val sub_c : t -> C.t -> t
  val neg : t -> t
  val min : t -> t -> t
  val is_zero : t -> bool
  val le : t -> t -> bool
  val lt : t -> t -> bool
  val ty : t -> ty
end

module type S = sig
  type c
  type t

  val pp : 'a Egraph.t -> unit

  val on_repr_change :
    Egraph.wt -> old_repr:Node.t -> repr:Node.t -> delta:c -> unit

  val fold_edges :
    'a Egraph.t ->
    ('a Egraph.t -> Node.t -> Node.t -> t -> 'b -> 'b) ->
    Node.t ->
    'b ->
    'b

  val get_dist : Egraph.wt -> Node.t -> Node.t -> t option
  val get_edge : Egraph.wt -> Node.t -> Node.t -> t option
  val add_edge : Egraph.wt -> Node.t -> Node.t -> t -> unit
  val vertex_ty : Egraph.wt -> Node.t -> ty option
end

module Make : functor
  (P : sig
     module Dist : DistSig

     val dl_debug : Colibri2_stdlib.Debug.flag
     val dl_incremental : bool Datastructure.Ref.t
   end)
  -> S with type t = P.Dist.t and type c = P.Dist.C.t

module MakeSup : functor
  (P : sig
     module Dist : DistSig

     val dl_debug : Colibri2_stdlib.Debug.flag
     val dl_incremental : bool Datastructure.Ref.t
   end)
  -> sig
  include S with type t = P.Dist.t and type c = P.Dist.C.t

  val add_constraint : Egraph.wt -> op -> ty -> Node.t -> Node.t -> c -> unit
end
