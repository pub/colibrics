(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Utils

let vertex_count = Debug.register_stats_int "LRA.DL.graph.vertex_count"

module type DistSig = sig
  module C : sig
    type t

    val pp : t Fmt.t
    val equal : t -> t -> bool
    val compare : t -> t -> int

    val hash_fold_t :
      Base_internalhash_types.state -> t -> Base_internalhash_types.state

    val add : t -> t -> t
    val sub : t -> t -> t
    val neg : t -> t
    val succ : t -> t
    val pred : t -> t
    val node : t -> Node.t
  end

  type t

  val pp : t Fmt.t
  val equal : t -> t -> bool
  val compare : t -> t -> int

  val hash_fold_t :
    Base_internalhash_types.state -> t -> Base_internalhash_types.state

  val zero : ty -> t
  val cst : C.t -> ty -> t
  val delta : C.t -> Z.t -> ty -> t
  val mk_pred : C.t -> ty -> t
  val pred : t -> t
  val add : t -> t -> t
  val sub : t -> t -> t
  val add_c : t -> C.t -> t
  val sub_c : t -> C.t -> t
  val neg : t -> t
  val min : t -> t -> t
  val is_zero : t -> bool
  val le : t -> t -> bool
  val lt : t -> t -> bool
  val ty : t -> ty
end

module type S = sig
  type c
  type t

  val pp : 'a Egraph.t -> unit

  val on_repr_change :
    Egraph.wt -> old_repr:Node.t -> repr:Node.t -> delta:c -> unit

  val fold_edges :
    'a Egraph.t ->
    ('a Egraph.t -> Node.t -> Node.t -> t -> 'b -> 'b) ->
    Node.t ->
    'b ->
    'b

  val get_dist : Egraph.wt -> Node.t -> Node.t -> t option
  val get_edge : Egraph.wt -> Node.t -> Node.t -> t option
  val add_edge : Egraph.wt -> Node.t -> Node.t -> t -> unit
  val vertex_ty : Egraph.wt -> Node.t -> ty option
end

module Make (P : sig
  module Dist : DistSig

  val dl_debug : Colibri2_stdlib.Debug.flag
  val dl_incremental : bool Datastructure.Ref.t
end) =
struct
  open P

  type c = Dist.C.t
  type t = Dist.t

  module Heap = Set.Make (struct
    type t = Node.t * Node.t * Dist.t

    let compare (dst1, src1, dist1) (dst2, src2, dist2) =
      let c = Dist.compare dist1 dist2 in
      if c = 0 then
        let c = Node.compare src1 src2 in
        if c = 0 then Node.compare dst1 dst2 else c
      else -c (* to have the biggest improvements on top of the heap *)
  end)

  let succs_db = NHT.create (Node.M.pp Dist.pp) "DG.Successors"
  let precs_db = NHT.create (Node.M.pp Dist.pp) "DG.Predecessors"
  (* TODO: currently the graph is directed, so predecessors aren't really used,
     but it is worth determining if they should be.
     - Make sure that only representatives are present in the graph. *)

  let get_vertex_count env () =
    let cnt = NHT.length succs_db env in
    NHT.fold
      (fun n _ acc -> if NHT.mem precs_db env n then acc else acc + 1)
      precs_db env cnt

  let pp_succs_db env =
    Debug.dprintf0 dl_debug "succs_db:";
    NHT.iter
      ~f:(fun src dstm ->
        Debug.dprintf4 dl_debug "-- {%a -> {%a}}" Node.pp src
          (Node.M.pp Dist.pp) dstm)
      succs_db env
  [@@warning "-32"]

  let pp = pp_succs_db

  let pp_precs_db env =
    Debug.dprintf0 dl_debug "precs_db:";
    NHT.iter
      ~f:(fun dst m ->
        Debug.dprintf4 dl_debug "-- {%a -> {%a}}" Node.pp dst
          (Node.M.pp Dist.pp) m)
      precs_db env
  [@@warning "-32"]

  (** folds over all edges, starting with the ones outgoing from the vertex [v]
      then the ones outgoing from its neighbours and so on. *)
  let fold_edges env f v acc =
    let rec aux seen acc v1 =
      match NHT.find_opt succs_db env v1 with
      | Some neighbours ->
          if Node.S.mem v1 seen then (seen, acc)
          else
            let seen = Node.S.add v1 seen in
            let next, acc =
              Node.M.fold
                (fun v2 edge (next, acc) ->
                  (Node.S.add v2 next, f env v1 v2 edge acc))
                neighbours (Node.S.empty, acc)
            in
            Node.S.fold (fun e (seen, acc) -> aux seen acc e) next (seen, acc)
      | None -> (seen, acc)
    in
    let _, acc = aux Node.S.empty acc v in
    acc

  let bellman_ford =
    let dists = NHT.create Dist.pp "DG.BF.dists" in
    let exception HasNegativeCycle in
    let iter ?(check_ns = false) env source =
      fold_edges env
        (fun env src dst d () ->
          match NHT.find_opt dists env src with
          | None -> ()
          | Some d1 ->
              let nd = Dist.add d d1 in
              NHT.change
                (function
                  | None -> Some nd
                  | Some d2 when Dist.lt nd d2 ->
                      if check_ns then (
                        Debug.dprintf8 dl_debug "[DL] found (%a %a) %a <=%a"
                          Node.pp src Node.pp dst Dist.pp nd Dist.pp d2;
                        raise HasNegativeCycle);
                      Some nd
                  | sd2 -> sd2)
                dists env dst)
        source ()
    in
    fun (env : Egraph.wt) source ->
      Trace.with_span ~__FUNCTION__ ~__FILE__ ~__LINE__ "LRA.DL.BellmanFord"
      @@ fun _ ->
      NHT.set dists env source (Dist.zero Int);
      iter env source;
      (* first iteration *)
      let n = NHT.length dists env in
      (* number of reachable nodes starting from source = number of iterations *)
      for _ = 3 to n do
        (* n-2 iterations *)
        iter env source
      done;
      (* last iteration *)
      try
        iter ~check_ns:true env source;
        let res =
          NHT.fold
            (fun node dist acc -> Node.M.add node dist acc)
            dists env Node.M.empty
        in
        NHT.clear dists env;
        res
      with HasNegativeCycle ->
        (* existence of negative cycles implies unsatisfiablity *)
        NHT.clear dists env;
        Egraph.contradiction ()

  (** [get_dist env v1 v2 = d] => v2 - v1 <= d *)
  let get_dist env v1 v2 =
    let bf = bellman_ford env v1 in
    let r = Node.M.find_opt v2 bf in
    Debug.dprintf8 dl_debug "[DL] bellman_ford: (%a)\n %a -> %a = %a"
      (Node.M.pp Dist.pp) bf Node.pp v1 Node.pp v2 (Fmt.option Dist.pp) r;
    r

  let get_edge env e1 e2 =
    match NHT.find_opt succs_db env e1 with
    | None -> None
    | Some m -> (
        match Node.M.find_opt e2 m with None -> None | Some v -> Some v)

  let add_edge_normal env e1 e2 v =
    assert (not (Egraph.is_equal env e1 e2));
    NHT.change
      (function
        | Some m ->
            Some
              (Node.M.change
                 (function Some v' -> Some (Dist.min v v') | None -> Some v)
                 e2 m)
        | None -> Some (Node.M.singleton e2 v))
      succs_db env e1;
    NHT.change
      (function
        | Some s -> Some (Node.M.add e1 v s)
        | None -> Some (Node.M.singleton e1 v))
      precs_db env e2;
    pp_succs_db env

  let add_edge_incr =
    let dist_to_v0 = NHT.create Dist.pp "DL.dist_to_v0" in
    let dzero = Dist.zero Int in
    let find0 env n =
      match NHT.find_opt dist_to_v0 env n with
      | None ->
          NHT.set dist_to_v0 env n dzero;
          dzero
      | Some d -> d
    in
    let insert_or_improve heap src dst impr =
      match
        Heap.find_first_opt
          (fun (src', dst', _) -> Node.equal src src' && Node.equal dst dst')
          heap
      with
      | Some (_, _, impr') when Dist.lt impr impr' ->
          (Heap.add (src, dst, impr) heap, true)
      | None when Dist.lt impr dzero -> (Heap.add (src, dst, impr) heap, true)
      | None | Some _ -> (heap, false)
    in
    let compute_impr dsrc ddst dist =
      let res = Dist.sub (Dist.add dsrc dist) ddst in
      Debug.dprintf8 dl_debug "compute_impr: (%a + %a) - %a = %a" Dist.pp dsrc
        Dist.pp dist Dist.pp ddst Dist.pp res;
      res
    in
    let rec aux env src heap =
      Debug.dprintf0 dl_debug "[DL] Dists:";
      NHT.iter
        ~f:(fun src dstm ->
          Debug.dprintf4 dl_debug "-- %a -> %a" Node.pp src Dist.pp dstm)
        dist_to_v0 env;
      match Heap.min_elt_opt heap with
      | None -> heap
      | Some ((src', dst', impr') as elt) -> (
          Debug.dprintf6 dl_debug "POP: %a -> %a: %a" Node.pp src' Node.pp dst'
            Dist.pp impr';
          let heap = Heap.remove elt heap in
          let new_dist = Dist.add (find0 env dst') impr' in
          Debug.dprintf2 dl_debug "new_dest: %a" Dist.pp new_dist;
          NHT.set dist_to_v0 env dst' new_dist;
          match NHT.find_opt succs_db env dst' with
          | None -> heap
          | Some m ->
              Node.M.fold
                (fun ndst dist' heap ->
                  let nimpr =
                    compute_impr (find0 env dst') (find0 env ndst) dist'
                  in
                  let heap, _c = insert_or_improve heap dst' ndst nimpr in
                  Debug.dprintf8 dl_debug "ITER: %a -> %a: %a  %b  %b  " Node.pp
                    dst' Node.pp ndst Dist.pp dist' (Dist.lt nimpr dzero)
                    (Node.equal ndst src);
                  if Dist.lt nimpr dzero && Node.equal ndst src then
                    Egraph.contradiction ()
                  else aux env src heap)
                m heap)
    in
    fun env src dst dist ->
      (* assert (not (Egraph.is_equal env src dst)); *)
      Debug.dprintf6 dl_debug
        "[DL][INC] Add the edge %a --(%a)--> %a to the delta graph" Node.pp src
        Dist.pp dist Node.pp dst;
      let old_dist = get_edge env src dst in
      add_edge_normal env src dst dist;
      let dsrc = find0 env src in
      let ddst = find0 env dst in
      let dist =
        (* If the new edge replaces an old edge,
           recover the weight of the old edge *)
        match old_dist with
        | Some old_dist
          when dsrc = Dist.add ddst old_dist && not (Dist.is_zero dsrc) ->
            Dist.sub dist old_dist
        | _ -> dist
      in
      (let impr = compute_impr dsrc ddst dist in
       if Dist.lt impr dzero then
         let heap = Heap.singleton (src, dst, impr) in
         ignore (aux env src heap));
      pp_succs_db env;
      Debug.dprintf0 dl_debug "[DL] Dists:";
      NHT.iter
        ~f:(fun src dstm ->
          Debug.dprintf4 dl_debug "-- %a -> %a" Node.pp src Dist.pp dstm)
        dist_to_v0 env

  let add_edge env src dst dist =
    let is_incr = Datastructure.Ref.get P.dl_incremental env in
    Trace.with_span ~__FUNCTION__ ~__FILE__ ~__LINE__
      (if is_incr then "LRA.DL.Incr.AddEdge" else "LRA.DL.AddEdge")
    @@ fun _ ->
    if Egraph.is_equal env src dst then
      Debug.dprintf10 dl_debug "[DL] Not adding %a --{%a}--> %a because %a = %a"
        Node.pp src Dist.pp dist Node.pp dst Node.pp src Node.pp dst
    else (
      Debug.dprintf7 dl_debug
        "[DL]%s Add the edge %a --(%a)--> %a to the delta graph"
        (if is_incr then "[INCR]" else "")
        Node.pp src Dist.pp dist Node.pp dst;
      if is_incr then add_edge_incr env src dst dist
      else add_edge_normal env src dst dist;
      Debug.max vertex_count (get_vertex_count env)
      (* pp_succs_db env;
         pp_precs_db env *))

  let rm_edge db env n loc =
    NHT.change
      (function
        | None -> assert false
        | Some m ->
            let m = Node.M.remove n m in
            if Node.M.is_empty m then None else Some m)
      db env loc

  let on_repr_change env ~old_repr ~repr ~delta =
    Debug.dprintf6 dl_debug "[DL] on_repr_change: %a = %a + %a " Node.pp
      old_repr Node.pp repr Dist.C.pp delta;
    let precs =
      match NHT.find_opt precs_db env old_repr with
      | None -> Node.M.empty
      | Some pmap ->
          let pmap =
            match Node.M.find_remove repr pmap with
            | pmap, None -> pmap
            | pmap, Some _ ->
                rm_edge succs_db env old_repr repr;
                pmap
          in
          pmap
    in
    let succs =
      match NHT.find_opt succs_db env old_repr with
      | None -> Node.M.empty
      | Some smap ->
          let smap =
            match Node.M.find_remove repr smap with
            | smap, None -> smap
            | smap, Some _ ->
                rm_edge precs_db env old_repr repr;
                smap
          in
          smap
    in
    NHT.remove precs_db env old_repr;
    NHT.remove succs_db env old_repr;
    Node.M.iter
      (fun n d ->
        rm_edge succs_db env old_repr n;
        add_edge env n repr (Dist.sub_c d delta))
      precs;
    Node.M.iter
      (fun n d ->
        rm_edge precs_db env old_repr n;
        add_edge env repr n (Dist.add_c d delta))
      succs;
    Debug.max vertex_count (get_vertex_count env)
  (* pp_succs_db env;
     pp_precs_db env *)

  let vertex_ty env n =
    match NHT.find_opt succs_db env n with
    | None -> None
    | Some m -> (
        try
          let _, d = Node.M.min_binding m in
          Some (Dist.ty d)
        with Not_found -> None)
end

module MakeSup (P : sig
  module Dist : DistSig

  val dl_debug : Colibri2_stdlib.Debug.flag
  val dl_incremental : bool Datastructure.Ref.t
end) =
struct
  open P
  include Make (P)

  module Norm = struct
    type t =
      | Atom of atom1  (** [{x;y;c}]: x - y <= c *)
      | Conj of atom2
          (** [{x';y';c1;c2;orig_c}]: x' - y' <= c1 && y' - x' <= c2 with x' -
              y' = orig_c *)
      | Disj of atom2
          (** [{x';y';c1;c2;orig_c}]: x' - y' <= c1 || y' - x' <= c2 with x' -
              y' <> orig_c *)

    and atom1 = { x : Node.t; y : Node.t; c : Dist.t }

    and atom2 = {
      x' : Node.t;
      y' : Node.t;
      c1 : Dist.t;
      c2 : Dist.t;
      orig_c : Dist.C.t;
    }
    [@@deriving eq, ord, hash]

    let pp fmt = function
      | Atom { x; y; c } ->
          Fmt.pf fmt "%a - %a <= %a" Node.pp x Node.pp y Dist.pp c
      | Conj { x'; y'; c1; c2; orig_c } ->
          Fmt.pf fmt "(%a - %a <= %a) && (%a - %a => %a) with %a - %a = %a"
            Node.pp x' Node.pp y' Dist.pp c1 Node.pp x' Node.pp y' Dist.pp c2
            Node.pp x' Node.pp y' Dist.C.pp orig_c
      | Disj { x'; y'; c1; c2; orig_c } ->
          Fmt.pf fmt "(%a - %a <= %a) || (%a - %a => %a) with %a - %a <> %a"
            Node.pp x' Node.pp y' Dist.pp c1 Node.pp x' Node.pp y' Dist.pp c2
            Node.pp x' Node.pp y' Dist.C.pp orig_c

    let normalized_atom x y c = Atom { x; y; c }
    let normalized_conj x' y' c1 c2 orig_c = Conj { x'; y'; c1; c2; orig_c }
    let normalized_disj x' y' c1 c2 orig_c = Disj { x'; y'; c1; c2; orig_c }

    let normalize _env op ty x y c =
      let res =
        match op with
        | Leq -> normalized_atom x y (Dist.cst c ty)
        | Geq -> normalized_atom y x (Dist.cst (Dist.C.neg c) ty)
        | Lt ->
            (* x - y < c: x - y <= pred(c) *)
            let dist = Dist.mk_pred c ty in
            normalized_atom x y dist
        | Gt ->
            (* x - y > c: y - x <= pred(-c) *)
            let dist = Dist.mk_pred (Dist.C.neg c) ty in
            normalized_atom y x dist
        | Eq ->
            (* x - y = c:
                (x - y <= c) || (x - y >= c)
                (x - y <= c) || (y - x <= -c) *)
            normalized_conj x y (Dist.cst c ty) (Dist.cst (Dist.C.neg c) ty) c
        | Neq ->
            (* x - y <> c:
                (x - y < c) || (x - y > c)
                (x - y <= pred(c)) || (y - x < -c)
                (x - y <= pred(c)) || (y - x <= pred(-c))
            *)
            let c1, c2 =
              match ty with
              | Int ->
                  ( Dist.cst (Dist.C.pred c) Int,
                    Dist.cst (Dist.C.pred (Dist.C.neg c)) Int )
              | _ ->
                  ( Dist.delta c Z.minus_one ty,
                    Dist.delta (Dist.C.neg c) Z.minus_one ty )
            in
            normalized_disj x y c1 c2 c
      in
      Debug.dprintf10 dl_debug "normalize (%a - %a %a %a) to %a" Node.pp x
        Node.pp y pp_op op Dist.C.pp c pp res;
      res

    let neg = function
      | Atom { x; y; c } -> Atom { x = y; y = x; c = Dist.pred (Dist.neg c) }
      | Conj { x'; y'; c1; c2; orig_c } ->
          Disj
            {
              x';
              y';
              c1 = Dist.pred (Dist.neg c2);
              c2 = Dist.pred (Dist.neg c1);
              orig_c;
            }
      | Disj { x'; y'; c1; c2; orig_c } ->
          Conj
            {
              x';
              y';
              c1 = Dist.pred (Dist.neg c2);
              c2 = Dist.pred (Dist.neg c1);
              orig_c;
            }
  end

  let check =
    let check_aux env x y d =
      match get_dist env x y with
      | Some d' ->
          let r = Dist.le d' d in
          Debug.dprintf5 dl_debug "[DL] Norm.check_aux %a with %a res: %b"
            Dist.pp d Dist.pp d' r;
          Some r
      | None ->
          Debug.dprintf2 dl_debug "[DL] Norm.check_aux %a with None res: false"
            Dist.pp d;
          None
    in
    let check env d =
      let open Norm in
      Debug.dprintf2 dl_debug "[DL] Norm.check %a" pp d;
      match d with
      | Atom { x; y; c } -> check_aux env y x c
      | Conj { x'; y'; c1; c2 } -> (
          match (check_aux env y' x' c1, check_aux env x' y' c2) with
          | Some false, _ | _, Some false -> Some false
          | None, _ | _, None -> None
          | Some true, Some true -> Some true)
      | Disj { x'; y'; c1; c2 } -> (
          match (check_aux env y' x' c1, check_aux env x' y' c2) with
          | Some true, _ | _, Some true -> Some true
          | None, _ | _, None -> None
          | Some false, Some false -> Some false)
    in
    fun env d ->
      match check env d with
      | Some b -> Some b
      | None -> (
          match check env (Norm.neg d) with
          | None -> None
          | Some b -> Some (not b))

  let ops_of_type = function
    | Int ->
        (Expr.Term.Const.Int.sub, Expr.Term.Const.Int.lt, Expr.Term.Const.Int.gt)
    | Rat ->
        (Expr.Term.Const.Rat.sub, Expr.Term.Const.Rat.lt, Expr.Term.Const.Rat.gt)
    | Real ->
        ( Expr.Term.Const.Real.sub,
          Expr.Term.Const.Real.lt,
          Expr.Term.Const.Real.gt )

  let add_constraint env op ty x y d =
    let norm = Norm.normalize env op ty x y d in
    match norm with
    | Atom { x; y; c } -> add_edge env y x c
    | Conj { x'; y'; c1; c2; _ } ->
        add_edge env y' x' c1;
        add_edge env x' y' c2
    | Disj { x'; y'; c1; orig_c } ->
        (* since we know that (x' - y' <> orig_c)
           The decision is to be made on whether
           (x' - y' > orig_c) or (x' - y' < orig_c) is true *)
        Choice.register_global env
          Choice.
            {
              print_cho =
                (fun fmt () ->
                  Fmt.pf fmt "Decision from DL.ml: %a - %a <> %a" Node.pp x'
                    Node.pp y' Dist.C.pp orig_c);
              prio = 1;
              key = None;
              choice =
                (fun env ->
                  let sub, lt, gt = ops_of_type (Dist.ty c1) in
                  let cn = Dist.C.node orig_c in
                  let lt =
                    Ground.apply' env lt []
                      [ Ground.apply' env sub [] [ x'; y' ]; cn ]
                  in
                  let gt =
                    Ground.apply' env gt []
                      [ Ground.apply' env sub [] [ x'; y' ]; cn ]
                  in
                  if Egraph.is_registered env lt && Boolean.is_true env lt then
                    DecNo
                  else if Egraph.is_registered env gt && Boolean.is_true env gt
                  then DecNo
                  else
                    DecTodo
                      [
                        (fun env ->
                          if Egraph.is_registered env lt then
                            Boolean.set_true env lt
                          else if Egraph.is_registered env gt then
                            Boolean.set_false env gt
                          else (
                            Egraph.register env lt;
                            Boolean.set_true env lt));
                        (fun env ->
                          if Egraph.is_registered env gt then
                            Boolean.set_true env gt
                          else if Egraph.is_registered env lt then
                            Boolean.set_false env lt
                          else (
                            Egraph.register env gt;
                            Boolean.set_true env gt));
                      ]);
            }

  module Questions = struct
    (* Questions are like decisions, something that another theory can ask the DL
       engine for.
       Essentialy other theories will be able to say to the DL solver I want to
       know if (x <> y), with x and y as nodes (or nodes +- constants) and <> is
       one of (>,>=,<,<=,=,=/=), (x <> y) should probably be normalized when it is
       received.
       The DL engine should somehow be able realize when it learns the answer to
       one of the questions (Ideally in a non-costly way) and be able to somehow
       communicate that information to the theory that asked about it.
       TODO:
       - add deamon that converts questions to decisions before the decision fase
       - Find a way to know to know when a question is answered
         (by checking all changed distances?)
       - Check if questions are answered only once after n changes to the graph?
    *)

    module HT = Datastructure.Hashtbl (struct
      include Norm
      include Colibri2_popop_lib.Popop_stdlib.MkDatatype (Norm)
    end)

    type data =
      | Unanswered of (Egraph.wt -> bool -> unit) list
      (* TODO: Should probably use `Egraph.rt` *)
      | Answered of bool

    let qdb : data HT.t = HT.create Fmt.nop "Questions DB"

    let upd_qdb env q none f =
      HT.change
        (function
          | None -> none ()
          | Some (Unanswered fl) -> Some (Unanswered (f :: fl))
          | Some (Answered b) as v ->
              f env b;
              v)
        qdb env q

    let add_q_aux env op ty n1 n2 d f =
      let q = Norm.normalize env op ty n1 n2 d in
      let nq = Norm.neg q in
      upd_qdb env nq
        (fun () ->
          upd_qdb env q (fun () -> Some (Unanswered [ f ])) f;
          None)
        (fun env b -> f env (not b))

    let is_lt env n1 n2 d ?(ty = Int) f = add_q_aux env Lt ty n1 n2 d f
    [@@warning "-32"]

    let is_le env n1 n2 d ?(ty = Int) f = add_q_aux env Leq ty n1 n2 d f
    [@@warning "-32"]

    let is_eq env n1 n2 d ?(ty = Int) f = add_q_aux env Eq ty n1 n2 d f
    [@@warning "-32"]

    let is_neq env n1 n2 d ?(ty = Int) f = add_q_aux env Neq ty n1 n2 d f
    [@@warning "-32"]

    let check env q =
      match HT.find_opt qdb env q with
      | None -> (
          let nq = Norm.neg q in
          match HT.find_opt qdb env nq with
          | None -> None
          | Some (Answered b) -> Some (true, not b)
          | Some (Unanswered fl) -> (
              match check env nq with
              | None -> None
              | Some b ->
                  List.iter (fun f -> f env (not b)) fl;
                  HT.set qdb env nq (Answered b);
                  Some (false, not b)))
      | Some (Unanswered fl) -> (
          match check env q with
          | None -> None
          | Some b ->
              List.iter (fun f -> f env b) fl;
              HT.set qdb env q (Answered b);
              Some (false, b))
      | Some (Answered b) -> Some (true, b)
    [@@warning "-32"]
  end

  module _ = Questions
end
