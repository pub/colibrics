(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Utils

let dl_debug =
  Debug.register_flag ~desc:"Debugging messages for the difference logic engine"
    "LRADL"

let dl_option =
  Options.register ~pp:Fmt.bool "LRA.DL"
    Cmdliner.Arg.(
      value & flag & info [ "lra-dl" ] ~doc:"Use the difference logic engine")

let dl_incr_option =
  Options.register ~pp:Fmt.bool "LRA.DL.Incremental"
    Cmdliner.Arg.(
      value & flag
      & info [ "lra-incr-dl" ]
          ~doc:
            "Use the incremental version of the difference logic engine. \n\
             Sets \"--lra-dl\" too if it is not set.")

let dl_no_poly =
  Options.register ~pp:Fmt.bool "LRA.DL.no_poly"
    Cmdliner.Arg.(
      value & flag
      & info [ "lra-dl-no-poly" ]
          ~doc:"Don't use polynomes when building the difference logic graph")

let dl_no_rm =
  Options.register ~pp:Fmt.bool "LRA.DL.no_rm"
    Cmdliner.Arg.(
      value & flag
      & info [ "lra-dl-no-rm" ]
          ~doc:
            "When the LRA representative changes [old_repr = repr + delta], \
             add a constraint stating [old_repr - repr = delta] instead of \
             removing [old_repr] and transferring its contraints to [repr].")

let dl_incremental =
  Datastructure.Ref.create Popop_stdlib.DBool.pp "IRDL.Graph.dl_incremental"
    false

module Graph = Graph.MakeSup (struct
  module Dist = Utils.Dist

  let dl_debug = dl_debug
  let dl_incremental = dl_incremental
end)

let add_aux env op ty x y d = Graph.add_constraint env op ty x y d

let rec extract_poly p =
  match Polynome.extract p with
  | Zero -> (A.zero, [])
  | Cst a -> (a, [])
  | Var (a, n, p') ->
      let a', l = extract_poly p' in
      (a', (a, n) :: l)

let get_poly env n =
  match Dom_polynome.get_repr env n with
  | None -> None
  | Some p -> Some (extract_poly p)

(* TODO:
   - support multiplications of variables?
   - add negated variables as separate ones? *)
let upd_delta_graph env op ty x y =
  Debug.dprintf6 dl_debug "upd_delta_graph %a %a %a" Node.pp x pp_op op Node.pp
    y;
  if Options.get env dl_no_poly then
    match (RealValue.get env x, RealValue.get env y) with
    | None, None -> add_aux env op ty x y A.zero
    | Some ax, None -> add_aux env op ty RealValue.zero y (A.neg ax)
    | None, Some ay -> add_aux env op ty x RealValue.zero ay
    | Some _, Some _ -> ()
  else
    let px, py = (get_poly env x, get_poly env y) in
    let print_poly fmt n =
      Fmt.option ~none:(Fmt.any "none") Polynome.pp fmt
        (Dom_polynome.get_repr env n)
    in
    Debug.dprintf4 dl_debug "associated poly: %a %a" print_poly x print_poly y;
    match (px, py) with
    | Some (ax, []), None -> add_aux env op ty RealValue.zero y (A.neg ax)
    | None, Some (ay, []) -> add_aux env op ty x RealValue.zero ay
    | Some (ax, [ (cx, vx) ]), None when A.equal cx A.one ->
        (* ax + vx <op> y :> vx - y <op> -ax *)
        add_aux env op ty vx y (A.neg ax)
    | None, Some (ay, [ (cy, vy) ]) when A.equal cy A.one ->
        (* x <op> ay + vy :> x - vy <op> ay *)
        add_aux env op ty x vy ay
    | Some (ax, [ (cx, vx) ]), Some (ay, [ (cy, vy) ])
      when A.equal cx A.one && A.equal cy A.one ->
        (* ax + vx <op> ay + vy :> vx - vy <op> ay - ax *)
        add_aux env op ty vx vy (A.sub ay ax)
    | Some (ax, [ (cx, vx) ]), Some (ay, [ (cy, vy) ])
      when A.equal cx A.minus_one && A.equal cy A.minus_one ->
        (* ax - vx <op> ay - vy :> vy - vx <op> ay - ax *)
        add_aux env op ty vy vx (A.sub ay ax)
    | Some (ax, [ (cx1, vx1); (cx2, vx2) ]), Some (ay, [])
      when A.equal cx1 A.one && A.equal cx2 A.minus_one ->
        (* ax + vx1 - vx2 <op> ay :> vx1 - vx2 <op> ay - ax *)
        add_aux env op ty vx1 vx2 (A.sub ay ax)
    | Some (ax, [ (cx1, vx1); (cx2, vx2) ]), Some (ay, [])
      when A.equal cx1 A.minus_one && A.equal cx2 A.one ->
        (* ax - vx1 + vx2 <op> ay :> vx2 - vx1 <op> ay - ax *)
        add_aux env op ty vx2 vx1 (A.sub ay ax)
    | Some (ax, []), Some (ay, [ (cy1, vy1); (cy2, vy2) ])
      when A.equal cy1 A.one && A.equal cy2 A.minus_one ->
        (* ax <op> ay + vy1 - vy2 :> vy2 - vy1 <op> ay - ax *)
        add_aux env op ty vy2 vy1 (A.sub ay ax)
    | Some (ax, []), Some (ay, [ (cy1, vy1); (cy2, vy2) ])
      when A.equal cy1 A.minus_one && A.equal cy2 A.one ->
        (* ax <op> ay - vy1 + vy2 :> vy1 - vy2 <op> ay - ax *)
        add_aux env op ty vy1 vy2 (A.sub ay ax)
    | None, None -> add_aux env op ty x y A.zero
    | _ -> add_aux env op ty x y A.zero

let handle_binop env n op ty a b =
  (match Boolean.is env n with
  | Some _ -> ()
  | None ->
      Choice.register_global env
        {
          print_cho =
            (fun fmt () -> Fmt.pf fmt "Decision from DL.ml/handle_binop");
          prio = 1;
          choice =
            (fun env ->
              match Boolean.is env n with
              | Some _ -> DecNo
              | None ->
                  DecTodo
                    [
                      (fun env -> Boolean.set_true env n);
                      (fun env -> Boolean.set_false env n);
                    ]);
          key = Some n;
        });
  Debug.dprintf7 dl_debug "waiting for schedule (upd_delta_graph %a %a %a): %b"
    Node.pp a pp_op op Node.pp b (Boolean.is_true env n);
  DaemonAlsoInterp.attach_value env n Boolean.dom (fun _ _ v ->
      if Boolean.BoolValue.value v then (
        Debug.dprintf6 dl_debug "schedule (upd_delta_graph %a %a %a)" Node.pp a
          pp_op op Node.pp b;
        Debug.dprintf6 dl_debug "Now schedule (upd_delta_graph %a %a %a)"
          Node.pp a pp_op op Node.pp b;
        upd_delta_graph env op ty a b)
      else
        let op = neg_op op in
        Debug.dprintf6 dl_debug "schedule (upd_delta_graph %a %a %a)" Node.pp a
          pp_op op Node.pp b;
        upd_delta_graph env op ty a b)

(* match Boolean.is env n with
   | Some true -> upd_delta_graph env op ty a b
   | Some false -> upd_delta_graph env (neg_op op) ty a b
   | None ->
       Daemon.attach_value env n Boolean.dom (fun env _ bval ->
           if Boolean.BoolValue.value bval then upd_delta_graph env op ty a b
           else upd_delta_graph env (neg_op op) ty a b) *)

let handle_nary_ops env n op ty l =
  (* TODO: decide on the n-ary operation first? *)
  let rec aux = function
    | h :: t ->
        List.iter (handle_binop env n op ty h) t;
        aux t
    | [] -> ()
  in
  aux l

let converter env f =
  let n = Ground.node f in
  match Ground.sem f with
  | {
   app =
     { builtin = (Expr.Leq ty | Expr.Geq ty | Expr.Lt ty | Expr.Gt ty) as op };
   args;
  } ->
      let a, b = IArray.extract2_exn args in
      handle_binop env n (conv_op op) (conv_ty ty) a b
  | { app = { builtin = (Expr.Equal | Expr.Distinct) as op }; tyargs; args }
    when match tyargs with
         | [ { app = { builtin = Expr.Int | Expr.Real | Expr.Rat; _ }; _ } ] ->
             true
         | _ -> false ->
      let ty =
        match tyargs with
        | [ { app = { builtin = Expr.Int; _ }; _ } ] -> Int
        | [ { app = { builtin = Expr.Real; _ }; _ } ] -> Real
        | [ { app = { builtin = Expr.Rat; _ }; _ } ] -> Rat
        | _ -> assert false
      in
      handle_nary_ops env n (conv_op op) ty (IArray.to_list args)
  | _ -> ()

let on_repr_change_add_contraint env ~old_repr ~repr ~delta =
  match Graph.vertex_ty env old_repr with
  | None -> ()
  | Some ty ->
      let x, cx = Dom_polynome.get_proxy env old_repr in
      let y, cy = Dom_polynome.get_proxy env repr in
      let c = A.add (A.sub delta cx) cy in
      Graph.add_constraint env Eq ty x y c

let init env =
  let module On_uninterpreted_domain =
    Colibri2_theories_quantifiers.Uninterp.On_uninterpreted_domain
  in
  if Options.get env dl_incr_option then
    Datastructure.Ref.set dl_incremental env true;
  Interp.Register.check env (fun d t ->
      match Ground.sem t with
      | { app = { builtin = Rat_DL_Delta | Real_DL_Delta; _ }; _ } ->
          Interp.check_of_bool (On_uninterpreted_domain.check d t)
      | _ -> NA);
  Interp.Register.compute env (fun d t ->
      match Ground.sem t with
      | { app = { builtin = Rat_DL_Delta | Real_DL_Delta; _ }; _ } ->
          On_uninterpreted_domain.compute d t
      | _ -> NA);
  Ground.register_converter env converter;
  if Options.get env dl_no_rm then
    Dom_polynome.attach_any_repr_change env on_repr_change_add_contraint
  else Dom_polynome.attach_any_repr_change env Graph.on_repr_change
