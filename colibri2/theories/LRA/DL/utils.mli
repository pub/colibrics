(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

val dl_debug : Debug.flag

type 'a Builtin.t += Rat_DL_Delta | Real_DL_Delta

val dl_delta_rat : Expr.ty Expr.id
val dl_delta_real : Expr.ty Expr.id

type ty = Int | Rat | Real

val hash_fold_ty :
  Base_internalhash_types.state -> ty -> Base_internalhash_types.state

val hash_ty : ty -> int

type op = Lt | Leq | Gt | Geq | Eq | Neq

val hash_fold_op :
  Base_internalhash_types.state -> op -> Base_internalhash_types.state

val hash_op : op -> int
val pp_op : Format.formatter -> op -> unit
val neg_op : op -> op
val inv_op : op -> op
val conv_op : < .. > Builtin.t -> op
val conv_ty : [< `Int | `Rat | `Real ] -> ty

module Dist : sig
  module C : sig
    include module type of A

    val succ : t -> t
    val pred : t -> t
    val node : t -> Node.t
  end

  type t =
    | Cst of { c : C.t; ty : ty }
    | Delta of { c' : C.t; k : Z.t; ty' : ty }

  val pp : t Fmt.t
  val equal : t -> t -> bool
  val compare : t -> t -> int

  val hash_fold_t :
    Base_internalhash_types.state -> t -> Base_internalhash_types.state

  val hash : t -> int
  val cst : C.t -> ty -> t
  val delta : C.t -> Z.t -> ty -> t
  val succ : t -> t
  val pred : t -> t
  val mk_pred : C.t -> ty -> t
  val add_c : t -> C.t -> t
  val sub_c : t -> C.t -> t
  val add : t -> t -> t
  val sub : t -> t -> t
  val neg : t -> t
  val delta_le : A.t -> Z.t -> A.t -> Z.t -> bool
  val delta_lt : A.t -> Z.t -> A.t -> Z.t -> bool
  val zero : ty -> t
  val is_zero : t -> bool
  val le : t -> t -> bool
  val lt : t -> t -> bool
  val min : t -> t -> t
  val node : 'a Egraph.t -> t -> Node.t
  val ty : t -> ty
end

module NHT : Datastructure.Sig with type key := Node.t
