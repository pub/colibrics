module Var = struct
  include Node

  let is_int _ = false
  let print = pp
end

(* Simplex explanations *)
module Ex = struct
  type t = unit

  let print fmt () = Fmt.pf fmt "()"
  let empty = ()
  let union () () = ()
end

module Rat = struct
  include A

  let m_one = A.minus_one
  let print = A.pp
  let is_int = A.is_integer
  let is_zero v = A.equal v A.zero
  let is_one v = A.equal v A.one
  let mult = A.mul
  let minus = A.neg
  let is_m_one v = A.equal v m_one
  let ceiling = ceil
end

module S = OcplibSimplex.Basic.Make (Var) (Rat) (Ex)

(** Convert boundary to a simplex bound, calling [round q] to the value [q]
    associated with the boundary. The value must be a real. *)
let to_simplex_bound round = function
  | Boundary.No -> None
  | Boundary.Large q ->
      assert (A.is_real q);
      Some S.Core.{ bvalue = R2.of_r q; explanation = () }
  | Boundary.Strict q ->
      assert (A.is_real q);
      Some S.Core.{ bvalue = round q; explanation = () }

(** Convert inferior boundary to simplex bound *)
let of_bound_inf = to_simplex_bound S.Core.R2.lower

(** Convert superior boundary to simplex bound *)
let of_bound_sup = to_simplex_bound S.Core.R2.upper

(** Convert a pair of inferior/superior boundaries to simplex bounds *)
let to_simplex_bounds (inf, sup) = (of_bound_inf inf, of_bound_sup sup)

(** Simplex polynom from [Polynome.t] value *)
let of_poly (p : A.t Node.M.t) =
  Node.M.fold
    (fun n r acc -> fst (S.Core.P.accumulate n r acc))
    p S.Core.P.empty

let of_bound_sup = function
  | Boundary.No -> None
  | Large q ->
      assert (A.is_real q);
      Some S.Core.{ bvalue = R2.of_r q; explanation = () }
  | Strict q ->
      assert (A.is_real q);
      Some S.Core.{ bvalue = R2.upper q; explanation = () }

let of_bound_inf = function
  | Boundary.No -> None
  | Large q ->
      assert (A.is_real q);
      Some S.Core.{ bvalue = R2.of_r q; explanation = () }
  | Strict q ->
      assert (A.is_real q);
      Some S.Core.{ bvalue = R2.lower q; explanation = () }

include S
