(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

(** Represents product of sign of nodes *)

type t

include Colibri2_popop_lib.Popop_stdlib.Datatype with type t := t

type cst = Pos | Neg

val cst : A.t -> t

val of_one_node : Node.t -> t
(** Build a value that represent one node *)

val of_one_node_non_zero : Node.t -> t
(** Build a value that represent one node when it is known to be non-zero *)

val abs_of_one_node : Node.t -> t
(** Build a value that represent the absolute value of one node *)

val is_one_node : t -> Node.t option
(** Test if a value represents one node *)

exception Div_zero

val subst_exn : t -> Node.t -> t -> t option
(** [subst p n q] substitute [n] by [q] in [p], if [n] is not in [p] None is
    returned, otherwise the result of the substitution is returned. raise
    {!Div_zero} if a non-zero node is subtituted by zero *)

val normalize : t -> f:(Node.t -> t) -> t
(** [norm p ~f] normalize [p] using [f] *)

type data
(** An abstract type to avoid translating the map to sets in {!nodes} *)

val nodes : t -> data Node.M.t
(** [nodes t] returns the node which are present in [t] *)

val mul : t -> t -> t

type extracted = Plus_one | Minus_one | Zero | Var of Node.t * t | NoNonZero

val extract : t -> extracted

type extract_cst = Pos | Neg | Zero

val extract_cst : t -> extract_cst * t
val remove : Node.t -> t -> t

val common : t -> t -> t
(** Return the common part *)

val one : t
val minus_one : t
val zero : t
val mul_cst : cst -> A.t -> A.t

type kind = PLUS_ONE | MINUS_ONE | ZERO | NODE of (cst * Node.t) | PRODUCT

val pp_kind : Format.formatter -> kind -> unit
val classify : t -> kind

type presolve =
  | Zero  (** equal to zero *)
  | NonZero  (** a product not equal to zero *)
  | OnePossibleZero of Node.t
      (** a product with only one term is possibly zero and it has an even power
      *)
  | Other  (** not one of the previous case *)

val presolve : t -> presolve
