(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

let comparisons = Datastructure.Push.create Ground.pp "LRA.fourier.comparisons"

let scheduled =
  Datastructure.Ref.create Base.Bool.pp "LRA.fourier.scheduled" false

let debug =
  Debug.register_info_flag ~desc:"Reasoning about <= < in LRA" "fourier"

let stats_run = Debug.register_stats_int "Fourier.run"
let stats_time = Debug.register_stats_time "Fourier.time"

type eq = { p : Polynome.t; bound : Bound.t; origins : Ground.S.t }
[@@deriving show]
(** p <= 0 or p < 0 *)

let _ = pp_eq

let divide d (p : Polynome.t) =
  try
    (match Polynome.is_cst p with None -> () | Some _ -> raise Exit);
    if A.sign p.cst <> 0 then raise Exit;
    let l = Node.M.bindings p.poly in
    let l =
      List.fold_left
        (fun acc (e, q) ->
          match
            ( Dom_product.SolveAbs.get_repr d e,
              Dom_product.SolveSign.get_repr d e )
          with
          | None, _ when Egraph.is_equal d RealValue.zero e -> acc
          | None, _ | _, None -> raise Exit
          | Some abs, Some sign -> (abs, sign, q) :: acc)
        [] l
    in
    (* Debug.dprintf4 debug "@[eq:%a@ %a@]" Polynome.pp p
     *   Fmt.(list ~sep:(any "+") (using CCPair.swap (pair A.pp Product.pp)))
     *   l; *)
    match l with
    | [] -> raise Exit
    | (hd, __, _) :: _ ->
        let common =
          List.fold_left
            (fun acc (abs, _, _) ->
              Node.M.inter
                (fun _ a b -> if Q.equal a b then Some a else None)
                acc abs.Product.poly)
            hd.Product.poly l
        in
        let common =
          Node.M.fold_left
            (fun acc abs v ->
              if Dom_interval.is_not_zero d abs then Node.M.add abs v acc
              else acc)
            Node.M.empty common
        in
        if Node.M.is_empty common then raise Exit;
        let common = Product.of_map A.one common in
        Debug.dprintf4 debug "[Fourier] found possible division: %a / |%a|"
          Polynome.pp p Product.pp common;
        let cst, l =
          List.fold_left
            (fun (cst, acc) (abs, sign, v) ->
              (* v * abs * sign *)
              let abs = Product.div abs common in
              let pos, sign = Sign_product.extract_cst sign in
              let v =
                match pos with Zero -> A.zero | Pos -> v | Neg -> A.neg v
              in
              match (Product.classify abs, Sign_product.classify sign) with
              | _, MINUS_ONE -> assert false
              | CST cst', PLUS_ONE -> (A.add (A.mul cst' v) cst, acc)
              | NODE (cst', n), NODE (cst'', n') when Egraph.is_equal d n n' ->
                  (cst, (n, A.mul (Sign_product.mul_cst cst'' cst') v) :: acc)
              | _ ->
                  let n = Dom_product.node_of_product d abs sign in
                  (cst, (n, v) :: acc))
            (A.zero, []) l
        in
        Some (Polynome.of_list cst l, common)
  with Exit -> None

let delta d eq =
  (* add info to delta *)
  if Node.M.is_num_elt 2 eq.p.poly then
    match Node.M.bindings eq.p.poly with
    | [ (src, a); (dst, b) ] when A.equal A.one a && A.equal A.minus_one b ->
        Delta.add d src (A.neg eq.p.cst) eq.bound dst
    | [ (dst, b); (src, a) ] when A.equal A.one a && A.equal A.minus_one b ->
        Delta.add d src (A.neg eq.p.cst) eq.bound dst
    | _ -> ()

let apply d ~f truth g =
  let aux d bound a b =
    let bound =
      if truth then bound
      else match bound with Bound.Large -> Strict | Strict -> Large
    in
    let a, b = if truth then (a, b) else (b, a) in
    f d bound a b
  in
  match Ground.sem g with
  | { app = { builtin = Expr.Lt _ }; tyargs = []; args; _ } ->
      let a, b = IArray.extract2_exn args in
      aux d Bound.Strict a b
  | { app = { builtin = Expr.Leq _ }; tyargs = []; args; _ } ->
      let a, b = IArray.extract2_exn args in
      aux d Large a b
  | { app = { builtin = Expr.Geq _ }; tyargs = []; args; _ } ->
      let a, b = IArray.extract2_exn args in
      aux d Large b a
  | { app = { builtin = Expr.Gt _ }; tyargs = []; args; _ } ->
      let a, b = IArray.extract2_exn args in
      aux d Strict b a
  | _ -> assert false

let add_eq d eqs p bound origins =
  match (Polynome.is_cst p, bound) with
  | None, _ ->
      let eq = { p; bound; origins } in
      delta d eq;
      eq :: eqs
  | Some p, Bound.Large when A.sign p = 0 ->
      Ground.S.iter
        (fun g ->
          let truth = Base.Option.value_exn (Boolean.is d (Ground.node g)) in
          apply d truth g ~f:(fun d bound a b ->
              let p =
                match bound with
                | Strict
                  when Dom_interval.is_integer d a
                       && Dom_interval.is_integer d b ->
                    Polynome.of_list A.minus_one [ (b, A.one) ]
                | Large -> Polynome.monome A.one b
                | Strict -> assert false
              in
              Debug.dprintf4 debug "[LRA/Fourier] Found %a = %a" Node.pp a
                Polynome.pp p;
              Dom_polynome.assume_equality d a p))
        origins;
      eqs
  | Some p, _ when A.sign p < 0 ->
      Debug.dprintf2 debug "[Fourier] discard %a" Ground.S.pp origins;
      eqs
  | Some p, bound ->
      Debug.dprintf6 Colibri2_core.Debug.contradiction
        "[LRA/Fourier] Found %a%a0 by %a" A.pp p Bound.pp bound Ground.S.pp
        origins;
      Egraph.contradiction ()

let mk_eq d bound a b =
  let ( ! ) n =
    match Dom_polynome.get_repr d n with
    | None -> Polynome.monome A.one (Egraph.find_def d n)
    | Some p -> p
  in
  let p, bound' =
    match bound with
    | Bound.Strict
      when Dom_interval.is_integer d a && Dom_interval.is_integer d b ->
        (Polynome.add_cst (Polynome.sub !a !b) A.one, Bound.Large)
    | _ -> (Polynome.sub !a !b, bound)
  in
  ( p,
    bound',
    Polynome.sub (Polynome.monome A.one a) (Polynome.monome A.one b),
    bound )

let make_equations d (eqs, vars) g =
  Debug.dprintf2 debug "[Fourier] %a" Ground.pp g;
  match Boolean.is d (Ground.node g) with
  | None -> (eqs, vars)
  | Some truth -> (
      let p, bound, p_non_norm, bound_non_norm = apply d ~f:mk_eq truth g in
      Debug.dprintf6 debug "[Fourier] %a(%a)%a0" Polynome.pp p Polynome.pp
        p_non_norm Bound.pp bound;
      let eqs, vars =
        ( add_eq d eqs p bound (Ground.S.singleton g),
          Node.M.union_merge (fun _ _ _ -> Some ()) vars p.Polynome.poly )
      in
      match divide d p_non_norm with
      | Some (p', _) ->
          let p' = Dom_polynome.normalize d p' in
          ( add_eq d eqs p' bound_non_norm Ground.S.empty,
            Node.M.union_merge (fun _ _ _ -> Some ()) vars p'.Polynome.poly )
      | None -> (eqs, vars))

let fm (d : Egraph.wt) =
  Debug.dprintf0 debug "[Fourier]";
  Debug.incr stats_run;
  Datastructure.Ref.set scheduled d false;
  let eqs, vars =
    Datastructure.Push.fold comparisons d ~f:(make_equations d)
      ~init:([], Node.S.empty)
  in
  let rec split n positive negative absent = function
    | [] -> (positive, negative, absent)
    | eq :: l -> (
        match Node.M.find_opt n eq.p.poly with
        | None -> split n positive negative (eq :: absent) l
        | Some q ->
            if A.sign q > 0 then split n ((q, eq) :: positive) negative absent l
            else split n positive ((q, eq) :: negative) absent l)
  in
  let rec aux eqs vars =
    match Node.S.choose vars with
    | exception Not_found -> ()
    | n -> (
        let vars = Node.S.remove n vars in
        let positive, negative, absent = split n [] [] [] eqs in
        match (positive, negative) with
        | [], _ | _, [] -> aux absent vars
        | _, _ ->
            let eqs =
              Lists.fold_product
                (fun eqs (pq, peq) (nq, neq) ->
                  let q = A.div pq (A.neg nq) in
                  let p = Polynome.cx_p_cy A.one peq.p q neq.p in
                  let bound =
                    match (peq.bound, neq.bound) with
                    | Large, Large -> Bound.Large
                    | _ -> Bound.Strict
                  in
                  let s = Ground.S.union peq.origins neq.origins in
                  add_eq d eqs p bound s)
                absent positive negative
            in
            (* let eqs =
             *   List.rev_map
             *     (fun { p; bound; origins = _ } ->
             *       { p = Polynome.normalize p; bound; origins = Ground.S.empty })
             *     eqs
             * in
             * let eqs =
             *   List.sort_uniq
             *     (fun eqs1 eqs2 -> Polynome.compare eqs1.p eqs2.p)
             *     eqs
             * in *)
            if Colibri2_stdlib.Debug.test_flag debug then
              Debug.dprintf2 debug "vars: %i, eqs: %i" (Node.S.cardinal vars)
                (List.length eqs);
            aux eqs vars)
  in
  aux eqs vars

let fm d = Debug.add_time_during stats_time (fun _ -> fm d)

module Daemon = struct
  let key =
    Events.Dem.create
      (module struct
        type t = unit

        let name = "LRA.fourier"
      end)

  let enqueue_aux d =
    if Datastructure.Ref.get scheduled d then (
      Debug.dprintf0 debug "[Scheduler] Fourier? Already";
      false)
    else (
      Debug.dprintf0 debug "[Scheduler] Fourier? Yes";
      Datastructure.Ref.set scheduled d true;
      true)

  let enqueue d _ =
    if enqueue_aux d then Events.EnqRun (key, (), None) else Events.EnqAlready

  let enqueue' d = if enqueue_aux d then Events.new_pending_daemon d key ()
  let delay = Events.Delayed_by 64

  type runable = unit

  let print_runable = Unit.pp
  let run d () = fm d
end

let () = Events.register (module Daemon)

(** {2 Initialization} *)
let converter d (f : Ground.t) =
  match Ground.sem f with
  | {
   app = { builtin = Expr.Lt _ | Expr.Leq _ | Expr.Geq _ | Expr.Gt _ };
   tyargs = [];
   args;
  } ->
      let attach n =
        Dom_polynome.attach_repr_change d ~node:n Daemon.enqueue';
        Events.attach_repr d n Daemon.enqueue
      in
      IArray.iter ~f:attach args;
      Events.attach_value d (Ground.node f) Boolean.dom (fun d n _ ->
          Daemon.enqueue d n);
      Datastructure.Push.push comparisons d f;
      Events.new_pending_daemon d Daemon.key ();
      if Boolean.dec_at_literal d then
        Choice.register d f (Boolean.chobool (Ground.node f))
  | _ -> ()

let init env = Ground.register_converter env converter
