type t = Boundary.t * Boundary.t
(** A couple of inferior and superior boundaries *)

(** Swap inferior and superior bounds if the condition is true *)
let swap_if test ((inf, sup) : t) = if test then (sup, inf) else (inf, sup)

(** Apply transform function to both bounds *)
let transform f ((inf, sup) : t) = (Boundary.map f inf, Boundary.map f sup)

(** Translate a pair of bounds by some distance *)
let translate (distance : A.t) (bounds : t) =
  bounds |> transform (A.add distance)

(** Scale a pair of bounds by a factor (and swap them if the factor is negative)
*)
let scale (factor : A.t) (bounds : t) =
  bounds |> swap_if (A.sign factor < 0) |> transform (A.mul factor)

(** Compute the bounds of a node *)
let node_bounds egraph node =
  match Dom_interval.get_dom egraph node with
  | None -> (Boundary.No, Boundary.No)
  | Some dom -> Boundary.domain_bounds dom
