(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

(** Distance Graph *)

val add_le : Egraph.wt -> Node.t -> A.t -> Node.t -> unit
(* [add_le d n1 q n2] n1 - n2 <= q *)

val add_lt : Egraph.wt -> Node.t -> A.t -> Node.t -> unit
(* [add_lt d n1 q n2] n1 - n2 < q *)

val add_ge : Egraph.wt -> Node.t -> A.t -> Node.t -> unit
(* [add_ge d n1 q n2] n1 - n2 >= q *)

val add_gt : Egraph.wt -> Node.t -> A.t -> Node.t -> unit
(* [add_gt d n1 q n2] n1 - n2 > q *)

val add :
  Egraph.wt ->
  Node.t ->
  A.t ->
  Colibri2_theories_LRA_stages_def.Bound.t ->
  Node.t ->
  unit
(* [add d n1 q bound n2] n1 - n2 <= q or < q *)

val th_register : Egraph.wt -> unit
