module Node = Colibri2_core.Node

module T = struct
  type t = { from : Node.t; to_ : Node.t } [@@deriving show, eq, ord, hash]
end

include T
include Popop_stdlib.MkDatatype (T)
