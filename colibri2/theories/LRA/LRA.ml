(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib
module Const = Colibri2_core.Expr.Term.Const
module RealValue = RealValue

module type IneqS = sig
  val le : Const.t
  val lt : Const.t
  val ge : Const.t
  val gt : Const.t
end

let fourier_option =
  Options.register ~pp:Fmt.bool "LRA.fourier"
    Cmdliner.Arg.(
      value & flag
      & info [ "lra-fourier" ]
          ~doc:"Use FourierMotskin engine instead of Simplex engine")

let pivot_intrv_upd =
  Options.register ~pp:Fmt.bool "LRA.pivot_intrv_upd"
    Cmdliner.Arg.(
      value & flag
      & info [ "lra-pivot-intr-upd" ]
          ~doc:
            "Use pivot to update intervals for all elements of pivot class \
             whenever the interval domain of one element changes")

let normalize_relops_to_le =
  Options.register ~pp:Fmt.bool "LRA.lra-relop-norm"
    Cmdliner.Arg.(
      value & flag
      & info [ "lra-relop-norm-to-le" ]
          ~doc:"Normalize all relational operators (<, ≤, >, ≥) to ≤")

let upd_idom_pivot_class env r ri m =
  Debug.dprintf6 Pivot.debug "upd_idom_pivot_class %a %a {%a}" Node.pp r
    Dom_interval.D.pp ri (A.M.pp Node.pp) m;
  A.M.iter
    (fun rd nr ->
      (* r = nr + rd -> i(nr) = i(r) - i(rd) *)
      let nri = Dom_interval.D.add_cst (A.neg rd) ri in
      Dom_interval.upd_dom env nr nri)
    m

let normalize_lt_gt_relops_to_le () =
  let reg_cst_norm ty (module IneqMod : IneqS) =
    Ground.register_cst_normalization IneqMod.lt ty
      (fun tyargs args rty : Ground.s ->
        (* x < y -> not (x >= y) -> not (y <= x) *)
        Ground.
          {
            app = Const.neg;
            args =
              IArray.of_list
                [
                  Ground.
                    {
                      app = IneqMod.le;
                      tyargs;
                      args = IArray.to_list args |> List.rev |> IArray.of_list;
                      ty = rty;
                    }
                  |> Ground.index |> Ground.node;
                ];
            tyargs = [];
            ty = Ty.bool;
          });
    Ground.register_cst_normalization IneqMod.ge ty
      (fun tyargs args rty : Ground.s ->
        Ground.
          {
            app = IneqMod.le;
            tyargs;
            args = IArray.to_list args |> List.rev |> IArray.of_list;
            ty = rty;
          });
    Ground.register_cst_normalization IneqMod.gt ty
      (fun tyargs args rty : Ground.s ->
        (* x > y -> not (x <= y) -> not (x <= y) *)
        Ground.
          {
            app = Const.neg;
            args =
              IArray.of_list
                [
                  Ground.{ app = IneqMod.le; tyargs; args; ty = rty }
                  |> Ground.index |> Ground.node;
                ];
            tyargs = [];
            ty = Ty.bool;
          })
  in
  reg_cst_norm Ground.Ty.int (module Const.Int);
  reg_cst_norm Ground.Ty.rat (module Const.Rat);
  reg_cst_norm Ground.Ty.real (module Const.Real)

(* Default normalization: simlpy changes ge to le and gt to le *)
let normalize_relops_to_lt_and_le () =
  let reg_cst_norm ty (module IneqMod : IneqS) =
    Ground.register_cst_normalization IneqMod.ge ty
      (fun tyargs args rty : Ground.s ->
        Ground.
          {
            app = IneqMod.le;
            tyargs;
            args = IArray.to_list args |> List.rev |> IArray.of_list;
            ty = rty;
          });
    Ground.register_cst_normalization IneqMod.gt ty
      (fun tyargs args rty : Ground.s ->
        (* x > y -> y < x *)
        Ground.
          {
            app = IneqMod.lt;
            tyargs;
            args = IArray.to_list args |> List.rev |> IArray.of_list;
            ty = rty;
          })
  in
  reg_cst_norm Ground.Ty.int (module Const.Int);
  reg_cst_norm Ground.Ty.rat (module Const.Rat);
  reg_cst_norm Ground.Ty.real (module Const.Real)

let th_register env =
  Converter.init env;
  Dom_interval.init env;
  Dom_polynome.init env;
  RealValue.init env;
  if Options.get env fourier_option then Fourier.init env else Simplex.init env;
  Dom_product.init env;
  if Options.get env DL.IRDL.dl_option || Options.get env DL.IRDL.dl_incr_option
  then DL.IRDL.init env;
  if Options.get env normalize_relops_to_le then normalize_lt_gt_relops_to_le ()
  else normalize_relops_to_lt_and_le ();
  if Options.get env pivot_intrv_upd then (
    Dom_interval.daemon_attach_any_dom env (fun env n ->
        match Dom_interval.get_dom env n with
        | None -> assert false
        | Some ni ->
            let r, d = Dom_polynome.get_proxy env n in
            let ri = Dom_interval.D.add_cst (A.neg d) ni in
            let m = A.M.remove (A.neg d) (Dom_polynome.get_deltas env r) in
            upd_idom_pivot_class env r ri m);
    Dom_polynome.attach_any_repr_change env (fun env ~old_repr ~repr ~delta ->
        (* old_repr = repr + delta *)
        let rm = Dom_polynome.get_deltas env repr in
        let orm = Dom_polynome.get_deltas env old_repr in
        DaemonAlsoInterp.schedule_immediately env (fun env ->
            match
              (Dom_interval.get_dom env old_repr, Dom_interval.get_dom env repr)
            with
            | None, None -> ()
            | Some ori, None ->
                let ri = Dom_interval.D.add_cst (A.neg delta) ori in
                upd_idom_pivot_class env repr ri rm
            | None, Some ri ->
                let ori = Dom_interval.D.add_cst delta ri in
                upd_idom_pivot_class env old_repr ori orm
            | Some ori, Some ri ->
                let new_ori =
                  match
                    Dom_interval.D.inter ori (Dom_interval.D.add_cst delta ri)
                  with
                  | None -> Egraph.contradiction ()
                  | Some i -> i
                in
                let new_ri = Dom_interval.D.add_cst (A.neg delta) new_ori in
                upd_idom_pivot_class env old_repr new_ori orm;
                upd_idom_pivot_class env repr new_ri rm)))

let () = Init.add_default_theory th_register

include LRA_build.LastEffort
