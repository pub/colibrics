(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

module TotalWithCst (P : sig
  module Base : sig
    include Colibri2_popop_lib.Popop_stdlib.Datatype

    val is_empty : t -> bool
  end

  module Cst : sig
    include Colibri2_popop_lib.Popop_stdlib.Datatype

    val zero : t
    val add : t -> t -> t
    val sub : t -> t -> t
  end

  type t

  val dt : t -> Base.t * Cst.t
  (** Normalizable terms *)

  val combine : t -> Cst.t -> t
  val name : string

  include Colibri2_popop_lib.Popop_stdlib.Datatype with type t := t

  val of_one_node : Node.t -> t
  (** Build a value that represent one node *)

  val subst : t -> Node.t -> t -> t
  (** [subst p n q] return the result of the substitution of [n] by [q] in [p]
  *)

  val normalize : t -> f:(Node.t -> t) -> t
  (** [norm p ~f] normalize [p] using [f] *)

  type data
  (** An abstract type to avoid translating the map to sets in {!nodes} *)

  val nodes : t -> data Node.M.t
  (** [nodes t] returns the node which are present in [t] *)

  val solve : t -> t -> t Pivot.solve_total
  (** [solve t1 t2] solve the equation [t1 = t2] by returning a substitution. *)

  val new_base : Egraph.wt -> Node.t -> t -> unit
  (** Called when a new base appear *)

  val empty_base : Egraph.wt -> Node.t -> Cst.t -> unit
end) : sig
  val assume_equality : Egraph.wt -> Node.t -> P.t -> unit
  (** [assume_equality d n p] assumes the equality [n = p] *)

  val get_deltas : _ Egraph.t -> Node.t -> Node.t P.Cst.M.t
  val get_repr : _ Egraph.t -> Node.t -> P.t option

  val get_proxy : _ Egraph.t -> Node.t -> Node.t * P.Cst.t
  (** [get_proxy d n] returns (m,c) such that n = m + c . m can be seen as a
      representative *)

  val get_node : Egraph.wt -> P.t -> Node.t option
  val normalize : _ Egraph.t -> P.t -> P.t

  val attach_repr_change :
    _ Egraph.t -> node:Node.t -> (Egraph.wt -> unit) -> unit

  val attach_any_repr_change :
    _ Egraph.t ->
    (Egraph.wt -> old_repr:Node.t -> repr:Node.t -> delta:P.Cst.t -> unit) ->
    unit
  (* old_repr = repr + delta *)

  val init : Egraph.wt -> unit
end
