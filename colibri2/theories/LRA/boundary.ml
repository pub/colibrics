(** An open, large or strict boundary *)
type t = No | Large of A.t | Strict of A.t [@@deriving show]

(** Apply [f] to the value associated with the bound, keeping the same variant
    constructor *)
let map f = function
  | No -> No
  | Strict c -> Strict (f c)
  | Large c -> Large (f c)

(** Pretty-printer for a bound as an inferior bound *)
let pp_bound_inf fmt = function
  | No -> ()
  | Large q -> Fmt.pf fmt "%a ≤" A.pp q
  | Strict q -> Fmt.pf fmt "%a <" A.pp q

(** Pretty-printer for a bound as a superior bound *)
let pp_bound_sup fmt = function
  | No -> ()
  | Large q -> Fmt.pf fmt "≤ %a" A.pp q
  | Strict q -> Fmt.pf fmt "< %a" A.pp q

(** Intersect two bounds [b1] and [b2] according to op [cmp]. In the general
    case the bounds are respectively associated with [q1] and [q2]: when
    [cmp q1 q2] is true, the boundary representing the intersection is [b1];
    otherwise it is [b2] *)
let intersect cmp b1 b2 =
  match (b1, b2) with
  | No, b | b, No -> b
  | (Large q1, (Strict q2 as b) | (Strict q2 as b), Large q1) when A.equal q1 q2
    ->
      b
  | ((Large q1 | Strict q1) as b1), ((Strict q2 | Large q2) as b2) ->
      if cmp q1 q2 then b1 else b2

(** Intersect inferior bounds by keeping the greater boundary *)
let inter_inf = intersect A.ge

(** Intersect superior bounds by keeping the smaller boundary *)
let inter_sup = intersect A.le

(** Compute the bounds of a domain *)
let domain_bounds domain =
  let to_bound = function
    | None -> No
    | Some (q, Bound.Large) -> Large q
    | Some (q, Bound.Strict) -> Strict q
  and inf, sup = Dom_interval.D.get_convexe_hull domain in
  (to_bound inf, to_bound sup)

(** Compute the domain from the bounds *)
let bounds_domain (inf, sup) =
  let to_bound = function
    | No -> None
    | Large q -> Some (q, Bound.Large)
    | Strict q -> Some (q, Bound.Strict)
  in
  Dom_interval.D.from_convexe_hull (to_bound inf, to_bound sup)
