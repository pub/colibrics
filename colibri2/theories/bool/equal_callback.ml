open Base
open Colibri2_popop_lib
open Popop_stdlib
open Colibri2_core

type callback = Egraph.rt -> Node.t -> Node.t -> unit

module Callback = struct
  type t = { f : callback; id : int }

  let equal x y = Int.equal x.id y.id
  let hash_fold_t env x = Int.hash_fold_t env x.id
  let pp fmt x = Fmt.int fmt x.id
end

module D = struct
  type t = Callback.t DInt.M.t [@@deriving eq, hash, show]

  let merged (b1 : t option) (b2 : t option) =
    match (b1, b2) with
    | Some b1, Some b2 -> equal b1 b2
    | None, None -> true
    | _ -> false

  let wakeup_threshold = None

  let key =
    Dom.Kind.create
      (module struct
        type nonrec t = t

        let name = "equal_callback"
      end)

  let union d s1 s2 cl1 cl2 =
    DInt.M.union
      (fun _ (f1 : Callback.t) (f2 : Callback.t) ->
        assert (Base.phys_equal f1.f f2.f);
        f1.f (Egraph.ro d) cl1 cl2;
        Some f1)
      s1 s2

  let merge d (s1, cl1) (s2, cl2) _ =
    match (s1, s2) with
    | None, None -> raise Impossible
    | Some s, None -> Egraph.set_dom d key cl2 s
    | None, Some s -> Egraph.set_dom d key cl1 s
    | Some s1, Some s2 ->
        let s = union d s1 s2 cl1 cl2 in
        Egraph.set_dom d key cl1 s;
        Egraph.set_dom d key cl2 s
end

let () = Dom.register (module D)
let counter = Datastructure.Ref.create Fmt.int "Equality_callback.counter" (-1)

let add_dom d n id f =
  let s = DInt.M.singleton id f in
  match Egraph.get_dom d D.key n with
  | None -> Egraph.set_dom d D.key n s
  | Some m ->
      (* Use union for the case where the nodes are already equal *)
      Egraph.set_dom d D.key n (D.union d m s n n)

let add_n_callback d f l =
  Datastructure.Ref.incr counter d;
  let c = Datastructure.Ref.get counter d in
  List.iteri l ~f:(fun id n -> add_dom d n c { f; id })

let add_callback d f n1 n2 = add_n_callback d f [ n1; n2 ]
