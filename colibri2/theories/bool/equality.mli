(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

(** {1 Disequalities} *)

(** Handling of disequalities can be viewed by associating a tag function (node
    -> int) for each disequality. This function is not a true constraint,
    because the function is associated even if the disequality is not true. *)

val equality : _ Egraph.t -> Node.t list -> Node.t
val disequality : _ Egraph.t -> Node.t list -> Node.t
val is_equal : _ Egraph.t -> Node.t -> Node.t -> bool
val is_disequal : _ Egraph.t -> Node.t -> Node.t -> bool

val iter_on_value_different :
  they_are_different:(Node.t -> Value.t -> unit) -> Egraph.wt -> Node.t -> unit

val register_hook_new_disequality :
  _ Egraph.t -> (Egraph.wt -> Node.S.t -> unit) -> unit
(** register a hook that is called each time a set of nodes is known to be
    disequal *)

val th_register : Egraph.wt -> unit
