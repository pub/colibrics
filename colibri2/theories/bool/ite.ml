(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib
open Colibri2_core

(** ITE *)

let simplify d own cond then_ else_ =
  let branch = if cond then then_ else else_ in
  Egraph.register d branch;
  Egraph.merge d own branch

let new_ite d own cond then_ else_ ty =
  DaemonAlsoInterp.attach_value d cond Boolean.dom (fun d _ v ->
      simplify d own (Boolean.BoolValue.value v) then_ else_);
  if Ground.Ty.(equal ty bool) then (
    let ite_useless d _ _ =
      match (Boolean.is d then_, Boolean.is d else_) with
      | Some tb, Some eb -> (
          match (tb, eb) with
          | true, true -> Boolean.set_true d own
          | false, false -> Boolean.set_false d own
          | true, false -> Egraph.merge d own cond
          | false, true -> ())
      | _ -> ()
    in
    DaemonAlsoInterp.attach_value d then_ Boolean.dom ite_useless;
    DaemonAlsoInterp.attach_value d else_ Boolean.dom ite_useless;
    Equal_callback.add_callback d
      (fun d a _ ->
        DaemonAlsoInterp.schedule_immediately d (fun d -> Egraph.merge d own a))
      then_ else_)

(** {2 Interpretations} *)
module Check = struct
  let interp d n = Opt.get_exn Impossible (Egraph.get_value d n)

  let compute_ground d t =
    let ( !> ) n =
      Boolean.BoolValue.value (Boolean.BoolValue.coerce_nodevalue (interp d n))
    in
    let check r = Some r in
    match Ground.sem t with
    | { app = { builtin = Expr.Ite }; tyargs = [ _ ]; args; ty = _ } ->
        let c, a, b = IArray.extract3_exn args in
        check (if !>c then interp d a else interp d b)
    | _ -> None

  let init_check d =
    Interp.Register.check d (fun d t ->
        let check r =
          Interp.check_of_bool (Value.equal r (interp d (Ground.node t)))
        in
        match compute_ground d t with None -> NA | Some v -> check v);
    Interp.Register.compute d (fun d t ->
        match compute_ground d t with None -> NA | Some v -> Value v)

  let attach d g =
    let f d g =
      match compute_ground d g with
      | None -> raise Impossible
      | Some v -> Egraph.set_value d (Ground.node g) v
    in

    Interp.WatchArgs.create d f g
end

let converter d (t : Ground.t) =
  let res = Ground.node t in
  match Ground.sem t with
  | { app = { builtin = Expr.Ite }; tyargs = [ ty ]; args; _ } ->
      Check.attach d t;
      let c, a, b = IArray.extract3_exn args in
      Egraph.register d c;
      if Node.equal a b then (
        Egraph.register d a;
        Egraph.merge d res a)
      else (
        new_ite d res c a b ty;
        if not (Boolean.dec_at_literal d) then
          Choice.register d t (Boolean.chobool c))
  | _ -> ()

let th_register env =
  Ground.register_converter env converter;
  Check.init_check env;
  ()

let () = Init.add_default_theory th_register
