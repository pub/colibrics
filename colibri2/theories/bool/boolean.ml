(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib
open Popop_stdlib
open Std
open Colibri2_core

let debug = Debug.register_info_flag
  ~desc:"for the boolean theory"
  "bool"

module BoolValue = Value.Register(struct
    include DBool
    let name = "bool"
  end)

let dom = BoolValue.key

let value_true = BoolValue.index ~basename:"⊤" true
let values_true = BoolValue.nodevalue value_true
let node_true = BoolValue.node value_true

let value_false = BoolValue.index ~basename:"⊥" false
let values_false = BoolValue.nodevalue value_false
let node_false = BoolValue.node value_false

let value_of_bool b = if b then value_true else value_false
let values_of_bool b = if b then values_true else values_false

(* Function is not used. Don't know what the types of b1 and b2 are. *)
(* let union_disjoint m1 m2 =
 *   Node.M.union (fun _ b1 b2 -> assert (Equal.physical b1 b2); Some b1) m1 m2 *)

let is env node =
    let open Option in
    let+ v = Egraph.get_value env node in
    Opt.get_exn Impossible (Value.value dom v)

let is_true  env node = Node.equal node node_true
                        || Equal.option Equal.bool (is env node) (Some true)
let is_false env node = Node.equal node node_false
                        || Equal.option Equal.bool (is env node) (Some false)
let is_unknown env node = Equal.option Equal.bool (is env node) None

let set_bool env node b =
  match is env node with
  | Some true when b -> ()
  | Some false when not b -> ()
  | None ->
    Egraph.merge env node
      (if b then node_true else node_false)
  | _ -> Egraph.contradiction ()

let _true = node_true
let _false = node_false

let set_true env node = set_bool env node true
let set_false env node = set_bool env node false

let a = Expr.Term.Var.mk "a" Expr.Ty.bool
let ta = Expr.Term.of_var a
let _not_ta = Expr.Term.neg ta

let _not d na =
    let subst = { Ground.Subst.empty with
                  term = Expr.Term.Var.M.of_list [a,na] } in
    Ground.convert ~subst d _not_ta

let _gen mk d l =
  let v = List.map (fun _ -> Expr.Term.Var.mk "b" Expr.Ty.bool) l in
  let t = List.map Expr.Term.of_var v in
  let subst = { Ground.Subst.empty with
                term = Expr.Term.Var.M.of_list (List.combine v l) } in
  Ground.convert ~subst d (mk t)

let _and d l = _gen Expr.Term._and d l
let _or d l = _gen Expr.Term._or d l

(** {2 Choice on bool} *)

module ChoBool = struct
  let make_decision env node b =
    Debug.dprintf3 Debug.decision "[Bool] decide %b on %a" b Node.pp node;
    set_bool env node b

  let choose_decision node env =
    match is env node with
    | Some _ ->
      Debug.dprintf2 debug "[Bool] No decision for %a" Node.pp node;
      Choice.DecNo
    | None -> DecTodo
                (List.map (fun v env -> make_decision env node v)
                   (Shuffle.shufflel [true;false]))

end

let chobool n = {
  Choice.prio = 1;
  choice = ChoBool.choose_decision n;
  print_cho = (fun fmt () -> Fmt.pf fmt "boolean on %a" Node.pp n);
  key = Some n;
}

module TwoWatchLiteral = struct
  (** todo move the check to enqueue; but it requires to be able to attach [%here] event
      in delayed_ro *)

  module Daemon = struct
    type t = {
      absorbent: Value.t;
      ground : Ground.t;  (** perhaps ground should have an array for args *)
      from_start : int Context.Ref.t;  (** already set before *)
      from_end : int Context.Ref.t;  (** already set after *)
    }

    type runable = (Egraph.wt -> unit)

    let print_runable = Fmt.nop

    let key =
      Events.Dem.create
        (module struct
          type t = runable

          let name = "Interp.TwoWatchLiteral"
        end)

    let delay = Events.Delayed_by 1

    type res_scan =
      | LimitReached
      | Absorbent
      | ToWatch of int

    let rec check_if_set d args absorbent incr stop i =
      match Egraph.get_value d (IArray.get args i) with
      | None -> ToWatch i
      | Some b when Value.equal b absorbent ->
        Absorbent
      | Some _ -> (* neutral *)
        let i = incr + i in
        if i = stop then LimitReached else
          check_if_set d args absorbent incr stop i

    let rec check_if_equal_or_set d args absorbent incr stop i n =
      match check_if_set d args absorbent incr stop i with
      | LimitReached | Absorbent as r -> r
      | ToWatch i as r ->
        if not (Egraph.is_equal d n (IArray.get args i)) then r
        else
        let i = incr + i in
        if i = stop then LimitReached else
          check_if_equal_or_set d args absorbent incr stop i n

    let run d r = r d

    let rec attach : type a. a Egraph.t -> _ = fun d r i ->
      Events.attach_value d
        (IArray.get (Ground.sem r.ground).args i) BoolValue.key (fun d _ _ -> enqueue d r);
      Events.attach_repr d (IArray.get (Ground.sem r.ground).args i) (fun d _ -> enqueue d r);

    and enqueue d r =
      let o_from_start = Context.Ref.get r.from_start in
      let o_from_end = Context.Ref.get r.from_end in
      let args = (Ground.sem r.ground).args in
      if o_from_start = o_from_end then Events.EnqStopped
      else
        let g = Some (Ground.thterm r.ground) in
        match Egraph.get_value d (Ground.node r.ground) with
        | Some v when not (Value.equal v r.absorbent) ->
          Context.Ref.set r.from_end o_from_start;
          Events.EnqLast(key,(fun d -> IArray.iter ~f:(fun a -> Egraph.set_value d a v) args),g)
        | _ ->
          let n_end = IArray.get args o_from_end in
          match check_if_equal_or_set d args r.absorbent (+1) o_from_end o_from_start
                  n_end with
          | LimitReached ->
            Context.Ref.set r.from_start o_from_end;
            Events.EnqLast(key,(fun d -> Egraph.merge d n_end (Ground.node r.ground)),g)
          | Absorbent ->
            Events.EnqLast(key,(fun d -> Egraph.set_value d (Ground.node r.ground) r.absorbent),g)
          | ToWatch from_start ->
            if from_start <> o_from_start then (
              Context.Ref.set r.from_start from_start;
              attach d r from_start;
            );
            let n_start = IArray.get args from_start in
            match check_if_equal_or_set d args r.absorbent (-1) from_start o_from_end n_start with
            | LimitReached ->
              Context.Ref.set r.from_end from_start;
              Events.EnqLast (key,(fun d -> Egraph.merge d n_start (Ground.node r.ground)),g)
            | Absorbent ->
              Events.EnqLast (key,(fun d -> Egraph.set_value d (Ground.node r.ground) r.absorbent),g)
            | ToWatch from_end when from_end = o_from_end ->
              Events.EnqAlready
            | ToWatch from_end ->
              Context.Ref.set r.from_end from_end;
              attach d r from_end;
              Events.EnqAlready

    let create d absorbent ground =
      let args = (Ground.sem ground).args in
      assert (IArray.not_empty args);
      if IArray.length args = 1 then
        Egraph.merge d (IArray.get args 0) (Ground.node ground)
      else
        match Egraph.get_value d (Ground.node ground) with
        | Some v when not (Value.equal v absorbent) ->
          IArray.iter ~f:(fun a -> Egraph.set_value d a v) args
        | _ ->
          let r from_start from_end =
            assert (from_start < from_end);
            Debug.dprintf4 debug "Bool create %a (%i-%i)" Ground.pp ground
              from_start from_end;
            {
              ground; absorbent;
              from_start = Context.Ref.create (Egraph.context d) from_start;
              from_end = Context.Ref.create (Egraph.context d) from_end;
            }
          in
          match check_if_set d args absorbent 1 (IArray.length args - 1) 0 with
          | LimitReached ->
            Egraph.merge d (IArray.get args (IArray.length args - 1)) (Ground.node ground)
          | Absorbent ->
            Egraph.set_value d (Ground.node ground) absorbent
          | ToWatch from_start ->
            let n_start = IArray.get args from_start in
            match
              check_if_equal_or_set d args absorbent (-1) from_start (IArray.length args - 1) n_start
            with
            | LimitReached ->
              Egraph.merge d n_start (Ground.node ground)
            | Absorbent ->
              Egraph.set_value d (Ground.node ground) absorbent
            | ToWatch from_end ->
              let r = r from_start from_end in
              Events.attach_any_value d (Ground.node ground) (fun _ _ v ->
                if not (Value.equal v absorbent) then
                  Events.EnqLast(key,(fun d -> IArray.iter ~f:(fun a -> Egraph.set_value d a v) args),
                    (Some (Ground.thterm ground)))
                else Events.EnqStopped
              );
              attach d r from_start;
              attach d r from_end
  end

  let () = Events.register (module Daemon)

  let create = Daemon.create
end

let dec_at_literal_option = Options.register_flag "dec-at-operators" ~doc:"Instead of deciding boolean literal, decide at operators whose value can't be propagated to arguments (e.g. a true disjunction)"

let dec_at_literal d = not (Options.get d dec_at_literal_option)

let decide_on_args_when_known d g ~unknown =
  if not (dec_at_literal d) then
  let n = Ground.node g in
  DaemonOnlyPropa.attach_value d n BoolValue.key (fun d _ v ->
    if BoolValue.equal v unknown then
      let choice _ =
        let unknown = BoolValue.nodevalue unknown in
        let args = (Ground.sem g).args in
        if IArray.for_alli args ~f:(fun _ a ->
          match Egraph.get_value d a with
          | None -> true
          | Some v -> not (Value.equal v unknown)) then
        let l = IArray.to_list args in
        let l = List.map (fun n d -> Egraph.set_value d n unknown) l in
        Choice.DecTodo l
    else
      Choice.DecNo
      in
      let dec = {Choice.prio = 1; choice; print_cho = (fun fmt () -> Fmt.pf fmt "decide on boolean operator %a" Ground.pp g); key = Some n}
      in
      Choice.register d g dec
    )

let converter d (f:Ground.t) =
  let res = Ground.node f in
  let reg_args args = IArray.iter ~f:(Egraph.register d) args in
  let reg n =
    Egraph.register d n;
    Egraph.merge d res n
  in
  match Ground.sem f with
  | { app = {builtin = Expr.Base; _ }; ty; _ }
    when Ground.Ty.equal ty Ground.Ty.bool  ->
      if dec_at_literal d then
    Choice.register d f (chobool res)
  | { app = {builtin = Expr.Or}; tyargs = []; args; _ } ->
    reg_args args;
    TwoWatchLiteral.create d values_true f;
    decide_on_args_when_known d f ~unknown:value_true
  | { app = {builtin = Expr.And}; tyargs = []; args; _ } ->
    reg_args args;
    TwoWatchLiteral.create d values_false f;
    decide_on_args_when_known d f ~unknown:value_false
  | { app = {builtin = Expr.Imply}; tyargs = []; args; _ } ->
    let a,b = IArray.extract2_exn args in
    reg_args args;
    let wait d n v =
      let v = BoolValue.value v in
      if Egraph.is_equal d n res then
        if v
        then (if not (dec_at_literal d) then
          let choice _ =
            let l = List.map (fun (n,v) d -> Egraph.set_value d n v)
          [a,values_false;b,values_true] in
            Choice.DecTodo l
          in
          let dec = {Choice.prio = 1; choice; print_cho = (fun fmt () -> Fmt.pf fmt "decide on boolean operator %a" Ground.pp f); key = Some n}
          in
          Choice.register d f dec)
        else
          (set_bool d a true; set_bool d b false);
      if Egraph.is_equal d n a then
        (if v then Egraph.merge d res b
         else set_bool d res true);
      if Egraph.is_equal d n b then
        if v then set_bool d res true
        else if is_true d res then set_bool d a false
    in
    List.iter (fun n -> DaemonAlsoInterp.attach_value d n BoolValue.key wait) [a;b;res]
  | { app = {builtin = Expr.Neg}; tyargs = []; args; _ } ->
    let a = IArray.extract1_exn args in
    reg_args args;
    DaemonAlsoInterp.attach_value d a BoolValue.key (fun d _ v -> set_bool d res (not (BoolValue.value v)));
    DaemonAlsoInterp.attach_value d res BoolValue.key (fun d _ v -> set_bool d a (not (BoolValue.value v)))
  | { app = {builtin = Expr.True}; tyargs = []; args; _ } ->
    assert ( IArray.is_empty args);
    reg _true
  | { app = {builtin = Expr.False}; tyargs = []; args; _ } ->
    assert ( IArray.is_empty args);
    reg _false
    | { app = {builtin = (Expr.Equal|Expr.Equiv)}; tyargs = [ty]; args; _ } when Ground.Ty.equal Ground.Ty.bool ty ->
      if not (dec_at_literal d) then
        DaemonOnlyPropa.attach_value d res BoolValue.key (fun d _ _ ->
          Choice.register d f (chobool (IArray.get args 0))
          )
  | _ -> ()


let init_check d =
  Interp.Register.check d (fun d t ->
      let interp n = Opt.get_exn Impossible (Egraph.get_value d n) in
      let (!>) t = BoolValue.value (BoolValue.coerce_nodevalue (interp t)) in
      let check r =
        Interp.check_of_bool (Value.equal r (interp (Ground.node t)))
      in
      let (!<) b =
        let r = if b then values_true else values_false in
        check r
      in
      match Ground.sem t with
      | { app = {builtin = Expr.Or}; tyargs = []; args; ty = _ } ->
        !< (IArray.fold ~f:(||) ~init:false (IArray.map ~f:(!>) args))
      | { app = {builtin = Expr.And}; tyargs = []; args; ty = _ } ->
        !< (IArray.fold ~f:(&&) ~init:true (IArray.map ~f:(!>) args))
      | { app = {builtin = Expr.Imply}; tyargs = []; args; ty = _ } ->
        let a,b = IArray.extract2_exn args in
        !< ( not (!> a) || !> b )
      | { app = {builtin = Expr.Xor}; tyargs = []; args; _ } ->
        let a,b = IArray.extract2_exn args in
        !< ( not (Bool.equal (!> a) (!> b) ))
      | { app = {builtin = Expr.Neg}; tyargs = []; args; ty = _ } ->
        let a = IArray.extract1_exn args in
        !< (not (!> a))
      | { app = {builtin = Expr.True}; tyargs = []; args; ty = _ } ->
        assert ( IArray.is_empty args);
        check values_true
      | { app = {builtin = Expr.False}; tyargs = []; args; ty = _ } ->
        assert ( IArray.is_empty args);
        check values_false
      | _ -> NA
    )

let init_ty d =
  Interp.Register.ty d (fun _ ty ->
      match ty with
      | { app = { builtin = Expr.Prop; _ }; _ } ->
        Some ((Sequence.of_list [values_true;values_false]))
      | _ -> None)


let th_register env =
  Egraph.register env node_true;
  Egraph.register env node_false;
  Ground.register_converter env converter;
  init_check env;
  init_ty env;
  let pp _ _ fmt c = BoolValue.SD.pp fmt (BoolValue.value c) in
  Interp.Register.print_value_smt dom pp;
  ()

let () = Init.add_default_theory th_register
