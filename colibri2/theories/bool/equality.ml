(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib
open Popop_stdlib
open Colibri2_core

let debug =
  Debug.register_info_flag ~desc:"for the equality and disequality predicate"
    "disequality"

(** {2 theory term} *)

(** Is there two elements equal *)

module Th = struct
  let inv s = not (Node.M.is_empty s || Node.M.is_num_elt 1 s)

  let only_two s =
    assert (inv s);
    if Node.M.is_num_elt 2 s then
      let enum = Node.M.start_enum s in
      let (cl1, ()), enum =
        (Opt.get (Node.M.val_enum enum), Node.M.next_enum enum)
      in
      let cl2, () = Opt.get (Node.M.val_enum enum) in
      Some (cl1, cl2)
    else None

  module T : OrderedHashedType with type t = Node.S.t = struct
    include Node.S

    let hash s =
      Node.S.fold (fun e acc -> Hashcons.combine acc (Node.hash e)) s 29

    let pp fmt s =
      assert (inv s);
      match only_two s with
      | Some (cl1, cl2) -> Format.fprintf fmt "%a=@,%a" Node.pp cl1 Node.pp cl2
      | None ->
          let aux fmt m =
            Node.S.elements m |> Format.(list ~sep:(const char ',') Node.pp) fmt
          in
          Format.fprintf fmt "or=(%a)" aux s
  end

  include T
  include MkDatatype (T)

  let ty _ = Ground.Ty.bool
  let name = "Eq"
end

module ThE = ThTerm.Register (Th)

(** {2 disequality domains} *)

module Dis : sig
  type t

  val pp : t Format.printer
  val equality : t -> t -> bool
  val empty : t
  val union_disjoint : Egraph.wt -> t -> t -> t
  val disequal : _ Egraph.t -> t -> t -> bool
  val singleton : ThE.t -> int -> t
  val iter : (ThE.t -> unit) -> t -> unit
  val iter_disequalities : _ Egraph.t -> (ThE.t -> unit) -> t -> unit
end = struct
  type t = int ThE.M.t

  let empty = ThE.M.empty
  let equality = ThE.M.equal Int.equal

  let pp fmt s =
    let aux fmt m =
      ThE.M.bindings m
      |> Format.(
           list ~sep:(const char ';')
             (pair ~sep:(const char ',') ThE.pp Pp.nothing))
           fmt
    in
    Format.fprintf fmt "{%a}" aux s

  let union_disjoint d m1 m2 =
    ThE.M.union
      (fun c a b ->
        if Int.equal a b then Some a
        else (
          if not (Boolean.is_true d (ThE.node c)) then (
            Debug.dprintf2 Debug.contradiction
              "[Equality] The modification of the domain makes %a true" ThE.pp c;
            Boolean.set_true d (ThE.node c));
          None))
      m1 m2

  let disjoint d m1 m2 =
    ThE.M.disjoint
      (fun the a b -> Int.equal a b || not (Boolean.is_false d (ThE.node the)))
      m1 m2

  let disequal d m1 m2 = not (disjoint d m1 m2)
  let singleton = ThE.M.singleton
  let iter f m = ThE.M.iter (fun k _ -> f k) m

  let iter_disequalities d f m =
    iter (fun the -> if Boolean.is_false d (ThE.node the) then f the) m
end

let dom =
  Dom.Kind.create
    (module struct
      type t = Dis.t

      let name = "dis"
    end)

module Simplify = struct
  exception Found of Node.t * Node.t

  let is_disequal d (dis1, values1) (dis2, values2) =
    (match (dis1, dis2) with
    | Some dis1, Some dis2 when Dis.disequal d dis1 dis2 -> true
    | _ -> false)
    ||
    match (values1, values2) with
    | Some v1, Some v2 -> not (Value.equal v1 v2)
    | _ -> false

  let get_dis_and_values d cl = (Egraph.get_dom d dom cl, Egraph.get_value d cl)

  type found_one = NoneFound | OneFound of Node.t * Node.t | StopFirst

  let rec inner_loop d n cl1 s1 enum2 =
    match (enum2, s1) with
    | [], _ -> n
    | (_, d1) :: enum2, d2 when is_disequal d d1 d2 ->
        inner_loop d n cl1 s1 enum2
    | (cl2, _) :: enum2, _ -> (
        match n with
        | NoneFound -> inner_loop d (OneFound (cl1, cl2)) cl1 s1 enum2
        | OneFound _ | StopFirst -> raise (Found (cl1, cl2)))

  let rec outer_loop d n enum1 =
    match enum1 with
    | [] -> n
    | (cl1, s1) :: enum1 -> outer_loop d (inner_loop d n cl1 s1 enum1) enum1

  let init d s =
    Node.M.fold_left
      (fun acc cl () -> (cl, get_dis_and_values d cl) :: acc)
      [] s

  let find_not_disequal d s =
    assert (Th.inv s);
    try
      ignore (outer_loop d StopFirst (init d s));
      `AllDiff
    with Found (cl1, cl2) -> `Found (cl1, cl2)

  let find_only_one_not_disequal d s =
    assert (Th.inv s);
    try
      match outer_loop d NoneFound (init d s) with
      | NoneFound | StopFirst -> `AllDiff
      | OneFound (cl1, cl2) ->
          (* "heuristic": respect ordering for the tests *)
          if Node.compare cl1 cl2 < 0 then `OnlyOne (cl1, cl2)
          else `OnlyOne (cl2, cl1)
    with Found _ -> `AtLeastTwo
end

let check_disequal d dis =
  Dis.iter
    (fun the ->
      match Simplify.find_only_one_not_disequal d (ThE.sem the) with
      | `AllDiff -> Boolean.set_false d (ThE.node the)
      | `OnlyOne (cl1, cl2) ->
          if Boolean.is_true d (ThE.node the) then Egraph.merge d cl1 cl2
      | `AtLeastTwo -> ())
    dis

module D = struct
  type t = Dis.t

  let merged (b1 : t option) (b2 : t option) =
    match (b1, b2) with
    | Some b1, Some b2 -> Dis.equality b1 b2
    | None, None -> true
    | _ -> false

  let merge d (s1, cl1) (s2, cl2) _ =
    match (s1, s2) with
    | None, None -> raise Impossible
    | Some s, None ->
        Egraph.set_dom d dom cl2 s;
        check_disequal d s
    | None, Some s ->
        Egraph.set_dom d dom cl1 s;
        check_disequal d s
    | Some s1, Some s2 ->
        let s = Dis.union_disjoint d s1 s2 in
        Egraph.set_dom d dom cl1 s;
        Egraph.set_dom d dom cl2 s;
        check_disequal d s

  let pp fmt s = Dis.pp fmt s
  let key = dom
  let wakeup_threshold = None
end

let () = Dom.register (module D)

let set_dom d cl s =
  let s =
    match Egraph.get_dom d dom cl with
    | Some s' ->
        Debug.dprintf4 debug "[%a %a]" Dis.pp s Dis.pp s';
        Dis.union_disjoint d s' s
    | None -> s
  in
  Egraph.set_dom d dom cl s;
  check_disequal d s

(** API *)

let equality_ ?(on_the = fun _ -> ()) _ cll =
  try
    let fold acc e = Node.S.add_new Exit e acc in
    let s = List.fold_left fold Node.S.empty cll in
    let v = ThE.index s in
    on_the v;
    ThE.node v
  with Exit -> Boolean._true

let equality d cl1 = equality_ d cl1
let disequality d cll = Boolean._not d (equality d cll)
let disequality_ ?on_the d cll = Boolean._not d (equality_ ?on_the d cll)
let is_equal t cl1 cl2 = Egraph.is_equal t cl1 cl2

let is_disequal t cl1 cl2 =
  (not (Egraph.is_equal t cl1 cl2))
  &&
  let dom1 = Egraph.get_dom t dom cl1 in
  let dom2 = Egraph.get_dom t dom cl2 in
  match (dom1, dom2) with
  | Some s1, Some s2 -> Dis.disequal t s1 s2
  | _ -> false

(** each instance of this tag must not be == *)

let add_new_tag_hooks =
  Datastructure.Push.create Fmt.nop "Equality.new_disequality"

let register_hook_new_disequality d f =
  Datastructure.Push.push add_new_tag_hooks d f

let norm_set d the =
  let v = ThE.sem the in
  let own = ThE.node the in
  let h = Node.H.create 10 in
  try
    Node.S.iter
      (fun e0 ->
        let e = Egraph.find_def d e0 in
        Node.H.add_new Exit h e ())
      v;
    false
  with Exit ->
    Boolean.set_true d own;
    true

module ChoEquals = struct
  let make_equal the (cl1, cl2) d =
    Debug.dprintf6 Debug.decision
      "[Equality] @[decide on merge of %a and %a in %a@]" Node.pp cl1 Node.pp
      cl2 ThE.pp the;
    Egraph.register d cl1;
    Egraph.register d cl2;
    Egraph.merge d cl1 cl2

  let make_disequal the (cl1, cl2) d =
    Debug.dprintf6 Debug.decision
      "[Equality] @[decide on disequality of %a and %a in %a@]" Node.pp cl1
      Node.pp cl2 ThE.pp the;
    Egraph.register d cl1;
    Egraph.register d cl2;
    let s = Node.S.of_list [ cl1; cl2 ] in
    let the = ThE.index s in
    Egraph.register d (ThE.node the);
    Boolean.set_false d (ThE.node the)

  let choose_decision the d =
    let v = ThE.sem the in
    let own = ThE.node the in
    Debug.dprintf4 debug "[Equality] @[dec on %a for %a@]" Node.pp own ThE.pp
      the;
    if Boolean.is_false d own then Choice.DecNo
    else if norm_set d the then Choice.DecNo
    else
      match Simplify.find_not_disequal d v with
      | `AllDiff ->
          Boolean.set_false d own;
          DecNo
      | `Found (cl1, cl2) ->
          DecTodo [ make_equal the (cl1, cl2); make_disequal the (cl1, cl2) ]

  let mk_choice the =
    {
      Choice.choice = choose_decision the;
      prio = 1;
      print_cho = (fun fmt () -> Fmt.pf fmt "equality: %a" ThE.pp the);
      key = Some (ThE.node the);
    }
end

let add_new_tag d the =
  let s = ThE.sem the in
  Debug.dprintf2 debug "[Dis: %a]" ThE.pp the;
  let i = ref (-1) in
  Node.S.iter
    (fun cl ->
      incr i;
      set_dom d cl (Dis.singleton the !i))
    s;
  DaemonAlsoInterp.attach_value d (ThE.node the) Boolean.dom (fun d _own v ->
      if Boolean.BoolValue.equal Boolean.value_false v then (
        let s' =
          lazy
            (Node.S.fold
               (fun n acc -> Node.S.add (Egraph.find_def d n) acc)
               s Node.S.empty)
        in
        Node.S.iter
          (fun n1 ->
            Disequality.update_disequality d n1 (fun d n2 ->
                (not (Egraph.is_equal d n1 n2))
                && Node.S.mem (Egraph.find_def d n2) (Lazy.force s')))
          s;
        Datastructure.Push.iter add_new_tag_hooks d ~f:(fun f -> f d s))
      else
        match Th.only_two s with
        | Some (cl1, cl2) -> Egraph.merge d cl1 cl2
        | None -> ())

let norm_dom d the =
  let v = ThE.sem the in
  let own = ThE.node the in
  Debug.dprintf4 debug "[Equality] @[norm_dom %a %a@]" Node.pp own Th.pp v;
  match Boolean.is d own with
  | Some false ->
      add_new_tag d the;
      false
  | Some true -> (
      match Simplify.find_only_one_not_disequal d v with
      | `AllDiff ->
          Boolean.set_false d own;
          false
      | `OnlyOne (cl1, cl2) ->
          Egraph.merge d cl1 cl2;
          false
      | `AtLeastTwo ->
          add_new_tag d the;
          true)
  | None -> (
      match Simplify.find_not_disequal d v with
      | `AllDiff ->
          Boolean.set_false d own;
          false
      | `Found _ ->
          add_new_tag d the;
          true)

(** Propagation *)

let attach_new_equalities d =
  DaemonAlsoInterp.attach_reg_sem d ThE.key (fun d clsem ->
      let v = ThE.sem clsem in
      let own = ThE.node clsem in
      Node.S.iter (Egraph.register d) v;
      if norm_set d clsem then ()
      else if norm_dom d clsem then (
        Debug.dprintf4 debug "[Equality] @[ask_dec on %a for %a@]" Node.pp own
          Th.pp v;
        Choice.register_thterm d (ThE.thterm clsem) (ChoEquals.mk_choice clsem)))

(** {2 Link between diff and values} *)

(** If can't be a value it they share a diff tag, are different *)

(** Give for a node the values that are different *)
let iter_on_value_different ~they_are_different (d : _ Egraph.t) (own : Node.t)
    =
  let dis = Opt.get_def Dis.empty (Egraph.get_dom d dom own) in
  let iter elt =
    let iter n =
      if not (Egraph.is_equal d own n) then
        match Egraph.get_value d n with
        | None -> ()
        | Some v -> they_are_different n v
    in
    if Boolean.is_false d (ThE.node elt) then Node.S.iter iter (ThE.sem elt)
  in
  Dis.iter iter dis

(** Give for a value the nodes that are different *)
let init_diff_to_value (type a b) ?(already_registered = ([] : b list)) d0
    (module Val : Colibri2_core.Value.S with type s = a and type t = b)
    ~(they_are_different : _ Egraph.t -> Node.t -> a -> unit) =
  let propagate_diff d v =
    let own = Val.node v in
    if Boolean.is_false d own then
      let dis = Opt.get_def Dis.empty (Egraph.get_dom d dom own) in
      let iter elt =
        let iter n =
          if not (Egraph.is_equal d own n) then
            they_are_different d n (Val.value v)
        in
        if Boolean.is_false d (ThE.node elt) then Node.S.iter iter (ThE.sem elt)
      in
      Dis.iter iter dis
  in
  let init d (v : Val.t) =
    propagate_diff d v;
    DaemonAlsoInterp.attach_dom d (Val.node v) dom (fun d _ ->
        propagate_diff d v)
  in
  List.iter (init d0) already_registered

(** {3 For booleans} *)
(* Since the module Bool is linked before *)

let bool_init_diff_to_value d =
  init_diff_to_value d
    (module Boolean.BoolValue)
    ~they_are_different:(fun d n b ->
      if not b then Boolean.set_true d n else Boolean.set_false d n)
    ~already_registered:[ Boolean.value_true; Boolean.value_false ]

(** {2 Interpretations} *)
module Check = struct
  let interp d n = Opt.get_exn Impossible (Egraph.get_value d n)

  let compute_ground d t =
    let check r = Some r in
    let ( !< ) b =
      let r = if b then Boolean.values_true else Boolean.values_false in
      check r
    in
    match Ground.sem t with
    | { app = { builtin = Expr.Equal }; tyargs = [ _ ]; args; ty = _ } ->
        assert (IArray.not_empty args);
        !<(IArray.for_alli_non_empty_exn ~init:(interp d)
             ~f:(fun _ a t -> Value.equal a (interp d t))
             args)
    | { app = { builtin = Expr.Equiv }; tyargs = [ _ ]; args; ty = _ } ->
        let a, b = IArray.extract2_exn args in
        !<(Value.equal (interp d a) (interp d b))
    | { app = { builtin = Expr.Distinct }; tyargs = [ _ ]; args; ty = _ } -> (
        try
          let fold acc v = Value.S.add_new Exit (interp d v) acc in
          let _ = IArray.fold ~f:fold ~init:Value.S.empty args in
          check Boolean.values_true
        with Exit -> check Boolean.values_false)
    | _ -> None

  let init_check d =
    Interp.Register.check d (fun d t ->
        let check r =
          Interp.check_of_bool (Value.equal r (interp d (Ground.node t)))
        in
        match compute_ground d t with None -> NA | Some v -> check v);
    Interp.Register.compute d (fun d t ->
        match compute_ground d t with None -> NA | Some v -> Value v)

  let attach d g =
    let f d g =
      match compute_ground d g with
      | None -> raise Impossible
      | Some v -> Egraph.set_value d (Ground.node g) v
    in

    Interp.WatchArgs.create d f g
end

let converter d (t : Ground.t) =
  let res = Ground.node t in
  let of_term n =
    Egraph.register d n;
    n
  in
  let reg n = Egraph.register d n in
  let reg_and_merge n =
    Egraph.register d n;
    Egraph.merge d res n
  in
  let mark th =
    Choice.Group.add_to_group_of d (ThE.thterm th) (Ground.thterm t)
  in
  match Ground.sem t with
  | { app = { builtin = Expr.Equal }; tyargs = [ _ ]; args = l; _ } ->
      Check.attach d t;
      IArray.iter ~f:reg l;
      reg_and_merge (equality_ ~on_the:mark d (IArray.to_list l))
  | { app = { builtin = Expr.Equiv }; tyargs = [ _ ]; args; _ } ->
      Check.attach d t;
      let a, b = IArray.extract2_exn args in
      reg_and_merge (equality_ ~on_the:mark d [ of_term a; of_term b ])
  | { app = { builtin = Expr.Distinct }; tyargs = [ _ ]; args; _ } ->
      Check.attach d t;
      IArray.iter ~f:reg args;
      reg_and_merge (disequality d (IArray.to_list args))
  | { app = { builtin = Expr.Xor }; tyargs = []; args; _ } ->
      let a, b = IArray.extract2_exn args in
      reg_and_merge (disequality_ ~on_the:mark d [ of_term a; of_term b ])
  | _ -> ()

let th_register env =
  attach_new_equalities env;
  Ground.register_converter env converter;
  bool_init_diff_to_value env;
  Check.init_check env;
  Disequality.register_disequal env is_disequal;
  (* Not efficient but simple: disequality_to_value *)
  let wakeup env n =
    match Egraph.get_value env n with
    | None -> ()
    | Some v -> (
        match Egraph.get_dom env dom n with
        | None -> ()
        | Some dis ->
            let iter f =
              Dis.iter_disequalities env
                (fun k ->
                  Node.S.iter
                    (fun n' -> if not (Egraph.is_equal env n n') then f env n')
                    (ThE.sem k))
                dis
            in
            Disequality.update_disequality_to_value env v iter)
  in
  DaemonAlsoInterp.attach_any_dom env dom wakeup;
  ()

let () = Init.add_default_theory th_register
