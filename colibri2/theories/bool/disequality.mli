(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

val is_disequal : Egraph.wt -> Node.t -> Node.t -> bool
(** [is_disequal d n1 n2] indicates if n1 and n2 are known to be distinct *)

val register_disequal :
  Egraph.wt -> (Egraph.wt -> Node.t -> Node.t -> bool) -> unit
(** [register_disequal f d] Register function [f] to test disequality *)

val update_disequality :
  Egraph.wt -> Node.t -> (Egraph.wt -> Node.t -> bool) -> unit
(** [update_disequality upd d n] warn that the disequality with [n] could have
    changed *)

val register_update :
  Egraph.wt ->
  (Egraph.wt -> Node.t -> (Egraph.wt -> Node.t -> bool) -> unit) ->
  unit

(** [new_daemon d f] registers a function [f] where [f n1 g] is called when the
    disequalities of n1 could have changed, [g] allows to check for such new or
    not disequalities. *)

type disequality_to_value =
  Egraph.wt -> Value.t -> ((Egraph.wt -> Node.t -> unit) -> unit) -> unit

val update_disequality_to_value : disequality_to_value
(** [update_disequality_to_value d v iter] warn that the potentially new nodes
    are disequal with [v] *)

val register_disequality_to_value : Egraph.wt -> disequality_to_value -> unit
(** [register_update d f] registers a function [f] where [f v g] is called when
    some values has new disequalities *)
