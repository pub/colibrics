(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

val _true : Node.t
val _false : Node.t
val _and  : _ Egraph.t -> Node.t list -> Node.t
val _or   : _ Egraph.t -> Node.t list -> Node.t
val _not  : _ Egraph.t -> Node.t -> Node.t

val set_true  : Egraph.wt -> Node.t -> unit
val set_false : Egraph.wt -> Node.t -> unit
val set_bool  : Egraph.wt -> Node.t -> bool -> unit

val is       : _ Egraph.t -> Node.t -> bool option
val is_true  : _ Egraph.t -> Node.t -> bool
val is_false : _ Egraph.t -> Node.t -> bool
(** is_true t node = false means the value is not constrained by the
    current constraints or due to incompletness *)
val is_unknown : _ Egraph.t -> Node.t -> bool

(* val true_is_false : Egraph.d -> Node.t -> Trail.Pexp.t -> 'a *)

val th_register: Egraph.wt -> unit

val chobool: Node.t -> Choice.t
val dec_at_literal: _ Egraph.t -> bool

(* val make_dec: Variable.make_dec *)

module BoolValue : Value.S with type s = bool

val dom: (bool,BoolValue.t) Value.Kind.t
(** The domain of boolean is isomorph to there Values *)

val value_true : BoolValue.t
val value_false: BoolValue.t
val values_true : Value.t
val values_false: Value.t
val value_of_bool: bool -> BoolValue.t
val values_of_bool: bool -> Value.t
