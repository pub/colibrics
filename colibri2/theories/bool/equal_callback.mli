type callback = Egraph.rt -> Node.t -> Node.t -> unit

val add_callback : Egraph.rw Egraph.t -> callback -> Node.t -> Node.t -> unit
(** [add_callback d f n1 n2] when [n1] and [n2] are merged call the given
    function with those congruence classes. Call directly the functions if those
    nodes are already merged. *)

val add_n_callback : Egraph.rw Egraph.t -> callback -> Node.t list -> unit
(** Same as {!add_callback}. [add_callback d f l] when equivalence classes in
    [l] are merged call the given function with the merged congruence classes *)
