(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_core

let disequal_hook = Datastructure.Push.create Fmt.nop "disequal_hook"
let register_disequal d f = Datastructure.Push.push disequal_hook d f

let is_disequal d n1 n2 =
  try
    Datastructure.Push.iter
      ~f:(fun f -> if f d n1 n2 then raise Exit)
      disequal_hook d;
    false
  with Exit -> true

let update_hook = Datastructure.Push.create Fmt.nop "update_hook"
let register_update d f = Datastructure.Push.push update_hook d f

let update_disequality d n1 check =
  Datastructure.Push.iter ~f:(fun f -> f d n1 check) update_hook d

type disequality_to_value =
  Egraph.wt -> Value.t -> ((Egraph.wt -> Node.t -> unit) -> unit) -> unit

let disequality_to_value =
  Datastructure.Push.create Fmt.nop "disequality_to_value"

let register_disequality_to_value d f =
  Datastructure.Push.push disequality_to_value d f

let update_disequality_to_value d n1 check =
  Datastructure.Push.iter ~f:(fun f -> f d n1 check) disequality_to_value d
