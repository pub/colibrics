(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_core
open Colibri2_popop_lib
open Popop_stdlib

module FVal = struct
  module T = struct
    type t = IsForeign [@@deriving eq, ord, hash, show]
  end

  include T
  include MkDatatype (T)
end

module D = struct
  type t = FVal.t = IsForeign [@@deriving eq, show]

  let is_singleton _ _ _ = None

  let key =
    Dom.Kind.create
      (module struct
        type nonrec t = t

        let name = "Array.foreign"
      end)

  let inter _ _ _ = Some IsForeign
  let wakeup_threshold = None
end

include D
include Dom.Lattice (D)

let new_foreign_array_hooks :
    (Egraph.wt -> Ground.Ty.t -> Node.t -> unit) Datastructure.Push.t =
  Datastructure.Push.create Fmt.nop "Array.new_foreign_array"

let register_hook_new_foreign_array env f =
  Datastructure.Push.push new_foreign_array_hooks env f

let set_dom env ty n d =
  set_dom env n d;
  Datastructure.Push.iter ~f:(fun f -> f env ty n) new_foreign_array_hooks env

let upd_dom env ty n d =
  upd_dom env n d;
  Datastructure.Push.iter ~f:(fun f -> f env ty n) new_foreign_array_hooks env
