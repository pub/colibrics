(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

include Colibri2_theories_utils.Ident

module ITN = MkIHT (struct
  type t = Node.t

  let pp = Node.pp
  let name = "IdsToNodes"
end)

let pp_nid env fmt i =
  match ITN.find_opt env i with
  | Some n -> Format.fprintf fmt "%a{id = %a}" Node.pp n pp i
  | None -> Format.fprintf fmt "Not_found{id = %a}" pp i

module Array = MkIdDom (struct
  let name = "Array.Id"
end)

module Index = MkIdDom (struct
  let name = "Array.Index.Id"
end)

module Value = MkIdDom (struct
  let name = "Array.Value.Id"
end)

let get = ITN.find
let set = ITN.set
let remove = ITN.remove
let pp_ht = ITN.pp
