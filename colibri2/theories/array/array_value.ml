(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_core
open Colibri2_popop_lib
open Popop_stdlib
open Common
open Colibri2_theories_quantifiers

let ( let+ ) = Interp.Seq.( let+ )
let ( and* ) = Interp.Seq.( and* )

module ArrayModelVal = struct
  type aval = {
    ind_gty : Ground.Ty.t;
    val_gty : Ground.Ty.t;
    vals : Value.t Value.M.t;
    other : Value.t;
  }

  include Value.Register (struct
    module T = struct
      type t = aval = {
        ind_gty : Ground.Ty.t;
        val_gty : Ground.Ty.t;
        vals : Value.t Value.M.t;
        other : Value.t;
      }
      [@@deriving eq, ord, hash]

      let pp fmt { ind_gty; val_gty; other; vals } =
        if false then
          Format.fprintf fmt "Array(%a => %a)" Ground.Ty.pp ind_gty Ground.Ty.pp
            val_gty
        else
          Format.fprintf fmt "Array(%a => %a: %a: %a)" Ground.Ty.pp ind_gty
            Ground.Ty.pp val_gty Value.pp other (Value.M.pp Value.pp) vals
    end

    include T
    include MkDatatype (T)

    let name = "Array.model.val"
  end)

  let index t =
    index
      {
        t with
        vals = Value.M.filter (fun _ v -> not (Value.equal t.other v)) t.vals;
      }

  let of_value v = nodevalue (index v)
end

let rec add_to_seq m d inds val_gty =
  let open Std.Sequence in
  match inds with
  | [] -> m
  | ind :: inds ->
      let m =
        let+ vals = m and* valv = Interp.ty d val_gty in
        Value.M.add ind valv vals
      in
      add_to_seq m d inds val_gty

let get_values v =
  let ArrayModelVal.{ other; vals } = ArrayModelVal.value v in
  (other, vals)

let all_arrays d ind_gty val_gty =
  let open Std.Sequence in
  let size =
    Sequence.unfold
      ~init:(Interp.ty d ind_gty, [ [] ])
      ~f:(fun (elts, seq0) ->
        match Sequence.next elts with
        | Some (elt, elts) ->
            let seq = Base.List.map seq0 ~f:(List.cons elt) in
            let seq' = seq0 @ seq in
            Some (Sequence.of_list seq, (elts, seq'))
        | None -> None)
  in
  let size = Sequence.interleave size in
  let vals_ =
    Sequence.map size ~f:(fun inds ->
        let seq =
          add_to_seq (Sequence.singleton Value.M.empty) d inds val_gty
        in
        seq)
  in
  let vals_ = Sequence.interleave vals_ in
  let+ vals = vals_ and* other = Interp.ty d val_gty in
  ArrayModelVal.nodevalue
    (ArrayModelVal.index { ind_gty; val_gty; vals; other })

(* ArrayModelVal.nodevalue
   (ArrayModelVal.index { ind_gty; val_gty; vals; other }) *)

(* let add_ind d n ind_gty val_gty k acc =
     let open Interp.SeqLim in
     let+ vals = acc
     and* indv =
       match Egraph.get_value d k with
       | None -> Interp.ty d ind_gty
       | Some kv -> Interp.SeqLim.of_seq d (Sequence.singleton kv)
     and* valv =
       let nk = mk_select d n k ind_gty val_gty in
       match Egraph.get_value d nk with
       | Some nkv -> Interp.SeqLim.of_seq d (Sequence.singleton nkv)
       | None | (exception Egraph.NotRegistered) -> Interp.ty d val_gty
     in
     Value.M.add indv valv vals

   let array_reads d reads n =
     Debug.dprintf4 debug "Array_value.array_reads %a : %a" Node.pp n Node.S.pp
       reads;
     let open Interp.SeqLim in
     let ind_gty, val_gty = array_gty_args (get_node_ty d n) in
     let init = Interp.SeqLim.of_seq d (Sequence.singleton Value.M.empty) in
     let+ other = Interp.ty d val_gty
     and* vals = Node.S.fold (add_ind d n ind_gty val_gty) reads init in
     ArrayModelVal.nodevalue
       (ArrayModelVal.index { ind_gty; val_gty; vals; other }) *)

let indv_to_vals ~rec_node_values d _ind_gty _val_gty inode vnode m =
  let open Interp.Seq in
  let+ iv =
    match Egraph.get_value d inode with
    | None -> rec_node_values d inode
    | Some ind_v -> Interp.Seq.return ind_v
  and* vv =
    match Egraph.get_value d vnode with
    | None -> rec_node_values d vnode
    | Some v -> Interp.Seq.return v
  in
  Value.M.add iv vv m

let map_to_values ~rec_node_values d ind_gty val_gty m =
  let open Interp.Seq in
  Id.M.fold
    (fun i (v, _) acc ->
      let inode = Id.get d i in
      let acc =
        let+ acc = acc in
        indv_to_vals ~rec_node_values d ind_gty val_gty inode v acc
      in
      Interp.Seq.concat acc)
    m
    (Interp.Seq.return Value.M.empty)

let all_arrays_kvs d ?(values = Value.M.empty) ind_gty val_gty =
  let size = Sequence.unfold ~init:() ~f:(fun () -> Some ((), ())) in
  let vals_ =
    Sequence.unfold_with size ~init:(Sequence.singleton Value.M.empty)
      ~f:(fun seq () ->
        let open Std.Sequence in
        let seq =
          let vals =
            let+ vals = seq and* indv = Interp.ty d ind_gty in
            match Value.M.find_opt indv values with
            | Some valv -> Sequence.singleton (Value.M.add indv valv vals)
            | None ->
                let+ valv = Interp.ty d val_gty in
                Value.M.add indv valv vals
          in
          Sequence.interleave vals
        in
        Yield { state = seq; value = seq })
  in
  let vals_ = Interp.Seq.of_seq @@ Sequence.interleave vals_ in
  let+ vals = vals_ and* other = Interp.Seq.of_seq @@ Interp.ty d val_gty in
  (vals, other)

let init_ty d =
  Interp.Register.node d (fun rec_node_values d n ->
      match get_array_gty_args d n with
      | ind_gty, val_gty ->
          let m =
            Opt.get_def Id.M.empty
              (Array_dom.get_array_vals d (Id.Array.get_id d n))
          in
          Some
            (let vo =
               let+ values =
                 map_to_values ~rec_node_values d ind_gty val_gty m
               in
               all_arrays_kvs d ~values ind_gty val_gty
             in
             let+ vals, other = Interp.Seq.concat vo in
             let av = ArrayModelVal.{ ind_gty; val_gty; vals; other } in
             ArrayModelVal.nodevalue (ArrayModelVal.index av))
      | exception Not_An_Array _ -> None);
  Interp.Register.ty d (fun d ty ->
      match ty with
      | { app = { builtin = Expr.Array; _ }; args = [ ind_gty; val_gty ]; _ } ->
          Some (all_arrays d ind_gty val_gty)
      | _ -> None)

let get_value env n = Opt.get_exn Impossible (Egraph.get_value env n)

let compute d g =
  match Ground.sem g with
  | { app = { builtin = Expr.Select; _ }; args; _ } -> (
      let a, k = IArray.extract2_exn args in
      let av =
        ArrayModelVal.value (ArrayModelVal.coerce_nodevalue (get_value d a))
      in
      let kv = get_value d k in
      match Value.M.find_opt kv av.vals with
      | Some fv -> `Some fv
      | None -> `Some av.other)
  | { app = { builtin = Expr.Store; _ }; args; _ } ->
      let a, k, v = IArray.extract3_exn args in
      let av =
        ArrayModelVal.value (ArrayModelVal.coerce_nodevalue (get_value d a))
      in
      let kv = get_value d k in
      let vv = get_value d v in
      `Some
        (ArrayModelVal.of_value { av with vals = Value.M.add kv vv av.vals })
  | { app = { builtin = Builtin.Array_diff; _ }; args; _ } -> (
      let a, b = IArray.extract2_exn args in
      let av =
        ArrayModelVal.value (ArrayModelVal.coerce_nodevalue (get_value d a))
      in
      let bv =
        ArrayModelVal.value (ArrayModelVal.coerce_nodevalue (get_value d b))
      in
      match
        Value.M.choose
          (Value.M.inter
             (fun _ v1 v2 -> if Value.equal v1 v2 then None else Some ())
             av.vals bv.vals)
      with
      | k, _ -> `Some k
      | exception Not_found -> `Uninterpreted)
  | _ -> `None

let init_check env =
  Interp.Register.check env (fun d t ->
      match compute d t with
      | `None -> NA
      | `Some r ->
          Interp.check_of_bool (Value.equal r (get_value d (Ground.node t)))
      | `Uninterpreted ->
          Interp.check_of_bool (Uninterp.On_uninterpreted_domain.check d t));
  Interp.Register.compute env (fun d t ->
      match compute d t with
      | `None -> NA
      | `Some v -> Value v
      | `Uninterpreted -> Uninterp.On_uninterpreted_domain.compute d t);
  Interp.Register.dependency_by_ground_term env (fun _ g ->
      let n = Ground.node g in
      match Ground.sem g with
      | { app = { builtin = Expr.Select; _ }; args; _ } ->
          let a, k = IArray.extract2_exn args in
          Some [ (k, a); (n, a) ]
      | { app = { builtin = Expr.Store; _ }; args; _ } ->
          let a, k, v = IArray.extract3_exn args in
          Some [ (k, n); (a, n); (v, n); (k, a); (v, a) ]
      | { app = { builtin = Builtin.Array_diff; _ }; args; _ } ->
          let a, b = IArray.extract2_exn args in
          Some [ (n, a); (n, b) ]
      | _ -> None)

let propagate_value d g =
  let f d g =
    match compute d g with
    | `None -> raise Impossible
    | `Some v -> Egraph.set_value d (Ground.node g) v
    | `Uninterpreted ->
        Colibri2_theories_quantifiers.Uninterp.On_uninterpreted_domain.propagate
          d g
  in
  Interp.WatchArgs.create d f g

let mk_array_value ind_gty val_gty ns_vals v =
  let av = ArrayModelVal.value (ArrayModelVal.coerce_nodevalue v) in
  assert (Ground.Ty.equal av.val_gty val_gty);
  assert (Ground.Ty.equal av.ind_gty ind_gty);
  ArrayModelVal.of_value
    {
      ind_gty;
      val_gty;
      vals =
        Value.M.union
          (fun _ v1 v2 ->
            assert (Value.equal v1 v2);
            Some v1)
          ns_vals av.vals;
      other = av.other;
    }
