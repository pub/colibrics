(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_core
open Colibri2_popop_lib
open Popop_stdlib

type map_info = {
  bi_ind_ty : Ground.Ty.t;
  bi_val_ty : Ground.Ty.t;
  a_val_ty : Ground.Ty.t;
  f_arity : DInt.t;
}
[@@deriving eq, ord, hash, show]

module AVal = struct
  module T = struct
    type t = map_info Ground.M.t (* parents that are maps *)
    [@@deriving eq, ord, hash, show]
  end

  include T
  include MkDatatype (T)
end

module D = struct
  type t = AVal.t [@@deriving eq, show]

  let is_singleton _ _ _ = None

  let key =
    Dom.Kind.create
      (module struct
        type nonrec t = t

        let name = "Array.dom"
      end)

  let inter _ mp1 mp2 = Some (Ground.M.set_union mp1 mp2)
  let wakeup_threshold = None
end

include D
include Dom.Lattice (D)

let new_read_hooks = Datastructure.Push.create Fmt.nop "Array.new_read"
let register_hook_new_read d f = Datastructure.Push.push new_read_hooks d f

let new_read env n =
  match Egraph.get_dom env key n with
  | Some map_parents ->
      Datastructure.Push.iter new_read_hooks env ~f:(fun hook ->
          hook env n map_parents)
  | None -> ()

let new_map_parent_hooks =
  Datastructure.Push.create Fmt.nop "Array.new_map_parent"

let register_hook_new_map_parent d f =
  Datastructure.Push.push new_map_parent_hooks d f

let add_map_parent env n gt { bi_ind_ty; bi_val_ty; a_val_ty; f_arity } =
  match Egraph.get_dom env key n with
  | Some map_parents ->
      set_dom env n
        (Ground.M.add gt
           { bi_ind_ty; bi_val_ty; a_val_ty; f_arity }
           map_parents);
      Datastructure.Push.iter new_map_parent_hooks env ~f:(fun hook ->
          hook env gt
            (match Array_dom.get_array_vals env (Id.Array.get_id env n) with
            | None -> Node.S.empty
            | Some m ->
                Id.M.fold
                  (fun k _ acc -> Node.S.add (Id.get env k) acc)
                  m Node.S.empty)
            { bi_ind_ty; bi_val_ty; a_val_ty; f_arity })
  | None ->
      set_dom env n
        (Ground.M.singleton gt { bi_ind_ty; bi_val_ty; a_val_ty; f_arity })
