(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

val add_kv : Egraph.wt -> Id.t -> ?b:Id.t -> Id.t -> Node.t -> unit
val eq_arrays_norm : Egraph.wt -> Id.t -> Id.t -> unit
val eq_indices_norm : Egraph.wt -> Id.t -> Id.t -> unit
val on_new_content : Egraph.wt -> Id.t -> Id.t -> unit
val get_array_vals : Egraph.wt -> Id.t -> (Node.t * Id.S.t) Id.M.t option
