(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_core
open Colibri2_popop_lib
open Common

(* a = store (b, i, v);  v = select (a, i)  *)

(* store array's default values
   before merging a value with the default value, check that it is indeed
   different from all the indices already present in the table
*)

(* a -> (i -> (v, b set)) *)
module AIV = Id.MkIHT (struct
  type t = (Node.t * Id.S.t) Id.M.t

  let pp =
    Id.M.pp (fun fmt (v, s) -> Fmt.pf fmt "(%a, {%a})" Node.pp v Id.S.pp s)

  let name = "ArrayIndsToValues"
end)

(* i -> (a -> v) *)
module Inds = Id.MkIHT (struct
  type t = Node.t Id.M.t [@@deriving show]

  let name = "ArrayIndices"
end)

(* b -> (a -> i set) *)
module KNeighbours = Id.MkIHT (struct
  type t = Id.S.t Id.M.t

  let pp = Id.M.pp (fun fmt s -> Fmt.pf fmt "{%a}" Id.S.pp s)
  let name = "ArrayKNeighbours"
end)

module NSContent = struct
  module HT = Id.MkIHT (struct
    let name = "ArrayNSContent"

    type t = Colibri2_popop_lib.Popop_stdlib.DBool.t [@@deriving show]
  end)

  let eq_nseqs_norm env kid rid =
    match HT.find_opt env rid with
    | None -> ()
    | Some rb ->
        HT.remove env rid;
        HT.change env kid ~f:(function
          | None -> Some rb
          | Some kb -> Some (rb || kb))

  let on_new_content env aid _ = HT.set env aid true
end

let add_kv env a ?b i v =
  Debug.dprintf8 debug "Array_dom.add %a (%a) %a %a" Id.pp a (Fmt.option Id.pp)
    b Id.pp i Node.pp v;
  let add_b_opt ?(s = Id.S.empty) b = Opt.fold (fun s b -> Id.S.add b s) s b in
  AIV.change env a ~f:(function
    | None -> Some (Id.M.singleton i (v, add_b_opt b))
    | Some m ->
        Some
          (Id.M.change
             (function
               | None -> Some (v, add_b_opt b)
               | Some (v', s) ->
                   Egraph.merge env v v';
                   Some (v', add_b_opt ~s b))
             i m));
  Inds.change env i ~f:(function
    | None -> Some (Id.M.singleton a v)
    | Some m ->
        Some
          (Id.M.change
             (function
               | None -> Some v
               | Some v' ->
                   Egraph.merge env v v';
                   (* should be useless as it's done while changing AIV *)
                   Some v')
             a m));
  Opt.iter
    (fun b ->
      KNeighbours.change env b ~f:(function
        | None -> Some (Id.M.singleton a (Id.S.singleton i))
        | Some m ->
            Some
              (Id.M.change
                 (function
                   | None -> Some (Id.S.singleton i)
                   | Some s -> Some (Id.S.add i s))
                 a m)))
    b;
  Colibri2_theories_utils.Nsa_content.add_arr_kv env a (Id.get env i) v

let upd_kneighbours env kid rid nsid s =
  Debug.dprintf8 debug "upd_kneighbours %a (%a <- %a) {%a}" Id.pp nsid Id.pp rid
    Id.pp kid Id.S.pp s;
  Id.S.iter
    (fun knid ->
      KNeighbours.change env knid ~f:(function
        | None -> assert false
        | Some m -> (
            match Id.M.find_remove nsid m with
            | _, None -> assert false
            | m', Some kns ->
                assert (Id.S.mem rid kns);
                Some (Id.M.add nsid (Id.S.add kid (Id.S.remove rid kns)) m'))))
    s

let eq_indices_norm env kid rid =
  Debug.dprintf4 debug "Array_dom.eq_indices_norm %a <- %a" Id.pp rid Id.pp kid;
  match Inds.find_opt env rid with
  | None -> ()
  | Some rm ->
      let km =
        match Inds.find_opt env kid with None -> Id.M.empty | Some m -> m
      in
      let nm =
        Id.M.fold
          (fun rnsid rv acc ->
            AIV.change env rnsid ~f:(function
              | None -> assert false
              | Some im -> (
                  match (Id.M.find_opt kid im, Id.M.find_opt rid im) with
                  | None, None -> assert false
                  | Some _, None -> None
                  | None, Some (v, s) ->
                      upd_kneighbours env kid rid rnsid s;
                      Some (Id.M.add kid (v, s) (Id.M.remove rid im))
                  | Some (v1, s1), Some (v2, s2) ->
                      assert (
                        Egraph.is_equal env rv v1 || Egraph.is_equal env rv v2);
                      upd_kneighbours env kid rid rnsid s2;
                      Egraph.merge env v1 v2;
                      Some
                        (Id.M.add kid
                           (rv, Id.S.union s1 s2)
                           (Id.M.remove rid im))));
            match Id.M.find_opt rnsid km with
            | None -> Id.M.add rnsid rv acc
            | Some v' ->
                Egraph.merge env rv v';
                Id.M.add rnsid rv acc)
          rm km
      in
      Inds.remove env rid;
      Inds.set env kid nm

let eq_arrays_norm env kid rid =
  Debug.dprintf4 debug "Array_dom.eq_arrays_norm %a <- %a" Id.pp rid Id.pp kid;
  NSContent.eq_nseqs_norm env kid rid;
  (match KNeighbours.find_opt env rid with
  | None -> ()
  | Some rkn ->
      let kkn =
        match KNeighbours.find_opt env kid with
        | None -> Id.M.empty
        | Some m -> m
      in
      let rkn =
        match Id.M.find_remove kid rkn with
        | _, None -> rkn
        | rkn', Some s ->
            AIV.change env kid ~f:(function
              | None -> assert false
              | Some m ->
                  let m' =
                    Id.S.fold
                      (fun iid acc ->
                        Id.M.change
                          (function
                            | None -> assert false
                            | Some (v, s) -> Some (v, Id.S.remove rid s))
                          iid acc)
                      s m
                  in
                  Some m');
            rkn'
      in
      let nkn =
        Id.M.fold
          (fun kneighbour kset acc ->
            let m = AIV.find env kneighbour in
            let m' =
              Id.S.fold
                (fun k acc ->
                  let v, s = Id.M.find k m in
                  let s' =
                    if Id.equal kneighbour kid then (
                      AIV.change env kneighbour ~f:(function
                        | None -> assert false
                        | Some m ->
                            Some
                              (Id.M.change
                                 (function
                                   | None -> assert false
                                   | Some (v, s) -> (
                                       match Id.M.find_remove rid s with
                                       | _, None -> assert false
                                       | s', Some () -> Some (v, s')))
                                 k m));
                      Id.S.remove rid s)
                    else Id.S.add kid (Id.S.remove rid s)
                  in
                  Id.M.add k (v, s') acc)
                kset m
            in
            AIV.set env kneighbour m';
            Id.M.change
              (function
                | None -> Some kset | Some s -> Some (Id.S.union kset s))
              kneighbour acc)
          rkn kkn
      in
      KNeighbours.remove env rid;
      KNeighbours.set env kid nkn);
  match AIV.find_opt env rid with
  | None -> ()
  | Some rm ->
      let km =
        match AIV.find_opt env kid with None -> Id.M.empty | Some m -> m
      in
      let nm =
        Id.M.fold
          (fun iid (v, s) acc ->
            let s' =
              Id.S.fold
                (fun nid acc ->
                  let nknm = KNeighbours.find env nid in
                  let nknm', acc' =
                    if Id.equal nid kid then
                      (Id.M.remove rid nknm, Id.S.remove nid acc)
                    else
                      ( (match Id.M.find_remove rid nknm with
                        | _, None ->
                            (* means that it was already removed *)
                            nknm
                        | nknm', Some s ->
                            Id.M.change
                              (function
                                | None -> Some s
                                | Some s' -> Some (Id.S.union s s'))
                              kid nknm'),
                        Id.S.add nid acc )
                  in
                  KNeighbours.set env nid nknm';
                  acc')
                s (Id.M.remove kid s)
            in
            Inds.change env iid ~f:(function
              | None -> assert false
              | Some im -> (
                  match (Id.M.find_opt kid im, Id.M.find_opt rid im) with
                  | None, None -> assert false
                  | Some _, None -> None
                  | None, Some m2 -> Some (Id.M.add kid m2 (Id.M.remove rid im))
                  | Some v1, Some v2 ->
                      assert (
                        Egraph.is_equal env v v1 || Egraph.is_equal env v v2);
                      Egraph.merge env v1 v2;
                      Some (Id.M.add kid v (Id.M.remove rid im))));
            match Id.M.find_opt iid km with
            | None -> Id.M.add iid (v, s') acc
            | Some (v', s'') ->
                Egraph.merge env v v';
                Id.M.add iid (v, Id.S.union s'' s') acc)
          rm km
      in
      AIV.remove env rid;
      AIV.set env kid nm

let on_new_content = NSContent.on_new_content
let get_array_vals = AIV.find_opt
