(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

type map_info = {
  bi_ind_ty : Ground.Ty.t;
  bi_val_ty : Ground.Ty.t;
  a_val_ty : Ground.Ty.t;
  f_arity : int;
}

type t = map_info Ground.M.t

val equal : t -> t -> bool
val pp : Format.formatter -> t -> unit
val show : t -> string
val is_singleton : 'a -> 'b -> _ -> 'c option
val key : t Dom.Kind.t
val inter : 'a -> t -> t -> t option
val set_dom : Egraph.wt -> Node.t -> t -> unit
val upd_dom : Egraph.wt -> Node.t -> t -> unit

val register_hook_new_read :
  Egraph.wt -> (Egraph.wt -> Node.t -> map_info Ground.M.t -> unit) -> unit

val new_read : Egraph.wt -> Node.t -> unit

val register_hook_new_map_parent :
  Egraph.wt -> (Egraph.wt -> Ground.t -> Node.S.t -> map_info -> unit) -> unit

val add_map_parent : Egraph.wt -> Node.t -> Ground.t -> map_info -> unit
