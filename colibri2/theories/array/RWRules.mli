(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_theories_quantifiers

val check_gty_num_size : Egraph.rw Egraph.t -> Ground.Ty.t -> bool
val adown_pattern : Pattern.t
val adown_run : Egraph.wt -> Ground.Subst.t -> unit
val aup_pattern : Pattern.t
val aup_run : Egraph.wt -> Ground.Subst.t -> unit
val raup_pattern : Pattern.t
val raup_run : Egraph.wt -> Ground.Subst.t -> unit
val const_read_pattern : Pattern.t
val const_read_run : Egraph.wt -> Ground.Subst.t -> unit
val eq_pattern : Pattern.t
val eq_run : Egraph.wt -> Ground.Subst.t -> unit
val apply_res_ext_1_1 : Egraph.wt -> Node.S.t -> unit
val apply_res_ext_1_2 : Egraph.wt -> Node.S.t -> unit
val apply_res_ext_2_1 : Egraph.wt -> Ground.Ty.t -> Node.t -> unit
val apply_res_ext_2_2 : Egraph.wt -> Ground.Ty.t -> Node.t -> unit
val new_array : Egraph.wt -> Ground.Ty.t -> Ground.Ty.t -> Ground.t -> unit

val map_adowm :
  Expr.term ->
  Expr.term ->
  Expr.term list ->
  Pattern.t * (Egraph.wt -> Ground.Subst.t -> unit)

val add_array_read_hook :
  Egraph.wt -> Node.t -> Array_maps_dom.map_info Ground.M.t -> unit

val add_array_map_hook :
  Egraph.wt -> Ground.t -> Node.S.t -> Array_maps_dom.map_info -> unit

val new_map : Egraph.rw Egraph.t -> Ground.t -> unit
val apply_blast_rule : Egraph.wt -> Node.t -> Ground.Ty.t -> Ground.Ty.t -> unit
val eq_arrays_norm : Egraph.wt -> Node.t * Id.t -> Node.t * Id.t -> bool -> unit

val eq_indices_norm :
  Egraph.wt -> Node.t * Id.t -> Node.t * Id.t -> bool -> unit
