(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_core
open Colibri2_popop_lib
open Popop_stdlib

let no_res_ext =
  Options.register ~pp:Fmt.bool "Array.no-res-ext"
    Cmdliner.Arg.(
      value & flag
      & info [ "array-no-res-ext" ]
          ~doc:"Don't restrict the array extensionality rule")

let no_res_aup =
  Options.register ~pp:Fmt.bool "Array.res-aup"
    Cmdliner.Arg.(
      value & flag
      & info [ "array-no-res-aup" ]
          ~doc:"Don't restrict the array's ⇑ (select over store) rule")

let extended_comb =
  Options.register ~pp:Fmt.bool "Array.res-comb"
    Cmdliner.Arg.(
      value & flag & info [ "array-ext-comb" ] ~doc:"Extended combinators")

let blast_rule =
  Options.register ~pp:Fmt.bool "Array.blast-rule"
    Cmdliner.Arg.(
      value & flag
      & info [ "array-blast-rule" ]
          ~doc:"Use the array blast rule when it is suitable")

let default_values =
  Options.register ~pp:Fmt.bool "Array.def-values"
    Cmdliner.Arg.(
      value & flag
      & info [ "array-def-values" ]
          ~doc:"Use inference rules for default values")

let use_choice_ext =
  Options.register ~pp:Fmt.bool "Array.ext-choice"
    Cmdliner.Arg.(
      value & flag
      & info [ "array-ext-cho" ]
          ~doc:"Use \"Choice\" when applying unrestricted extensionality")

let debug =
  Debug.register_flag ~desc:"Debugging messages of the array theory" "Array"

(* exceptions *)
exception Not_An_Array of Node.t
exception Not_An_Array_gty of Ground.Ty.t
exception Not_An_Array_ty of Expr.ty
exception Type_Not_Set of Node.t

let () =
  Printexc.register_printer (function
    | Not_An_Array n -> Some (Fmt.str "%a is not an array!" Node.pp n)
    | Not_An_Array_gty gty ->
        Some (Fmt.str "%a is not an array ground type!" Ground.Ty.pp gty)
    | Not_An_Array_ty ty ->
        Some (Fmt.str "%a is not an array type!" Expr.Ty.pp ty)
    | Type_Not_Set n ->
        Some (Fmt.str "the type of the node %a was not set!" Node.pp n)
    | _ -> None)

module STV = struct
  let ind_ty_var = Expr.Ty.Var.mk "ind_ty"
  let val_ty_var = Expr.Ty.Var.mk "val_ty"
  let alpha_ty_var = Expr.Ty.Var.mk "alpha"
  let a_ty_var = Expr.Ty.Var.mk "a"
  let b_ty_var = Expr.Ty.Var.mk "b"
  let c_ty_var = Expr.Ty.Var.mk "c"
  let ind_ty = Expr.Ty.of_var ind_ty_var
  let val_ty = Expr.Ty.of_var val_ty_var
  let a_ty = Expr.Ty.of_var a_ty_var
  let b_ty = Expr.Ty.of_var b_ty_var
  let c_ty = Expr.Ty.of_var c_ty_var
  let alpha_ty = Expr.Ty.of_var alpha_ty_var
  let array_ty = Expr.Ty.array ind_ty val_ty
  let array_ty_ab = Expr.Ty.array a_ty b_ty
  let array_ty_ac = Expr.Ty.array a_ty c_ty
  let array_ty_alpha = Expr.Ty.array ind_ty alpha_ty
  let term_of_var = Expr.Term.of_var
  let mk_index_var name = Expr.Term.Var.mk name ind_ty
  let mk_value_var name = Expr.Term.Var.mk name val_ty
  let mk_array_var name = Expr.Term.Var.mk name array_ty
  let vi = mk_index_var "i"
  let vj = mk_index_var "j"
  let vk = mk_index_var "k"
  let vv = mk_value_var "v"
  let ti = term_of_var vi
  let tj = term_of_var vj
  let tk = term_of_var vk
  let tv = term_of_var vv
  let va = mk_array_var "a"
  let vb = mk_array_var "b"
  let ta = term_of_var va
  let tb = term_of_var vb
end

let replicate n v = List.init n (fun _ -> v)
let mk_store_term = Expr.Term.Array.store
let mk_select_term = Expr.Term.Array.select
let apply_cst = Expr.Term.apply_cst

let array_ty_args : Expr.ty -> Expr.ty * Expr.ty = function
  | { ty_descr = TyApp ({ builtin = Expr.Array; _ }, [ ind_ty; val_ty ]); _ } ->
      (ind_ty, val_ty)
  | ty -> raise (Not_An_Array_ty ty)

let array_gty_args : Ground.Ty.t -> Ground.Ty.t * Ground.Ty.t = function
  | { app = { builtin = Expr.Array; _ }; args = [ ind_gty; val_gty ] } ->
      (ind_gty, val_gty)
  | ty -> raise (Not_An_Array_gty ty)

let get_array_gty env n =
  try
    List.find
      (function
        | Ground.Ty.{ app = { builtin = Expr.Array; _ }; _ } -> true
        | _ -> false)
      (Ground.Ty.S.elements (Ground.tys env n))
  with Not_found -> raise (Not_An_Array n)

let get_array_gty_args env n = array_gty_args (get_array_gty env n)

let add_array_gty env n ind_gty val_gty =
  Ground.add_ty env n (Ground.Ty.array ind_gty val_gty)

module Builtin = struct
  (** Additional array Builtins *)
  type _ Expr.t +=
    | Array_diff
          (** [Array_diff: 'a 'b. ('a, 'b) Array -> ('a, 'b) Array -> 'a] *)
    | Array_const  (** [Array_const: 'b. 'b -> (ind_ty, 'b) Array] *)
    | Array_map
          (** [Array_map: 'a 'b 'c. (('b -> ... -> 'b -> 'c) -> ('a, 'b) Array
               -> ... -> ('a, 'b) Array)-> ('a, 'c) Array] *)
    | Array_default_index
          (** [Array_default_index: 'a 'b. ('a, 'b) Array -> 'a] *)
    | Array_default_value
          (** [Array_default_value: 'a 'b. ('a, 'b) Array -> 'b] *)

  let array_diff : Dolmen_std.Expr.term_cst =
    Expr.Id.mk ~name:"colibri2_array_diff" ~builtin:Array_diff
      (Dolmen_std.Path.global "colibri2_array_diff")
      (Expr.Ty.pi
         [ STV.a_ty_var; STV.b_ty_var ]
         (Expr.Ty.arrow [ STV.array_ty_ab; STV.array_ty_ab ] STV.a_ty))

  let array_const : Dolmen_std.Expr.term_cst =
    Expr.Id.mk ~name:"colibri2_array_const" ~builtin:Array_const
      (Dolmen_std.Path.global "colibri2_array_const")
      (Expr.Ty.pi
         [ STV.a_ty_var; STV.b_ty_var ]
         (Expr.Ty.arrow [ STV.b_ty ] STV.array_ty_ab))

  let array_map : int -> Dolmen_std.Expr.term_cst =
    let cache = DInt.H.create 13 in
    let get_ty i =
      match DInt.H.find cache i with
      | ty -> ty
      | exception Not_found ->
          let ty =
            Expr.Ty.arrow
              (Expr.Ty.arrow (replicate i STV.b_ty) STV.c_ty
              :: replicate i STV.array_ty_ab)
              STV.array_ty_ac
          in
          DInt.H.add cache i ty;
          ty
    in
    fun i ->
      Expr.Id.mk ~name:"colibri2_array_map" ~builtin:Array_map
        (Dolmen_std.Path.global "colibri2_array_map")
        (Expr.Ty.pi [ STV.a_ty_var; STV.b_ty_var; STV.c_ty_var ] (get_ty i))

  let array_default_index : Dolmen_std.Expr.term_cst =
    Expr.Id.mk ~name:"colibri2_array_default_index" ~builtin:Array_default_index
      (Dolmen_std.Path.global "colibri2_array_default_index")
      (Expr.Ty.pi
         [ STV.a_ty_var; STV.b_ty_var ]
         (Expr.Ty.arrow [ STV.array_ty_ab ] STV.a_ty))

  let array_default_value : Dolmen_std.Expr.term_cst =
    Expr.Id.mk ~name:"colibri2_array_default_value" ~builtin:Array_default_value
      (Dolmen_std.Path.global "colibri2_array_default_value")
      (Expr.Ty.pi
         [ STV.a_ty_var; STV.b_ty_var ]
         (Expr.Ty.arrow [ STV.array_ty_ab ] STV.b_ty))

  let apply_array_diff a b =
    let ind_ty, val_ty = array_ty_args a.Expr.term_ty in
    apply_cst array_diff [ ind_ty; val_ty ] [ a; b ]

  let apply_array_const v =
    apply_cst array_const
      [ Expr.Ty.of_var (Expr.Ty.Var.wildcard ()); v.Expr.term_ty ]
      [ v ]

  let apply_array_def_index a =
    let ind_ty, val_ty = array_ty_args a.Expr.term_ty in
    apply_cst array_default_index [ ind_ty; val_ty ] [ a ]

  let apply_array_def_value a =
    let ind_ty, val_ty = array_ty_args a.Expr.term_ty in
    apply_cst array_default_value [ ind_ty; val_ty ] [ a ]

  let apply_array_map f_arity f_term bitl =
    match (bitl, f_term) with
    | h :: _, Expr.{ term_ty = { ty_descr = Arrow (_, ret_ty); _ }; _ } ->
        let bi_ind_ty, bi_val_ty = array_ty_args h.Expr.term_ty in
        apply_cst (array_map f_arity)
          [ bi_ind_ty; bi_val_ty; ret_ty ]
          (f_term :: bitl)
    | _, _ ->
        failwith "array_map needs to be applied to a function and n > 0 arrays"

  let () =
    let app1 env s f =
      Dolmen_loop.Typer.T.builtin_term
        (Dolmen_type.Base.term_app1
           (module Dolmen_loop.Typer.T)
           env s
           (fun a ->
             let ind_ty, val_ty = array_ty_args a.term_ty in
             apply_cst f [ ind_ty; val_ty ] [ a ]))
    in
    Expr.add_builtins (fun env s ->
        match s with
        | Dolmen_loop.Typer.T.Id
            { ns = Term; name = Simple "colibri2_array_diff" } ->
            Dolmen_loop.Typer.T.builtin_term
              (Dolmen_type.Base.term_app2
                 (module Dolmen_loop.Typer.T)
                 env s
                 (fun a b ->
                   let ind_ty, val_ty = array_ty_args a.term_ty in
                   apply_cst array_diff [ ind_ty; val_ty ] [ a; b ]))
        | Dolmen_loop.Typer.T.Id
            { ns = Term; name = Simple "colibri2_array_const" } ->
            Dolmen_loop.Typer.T.builtin_term
              (Dolmen_type.Base.term_app1
                 (module Dolmen_loop.Typer.T)
                 env s
                 (fun a -> apply_cst array_const [ a.term_ty ] [ a ]))
        | Dolmen_loop.Typer.T.Id
            { ns = Term; name = Simple "colibri2_array_default_index" } ->
            app1 env s array_default_index
        | Dolmen_loop.Typer.T.Id
            { ns = Term; name = Simple "colibri2_array_default_value" } ->
            app1 env s array_default_value
        | Dolmen_loop.Typer.T.Id
            { ns = Term; name = Simple "colibri2_array_map" } ->
            Dolmen_loop.Typer.T.builtin_term
              (Dolmen_type.Base.term_app_list
                 (module Dolmen_loop.Typer.T)
                 env s
                 (function
                   | f_term :: t -> apply_array_map (List.length t) f_term t
                   | _ ->
                       failwith
                         "array_map needs to be applied to a function and n > \
                          0 arrays"))
        | _ -> `Not_found)
end

let mk_distinct env l gty =
  Ground.apply' env (Expr.Term.Const.distinct (List.length l)) [ gty ] l

let mk_select env a k ind_gty val_gty =
  Ground.apply' env Expr.Term.Const.Array.select [ ind_gty; val_gty ] [ a; k ]

let mk_store env a k v ind_gty val_gty =
  Ground.apply' env Expr.Term.Const.Array.store [ ind_gty; val_gty ] [ a; k; v ]

let mk_array_diff env a b ind_gty val_gty =
  let diff_ab =
    Ground.apply' env Builtin.array_diff [ ind_gty; val_gty ] [ a; b ]
  in
  if Egraph.is_registered env diff_ab then diff_ab
  else
    let diff_ba =
      Ground.apply' env Builtin.array_diff [ ind_gty; val_gty ] [ b; a ]
    in
    if Egraph.is_registered env diff_ba then diff_ba
    else (
      Egraph.register env diff_ab;
      diff_ab)

let mk_distinct_arrays env a b ind_gty val_gty =
  let diffn = mk_array_diff env a b ind_gty val_gty in
  mk_distinct env
    [
      mk_select env a diffn ind_gty val_gty;
      mk_select env b diffn ind_gty val_gty;
    ]
    (Ground.Ty.array ind_gty val_gty)

let mk_array_const env v val_gty =
  let wc_ty =
    Ground.Ty.convert Ground.Subst.empty.ty
      (Expr.Ty.of_var (Expr.Ty.Var.wildcard ()))
  in
  Ground.apply' env Builtin.array_const [ wc_ty; val_gty ] [ v ]

let distinct_arrays_term a b =
  let diff = Builtin.apply_array_diff a b in
  Expr.Term.distinct [ mk_select_term a diff; mk_select_term b diff ]

module SHT (K : sig
  include Datatype

  val sort : t -> t
  val pp : t Pp.pp
end) (V : sig
  type t

  val name : string
  val pp : t Pp.pp
end) : Colibri2_theories_utils.Ident.HTS with type key = K.t and type t = V.t =
struct
  type key = K.t
  type t = V.t

  module HT = Datastructure.Hashtbl (K)

  let db = HT.create V.pp V.name
  let remove env k = HT.remove db env (K.sort k)
  let set env k = HT.set db env (K.sort k)
  let find env k = HT.find db env (K.sort k)
  let find_opt env k = HT.find_opt db env (K.sort k)
  let change ~f env k = HT.change f db env (K.sort k)
  let iter = HT.iter db
  let fold f env acc = HT.fold f db env acc

  let pp fmt env =
    Fmt.pf fmt "%s:[%a]" V.name
      (fun fmt () ->
        iter env ~f:(fun i v -> Fmt.pf fmt "(%a -> %a);@ " K.pp i V.pp v))
      ()
end

module I4 = struct
  module T = struct
    type t = Id.t * Id.t * Id.t * Id.t * Ground.Ty.t * Ground.Ty.t
    [@@deriving eq, ord, hash, show]
  end

  include T
  include MkDatatype (T)
end

module I3 = struct
  module T = struct
    type t = DBool.t * Id.t * Id.t * Id.t * Ground.Ty.t * Ground.Ty.t
    [@@deriving eq, ord, hash, show]
  end

  include T
  include MkDatatype (T)
end

module I2 = struct
  module T = struct
    type t = Id.t * Id.t * Ground.Ty.t * Ground.Ty.t
    [@@deriving eq, ord, hash, show]
  end

  include T
  include MkDatatype (T)
end

module I1 = struct
  module T = struct
    type t = DBool.t * Id.t * Ground.Ty.t * Ground.Ty.t
    [@@deriving eq, ord, hash, show]
  end

  include T
  include MkDatatype (T)
end

(* (id1,id2,id3,id4,ty1,ty2) *)
(* (id1,id2,ty1,ty2) *)

(* id1 -> (id2,id3,id4,ty1,ty2) *)
(* id1 -> (id2,ty1,ty2) *)

let split_cond env ?(cons = fun _ -> assert false)
    ?(alt = fun _ -> assert false) ?(none = fun _ -> assert false) cond =
  match Boolean.is env cond with
  | Some true -> cons env
  | Some false -> alt env
  | None -> none env

let new_decision env cond print_cho ?(cons = fun _ -> ()) ?(alt = fun _ -> ())
    decs =
  split_cond env cond ~cons ~alt ~none:(fun env ->
      Choice.register_global env
        {
          print_cho = (fun fmt () -> Fmt.pf fmt "%s" print_cho);
          prio = 1;
          key = None;
          choice =
            Choice.(
              fun env ->
                match Boolean.is env cond with
                | Some true ->
                    cons env;
                    DecNo
                | Some false ->
                    alt env;
                    DecNo
                | None -> decs env);
        })

let bool_dec ?(cons = fun _ -> ()) ?(alt = fun _ -> ()) cond =
  Choice.DecTodo
    [
      (fun env ->
        Boolean.set_true env cond;
        cons env);
      (fun env ->
        Boolean.set_false env cond;
        alt env);
    ]
