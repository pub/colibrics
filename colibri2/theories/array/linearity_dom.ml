(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_core
open Colibri2_popop_lib
open Popop_stdlib

module LVal = struct
  module T = struct
    type t = NonLinear | Linear of Node.t | Empty [@@deriving eq, ord, hash]

    let pp fmt = function
      | NonLinear -> Fmt.pf fmt "NonLinear"
      | Linear n -> Fmt.pf fmt "Linear(%a)" Node.pp n
      | Empty -> Fmt.pf fmt "Empty"
  end

  include T
  include MkDatatype (T)
end

module D = struct
  type t = LVal.t = NonLinear | Linear of Node.t | Empty [@@deriving eq]

  let is_singleton _ _ _ = None

  let key =
    Dom.Kind.create
      (module struct
        type nonrec t = t

        let name = "Array.linearity"
      end)

  let inter d d1 d2 =
    match (d1, d2) with
    | NonLinear, NonLinear | NonLinear, Empty | Empty, NonLinear ->
        Some NonLinear
    | Linear n, Empty | Empty, Linear n -> Some (Linear n)
    | Linear n1, Linear n2 ->
        if Egraph.is_equal d n1 n2 then
          (* does this ever happen? *)
          Some (Linear n1)
        else (
          Egraph.set_dom d key n1 NonLinear;
          Egraph.set_dom d key n2 NonLinear;
          Some NonLinear)
    | NonLinear, Linear n | Linear n, NonLinear ->
        Egraph.set_dom d key n NonLinear;
        Some NonLinear
    | Empty, Empty -> Some Empty

  let pp fmt = function
    | NonLinear -> Fmt.pf fmt "NonLinear"
    | Linear n -> Fmt.pf fmt "Linear(%a)" Node.pp n
    | Empty -> Fmt.pf fmt "Empty"

  let wakeup_threshold = None
end

include D
include Dom.Lattice (D)
