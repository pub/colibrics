(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

type t = IsForeign

val equal : t -> t -> bool
val is_singleton : Egraph.wt -> _ -> t -> Colibri2_core.Value.t option
val key : t Dom.Kind.t
val inter : Egraph.wt -> t -> t -> t option
val pp : t Fmt.t

val register_hook_new_foreign_array :
  Egraph.wt -> (Egraph.wt -> Ground.Ty.t -> Node.t -> unit) -> unit

val set_dom : Egraph.wt -> Ground.Ty.t -> Node.t -> t -> unit
val upd_dom : Egraph.wt -> Ground.Ty.t -> Node.t -> t -> unit
