(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_core
open Colibri2_popop_lib
open Popop_stdlib
open Common
module IdP = Id.IdP
module PM = IdP.M

(* TODO: only store for reloc representatives
   - shared slices modulo relocation
   - full switch to shared slices/ignore relations *)

(* add (nonrepr -> concat)
   add (slices -> concat)
   add (updates -> concat) *)
module T = struct
  type t = {
    concat : (Id.t * DBool.t) Id.M.t;
    slices : Id.S.t;
    updates : (Id.S.t * DBool.t) Id.M.t;
  }
  [@@deriving hash]
  (* TODO: updates:
       - should also simplify the cases in which there is a const.
       - should update be rewritten as concats?
       - when id1 = update(id2, id3) = update(id4, id3)
         add that
         if slice(id2, first(id3), last(id3)) =
            slice(id4, first(id3), last(id3))
         then id2 = id4
  *)

  let pp_im = Id.M.pp (fun fmt (id, b) -> Fmt.pf fmt "(%a, %b)" Id.pp id b)
  let pp_sm = Id.M.pp (fun fmt (s, b) -> Fmt.pf fmt "({%a}, %b)" Id.S.pp s b)

  let pp fmt { concat; slices; updates } =
    Fmt.pf fmt "{concat: %a; slices: %a; updates: %a}"
      (Id.M.pp (fun fmt (id, b) -> Fmt.pf fmt "(%a, %b)" Id.pp id b))
      concat Id.S.pp slices
      (Id.M.pp (fun fmt (s, b) -> Fmt.pf fmt "({%a}, %b)" Id.S.pp s b))
      updates
end

include T

module HT = Id.MkIHT (struct
  let name = "NSDomHT"

  include T
end)

module Locs = Id.MkIHT (struct
  let name = "NSDomHTLocs"

  type t = Id.S.t * Id.S.t * Id.S.t [@@deriving hash]

  let pp fmt (s1, s2, s3) =
    Fmt.pf fmt "(%a | %a | %a)" Id.S.pp s1 Id.S.pp s2 Id.S.pp s3
end)

module SharedSlices = struct
  (* id1 -> ((fst, lst) -> id2) *)
  (* add (nonrepr -> (fst', lst') -> id2) *)
  module Bounds2NS = Id.MkIHT (struct
    let name = "Bounds2NS"

    type t = Id.t PM.t [@@deriving show]
  end)

  (* id2 -> id1 set | nonrepr set *)
  module Locs = Id.MkIHT (struct
    let name = "Locs"

    type t = Id.S.t [@@deriving show]
  end)

  let add_loc env a b =
    Debug.dprintf4 debug "Relations.SharedSlices.add_loc %a %a" Id.pp a Id.pp b;
    Locs.change env a ~f:(function
      | None -> Some (Id.S.singleton b)
      | Some s -> Some (Id.S.add b s))

  let add_shared_slice env a b =
    let bounds =
      match Bounds.get_bounds env b with
      | Some bounds -> bounds
      | None -> assert false
    in
    Debug.dprintf6 debug "Relations.SharedSlices.add_shared_slice %a %a %a"
      Id.pp a (Id.pp_nid env) b IdP.pp bounds;
    assert (not (Id.equal a b));
    match Bounds2NS.find_opt env a with
    | None ->
        add_loc env b a;
        let m = PM.singleton bounds b in
        Bounds2NS.set env a m
    | Some m -> (
        match PM.find_opt bounds m with
        | None ->
            add_loc env b a;
            Bounds2NS.set env a (PM.add bounds b m)
        | Some b' ->
            Debug.dprintf2 debug
              "Relations.SharedSlices.add_shared_slice found %a" (Id.pp_nid env)
              b';
            Egraph.merge env (Id.get env b) (Id.get env b');
            Bounds2NS.set env a m)

  let eq_nseqs_norm env kid rid =
    Debug.dprintf4 debug "Relations.SharedSlices.eq_nseqs_norm (%a <- %a)" Id.pp
      rid Id.pp kid;
    let rb2ns =
      match Bounds2NS.find_opt env rid with
      | None -> Id.IdP.M.empty
      | Some m -> m
    in
    let kb2ns =
      match Bounds2NS.find_opt env kid with
      | None -> Id.IdP.M.empty
      | Some m -> m
    in
    let kb2ns, rb2ns, nklocs =
      match Bounds.get_bounds env kid with
      | Some (fst, lst) ->
          (match Bounds.get_bounds env rid with
          | None -> ()
          | Some (rfst, rlst) ->
              assert (Id.equal fst rfst);
              assert (Id.equal lst rlst));
          let rlocs, kb2ns =
            match Locs.find_opt env rid with
            | None -> (Id.S.empty, kb2ns)
            | Some s -> (
                match Id.M.find_remove kid s with
                | _, None -> (s, kb2ns)
                | s', Some _ -> (s', PM.remove (fst, lst) kb2ns))
          in
          let klocs, rb2ns =
            match Locs.find_opt env kid with
            | None -> (Id.S.empty, rb2ns)
            | Some s -> (
                match Id.M.find_remove rid s with
                | _, None -> (s, rb2ns)
                | s', Some _ -> (s', PM.remove (fst, lst) rb2ns))
          in
          Id.S.iter
            (fun id ->
              Bounds2NS.change env id ~f:(function
                | None -> assert false
                | Some m ->
                    Some
                      (PM.change
                         (function
                           | None -> assert false
                           | Some id' ->
                               assert (Id.equal id' rid || Id.equal id' kid);
                               Some kid)
                         (fst, lst) m)))
            rlocs;
          let nklocs = Id.S.union klocs rlocs in
          (kb2ns, rb2ns, nklocs)
      | None -> (kb2ns, rb2ns, Id.S.empty)
    in
    let nkb2ns =
      PM.fold
        (fun (ifst, ilst) iid nb2ns ->
          match PM.find_opt (ifst, ilst) nb2ns with
          | None ->
              Locs.change env iid ~f:(function
                | None -> assert false
                | Some s ->
                    assert (Id.S.mem rid s);
                    Some
                      (Id.S.add_new (Failure "found") kid (Id.S.remove rid s)));
              PM.add (ifst, ilst) iid nb2ns
          | Some iid' when Id.equal iid iid' ->
              Locs.change env iid ~f:(function
                | None -> assert false
                | Some s ->
                    assert (Id.S.mem rid s);
                    Some (Id.S.remove rid s));
              nb2ns
          | Some iid' ->
              Locs.change env iid ~f:(function
                | None -> assert false
                | Some s ->
                    assert (Id.S.mem rid s);
                    Some (Id.S.remove rid s));
              Egraph.merge env (Id.get env iid) (Id.get env iid');
              nb2ns)
        rb2ns kb2ns
    in
    Bounds2NS.remove env rid;
    Locs.remove env rid;
    Locs.change env kid ~f:(fun _ ->
        if Id.S.is_empty nklocs then None else Some nklocs);
    Bounds2NS.change env kid ~f:(fun _ ->
        if PM.is_empty nkb2ns then None else Some nkb2ns)

  let on_bounds_change env id obounds nbounds =
    Debug.dprintf6 debug "SharedSlices.on_bounds_change %a %a %a" Id.pp id
      IdP.pp obounds IdP.pp nbounds;
    match Locs.find_opt env id with
    | None -> ()
    | Some ilocs ->
        let nilocs =
          Id.S.fold
            (fun id' acc ->
              match Bounds2NS.find_opt env id' with
              | None -> assert false
              | Some m ->
                  let m' =
                    match PM.find_remove obounds m with
                    | _, None -> assert false
                    | _, Some id'' when not (Id.equal id id'') -> assert false
                    | m', _ -> m'
                  in
                  let nm, acc' =
                    match PM.find_opt nbounds m' with
                    | None -> (PM.add nbounds id m', acc)
                    | Some id'' ->
                        Egraph.merge env (Id.get env id) (Id.get env id'');
                        (m', Id.S.remove id' acc)
                  in
                  Bounds2NS.set env id' nm;
                  acc')
            ilocs ilocs
        in
        Locs.set env id nilocs

  let get_shared_slices = Bounds2NS.find_opt
end

let set_rels env id concat slices updates =
  if Id.M.is_empty concat && Id.M.is_empty slices && Id.M.is_empty updates then
    HT.remove env id
  else HT.set env id { concat; slices; updates }

let get_succs env id = HT.find_opt env id
let get_precs env id = Locs.find_opt env id

let add_cloc env id locid =
  Debug.dprintf4 debug "Relations.add_cloc %a %a" Id.pp id Id.pp locid;
  Locs.change env id ~f:(function
    | None -> Some (Id.S.singleton locid, Id.S.empty, Id.S.empty)
    | Some (s1, s2, s3) -> Some (Id.S.add locid s1, s2, s3))

let add_sloc env id locid =
  Debug.dprintf4 debug "Relations.add_sloc %a %a" Id.pp id Id.pp locid;
  Locs.change env id ~f:(function
    | None -> Some (Id.S.empty, Id.S.singleton locid, Id.S.empty)
    | Some (s1, s2, s3) -> Some (s1, Id.S.add locid s2, s3))

let add_uloc env id locid =
  Debug.dprintf4 debug "Relations.add_uloc %a %a" Id.pp id Id.pp locid;
  Locs.change env id ~f:(function
    | None -> Some (Id.S.empty, Id.S.empty, Id.S.singleton locid)
    | Some (s1, s2, s3) -> Some (s1, s2, Id.S.add locid s3))

let rm_loc env ?(c1 = false) ?(c2 = false) ?(c3 = false) id locid =
  Debug.dprintf7 debug "Relations.rm_loc %b %b %b %a %a" c1 c2 c3 Id.pp id Id.pp
    locid;
  assert (c1 || c2 || c3);
  Locs.change env id ~f:(function
    | None -> assert false
    | Some (s1, s2, s3) ->
        let s1' =
          if c1 then (
            assert (Id.S.mem locid s1);
            Id.S.remove locid s1)
          else s1
        in
        let s2' =
          if c2 then (
            assert (Id.S.mem locid s2);
            Id.S.remove locid s2)
          else s2
        in
        let s3' =
          if c3 then (
            assert (Id.S.mem locid s3);
            Id.S.remove locid s3)
          else s3
        in
        if Id.S.is_empty s1' && Id.S.is_empty s2' && Id.S.is_empty s3' then None
        else Some (s1', s2', s3'))

let upd_loc =
  let subst_loc_s c kid rid s =
    if c then (
      assert (Id.S.mem rid s);
      (* TODO: should this be an add_new? *)
      Id.S.add kid (Id.S.remove rid s))
    else s
  in
  fun env ?(c1 = false) ?(c2 = false) ?(c3 = false) id kid rid ->
    Debug.dprintf9 debug "Relations.upd_loc %a: %a <- %a (%b, %b, %b)" Id.pp id
      Id.pp rid Id.pp kid c1 c2 c3;
    assert (c1 || c2 || c3);
    Locs.change env id ~f:(function
      | None -> assert false
      | Some (s1, s2, s3) ->
          Debug.dprintf6 debug "%a  |  %a  |  %a" Id.S.pp s1 Id.S.pp s2 Id.S.pp
            s3;
          Some
            ( subst_loc_s c1 kid rid s1,
              subst_loc_s c2 kid rid s2,
              subst_loc_s c3 kid rid s3 ))

let add_concat env id id1 id2 =
  Debug.dprintf6 debug "Relations.add_concat %a %a %a" (Id.pp_nid env) id
    (Id.pp_nid env) id1 (Id.pp_nid env) id2;
  if Id.equal id1 id2 then Egraph.contradiction ();
  assert (id <> id1);
  assert (id <> id2);
  SharedSlices.add_shared_slice env id id1;
  SharedSlices.add_shared_slice env id id2;
  HT.change env id ~f:(function
    | None ->
        Some
          {
            concat =
              (add_cloc env id1 id;
               add_cloc env id2 id;
               Id.M.add id2 (id1, false) (Id.M.singleton id1 (id2, true)));
            slices = Id.S.empty;
            updates = Id.M.empty;
          }
    | Some ({ concat; _ } as c) ->
        let concat' =
          Id.M.change
            (function
              | None ->
                  add_cloc env id1 id;
                  Some (id2, true)
              | Some ((id2', b) as p) ->
                  assert b;
                  if not (Id.equal id2 id2') then
                    Egraph.merge env (Id.get env id2) (Id.get env id2');
                  Some p)
            id1 concat
        in
        let concat'' =
          Id.M.change
            (function
              | None ->
                  add_cloc env id2 id;
                  Some (id1, false)
              | Some ((id1', b) as p) ->
                  assert (not b);
                  if not (Id.equal id1 id1') then
                    Egraph.merge env (Id.get env id1) (Id.get env id1');
                  Some p)
            id2 concat'
        in
        Some { c with concat = concat'' })

let add_slice env id slid =
  Debug.dprintf4 debug "Relations.add_slice %a %a" (Id.pp_nid env) id
    (Id.pp_nid env) slid;
  assert (slid <> id);
  SharedSlices.add_shared_slice env id slid;
  add_sloc env slid id;
  HT.change env id ~f:(function
    | None ->
        Some
          {
            concat = Id.M.empty;
            slices = Id.S.singleton slid;
            updates = Id.M.empty;
          }
    | Some { concat; slices; updates } ->
        Some { concat; slices = Id.S.add slid slices; updates })

let add_update env id id1 id2 =
  (* TODO: with updates, support shared slices out of the bounds of the second
     argument.
     - experiment by redefining using slices and concatenations? *)
  Debug.dprintf6 debug "Relations.add_update %a %a %a" (Id.pp_nid env) id
    (Id.pp_nid env) id1 (Id.pp_nid env) id2;
  assert (id1 <> id2);
  SharedSlices.add_shared_slice env id id2;
  HT.change env id ~f:(function
    | None ->
        Some
          {
            concat = Id.M.empty;
            slices = Id.S.empty;
            updates =
              (add_uloc env id1 id;
               add_uloc env id2 id;
               Id.M.add id2
                 (Id.S.singleton id1, false)
                 (Id.M.singleton id1 (Id.S.singleton id2, true)));
          }
    | Some { concat; slices; updates } ->
        Some
          {
            concat;
            slices;
            updates =
              (let updates =
                 Id.M.change
                   (function
                     | None ->
                         add_uloc env id1 id;
                         add_uloc env id2 id;
                         Some (Id.S.singleton id2, true)
                     | Some (s, b) when b -> Some (Id.S.add id2 s, b)
                     | _ -> assert false)
                   id1 updates
               in
               Id.M.change
                 (function
                   | None ->
                       add_uloc env id2 id;
                       Some (Id.S.singleton id1, false)
                   | Some (s, b) when not b -> Some (Id.S.add id1 s, b)
                   | _ -> assert false)
                 id2 updates);
          })

let get_updates env id =
  match HT.find_opt env id with
  | None -> Id.M.empty
  | Some { updates } -> updates

let rm_rel_updates_aux env b id1 id2 s updates' =
  Id.S.fold
    (fun id3 acc ->
      Id.M.change
        (function
          | None -> assert false
          | Some (s', b') -> (
              assert (b' = not b);
              match Id.M.find_remove id2 s' with
              | _, None -> assert false
              | s'', Some () when Id.S.is_empty s'' ->
                  rm_loc env ~c3:true id3 id1;
                  None
              | s'', Some () -> Some (s'', b')))
        id3 acc)
    s updates'

let rm_rel env ?(c1 = false) ?(c2 = false) ?(c3 = false) id1 id2 =
  Debug.dprintf7 debug "Relations.remove (%b, %b, %b) %a %a" c1 c2 c3 Id.pp id1
    Id.pp id2;
  assert (c1 || c2 || c3);
  HT.change env id1 ~f:(function
    | None -> assert false
    | Some { concat; slices; updates } ->
        let concat =
          if c1 then (
            match Id.M.find_remove id2 concat with
            | _, None -> assert false
            | concat', Some (id3, _) ->
                (* that means id3 is empty, right? *)
                rm_loc env ~c1:true id2 id1;
                rm_loc env ~c1:true id3 id1;
                Id.M.remove id3 concat')
          else concat
        in
        let slices =
          if c2 then (
            assert (Id.S.mem id2 slices);
            rm_loc env ~c2:true id2 id1;
            Id.S.remove id2 slices)
          else slices
        in
        let updates =
          if c3 then (
            match Id.M.find_remove id2 updates with
            | _, None -> assert false
            | updates', Some (s, true) ->
                (* that means that id3 is an extract on id2 *)
                rm_loc env ~c3:true id2 id1;
                rm_rel_updates_aux env true id1 id2 s updates'
            | updates', Some (s, false) ->
                (* that says nothing about id3 *)
                rm_loc env ~c3:true id2 id1;
                rm_rel_updates_aux env false id1 id2 s updates')
          else updates
        in
        if Id.M.is_empty concat && Id.M.is_empty slices && Id.M.is_empty updates
        then None
        else Some { concat; slices; updates })

let subst_concat env locid kid rid m =
  Debug.dprintf8 debug "Relations.subst_concat %a (%a <- %a) {%a}" Id.pp locid
    Id.pp rid Id.pp kid
    (Id.M.pp (fun fmt (s, b) -> Fmt.pf fmt "(%a, %b)" Id.pp s b))
    m;
  match Id.M.find_remove rid m with
  | _, None -> assert false
  | m1, Some (rother, _) when rother = kid ->
      rm_loc ~c1:true env kid locid;
      rm_loc ~c1:true env rid locid;
      Id.M.remove kid m1
  | m1, Some (rother, rb) -> (
      match Id.M.find_remove rother m1 with
      | _, None -> assert false
      | m2, Some (rid', rb') -> (
          assert (rid = rid');
          assert (rb = not rb');
          match Id.M.find_remove kid m2 with
          | _, None ->
              (* update locs of kid *)
              Locs.change env kid ~f:(function
                | None -> Some (Id.S.singleton locid, Id.S.empty, Id.S.empty)
                | Some (s1, s2, s3) -> Some (Id.S.add locid s1, s2, s3));
              Id.M.add_new Exit rother (kid, rb')
                (Id.M.add_new Exit kid (rother, rb) m2)
          | _, Some (kother, _) ->
              rm_loc ~c1:true env rother locid;
              Egraph.merge env (Id.get env kother) (Id.get env rother);
              m2))

let subst_slice env locid kid rid s =
  Debug.dprintf8 debug "subst_slice %a (%a <- %a) {%a}" Id.pp locid Id.pp rid
    Id.pp kid Id.S.pp s;
  match Id.M.find_remove rid s with
  | _, None -> assert false
  | s', Some () when Id.S.mem kid s ->
      Locs.change env kid ~f:(function
        | None -> None
        | Some (s1, s2, s3) -> Some (s1, Id.S.remove locid s2, s3));
      Id.S.remove kid s'
  | s', Some () ->
      Locs.change env kid ~f:(function
        | None -> Some (Id.M.empty, Id.S.singleton locid, Id.M.empty)
        | Some (s1, s2, s3) -> Some (s1, Id.S.add locid s2, s3));
      Id.S.add kid s'

let s_upd_rm env locid rid s m =
  Id.S.fold
    (fun id acc ->
      Id.M.change
        (function
          | None -> assert false
          | Some (s, b) -> (
              match Id.M.find_remove rid s with
              | _, None -> assert false
              | s', Some () when Id.S.is_empty s' ->
                  rm_loc env ~c3:true id locid;
                  None
              | s', Some () -> Some (s', b)))
        id acc)
    s m

let s_upd_mk_sl env oid rid s m =
  let n = Id.get env oid in
  let val_gty = get_nseq_val_gty env n in
  Id.S.fold
    (fun id acc ->
      let nid = Id.get env id in
      let fst, _, lst, _ = Bounds.get_mk_bounds_n env ~val_gty nid in
      let slice = mk_nseq_slice env val_gty n fst lst in
      Egraph.register env slice;
      Egraph.merge env nid slice;
      Id.M.change
        (function
          | None -> assert false
          | Some (s, b) -> (
              match Id.M.find_remove rid s with
              | _, None -> assert false
              | s', Some () when Id.S.is_empty s' ->
                  rm_loc env ~c3:true id oid;
                  None
              | s', Some () -> Some (s', b)))
        id acc)
    s m

let subst_update env locid kid rid m =
  Debug.dprintf8 debug "subst_updates %a (%a <- %a) {%a}" Id.pp locid Id.pp rid
    Id.pp kid
    (Id.M.pp (fun fmt (s, b) -> Fmt.pf fmt "({%a}, %b)" Id.S.pp s b))
    m;
  match Id.M.find_remove rid m with
  | _, None -> assert false
  | rm, Some (rs, rb) -> (
      match Id.M.find_remove kid rm with
      | km, Some (ks, kb) when kb = not rb ->
          let rs' = Id.M.remove kid rs in
          let ks' = Id.M.remove rid ks in
          rm_loc ~c3:true env kid locid;
          Egraph.merge env (Id.get env kid) (Id.get env locid);
          if rb then
            let m3 = s_upd_rm env locid kid ks' km in
            let m4 = s_upd_mk_sl env locid rid rs' m3 in
            m4
          else
            let m3 = s_upd_rm env locid rid rs' km in
            let m4 = s_upd_mk_sl env locid kid ks' m3 in
            m4
      | _ ->
          Id.S.fold
            (fun rother acc ->
              match Id.M.find_remove rother acc with
              | _, None -> assert false
              | acc', Some (ros, rob) ->
                  assert (rb = not rob);
                  assert (Id.S.mem rid ros);
                  let ros' = Id.S.remove rid ros in
                  if Id.S.mem kid ros' then Id.M.add rother (ros', rob) acc'
                  else
                    let ros' = Id.S.add kid ros' in
                    let acc' = Id.M.add rother (ros', rob) acc' in
                    Id.M.change
                      (function
                        | None ->
                            add_uloc env kid locid;
                            Some (Id.S.singleton rother, not rob)
                        | Some (kos, kob) ->
                            assert (kob = not rob);
                            Some (Id.S.add rother kos, kob))
                      kid acc')
            rs rm)

let ns_eq_aux env kid rid locid c1 c2 c3 =
  Debug.dprintf9 debug "NSD.ns_eq_aux %a (%b, %b, %b) %a <- %a" Id.pp locid c1
    c2 c3 Id.pp rid Id.pp kid;
  HT.change env locid ~f:(function
    | None -> assert false
    | Some { concat; slices; updates } ->
        Some
          {
            concat =
              (if c1 then subst_concat env locid kid rid concat else concat);
            slices =
              (if c2 then subst_slice env locid kid rid slices else slices);
            updates =
              (if c3 then subst_update env locid kid rid updates else updates);
          })

let mk_nseq_to_slice env sid nid =
  let val_gty = get_nseq_val_gty env nid in
  let nsid = Id.get env sid in
  let sfst, _, slst, _ = Bounds.get_mk_bounds_n env ~val_gty nsid in
  let sl = mk_nseq_slice env val_gty nid sfst slst in
  Egraph.register env sl;
  Egraph.merge env sl nsid

let filter_updates env id1 id2 updates =
  match Id.M.find_remove id1 updates with
  | _, None -> updates
  | updates', Some (s, true) ->
      (* id2 -> (id1 -> {s})
         x in s: x = slice(id1, bounds(x))*)
      rm_loc env id1 ~c3:true id2;
      let kn = Id.get env id1 in
      Id.S.fold
        (fun id acc ->
          mk_nseq_to_slice env id kn;
          Id.M.change
            (function
              | None -> assert false
              | Some (fs, fb) ->
                  let fs' = Id.S.remove id1 fs in
                  if Id.S.is_empty fs' then (
                    rm_loc env id ~c3:true id2;
                    None)
                  else Some (fs', fb))
            id acc)
        s updates'
  | updates', Some (s, false) ->
      (* id2 -> ({s} -> id1) *)
      rm_loc env id1 ~c3:true id2;
      Id.S.fold
        (fun id acc ->
          Id.M.change
            (function
              | None -> assert false
              | Some (fs, fb) ->
                  let fs' = Id.S.remove id1 fs in
                  if Id.S.is_empty fs' then (
                    rm_loc env id ~c3:true id2;
                    None)
                  else Some (fs', fb))
            id acc)
        s updates'

let ns_eq_updates env kid rid updates acc =
  Debug.dprintf8 debug "ns_eq_updates (%a <- %a)\n  {%a}\n  {%a}" Id.pp rid
    Id.pp kid pp_sm updates pp_sm acc;
  let rec aux updates ignore acc =
    match Id.M.min_binding updates with
    | id1, (s1, b1) when b1 -> (
        rm_loc env id1 ~c3:true rid;
        let ignore =
          Id.S.fold
            (fun id2 ignore ->
              if not (Id.S.mem id2 ignore) then (
                rm_loc env id2 ~c3:true rid;
                Id.S.add id2 ignore)
              else ignore)
            s1 ignore
        in
        match Id.M.find_opt id1 acc with
        | None ->
            add_uloc env id1 kid;
            let ignore, s1', acc' =
              Id.S.fold
                (fun id2 (ignore, s_acc, acc) ->
                  match Id.M.find_opt id2 acc with
                  | None ->
                      add_uloc env id2 kid;
                      ( ignore,
                        s_acc,
                        Id.M.add id2 (Id.S.singleton id1, false) acc )
                  | Some (s2, false) ->
                      assert (not (Id.S.mem id1 s2));
                      (ignore, s_acc, Id.M.add id2 (Id.S.add id1 s2, false) acc)
                  | Some (s2, true) ->
                      (* id1 -> s1 (id2)
                         id2 -> s2
                         => rm(id2) & s2 = slice(id2, fst(s2), lst(s2)) *)
                      let nid2 = Id.get env id2 in
                      Egraph.merge env nid2 (Id.get env rid);
                      Id.S.fold
                        (fun sid (ignore, s_acc, acc) ->
                          mk_nseq_to_slice env sid nid2;
                          ( Id.S.add sid ignore,
                            Id.S.remove id2 s_acc,
                            Id.M.change
                              (function
                                | None -> assert false
                                | Some (s, b) ->
                                    assert (Id.M.mem id2 s);
                                    let s' = Id.S.remove id2 s in
                                    if Id.S.is_empty s' then (
                                      rm_loc env ~c3:true id2 kid;
                                      None)
                                    else Some (s', b))
                              sid acc ))
                        s2
                        (ignore, s_acc, Id.M.remove id2 acc))
                s1 (ignore, s1, acc)
            in
            let acc' =
              if Id.S.is_empty s1' then Id.M.remove id1 acc'
              else Id.M.add id1 (s1', b1) acc'
            in
            aux (Id.M.remove id1 updates) ignore acc'
        | Some (s3, true) (* when b1 = b1' *) ->
            let ns, acc' =
              Id.S.fold
                (fun id2 (s_acc, acc) ->
                  if Id.S.mem id2 s_acc then (s_acc, acc)
                  else
                    match Id.M.find_opt id2 acc with
                    | None ->
                        add_uloc env id2 kid;
                        ( Id.S.add id2 s_acc,
                          Id.M.add id2 (Id.S.singleton id1, not b1) acc )
                    | Some (s2, false) ->
                        assert (not (Id.S.mem id1 s2));
                        ( Id.S.add id2 s_acc,
                          Id.M.add id2 (Id.S.add id1 s2, false) acc )
                    | Some (s2, true) ->
                        rm_loc env ~c3:true id2 kid;
                        let kns = Id.get env kid in
                        ( s_acc,
                          Id.S.fold
                            (fun id acc ->
                              mk_nseq_to_slice env id kns;
                              Id.M.change
                                (function
                                  | None -> assert false
                                  | Some (s, b) ->
                                      assert (Id.M.mem id2 s);
                                      let s' = Id.M.remove id2 s in
                                      if Id.S.is_empty s' then None
                                      else Some (s', b))
                                id acc)
                            s2 (Id.M.remove id2 acc) ))
                s1 (s3, acc)
            in
            let acc' = Id.M.add id1 (ns, b1) acc' in
            aux (Id.M.remove id1 updates) ignore acc'
        | Some (s3, false) ->
            (* rid -> (id1 -> s1) & kid -> (s3 -> id1) =>
               id1 = rid && s1: slice(id1, fst(s1), lst(s1)) *)
            let nid1 = Id.get env id1 in
            Egraph.merge env nid1 (Id.get env rid);
            rm_loc env ~c3:true id1 kid;
            let acc' =
              Id.S.fold
                (fun id acc ->
                  Id.M.change
                    (function
                      | None -> assert false
                      | Some (s, b) ->
                          assert (Id.M.mem id1 s);
                          let s' = Id.S.remove id1 s in
                          if Id.S.is_empty s' then (
                            rm_loc env ~c3:true id kid;
                            None)
                          else Some (s', b))
                    id acc)
                s3 (Id.M.remove id1 acc)
            in
            let ignore, acc' =
              Id.S.fold
                (fun sid (ignore, acc) ->
                  mk_nseq_to_slice env sid nid1;
                  (Id.S.add sid ignore, acc))
                s1 (ignore, acc')
            in
            aux (Id.M.remove id1 updates) ignore acc')
    | id1, _ -> aux (Id.M.remove id1 updates) ignore acc
    | exception Not_found -> acc
  in
  aux updates Id.S.empty acc

let eq_nseqs_norm env kid rid =
  Debug.dprintf4 debug "NSD.ns_eq1 %a <- %a" Id.pp rid Id.pp kid;
  SharedSlices.eq_nseqs_norm env kid rid;
  (match Locs.find_opt env rid with
  | None -> ()
  | Some (clocs, slocs, ulocs) ->
      let clocs, slocs, ulocs =
        ( (if Id.S.mem kid clocs then (
             rm_rel env ~c1:true kid rid;
             Id.S.remove kid clocs)
           else clocs),
          (if Id.S.mem kid slocs then (
             rm_rel env ~c2:true kid rid;
             Id.S.remove kid slocs)
           else slocs),
          if Id.S.mem kid ulocs then (
            rm_rel env ~c3:true kid rid;
            Id.S.remove kid ulocs)
          else ulocs )
      in
      let slocs', ulocs' =
        Id.S.fold
          (fun locid (slocs, ulocs) ->
            let c2, slocs' =
              if Id.S.mem locid slocs then (true, Id.S.remove locid slocs)
              else (false, slocs)
            in
            let c3, ulocs' =
              if Id.S.mem locid ulocs then (true, Id.S.remove locid ulocs)
              else (false, ulocs)
            in
            ns_eq_aux env kid rid locid true c2 c3;
            (slocs', ulocs'))
          clocs (slocs, ulocs)
      in
      let ulocs'' =
        Id.S.fold
          (fun locid ulocs ->
            let c3, ulocs' =
              if Id.S.mem locid ulocs then (true, Id.S.remove locid ulocs)
              else (false, ulocs)
            in
            ns_eq_aux env kid rid locid false true c3;
            ulocs')
          slocs' ulocs'
      in
      Id.S.iter
        (fun locid -> ns_eq_aux env kid rid locid false false true)
        ulocs'';
      Locs.remove env rid);
  Debug.dprintf4 debug "NSD.ns_eq2 %a <- %a" Id.pp rid Id.pp kid;
  match HT.find_opt env rid with
  | None -> ()
  | Some { concat = concat1; slices = slices1; updates = updates1 } ->
      let concat2, slices2, updates2 =
        match HT.find_opt env kid with
        | None -> (Id.M.empty, Id.S.empty, Id.M.empty)
        | Some { concat = concat2; slices = slices2; updates = updates2 } ->
            (concat2, slices2, updates2)
      in
      let concat =
        Id.M.fold
          (fun id1 (id2, b) acc ->
            if b then
              (* The logic behind the contradictions is that concatenations
                 are only defined over non-empty sequences, if a sequence is
                 equal to a concatenation between itself and something else,
                 then that something else, is necessary empty which is a
                 contradiction with the definition of concatenation *)
              if Id.equal id1 kid then Egraph.contradiction ()
              else if Id.equal id2 kid then Egraph.contradiction ()
              else
                let acc =
                  match Id.M.find_opt id2 acc with
                  | Some (id1', b') when not (Id.equal id1 id1') ->
                      assert (not b');
                      Egraph.merge env (Id.get env id1) (Id.get env id1');
                      rm_loc env ~c1:true id1' kid;
                      Id.M.remove id2 (Id.M.remove id1' acc)
                  | _ -> acc
                in
                match Id.M.find_opt id1 acc with
                | None ->
                    upd_loc env id1 ~c1:true kid rid;
                    upd_loc env id2 ~c1:true kid rid;
                    Id.M.add id2 (id1, false) (Id.M.add id1 (id2, true) acc)
                | Some (id2', b') when id2 = id2' ->
                    assert (b = b');
                    rm_loc env id1 ~c1:true rid;
                    rm_loc env id2 ~c1:true rid;
                    Id.M.add id2' (id1, false) (Id.M.add id1 (id2', true) acc)
                | Some (id2', b') when b' ->
                    rm_loc env id1 ~c1:true rid;
                    rm_loc env id2 ~c1:true rid;
                    Egraph.merge env (Id.get env id2) (Id.get env id2');
                    Id.M.add id2' (id1, false) (Id.M.add id1 (id2', true) acc)
                | _ -> assert false
            else acc)
          concat1 concat2
      in
      let slices =
        Id.S.fold
          (fun id acc ->
            if Id.equal id kid then (
              rm_loc env ~c2:true id rid;
              acc)
            else if Id.S.mem id acc then (
              rm_loc env ~c2:true id rid;
              acc)
            else (
              upd_loc env ~c2:true id kid rid;
              Id.S.add id acc))
          slices1 slices2
      in
      let updates1', updates2' =
        ( filter_updates env kid rid updates1,
          filter_updates env rid kid updates2 )
      in
      let updates = ns_eq_updates env kid rid updates1' updates2' in
      HT.remove env rid;
      set_rels env kid concat slices updates

let on_bounds_change = SharedSlices.on_bounds_change
let get_shared_slices = SharedSlices.get_shared_slices
let pp_shs = SharedSlices.Bounds2NS.pp
let pp_ht = HT.pp
