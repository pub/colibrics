(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_core
open Common

(* NSeq id -> (first bound id * last bound id) *)
module HT = Id.MkIHT (struct
  type t = Id.t * Id.t [@@deriving show]

  let name = "NSeqBounds"
end)

(* Index id -> (map of last bound id -> nseq id * first bound id set) *)
module ILocs = Id.MkIHT (struct
  type t = Id.S.t Id.M.t * Id.S.t

  let pp fmt (m, s) =
    Fmt.pf fmt "[%a | %a]"
      (Id.M.pp (fun fmt s -> Fmt.pf fmt "{%a}" Id.S.pp s))
      m Id.S.pp s

  let name = "index_locations"
end)

let get_locs env id = ILocs.find_opt env id

let on_bounds_change_hook :
    (Egraph.rw Egraph.t -> Id.t -> Id.t * Id.t -> Id.t * Id.t -> unit)
    Datastructure.Push.t =
  Datastructure.Push.create Fmt.nop "DB.on_bounds_change"

let register_bounds_change_hook d f =
  Datastructure.Push.push on_bounds_change_hook d f

let on_bounds_change env id (ofst, olst) (nfst, nlst) =
  Datastructure.Push.iter on_bounds_change_hook env ~f:(fun f ->
      f env id (ofst, olst) (nfst, nlst))

let add_locs env nsid fstid lstid =
  ILocs.change env fstid ~f:(function
    | None -> Some (Id.M.singleton lstid (Id.S.singleton nsid), Id.S.empty)
    | Some (m, s) ->
        Some
          ( Id.M.change
              (function
                | None -> Some (Id.S.singleton nsid)
                | Some s -> Some (Id.S.add nsid s))
              lstid m,
            s ));
  ILocs.change env lstid ~f:(function
    | None -> Some (Id.M.empty, Id.S.singleton fstid)
    | Some (m, s) -> Some (m, Id.S.add fstid s))

(* TODO: add decision that if fb < lb then NSeq is empty?  *)
let add_bounds env id fb lb =
  Debug.dprintf6 debug "Bounds.add_bounds %a [%a; %a]" Id.pp id Id.pp fb Id.pp
    lb;
  HT.change env id ~f:(function
    | None -> Some (fb, lb)
    | Some (fb', lb') ->
        if fb <> fb' then Egraph.merge env (Id.get env fb) (Id.get env fb');
        if lb <> lb' then Egraph.merge env (Id.get env lb) (Id.get env lb');
        Some (fb', lb'));
  add_locs env id fb lb

let upd_bounds_id_ret env nsid nfb nlb =
  set_int_dom env nfb;
  set_int_dom env nlb;
  let nfbid = Id.Index.get_id_safe ~debug env nfb in
  let nlbid = Id.Index.get_id_safe ~debug env nlb in
  Debug.dprintf6 debug "upd_bounds %a [%a; %a]" (Id.pp_nid env) nsid
    (Id.Index.pp env) nfb (Id.Index.pp env) nlb;
  match HT.find_opt env nsid with
  | Some (fb, lb) ->
      if fb <> nfbid then Egraph.merge env (Id.get env fb) nfb;
      if lb <> nlbid then Egraph.merge env (Id.get env lb) nlb;
      (fb, lb)
  | None ->
      add_bounds env nsid nfbid nlbid;
      (nfbid, nlbid)

let upd_bounds_id env nsid nfb nlb = ignore (upd_bounds_id_ret env nsid nfb nlb)

let upd_bounds env n nfb nlb =
  let nsid = Id.NSeq.get_id_safe ~debug env n in
  upd_bounds_id env nsid nfb nlb

let upd_bounds_ret env n nfb nlb =
  let nsid = Id.NSeq.get_id_safe ~debug env n in
  let fst, lst = upd_bounds_id_ret env nsid nfb nlb in
  (fst, lst)

let get_bounds env id =
  let r = HT.find_opt env id in
  Debug.dprintf4 debug "Bounds.get_bounds %a: (%a)" Id.pp id
    (Fmt.option (fun fmt (fst, lst) -> Fmt.pf fmt "(%a,%a)" Id.pp fst Id.pp lst))
    r;
  r

let get_bounds_exn env id =
  match get_bounds env id with None -> assert false | Some v -> v

let get_mk_bounds env ?val_gty n nsid =
  let f, fid, l, lid =
    match HT.find_opt env nsid with
    | Some (fb, lb) -> (Id.get env fb, fb, Id.get env lb, lb)
    | None ->
        let val_gty =
          match val_gty with
          | Some val_gty -> val_gty
          | None -> get_nseq_val_gty env n
        in
        let f = mk_nseq_first env val_gty n in
        Egraph.register env f;
        let fid = Id.Index.get_id_safe ~debug env f in
        let l = mk_nseq_last env val_gty n in
        Egraph.register env l;
        let lid = Id.Index.get_id_safe ~debug env l in
        add_bounds env nsid fid lid;
        set_int_dom env f;
        set_int_dom env l;
        (f, fid, l, lid)
  in
  Debug.dprintf12 debug "get_mk_bounds %a(%a): [%a(%a); %a(%a)]" Node.pp n Id.pp
    nsid Node.pp f Id.pp fid Node.pp l Id.pp lid;
  (f, fid, l, lid)

let get_mk_bounds_id env ?val_gty id =
  get_mk_bounds env ?val_gty (Id.get env id) id

let get_mk_bounds_n env ?val_gty n =
  get_mk_bounds env ?val_gty n (Id.NSeq.get_id_safe env n)

let upd_fb env ?val_gty n nfb =
  Id.Index.set_id env nfb;
  Debug.dprintf4 debug "upd_fb %a %a" Node.pp n Node.pp nfb;
  let fb, _, _, _ = get_mk_bounds_n env ?val_gty n in
  Egraph.merge env fb nfb

let upd_lb env ?val_gty n nlb =
  Id.NSeq.set_id env n;
  Id.Index.set_id env nlb;
  Debug.dprintf4 debug "upd_lb %a %a" Node.pp n Node.pp nlb;
  let _, _, lb, _ = get_mk_bounds_n env ?val_gty n in
  Egraph.merge env lb nlb

let loc_subst_bounds env (fstid, lstid) nsk nsr =
  ILocs.change env fstid ~f:(function
    | None -> assert false
    | Some (m, s) ->
        Some
          ( Id.M.change
              (function
                | None -> assert false
                | Some s -> Some (Id.S.add nsk (Id.S.remove nsr s)))
              lstid m,
            s ))

let rm1 env fb' lb' rid =
  Debug.dprintf6 debug "rm1 %a %a %a" Id.pp fb' Id.pp lb' Id.pp rid;
  match ILocs.find_opt env fb' with
  | None -> assert false
  | Some (m, s) -> (
      match Id.M.find_remove lb' m with
      | _, None -> assert false
      | m', Some nss -> (
          match Id.M.find_remove rid nss with
          | _, None -> assert false
          | nss', Some () when Id.S.is_empty nss' ->
              let s' =
                if fb' = lb' then (
                  let s', opt = Id.M.find_remove fb' s in
                  assert (opt = Some ());
                  s')
                else (
                  ILocs.change env lb' ~f:(function
                    | None -> assert false
                    | Some (m, s) -> Some (m, Id.S.remove fb' s));
                  s)
              in
              ILocs.set env fb' (m', s')
          | nss', Some () ->
              let m'' = Id.M.add lb' nss' m' in
              ILocs.set env fb' (m'', s)))

(* lb' is removed only if it's the first bound nowhere, and that it is the last
   bound of fb' for no nseq. *)
let rm2 env fb' lb' =
  Debug.dprintf4 debug "rm2 %a %a" Id.pp fb' Id.pp lb';
  ILocs.change env lb' ~f:(function
    | None -> assert false
    | Some (m, s) ->
        let c =
          Option.fold ~none:false
            ~some:(fun (m, _) -> Id.M.mem lb' m)
            (ILocs.find_opt env fb')
        in
        if not c then
          let s' = Id.S.remove fb' s in
          if Id.S.is_empty s' && Id.M.is_empty m then None else Some (m, s')
        else Some (m, s))

let eq_nseqs_norm env kid rid =
  Debug.dprintf4 debug "Bounds.eq_nseqs_norm %a <- %a" Id.pp rid Id.pp kid;
  HT.change env kid ~f:(function
    | None -> (
        match HT.find_opt env rid with
        | None -> None
        | Some rb ->
            HT.remove env rid;
            loc_subst_bounds env rb kid rid;
            Some rb)
    | Some ((fb, lb) as kb) -> (
        match HT.find_opt env rid with
        | None -> Some kb
        | Some (fb', lb') ->
            HT.remove env rid;
            (match (fb <> fb', lb <> lb') with
            | true, true ->
                rm1 env fb' lb' rid;
                rm2 env fb' lb';
                Egraph.merge env (Id.get env fb) (Id.get env fb');
                Egraph.merge env (Id.get env lb) (Id.get env lb');
                on_bounds_change env rid (fb', lb') (fb, lb)
            | true, false ->
                rm1 env fb' lb' rid;
                Egraph.merge env (Id.get env fb) (Id.get env fb');
                on_bounds_change env rid (fb', lb') (fb, lb)
            | false, true ->
                ILocs.change env fb ~f:(function
                  | None -> assert false
                  | Some (m, s) ->
                      Some
                        ( Id.M.change
                            (function
                              | None -> assert false
                              | Some s -> Some (Id.S.remove rid s))
                            lb' m,
                          s ));
                rm2 env fb' lb';
                Egraph.merge env (Id.get env lb) (Id.get env lb');
                on_bounds_change env rid (fb', lb') (fb, lb)
            | false, false ->
                ILocs.change env fb ~f:(function
                  | None -> assert false
                  | Some (m, s) ->
                      Some
                        ( Id.M.change
                            (function
                              | None -> assert false
                              | Some s -> Some (Id.S.remove rid s))
                            lb' m,
                          s )));
            Some (fb, lb)))

let subst_ns_bounds env nsids kid rid =
  Debug.dprintf6 debug "subst_ns_bounds (%a) [_; _]: subst %a <- %a" Id.S.pp
    nsids Id.pp rid Id.pp kid;
  Id.S.iter
    (fun nsid ->
      HT.change env nsid ~f:(function
        | None -> assert false
        | Some (fb, lb) ->
            assert (Id.equal fb rid);
            assert (Id.equal lb rid);
            on_bounds_change env nsid (rid, rid) (kid, kid);
            Some (kid, kid)))
    nsids

let subst_ns_fst_bounds env lstid nsids kid rid =
  Debug.dprintf8 debug "subst_ns_fst_bounds (%a) [_; %a]: subst %a <- %a"
    Id.S.pp nsids Id.pp lstid Id.pp rid Id.pp kid;
  Id.S.iter
    (fun nsid ->
      HT.change env nsid ~f:(function
        | None -> assert false
        | Some (fb, lb) ->
            Debug.dprintf6 debug "found %a [%a; %a]" Id.pp nsid Id.pp fb Id.pp
              lb;
            assert (Id.equal fb rid);
            on_bounds_change env nsid (fb, lb) (kid, lb);
            Some (kid, lb)))
    nsids;
  ILocs.change env lstid ~f:(function
    | None -> assert false
    | Some (m, s) ->
        assert (Id.S.mem rid s);
        Some (m, Id.S.add kid (Id.S.remove rid s)))

let subst_ns_lst_bounds env fstid nsids kid rid =
  Debug.dprintf8 debug "subst_ns_lst_bounds (%a) [%a; _]: subst %a <- %a"
    Id.S.pp nsids Id.pp fstid Id.pp rid Id.pp kid;
  Id.S.iter
    (fun nsid ->
      HT.change env nsid ~f:(function
        | None -> assert false
        | Some (fb, lb) ->
            assert (Id.equal lb rid);
            on_bounds_change env nsid (fb, lb) (fb, kid);
            Some (fb, kid)))
    nsids

let eq_indices_norm env kid rid =
  Debug.dprintf4 debug "Bounds.eq_indices_norm %a <- %a" Id.pp rid Id.pp kid;
  match ILocs.find_opt env rid with
  | None -> ()
  | Some (m, s) ->
      ILocs.remove env rid;
      let m, s, nsid_opt =
        match Id.M.find_remove rid m with
        | _, None -> (m, s, None)
        | m', Some nsid -> (m', Id.S.remove rid s, Some nsid)
      in
      Id.S.iter
        (fun fstid ->
          ILocs.change env fstid ~f:(function
            | None -> assert false
            | Some (m, s) -> (
                match Id.M.find_remove rid m with
                | _, None -> assert false
                | m', Some nsids ->
                    subst_ns_lst_bounds env fstid nsids kid rid;
                    Some
                      ( Id.M.change
                          (function
                            | None -> Some nsids
                            | Some nsids' -> Some (Id.S.union nsids' nsids))
                          kid m',
                        s ))))
        s;
      Id.M.iter
        (fun lstid nsids -> subst_ns_fst_bounds env lstid nsids kid rid)
        m;
      let m, s =
        match nsid_opt with
        | None -> (m, s)
        | Some nsids ->
            subst_ns_bounds env nsids kid rid;
            ( Id.M.change
                (function
                  | None -> Some nsids | Some s -> Some (Id.S.union nsids s))
                kid m,
              Id.S.add kid s )
      in
      ILocs.change env kid ~f:(function
        | None -> Some (m, s)
        | Some (m', s') ->
            Some
              ( Id.M.union (fun _ s1 s2 -> Some (Id.S.union s1 s2)) m m',
                Id.S.union s s' ))

let iter env f =
  ILocs.iter env ~f:(fun fstid (m, _) ->
      Id.M.iter (fun lstid s -> f env fstid lstid s) m)

let fold env f acc =
  ILocs.fold
    (fun fstid (m, _) acc ->
      Id.M.fold (fun lstid s acc -> f env fstid lstid s acc) m acc)
    env acc

let pp_ht = HT.pp
