(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

include
  Colibri2_theories_utils.Ident.S with type t = Colibri2_theories_utils.Ident.t

val pp_nid : Egraph.wt -> Format.formatter -> t -> unit

module NSeq : Colibri2_theories_utils.Ident.IdDomSig
module Index : Colibri2_theories_utils.Ident.IdDomSig

val get : _ Egraph.t -> t -> Node.t
val set : Egraph.wt -> t -> Node.t -> unit
val pp_ht : Format.formatter -> Egraph.wt -> unit

module IdP :
  Colibri2_popop_lib.Popop_stdlib.Datatype
    with type t =
      Colibri2_theories_utils.Ident.t * Colibri2_theories_utils.Ident.t

module IdT :
  Colibri2_popop_lib.Popop_stdlib.Datatype
    with type t =
      Colibri2_theories_utils.Ident.t
      * Colibri2_theories_utils.Ident.t
      * Colibri2_theories_utils.Ident.t

module type HTS = Colibri2_theories_utils.Ident.HTS

module MkIHT (V : sig
  type t

  val pp : t Fmt.t
  val name : string
end) : HTS with type key = t and type t = V.t

module MkIPHT (V : sig
  type t

  val pp : t Fmt.t
  val name : string
end) : HTS with type key = t * t and type t = V.t
