(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

module Pattern = Colibri2_theories_quantifiers.Pattern
module InvertedPath = Colibri2_theories_quantifiers.InvertedPath
module RealValue = Colibri2_theories_LRA.RealValue

val fresh_var_id :
  Egraph.wt -> ?val_gty:Ground.Ty.t -> Node.t -> Node.t -> Node.t -> Node.t

module ConcatDom : sig
  type t = NSComp_info.NSC.t

  val pp_ht : Format.formatter -> _ Egraph.t -> unit
  val get_cdom : Egraph.wt -> Id.t -> t option
  val set_cdom : Egraph.wt -> Id.t -> t -> unit
  val upd_cdom : Egraph.wt -> ?upd_loc:bool -> Id.t -> t -> unit

  val upd_dom' :
    Egraph.wt ->
    Node.t ->
    ?next:Node.t ->
    (NSComp_info.NSC.atom * Id.t * Id.t) list ->
    unit

  val register_on_first_set_cdom_hook :
    Egraph.wt -> (Egraph.wt -> Id.t -> t -> unit) -> unit

  val subst_n_dom : Egraph.wt -> Id.t -> t -> unit
  val update : 'a Id.M.t Id.M.t -> Id.t -> Id.t -> 'a -> 'a Id.M.t Id.M.t

  val apply_nth :
    Egraph.wt ->
    (Egraph.wt ->
    NSComp_info.NSC.atom ->
    Id.t ->
    Id.t ->
    ?next:Node.t ->
    'a ->
    'a) ->
    Node.t ->
    t ->
    Node.t ->
    'a ->
    'a

  val relocate_cc :
    Egraph.wt ->
    ?upd_locs:bool ->
    ?old_repr:Id.t ->
    Id.t ->
    NSComp_info.NSC.t ->
    Colibri2_theories_LRA.Polynome.t ->
    NSComp_info.NSC.t
end

module EqHook : sig
  (* a = slice(b, f, l):
     - if fst(b) = f then fst(a) = f
     - if lst(b) = l then lst(b) = l *)
  val add_rmnsc_hook : Egraph.wt -> Id.t -> Id.t -> Node.t * Node.t -> unit
  val add_mrnodes_hook : Egraph.wt -> Id.t -> Id.t -> Node.t * Node.t -> unit
  val eq_indices_norm : Egraph.wt -> Id.t -> Id.t -> unit
end

val get_n_cdom_locs : Egraph.wt -> Id.t -> Node.S.t
val init_hooks : Egraph.wt -> unit
val find_nsc_eqs : Egraph.wt -> unit

val relocate_node :
  Egraph.wt -> ?val_gty:Ground.Ty.t -> Node.t -> Node.t -> Node.t -> Node.t

val relocate_node_wbounds :
  Egraph.wt ->
  ?val_gty:Ground.Ty.t ->
  Node.t ->
  Node.t ->
  Id.t ->
  Node.t ->
  Id.t ->
  Colibri2_theories_LRA.Polynome.t ->
  Node.t * Node.t * Id.t * Node.t * Id.t

val relocate_atom :
  Egraph.wt ->
  ?val_gty:Ground.Ty.t ->
  NSComp_info.NSC.atom ->
  Node.t ->
  Node.t ->
  Node.t ->
  Id.t * Id.t * NSComp_info.NSC.atom

val on_reloc_repr_change :
  Egraph.wt ->
  old_repr:Id.t ->
  repr:Id.t ->
  Colibri2_theories_LRA.Polynome.t ->
  unit
