(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

val new_nseq : Egraph.wt -> Node.t -> Id.t
val new_empty_nseq : Egraph.wt -> Node.t -> Id.t
val new_const_nseq : Egraph.wt -> Node.t -> Node.t -> Id.t
val set_is_empty : Egraph.wt -> Id.t -> unit
val set_defval : Egraph.wt -> Id.t -> Node.t -> unit
val is_empty : Egraph.wt -> Id.t -> bool
val get_defval : Egraph.wt -> Id.t -> Node.t option

val get_kvs :
  Egraph.wt -> Id.t -> (Node.t * Id.S.t * Id.S.t Id.M.t) Id.M.t option

val get_kns : Egraph.wt -> Id.t -> Id.t Id.M.t option

val find_kv :
  Egraph.wt -> Id.t -> Id.t -> (Node.t * Id.S.t * Id.S.t Id.M.t) option

val add_kv : Egraph.wt -> Id.t -> ?b:Id.t -> Id.t -> Node.t -> unit
val add_ns_concat : Egraph.wt -> Node.t -> Node.t -> Node.t -> unit
val add_ns_slice : Egraph.wt -> Node.t -> Node.t -> Node.t -> Node.t -> unit

val add_ns_update :
  Egraph.wt ->
  Ground.Ty.t ->
  Node.t ->
  Node.t ->
  Node.t ->
  Node.t ->
  Node.t ->
  Node.t ->
  Node.t ->
  unit

val eq_nseqs_norm : Egraph.wt -> Id.t -> Id.t -> unit
val eq_indices_norm : Egraph.wt -> Id.t -> Id.t -> unit

val on_reloc_repr_change :
  Egraph.wt ->
  old_repr:Id.t ->
  repr:Id.t ->
  Colibri2_theories_LRA.Polynome.t ->
  unit

val add_kv_check_bounds : Egraph.wt -> Id.t -> Node.t -> Node.t -> unit

val set_on_set_defval_hook :
  Egraph.wt -> (Egraph.wt -> Id.t -> Node.t -> unit) -> unit

val register_on_new_val_hook :
  Egraph.wt -> (Egraph.wt -> Id.t -> Id.t -> Node.t -> unit) -> unit

val register_on_set_is_empty_hook :
  Egraph.wt -> (Egraph.wt -> Id.t -> Node.t -> unit) -> unit

val pp_nsht : Format.formatter -> _ Egraph.t -> unit
val pp_ht : Format.formatter -> _ Egraph.t -> unit
val pp_kns : Format.formatter -> _ Egraph.t -> unit
