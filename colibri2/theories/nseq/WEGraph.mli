(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)
val register_new_link_hook :
  Egraph.wt -> (Egraph.wt -> Id.t -> Id.t -> unit) -> unit

val on_new_link : Egraph.wt -> Id.t -> Id.t -> unit
val new_set : Egraph.wt -> Id.t -> Id.t -> Id.t -> unit
val eq_indices_norm : Egraph.wt -> Id.t -> Id.t -> unit
val eq_nseqs_norm : Egraph.wt -> Id.t -> Id.t -> unit
val get_repr : Egraph.wt -> Id.t -> Id.t
val get_paths : Egraph.wt -> Id.t -> Id.t -> Id.S.t list
val get_links : _ Egraph.t -> Id.t -> Id.S.t Id.M.t

type path = PStep of Id.t * Id.S.t * path | PSplit of path list | PEnd

val get_undirected_path : _ Egraph.t -> Id.t -> Id.t -> Id.S.t -> path option
val are_linked : Egraph.wt -> Id.t -> Id.t -> bool
val pp_ht : Format.formatter -> _ Egraph.t -> unit
val pp_class : Format.formatter -> _ Egraph.t -> unit
