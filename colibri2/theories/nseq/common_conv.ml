(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_core
open Colibri2_popop_lib
open Common
module RealValue = Colibri2_theories_LRA.RealValue
module P = Colibri2_theories_LRA.Polynome
module DP = Colibri2_theories_LRA.Dom_polynome

(* a = nseq.content(b), select(a, i),
   if first(b) <= i <= last(b) them *)
(* let select_ns_content = assert false *)

(** Common converter for: NSeq_first, NSeq_last, NSeq_content, NSeq_length,
    NSeq_const *)
let converter env f =
  let s = Ground.sem f in
  Debug.dprintf4 debug "Seq Register %a: %a" Ground.pp f Ground.Term.pp s;
  let n = Ground.node f in
  (match s.ty with
  | { app = { builtin = Builtin.NSeq; _ }; args = [ val_gty ]; _ } ->
      add_nseq_gty env n val_gty;
      ignore (NSeq_dom.new_nseq env n)
  | _ -> ());
  match s with
  | { app = { builtin = Builtin.NSeq_first; _ }; args; tyargs = [ val_gty ]; _ }
    ->
      let a = IArray.extract1_exn args in
      Egraph.register env a;
      Bounds.upd_fb env a ~val_gty n
  | { app = { builtin = Builtin.NSeq_last; _ }; args; tyargs = [ val_gty ]; _ }
    ->
      let a = IArray.extract1_exn args in
      Egraph.register env a;
      Bounds.upd_lb env a ~val_gty n
  | { app = { builtin = Builtin.NSeq_const; _ }; args; tyargs = [ _ ]; _ } ->
      NSeq_value.propagate_value env f;
      let f, l, v = IArray.extract3_exn args in
      Egraph.register env f;
      Egraph.register env l;
      Egraph.register env v;
      Bounds.upd_bounds env n f l;
      (* just to update the is_const and other fields *)
      ignore (NSeq_dom.new_const_nseq env n v);
      let le = mk_le env [ f; l ] in
      Egraph.register env le;
      let alt env = NSeq_dom.set_is_empty env (Id.NSeq.get_id env n) in
      new_decision env le "Decision from NSeq_const" ~alt (fun _ ->
          bool_dec ~alt le)
  | {
   app = { builtin = Builtin.NSeq_length; _ };
   args;
   tyargs = [ val_gty ];
   _;
  } ->
      let a = IArray.extract1_exn args in
      Egraph.register env a;
      let aid = NSeq_dom.new_nseq env a in
      if NSeq_dom.is_empty env aid then Egraph.merge env n RealValue.zero
      else
        let fst, _, lst, _ = Bounds.get_mk_bounds_n env ~val_gty a in
        let lt = mk_lt env [ lst; fst ] in
        Egraph.register env lt;
        let cons env = Egraph.merge env n RealValue.zero in
        let alt env =
          let len = mk_add env (mk_sub env lst fst) RealValue.one in
          Egraph.register env len;
          Egraph.merge env n len
        in
        new_decision env lt "Decision from NSeq_length" ~cons ~alt (fun _ ->
            bool_dec ~cons ~alt lt)
      (* TODO: for some reason, replacing lt with le causes the following test
         to reach the step limit:
          - colibri2/tests/solve/smt_nseq/sat/concat2.psmt2
      *)
      (* let le = mk_le env [ fst; lst ] in
         Egraph.register env le;
         new_decision env le "Decision from NSeq_length"
           ~cons:(fun _ ->
             let len = mk_add env (mk_sub env lst fst) RealValue.one in
             Egraph.register env len;
             Egraph.merge env n len;
             DecNo)
           ~alt:(fun env ->
             Egraph.merge env n RealValue.zero;
             DecNo)
           (fun _ -> Boolean.dectodo le) *)
  | {
   app = { builtin = Builtin.NSeq_content; _ };
   args;
   tyargs = [ val_gty ];
   _;
  } ->
      let ns = IArray.extract1_exn args in
      Debug.dprintf2 debug "Register %a" Node.pp ns;
      Egraph.register env ns;
      add_nseq_gty env ns val_gty;
      let aid = Colibri2_theories_array.Id.Array.get_id_safe env n in
      let nsid = Id.NSeq.get_id_safe env ns in
      Colibri2_theories_utils.Nsa_content.new_content env aid nsid
  | { app = { builtin = Builtin.NSeq_relocate }; args; tyargs = [ val_gty ] } ->
      NSeq_value.propagate_value env f;
      let a, i = IArray.extract2_exn args in
      Egraph.register env a;
      Egraph.register env i;
      let afst, _afstid, alst, _ = Bounds.get_mk_bounds_n env ~val_gty a in
      if Egraph.is_equal env afst i then Egraph.merge env n a
      else
        (* let iid = Id.Index.get_id_safe env i in
           NSComp_dom.add_mrnodes_hook env afstid iid (n, a);
           (* This is probably unnecessary *)
        *)
        let nl' = mk_add env (mk_sub env alst afst) i in
        Egraph.register env nl';
        Bounds.upd_bounds env n i nl';
        ignore
        @@ Relocate.new_relocate env
             (Id.NSeq.get_id_safe env n)
             i
             (Id.NSeq.get_id_safe env a)
             afst
  | _ -> ()

let nseq_size env fst lst =
  let open Colibri2_theories_LRA in
  match (RealValue.get env fst, RealValue.get env lst) with
  | Some fstv, Some lstv ->
      let sz = A.add (A.sub lstv fstv) A.one in
      Debug.dprintf10 debug "size %a (%a) -- %a (%a): %a" (Id.Index.pp env) fst
        A.pp fstv (Id.Index.pp env) lst A.pp lstv A.pp sz;
      Some sz
  | _ ->
      (* TODO: does this ever work? *)
      let fstn, fstc = Dom_polynome.get_proxy env fst in
      let lstn, lstc = Dom_polynome.get_proxy env lst in
      if Egraph.is_equal env fstn lstn then (
        let sz = A.add (A.sub lstc fstc) A.one in
        Debug.dprintf14 debug "size %a (%a + %a) -- %a (%a + %a): %a"
          (Id.Index.pp env) fst Node.pp fstn A.pp fstc (Id.Index.pp env) lst
          Node.pp lstn A.pp lstc A.pp sz;
        Some sz)
      else None

(* TODO: fix, this is very naive, avoid generating already existing indices
   and indices for constant NSeqs.
   Is it possible to call NSeq_dom.add_kv and check right away?
   - Only generate for interesting indices *)
let gen_gets env fst sz id1 id2 =
  Debug.dprintf8 debug "gen_gets  %a %a  %a %a" Node.pp fst A.pp sz
    (Id.pp_nid env) id1 (Id.pp_nid env) id2;
  let n1 = Id.get env id1 in
  let n2 = Id.get env id2 in
  let val_gty = get_nseq_val_gty env n1 in
  let rec aux n =
    if A.ge n sz then []
    else
      let ind = if A.is_zero n then fst else mk_add env fst (RealValue.cst n) in
      let get1 = mk_nseq_get env val_gty n1 ind in
      let get2 = mk_nseq_get env val_gty n2 ind in
      let eq = Equality.equality env [ get1; get2 ] in
      Egraph.register env eq;
      (* TODO: decide on equalities? *)
      eq :: aux (A.add n A.one)
  in
  let eqs = aux A.zero in
  if eqs = [] then () (* TODO: could be because size is zero *)
  else
    let conj = Boolean._and env eqs in
    let ns_eq = Equality.equality env [ n1; n2 ] in
    Egraph.register env conj;
    Egraph.register env ns_eq;
    Egraph.merge env conj ns_eq

let compare_kvs_naive env ~mk_gets fst lst id1 o1opt m1 id2 o2opt m2 =
  Debug.dprintf13 debug "compare_kvs_naive  %b  %a (%a) {%a}  %a (%a) {%a}"
    mk_gets (Id.pp_nid env) id1 (Fmt.option Node.pp) o1opt (Id.M.pp Node.pp) m1
    (Id.pp_nid env) id2 (Fmt.option Node.pp) o2opt (Id.M.pp Node.pp) m2;
  (* should only return true if there is an other and all values are equal
     or if there is no other, the number of indices needs to be equal to the
     size of the n-sequence *)
  if Id.M.is_empty m1 && Id.M.is_empty m2 then
    match (o1opt, o2opt) with
    | Some o1, Some o2 -> eq_vals env o1 o2
    | _ when mk_gets -> (
        match nseq_size env fst lst with
        | None -> 0
        | Some sz ->
            gen_gets env fst sz id1 id2;
            0)
    | _ -> 0
  else
    let res =
      match (o1opt, o2opt) with
      | None, None ->
          Id.M.fold2_union
            (fun _ a_opt b_opt acc ->
              if acc <> 1 then acc
              else
                match (a_opt, b_opt) with
                | Some a, Some b -> eq_vals env a b
                | _ -> 0)
            m1 m2 1
      | Some o1, None ->
          Id.M.fold2_union
            (fun _ a_opt b_opt acc ->
              if acc <> 1 then acc
              else
                match (a_opt, b_opt) with
                | Some a, Some b -> eq_vals env a b
                | None, Some b -> eq_vals env o1 b
                | _ -> 0)
            m1 m2 1
      | None, Some o2 ->
          Id.M.fold2_union
            (fun _ a_opt b_opt acc ->
              if acc <> 1 then acc
              else
                match (a_opt, b_opt) with
                | Some a, Some b -> eq_vals env a b
                | Some a, None -> eq_vals env a o2
                | _ -> 0)
            m1 m2 1
      | Some o1, Some o2 ->
          Id.M.fold2_union
            (fun _ a_opt b_opt acc ->
              if acc <> 1 then acc
              else
                match (a_opt, b_opt) with
                | Some a, Some b -> eq_vals env a b
                | Some a, None -> eq_vals env a o2
                | None, Some b -> eq_vals env o1 b
                | _ -> 0)
            m1 m2 1
    in
    if res = 1 then
      let ns_sz = nseq_size env fst lst in
      let m1_sz = Id.M.cardinal m1 in
      let m2_sz = Id.M.cardinal m2 in
      match ns_sz with
      | None -> if Option.is_some o1opt && Option.is_some o2opt then res else 0
      | Some sz ->
          let c =
            (Option.is_some o1opt || A.equal (A.of_int m1_sz) sz)
            && (Option.is_some o2opt || A.equal (A.of_int m2_sz) sz)
          in
          if c then res
          else (
            if mk_gets then gen_gets env fst sz id1 id2;
            0)
    else if res = 0 then
      if mk_gets then (
        match nseq_size env fst lst with
        | None -> 0
        | Some sz ->
            gen_gets env fst sz id1 id2;
            0)
      else 0
    else res

let rec mk_eqs =
  (* TODO: better detection of empty NSeqs, make sure the empty case in
      NSeq_dom is reliable *)
  let get_vals env ?dist nsid =
    if NSeq_dom.is_empty env nsid then None
    else
      let m =
        match NSeq_dom.get_kvs env nsid with
        | None -> Id.M.empty
        | Some m -> (
            match dist with
            | None -> Id.M.map (fun (v, _, _) -> v) m
            | Some d ->
                let dn = DP.node_of_polynome env d in
                Id.M.fold
                  (fun k (v, _, _) acc ->
                    let knode = Id.get env k in
                    match DP.get_repr env knode with
                    | Some kp ->
                        let kp' = P.add kp d in
                        let knode' = DP.node_of_polynome env kp' in
                        Egraph.register env knode';
                        let k' = Id.Index.get_id_safe env knode' in
                        Id.M.add k' v acc
                    | None ->
                        let knode' = mk_add env knode dn in
                        Egraph.register env knode';
                        let k' = Id.Index.get_id_safe env knode' in
                        Id.M.add k' v acc)
                  m Id.M.empty)
      in
      let o = NSeq_dom.get_defval env nsid in
      Some (o, m)
  in
  fun env acc l ->
    match l with
    | [] -> (false, acc)
    | nsid :: t -> (
        match get_nseq_val_gty env (Id.get env nsid) with
        | exception Not_found -> (false, acc)
        | gty -> (
            match Relocate.get_repr env nsid with
            | Repr _ -> (
                match get_vals env nsid with
                | None -> (true, Ground.Ty.M.empty)
                | Some (o, m) ->
                    mk_eqs env
                      (Ground.Ty.M.change
                         (function
                           | None -> Some [ (nsid, o, m) ]
                           | Some l -> Some ((nsid, o, m) :: l))
                         gty acc)
                      t)
            | NonRepr { repr; dist } -> (
                match get_vals env ~dist repr with
                | None -> (true, Ground.Ty.M.empty)
                | Some (o, m) ->
                    mk_eqs env
                      (Ground.Ty.M.change
                         (function
                           | None -> Some [ (nsid, o, m) ]
                           | Some l -> Some ((nsid, o, m) :: l))
                         gty acc)
                      t)))

let compare_kvs =
  let rec compare_kwitness_vals env s m1 m2 =
    if Id.S.is_empty s then 1
    else
      let k = Id.S.min_elt s in
      let s' = Id.S.remove k s in
      match (Id.M.find_remove k m1, Id.M.find_remove k m2) with
      | (m1', Some v1), (m2', Some v2) ->
          let r = eq_vals env v1 v2 in
          if r <= 0 then r else compare_kwitness_vals env s' m1' m2'
      | _ -> 0
  in
  let rec aux env l m1 m2 =
    match l with
    | [] -> 0
    | h :: t when Id.S.is_empty h -> aux env t m1 m2
    | h :: t ->
        let r = compare_kwitness_vals env h m1 m2 in
        if r <> 0 then r else aux env t m1 m2
  in
  fun f env ~mk_gets id1 m1 id2 m2 ->
    let kpaths = WEGraph.get_paths env id1 id2 in
    Debug.dprintf7 debug "compare_kvs %a %a: kpaths: [%a]:%d" (Id.pp_nid env)
      id1 (Id.pp_nid env) id2
      (Fmt.list ~sep:Fmt.comma (fun fmt s -> Fmt.pf fmt "{%a}" Id.S.pp s))
      kpaths (List.length kpaths);
    let r = aux env kpaths m1 m2 in
    if r <> 0 then r else f env ~mk_gets id1 m1 id2 m2

let check_eq_nseqs_aux =
  let rec aux env ~mk_gets fst lst = function
    | [] -> ()
    | (id, o, m) :: t -> (
        match Id.get env id with
        | exception Not_found -> aux env ~mk_gets fst lst t
        | nid ->
            let t' =
              List.filter
                (fun (id', o', m') ->
                  let r =
                    compare_kvs
                      (fun env ~mk_gets id1 m1 id2 m2 ->
                        compare_kvs_naive env ~mk_gets fst lst id1 o m1 id2 o'
                          m2)
                      env ~mk_gets id m id' m'
                  in
                  Debug.dprintf13 debug
                    "compare_kvs_res (%a: %a (%a)) (%a: %a (%a)) = %d" Id.pp id
                    (Fmt.option Node.pp) o (Id.M.pp Node.pp) m Id.pp id'
                    (Fmt.option Node.pp) o' (Id.M.pp Node.pp) m' r;
                  if r = 1 then (
                    Egraph.merge env nid (Id.get env id');
                    false)
                  else (
                    if r = -1 then make_disequal env nid (Id.get env id');
                    true))
                t
            in
            aux env ~mk_gets fst lst t')
  in
  let empty_case env nsl =
    Debug.dprintf2 debug "empty_case [%a]" (Fmt.list ~sep:Fmt.comma Id.pp) nsl;
    let nsl =
      List.filter_map
        (fun id ->
          match Id.get env id with hn -> Some hn | exception Not_found -> None)
        nsl
    in
    match nsl with
    | [] -> ()
    | hn :: t -> List.iter (fun n -> Egraph.merge env hn n) t
  in
  fun env ~mk_gets fid lid s ->
    (* TODO:
       - all empty nseqs are equal with equal bounds
       - all nseqs with distinct bounds are unequal
       - modulo relocations
        (probably by using polynomes to avoid creating new indices)
    *)
    Debug.dprintf6 debug "check_eq_nseqs_aux [%a; %a] {%a}" (Id.pp_nid env) fid
      (Id.pp_nid env) lid Id.S.pp s;
    let nsl = Id.S.elements s in
    let is_empty, glm = mk_eqs env Ground.Ty.M.empty nsl in
    if is_empty then empty_case env nsl
    else
      let fst = Id.get env fid in
      let lst = Id.get env lid in
      Ground.Ty.M.iter (fun _ l -> aux env ~mk_gets fst lst l) glm

let check_eq_nseqs ~mk_gets env =
  Debug.dprintf1 debug "check_eq_nseqs %b" mk_gets;
  Bounds.iter env (check_eq_nseqs_aux ~mk_gets)

let rec mk_eqs_arrs env acc nsl =
  match nsl with
  | [] -> acc
  | h :: t ->
      let m =
        match NSeq_dom.get_kvs env h with
        | None -> Id.M.empty
        | Some m -> Id.M.map (fun (v, _, _) -> v) m
      in
      mk_eqs_arrs env ((h, m) :: acc) t

let check_eq_arrs_aux =
  let rec aux env = function
    | [] -> ()
    | (id, m) :: t ->
        let nid = Id.get env id in
        let t' =
          List.filter
            (fun (id', m') ->
              let r =
                compare_kvs
                  (fun env ~mk_gets:_ _id1 m1 _id2 m2 ->
                    if Id.M.is_empty m1 && Id.M.is_empty m2 then
                      (* TODO: decide on whether they are equal or not by using
                         the extensionality rule? *)
                      0
                    else
                      Id.M.fold2_union
                        (fun _ a_opt b_opt acc ->
                          if acc <> 1 then acc
                          else
                            match (a_opt, b_opt) with
                            | Some a, Some b -> eq_vals env a b
                            (* TODO: what should be done with missing indices? *)
                            | _ -> 0)
                        m1 m2 1)
                  env ~mk_gets:false id m id' m'
              in
              Debug.dprintf9 debug "compare_kvs_arrs_res (%a: %a) (%a: %a) = %d"
                Id.pp id (Id.M.pp Node.pp) m Id.pp id' (Id.M.pp Node.pp) m' r;
              if
                r = 1
                && ((not
                       (Options.get env
                          Colibri2_theories_utils.Array_opt.array_use_nseqs))
                   || WEGraph.are_linked env id id')
              then (
                (* TODO: check bounds when working with NSeqs? *)
                Egraph.merge env nid (Id.get env id');
                false)
              else (
                if r = -1 then make_disequal env nid (Id.get env id');
                true))
            t
        in
        aux env t'
  in
  fun env s ->
    let nsl = Id.S.elements s in
    let glm = mk_eqs_arrs env [] nsl in
    aux env glm

let check_eq_arrs env =
  Debug.dprintf0 debug "check_eq_arrs";
  NS_arrays.iter_arrays env (fun env _ ids -> check_eq_arrs_aux env ids)

let pp_np_opt = Fmt.option ~none:(fun fmt () -> Fmt.pf fmt "None") Node.pp

let pp_missing_vals fmt m =
  Fmt.pf fmt "{%a}"
    (fun fmt m ->
      Id.M.iter
        (fun i (v1opt, v2opt) ->
          Fmt.pf fmt "%a -> (%a, %a);" Id.pp i pp_np_opt v1opt pp_np_opt v2opt)
        m)
    m

let pp_decs fmt m =
  Fmt.pf fmt "{%a}"
    (fun fmt m ->
      Id.M.iter (fun i dec -> Fmt.pf fmt "%a -> (%a);" Id.pp i pp_dec dec) m)
    m

let choose_lowest_deccount decl =
  match
    List.sort
      (fun ({ dec_count = n1 } : ppg_dec) { dec_count = n2 } ->
        Int.compare n1 n2)
      decl
  with
  | [] -> None
  | h :: _ -> Some h

let compare_sel_vals env i v1 v2 eqinds neqinds decs =
  if Egraph.is_equal env v1 v2 then
    (Some true, Id.S.add i eqinds, neqinds, Id.M.remove i decs)
  else if Equality.is_disequal env v1 v2 then
    (Some false, eqinds, Id.S.add i neqinds, Id.M.remove i decs)
  else (None, eqinds, neqinds, add_val_eq_dec i false v1 v2 decs)

let check_sel_eq env i v1_opt v2_opt eqinds neqinds mvals decs =
  match (v1_opt, v2_opt) with
  | None, None | Some _, None | None, Some _ ->
      ( None,
        eqinds,
        neqinds,
        Id.M.add i
          ( Opt.fold (fun _ (v, _) -> Some v) None v1_opt,
            Opt.fold (fun _ (v, _) -> Some v) None v2_opt )
          mvals,
        decs )
  | Some (v1, _), Some (v2, _) -> (
      match compare_sel_vals env i v1 v2 eqinds neqinds decs with
      | None, eqinds, neqinds, decs ->
          ( None,
            eqinds,
            neqinds,
            Id.M.add i (Some v1, Some v2) mvals,
            add_val_eq_dec i false v1 v2 decs )
      | res, eqinds, neqinds, decs -> (res, eqinds, neqinds, mvals, decs))

let rec check_present_selects env id1 m1 id2 m2 pset eqinds neqinds mvals decs
    inds =
  match Id.S.min_elt inds with
  | exception Not_found -> (1, pset, eqinds, neqinds, mvals, decs)
  | i when Id.M.mem i mvals ->
      let r, pset, eqinds, neqinds, mvals, decs =
        check_present_selects env id1 m1 id2 m2 (Id.S.add i pset) eqinds neqinds
          mvals decs (Id.S.remove i inds)
      in
      (min r 0, pset, eqinds, neqinds, mvals, decs)
  | i when Id.M.mem i eqinds ->
      assert (not (Id.M.mem i mvals));
      check_present_selects env id1 m1 id2 m2 pset eqinds neqinds mvals decs
        (Id.S.remove i inds)
  | i when Id.M.mem i neqinds ->
      assert (not (Id.M.mem i mvals));
      (-1, pset, eqinds, neqinds, mvals, decs)
  | i -> (
      let v1_opt = Id.M.find_opt i m1 in
      let v2_opt = Id.M.find_opt i m2 in
      match check_sel_eq env i v1_opt v2_opt eqinds neqinds mvals decs with
      | Some true, eqinds, neqinds, mvals, decs ->
          check_present_selects env id1 m1 id2 m2 pset eqinds neqinds mvals decs
            (Id.S.remove i inds)
      | None, eqinds, neqinds, mvals, decs ->
          let r, pset, eqinds, neqinds, mvals, decs =
            check_present_selects env id1 m1 id2 m2 (Id.S.add i pset) eqinds
              neqinds mvals decs (Id.S.remove i inds)
          in
          (min r 0, pset, eqinds, neqinds, mvals, decs)
      | Some false, eqinds, neqinds, mvals, decs ->
          (-1, pset, eqinds, neqinds, mvals, decs))

let rec check_present_selects_l env ~is_diseq id1 m1 id2 m2 psetl eqinds neqinds
    mvals decs paths =
  (* Debug.dprintf2 debug "|> psetl = [%a]"
     (Fmt.list ~sep:Fmt.comma (fun fmt (c, s) ->
          Fmt.pf fmt "(%d, {%a})" c Id.S.pp s))
     psetl; *)
  match paths with
  | [] -> (0, psetl, Id.S.empty, Id.S.empty, mvals, decs)
  | path :: t -> (
      let inds =
        List.fold_left (fun acc (i, _, _, _) -> Id.S.add i acc) Id.S.empty path
      in
      let r, pset, eqinds, neqinds, mvals, decs =
        check_present_selects env id1 m1 id2 m2 Id.S.empty eqinds neqinds mvals
          decs inds
      in
      (* Debug.dprintf9 debug "check_present_selects %a{%a} %a{%a} = %d " Id.pp id1
           (Id.M.pp (fun fmt (v, _) -> Node.pp fmt v))
           m1 Id.pp id2
           (Id.M.pp (fun fmt (v, _) -> Node.pp fmt v))
           m2 r;
         Debug.dprintf10 debug
           ">> pset={%a} eqinds={%a} neqinds={%a} mvals={%a} decs={%a}" Id.S.pp
           pset Id.S.pp eqinds Id.S.pp neqinds pp_missing_vals mvals pp_decs decs; *)
      match is_diseq with
      | true ->
          if r = 1 then Egraph.contradiction ()
          else if r = -1 then
            check_present_selects_l env ~is_diseq id1 m1 id2 m2 psetl eqinds
              neqinds mvals decs t
          else
            check_present_selects_l env ~is_diseq id1 m1 id2 m2
              ((Id.S.cardinal pset, pset) :: psetl)
              eqinds neqinds mvals decs t
      | false ->
          if r = 1 then (1, [], Id.S.empty, Id.S.empty, mvals, decs)
          else if r = -1 then
            let tres, psetl, eqinds, neqinds, mvals, decs =
              check_present_selects_l env ~is_diseq id1 m1 id2 m2 psetl eqinds
                neqinds mvals decs t
            in
            if tres = 1 then Egraph.contradiction ()
            else (-1, psetl, eqinds, neqinds, mvals, decs)
          else
            check_present_selects_l env ~is_diseq id1 m1 id2 m2
              ((Id.S.cardinal pset, pset) :: psetl)
              eqinds neqinds mvals decs t)

let filter_psetl is_diseq eqinds neqinds acc paths =
  let rec aux is_diseq eqinds neqinds acc paths =
    match paths with
    | [] -> (0, (* UWEGraph.filter_paths *) acc)
    | (c, ids) :: t -> (
        try
          let c, ids =
            Id.S.fold
              (fun id (c, acc) ->
                if Id.S.mem id eqinds then (c - 1, acc)
                else if Id.S.mem id neqinds then raise Exit
                else (c, Id.S.add id acc))
              ids (c, Id.S.empty)
          in
          if c = 0 then if is_diseq then Egraph.contradiction () else (1, [])
          else aux is_diseq eqinds neqinds ((c, ids) :: acc) t
        with Exit ->
          let r, acc = aux is_diseq eqinds neqinds acc t in
          if r = 1 then Egraph.contradiction () else (-1, acc))
  in
  if (not (Id.S.is_empty neqinds)) && not is_diseq then (-1, [])
  else aux is_diseq eqinds neqinds acc paths

(* TODO: when checking weq, it is probably not necessary to
   recompute paths, it should be enough to check psetl *)
let check_weq env id1 id2 mvals decs =
  if Id.M.is_empty mvals then (0, Id.S.empty, Id.M.empty, decs)
  else
    let r, eqinds, mvals, decs =
      Id.M.fold
        (fun i mval (r, eqinds, mv_acc, decs) ->
          if Id.S.mem i eqinds || Id.M.mem i mv_acc then
            (r, eqinds, mv_acc, decs)
          else
            try
              let r', jnode_opt = NS_arrays.is_weq_mod_i_v1 env id1 id2 i in
              if r' = 1 then
                match mval with
                | Some v1, Some v2 ->
                    Egraph.merge env v1 v2;
                    (min r 1, Id.S.add i eqinds, mv_acc, Id.M.remove i decs)
                | _ -> (min r 1, Id.S.add i eqinds, mv_acc, decs)
              else if r' = 0 then
                let dec_count, jnode =
                  match jnode_opt with
                  | None -> assert false
                  | Some (c, j) -> (c, j)
                in
                (* If we're going to decide on weak equivalency, we won't be making
                   the other decisions to find missing values yet *)
                ( 0,
                  eqinds,
                  Id.M.add i mval mv_acc,
                  add_weq_dec i dec_count (Id.get env i) jnode decs )
              else (0, eqinds, Id.M.add i mval mv_acc, decs)
            with Not_found -> (0, eqinds, Id.M.add i mval mv_acc, decs))
        mvals
        (1, Id.S.empty, Id.M.empty, decs)
    in
    (r, eqinds, mvals, decs)

let check_weq_l env ~is_diseq id1 id2 psetl mvals decs =
  (* Debug.dprintf6 debug "check_weq_l %a %a {%a}" Id.pp id1 Id.pp id2
     pp_missing_vals mvals; *)
  let r, eqinds, mvals, decs = check_weq env id1 id2 mvals decs in
  (* Debug.dprintf5 debug "check_weq %d eqinds={%a} mvals={%a}" r Id.S.pp eqinds
     pp_missing_vals mvals; *)
  if r = 1 then
    if is_diseq then Egraph.contradiction () else (1, [], Id.M.empty, Id.M.empty)
  else
    let r, psetl = filter_psetl is_diseq eqinds Id.S.empty [] psetl in
    (* Debug.dprintf3 debug "check_weq.aux %d psetl = [%a]" r
       (Fmt.list ~sep:Fmt.comma (fun fmt (c, s) ->
            Fmt.pf fmt "(%d, {%a})" c Id.S.pp s))
       psetl; *)
    if r = 1 then
      if is_diseq then Egraph.contradiction ()
      else (1, [], Id.M.empty, Id.M.empty)
    else (r, psetl, mvals, decs)

let fetch_selects env id1 id2 eqinds neqinds mvals decs =
  Id.M.fold
    (fun i (v1_opt, v2_opt) (eqinds, neqinds, decs) ->
      match (v1_opt, v2_opt) with
      | Some v1, Some v2 ->
          assert (
            (not (Egraph.is_equal env v1 v2))
            && not (Equality.is_disequal env v1 v2));
          assert (
            match Id.M.find_opt i decs with
            | Some { val_eq = Some (false, v1', v2') } ->
                Egraph.is_equal env v1 v1' && Egraph.is_equal env v2 v2'
            | _ -> false);
          (eqinds, neqinds, decs)
      | None, Some v1 | Some v1, None -> (
          let id' = if Option.is_none v1_opt then id1 else id2 in
          let ppg_decs, v2opt = NS_arrays.get_weg_val env ~ov:v1 id' i in
          match v2opt with
          | Some v2 ->
              let _, eqinds, neqinds, decs =
                compare_sel_vals env i v1 v2 eqinds neqinds decs
              in
              (eqinds, neqinds, decs)
          | None -> (
              match choose_lowest_deccount ppg_decs with
              | None -> (eqinds, neqinds, decs)
              | Some ppg_dec -> (eqinds, neqinds, add_ppg1_dec i ppg_dec decs)))
      | None, None -> (
          let ppg_decs1, vopt1 = NS_arrays.get_weg_val env id1 i in
          let ppg_decs2, vopt2 = NS_arrays.get_weg_val env id2 i in
          match (vopt1, vopt2) with
          | Some v1, Some v2 ->
              let _, eqinds, neqinds, decs =
                compare_sel_vals env i v1 v2 eqinds neqinds decs
              in
              (eqinds, neqinds, decs)
          | Some _, None | None, Some _ -> (
              let ppg_decs, v =
                if Option.is_some vopt1 then (ppg_decs2, Option.get vopt1)
                else (ppg_decs1, Option.get vopt2)
              in
              assert (
                match Id.M.find_opt i decs with
                | None | Some { ppg2 = None; ppg1 = None } -> true
                | _ -> false);
              match choose_lowest_deccount ppg_decs with
              | None -> (eqinds, neqinds, decs)
              | Some ppg_dec ->
                  assert (Option.is_none ppg_dec.ov);
                  ( eqinds,
                    neqinds,
                    add_ppg1_dec i { ppg_dec with ov = Some v } decs ))
          | None, None -> (
              assert (
                match Id.M.find_opt i decs with
                | None | Some { ppg2 = None; ppg1 = None } -> true
                | _ -> false);
              match
                ( choose_lowest_deccount ppg_decs1,
                  choose_lowest_deccount ppg_decs2 )
              with
              | Some ppg_dec1, Some ppg_dec2 ->
                  (eqinds, neqinds, add_ppg2_dec i ppg_dec1 ppg_dec2 decs)
              | _ -> (eqinds, neqinds, decs))))
    mvals (eqinds, neqinds, decs)

let fetch_selects_l env ~is_diseq id1 id2 psetl mvals decs =
  (* Debug.dprintf10 debug "fetch_selects %a %a psetl={%a} mvals={%a} decs={%a}"
     Id.pp id1 Id.pp id2
     (Fmt.list ~sep:Fmt.comma (fun fmt (c, s) ->
          Fmt.pf fmt "(%d, {%a})" c Id.S.pp s))
     psetl pp_missing_vals mvals pp_decs decs; *)
  let eqinds, neqinds, decs =
    fetch_selects env id1 id2 Id.S.empty Id.S.empty mvals decs
  in
  (* Debug.dprintf6 debug "fetch_selects eqinds={%a} neqinds={%a} decs=[%a]"
     Id.S.pp eqinds Id.S.pp neqinds pp_decs decs; *)
  let r, psetl = filter_psetl is_diseq eqinds neqinds [] psetl in
  (* Debug.dprintf6 debug "fetch_selects.aux eqinds={%a} neqinds={%a} decs=[%a]"
     Id.S.pp eqinds Id.S.pp neqinds pp_decs decs; *)
  if r = 1 then if is_diseq then Egraph.contradiction () else (1, [], Id.M.empty)
  else (r, psetl, decs)

let dec_union_to_decfun = function
  | VEq (false, v1, v2) -> (true, fun env -> decide_equality env v1 v2)
  | VEq (true, v1, v2) ->
      ( true,
        fun env ->
          decide_equality env
            ~dec_check:(fun env anode bnode ->
              NS_arrays.has_distinct_val env (Id.NSeq.get_id env anode)
                (Id.NSeq.get_id env bnode))
            v1 v2 )
  | Weq { inode; jnode } ->
      ( false,
        fun env ->
          decide_equality env
            ~dec_check:(fun env inode jnode ->
              NS_arrays.are_holding_dist_vals env
                (Id.Index.get_id env inode)
                (Id.Index.get_id env jnode))
            inode jnode )
  | Ppg1 { src; inode; dst; v; curr_node } ->
      ( false,
        fun env ->
          NS_arrays.ppg_over_indices_decision env src inode dst v curr_node )
  | Ppg2
      ( { src = s1; inode = i1; dst = d1; v = v1; curr_node = cn1 },
        { src = s2; inode = i2; dst = d2; v = v2; curr_node = cn2 } ) ->
      ( false,
        fun env ->
          NS_arrays.ppg_over_indices_decision env s1 i1 d1 v1 cn1;
          NS_arrays.ppg_over_indices_decision env s2 i2 d2 v2 cn2 )

let check_map_arrs_no_ppg_aux env ~is_diseq id1 m1 id2 m2 :
    bool * bool * int * (Egraph.wt -> unit) list * (Egraph.wt -> unit) list =
  Debug.dprintf4 debug "check_map_arrs_no_ppg_aux %a %a" (Id.pp_nid env) id1
    (Id.pp_nid env) id2;
  let paths = UWEGraph.all_paths env ~filter:false id1 (Id.S.singleton id2) in
  let r, psetl, _, _, mvals, decs =
    check_present_selects_l env ~is_diseq id1 m1 id2 m2 [] Id.S.empty Id.S.empty
      Id.M.empty Id.M.empty paths
  in
  assert (psetl <> [] || r <> 0 || is_diseq);
  assert ((not is_diseq) || r <> 1);
  Debug.dprintf7 debug "check_present_selects_l %a %a = %d mvals=%a" Id.pp id1
    Id.pp id2 r pp_missing_vals mvals;
  Debug.dprintf2 debug "psetl = [%a]"
    (Fmt.list ~sep:Fmt.comma (fun fmt (c, s) ->
         Fmt.pf fmt "(%d, {%a})" c Id.S.pp s))
    psetl;
  if r = 1 then (false, is_diseq, r, [], [])
  else if psetl = [] then (false, is_diseq, r, [], [])
  else
    let is_diseq = if r = -1 && not is_diseq then true else is_diseq in
    let r, psetl, mvals, decs =
      check_weq_l env ~is_diseq id1 id2 psetl mvals decs
    in
    assert (psetl <> [] || r <> 0 || is_diseq);
    assert ((not is_diseq) || r <> 1);
    Debug.dprintf6 debug "check_weq_l %d %b decl=[%a] mvals=%a" r is_diseq
      pp_decs decs pp_missing_vals mvals;
    Debug.dprintf2 debug "psetl = [%a]"
      (Fmt.list ~sep:Fmt.comma (fun fmt (c, s) ->
           Fmt.pf fmt "(%d, {%a})" c Id.S.pp s))
      psetl;
    if r = 1 then (false, is_diseq, r, [], [])
    else if psetl = [] then (false, is_diseq, r, [], [])
    else
      let r, psetl, decs =
        fetch_selects_l env ~is_diseq id1 id2 psetl mvals decs
      in
      assert (psetl <> [] || r <> 0 || is_diseq);
      assert ((not is_diseq) || r <> 1);
      Debug.dprintf4 debug "fetch_selects_l %d %b decl=[%a]" r is_diseq pp_decs
        decs;
      Debug.dprintf2 debug "psetl = [%a]"
        (Fmt.list ~sep:Fmt.comma (fun fmt (c, s) ->
             Fmt.pf fmt "(%d, {%a})" c Id.S.pp s))
        psetl;
      let is_diseq = if r = -1 && not is_diseq then true else is_diseq in
      match psetl with
      | [] ->
          (* If no paths allow determining equality, and disequality is no known
             then decide on equality (+) *)
          if r = 0 && not is_diseq then
            ( false,
              false,
              0,
              [],
              [
                (fun env ->
                  decide_equality env
                    ~dec_check:(fun env anode bnode ->
                      NS_arrays.has_distinct_val env (Id.NSeq.get_id env anode)
                        (Id.NSeq.get_id env bnode))
                    (Id.get env id1) (Id.get env id2));
              ] )
          else (false, is_diseq, r, [], [])
      | _ -> (
          let split_count, path =
            psetl
            |> List.sort (fun (c1, _) (c2, _) -> Int.compare c1 c2)
            |> List.hd
          in
          if is_diseq && split_count = 1 then (
            let i = Id.S.min_elt path in
            assert (Id.S.cardinal path = 1);
            match Id.M.find_opt i decs with
            | None ->
                (* If no decision allows determining equality on some index and
                   disequality is not known, then decide on array equality (+)
                *)
                if r = 0 && not is_diseq then
                  ( false,
                    false,
                    0,
                    [],
                    [
                      (fun env ->
                        decide_equality env
                          ~dec_check:(fun env anode bnode ->
                            NS_arrays.has_distinct_val env
                              (Id.NSeq.get_id env anode)
                              (Id.NSeq.get_id env bnode))
                          (Id.get env id1) (Id.get env id2));
                    ] )
                else (false, true, -1, [], [])
            | Some ({ val_eq; weq; ppg1 } as dec) ->
                (* ALL decisions in which there is one split can be propagated in contraposition *)
                let made_ppg =
                  false
                  |> Opt.fold_right
                       (fun (b, v1, v2) _ ->
                         assert (not b);
                         make_disequal env v1 v2;
                         true)
                       val_eq
                  |> Opt.fold_right
                       (fun { dec_count; inode; jnode } b ->
                         if dec_count = 1 then (
                           Egraph.merge env inode jnode;
                           true)
                         else b)
                       weq
                  |> Opt.fold_right
                       (fun { dec_count; inode; next_ind; v = v1; ov } b ->
                         if dec_count = 1 then b
                         else
                           Opt.fold
                             (fun _ v2 ->
                               if Egraph.is_equal env v1 v2 then (
                                 Egraph.merge env inode next_ind;
                                 true)
                               else b)
                             true ov)
                       ppg1
                in
                if made_ppg then (true, is_diseq, r, [], [])
                else
                  let _, dec_u = choose_decision dec in
                  let is_val_dec, dec = dec_union_to_decfun dec_u in
                  let ind_decs, val_decs =
                    if is_val_dec then ([], [ dec ]) else ([ dec ], [])
                  in
                  (false, is_diseq, r, ind_decs, val_decs))
          else
            try
              let _, ind_decs, val_decs =
                Id.S.fold
                  (fun i (acc, ind_decs, val_decs) ->
                    if Id.S.mem i acc then (acc, ind_decs, val_decs)
                    else
                      let is_val_dec, dec =
                        match Id.M.find_opt i decs with
                        | None ->
                            (* TODO: should other index decision really be
                               ignored? *)
                            raise Exit
                        | Some dec ->
                            dec_union_to_decfun (snd (choose_decision dec))
                      in
                      let ind_decs, val_decs =
                        if is_val_dec then (ind_decs, dec :: val_decs)
                        else (dec :: ind_decs, val_decs)
                      in
                      (Id.S.add i acc, ind_decs, val_decs))
                  path (Id.S.empty, [], [])
              in
              (false, is_diseq, r, ind_decs, val_decs)
            with Exit ->
              (* Means that no decision can allow the detection of equality (+)
              *)
              if r = 0 && not is_diseq then
                ( false,
                  false,
                  0,
                  [],
                  [
                    (fun env ->
                      decide_equality env (Id.get env id1) (Id.get env id2));
                  ] )
              else (false, true, -1, [], []))
(* (+): necessary to avoid unknown-branch-cut in the sat case, but overall
   solves less unsat cases *)

let add_select_ppg_dec last dec_count anode inode next_node av curr_node
    next_ind lv m =
  Id.M.change
    (function
      | None ->
          Some
            {
              dec_count;
              src = anode;
              inode;
              dst = next_node;
              v = av;
              curr_node;
              next_ind;
              ov = Some lv;
            }
      | Some (({ dec_count = dc; _ } : ppg_dec) as ppg_dec) ->
          if dc > dec_count then
            Some
              {
                dec_count;
                src = anode;
                inode;
                dst = next_node;
                v = av;
                curr_node;
                next_ind;
                ov = Some lv;
              }
          else Some ppg_dec)
    last m

let check_selects_ppg =
  (* let add_ex_arr i a m =
       match Id.M.find_opt i m with
       | None -> Id.M.add i (Id.S.singleton a) m
       | Some s -> Id.M.add i (Id.S.add a s) m
     in *)
  let rec aux env ex_arrs ppg_decs ppg_unk_vals val_decs = function
    | [] -> (ppg_decs, ppg_unk_vals, val_decs)
    | (aid, anode) :: idlt -> (
        match NS_arrays.get_selects env aid with
        | None -> aux env ex_arrs ppg_decs ppg_unk_vals val_decs idlt
        | Some amap ->
            let ppg_decs, ppg_unk_vals, val_decs =
              Id.M.fold
                (fun i (av, _) (ppg_decs, ppg_unk_vals, val_decs) ->
                  let inode = Id.get env i in
                  match NS_arrays.get_selects_ind env i with
                  | None -> assert false
                  | Some imap -> (
                      Debug.dprintf6 debug ">>> %a | %a -> {%a}" Id.pp aid Id.pp
                        i
                        (Id.M.pp (fun fmt (v, b) ->
                             Fmt.pf fmt "(%a, %b)" Node.pp v b))
                        imap;
                      let dsts =
                        List.fold_left
                          (fun s d -> Id.S.add d s)
                          Id.S.empty
                          (Id.M.keys (Id.M.remove aid imap))
                      in
                      if Id.S.is_empty dsts then
                        (ppg_decs, ppg_unk_vals, val_decs)
                      else
                        let paths =
                          try
                            UWEGraph.all_paths env ~filter:false ~ex_arrs
                              ~ex_ind:i aid dsts
                            |> List.filter (fun path ->
                                   let lid, _ = UWEGraph.last_edge path in
                                   let lv, _ = Id.M.find lid imap in
                                   not (Egraph.is_equal env av lv))
                          with Not_found -> []
                        in
                        match paths with
                        | [] -> (ppg_decs, ppg_unk_vals, val_decs)
                        | _ -> (
                            match
                              NS_arrays.get_and_explore_paths env ~paths anode
                                aid dsts inode i av
                            with
                            | _, [] -> (ppg_decs, ppg_unk_vals, val_decs)
                            | true, _ -> assert false
                            | false, pcnl ->
                                let ppg_decs, ppg_unk_vals, val_decs =
                                  List.fold_left
                                    (fun (ppg_decs, ppg_unk_vals, val_decs)
                                         (dcount, path, curr_n, next_i, next_n)
                                       ->
                                      match path with
                                      | [] -> assert false
                                      | (_, next_ind, nnid, _) :: _ ->
                                          let skip =
                                            match
                                              NS_arrays.get_val env nnid i
                                            with
                                            | None -> false
                                            | Some nnv ->
                                                Equality.is_disequal env av nnv
                                          in
                                          if skip then (
                                            Egraph.merge env next_ind inode;
                                            (ppg_decs, ppg_unk_vals, val_decs))
                                          else
                                            let last, _ =
                                              UWEGraph.last_edge path
                                            in
                                            let lv, _ = Id.M.find last imap in
                                            if Equality.is_disequal env av lv
                                            then
                                              ( add_select_ppg_dec last dcount
                                                  anode inode next_n av curr_n
                                                  next_i lv ppg_decs,
                                                ppg_unk_vals,
                                                val_decs )
                                            else if Egraph.is_equal env av lv
                                            then
                                              (ppg_decs, ppg_unk_vals, val_decs)
                                            else
                                              ( ppg_decs,
                                                add_select_ppg_dec last dcount
                                                  anode inode next_n av curr_n
                                                  next_i lv ppg_unk_vals,
                                                (av, lv) :: val_decs ))
                                    (ppg_decs, ppg_unk_vals, val_decs)
                                    pcnl
                                in
                                (ppg_decs, ppg_unk_vals, val_decs))))
                amap
                (ppg_decs, ppg_unk_vals, val_decs)
            in
            aux env ex_arrs ppg_decs ppg_unk_vals val_decs idlt)
  in
  fun env ->
    Trace.with_span ~__FUNCTION__ ~__FILE__ ~__LINE__
      "NSeq.Array.WE.CheckSelectPpg"
    @@ fun _ ->
    Debug.dprintf0 debug "check_selects_ppg";
    let ppg_decs, ppg_unk_vals, val_decs =
      NS_arrays.fold_arrays env
        (fun env _ ids (ppg_decs, ppg_unk_vals, val_decs) ->
          let idl = UWEGraph.filter_by_comp env ids in
          List.fold_left
            (fun (ppg_decs, ppg_unk_vals, val_decs) idl ->
              let idln = List.map (fun id -> (id, Id.get env id)) idl in
              let ppg_decs_m, ppg_unk_vals_m, new_val_decs =
                aux env Id.S.empty Id.M.empty Id.M.empty [] idln
              in
              let ppg_decs =
                Id.M.fold
                  (fun _ ppg_dec acc ->
                    snd (dec_union_to_decfun (Ppg1 ppg_dec)) :: acc)
                  ppg_decs_m ppg_decs
              in
              let ppg_unk_vals =
                Id.M.fold
                  (fun _ ppg_dec acc ->
                    snd (dec_union_to_decfun (Ppg1 ppg_dec)) :: acc)
                  ppg_unk_vals_m ppg_unk_vals
              in
              let val_decs =
                List.fold_left
                  (fun acc (v1, v2) ->
                    snd (dec_union_to_decfun (VEq (false, v1, v2))) :: acc)
                  val_decs new_val_decs
              in
              (ppg_decs, ppg_unk_vals, val_decs))
            (ppg_decs, ppg_unk_vals, val_decs)
            idl)
        ([], [], [])
    in
    (ppg_decs, ppg_unk_vals, val_decs)

let arr_eqs_no_ppg =
  let rm_treated id l =
    List.filter
      (fun (id1, id2) -> (not (Id.equal id id1)) && not (Id.equal id id2))
      l
  in
  let new_treated_ids =
    let aux id l treated =
      if Id.S.mem id treated then (l, treated)
      else (rm_treated id l, Id.S.add id treated)
    in
    fun id1 id2 l treated ->
      let l, treated = aux id1 l treated in
      aux id2 l treated
  in
  let rec aux env ~distinct_only made_ppg_acc ind_dec_acc val_dec_acc
      aeq_dec_acc = function
    | [] -> (made_ppg_acc, ind_dec_acc, val_dec_acc, aeq_dec_acc)
    | (id, m) :: t ->
        Debug.dprintf4 debug "arr_eqs_no_ppg aux: %a [%a]" Id.pp id
          (Fmt.list ~sep:Fmt.comma (fun fmt (id, _) -> Id.pp fmt id))
          t;
        let t', made_ppg_acc, ind_dec_acc, val_dec_acc, aeq_dec_acc, _ =
          List.fold_left
            (fun ( t_acc,
                   made_ppg_acc,
                   ind_dec_acc,
                   val_dec_acc,
                   aeq_dec_acc,
                   treated ) (id', m') ->
              let nid = Id.get env id in
              let nid' = Id.get env id' in
              let is_diseq = Equality.is_disequal env nid nid' in
              Debug.dprintf2 debug ">>> %b %b" (not distinct_only) is_diseq;
              if (not distinct_only) || is_diseq then (
                let made_ppg, new_is_diseq, r, ind_decs, val_decs =
                  check_map_arrs_no_ppg_aux env ~is_diseq id m id' m'
                in
                let made_ppg_acc = made_ppg || made_ppg_acc in
                Debug.dprintf9 debug
                  ">>> made_ppg_acc=%b ind_decs=[%d] val_decs=[%d] \
                   ind_dec_acc:%d=[%a] val_dec_acc:%d=[%a]"
                  made_ppg_acc (List.length ind_decs) (List.length val_decs)
                  (List.length ind_dec_acc)
                  (Fmt.list ~sep:Fmt.comma (fun fmt l ->
                       Fmt.pf fmt "%d" (List.length l)))
                  ind_dec_acc (List.length val_dec_acc)
                  (Fmt.list ~sep:Fmt.comma (fun fmt l ->
                       Fmt.pf fmt "%d" (List.length l)))
                  val_dec_acc;
                Debug.dprintf5 debug "check_map_arrs_no_ppg_aux %a %a = %d"
                  (Id.pp_nid env) id (Id.pp_nid env) id' r;
                if r = 1 then (
                  let aeq_dec_acc, treated =
                    new_treated_ids id id' aeq_dec_acc treated
                  in
                  Egraph.merge env nid nid';
                  (t_acc, true, ind_dec_acc, val_dec_acc, aeq_dec_acc, treated))
                else if r = -1 || (new_is_diseq && not is_diseq) then
                  let aeq_dec_acc, treated =
                    new_treated_ids id id' aeq_dec_acc treated
                  in
                  let made_pgg =
                    if is_diseq then false
                    else (
                      make_disequal env nid nid';
                      true)
                  in
                  ( (id', m') :: t_acc,
                    made_pgg,
                    ind_dec_acc,
                    val_dec_acc,
                    aeq_dec_acc,
                    treated )
                else
                  match (ind_decs, val_decs) with
                  | [], []
                    when (not (Id.S.mem id treated))
                         && not (Id.S.mem id' treated) ->
                      ( (id', m') :: t_acc,
                        made_ppg_acc,
                        ind_dec_acc,
                        val_dec_acc,
                        (id, id') :: aeq_dec_acc,
                        treated )
                  | _ ->
                      let ind_dec_acc =
                        match ind_decs with
                        | [] -> ind_dec_acc
                        | _ -> ind_decs :: ind_dec_acc
                      in
                      let val_dec_acc =
                        match val_decs with
                        | [] -> val_dec_acc
                        | _ -> val_decs :: val_dec_acc
                      in
                      ( (id', m') :: t_acc,
                        made_ppg_acc,
                        ind_dec_acc,
                        val_dec_acc,
                        aeq_dec_acc,
                        treated ))
              else
                ( (id', m') :: t_acc,
                  made_ppg_acc,
                  ind_dec_acc,
                  val_dec_acc,
                  aeq_dec_acc,
                  treated ))
            ([], made_ppg_acc, ind_dec_acc, val_dec_acc, aeq_dec_acc, Id.S.empty)
            t
        in
        aux env ~distinct_only made_ppg_acc ind_dec_acc val_dec_acc aeq_dec_acc
          (List.rev t')
  in
  let check_eq_arrs_no_ppg env ~distinct_only ids =
    Debug.dprintf2 debug "check_eq_arrs_no_ppg {%a}" Id.S.pp ids;
    let ll =
      List.map snd
      @@ Id.S.fold
           (fun id acc ->
             let m =
               match NS_arrays.get_selects env id with
               | None -> Id.M.empty
               | Some m -> m
             in
             match UWEGraph.get_repr env id with
             | exception Not_found -> acc
             | r ->
                 let rec filter_by_weg = function
                   | [] -> [ (r, [ (id, m) ]) ]
                   | (rr, l) :: t ->
                       if Id.equal r rr then (rr, (id, m) :: l) :: t
                       else (rr, l) :: filter_by_weg t
                 in
                 filter_by_weg acc)
           ids []
    in
    let made_ppg, ind_dec_acc, val_dec_acc, aeq_dec_ac =
      List.fold_left
        (fun (redo, ind_dec_acc, val_dec_acc, aeq_dec_acc) l ->
          let made_ppg, ind_dec_acc, val_dec_acc, aeq_dec_acc =
            aux env ~distinct_only redo ind_dec_acc val_dec_acc aeq_dec_acc l
          in
          (made_ppg, ind_dec_acc, val_dec_acc, aeq_dec_acc))
        (false, [], [], []) ll
    in
    let aeq_dec_ac =
      List.map
        (fun (id1, id2) ->
          snd
          @@ dec_union_to_decfun (VEq (true, Id.get env id1, Id.get env id2)))
        aeq_dec_ac
    in
    (made_ppg, ind_dec_acc, val_dec_acc, aeq_dec_ac)
  in
  fun ~distinct_only env ->
    Trace.with_span ~__FUNCTION__ ~__FILE__ ~__LINE__
      "NSeq.Array.WE.ArrayEqualityCheck"
    @@ fun _ ->
    NS_arrays.fold_arrays env
      (fun env _ ids (made_ppg_acc, ind_dec_acc, val_dec_acc, aeq_dec_acc) ->
        let made_ppg, ind_dec, val_dec, aeq_dec =
          check_eq_arrs_no_ppg env ~distinct_only ids
        in
        let ind_dec_acc = ind_dec @ ind_dec_acc in
        let val_dec_acc = val_dec @ val_dec_acc in
        let aeq_dec_acc = aeq_dec @ aeq_dec_acc in
        Debug.dprintf9 debug
          "arr_eqs_no_ppg made_ppg_acc=%b made_ppg=%b ind_dec_acc=[%a] \
           val_dec_acc=[%a] aeq_dec_ac=[%d] {%a}"
          made_ppg_acc made_ppg
          (Fmt.list ~sep:Fmt.comma (fun fmt l ->
               Fmt.pf fmt "%d" (List.length l)))
          ind_dec_acc
          (Fmt.list ~sep:Fmt.comma (fun fmt l ->
               Fmt.pf fmt "%d" (List.length l)))
          val_dec_acc (List.length aeq_dec_acc) Id.S.pp ids;
        (made_ppg || made_ppg_acc, ind_dec_acc, val_dec_acc, aeq_dec_acc))
      (false, [], [], [])

let dist_select_check env =
  Trace.with_span ~__FUNCTION__ ~__FILE__ ~__LINE__
    "NSeq.Array.WE.DistSelectsCheck"
  @@ fun _ ->
  NS_arrays.iter_selects env ~f:(fun _ m ->
      ignore
      @@ Id.M.fold
           (fun i (v, _) acc ->
             let acc = Id.M.remove i acc in
             Id.M.iter
               (fun j (v', _) ->
                 if Disequality.is_disequal env v v' then
                   make_disequal env (Id.get env i) (Id.get env j))
               acc;
             acc)
           m m)

let rec last_effort_arr_eqs_no_ppg ~distinct_only env =
  Trace.with_span ~__FUNCTION__ ~__FILE__ ~__LINE__ "NSeq.Array.WE.LastEffort"
  @@ fun _ ->
  Debug.dprintf1 debug "last_effort_arr_eqs_no_ppg %b" distinct_only;
  Debug.dprintf2 debug "%a" NS_arrays.pp env;
  Debug.dprintf2 debug "%a" UWEGraph.pp env;
  dist_select_check env;
  let made_ppg, ind_decs, val_decs, aeq_decs =
    arr_eqs_no_ppg ~distinct_only env
  in
  let ind_decs = List.concat ind_decs in
  let val_decs = List.concat val_decs in
  let dist_ppg_decs, unk_ppg_decs, select_val_decs = check_selects_ppg env in
  Debug.dprintf7 debug "here %b %b %d %d %d %d %d" made_ppg distinct_only
    (List.length ind_decs) (List.length val_decs)
    (List.length dist_ppg_decs)
    (List.length unk_ppg_decs)
    (List.length select_val_decs);
  let redo, distinct_only' =
    if made_ppg then (true, distinct_only)
    else
      match (ind_decs, dist_ppg_decs) with
      | [], [] -> (
          match unk_ppg_decs with
          | [] -> (
              match val_decs with
              (* | [] when distinct_only -> (distinct_only, false) *)
              | [] -> (
                  match select_val_decs with
                  | [] when not distinct_only -> (
                      (* (false, false) *)
                      match aeq_decs with
                      | [] -> (false, false)
                      | _ ->
                          List.iter (fun f -> f env) aeq_decs;
                          (true, distinct_only))
                  | [] when distinct_only -> (true, false)
                  | _ ->
                      List.iter (fun f -> f env) select_val_decs;
                      (true, distinct_only))
              | _ ->
                  List.iter (fun f -> f env) val_decs;
                  (true, distinct_only))
          | _ ->
              List.iter (fun f -> f env) unk_ppg_decs;
              (true, distinct_only))
      | _ ->
          List.iter (fun f -> f env) ind_decs;
          List.iter (fun f -> f env) dist_ppg_decs;
          (true, distinct_only)
  in
  if redo then
    DaemonLastEffortLateUncontextual.schedule_immediately env
      (last_effort_arr_eqs_no_ppg ~distinct_only:distinct_only')

let on_set_is_empty_hook env id n =
  if is_seq env n then (
    Egraph.register env RealValue.zero;
    Egraph.register env RealValue.minus_one;
    Bounds.upd_bounds_id env id RealValue.zero RealValue.minus_one)

let init_hooks env =
  NSeq_dom.register_on_set_is_empty_hook env on_set_is_empty_hook;
  Relocate.on_lra_repr_change env;
  Egraph.attach_after_merge env Relocate.on_merge;
  Relocate.register_repr_change_hook env NSeq_dom.on_reloc_repr_change;
  if Options.get env Colibri2_theories_utils.Array_opt.array_use_nseqs then
    if Options.get env array_nseqs_no_ppg then
      DaemonLastEffortLateUncontextual.schedule_immediately env
        (last_effort_arr_eqs_no_ppg
           ~distinct_only:(not (Options.get env nseq_no_dist_first)))
    else DaemonLastEffortLateUncontextual.schedule_immediately env check_eq_arrs;
  Id.NSeq.register_merge_hook env (fun env id1 id2 ->
      Bounds.eq_nseqs_norm env id1 id2;
      Relocate.eq_nseqs_norm env id1 id2;
      NSeq_dom.eq_nseqs_norm env id1 id2;
      if
        Options.get env Colibri2_theories_utils.Array_opt.array_use_nseqs
        && Options.get env array_nseqs_no_ppg
      then (
        UWEGraph.eq_nseqs_norm env id1 id2;
        NS_arrays.eq_ns_arrays_norm env id1 id2));
  Id.Index.register_merge_hook env (fun env id1 id2 ->
      Common.eq_indices_norm_prec_succ env id1 id2;
      Bounds.eq_indices_norm env id1 id2;
      NSeq_dom.eq_indices_norm env id1 id2;
      if
        Options.get env Colibri2_theories_utils.Array_opt.array_use_nseqs
        && Options.get env array_nseqs_no_ppg
      then (
        UWEGraph.eq_indices_norm env id1 id2;
        NS_arrays.eq_indices_norm env id1 id2))
