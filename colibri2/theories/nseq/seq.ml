(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_core
open Colibri2_popop_lib
open Common
module RealValue = Colibri2_theories_LRA.RealValue

let mk_concat =
  let align env val_gty prec n =
    let lenprec, _ = nsucc env (mk_nseq_last env val_gty prec) in
    mk_nseq_relocate env val_gty n lenprec
  in
  fun env val_gty args ->
    let rec aux gt = function
      | [] -> gt
      | h :: t ->
          let pn = Ground.node gt in
          let gt' =
            Ground.index
              (Ground.apply env Builtin.nseq_concat [ val_gty ]
                 (IArray.of_list [ pn; align env val_gty pn h ]))
          in
          aux gt' t
    in
    match IArray.to_list args with
    | [] | [ _ ] -> assert false
    | h1 :: h2 :: t ->
        (* TODO: sould it use a variant of concat, with no decision? *)
        aux
          (Ground.index
             (Ground.apply env Builtin.nseq_concat [ val_gty ]
                (IArray.of_list [ h1; align env val_gty h1 h2 ])))
          t

let mk_empty_seq env val_gty =
  let n = empty_seq_n val_gty in
  Egraph.register env n;
  Egraph.register env RealValue.zero;
  Egraph.register env RealValue.minus_one;
  Bounds.upd_bounds env n RealValue.zero RealValue.minus_one;
  n

let converter env f =
  let s = Ground.sem f in
  let n = Ground.node f in
  (match s.ty with
  | { app = { builtin = Builtin.Seq; _ }; args = [ val_gty ]; _ } ->
      Egraph.register env RealValue.zero;
      (* TODO: use upd_bounds instead? *)
      let fst, _, _lst, _ = Bounds.get_mk_bounds_n env ~val_gty n in
      Egraph.merge env fst RealValue.zero;
      (* let lge0 = mk_ge env [ lst; RealValue.minus_one ] in
         Egraph.register env lge0;
         Boolean.set_true env lge0; *)
      add_nseq_gty env n val_gty
  | _ -> ());
  match s with
  | { app = { builtin = Builtin.Seq_empty }; tyargs = [ val_gty ] } ->
      NSeq_value.propagate_value env f;
      Egraph.merge env n (mk_empty_seq env val_gty)
  | { app = { builtin = Builtin.Seq_unit }; args; tyargs = [ val_gty ] } ->
      NSeq_value.propagate_value env f;
      let v = IArray.extract1_exn args in
      let ns =
        Ground.apply' env Builtin.nseq_const [ val_gty ]
          [ RealValue.zero; RealValue.zero; v ]
      in
      Egraph.register env ns;
      Egraph.merge env n ns
  | { app = { builtin = Builtin.Seq_len }; args; tyargs = [ val_gty ] } ->
      NSeq_value.propagate_value env f;
      let a = IArray.extract1_exn args in
      let ns =
        mk_add env
          (Ground.apply' env Builtin.nseq_last [ val_gty ] [ a ])
          RealValue.one
      in
      Egraph.register env ns;
      Egraph.merge env ns n
  | { app = { builtin = Builtin.Seq_get }; args; tyargs = [ val_gty ] } ->
      NSeq_value.propagate_value env f;
      let a, i = IArray.extract2_exn args in
      let ns = Ground.apply' env Builtin.nseq_get [ val_gty ] [ a; i ] in
      Egraph.register env ns;
      Egraph.merge env n ns
  | { app = { builtin = Builtin.Seq_set }; args; tyargs = [ val_gty ] } ->
      NSeq_value.propagate_value env f;
      let a, i, v = IArray.extract3_exn args in
      let ns = Ground.apply' env Builtin.nseq_set [ val_gty ] [ a; i; v ] in
      Egraph.register env ns;
      Egraph.merge env n ns
  | { app = { builtin = Builtin.Seq_update }; args; tyargs = [ val_gty ] } ->
      NSeq_value.propagate_value env f;
      let a, i, b = IArray.extract3_exn args in
      let afst, _, alst, _ = Bounds.get_mk_bounds_n env ~val_gty a in
      let in_bounds = mk_in_bounds env i afst alst in
      Egraph.register env in_bounds;
      let relb = mk_nseq_relocate env val_gty b i in
      Egraph.register env relb;
      let rbfst, _, rblst, _ = Bounds.get_mk_bounds_n env ~val_gty relb in
      let overflow = mk_lt env [ alst; rblst ] in
      Egraph.register env overflow;
      let alt env = Egraph.merge env n a in
      let update env =
        let ns =
          Ground.apply' env Builtin.nseq_update [ val_gty ] [ a; relb ]
        in
        Egraph.register env ns;
        Egraph.merge env n ns
      in
      let slice env =
        let sln =
          Ground.apply' env Builtin.nseq_slice [ val_gty ] [ relb; rbfst; alst ]
        in
        let ns = Ground.apply' env Builtin.nseq_update [ val_gty ] [ a; sln ] in
        Egraph.register env ns;
        Egraph.merge env n ns
      in
      new_decision env in_bounds "Decision from Seq.update 1"
        ~cons:(fun env ->
          new_decision env overflow "Decision from Seq.update 2" ~cons:slice
            ~alt:update (fun _ ->
              DecTodo
                [
                  (fun env ->
                    Boolean.set_true env overflow;
                    slice env);
                  (fun env ->
                    Boolean.set_false env overflow;
                    update env);
                ]))
        ~alt
        (fun _ ->
          DecTodo
            [
              (fun env ->
                Boolean.set_true env in_bounds;
                Boolean.set_true env overflow;
                slice env);
              (fun env ->
                Boolean.set_true env in_bounds;
                Boolean.set_false env overflow;
                update env);
              (fun env ->
                Boolean.set_false env in_bounds;
                alt env);
            ])
  | { app = { builtin = Builtin.Seq_concat }; args; tyargs = [ val_gty ] } ->
      NSeq_value.propagate_value env f;
      let gt = mk_concat env val_gty args in
      let ns = Ground.node gt in
      Egraph.register env ns;
      Egraph.merge env n ns
  | { app = { builtin = Builtin.Seq_extract }; args; tyargs = [ val_gty ] } ->
      NSeq_value.propagate_value env f;
      let a, i, l = IArray.extract3_exn args in
      Egraph.register env a;
      Egraph.register env i;
      Egraph.register env l;
      let pos_len = mk_lt env [ RealValue.zero; l ] in
      let afst, _, alst, _ = Bounds.get_mk_bounds_n env ~val_gty a in
      let in_bounds = mk_in_bounds env i afst alst in
      let cond = Boolean._and env [ pos_len; in_bounds ] in
      Egraph.register env cond;
      let alt env = Egraph.merge env n (mk_empty_seq env val_gty) in
      let mk_slice env lst =
        let ns =
          Ground.apply' env Builtin.nseq_relocate [ val_gty ]
            [
              Ground.apply' env Builtin.nseq_slice [ val_gty ] [ a; i; lst ];
              RealValue.zero;
            ]
        in
        Egraph.register env ns;
        Egraph.merge env n ns
      in
      let nlst, _ =
        let l' = mk_add env i l in
        Egraph.register env l';
        nprec env l'
      in
      let overflow_cond = mk_lt env [ alst; nlst ] in
      Egraph.register env overflow_cond;
      new_decision env cond "Decision from Seq.Seq_extract 1"
        ~cons:(fun env ->
          new_decision env overflow_cond "Decision from Seq.Seq_extract 2"
            ~cons:(fun env -> mk_slice env alst)
            ~alt:(fun env -> mk_slice env nlst)
            (fun _ ->
              DecTodo
                [
                  (fun env ->
                    Boolean.set_true env overflow_cond;
                    mk_slice env alst);
                  (fun env ->
                    Boolean.set_false env overflow_cond;
                    mk_slice env nlst);
                ]))
        ~alt
        (fun _ ->
          DecTodo
            [
              (fun env ->
                Boolean.set_true env cond;
                Boolean.set_true env overflow_cond;
                mk_slice env alst);
              (fun env ->
                Boolean.set_true env cond;
                Boolean.set_false env overflow_cond;
                mk_slice env nlst);
              (fun env ->
                Boolean.set_false env cond;
                alt env);
            ])
  | _ -> ()

let init env = Ground.register_converter env converter
