(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib
open Common

(* a -> b -> i *)
module Vert2Edge = Id.MkIHT (struct
  type t = Id.t Id.M.t

  let pp = Id.M.pp Id.pp
  let name = "UndirectedWEGraph.Vert2Edge"
end)

(* a -> i -> b set *)
module Edge2Vert = Id.MkIHT (struct
  type t = Id.S.t Id.M.t

  let pp = Id.M.pp (fun fmt s -> Fmt.pf fmt "{%a}" Id.S.pp s)
  let name = "UndirectedWEGraph.Edge2Vert"
end)

(* i -> a set *)
module EdgeLocs = Id.MkIHT (struct
  type t = Id.S.t

  let pp fmt s = Fmt.pf fmt "{%a}" Id.S.pp s
  let name = "UndirectedWEGraph.ELocs"
end)

let idm_to_opt m = if Id.M.is_empty m then None else Some m

let rm_edge_loc env i loc =
  Debug.dprintf4 debug "UWEG.rm_edge_loc %a %a" Id.pp i Id.pp loc;
  EdgeLocs.change env i ~f:(function
    | None -> assert false
    | Some s ->
        assert (Id.S.mem loc s);
        let s' = Id.S.remove loc s in
        idm_to_opt s')

let subst_edge_loc env i rloc nloc =
  EdgeLocs.change env i ~f:(function
    | None -> assert false
    | Some s ->
        assert (Id.S.mem rloc s);
        let s' = Id.S.add nloc (Id.S.remove rloc s) in
        Some s')

let rm_neighbour env id k neighbour =
  Vert2Edge.change env id ~f:(function
    | None -> assert false
    | Some m -> (
        match Id.M.find_remove neighbour m with
        | _, None -> assert false
        | m', Some k' ->
            assert (Id.equal k k');
            idm_to_opt m'));
  Edge2Vert.change env id ~f:(function
    | None -> assert false
    | Some m -> (
        match Id.M.find_remove k m with
        | _, None -> assert false
        | m', Some s -> (
            match Id.M.find_remove neighbour s with
            | _, None -> assert false
            | s', Some _ ->
                let m' = if Id.S.is_empty s' then m' else Id.M.add k s' m' in
                idm_to_opt m')))

let subst_neighbour env id k rn kn =
  Vert2Edge.change env id ~f:(function
    | None -> assert false
    | Some m -> (
        match Id.M.find_remove rn m with
        | _, None -> assert false
        | m', Some i ->
            let m =
              Id.M.change
                (function None -> Some i | Some _ -> assert false)
                kn m'
            in
            Some m));
  Edge2Vert.change env id ~f:(function
    | None -> assert false
    | Some m -> (
        match Id.M.find_opt k m with
        | None -> assert false
        | Some s ->
            assert (Id.S.mem rn s);
            let s' = Id.S.add kn (Id.S.remove rn s) in
            let m' =
              if Id.S.is_empty s' then Id.M.remove k m else Id.M.add k s' m
            in
            idm_to_opt m'))

type r = CGraph of Id.S.t | CEdge of Id.t

let pp_r fmt = function
  | CGraph s -> Fmt.pf fmt "Graph{%a}" Id.S.pp s
  | CEdge id -> Fmt.pf fmt "Edge(%a)" Id.pp id

module Class = Id.MkIHT (struct
  type t = r

  let pp = pp_r
  let name = "WEG.representatives"
end)

let new_class = Colibri2_theories_utils.Ident.new_id

let class_merge_hooks :
    (Egraph.wt -> Id.S.t -> Id.S.t -> unit) Datastructure.Push.t =
  Datastructure.Push.create Fmt.nop "NSeq.UWEG.class_merge_hook"

let register_class_merge_hook d f =
  Datastructure.Push.push class_merge_hooks d f

let on_class_merge env s1 s2 =
  Datastructure.Push.iter class_merge_hooks env ~f:(fun f -> f env s1 s2)

let add_to_class env s c =
  Id.S.iter (fun id -> Class.set env id (CEdge c)) s;
  Class.change env c ~f:(function
    | None -> Some (CGraph s)
    | Some (CGraph s') -> Some (CGraph (Id.S.union s s'))
    | Some (CEdge _) -> assert false)

let rm_from_class env c id =
  Class.change env c ~f:(function
    | None -> assert false
    | Some (CEdge _) -> assert false
    | Some (CGraph s) ->
        assert (Id.S.mem id s);
        let s' = Id.S.remove id s in
        assert (not (Id.S.is_empty s));
        Some (CGraph s'))

let subst_in_class env c rid kid =
  Class.set env kid (CEdge c);
  Class.change env c ~f:(function
    | None -> assert false
    | Some (CEdge _) -> assert false
    | Some (CGraph s) ->
        assert (Id.S.mem rid s);
        let s' = Id.S.add kid (Id.S.remove rid s) in
        Some (CGraph s'))

let merge_classes env c1 c2 =
  match (Class.find_opt env c1, Class.find_opt env c2) with
  | None, _ | _, None -> assert false
  | Some (CEdge _), _ | _, Some (CEdge _) -> assert false
  | Some (CGraph s1), Some (CGraph s2) when Id.S.cardinal s1 > Id.S.cardinal s2
    ->
      add_to_class env s2 c1;
      on_class_merge env s1 s2
  | Some (CGraph s1), Some (CGraph s2) ->
      add_to_class env s1 c2;
      on_class_merge env s1 s2

let new_edge_upd_classes env a b =
  match (Class.find_opt env a, Class.find_opt env b) with
  | Some (CGraph _), _ | _, Some (CGraph _) -> assert false
  | None, None ->
      let c = new_class () in
      add_to_class env (Id.S.of_list [ a; b ]) c;
      on_class_merge env (Id.S.singleton a) (Id.S.singleton b)
  | None, Some (CEdge c) ->
      add_to_class env (Id.S.singleton a) c;
      let cset =
        match Class.find_opt env c with
        | Some (CGraph s) -> s
        | _ -> assert false
      in
      on_class_merge env (Id.S.singleton a) cset
  | Some (CEdge c), None ->
      add_to_class env (Id.S.singleton b) c;
      let cset =
        match Class.find_opt env c with
        | Some (CGraph s) -> s
        | _ -> assert false
      in
      on_class_merge env cset (Id.S.singleton b)
  | Some (CEdge c1), Some (CEdge c2) when Id.equal c1 c2 -> ()
  | Some (CEdge c1), Some (CEdge c2) -> merge_classes env c1 c2

let add_neighbour =
  let add_v2e env updloc a i b =
    Edge2Vert.change env a ~f:(function
      | None -> Some (Id.M.singleton i (Id.S.singleton b))
      | Some m ->
          Some
            (Id.M.change
               (function
                 | None -> Some (Id.S.singleton b)
                 | Some bs -> Some (Id.S.add b bs))
               i m));
    if updloc then
      EdgeLocs.change env i ~f:(function
        | None -> Some (Id.S.add a (Id.S.singleton b))
        | Some s -> Some (Id.S.add a (Id.S.add b s)))
  in
  fun env updloc a i b ->
    match Vert2Edge.find_opt env a with
    | None ->
        Vert2Edge.set env a (Id.M.singleton b i);
        add_v2e env updloc b i a
    | Some m -> (
        match Id.M.find_opt b m with
        | None ->
            Vert2Edge.set env a (Id.M.add b i m);
            add_v2e env updloc b i a
        | Some j when Id.equal i j -> ()
        | Some j -> on_multiple_kns env a b i j)

let add_edge env a i b =
  Debug.dprintf6 debug "UWEG.add_edge %a %a %a" Id.pp a Id.pp i Id.pp b;
  add_neighbour env true a i b;
  add_neighbour env false b i a;
  new_edge_upd_classes env a b

let eq_nseqs_norm env kid rid =
  (* TODO: when the graph is updated check if select propagation can be
     done. Maybe only check the ones that previously failed where they
     failed? By memoizing them? *)
  Debug.dprintf4 debug "UWEG.eq_nseqs_norm %a <- %a" Id.pp rid Id.pp kid;
  match Edge2Vert.find_opt env rid with
  | None -> ()
  | Some re2v -> (
      Vert2Edge.remove env rid;
      Edge2Vert.remove env rid;
      let kv2e = Opt.get_def Id.M.empty (Vert2Edge.find_opt env kid) in
      let ke2v = Opt.get_def Id.M.empty (Edge2Vert.find_opt env kid) in
      let kv2e, ke2v, re2v =
        match Id.M.find_remove rid kv2e with
        | _, None -> (kv2e, ke2v, re2v)
        | kv2e', Some i -> (
            match Id.M.find_remove i ke2v with
            | _, None -> assert false
            | ke2v', Some kbset ->
                assert (Id.S.mem rid kbset);
                let kbset' = Id.S.remove rid kbset in
                if Id.S.is_empty kbset' then (
                  rm_edge_loc env i kid;
                  let re2v' =
                    Id.M.change
                      (function
                        | None -> assert false
                        | Some s ->
                            let s' = Id.S.remove kid s in
                            if Id.S.is_empty s' then (
                              rm_edge_loc env i rid;
                              None)
                            else Some s')
                      i re2v
                  in
                  (kv2e', ke2v', re2v'))
                else
                  let re2v' =
                    Id.M.change
                      (function
                        | None -> assert false
                        | Some s ->
                            let s' = Id.S.remove kid s in
                            if Id.S.is_empty s' then (
                              subst_edge_loc env i rid kid;
                              None)
                            else Some s')
                      i re2v
                  in
                  (kv2e', Id.M.add i kbset' ke2v', re2v'))
      in
      let v2e_acc, e2v_acc =
        Id.M.fold
          (fun i aset (v2e_acc, e2v_acc) ->
            let aset, v2e_acc =
              Id.S.fold
                (fun a (acc, v2e_acc) ->
                  match Id.M.find_opt a v2e_acc with
                  | None ->
                      subst_neighbour env a i rid kid;
                      (Id.S.add a acc, Id.M.add a i v2e_acc)
                  | Some j when Id.equal i j ->
                      rm_neighbour env a i rid;
                      (acc, v2e_acc)
                  | Some j ->
                      rm_neighbour env a i rid;
                      on_multiple_kns env kid a i j;
                      if
                        match Id.M.find_opt i (Edge2Vert.find env a) with
                        | None -> true
                        | Some s -> Id.S.is_empty s
                      then rm_edge_loc env i a;
                      (acc, v2e_acc))
                aset (Id.S.empty, v2e_acc)
            in
            let e2v_acc =
              Id.M.change
                (function
                  | None ->
                      subst_edge_loc env i rid kid;
                      Some aset
                  | Some aset' ->
                      rm_edge_loc env i rid;
                      Some (Id.S.union aset aset'))
                i e2v_acc
            in
            (v2e_acc, e2v_acc))
          re2v (kv2e, ke2v)
      in
      Vert2Edge.set env kid v2e_acc;
      Edge2Vert.set env kid e2v_acc;
      match (Class.find_opt env rid, Class.find_opt env kid) with
      | None, None -> assert false
      | Some (CGraph _), _ | _, Some (CGraph _) -> assert false
      | None, Some (CEdge c) -> subst_in_class env c rid kid
      | Some (CEdge c), None -> subst_in_class env c rid kid
      | Some (CEdge c1), Some (CEdge c2) when Id.equal c1 c2 ->
          rm_from_class env c1 rid
      | Some (CEdge c1), Some (CEdge c2) ->
          rm_from_class env c1 rid;
          merge_classes env c1 c2)

let eq_indices_norm env kid rid =
  Debug.dprintf4 debug "UWEG.eq_indices_norm %a <- %a" Id.pp rid Id.pp kid;
  match EdgeLocs.find_opt env rid with
  | None -> ()
  | Some rlocs ->
      let klocs = Opt.get_def Id.S.empty (EdgeLocs.find_opt env kid) in
      EdgeLocs.remove env rid;
      let klocs =
        Id.S.fold
          (fun a acc ->
            let ae2v, av2e =
              let av2e = Vert2Edge.find env a in
              match Edge2Vert.find_opt env a with
              | None -> assert false
              | Some ae2v -> (
                  match Id.M.find_remove rid ae2v with
                  | _, None -> assert false
                  | ae2v', Some bset ->
                      let bset' = Id.M.find_def Id.S.empty kid ae2v' in
                      let bset, av2e =
                        Id.S.fold
                          (fun b (acc, av2e_acc) ->
                            assert (not (Id.S.mem b acc));
                            let av2e_acc =
                              match Id.M.find_opt b av2e_acc with
                              | None -> assert false
                              | Some ind ->
                                  assert (Id.equal ind rid);
                                  Id.M.add b kid av2e_acc
                            in
                            (Id.S.add b acc, av2e_acc))
                          bset (bset', av2e)
                      in
                      (Id.M.add kid bset ae2v', av2e))
            in
            Vert2Edge.set env a av2e;
            Edge2Vert.set env a ae2v;
            Id.S.add a acc)
          rlocs klocs
      in
      EdgeLocs.set env kid klocs

let get_repr env id =
  match Class.find_opt env id with
  | None | Some (CGraph _) -> raise Not_found
  | Some (CEdge r) -> r

let are_linked env id1 id2 =
  match (Class.find_opt env id1, Class.find_opt env id2) with
  | Some (CGraph _), _ | _, Some (CGraph _) -> assert false
  | Some (CEdge c1), Some (CEdge c2) when Id.equal c1 c2 -> true
  | _ -> false

type path = (Id.t * Node.t * Id.t * Node.t) list

let pp_step fmt (id1, _, id2, _) = Fmt.pf fmt "(%a, %a)" Id.pp id1 Id.pp id2

let pp_path fmt (path : path) =
  Fmt.pf fmt "[%a]" (Fmt.list ~sep:Fmt.comma pp_step) path

let pp_path_list fmt paths =
  Fmt.pf fmt "[%a]" (Fmt.list ~sep:Fmt.comma pp_path) paths

let shortest_path =
  (* let ppl fmt (id1, _, id2, _) = Fmt.pf fmt "(%a, %a)" Id.pp id1 Id.pp id2 in *)
  let rec aux env ?ex_ind src dsts seen path =
    (* Debug.dprintf2 debug "aux         path = [%a]" (Fmt.list pp_step) path; *)
    if Id.S.mem src dsts then (path, seen)
    else if Id.S.mem src seen then ([], seen)
    else
      let seen = Id.S.add src seen in
      match Edge2Vert.find_opt env src with
      | None -> assert false
      | Some edges -> aux_edges env ?ex_ind src dsts seen path edges
  and aux_edges env ?ex_ind src dsts seen path edges =
    (* Debug.dprintf2 debug "aux_edges    path = [%a]" (Fmt.list pp_step) path; *)
    match Id.M.min_binding edges with
    | exception Not_found -> ([], seen)
    | i, bset -> (
        if Opt.fold (fun _ j -> Id.equal i j) false ex_ind then
          aux_edges env ?ex_ind src dsts seen path (Id.M.remove i edges)
        else
          let rpath, seen =
            aux_vertices env ?ex_ind src dsts seen path i bset
          in
          match rpath with
          | [] -> aux_edges env ?ex_ind src dsts seen path (Id.M.remove i edges)
          | _ -> (rpath, seen))
  and aux_vertices env ?ex_ind src dsts seen path i bset =
    (* Debug.dprintf4 debug "aux_vertices path = [%a] i=%a" (Fmt.list pp_step) path
       Id.pp i; *)
    match Id.M.min_binding bset with
    | exception Not_found -> ([], seen)
    | b, () -> (
        let npath = (i, Id.get env i, b, Id.get env b) :: path in
        let rpath, seen = aux env ?ex_ind b dsts seen npath in
        match rpath with
        | [] ->
            aux_vertices env ?ex_ind src dsts seen path i (Id.S.remove b bset)
        | _ -> (rpath, seen))
  in
  fun env ?ex_ind src dsts ->
    let path, _ = aux env ?ex_ind src dsts Id.S.empty [] in
    Debug.dprintf2 debug "res path = [%a]" pp_path path;
    match path with
    | [] -> raise Not_found
    | (_, _, id, _) :: _ -> (id, List.rev path)

let filter_paths l =
  let rec aux l =
    match l with
    | [] -> []
    | ((_, ids) as h) :: t ->
        let t', rm =
          List.fold_left
            (fun (acc, rm) ((_, ids') as h') ->
              if Id.S.subset ids ids' then (acc, rm)
              else if Id.S.subset ids' ids then (h' :: acc, true)
              else (h' :: acc, rm))
            ([], false) t
        in
        if rm then aux t' else h :: aux t'
  in
  aux l

let all_paths =
  let rec aux env ?ex_ind ~ex_arrs src dsts seen =
    if Id.S.mem src dsts then ([], true)
    else if Id.S.mem src seen || Id.S.mem src ex_arrs then ([], false)
    else
      let seen = Id.S.add src seen in
      match Edge2Vert.find_opt env src with
      | None -> assert false
      | Some edges ->
          let acc, f =
            Id.M.fold
              (fun i bset (acc, f) ->
                if Opt.fold (fun _ j -> Id.equal i j) false ex_ind then (acc, f)
                else
                  Id.S.fold
                    (fun b (acc, f) ->
                      let path, f' = aux env ?ex_ind ~ex_arrs b dsts seen in
                      let path =
                        match path with
                        | [] -> [ [ (i, Id.get env i, b, Id.get env b) ] ]
                        | _ ->
                            List.map
                              (fun l -> (i, Id.get env i, b, Id.get env b) :: l)
                              path
                      in
                      if f' then (path @ acc, f || f') else (acc, f))
                    bset (acc, f))
              edges ([], false)
          in
          (acc, f)
  in
  fun env ~filter ?ex_ind ?(ex_arrs = Id.S.empty) src dsts ->
    Debug.dprintf8 debug
      "UWEGraph.all_paths %a {%a} (ex_arrs = {%a}) (ex_ind = %a)" Id.pp src
      Id.S.pp dsts Id.S.pp ex_arrs
      (Fmt.option ~none:(fun fmt () -> Fmt.pf fmt "None") Id.pp)
      ex_ind;
    assert (not (Id.S.is_empty dsts));
    let ex_arrs = Id.S.remove src ex_arrs in
    let paths, f = aux env ?ex_ind ~ex_arrs src dsts Id.S.empty in
    Debug.dprintf0 debug "found paths:";
    List.iter (Debug.dprintf2 debug "> %a" pp_path) paths;
    if (not f) || paths = [] then raise Not_found
    else
      let paths =
        List.map
          (fun path ->
            let indices =
              List.fold_left
                (fun acc (id, _, _, _) -> Id.S.add id acc)
                Id.S.empty path
            in
            (path, indices))
          paths
        |> (if filter then filter_paths else Fun.id)
        |> List.map fst
      in
      Debug.dprintf0 debug "filtered paths:";
      List.iter (Debug.dprintf2 debug "> %a" pp_path) paths;
      paths

let rec last_edge : path -> Id.t * Node.t = function
  | [] -> assert false
  | [ (_, _, id, idn) ] -> (id, idn)
  | _ :: t -> last_edge t

let rev_path (l : path) dst dstn : Id.t * Node.t * path =
  let rec aux l a anode acc =
    match l with
    | [] -> (a, anode, acc)
    | (i', inode', a', anode') :: t ->
        aux t a' anode' ((i', inode', a, anode) :: acc)
  in
  match l with
  | [] -> assert false
  | (i, inode, a, anode) :: t -> aux t a anode [ (i, inode, dst, dstn) ]

let filter_by_comp env ids =
  let rec aux id r = function
    | [] -> [ (r, [ id ]) ]
    | (rr, l) :: t ->
        if Id.equal r rr then (rr, id :: l) :: t else (rr, l) :: aux id r t
  in
  List.map snd
  @@ Id.S.fold
       (fun id acc ->
         match get_repr env id with
         | exception Not_found -> acc
         | r -> aux id r acc)
       ids []

let pp fmt env = Fmt.pf fmt "%a" Edge2Vert.pp env
