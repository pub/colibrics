(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

val add_bounds : Egraph.wt -> Id.t -> Id.t -> Id.t -> unit
val upd_bounds : Egraph.wt -> Node.t -> Node.t -> Node.t -> unit
val upd_bounds_ret : Egraph.wt -> Node.t -> Node.t -> Node.t -> Id.t * Id.t
val upd_bounds_id : Egraph.wt -> Id.t -> Node.t -> Node.t -> unit
val get_locs : Egraph.wt -> Id.t -> (Id.S.t Id.M.t * Id.S.t) option
val get_bounds : Egraph.wt -> Id.t -> (Id.t * Id.t) option
val get_bounds_exn : Egraph.wt -> Id.t -> Id.t * Id.t

val get_mk_bounds :
  Egraph.wt ->
  ?val_gty:Ground.Ty.t ->
  Node.t ->
  Id.t ->
  Node.t * Id.t * Node.t * Id.t

val get_mk_bounds_id :
  Egraph.wt -> ?val_gty:Ground.Ty.t -> Id.t -> Node.t * Id.t * Node.t * Id.t

val get_mk_bounds_n :
  Egraph.wt -> ?val_gty:Ground.Ty.t -> Node.t -> Node.t * Id.t * Node.t * Id.t

val upd_fb : Egraph.wt -> ?val_gty:Ground.Ty.t -> Node.t -> Node.t -> unit
val upd_lb : Egraph.wt -> ?val_gty:Ground.Ty.t -> Node.t -> Node.t -> unit
val eq_nseqs_norm : Egraph.wt -> Id.t -> Id.t -> unit
val eq_indices_norm : Egraph.wt -> Id.t -> Id.t -> unit
val iter : Egraph.wt -> (Egraph.wt -> Id.t -> Id.t -> Id.S.t -> unit) -> unit

val fold :
  'a Egraph.t -> ('a Egraph.t -> Id.t -> Id.t -> Id.S.t -> 'b -> 'b) -> 'b -> 'b

val register_bounds_change_hook :
  Egraph.wt ->
  (Egraph.rw Egraph.t -> Id.t -> Id.t * Id.t -> Id.t * Id.t -> unit) ->
  unit

val pp_ht : Format.formatter -> _ Egraph.t -> unit
