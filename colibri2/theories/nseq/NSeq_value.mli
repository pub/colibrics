(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

val get_ns_values :
  Common.NSeqValue.t -> Value.t * Value.t * Ground.Ty.t * Value.t Value.M.t

val set_ns_values :
  Egraph.wt ->
  Node.t ->
  Value.t ->
  Value.t ->
  Ground.Ty.t ->
  Value.t Value.M.t ->
  unit

val init : Egraph.wt -> unit
val propagate_value : Egraph.wt -> Ground.t -> unit

val compute :
  _ Egraph.t -> Ground.t -> [> `None | `Some of Value.t | `Uninterpreted ]
