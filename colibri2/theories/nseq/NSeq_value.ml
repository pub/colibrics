(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_core
open Colibri2_popop_lib
open Common
open Colibri2_theories_quantifiers
module RealValue = Colibri2_theories_LRA.RealValue
module NSC = NSComp_info.NSC
module DP = Colibri2_theories_LRA.Dom_polynome

let ( let+ ) = Interp.Seq.( let+ )
let ( and* ) = Interp.Seq.( and* )
let ( let++ ) = Sequence.( let+ )
let ( and** ) = Sequence.( and* )

let get_ns_values v =
  let NSeqValue.{ lower_bound; upper_bound; val_gty; vals } =
    NSeqValue.value v
  in
  (lower_bound, upper_bound, val_gty, vals)

let set_ns_values env n lower_bound upper_bound val_gty vals =
  Egraph.set_value env n
    (NSeqValue.of_value
       NSeqValue.{ lower_bound; upper_bound; val_gty; vals; other = None })

let get_value env n = Opt.get_exn Impossible (Egraph.get_value env n)

let mk_const_nseq_vals ?(values = Value.M.empty) lb ub (dv : Value.t) =
  let rec aux vals n =
    if A.gt n ub then vals
    else
      let indv = RealValue.of_value n in
      match Value.M.find_opt indv values with
      | None -> aux (Value.M.add indv dv vals) (A.add n A.one)
      | Some v -> aux (Value.M.add indv v vals) (A.add n A.one)
  in
  aux Value.M.empty lb

(* TODO: add known values in an nseq (not only from nseq_content) *)
let mk_nseq_vals d ?(values = Value.M.empty) ~lower_bound ~upper_bound
    ?(is_empty = false) ?other val_gty =
  let vals =
    let lb = RealValue.coerce_value lower_bound in
    let ub = RealValue.coerce_value upper_bound in
    if is_empty || A.lt ub lb then
      (* TODO: in theory, whenever an nseq is empty, the [lower_bound] and
         [upper_bound] are provided, since we need to know that the lower
         bound is greater than the upper bound to know that an nseq is empty.
         But if, for some reason we don't know their precise value, we must
         make sure that the generated values respect the property (that the
         lower bound is lesser than the upper bound). *)
      Sequence.singleton Value.M.empty
    else
      match other with
      | Some dvn -> (
          try
            let dv = get_value d dvn in
            Sequence.singleton (mk_const_nseq_vals ~values lb ub dv)
          with Impossible ->
            (* TODO: how safe is this? *)
            Debug.dprintf0 debug "ICI4";
            let vals =
              let++ dv = Interp.ty d val_gty in
              Sequence.singleton (mk_const_nseq_vals ~values lb ub dv)
            in
            Sequence.concat vals)
      | None ->
          let rec aux vals n =
            if A.gt n ub then vals
            else
              let indv = RealValue.of_value n in
              aux
                (match Value.M.find_opt indv values with
                | Some valv ->
                    Debug.dprintf2 debug "ICI8: %a" Value.pp indv;
                    let++ vals = vals in
                    Value.M.add indv valv vals
                | None ->
                    let++ valv = Interp.ty d val_gty and** vals = vals in
                    Value.M.add indv valv vals)
                (A.add n A.one)
          in
          aux (Sequence.singleton Value.M.empty) lb
  in
  let++ vals = vals in
  Debug.dprintf0 debug "ICI7";
  vals

let all_nseqs d val_gty =
  let++ lower_bound = Interp.ty d Ground.Ty.int
  and** upper_bound = Interp.ty d Ground.Ty.int in
  let++ vals = mk_nseq_vals d ~lower_bound ~upper_bound val_gty in
  (lower_bound, upper_bound, vals)

let all_seqs d val_gty =
  let++ lower_bound = Sequence.cycle_list_exn [ RealValue.of_value A.zero ]
  and** upper_bound =
    let++ z =
      Sequence.map ~f:(fun z -> Z.min z Z.one) RealValue.positive_int_sequence
    in
    RealValue.of_value (A.of_z z)
  in
  let++ vals = mk_nseq_vals d ~lower_bound ~upper_bound val_gty in
  (lower_bound, upper_bound, vals)

let rec get_nseq_kvs env interp val_gty nid n (lbn : Node.t) fst (ubn : Node.t)
    : Value.t Value.M.t Interp.Seq.t =
  Debug.dprintf6 debug "get_nseq_kvs %a[%a; %a]" (Id.pp_nid env) nid
    (Id.pp_nid env) fst (Id.Index.pp env) ubn;
  (* let rec get_nseq_kvs_from_nsc env acc fst (c : NSC.t) =
       Debug.dprintf6 debug "get_nseq_kvs_from_nsc %a {%a}\nacc = {%a}"
         (Id.pp_nid env) fst NSC.pp c (Value.M.pp Value.pp) acc;
       match Id.M.find_opt fst c with
       | None ->
           acc
           (* TODO: not safe, but soemtimes we have next when there isn't one *)
       | Some m' -> (
           assert (Id.M.cardinal m' = 1);
           let _, ap = Id.M.min_binding m' in
           let n', next =
             match ap with
             | { a = NSeq n; next } -> (n, next)
             | { a = Var _ } -> assert false
           in
           let nid' = Id.NSeq.get_id env n' in
           let lbn', fst', ubn', _ = Bounds.get_mk_bounds_id env ~val_gty nid' in
           let vals = get_nseq_kvs env interp val_gty nid' n' lbn' fst' ubn' in
           let accvals = Value.M.union (fun _ _ _ -> assert false) vals acc in
           match next with
           | None -> accvals
           | Some nxt ->
               let nxtid = Id.Index.get_id env nxt in
               get_nseq_kvs_from_nsc env accvals nxtid c)
     in *)
  let get_nseq_kvs_from_nsc env acc fst (c : NSC.t) =
    Debug.dprintf6 debug "get_nseq_kvs_from_nsc %a {%a}\nacc = {%a}"
      (Id.pp_nid env) fst NSC.pp c (Value.M.pp Value.pp) acc;
    Id.M.fold
      (fun _ c' acc ->
        Id.M.fold
          (fun _ ap acc ->
            let n' = ap.NSC.a in
            let nid' = Id.NSeq.get_id env n' in
            let lbn', fst', ubn', _ =
              Bounds.get_mk_bounds_id env ~val_gty nid'
            in
            if Egraph.is_equal env n n' then acc
            else
              let+ vals = get_nseq_kvs env interp val_gty nid' n' lbn' fst' ubn'
              and* acc = acc in
              Value.M.union
                (fun _ v1 v2 ->
                  (* assert false *)
                  assert (Value.equal v1 v2);
                  Some v1)
                vals acc)
          c' acc)
      c
      (Interp.Seq.return Value.M.empty)
  in
  let vals1 =
    match NSComp_dom.ConcatDom.get_cdom env nid with
    | None -> Interp.Seq.return Value.M.empty
    | Some nsc -> get_nseq_kvs_from_nsc env Value.M.empty fst nsc
  in
  let vals2 =
    match NSeq_dom.get_defval env nid with
    | Some dvn ->
        let+ values =
          let+ lbv = interp env lbn
          and* ubv = interp env ubn
          and* dvv = interp env dvn in
          mk_const_nseq_vals
            (RealValue.coerce_value lbv)
            (RealValue.coerce_value ubv)
            dvv
        in
        values
    | None -> (
        Interp.Seq.return
        @@
        match NSeq_dom.get_kvs env nid with
        | None -> Value.M.empty
        | Some m ->
            let m = Id.M.map (fun (v, _, _) -> v) m in
            Id.M.fold
              (fun iid v acc ->
                let i = Id.get env iid in
                try
                  Value.M.add_new Exit
                    (Opt.get_exn Exit (Egraph.get_value env i))
                    (Opt.get_exn Exit (Egraph.get_value env v))
                    acc
                with Exit -> acc)
              m Value.M.empty)
  in
  let values =
    let+ vals1 = vals1 and* vals2 = vals2 in
    Value.M.union
      (fun _ v1 v2 ->
        (* TODO: workaround, should be avoided *)
        if not (Value.equal v1 v2) then Egraph.contradiction ();
        Some v1)
      vals1 vals2
  in
  values

and interp_nseq_node_repr env interp val_gty nid n lower_bound_n fst
    upper_bound_n =
  Debug.dprintf6 debug "interp_nseq_node_repr %a [%a; %a]" (Id.NSeq.pp env) n
    (Id.Index.pp env) lower_bound_n (Id.Index.pp env) upper_bound_n;
  let is_empty = NSeq_dom.is_empty env nid in
  let other = NSeq_dom.get_defval env nid in
  let+ lower_bound = interp env lower_bound_n
  and* upper_bound = interp env upper_bound_n
  and* values =
    get_nseq_kvs env interp val_gty nid n lower_bound_n fst upper_bound_n
  in
  Debug.dprintf0 debug "Ici3";
  let+ vals =
    Interp.Seq.of_seq
    @@ mk_nseq_vals env ~values ~lower_bound ~upper_bound ~is_empty ?other
         val_gty
  in
  NSeqValue.{ val_gty; vals; lower_bound; upper_bound; other = None }

and interp_nseq_node_aux env interp val_gty n : Value.t Interp.Seq.t option =
  match Id.NSeq.get_id env n with
  | exception Colibri2_theories_utils.Ident.NoIdFound _ ->
      (* Does this ever happen? *)
      assert false
  | nid -> (
      let lower_bound_n, lower_bound_id, upper_bound_n, _ =
        Bounds.get_mk_bounds_n env ~val_gty n
      in
      match Relocate.get_repr env nid with
      | Repr _ ->
          Debug.dprintf0 debug "Ici";
          let res =
            let+ nsv =
              Interp.Seq.concat
              @@ interp_nseq_node_repr env interp val_gty nid n lower_bound_n
                   lower_bound_id upper_bound_n
            in
            let v = NSeqValue.index nsv in
            Debug.dprintf2 debug "La: %a" NSeqValue.pp v;
            NSeqValue.nodevalue v
          in
          Some res
      | NonRepr { repr; dist } -> (
          let rn = Id.get env repr in
          match Egraph.get_value env rn with
          | Some reprv ->
              let nsv = NSeqValue.coerce_value reprv in
              let v =
                let dnode = DP.node_of_polynome env dist in
                let+ nsv' =
                  Relocate.reloc_nsv_by_delta env interp val_gty dist ~dnode nsv
                    lower_bound_n upper_bound_n
                in
                NSeqValue.nodevalue (NSeqValue.index nsv')
              in
              Some v
          | None ->
              let r_lower_bound_n, _, r_upper_bound_n, _ =
                Bounds.get_mk_bounds_id env ~val_gty repr
              in
              let rnsv_seq =
                let rn = Id.get env repr in
                let+ rnsv =
                  Interp.Seq.concat
                  @@ interp_nseq_node_repr env interp val_gty repr rn
                       r_lower_bound_n lower_bound_id r_upper_bound_n
                in
                Egraph.set_value env rn
                  (NSeqValue.nodevalue (NSeqValue.index rnsv));
                rnsv
              in
              let v =
                let dnode = DP.node_of_polynome env dist in
                let+ rnsv = rnsv_seq in
                let+ nsv =
                  Relocate.reloc_nsv_by_delta env interp val_gty dist ~dnode
                    rnsv lower_bound_n upper_bound_n
                in
                NSeqValue.nodevalue (NSeqValue.index nsv)
              in
              Some (Interp.Seq.concat v)))

let nsv_of_val v = NSeqValue.value (NSeqValue.coerce_nodevalue v)

let mk_array_nsval val_gty vals other =
  NSeqValue.nodevalue
    (NSeqValue.index
       {
         val_gty;
         vals = Value.M.filter (fun _ v -> not (Value.equal v other)) vals;
         lower_bound = RealValue.of_value A.zero;
         upper_bound = RealValue.of_value A.minus_inf;
         other = Some other;
       })

let compute_aux d g gs =
  match gs with
  | Ground.{ app = { builtin = Builtin.NSeq_get }; args } ->
      let a, k = IArray.extract2_exn args in
      let NSeqValue.{ vals; lower_bound; upper_bound } =
        nsv_of_val (get_value d a)
      in
      let kv = get_value d k in
      let alb = RealValue.coerce_value lower_bound in
      let aub = RealValue.coerce_value upper_bound in
      let kvv = RealValue.coerce_value kv in
      if A.le alb kvv && A.le kvv aub then
        match Value.M.find_opt kv vals with
        | Some fv -> `Some fv
        | None -> `Uninterpreted
      else `Uninterpreted
  | { app = { builtin = Builtin.NSeq_set }; args } ->
      let a, k, v = IArray.extract3_exn args in
      let av = get_value d a in
      let asv = nsv_of_val av in
      let kv = get_value d k in
      let vv = get_value d v in
      let k = RealValue.coerce_value kv in
      let lb = RealValue.coerce_value asv.lower_bound in
      let ub = RealValue.coerce_value asv.upper_bound in
      if A.le lb k && A.le k ub then
        let v = { asv with vals = Value.M.add kv vv asv.vals } in
        let nsv = NSeqValue.of_value v in
        `Some nsv
      else `Some av
  | { app = { builtin = Builtin.NSeq_first }; args } ->
      let a = IArray.extract1_exn args in
      let NSeqValue.{ lower_bound } = nsv_of_val (get_value d a) in
      `Some lower_bound
  | { app = { builtin = Builtin.NSeq_last }; args } ->
      let a = IArray.extract1_exn args in
      let NSeqValue.{ upper_bound } = nsv_of_val (get_value d a) in
      `Some upper_bound
  | { app = { builtin = Builtin.NSeq_const }; args; tyargs = [ val_gty ] } ->
      let f, l, other = IArray.extract3_exn args in
      let lower_bound = get_value d f in
      let upper_bound = get_value d l in
      let dv = get_value d other in
      let lb = RealValue.coerce_value lower_bound in
      let ub = RealValue.coerce_value upper_bound in
      let vals = mk_const_nseq_vals lb ub dv in
      `Some
        (NSeqValue.of_value
           { val_gty; vals; lower_bound; upper_bound; other = None })
      (* TODO: use other for NSeqs in the const case only? *)
  | { app = { builtin = Builtin.NSeq_length }; args } ->
      let a = IArray.extract1_exn args in
      let NSeqValue.{ lower_bound; upper_bound } = nsv_of_val (get_value d a) in
      let lb = RealValue.coerce_value lower_bound in
      let ub = RealValue.coerce_value upper_bound in
      let len = A.add (A.sub ub lb) A.one in
      `Some (RealValue.of_value len)
  | { app = { builtin = Builtin.NSeq_concat }; args } ->
      let a, b = IArray.extract2_exn args in
      let av = get_value d a in
      let bv = get_value d b in
      let ansv = NSeqValue.value (NSeqValue.coerce_nodevalue av) in
      let bnsv = NSeqValue.value (NSeqValue.coerce_nodevalue bv) in
      let f1 = RealValue.coerce_value ansv.lower_bound in
      let l1 = RealValue.coerce_value ansv.upper_bound in
      if A.lt l1 f1 then `Some bv
      else
        let f2 = RealValue.coerce_value bnsv.lower_bound in
        if A.compare (A.add l1 A.one) f2 <> 0 then `Some av
        else
          let upper_bound = bnsv.upper_bound in
          let vals =
            Value.M.union (fun _ _ _ -> raise Impossible) ansv.vals bnsv.vals
          in
          `Some (NSeqValue.of_value { ansv with vals; upper_bound })
  | { app = { builtin = Builtin.NSeq_relocate }; args } ->
      let a, i = IArray.extract2_exn args in
      let av = nsv_of_val (get_value d a) in
      let ubnv = RealValue.coerce_value av.upper_bound in
      let lbnv = RealValue.coerce_value av.lower_bound in
      let bdelta = A.sub ubnv lbnv in
      let nlb = RealValue.coerce_value (get_value d i) in
      let nub = A.add nlb bdelta in
      let lower_bound = RealValue.of_value nlb in
      let upper_bound = RealValue.of_value nub in
      let vals =
        Value.M.fold_left
          (fun acc k v ->
            let ki = RealValue.coerce_value k in
            let nk = A.add (A.sub ki lbnv) nlb in
            let nkv = RealValue.of_value nk in
            Value.M.add nkv v acc)
          Value.M.empty av.vals
      in
      `Some (NSeqValue.of_value { av with vals; lower_bound; upper_bound })
  | { app = { builtin = Builtin.NSeq_slice }; args } ->
      let a, f, l = IArray.extract3_exn args in
      let slice_fv = get_value d f in
      let slice_lv = get_value d l in
      let av = get_value d a in
      let ansv = NSeqValue.value (NSeqValue.coerce_nodevalue av) in
      let av_f = RealValue.coerce_value ansv.lower_bound in
      let av_l = RealValue.coerce_value ansv.upper_bound in
      let slice_f = RealValue.coerce_value slice_fv in
      let slice_l = RealValue.coerce_value slice_lv in
      let ansv' =
        if A.le av_f av_l && A.le av_f slice_f && A.le slice_l av_l then
          let lower_bound = slice_fv in
          let upper_bound = slice_lv in
          if A.lt slice_l slice_f then
            { ansv with vals = Value.M.empty; lower_bound; upper_bound }
          else
            {
              ansv with
              vals =
                Value.M.fold
                  (fun k v acc ->
                    let kval = RealValue.coerce_value k in
                    if A.ge kval slice_f && A.le kval slice_l then
                      let nk = RealValue.of_value kval in
                      Value.M.add nk v acc
                    else acc)
                  ansv.vals Value.M.empty;
              lower_bound;
              upper_bound;
            }
        else ansv
      in
      `Some (NSeqValue.of_value ansv')
  | { app = { builtin = Builtin.NSeq_update }; args } ->
      let a, b = IArray.extract2_exn args in
      let av = get_value d a in
      let bv = get_value d b in
      let ansv = nsv_of_val av in
      let bnsv = nsv_of_val bv in
      let av_f = RealValue.coerce_value ansv.lower_bound in
      let av_l = RealValue.coerce_value ansv.upper_bound in
      let bv_f = RealValue.coerce_value bnsv.lower_bound in
      let bv_l = RealValue.coerce_value bnsv.upper_bound in
      if A.le av_f av_l && A.le bv_f bv_l && A.le av_f bv_f && A.le bv_l av_l
      then
        `Some
          (NSeqValue.of_value
             { ansv with vals = Value.M.set_union bnsv.vals ansv.vals })
        (* assuming all values are defined *)
      else `Some av
  | { app = { builtin = Builtin.NSeq_content }; args } ->
      let ns = IArray.extract1_exn args in
      let NSeqValue.{ vals; val_gty } = nsv_of_val (get_value d ns) in
      let gv = get_value d (Ground.node g) in
      if Options.get d Colibri2_theories_utils.Array_opt.array_use_nseqs then
        let NSeqValue.{ other } =
          NSeqValue.value (NSeqValue.coerce_nodevalue gv)
        in
        `Some (mk_array_nsval val_gty vals (Opt.get other))
      else
        let ind_gty = Ground.Ty.int in
        `Some
          (Colibri2_theories_array.Array_value.mk_array_value ind_gty val_gty
             vals gv)
  (* Seq *)
  | { app = { builtin = Builtin.Seq_empty }; ty = { args = [ val_gty ] } } ->
      let r = NSeqValue.of_value @@ empty_seq_nsval val_gty in
      `Some r
  | { app = { builtin = Builtin.Seq_len }; args } ->
      let a = IArray.extract1_exn args in
      let NSeqValue.{ upper_bound } =
        NSeqValue.value
          (NSeqValue.coerce_nodevalue
             (Opt.get_exn Impossible (Egraph.get_value d a)))
      in
      let v = RealValue.coerce_value upper_bound in
      `Some (RealValue.of_value (A.add v A.one))
  | { app = { builtin = Builtin.Seq_unit }; args; ty = { args = [ val_gty ] } }
    ->
      let a = IArray.extract1_exn args in
      let av = get_value d a in
      let ansv =
        NSeqValue.
          {
            lower_bound = RealValue.of_value A.zero;
            upper_bound = RealValue.of_value A.zero;
            vals = Value.M.singleton (RealValue.of_value A.zero) av;
            val_gty;
            other = None;
          }
      in
      `Some (NSeqValue.of_value ansv)
  | { app = { builtin = Builtin.Seq_get }; args } ->
      let a, i = IArray.extract2_exn args in
      let av = get_value d a in
      let iv = get_value d i in
      let NSeqValue.{ vals; upper_bound } = NSeqValue.coerce_value av in
      let aub = RealValue.coerce_value upper_bound in
      let ivv = RealValue.coerce_value iv in
      if A.le A.zero ivv && A.le ivv aub then
        match Value.M.find_opt iv vals with
        | None -> `Uninterpreted
        | Some v -> `Some v
      else `Uninterpreted
  | { app = { builtin = Builtin.Seq_set }; args } ->
      let a, i, v = IArray.extract3_exn args in
      let av = get_value d a in
      let iv = get_value d i in
      let vv = get_value d v in
      let (NSeqValue.{ upper_bound = aub; vals = avals } as avns) =
        NSeqValue.coerce_value av
      in
      let iv_val = RealValue.coerce_value iv in
      let aub_val = RealValue.coerce_value aub in
      let avns' =
        if A.le A.zero iv_val && A.le iv_val aub_val then
          { avns with vals = Value.M.add iv vv avals }
        else avns
      in
      Debug.dprintf2 debug ">Seq_seq: res: %a" NSeqValue.pp_nsval avns';
      `Some (NSeqValue.of_value avns')
  | { app = { builtin = Builtin.Seq_update }; args } ->
      let a, i, b = IArray.extract3_exn args in
      let av = get_value d a in
      let iv = get_value d i in
      let bv = get_value d b in
      let (NSeqValue.{ upper_bound = aub; vals = avals } as avns) =
        NSeqValue.coerce_value av
      in
      let iv_val = RealValue.coerce_value iv in
      let NSeqValue.{ vals = bvals; upper_bound = bub } =
        NSeqValue.coerce_value bv
      in
      let avns' =
        let aub_val = RealValue.coerce_value aub in
        let bub_val = RealValue.coerce_value bub in
        (* TODO: fix semantics of Seq_update *)
        if
          A.le A.zero iv_val && A.le iv_val aub_val
          &&
          (* A.gt bub_val A.zero *)
          A.le (A.add iv_val bub_val) aub_val
        then
          {
            avns with
            vals =
              Value.M.fold
                (fun k v acc ->
                  let kval = RealValue.coerce_value k in
                  assert (A.le (A.add kval iv_val) aub_val);
                  if A.le (A.add kval iv_val) aub_val then
                    let nkval = A.add kval iv_val in
                    Value.M.add (RealValue.of_value nkval) v acc
                  else acc)
                bvals avals;
          }
        else avns
      in
      `Some (NSeqValue.of_value avns')
  | {
   app = { builtin = Builtin.Seq_concat };
   args;
   ty = { args = [ val_gty ] };
  } ->
      let vals, ub_val =
        IArray.fold
          ~f:(fun (vals, ub_val) a ->
            let av = get_value d a in
            let NSeqValue.{ upper_bound = aub; vals = avals } =
              NSeqValue.coerce_value av
            in
            Debug.dprintf2 debug ">Seq_concat: %a" NSeqValue.pp_nsval
              (NSeqValue.coerce_value av);
            let aub_val = RealValue.coerce_value aub in
            let nlb_val = A.add ub_val A.one in
            let nub_val = A.add aub_val nlb_val in
            let nvals =
              Value.M.fold
                (fun k v acc ->
                  let kval = RealValue.coerce_value k in
                  let nkval = A.add kval nlb_val in
                  let nk = RealValue.of_value nkval in
                  Value.M.add_new Impossible nk v acc)
                avals vals
            in
            (nvals, nub_val))
          ~init:(Value.M.empty, A.minus_one)
          args
      in
      `Some
        (NSeqValue.of_value
           {
             lower_bound = RealValue.of_value A.zero;
             upper_bound = RealValue.of_value ub_val;
             vals;
             val_gty;
             other = None;
           })
  | {
   app = { builtin = Builtin.Seq_extract };
   args;
   ty = { args = [ val_gty ] };
  } ->
      Debug.dprintf0 debug ">Seq_extract";
      let a, i, l = IArray.extract3_exn args in
      let av = get_value d a in
      let iv = get_value d i in
      let lv = get_value d l in
      let (NSeqValue.{ upper_bound = aub; vals = avals } as avns) =
        NSeqValue.coerce_value av
      in
      let aub_val = RealValue.coerce_value aub in
      let iv_val = RealValue.coerce_value iv in
      let lv_val = RealValue.coerce_value lv in
      Debug.dprintf6 debug ">Seq_extract %a  [%a; %a]" NSeqValue.pp_nsval avns
        A.pp iv_val A.pp lv_val;
      let avns' =
        if
          A.ge iv_val A.zero && A.le iv_val aub_val && A.gt lv_val A.zero
          && A.le (A.add iv_val (A.sub lv_val A.one)) aub_val
          (* TODO: fix semantics of Seq_extract *)
        then
          NSeqValue.
            {
              lower_bound = RealValue.of_value A.zero;
              upper_bound = RealValue.of_value (A.sub lv_val A.one);
              vals =
                Value.M.fold
                  (fun k v acc ->
                    let kval = RealValue.coerce_value k in
                    if A.ge kval iv_val && A.lt kval (A.add iv_val lv_val) then
                      let nkval = A.sub kval iv_val in
                      let nk = RealValue.of_value nkval in
                      Value.M.add nk v acc
                    else acc)
                  avals Value.M.empty;
              val_gty;
              other = None;
            }
        else empty_seq_nsval val_gty
      in
      `Some (NSeqValue.of_value avns')
  | _ -> `None

let compute d g =
  Debug.dprintf2 debug "NSV.compute %a" Ground.pp g;
  compute_aux d g (Ground.sem g)

let compute_arr d g =
  Debug.dprintf2 debug "NSV.compute_arr %a" Ground.pp g;
  let gs = Ground.sem g in
  match compute_aux d g gs with
  | (`Some _ | `Uninterpreted) as res -> res
  | `None -> (
      match gs with
      | { app = { builtin = Expr.Select; _ }; args; _ } -> (
          let a, k = IArray.extract2_exn args in
          let av = get_value d a in
          let kv = get_value d k in
          let NSeqValue.{ vals; other } = NSeqValue.coerce_value av in
          match Value.M.find_opt kv vals with
          | Some fv -> `Some fv
          | None -> `Some (Opt.get other))
      | { app = { builtin = Expr.Store; _ }; args; _ } ->
          let a, k, v = IArray.extract3_exn args in
          let av = get_value d a in
          let kv = get_value d k in
          let vv = get_value d v in
          let (NSeqValue.{ vals; other } as av) = NSeqValue.coerce_value av in
          `Some
            (NSeqValue.of_value
               {
                 av with
                 vals =
                   (if Value.equal vv (Opt.get other) then
                      Value.M.remove kv vals
                    else Value.M.add kv vv vals);
               })
      | _ -> `None)

let common_dep_ordering_aux _ g n =
  match g with
  | Ground.{ app = { builtin = Builtin.NSeq_first }; args } ->
      let a = IArray.extract1_exn args in
      Some [ (n, a) ]
  | { app = { builtin = Builtin.NSeq_last }; args } ->
      let a = IArray.extract1_exn args in
      Some [ (n, a) ]
  | { app = { builtin = Builtin.NSeq_length }; args } ->
      let a = IArray.extract1_exn args in
      Some [ (n, a) ]
      (* make length depend on first and last? *)
  | { app = { builtin = Builtin.NSeq_get }; args } ->
      let a, k = IArray.extract2_exn args in
      Some [ (k, a); (n, a) ]
  | { app = { builtin = Builtin.NSeq_set }; args } ->
      let a, k, v = IArray.extract3_exn args in
      Some [ (k, n); (a, n); (v, n); (k, a); (v, a) ]
  | { app = { builtin = Builtin.NSeq_const }; args } ->
      let f, l, v = IArray.extract3_exn args in
      Some [ (f, n); (l, n); (v, n) ]
  | { app = { builtin = Builtin.NSeq_relocate }; args } ->
      let a, i = IArray.extract2_exn args in
      Some [ (i, a); (i, n); (a, n) ]
  | { app = { builtin = Builtin.NSeq_concat }; args } ->
      let a, b = IArray.extract2_exn args in
      Some [ (a, n); (b, n) ]
  | { app = { builtin = Builtin.NSeq_slice }; args } ->
      let a, f, l = IArray.extract3_exn args in
      Some [ (f, n); (l, n); (a, n) ]
  | { app = { builtin = Builtin.NSeq_update }; args } ->
      let a, b = IArray.extract2_exn args in
      Some [ (a, n); (b, n) ]
  | { app = { builtin = Builtin.NSeq_content }; args = _ } ->
      (* TODO *)
      None
  | { app = { builtin = Builtin.Seq_unit }; args } ->
      let a = IArray.extract1_exn args in
      Some [ (a, n) ]
  | { app = { builtin = Builtin.Seq_len }; args } ->
      let a = IArray.extract1_exn args in
      Some [ (a, n) ]
  | { app = { builtin = Builtin.Seq_get }; args } ->
      let a, b = IArray.extract2_exn args in
      Some [ (a, n); (b, n) ]
  | { app = { builtin = Builtin.Seq_set }; args } ->
      let a, b, c = IArray.extract3_exn args in
      Some [ (a, n); (b, n); (c, n) ]
  | { app = { builtin = Builtin.Seq_update }; args } ->
      let a, b, c = IArray.extract3_exn args in
      Some [ (a, n); (b, n); (c, n) ]
  | { app = { builtin = Builtin.Seq_concat }; args } ->
      Some (List.map (fun n' -> (n', n)) (IArray.to_list args))
  | { app = { builtin = Builtin.Seq_extract }; args } ->
      let a, b, c = IArray.extract3_exn args in
      Some [ (a, n); (b, n); (c, n) ]
  | _ -> None

let dep_ordering env g =
  let n = Ground.node g in
  let gs = Ground.sem g in
  common_dep_ordering_aux env gs n

let arr_dep_ordering env g =
  let n = Ground.node g in
  let gs = Ground.sem g in
  match common_dep_ordering_aux env gs n with
  | Some l -> Some l
  | None -> (
      match gs with
      | { app = { builtin = Expr.Select; _ }; args; _ } ->
          let a, k = IArray.extract2_exn args in
          Some [ (k, a); (n, a) ]
      | { app = { builtin = Expr.Store; _ }; args; _ } ->
          let a, k, v = IArray.extract3_exn args in
          Some [ (k, n); (a, n); (v, n); (k, a); (v, a) ]
      | _ -> None)

type types =
  | Array of { ind_gty : Ground.Ty.t; val_gty : Ground.Ty.t }
  | NSeq of { val_gty : Ground.Ty.t }
  | Seq of { val_gty : Ground.Ty.t }

let most_precise_type_of d n =
  let fold ty best =
    match (ty, best) with
    | ( Ground.Ty.{ app = { builtin = Expr.Array }; args = [ ind_gty; val_gty ] },
        None ) ->
        Some (Array { ind_gty; val_gty })
    | ( Ground.Ty.{ app = { builtin = Builtin.NSeq }; args = [ val_gty ] },
        (None | Some (Array _)) ) ->
        Some (NSeq { val_gty })
    | ( { app = { builtin = Builtin.Seq }; args = [ val_gty ] },
        (None | Some (Array _ | NSeq _)) ) ->
        Some (Seq { val_gty })
    | _ -> best
  in
  Ground.Ty.S.fold fold (Ground.tys d n) None

let interp_node interp d n =
  match most_precise_type_of d n with
  | Some (NSeq { val_gty } | Seq { val_gty }) ->
      interp_nseq_node_aux d interp val_gty n
  | _ -> None

let indv_to_vals ~rec_node_values d _ind_gty _val_gty inode vnode m =
  let open Interp.Seq in
  let+ iv =
    match Egraph.get_value d inode with
    | None -> rec_node_values d inode
    | Some ind_v -> Interp.Seq.return ind_v
  and* vv =
    match Egraph.get_value d vnode with
    | None -> rec_node_values d vnode
    | Some v -> Interp.Seq.return v
  in
  Value.M.add iv vv m

let map_to_values ~rec_node_values d ind_gty val_gty m =
  let open Interp.Seq in
  Id.M.fold
    (fun i (v, _, _) acc ->
      let inode = Id.get d i in
      let acc =
        let+ acc = acc in
        indv_to_vals ~rec_node_values d ind_gty val_gty inode v acc
      in
      Interp.Seq.concat acc)
    m
    (Interp.Seq.return Value.M.empty)

let all_arrays_kvs d ?(values = Value.M.empty) ind_gty val_gty =
  let size = Sequence.unfold ~init:() ~f:(fun () -> Some ((), ())) in
  let vals_ =
    Sequence.unfold_with size ~init:(Sequence.singleton Value.M.empty)
      ~f:(fun seq () ->
        let open Std.Sequence in
        let seq =
          let vals =
            let+ vals = seq and* indv = Interp.ty d ind_gty in
            match Value.M.find_opt indv values with
            | Some valv -> Sequence.singleton (Value.M.add indv valv vals)
            | None ->
                let+ valv = Interp.ty d val_gty in
                Value.M.add indv valv vals
          in
          Sequence.interleave vals
        in
        Yield { state = seq; value = seq })
  in
  let vals_ = Interp.Seq.of_seq @@ Sequence.interleave vals_ in
  let+ vals = vals_ and* other = Interp.Seq.of_seq @@ Interp.ty d val_gty in
  (vals, other)

let interp_node_arr rec_node_values d n =
  match most_precise_type_of d n with
  | Some (NSeq { val_gty } | Seq { val_gty }) ->
      interp_nseq_node_aux d rec_node_values val_gty n
  | Some (Array { ind_gty; val_gty }) ->
      let m =
        Opt.get_def Id.M.empty (NSeq_dom.get_kvs d (Id.NSeq.get_id d n))
      in
      Some
        (let vo =
           let+ values = map_to_values ~rec_node_values d ind_gty val_gty m in
           all_arrays_kvs d ~values ind_gty val_gty
         in
         let+ vals, other = Interp.Seq.concat vo in
         mk_array_nsval val_gty vals other)
  | None -> None

let interp_ty d (ty : Ground.Ty.t) =
  match ty with
  | { app = { builtin = Builtin.NSeq }; args = [ val_gty ] } ->
      Some
        (let++ lower_bound, upper_bound, vals =
           Sequence.concat (all_nseqs d val_gty)
         in
         NSeqValue.nodevalue
           (NSeqValue.index
              { val_gty; vals; lower_bound; upper_bound; other = None }))
  | { app = { builtin = Builtin.Seq }; args = [ val_gty ] } ->
      Some
        (let++ lower_bound, upper_bound, vals =
           Sequence.concat (all_seqs d val_gty)
         in
         NSeqValue.nodevalue
           (NSeqValue.index
              { val_gty; vals; lower_bound; upper_bound; other = None }))
  | _ -> None

let rec add_to_seq m d inds val_gty =
  let open Std.Sequence in
  match inds with
  | [] -> m
  | ind :: inds ->
      let m =
        let+ vals = m and* valv = Interp.ty d val_gty in
        Value.M.add ind valv vals
      in
      add_to_seq m d inds val_gty

let all_arrays d ind_gty val_gty =
  let open Std.Sequence in
  let size =
    Sequence.unfold
      ~init:(Interp.ty d ind_gty, [ [] ])
      ~f:(fun (elts, seq0) ->
        match Sequence.next elts with
        | Some (elt, elts) ->
            let seq = Base.List.map seq0 ~f:(List.cons elt) in
            let seq' = seq0 @ seq in
            Some (Sequence.of_list seq, (elts, seq'))
        | None -> None)
  in
  let size = Sequence.interleave size in
  let vals_ =
    Sequence.map size ~f:(fun inds ->
        let seq =
          add_to_seq (Sequence.singleton Value.M.empty) d inds val_gty
        in
        seq)
  in
  let vals_ = Sequence.interleave vals_ in
  let+ vals = vals_ and* other = Interp.ty d val_gty in
  mk_array_nsval val_gty vals other

let interp_ty_arr d ty =
  match interp_ty d ty with
  | Some _ as r -> r
  | None -> (
      match ty with
      | { app = { builtin = Expr.Array }; args = [ ind_gty; val_gty ] } ->
          Some (all_arrays d ind_gty val_gty)
      | _ -> None)

let init env =
  let compute =
    if Options.get env Colibri2_theories_utils.Array_opt.array_use_nseqs then (
      Interp.Register.dependency_by_ground_term env arr_dep_ordering;
      Interp.Register.node env interp_node_arr;
      Interp.Register.ty env interp_ty_arr;
      compute_arr)
    else (
      Interp.Register.dependency_by_ground_term env dep_ordering;
      Interp.Register.node env interp_node;
      Interp.Register.ty env interp_ty;
      compute)
  in
  Interp.Register.check env (fun d t ->
      match compute d t with
      | `None -> NA
      | `Some r ->
          Interp.check_of_bool (Value.equal r (get_value d (Ground.node t)))
      | `Uninterpreted ->
          Interp.check_of_bool (Uninterp.On_uninterpreted_domain.check d t));
  Interp.Register.compute env (fun d t ->
      match compute d t with
      | `None -> NA
      | `Some v -> Value v
      | `Uninterpreted -> Uninterp.On_uninterpreted_domain.compute d t)

let propagate_value d g =
  let compute =
    if Options.get d Colibri2_theories_utils.Array_opt.array_use_nseqs then
      compute_arr
    else compute
  in
  Interp.WatchArgs.create d
    (fun d g ->
      match compute d g with
      | `None -> raise Impossible
      | `Some v -> Egraph.set_value d (Ground.node g) v
      | `Uninterpreted ->
          Colibri2_theories_quantifiers.Uninterp.On_uninterpreted_domain
          .propagate d g)
    g
