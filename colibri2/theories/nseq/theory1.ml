(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_core
open Colibri2_popop_lib
open Common
module RealValue = Colibri2_theories_LRA.RealValue

let converter env f =
  let s = Ground.sem f in
  let n = Ground.node f in
  match s with
  | { app = { builtin = Builtin.NSeq_set }; args; tyargs = [ val_gty ] } ->
      NSeq_value.propagate_value env f;
      let a, i, v = IArray.extract3_exn args in
      Egraph.register env a;
      Egraph.register env i;
      Egraph.register env v;
      let aid = NSeq_dom.new_nseq env a in
      if NSeq_dom.is_empty env aid then Egraph.merge env a n
      else
        let fst, _, lst, _ = Bounds.get_mk_bounds_n env ~val_gty a in
        Bounds.upd_bounds env n fst lst;
        let cond = mk_in_bounds env i fst lst in
        Egraph.register env cond;
        let cons env =
          let nid = Id.NSeq.get_id_safe env n in
          let aid = Id.NSeq.get_id env a in
          let iid = Id.Index.get_id_safe env i in
          NSeq_dom.add_kv env nid ~b:aid iid v
        in
        let alt env = Egraph.merge env a n in
        new_decision env cond "Decision from Theory1.NSeq_set" ~cons ~alt
          (fun _ -> bool_dec ~cons ~alt cond)
  | { app = { builtin = Builtin.NSeq_get }; args; tyargs = [ val_gty ] } ->
      NSeq_value.propagate_value env f;
      let a, i = IArray.extract2_exn args in
      Egraph.register env a;
      Egraph.register env i;
      let aid = NSeq_dom.new_nseq env a in
      if NSeq_dom.is_empty env aid then ()
      else
        let fst, _, lst, _ = Bounds.get_mk_bounds_n env ~val_gty a in
        let cond = mk_in_bounds env i fst lst in
        Egraph.register env cond;
        let cons env =
          let aid = Id.NSeq.get_id_safe env a in
          let iid = Id.Index.get_id_safe env i in
          NSeq_dom.add_kv env aid iid n
        in
        new_decision env cond "Decision from Theory1.NSeq_get" ~cons (fun _ ->
            bool_dec ~cons cond)
  | { app = { builtin = Builtin.NSeq_concat }; args; tyargs = [ val_gty ] } ->
      NSeq_value.propagate_value env f;
      let a, b = IArray.extract2_exn args in
      Egraph.register env a;
      Egraph.register env b;
      let aid = NSeq_dom.new_nseq env a in
      if NSeq_dom.is_empty env aid then Egraph.merge env n b
      else (
        ignore (NSeq_dom.new_nseq env b);
        let afst, _, alst, _ = Bounds.get_mk_bounds_n env ~val_gty a in
        let bfst, _, blst, _ = Bounds.get_mk_bounds_n env ~val_gty b in
        let a_isnt_empty = mk_le env [ afst; alst ] in
        Egraph.register env a_isnt_empty;
        let b_isnt_empty = mk_le env [ bfst; blst ] in
        Egraph.register env b_isnt_empty;
        let cond =
          Equality.equality env [ mk_add env alst RealValue.one; bfst ]
        in
        Egraph.register env cond;
        let cons env =
          Bounds.upd_bounds env n afst blst;
          (* TODO: reasoning over bounds should be better than this, it should
             detect on its own the simplification. *)
          if Egraph.is_equal env n a then
            NSeq_dom.set_is_empty env (Id.NSeq.get_id env b)
          else if Egraph.is_equal env n b then
            NSeq_dom.set_is_empty env (Id.NSeq.get_id env a)
          else NSeq_dom.add_ns_concat env n a b
        in
        let n_eq_a env = Egraph.merge env n a in
        let a_is_empty env =
          NSeq_dom.set_is_empty env (Id.NSeq.get_id env a);
          Egraph.merge env n b
        in
        let b_is_empty env =
          NSeq_dom.set_is_empty env (Id.NSeq.get_id env b);
          Egraph.merge env n a
        in
        new_decision env a_isnt_empty "Decision from Theory1.NSeq_concat 1"
          ~cons:(fun env ->
            new_decision env b_isnt_empty "Decision from Theory1.NSeq_concat 2"
              ~cons:(fun env ->
                new_decision env cond "Decision from Theory1.NSeq_concat 3"
                  ~cons ~alt:n_eq_a (fun _ -> bool_dec cond ~cons ~alt:n_eq_a))
              ~alt:b_is_empty
              (fun _ ->
                DecTodo
                  [
                    (fun env ->
                      Boolean.set_true env b_isnt_empty;
                      Boolean.set_true env cond;
                      cons env);
                    (fun env ->
                      Boolean.set_true env b_isnt_empty;
                      Boolean.set_false env cond;
                      n_eq_a env);
                    (fun env ->
                      Boolean.set_false env b_isnt_empty;
                      b_is_empty env);
                  ]))
          ~alt:a_is_empty
          (fun _ ->
            DecTodo
              [
                (fun env ->
                  Boolean.set_true env a_isnt_empty;
                  Boolean.set_true env b_isnt_empty;
                  Boolean.set_true env cond;
                  cons env);
                (fun env ->
                  Boolean.set_true env a_isnt_empty;
                  Boolean.set_false env b_isnt_empty;
                  b_is_empty env);
                (fun env ->
                  Boolean.set_true env a_isnt_empty;
                  Boolean.set_true env b_isnt_empty;
                  Boolean.set_false env cond;
                  n_eq_a env);
                (fun env ->
                  Boolean.set_false env a_isnt_empty;
                  a_is_empty env);
              ]))
  | { app = { builtin = Builtin.NSeq_slice }; args; tyargs = [ val_gty ] } ->
      let a, f, l = IArray.extract3_exn args in
      Egraph.register env a;
      Egraph.register env f;
      Egraph.register env l;
      let aid = NSeq_dom.new_nseq env a in
      if NSeq_dom.is_empty env aid then Egraph.merge env n a
      else
        let fst, _, lst, _ = Bounds.get_mk_bounds_n env ~val_gty a in
        let c1 = mk_le env [ fst; f ] in
        let c2 = mk_le env [ l; lst ] in
        let cond = Boolean._and env [ c1; c2 ] in
        let c3 = mk_le env [ f; l ] in
        Egraph.register env c3;
        Egraph.register env cond;
        let cons env =
          if Egraph.is_equal env f fst && Egraph.is_equal env l lst then
            Egraph.merge env n a
          else (
            Bounds.upd_bounds env n f l;
            if not (Egraph.is_equal env n a) then
              NSeq_dom.add_ns_slice env n f l a)
        in
        let alt1 env = Egraph.merge env n a in
        let alt2 env =
          Bounds.upd_bounds env n f l;
          NSeq_dom.set_is_empty env (Id.NSeq.get_id_safe env n)
        in
        new_decision env cond "Decision from Theory1.NSeq_slice 1"
          ~cons:(fun env ->
            new_decision env c3 "Decision from Theory1.NSeq_slice 2" ~cons
              ~alt:alt2 (fun _ -> bool_dec ~cons ~alt:alt2 c3))
          ~alt:alt1
          (fun env ->
            match Boolean.is env c3 with
            | Some true ->
                DecTodo
                  [
                    (fun env ->
                      Boolean.set_true env cond;
                      cons env);
                    (fun env ->
                      Boolean.set_false env cond;
                      alt1 env);
                  ]
            | Some false ->
                alt1 env;
                DecNo
            | None ->
                DecTodo
                  [
                    (fun env ->
                      Boolean.set_true env cond;
                      Boolean.set_true env c3;
                      cons env);
                    (fun env ->
                      Boolean.set_true env cond;
                      Boolean.set_false env c3;
                      alt2 env);
                    (fun env ->
                      Boolean.set_false env cond;
                      alt1 env);
                  ])
  | { app = { builtin = Builtin.NSeq_update }; args; tyargs = [ val_gty ] } ->
      let a, b = IArray.extract2_exn args in
      Egraph.register env a;
      Egraph.register env b;
      let aid = NSeq_dom.new_nseq env a in
      let bid = NSeq_dom.new_nseq env b in
      if NSeq_dom.is_empty env aid || NSeq_dom.is_empty env bid then
        Egraph.merge env n a
      else
        let afst, _, alst, _ = Bounds.get_mk_bounds_n env ~val_gty a in
        Bounds.upd_bounds env n afst alst;
        let bfst, _, blst, _ = Bounds.get_mk_bounds_n env ~val_gty b in
        if Egraph.is_equal env afst bfst && Egraph.is_equal env alst blst then
          Egraph.merge env n b
        else
          let a_isnt_empty = mk_le env [ afst; alst ] in
          Egraph.register env a_isnt_empty;
          let b_isnt_empty = mk_le env [ bfst; blst ] in
          Egraph.register env b_isnt_empty;
          let a_is_empty env =
            NSeq_dom.set_is_empty env (Id.NSeq.get_id env a);
            Egraph.merge env n a
          in
          let b_is_empty env =
            NSeq_dom.set_is_empty env (Id.NSeq.get_id env b);
            Egraph.merge env n a
          in
          let alt env = Egraph.merge env n a in
          let cons1 env =
            let cons2 env =
              let in_bounds =
                Boolean._and env
                  [ mk_le env [ afst; bfst ]; mk_le env [ blst; alst ] ]
              in
              Egraph.register env in_bounds;
              let cons3 env =
                NSeq_dom.add_ns_update env val_gty n a afst alst b bfst blst
              in
              new_decision env in_bounds "Decision from Theory1.NSeq_update 3"
                ~cons:cons3 ~alt (fun _ -> bool_dec ~cons:cons3 ~alt in_bounds)
            in
            new_decision env b_isnt_empty "Decision from Theory1.NSeq_update 2"
              ~cons:cons2 ~alt (fun _ ->
                DecTodo
                  [
                    (fun env ->
                      Boolean.set_false env b_isnt_empty;
                      alt env);
                    (fun env ->
                      Boolean.set_true env b_isnt_empty;
                      let in_bounds =
                        Boolean._and env
                          [ mk_le env [ afst; bfst ]; mk_le env [ blst; alst ] ]
                      in
                      Egraph.register env in_bounds;
                      Boolean.set_false env in_bounds;
                      alt env);
                    (fun env ->
                      Boolean.set_true env b_isnt_empty;
                      let in_bounds =
                        Boolean._and env
                          [ mk_le env [ afst; bfst ]; mk_le env [ blst; alst ] ]
                      in
                      Egraph.register env in_bounds;
                      Boolean.set_true env in_bounds;
                      NSeq_dom.add_ns_update env val_gty n a afst alst b bfst
                        blst);
                  ])
          in
          new_decision env a_isnt_empty "Decision from Theory1.NSeq_update 1"
            ~cons:cons1 ~alt:a_is_empty (fun _ ->
              DecTodo
                [
                  (fun env ->
                    Boolean.set_false env a_isnt_empty;
                    a_is_empty env);
                  (fun env ->
                    Boolean.set_true env a_isnt_empty;
                    Boolean.set_false env b_isnt_empty;
                    b_is_empty env);
                  (fun env ->
                    Boolean.set_true env a_isnt_empty;
                    Boolean.set_true env b_isnt_empty;
                    let in_bounds =
                      Boolean._and env
                        [ mk_le env [ afst; bfst ]; mk_le env [ blst; alst ] ]
                    in
                    Egraph.register env in_bounds;
                    Boolean.set_false env in_bounds;
                    alt env);
                  (fun env ->
                    Boolean.set_true env a_isnt_empty;
                    Boolean.set_true env b_isnt_empty;
                    let in_bounds =
                      Boolean._and env
                        [ mk_le env [ afst; bfst ]; mk_le env [ blst; alst ] ]
                    in
                    Egraph.register env in_bounds;
                    Boolean.set_true env in_bounds;
                    NSeq_dom.add_ns_update env val_gty n a afst alst b bfst blst);
                ])
  (*  *)
  (* (* Works better in some cases *)
     let in_bounds =
       Boolean._and env
         [ mk_le env [ afst; bfst ]; mk_le env [ blst; alst ] ]
     in
     Egraph.register env in_bounds;
     let cons env =
       add_ns_update env val_gty n a afst alst b bfst blst
     in
     let alt env = Egraph.merge env n a in
     new_decision env in_bounds "Decision from Theory1.NSeq_update" ~cons
       ~alt (fun _ -> bool_dec ~cons ~alt in_bounds) *)
  | _ -> ()

let th_register env =
  Bounds.register_bounds_change_hook env Relations.on_bounds_change;
  Colibri2_theories_utils.Nsa_content.(
    set_new_arr_kv_hook env NSeq_dom.add_kv_check_bounds);
  DaemonLastEffortLateUncontextual.schedule_immediately env
    (Common_conv.check_eq_nseqs
       ~mk_gets:(not (Options.get env ns_no_get_intro)));
  Common_conv.init_hooks env;
  Id.Index.register_merge_hook env (fun env id1 id2 ->
      WEGraph.eq_indices_norm env id1 id2);
  Id.NSeq.register_merge_hook env (fun env id1 id2 ->
      WEGraph.eq_nseqs_norm env id1 id2;
      Relations.eq_nseqs_norm env id1 id2);
  Ground.register_converter env converter
