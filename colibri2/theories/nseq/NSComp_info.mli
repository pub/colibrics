(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

module NSC : sig
  type atom = Node.t
  type atom_pos = { a : atom; next : Node.t option }
  type t = atom_pos Id.M.t Id.M.t

  val pp_atom : ?env:_ Egraph.t -> Format.formatter -> atom -> unit
  val pp_atom_pos : ?env:_ Egraph.t -> Format.formatter -> atom_pos -> unit
  val pp : ?env:_ Egraph.t -> Format.formatter -> t -> unit
  val equal : t -> t -> bool

  module L : sig
    type t = (atom * Node.t * Node.t) list

    val pp : Format.formatter -> t -> unit
  end
end
