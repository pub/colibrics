(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_core
open Colibri2_popop_lib
open Popop_stdlib
module RealValue = Colibri2_theories_LRA.RealValue
module P = Colibri2_theories_LRA.Polynome
module DP = Colibri2_theories_LRA.Dom_polynome

let debug =
  Debug.register_info_flag ~desc:"Debugging messages of the n-sequence theory"
    "NSeq"

let ns_no_get_intro =
  Options.register ~pp:Fmt.bool "NSeq.intro"
    Cmdliner.Arg.(
      value & flag
      & info [ "nseq-no-get-intro" ]
          ~doc:
            "Don't introduce nseq.get terms to detect equalities or \
             differences between n-sequences")

let array_nseqs_no_ppg =
  Options.register ~pp:Fmt.bool "Array.nseqs-no-ppg"
    Cmdliner.Arg.(
      value & flag
      & info [ "array-nseqs-no-ppg" ]
          ~doc:
            "Use the NSeq solver for the theory of Arrays with no systematic \
             propagation")

let nseq_no_dist_first =
  Options.register ~pp:Fmt.bool "NSeq.nseq-no-dist-first"
    Cmdliner.Arg.(
      value & flag
      & info [ "nseq-no-dist-first" ]
          ~doc:
            "When checking equalities, do not start with distinct NSeqs. Do it \
             for all.")

(* exceptions *)
exception NotAnNSeqType of Expr.ty

let () =
  Printexc.register_printer (function
    | NotAnNSeqType ty ->
        Some (Fmt.str "the type %a is not an NSeq type!" Expr.Ty.pp ty)
    | _ -> None)

(* NSeq value *)
module NSeqValue = struct
  type nsval = {
    val_gty : Ground.Ty.t;
    vals : Value.t Value.M.t;
    upper_bound : Value.t;
    lower_bound : Value.t;
    other : Value.t option;
  }

  let pp_nsval fmt { val_gty; vals; upper_bound; lower_bound; other } =
    Format.fprintf fmt "NSeq(%a: [%a; %a]: {%a} (%a))" Ground.Ty.pp val_gty
      Value.pp lower_bound Value.pp upper_bound (Value.M.pp Value.pp) vals
      (Opt.pp Value.pp) other

  include Value.Register (struct
    module T = struct
      type t = nsval = {
        val_gty : Ground.Ty.t;
        vals : Value.t Value.M.t;
        upper_bound : Value.t;
        lower_bound : Value.t;
        other : Value.t Base.Option.t;
      }
      [@@deriving eq, ord, hash]

      let pp = pp_nsval
    end

    include T
    include MkDatatype (T)

    let name = "NSeq.model.val"
  end)
end

(* Builtins *)
module Builtin = struct
  type _ Expr.t +=
    | NSeq
    | NSeq_first
    | NSeq_last
    | NSeq_content
    | NSeq_length
    | NSeq_get
    | NSeq_set
    | NSeq_const
    | NSeq_relocate
    | NSeq_concat
    | NSeq_slice
    | NSeq_update
    | Seq
    | Seq_empty
    | Seq_unit
    | Seq_len
    | Seq_get
    | Seq_set
    | Seq_update
    | Seq_concat
    | Seq_extract

  let nseq_ty_const : Expr.Term.ty_const =
    Expr.Id.mk ~name:"NSeq" ~builtin:NSeq
      (Dolmen_std.Path.global "NSeq")
      Expr.{ arity = 1; alias = No_alias }

  let nseq_ty ty = Expr.Ty.apply nseq_ty_const [ ty ]
  let val_ty_var = Expr.Ty.Var.mk "val_ty"
  let val_ty = Expr.Ty.of_var val_ty_var
  let val_nseq_ty = nseq_ty val_ty
  let nsi_v = Expr.Term.Var.mk "nsi_v" Expr.Ty.int
  let nsj_v = Expr.Term.Var.mk "nsj_v" Expr.Ty.int
  let nsk_v = Expr.Term.Var.mk "nsk_v" Expr.Ty.int
  let nsv_v = Expr.Term.Var.mk "nsv_v" val_ty
  let nsv'_v = Expr.Term.Var.mk "nsv_v'" val_ty
  let ns_v1 = Expr.Term.Var.mk "ns_v1" val_nseq_ty
  let ns_v2 = Expr.Term.Var.mk "ns_v2" val_nseq_ty
  let ns_v3 = Expr.Term.Var.mk "ns_v3" val_nseq_ty
  let fst_v1 = Expr.Term.Var.mk "fst_v1" Expr.Ty.int
  let lst_v1 = Expr.Term.Var.mk "lst_v1" Expr.Ty.int
  let fst_v2 = Expr.Term.Var.mk "fst_v2" Expr.Ty.int
  let lst_v2 = Expr.Term.Var.mk "lst_v2" Expr.Ty.int
  let nsi_t = Expr.Term.of_var nsi_v
  let nsj_t = Expr.Term.of_var nsj_v
  let nsk_t = Expr.Term.of_var nsk_v
  let nsv_t = Expr.Term.of_var nsv_v
  let nsv'_t = Expr.Term.of_var nsv'_v
  let ns_t1 = Expr.Term.of_var ns_v1
  let ns_t2 = Expr.Term.of_var ns_v2
  let ns_t3 = Expr.Term.of_var ns_v3
  let fst_t1 = Expr.Term.of_var fst_v1
  let lst_t1 = Expr.Term.of_var lst_v1
  let fst_t2 = Expr.Term.of_var fst_v2
  let lst_t2 = Expr.Term.of_var lst_v2

  let nseq_first : Dolmen_std.Expr.term_cst =
    let a = Expr.Ty.Var.mk "alpha" in
    let a_ty = Expr.Ty.of_var a in
    let a_ns_ty = nseq_ty a_ty in
    Expr.Id.mk ~name:"nseq.first" ~builtin:NSeq_first
      (Dolmen_std.Path.global "nseq.first")
      (Expr.Ty.pi [ a ] (Expr.Ty.arrow [ a_ns_ty ] Expr.Ty.int))

  let nseq_last : Dolmen_std.Expr.term_cst =
    let a = Expr.Ty.Var.mk "alpha" in
    let a_ty = Expr.Ty.of_var a in
    let a_ns_ty = nseq_ty a_ty in
    Expr.Id.mk ~name:"nseq.last" ~builtin:NSeq_last
      (Dolmen_std.Path.global "nseq.last")
      (Expr.Ty.pi [ a ] (Expr.Ty.arrow [ a_ns_ty ] Expr.Ty.int))

  let nseq_content : Dolmen_std.Expr.term_cst =
    let a = Expr.Ty.Var.mk "alpha" in
    let a_ty = Expr.Ty.of_var a in
    let a_ns_ty = nseq_ty a_ty in
    Expr.Id.mk ~name:"nseq.content" ~builtin:NSeq_content
      (Dolmen_std.Path.global "nseq.content")
      (Expr.Ty.pi [ a ]
         (Expr.Ty.arrow [ a_ns_ty ] (Expr.Ty.array Expr.Ty.int a_ty)))

  let nseq_length : Dolmen_std.Expr.term_cst =
    let a = Expr.Ty.Var.mk "alpha" in
    let a_ty = Expr.Ty.of_var a in
    let a_ns_ty = nseq_ty a_ty in
    Expr.Id.mk ~name:"nseq.length" ~builtin:NSeq_length
      (Dolmen_std.Path.global "nseq.length")
      (Expr.Ty.pi [ a ] (Expr.Ty.arrow [ a_ns_ty ] Expr.Ty.int))

  let nseq_get : Dolmen_std.Expr.term_cst =
    let a = Expr.Ty.Var.mk "alpha" in
    let a_ty = Expr.Ty.of_var a in
    let a_ns_ty = nseq_ty a_ty in
    Expr.Id.mk ~name:"nseq.get" ~builtin:NSeq_get
      (Dolmen_std.Path.global "nseq.get")
      (Expr.Ty.pi [ a ] (Expr.Ty.arrow [ a_ns_ty; Expr.Ty.int ] a_ty))

  let nseq_set : Dolmen_std.Expr.term_cst =
    let a = Expr.Ty.Var.mk "alpha" in
    let a_ty = Expr.Ty.of_var a in
    let a_ns_ty = nseq_ty a_ty in
    Expr.Id.mk ~name:"nseq.set" ~builtin:NSeq_set
      (Dolmen_std.Path.global "nseq.set")
      (Expr.Ty.pi [ a ] (Expr.Ty.arrow [ a_ns_ty; Expr.Ty.int; a_ty ] a_ns_ty))

  let nseq_const : Dolmen_std.Expr.term_cst =
    let a = Expr.Ty.Var.mk "alpha" in
    let a_ty = Expr.Ty.of_var a in
    let a_ns_ty = nseq_ty a_ty in
    Expr.Id.mk ~name:"nseq.const" ~builtin:NSeq_const
      (Dolmen_std.Path.global "nseq.const")
      (Expr.Ty.pi [ a ]
         (Expr.Ty.arrow [ Expr.Ty.int; Expr.Ty.int; a_ty ] a_ns_ty))

  let nseq_concat : Dolmen_std.Expr.term_cst =
    let a = Expr.Ty.Var.mk "alpha" in
    let a_ty = Expr.Ty.of_var a in
    let a_ns_ty = nseq_ty a_ty in
    Expr.Id.mk ~name:"nseq.concat" ~builtin:NSeq_concat
      (Dolmen_std.Path.global "nseq.concat")
      (Expr.Ty.pi [ a ] (Expr.Ty.arrow [ a_ns_ty; a_ns_ty ] a_ns_ty))

  let nseq_relocate : Dolmen_std.Expr.term_cst =
    let a = Expr.Ty.Var.mk "alpha" in
    let a_ty = Expr.Ty.of_var a in
    let a_ns_ty = nseq_ty a_ty in
    Expr.Id.mk ~name:"nseq.relocate" ~builtin:NSeq_relocate
      (Dolmen_std.Path.global "nseq.relocate")
      (Expr.Ty.pi [ a ] (Expr.Ty.arrow [ a_ns_ty; Expr.Ty.int ] a_ns_ty))

  let nseq_slice : Dolmen_std.Expr.term_cst =
    let a = Expr.Ty.Var.mk "alpha" in
    let a_ty = Expr.Ty.of_var a in
    let a_ns_ty = nseq_ty a_ty in
    Expr.Id.mk ~name:"nseq.slice" ~builtin:NSeq_slice
      (Dolmen_std.Path.global "nseq.slice")
      (Expr.Ty.pi [ a ]
         (Expr.Ty.arrow [ a_ns_ty; Expr.Ty.int; Expr.Ty.int ] a_ns_ty))

  let nseq_update : Dolmen_std.Expr.term_cst =
    let a = Expr.Ty.Var.mk "alpha" in
    let a_ty = Expr.Ty.of_var a in
    let a_ns_ty = nseq_ty a_ty in
    Expr.Id.mk ~name:"nseq.update" ~builtin:NSeq_update
      (Dolmen_std.Path.global "nseq.update")
      (Expr.Ty.pi [ a ] (Expr.Ty.arrow [ a_ns_ty; a_ns_ty ] a_ns_ty))

  let seq_ty_const : Expr.Term.ty_const =
    Expr.Id.mk ~name:"Seq" ~builtin:Seq
      (Dolmen_std.Path.global "Seq")
      Expr.{ arity = 1; alias = No_alias }

  let seq_ty ty = Expr.Ty.apply seq_ty_const [ ty ]

  let seq_empty : Dolmen_std.Expr.term_cst =
    let a = Expr.Ty.Var.mk "alphaX" in
    let a_ty = Expr.Ty.of_var a in
    let a_s_ty = seq_ty a_ty in
    Expr.Id.mk ~name:"seq.empty" ~builtin:Seq_empty
      (Dolmen_std.Path.global "seq.empty")
      (Expr.Ty.pi [ a ] a_s_ty)

  let seq_unit : Dolmen_std.Expr.term_cst =
    let a = Expr.Ty.Var.mk "alpha" in
    let a_ty = Expr.Ty.of_var a in
    let a_s_ty = seq_ty a_ty in
    Expr.Id.mk ~name:"seq.unit" ~builtin:Seq_unit
      (Dolmen_std.Path.global "seq.unit")
      (Expr.Ty.pi [ a ] (Expr.Ty.arrow [ a_ty ] a_s_ty))

  let seq_length : Dolmen_std.Expr.term_cst =
    let a = Expr.Ty.Var.mk "alpha" in
    let a_ty = Expr.Ty.of_var a in
    let a_s_ty = seq_ty a_ty in
    Expr.Id.mk ~name:"seq.len" ~builtin:Seq_len
      (Dolmen_std.Path.global "seq.len")
      (Expr.Ty.pi [ a ] (Expr.Ty.arrow [ a_s_ty ] Expr.Ty.int))

  let seq_get : Dolmen_std.Expr.term_cst =
    let a = Expr.Ty.Var.mk "alpha" in
    let a_ty = Expr.Ty.of_var a in
    let a_s_ty = seq_ty a_ty in
    Expr.Id.mk ~name:"seq.nth" ~builtin:Seq_get
      (Dolmen_std.Path.global "seq.nth")
      (Expr.Ty.pi [ a ] (Expr.Ty.arrow [ a_s_ty; Expr.Ty.int ] a_ty))

  let seq_set : Dolmen_std.Expr.term_cst =
    let a = Expr.Ty.Var.mk "alpha" in
    let a_ty = Expr.Ty.of_var a in
    let a_s_ty = seq_ty a_ty in
    Expr.Id.mk ~name:"seq.set" ~builtin:Seq_set
      (Dolmen_std.Path.global "seq.set")
      (Expr.Ty.pi [ a ] (Expr.Ty.arrow [ a_s_ty; Expr.Ty.int; a_ty ] a_s_ty))

  let seq_update : Dolmen_std.Expr.term_cst =
    let a = Expr.Ty.Var.mk "alpha" in
    let a_ty = Expr.Ty.of_var a in
    let a_s_ty = seq_ty a_ty in
    Expr.Id.mk ~name:"seq.update" ~builtin:Seq_update
      (Dolmen_std.Path.global "seq.update")
      (Expr.Ty.pi [ a ] (Expr.Ty.arrow [ a_s_ty; Expr.Ty.int; a_s_ty ] a_s_ty))

  let seq_extract : Dolmen_std.Expr.term_cst =
    let a = Expr.Ty.Var.mk "alpha" in
    let a_ty = Expr.Ty.of_var a in
    let a_s_ty = seq_ty a_ty in
    Expr.Id.mk ~name:"seq.extract" ~builtin:Seq_extract
      (Dolmen_std.Path.global "seq.extract")
      (Expr.Ty.pi [ a ]
         (Expr.Ty.arrow [ a_s_ty; Expr.Ty.int; Expr.Ty.int ] a_s_ty))

  let get_ty_arg : Expr.ty -> Expr.ty = function
    (* | {
         ty_descr =
           Pi ([ _ ], { ty_descr = TyApp ({ builtin = NSeq; _ }, [ _ ]); _ });
       } ->
         Expr.Ty.of_var (Expr.Ty.Var.wildcard ()) *)
    | { ty_descr = TyApp ({ builtin = NSeq | Seq; _ }, [ a_ty ]); _ } -> a_ty
    | ty -> raise (NotAnNSeqType ty)

  let seq_concat : int -> Dolmen_std.Expr.term_cst =
    let cache = DInt.H.create 13 in
    let get_sym i =
      match DInt.H.find cache i with
      | res -> res
      | exception Not_found ->
          let a = Expr.Ty.Var.mk "alphaY" in
          let a_ty = Expr.Ty.of_var a in
          let a_ns_ty = seq_ty a_ty in
          let ty = Expr.Ty.arrow (List.init i (fun _ -> a_ns_ty)) a_ns_ty in
          let res =
            Expr.Id.mk ~name:"seq.++" ~builtin:Seq_concat
              (Dolmen_std.Path.global "seq.++")
              (Expr.Ty.pi [ a ] ty)
          in
          DInt.H.add cache i res;
          res
    in
    fun i -> get_sym i

  let add_builtins () =
    let ty_app1 env s f =
      Dolmen_loop.Typer.T.builtin_ty
        (Dolmen_type.Base.ty_app1
           (module Dolmen_loop.Typer.T)
           env s
           (fun ty -> Expr.Ty.apply f [ ty ]))
    in
    let app0 env s f =
      Dolmen_loop.Typer.T.builtin_term
        (Dolmen_type.Base.app0
           (module Dolmen_loop.Typer.T)
           env s
           (Expr.Term.apply_cst f
              [ Expr.Ty.of_var (Expr.Ty.Var.wildcard ()) ]
              []))
    in
    let app1 ?(get_ty_arg = Fun.id) env s f =
      Dolmen_loop.Typer.T.builtin_term
        (Dolmen_type.Base.term_app1
           (module Dolmen_loop.Typer.T)
           env s
           (fun a -> Expr.Term.apply_cst f [ get_ty_arg a.Expr.term_ty ] [ a ]))
    in
    let app2 ?(get_ty_arg = Fun.id) env s f =
      Dolmen_loop.Typer.T.builtin_term
        (Dolmen_type.Base.term_app2
           (module Dolmen_loop.Typer.T)
           env s
           (fun a b ->
             Expr.Term.apply_cst f [ get_ty_arg a.Expr.term_ty ] [ a; b ]))
    in
    let app3 env ?(get_ty_arg = Fun.id) s f =
      Dolmen_loop.Typer.T.builtin_term
        (Dolmen_type.Base.term_app3
           (module Dolmen_loop.Typer.T)
           env s
           (fun a b c ->
             Expr.Term.apply_cst f [ get_ty_arg a.Expr.term_ty ] [ a; b; c ]))
    in
    Expr.add_builtins (fun env s ->
        match s with
        | Dolmen_loop.Typer.T.Id { ns = Sort; name = Simple "NSeq" } ->
            ty_app1 env s nseq_ty_const
        | Dolmen_loop.Typer.T.Id { ns = Term; name = Simple "nseq.get" } ->
            app2 ~get_ty_arg env s nseq_get
        | Dolmen_loop.Typer.T.Id { ns = Term; name = Simple "nseq.set" } ->
            app3 ~get_ty_arg env s nseq_set
        | Dolmen_loop.Typer.T.Id { ns = Term; name = Simple "nseq.first" } ->
            app1 ~get_ty_arg env s nseq_first
        | Dolmen_loop.Typer.T.Id { ns = Term; name = Simple "nseq.last" } ->
            app1 ~get_ty_arg env s nseq_last
        | Dolmen_loop.Typer.T.Id { ns = Term; name = Simple "nseq.content" } ->
            app1 ~get_ty_arg env s nseq_content
        | Dolmen_loop.Typer.T.Id { ns = Term; name = Simple "nseq.length" } ->
            app1 ~get_ty_arg env s nseq_length
        | Dolmen_loop.Typer.T.Id { ns = Term; name = Simple "nseq.const" } ->
            Dolmen_loop.Typer.T.builtin_term
              (Dolmen_type.Base.term_app3
                 (module Dolmen_loop.Typer.T)
                 env s
                 (fun a b c ->
                   Expr.Term.apply_cst nseq_const [ c.Expr.term_ty ] [ a; b; c ]))
        | Dolmen_loop.Typer.T.Id { ns = Term; name = Simple "nseq.concat" } ->
            app2 ~get_ty_arg env s nseq_concat
        | Dolmen_loop.Typer.T.Id { ns = Term; name = Simple "nseq.relocate" } ->
            app2 ~get_ty_arg env s nseq_relocate
        | Dolmen_loop.Typer.T.Id { ns = Term; name = Simple "nseq.slice" } ->
            app3 ~get_ty_arg env s nseq_slice
        | Dolmen_loop.Typer.T.Id { ns = Term; name = Simple "nseq.update" } ->
            app2 ~get_ty_arg env s nseq_update
        (* Seq terms *)
        | Dolmen_loop.Typer.T.Id
            { ns = Attr; name = Simple (":qid" | ":skolemid" | ":weight") } ->
            Dolmen_loop.Typer.T.builtin_tags
              (Dolmen_type.Base.make_op1
                 (module Dolmen_loop.Typer.T)
                 env s
                 (fun _ _ -> []))
        | Dolmen_loop.Typer.T.Id { ns = Sort; name = Simple "Seq" } ->
            ty_app1 env s seq_ty_const
        | Dolmen_loop.Typer.T.Id { ns = Term; name = Simple "seq.empty" } ->
            app0 env s seq_empty
        | Dolmen_loop.Typer.T.Id { ns = Term; name = Simple "seq.unit" } ->
            app1 env s seq_unit
        | Dolmen_loop.Typer.T.Id { ns = Term; name = Simple "seq.len" } ->
            app1 ~get_ty_arg env s seq_length
        | Dolmen_loop.Typer.T.Id { ns = Term; name = Simple "seq.nth" } ->
            app2 ~get_ty_arg env s seq_get
        | Dolmen_loop.Typer.T.Id { ns = Term; name = Simple "seq.update" } ->
            Dolmen_loop.Typer.T.builtin_term
              (Dolmen_type.Base.term_app3
                 (module Dolmen_loop.Typer.T)
                 env s
                 (fun a b c ->
                   try
                     if
                       Expr.Ty.equal
                         (get_ty_arg a.Expr.term_ty)
                         (get_ty_arg c.Expr.term_ty)
                     then
                       match c with
                       | {
                        term_descr =
                          App
                            ( { term_descr = Cst { builtin = Seq_unit } },
                              _,
                              [ v ] );
                       } ->
                           (* TODO: do the same with const(i,i,v)?  *)
                           Expr.Term.apply_cst seq_set
                             [ get_ty_arg a.Expr.term_ty ]
                             [ a; b; v ]
                       | _ ->
                           Expr.Term.apply_cst seq_update
                             [ get_ty_arg a.Expr.term_ty ]
                             [ a; b; c ]
                     else raise Exit
                   with Exit | NotAnNSeqType _ ->
                     Expr.Term.apply_cst seq_set
                       [ get_ty_arg a.Expr.term_ty ]
                       [ a; b; c ]))
        | Dolmen_loop.Typer.T.Id { ns = Term; name = Simple "seq.extract" } ->
            app3 ~get_ty_arg env s seq_extract
        | Dolmen_loop.Typer.T.Id { ns = Term; name = Simple "seq.++" } ->
            Dolmen_loop.Typer.T.builtin_term
              (Dolmen_type.Base.term_app_list
                 (module Dolmen_loop.Typer.T)
                 env s
                 (fun l ->
                   match l with
                   | a :: _ :: _ ->
                       let a_vty = get_ty_arg a.Expr.term_ty in
                       Expr.Term.apply_cst
                         (seq_concat (List.length l))
                         [ a_vty ] l
                   | _ ->
                       failwith
                         "seq.++ expects at least two sequences as arguments"))
        | _ -> `Not_found)
end

let nseq_val_gty = function
  | Ground.Ty.
      { app = { builtin = Builtin.(NSeq | Seq); _ }; args = [ val_gty ] } ->
      val_gty
  | _ -> raise Not_found

let get_nseq_gty env n =
  let sty = Ground.tys env n in
  assert (not (Ground.Ty.S.is_empty sty));
  List.find
    (function
      | Ground.Ty.{ app = { builtin = Builtin.(NSeq | Seq); _ }; _ } -> true
      | _ -> false)
    (List.sort Ground.Ty.compare @@ Ground.Ty.S.elements sty)

(* TODO: make sure that the typing of Seqs and NSeqs is done correctly *)
let is_seq env n =
  let sty = Ground.tys env n in
  assert (not (Ground.Ty.S.is_empty sty));
  List.exists
    (function
      | Ground.Ty.{ app = { builtin = Builtin.(Seq); _ }; _ } -> true
      | _ -> false)
    (Ground.Ty.S.elements sty)

let is_array env n =
  let sty = Ground.tys env n in
  assert (not (Ground.Ty.S.is_empty sty));
  List.exists
    (function
      | Ground.Ty.{ app = { builtin = Expr.Array; _ }; _ } -> true | _ -> false)
    (Ground.Ty.S.elements sty)

let get_nseq_val_gty env n =
  Debug.dprintf2 debug "get_nseq_val_gty %a" Node.pp n;
  nseq_val_gty (get_nseq_gty env n)

let empty_seq_gt env val_gty =
  Ground.index (Ground.apply env Builtin.seq_empty [ val_gty ] IArray.empty)

let empty_seq_nsval val_gty =
  NSeqValue.
    {
      val_gty;
      vals = Value.M.empty;
      lower_bound = RealValue.of_value A.zero;
      upper_bound = RealValue.of_value A.minus_one;
      other = None;
    }

let empty_seq_n val_gty =
  NSeqValue.node @@ NSeqValue.index @@ empty_seq_nsval val_gty

let mk_ite env c c1 c2 = Ground.apply' env Expr.Term.Const.ite [] [ c; c1; c2 ]
let mk_le env l = Ground.apply' env Expr.Term.Const.Int.le [] l
let mk_ge env l = Ground.apply' env Expr.Term.Const.Int.ge [] l
let mk_lt env l = Ground.apply' env Expr.Term.Const.Int.lt [] l
let mk_sub env v1 v2 = Ground.apply' env Expr.Term.Const.Int.sub [] [ v1; v2 ]
let mk_add env a b = Ground.apply' env Expr.Term.Const.Int.add [] [ a; b ]
let mk_mul env a b = Ground.apply' env Expr.Term.Const.Int.mul [] [ a; b ]
let mk_nseq_ty ty = Ground.Ty.apply Builtin.nseq_ty_const ty
let mk_nseq_first env vty n = Ground.apply' env Builtin.nseq_first [ vty ] [ n ]
let mk_nseq_last env vty n = Ground.apply' env Builtin.nseq_last [ vty ] [ n ]

let mk_nseq_length env vty n =
  Ground.apply' env Builtin.nseq_length [ vty ] [ n ]

let mk_nseq_get env vty ns i =
  Ground.apply' env Builtin.nseq_get [ vty ] [ ns; i ]

let mk_nseq_set env vty ns i v =
  Ground.apply' env Builtin.nseq_set [ vty ] [ ns; i; v ]

let mk_nseq_const env vty i j v =
  Ground.apply' env Builtin.nseq_const [ vty ] [ i; j; v ]

let mk_nseq_slice env vty v i j =
  Ground.apply' env Builtin.nseq_slice [ vty ] [ v; i; j ]

let mk_nseq_concat env vty a b =
  Ground.apply' env Builtin.nseq_concat [ vty ] [ a; b ]

let mk_nseq_relocate env vty a b =
  Ground.apply' env Builtin.nseq_relocate [ vty ] [ a; b ]

let mk_in_bounds env n lb ub =
  Boolean._and env [ mk_le env [ lb; n ]; mk_le env [ n; ub ] ]

let mk_eq env a b = Equality.equality env [ a; b ]
let mk_diseq env a b = Equality.disequality env [ a; b ]
let mk_imp env p q = Ground.apply' env Expr.Term.Const.imply [] [ p; q ]
let add_nseq_gty env n val_gty = Ground.add_ty env n (mk_nseq_ty [ val_gty ])

let mk_in_bounds_term i fst lst =
  Expr.Term._and [ Expr.Term.Int.le fst i; Expr.Term.Int.le i lst ]

let mk_nseq_fst_term vty a =
  Expr.Term.apply_cst Builtin.nseq_first [ vty ] [ a ]

let mk_nseq_lst_term vty a = Expr.Term.apply_cst Builtin.nseq_last [ vty ] [ a ]

let mk_nseq_get_term vty a i =
  Expr.Term.apply_cst Builtin.nseq_get [ vty ] [ a; i ]

let mk_nseq_set_term vty a i v =
  Expr.Term.apply_cst Builtin.nseq_set [ vty ] [ a; i; v ]

let rv_of_node env n =
  let open Colibri2_theories_LRA in
  match Egraph.get_value env n with
  | None -> None
  | Some v -> (
      match RealValue.of_nodevalue v with
      | Some v -> Some (RealValue.value v)
      | None -> None)

let set_int_dom env n =
  Colibri2_theories_LRA.Dom_interval.upd_dom env n
    Colibri2_theories_LRA.Dom_interval.D.integers

let is_in_bounds env i fst lst =
  let cond = mk_in_bounds env i fst lst in
  Egraph.register env cond;
  Boolean.is env cond

let is_le env a b =
  let cond = mk_le env [ a; b ] in
  Egraph.register env cond;
  Boolean.is env cond

module Precs = Id.MkIHT (struct
  let name = "PrecsHT"

  include Id
end)

module Succs = Id.MkIHT (struct
  let name = "SuccsHT"

  include Id
end)

let reg_prec_succ env prec succ =
  Debug.dprintf4 debug "reg_prec_succ %a %a" Id.pp prec Id.pp succ;
  let add_prec =
    match Succs.find_opt env prec with
    | None -> true
    | Some succ' when Id.equal succ succ' -> false
    | Some succ' ->
        Egraph.merge env (Id.get env succ) (Id.get env succ');
        false
  in
  let add_succ =
    match Precs.find_opt env succ with
    | None -> true
    | Some prec' when Id.equal prec prec' -> false
    | Some prec' ->
        Egraph.merge env (Id.get env prec) (Id.get env prec');
        false
  in
  if add_prec && add_succ then (
    Succs.set env prec succ;
    Precs.set env succ prec)

let eq_indices_norm_prec_succ env kid rid =
  Debug.dprintf4 debug "eq_indices_norm_prec_succ (%a <- %a)" Id.pp rid Id.pp
    kid;
  let () =
    match Succs.find_opt env rid with
    | None -> ()
    | Some rsucc ->
        Succs.remove env rid;
        let upd_succ =
          match Succs.find_opt env kid with
          | None ->
              Succs.set env kid rsucc;
              true
          | Some ksucc ->
              assert (not (Id.equal rsucc ksucc));
              Precs.remove env rsucc;
              Egraph.merge env (Id.get env rsucc) (Id.get env ksucc);
              false
        in
        if upd_succ then
          Precs.change env rsucc ~f:(function
            | None -> assert false
            | Some rid' ->
                assert (Id.equal rid rid');
                Some kid)
  in
  match Precs.find_opt env rid with
  | None -> ()
  | Some rprec ->
      Precs.remove env rid;
      let upd_prec =
        match Precs.find_opt env kid with
        | None ->
            Precs.set env kid rprec;
            true
        | Some kprec ->
            assert (not (Id.equal rprec kprec));
            Succs.remove env rprec;
            Egraph.merge env (Id.get env rprec) (Id.get env kprec);
            false
      in
      if upd_prec then
        Succs.change env rprec ~f:(function
          | None -> assert false
          | Some rid' ->
              assert (Id.equal rid rid');
              Some kid)

let nsucc env ?id n =
  let id = Opt.get_def (Id.Index.get_id_safe env n) id in
  Debug.dprintf4 debug "nsucc(%a) %a" Node.pp n Id.pp id;
  match Succs.find_opt env id with
  | Some sid ->
      Debug.dprintf6 debug "nsucc(%a) %a -> found %a" Node.pp n Id.pp id Id.pp
        sid;
      (Id.get env sid, sid)
  | None ->
      let nsp, nsc = DP.get_proxy env n in
      let nsc' = A.add nsc A.one in
      let nspoly = P.of_list nsc' [ (nsp, A.one) ] in
      let ns = DP.node_of_polynome env nspoly in
      Egraph.register env ns;
      let sid = Id.Index.get_id_safe env ns in
      Debug.dprintf6 debug "nsucc(%a) %a -> new %a" Node.pp n Id.pp id Id.pp sid;
      reg_prec_succ env id sid;
      (ns, sid)

let nprec env ?id n =
  let id = Opt.get_def (Id.Index.get_id_safe env n) id in
  Debug.dprintf4 debug "nprec(%a) %a" Node.pp n Id.pp id;
  match Precs.find_opt env id with
  | Some pid ->
      Debug.dprintf6 debug "nprec(%a) %a -> found %a" Node.pp n Id.pp id Id.pp
        pid;
      (Id.get env pid, pid)
  | None ->
      let npp, npc = DP.get_proxy env n in
      let npc' = A.sub npc A.one in
      let nppoly = P.of_list npc' [ (npp, A.one) ] in
      let np = DP.node_of_polynome env nppoly in
      Egraph.register env np;
      let pid = Id.Index.get_id_safe env np in
      Debug.dprintf6 debug "nprec(%a) %a -> new %a" Node.pp n Id.pp id Id.pp pid;
      reg_prec_succ env pid id;
      (np, pid)

let split_cond env ?(cons = fun _ -> assert false)
    ?(alt = fun _ -> assert false) ?(none = fun _ -> assert false) cond =
  match Boolean.is env cond with
  | Some true -> cons env
  | Some false -> alt env
  | None -> none env

let new_decision env cond print_cho ?(cons = fun _ -> ()) ?(alt = fun _ -> ())
    decs =
  split_cond env cond ~cons ~alt ~none:(fun env ->
      Choice.register_global env
        {
          print_cho =
            (fun fmt () -> Fmt.pf fmt "%s: (%a)" print_cho Node.pp cond);
          prio = 1;
          key = None;
          choice =
            Choice.(
              fun env ->
                Debug.dprintf3 debug "%s: (%a)" print_cho Node.pp cond;
                match Boolean.is env cond with
                | Some true ->
                    Debug.dprintf2 debug "%a is true" Node.pp cond;
                    DecTodo [ cons ]
                | Some false ->
                    Debug.dprintf2 debug "%a is false" Node.pp cond;
                    DecTodo [ alt ]
                | None -> decs env);
        })

let bool_dec ?(cons = fun _ -> ()) ?(alt = fun _ -> ()) cond =
  Choice.DecTodo
    [
      (fun env ->
        Debug.dprintf2 debug "Bool_decision: chose %a = true" Node.pp cond;
        Boolean.set_true env cond;
        cons env);
      (fun env ->
        Debug.dprintf2 debug "Bool_decision: chose %a = false" Node.pp cond;
        Boolean.set_false env cond;
        alt env);
    ]

let eq_vals env v1 v2 =
  if Disequality.is_disequal env v1 v2 then -1
  else if Egraph.is_equal env v1 v2 then 1
  else 0

let make_disequal env a b =
  let diseq = Equality.equality env [ a; b ] in
  Egraph.register env diseq;
  Boolean.set_false env diseq

let on_multiple_kns env nsid knid k1 k2 =
  Debug.dprintf8 debug "on_multiple_kns %a %a %a %a" (Id.pp_nid env) nsid
    (Id.pp_nid env) knid (Id.pp_nid env) k1 (Id.pp_nid env) k2;
  let k1node = Id.get env k1 in
  let k2node = Id.get env k2 in
  let knnode = Id.get env knid in
  let nsnode = Id.get env nsid in
  if Disequality.is_disequal env k1node k2node then
    Egraph.merge env nsnode knnode
  else
    Choice.register_global env
      {
        print_cho =
          (fun fmt () ->
            Fmt.pf fmt "Decision: on_multiple_kns %a %a %a %a" Node.pp nsnode
              Node.pp knnode Node.pp k1node Node.pp k2node);
        prio = 0;
        key = None;
        choice =
          (fun env ->
            if Egraph.is_equal env k1node k2node then DecNo
            else if Disequality.is_disequal env k1node k2node then (
              Egraph.merge env nsnode knnode;
              DecNo)
            else
              DecTodo
                [
                  (fun env ->
                    Debug.dprintf8 debug
                      "Decision: on_multiple_kns %a %a: (%a = %a) is true"
                      Node.pp nsnode Node.pp knnode Node.pp k1node Node.pp
                      k2node;
                    Egraph.merge env k1node k2node);
                  (fun env ->
                    Debug.dprintf8 debug
                      "Decision: on_multiple_kns: (%a = %a) is false and (%a = \
                       %a)is true "
                      Node.pp k1node Node.pp k2node Node.pp nsnode Node.pp
                      knnode;
                    make_disequal env k1node k2node;
                    Egraph.merge env nsnode knnode);
                ]);
      }

let decide_equality env ?(dec_check = fun _ _ _ -> false) v1 v2 =
  let is_eq = Egraph.is_equal env v1 v2 in
  let is_diseq = dec_check env v1 v2 || Equality.is_disequal env v1 v2 in
  if is_eq || is_diseq then
    Debug.dprintf5 debug
      "register equality decision: %a ? %a = %d (no decision needed)" Node.pp v1
      Node.pp v2
      (if is_eq then 1 else if is_diseq then -1 else 0)
  else
    Debug.dprintf4 debug "register equality decision: %a ? %a" Node.pp v1
      Node.pp v2;
  Choice.register_global env
    {
      print_cho =
        (fun fmt () -> Fmt.pf fmt "decide_equality %a %a" Node.pp v1 Node.pp v2);
      prio = 1;
      key = None;
      choice =
        (fun env ->
          if
            Egraph.is_equal env v1 v2 || dec_check env v1 v2
            || Equality.is_disequal env v1 v2
          then DecNo
          else
            DecTodo
              [
                (fun env ->
                  Debug.dprintf4 debug "decide_equality (%a = %a) is true"
                    Node.pp v1 Node.pp v2;
                  Egraph.merge env v1 v2);
                (fun env ->
                  Debug.dprintf4 debug "decide_equality (%a = %a) is false"
                    Node.pp v1 Node.pp v2;
                  make_disequal env v1 v2);
              ]);
    }

type weq_dec = { dec_count : int; inode : Node.t; jnode : Node.t }
[@@deriving show]
(* TODO: change weq_dec to something that takes into account all the indices in
   the path *)

type ppg_dec = {
  dec_count : int;
  src : Node.t;
  inode : Node.t;
  dst : Node.t;
  v : Node.t;
  curr_node : Node.t;
  next_ind : Node.t;
  ov : Node.t option;
}
[@@deriving show]

type dec = {
  val_eq : (bool * Node.t * Node.t) option;
  weq : weq_dec option;
  ppg1 : ppg_dec option;
  ppg2 : (ppg_dec * ppg_dec) option;
}
[@@deriving show]

type dec_union =
  | VEq of bool * Node.t * Node.t
  (* differentiate between value decisions and array decisions:
     when two arrays are distinct and we have a value decisions between their values on only one index, make those values distinct *)
  | Weq of weq_dec
  | Ppg1 of ppg_dec
  | Ppg2 of ppg_dec * ppg_dec

let mk_deq ?val_eq ?weq ?ppg1 ?ppg2 () =
  assert (
    Option.is_some val_eq || Option.is_some weq || Option.is_some ppg1
    || Option.is_some ppg2);
  { val_eq; weq; ppg1; ppg2 }

let add_val_eq_dec =
  let aux b v1 v2 ({ val_eq } as dec) =
    match val_eq with
    | None -> { dec with val_eq = Some (b, v1, v2) }
    | Some (b', v1', v2') when b = b' ->
        assert (Node.equal v1 v1');
        assert (Node.equal v2 v2');
        dec
    | Some (false, _, _) -> dec
    | Some (true, _, _) -> { dec with val_eq = Some (b, v1, v2) }
  in
  fun id b v1 v2 dm ->
    Id.M.change
      (function
        | None -> Some (mk_deq ~val_eq:(b, v1, v2) ())
        | Some dec -> Some (aux b v1 v2 dec))
      id dm

let add_weq_dec =
  let aux dec_count inode jnode ({ weq } as dec) =
    match weq with
    | None -> { dec with weq = Some { dec_count; inode; jnode } }
    | Some _ -> assert false
  in
  fun id dec_count inode jnode dm ->
    Id.M.change
      (function
        | None -> Some (mk_deq ~weq:{ dec_count; inode; jnode } ())
        | Some dec -> Some (aux dec_count inode jnode dec))
      id dm

let add_ppg1_dec =
  let aux ppg_dec ({ ppg1; ppg2 } as dec) =
    assert (Option.is_none ppg2);
    match ppg1 with
    | None -> { dec with ppg1 = Some ppg_dec }
    | Some _ -> assert false
  in
  fun id ppg_dec dm ->
    Id.M.change
      (function
        | None -> Some (mk_deq ~ppg1:ppg_dec ())
        | Some dec -> Some (aux ppg_dec dec))
      id dm

let add_ppg2_dec =
  let aux ppg_dec1 ppg_dec2 ({ ppg1; ppg2 } as dec) =
    assert (Option.is_none ppg1);
    match ppg2 with
    | None -> { dec with ppg2 = Some (ppg_dec1, ppg_dec2) }
    | Some _ -> assert false
  in
  fun id ppg_dec1 ppg_dec2 dm ->
    Id.M.change
      (function
        | None -> Some (mk_deq ~ppg2:(ppg_dec1, ppg_dec2) ())
        | Some dec -> Some (aux ppg_dec1 ppg_dec2 dec))
      id dm

let choose_decision { val_eq; weq; ppg1; ppg2 } =
  match val_eq with
  | Some (b, v1, v2) -> (1, VEq (b, v1, v2))
  | None -> (
      let acc =
        match weq with
        | None -> []
        | Some ({ dec_count } as wdec) -> [ (dec_count, Weq wdec) ]
      in
      let acc =
        match ppg1 with
        | None -> acc
        | Some ({ dec_count } as ppg) -> (dec_count, Ppg1 ppg) :: acc
      in
      let acc =
        match ppg2 with
        | None -> acc
        | Some (({ dec_count = d1 } as ppg1), ({ dec_count = d2 } as ppg2)) ->
            (* Not totally the right count of decisions *)
            (d1 + d2, Ppg2 (ppg1, ppg2)) :: acc
      in
      match List.sort (fun (d1, _) (d2, _) -> Int.compare d1 d2) acc with
      | [] -> assert false
      | h :: _ -> h)
