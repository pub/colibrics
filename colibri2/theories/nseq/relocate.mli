(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

(* TODO: maybe functorize it? Or at least make sure that you differentiate
   between the relocation graph of terms and the relocations graph of
   variables. In theory the two should not interact, when a var is a =_{reloc}
   of a term, it should be replaced by the relocations of that term. *)
type t =
  | Repr of Id.t Colibri2_theories_LRA.Polynome.M.t  (** y -> c -> b *)
  | NonRepr of { repr : Id.t; dist : Colibri2_theories_LRA.Polynome.t }
      (** x -- delta --> repr x = relocate(repr, first(x) = first(y) + delta) *)

val new_relocate : Egraph.wt -> Id.t -> Node.t -> Id.t -> Node.t -> unit
val eq_nseqs_norm : Egraph.wt -> Id.t -> Id.t -> unit
(* needs to run before the eq_nseqs_norm of known_values *)

val on_lra_repr_change : Egraph.wt -> unit

val register_repr_change_hook :
  Egraph.wt ->
  (Egraph.wt ->
  old_repr:Id.t ->
  repr:Id.t ->
  Colibri2_theories_LRA.Polynome.t ->
  unit) ->
  unit

val get_repr : Egraph.wt -> Id.t -> t
val is_eq_mod_reloc : Egraph.wt -> Id.t -> Id.t -> bool

val eqclass_mod_reloc :
  Egraph.wt -> Id.t -> Id.t Colibri2_theories_LRA.Polynome.M.t

val relocate_ind : Egraph.wt -> Node.t -> Node.t -> Node.t -> Id.t

val reloc_by_delta :
  Egraph.wt -> Node.t -> Colibri2_theories_LRA.Polynome.t -> Node.t * Id.t

val pp_locs : Format.formatter -> Egraph.wt -> unit
val pp_ht : Format.formatter -> _ Egraph.t -> unit
val on_merge : Egraph.wt -> repr:Node.t -> Node.t -> unit

val fold_class :
  Egraph.wt ->
  (Colibri2_theories_LRA.Polynome.t -> Id.t -> 'a -> 'a) ->
  Id.t ->
  'a ->
  'a

val add_dn :
  Egraph.rw Egraph.t ->
  Colibri2_theories_LRA.Polynome.t ->
  ?dnode:Node.t ->
  Node.t ->
  Node.t

val reloc_nsv_by_delta :
  Egraph.wt ->
  (Egraph.wt -> Node.t -> Value.t Interp.Seq.t) ->
  Ground.Ty.t ->
  Colibri2_theories_LRA.Polynome.t ->
  ?dnode:Node.t ->
  Common.NSeqValue.nsval ->
  Node.t ->
  Node.t ->
  Common.NSeqValue.nsval Interp.Seq.t
