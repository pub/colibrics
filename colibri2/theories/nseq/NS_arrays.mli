(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

val iter_selects :
  f:(Id.t -> (Node.t * bool) Id.M.t -> unit) -> 'a Egraph.t -> unit

val get_selects : _ Egraph.t -> Id.t -> (Node.t * bool) Id.M.t option
val get_selects_ind : _ Egraph.t -> Id.t -> (Node.t * bool) Id.M.t option
val get_val : _ Egraph.t -> Id.t -> Id.t -> Node.t option
val has_distinct_val : Egraph.wt -> Id.t -> Id.t -> bool
val are_holding_dist_vals : Egraph.wt -> Id.t -> Id.t -> bool

val dec_on_val_for_ppg :
  Egraph.wt -> Node.t -> Node.t -> Node.t -> Node.t -> Node.t -> unit

val get_and_explore_paths :
  Egraph.wt ->
  ?paths:UWEGraph.path list ->
  ?ex_arrs:Id.S.t ->
  Node.t ->
  Id.t ->
  Id.S.t ->
  Node.t ->
  Id.t ->
  Node.t ->
  bool
  * (int * (Id.t * Node.t * Id.t * Node.t) list * Node.t * Node.t * Node.t) list

val ppg_over_indices_decision :
  Egraph.wt -> Node.t -> Node.t -> Node.t -> Node.t -> Node.t -> unit

val ppg_over_indices :
  Egraph.wt ->
  ?paths:UWEGraph.path list ->
  ?decide:bool ->
  Node.t ->
  Id.t ->
  Node.t ->
  Id.t ->
  Node.t ->
  Id.t ->
  Node.t ->
  bool * (int * UWEGraph.path * Node.t * Node.t * Node.t) list

val converter : Egraph.wt -> Ground.t -> unit
val converter_no_ppg : Egraph.wt -> Ground.t -> unit

val get_weg_val :
  Egraph.wt -> ?ov:Node.t -> Id.t -> Id.t -> Common.ppg_dec list * Node.t option

val is_weq_mod_i_v1 :
  Egraph.wt -> Id.t -> Id.t -> Id.t -> int * (int * Node.t) option

val iter_arrays :
  Egraph.wt -> (Egraph.wt -> Ground.Ty.t -> Id.S.t -> unit) -> unit

val fold_arrays :
  'a Egraph.t -> ('a Egraph.t -> Ground.Ty.t -> Id.S.t -> 'b -> 'b) -> 'b -> 'b

val pp : Format.formatter -> _ Egraph.t -> unit
val pp_inds : Format.formatter -> _ Egraph.t -> unit
val on_new_graph_link : Egraph.wt -> Id.S.t -> Id.S.t -> unit
val eq_indices_norm : Egraph.wt -> Id.t -> Id.t -> unit
val eq_ns_arrays_norm : Egraph.wt -> Id.t -> Id.t -> unit
