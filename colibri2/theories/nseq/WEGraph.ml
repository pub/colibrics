(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

module DInt = Colibri2_popop_lib.Popop_stdlib.DInt
module P = Colibri2_theories_LRA.Polynome

(* a = b[i <- v]                : a[i]=v -- i --> b   <=>
   b = a[i <- b[i]] & a[i]=v    : b -- i --> a[i]=v
*)

let debug =
  Debug.register_info_flag ~desc:"Debugging messages of the n-sequence theory"
    "NSeq-weg"

(* a -> i -> b set *)
module Out = Id.MkIHT (struct
  type t = Id.S.t Id.M.t

  let pp = Id.M.pp Id.S.pp
  let name = "WEG.outgoing_edges"
end)

(* b -> a -> i *)
module In = Id.MkIHT (struct
  type t = Id.t Id.M.t

  let pp = Id.M.pp Id.pp
  let name = "WEG.incoming_edges"
end)

(* i -> (a -> occurrences) set *)
module Locs = Id.MkIHT (struct
  type t = DInt.t Id.M.t

  let pp = Id.M.pp DInt.pp
  let name = "WEG.index_locs"
end)

type r = Repr | NonRepr of Id.t
(* TODO: add counter to Repr, when changing representatives, choose to change
   the representative with the least nonreprs *)

let pp_r fmt = function
  | Repr -> Fmt.pf fmt "Repr"
  | NonRepr id -> Fmt.pf fmt "NonRepr(%a)" Id.pp id

module Class = Id.MkIHT (struct
  type t = r

  let pp = pp_r
  let name = "WEG.representatives"
end)

let new_link_hook : (Egraph.wt -> Id.t -> Id.t -> unit) Datastructure.Push.t =
  Datastructure.Push.create Fmt.nop "NSeq.WEG.new_link_hook"

let register_new_link_hook d f = Datastructure.Push.push new_link_hook d f

let on_new_link env id1 id2 =
  Datastructure.Push.iter new_link_hook env ~f:(fun f -> f env id1 id2)

(* let pp env =
   Debug.dprintf8 debug "%a\n%a\n%a\n%a\n" Out.pp env In.pp env Locs.pp env
     Class.pp env *)

let add_out env src i dst =
  Debug.dprintf6 debug "add_out %a %a %a" Id.pp src Id.pp i Id.pp dst;
  Out.change env src ~f:(function
    | None -> Some (Id.M.singleton i (Id.S.singleton dst))
    | Some m ->
        Some
          (Id.M.change
             (function
               | None -> Some (Id.S.singleton dst)
               | Some s -> Some (Id.S.add dst s))
             i m))

let remove_out env src i dst =
  Debug.dprintf6 debug "remove_out %a %a %a" Id.pp src Id.pp i Id.pp dst;
  match Out.find_opt env src with
  | None -> assert false
  | Some m ->
      let m' =
        match Id.M.find_opt i m with
        | None -> assert false
        | Some s ->
            let s' = Id.S.remove dst s in
            if Id.S.is_empty s' then Id.M.remove i m else Id.M.add i s' m
      in
      if Id.M.is_empty m' then Out.remove env src else Out.set env src m'

(* Out.change env src ~f:(function
   | None -> assert false
   | Some m ->
       let m' =
         Id.M.change
           (function
             | None -> assert false
             | Some s ->
                 let s' = Id.S.remove dst s in
                 if Id.S.is_empty s' then None else Some s')
           i m
       in
       if Id.M.is_empty m' then None else Some m') *)

let add_in env dst i src =
  Debug.dprintf6 debug "add_in %a %a %a" Id.pp src Id.pp i Id.pp dst;
  match In.find_opt env dst with
  | None ->
      In.set env dst (Id.M.singleton src i);
      true
  | Some m -> (
      match Id.M.find_opt src m with
      | None ->
          In.set env dst (Id.M.add src i m);
          true
      | Some i' when Id.equal i i' -> false
      | Some i' ->
          Common.on_multiple_kns env src dst i i';
          false)

let remove_in env dst i src =
  Debug.dprintf6 debug "remove_in %a %a %a" Id.pp dst Id.pp i Id.pp src;
  In.change env dst ~f:(function
    | None -> assert false
    | Some m ->
        let m' =
          match Id.M.find_remove src m with
          | _, None -> assert false
          | m', Some i' ->
              assert (Id.equal i i');
              m'
        in
        if Id.M.is_empty m' then None else Some m')

let add_loc_aux n m =
  Id.M.change (function None -> Some 1 | Some n -> Some (n + 1)) n m

let add_loc env i n =
  Debug.dprintf4 debug "WEGraph.add_loc %a %a" Id.pp i Id.pp n;
  Locs.change env i ~f:(function
    | None -> Some (Id.M.singleton n 1)
    | Some m -> Some (add_loc_aux n m))

let remove_loc_aux n m =
  Id.M.change
    (function
      | None -> assert false
      | Some n ->
          let n' = n - 1 in
          if n' = 0 then None else Some n')
    n m

let remove_loc env i n =
  Debug.dprintf4 debug "WEGraph.remove_loc %a %a" Id.pp i Id.pp n;
  Locs.change env i ~f:(function
    | None -> assert false
    | Some m ->
        let m' = remove_loc_aux n m in
        if Id.M.is_empty m' then None else Some m')

let subst_loc env i n1 n2 =
  Debug.dprintf6 debug "WEGraph.subst_loc %a (%a <- %a)" Id.pp i Id.pp n1 Id.pp
    n2;
  Locs.change env i ~f:(function
    | None -> assert false
    | Some m ->
        let m = remove_loc_aux n1 m in
        let m = add_loc_aux n2 m in
        Some m)

let add_edge env src i dst =
  Debug.dprintf6 debug "WEGraph.add_edge %a -- %a --> %a" Id.pp src Id.pp i
    Id.pp dst;
  let added = add_in env dst i src in
  if added then (
    add_out env src i dst;
    add_loc env i src)

let remove_edge env src i dst =
  Debug.dprintf6 debug "WEGraph.remove_edge %a -- %a --> %a" Id.pp src Id.pp i
    Id.pp dst;
  remove_out env src i dst;
  remove_in env dst i src;
  remove_loc env i src

let repr c id = match c with Repr -> id | NonRepr rid -> rid

let change_repr_incoming =
  let rec aux env ?(seen = Id.S.empty) ~new_repr dst =
    Debug.dprintf4 debug "WEGraph.change_repr_incoming ~new_repr:%a %a" Id.pp
      new_repr Id.pp dst;
    match In.find_opt env dst with
    | None -> seen
    | Some m ->
        Id.M.fold
          (fun src _ seen ->
            if not (Id.S.mem src seen) then (
              let seen = Id.S.add src seen in
              Class.set env src (NonRepr new_repr);
              aux env ~seen ~new_repr src)
            else seen)
          m seen
  in
  fun env ~new_repr dst -> ignore (aux env ~new_repr dst)

(* src -- i --> dst ::> dst -- i --> src *)
(* TODO: improve *)
let rev_edge env src i dst =
  Debug.dprintf6 debug "WEGraph.rev_edge %a <- %a -- %a" Id.pp src Id.pp i Id.pp
    dst;
  remove_out env src i dst;
  remove_in env dst i src;
  let added = add_in env src i dst in
  if added then (
    add_out env dst i src;
    subst_loc env i src dst)
  else remove_loc env i src

let rev_outgoing_edges =
  let rec aux env ?(seen = Id.S.empty) ~new_repr src =
    Debug.dprintf4 debug "WEGraph.rev_outgoing_edges ~new_repr:%a %a" Id.pp
      new_repr Id.pp src;
    let seen = Id.S.add src seen in
    match Out.find_opt env src with
    | None ->
        change_repr_incoming env ~new_repr src;
        seen
    | Some m ->
        Id.M.fold
          (fun i s seen ->
            Id.S.fold
              (fun dst seen ->
                if Id.S.mem dst seen then seen
                else
                  let seen = aux env ~seen ~new_repr dst in
                  Class.set env dst (NonRepr new_repr);
                  rev_edge env src i dst;
                  seen)
              s seen)
          m seen
  in
  fun env ~new_repr src -> ignore (aux env ~new_repr src)

let edge_exists env a b i =
  match Out.find_opt env a with
  | None -> false
  | Some m -> (
      match Id.M.find_opt i m with None -> false | Some s -> Id.M.mem b s)

let subst_out env kid rid =
  Debug.dprintf4 debug "WEGraph.subst_out %a <- %a" Id.pp rid Id.pp kid;
  let () =
    match In.find_opt env kid with
    | None -> ()
    | Some km -> (
        match Id.M.find_remove rid km with
        | _, None -> ()
        | _, Some i -> remove_edge env rid i kid)
  in
  match Out.find_opt env rid with
  | None -> ()
  | Some rm ->
      Id.M.iter
        (fun i dsts ->
          Id.S.iter
            (fun dst ->
              remove_in env dst i rid;
              let added = add_in env dst i kid in
              if added then (
                subst_loc env i rid kid;
                add_out env kid i dst)
              else remove_loc env i rid)
            dsts)
        rm

let subst_in env kid rid =
  Debug.dprintf4 debug "WEGraph.subst_in %a <- %a" Id.pp rid Id.pp kid;
  match In.find_opt env rid with
  | None -> ()
  | Some rm ->
      let rm =
        match Id.M.find_remove kid rm with
        | _, None -> rm
        | rm', Some i ->
            remove_edge env kid i rid;
            rm'
      in
      Id.M.iter
        (fun src i ->
          remove_out env src i rid;
          let added = add_in env kid i src in
          if added then add_out env src i kid else remove_loc env i src)
        rm

let remove_all_out env rid =
  Debug.dprintf2 debug "WEGraph.remove_all_out %a" Id.pp rid;
  match Out.find_opt env rid with
  | None -> ()
  | Some rm ->
      Id.M.iter
        (fun i dsts ->
          Id.S.iter
            (fun dst ->
              remove_in env dst i rid;
              remove_loc env i rid)
            dsts)
        rm;
      Out.remove env rid

let new_set env a b i =
  Debug.dprintf6 debug "WEGraph.new_set %a = %a[%a <- _]" Id.pp a Id.pp b Id.pp
    i;
  if Id.equal a b || edge_exists env a b i || edge_exists env b a i then ()
  else
    let src, dst =
      match (Class.find_opt env a, Class.find_opt env b) with
      | None, None ->
          Class.set env a (NonRepr b);
          Class.set env b Repr;
          (a, b)
      | None, Some c ->
          let br = repr c b in
          Class.set env a (NonRepr br);
          (a, b)
      | Some c, None ->
          let ar = repr c a in
          Class.set env b (NonRepr ar);
          (b, a)
      | Some (NonRepr ar), Some (NonRepr br) when ar = br -> (a, b)
      | Some Repr, Some Repr ->
          Class.set env a (NonRepr b);
          change_repr_incoming env ~new_repr:b a;
          (a, b)
      | Some Repr, Some (NonRepr br) when Id.equal br a -> (b, a)
      | Some (NonRepr ar), Some Repr when Id.equal ar b -> (a, b)
      | Some Repr, Some (NonRepr br) ->
          Class.set env br (NonRepr a);
          change_repr_incoming env ~new_repr:a b;
          rev_outgoing_edges env ~new_repr:a b;
          (* subst_in env a br;*)
          (b, a)
      | Some (NonRepr ar), Some Repr ->
          Class.set env ar (NonRepr b);
          change_repr_incoming env ~new_repr:b a;
          rev_outgoing_edges env ~new_repr:b a;
          (* subst_in env b ar; *)
          (a, b)
      | Some (NonRepr ar), Some (NonRepr br) ->
          Class.set env ar (NonRepr br);
          change_repr_incoming env ~new_repr:br a;
          rev_outgoing_edges env ~new_repr:br a;
          (a, b)
    in
    add_edge env src i dst

let eq_indices_norm env kid rid =
  Debug.dprintf4 debug "WEGraph.eq_indices_norm %a <- %a" Id.pp rid Id.pp kid;
  match Locs.find_opt env rid with
  | None -> ()
  | Some lm ->
      Locs.remove env rid;
      Locs.change env kid ~f:(function
        | None -> Some lm
        | Some lm' -> Some (Id.M.union (fun _ n1 n2 -> Some (n1 + n2)) lm lm'));
      Id.M.iter
        (fun src _ ->
          Out.change env src ~f:(function
            | None -> assert false
            | Some m ->
                let m' =
                  match Id.M.find_remove rid m with
                  | _, None -> assert false
                  | m', Some dsts ->
                      Id.S.iter
                        (fun dst ->
                          In.change env dst ~f:(function
                            | None -> assert false
                            | Some m ->
                                let m' =
                                  match Id.M.find_remove src m with
                                  | _, None -> assert false
                                  | m', Some i ->
                                      assert (Id.equal i rid);
                                      Id.M.add src kid m'
                                in
                                Some m'))
                        dsts;
                      Id.M.change
                        (function
                          | None -> Some dsts
                          | Some dsts' -> Some (Id.S.union dsts dsts'))
                        kid m'
                in
                Some m'))
        lm

let eq_nseqs_norm env kid rid =
  Debug.dprintf4 debug "WEGraph.eq_nseqs_norm %a <- %a" Id.pp rid Id.pp kid;
  let () =
    match (Class.find_opt env kid, Class.find_opt env rid) with
    | None, None | Some _, None -> ()
    | None, Some Repr ->
        Class.set env kid Repr;
        change_repr_incoming env ~new_repr:kid rid;
        subst_in env kid rid
    | None, Some (NonRepr rr) ->
        Class.set env kid (NonRepr rr);
        subst_in env kid rid;
        subst_out env kid rid
    | Some (NonRepr kr), Some (NonRepr rr) when Id.equal kr rr ->
        (* TODO: avoid cycles by checking paths to the representative? *)
        subst_in env kid rid;
        subst_out env kid rid
    | Some Repr, Some Repr ->
        change_repr_incoming env ~new_repr:kid rid;
        subst_in env kid rid
    | Some Repr, Some (NonRepr br) when Id.equal br kid ->
        subst_in env kid rid;
        remove_all_out env rid
    | Some (NonRepr kr), Some Repr when Id.equal kr rid ->
        Class.set env kid Repr;
        remove_all_out env kid;
        change_repr_incoming env ~new_repr:kid kid;
        change_repr_incoming env ~new_repr:kid rid;
        subst_in env kid rid
    | Some Repr, Some (NonRepr _) ->
        change_repr_incoming env ~new_repr:kid rid;
        rev_outgoing_edges env ~new_repr:kid rid;
        subst_in env kid rid;
        remove_all_out env rid
    | Some (NonRepr ar), Some Repr ->
        change_repr_incoming env ~new_repr:ar rid;
        subst_in env kid rid
    | Some (NonRepr ar), Some (NonRepr _) ->
        change_repr_incoming env ~new_repr:ar rid;
        rev_outgoing_edges env ~new_repr:ar rid;
        subst_in env kid rid;
        remove_all_out env rid
  in
  Out.remove env rid;
  In.remove env rid;
  Class.remove env rid

let shortest_paths =
  let rec aux env src dst path seen =
    Debug.dprintf8 debug "shortest_paths_aux %a %a {%a} {%a}" Id.pp src Id.pp
      dst Id.S.pp path Id.S.pp seen;
    if Id.equal src dst then [ path ]
    else
      let seen = Id.S.add src seen in
      match Out.find_opt env src with
      | None -> []
      | Some m ->
          let paths =
            Id.M.fold
              (fun k dsts' acc ->
                let path = Id.S.add k path in
                Id.S.fold
                  (fun dst' acc ->
                    if Id.S.mem dst' seen then acc
                    else
                      let new_paths = aux env dst' dst path seen in
                      new_paths @ acc)
                  dsts' acc)
              m []
          in
          paths
  in
  fun env src dst ->
    let l = aux env src dst Id.S.empty Id.S.empty in
    Debug.dprintf6 debug "WEGraph.shortest_path %a %a: [%a]" Id.pp src Id.pp dst
      (Fmt.list ~sep:Fmt.comma Id.S.pp)
      l;
    l

let filter_paths l =
  let rec aux acc l =
    match l with
    | [] -> acc
    | h :: t ->
        if
          Id.S.is_empty h
          || List.exists (fun s -> Id.S.subset s h) acc
          || List.exists (fun s -> Id.S.subset s h) t
        then aux acc t
        else aux (h :: acc) t
  in
  aux [] l

let get_path_aux env id1 id2 =
  Debug.dprintf4 debug "WEGraph.get_path_aux %a %a" Id.pp id1 Id.pp id2;
  let paths =
    match (Class.find_opt env id1, Class.find_opt env id2) with
    | Some Repr, Some Repr | None, _ | _, None -> []
    | Some Repr, Some (NonRepr rid2) when not (Id.equal rid2 id1) -> []
    | Some (NonRepr rid1), Some Repr when not (Id.equal rid1 id2) -> []
    | Some (NonRepr rid1), Some (NonRepr rid2) when not (Id.equal rid1 rid2) ->
        []
    | Some (NonRepr _), Some Repr -> shortest_paths env id1 id2
    | Some Repr, Some (NonRepr _) -> shortest_paths env id2 id1
    | Some (NonRepr rid1), Some (NonRepr rid2) ->
        assert (Id.equal rid1 rid2);
        let paths = shortest_paths env id1 id2 in
        (* Computing paths to the repr is only necessary when the shortest
           path between id1 and id2 is different from the union of their
           shortest paths to the repr, or when there is no shortest path
           between them without the repr, it should be computed only when
           necessary *)
        let paths1 = shortest_paths env id1 rid1 in
        let paths2 = shortest_paths env id2 rid1 in
        let paths =
          List.fold_left
            (fun acc h ->
              let l = List.map (fun s -> Id.S.union h s) paths2 in
              l @ acc)
            paths paths1
        in
        let paths = filter_paths paths in
        paths
  in
  Debug.dprintf6 debug "WEGraph.get_path_aux %a %a: [%a]" Id.pp id1 Id.pp id2
    (Fmt.list ~sep:Fmt.comma Id.S.pp)
    paths;
  paths

let get_repr env id =
  match Class.find_opt env id with
  | None | Some Repr -> id
  | Some (NonRepr r) -> r

let get_paths env id1 id2 =
  let paths = get_path_aux env id1 id2 in
  if not (paths = []) then paths
  else
    let reloc_c1 = Relocate.eqclass_mod_reloc env id1 in
    let reloc_c2 = Relocate.eqclass_mod_reloc env id2 in
    let m = P.M.inter (fun _ a b -> Some (a, b)) reloc_c1 reloc_c2 in
    Debug.dprintf6 debug "reloc class union {%a} {%a} = {%a}" (P.M.pp Id.pp)
      reloc_c1 (P.M.pp Id.pp) reloc_c2 (P.M.pp Id.IdP.pp) m;
    let paths =
      P.M.fold
        (fun negdist (id1, id2) acc ->
          let dist = P.neg negdist in
          let paths = get_path_aux env id1 id2 in
          let paths =
            List.map
              (fun s ->
                Id.S.fold
                  (fun k acc ->
                    let knode = Id.get env k in
                    let _, k' = Relocate.reloc_by_delta env knode dist in
                    Id.S.add k' acc)
                  s Id.S.empty)
              paths
          in
          paths @ acc)
        m []
    in
    let paths = filter_paths paths in
    paths

let get_links env id =
  let m1 = match Out.find_opt env id with None -> Id.M.empty | Some m -> m in
  let m2 = match In.find_opt env id with None -> Id.M.empty | Some m -> m in
  Id.M.fold
    (fun a i acc ->
      Id.M.change
        (function
          | None -> Some (Id.S.singleton a) | Some s -> Some (Id.S.add a s))
        i acc)
    m2 m1

let are_linked env id1 id2 =
  match (Class.find_opt env id1, Class.find_opt env id2) with
  | Some (NonRepr r1), Some (NonRepr r2) when Id.equal r1 r2 -> true
  | Some (NonRepr r1), Some Repr when Id.equal r1 id2 -> true
  | Some Repr, Some (NonRepr r2) when Id.equal id1 r2 -> true
  | _ -> false

type path = PStep of Id.t * Id.S.t * path | PSplit of path list | PEnd

let add_step i aset paths =
  match paths with
  | [] -> assert false
  | [ h ] -> PStep (i, aset, h)
  | _ -> PStep (i, aset, PSplit paths)

let get_undirected_path =
  let rec aux env id i_ex targets seen =
    if Id.S.mem id targets then ([ PEnd ], seen)
    else
      let links = get_links env id in
      Id.M.fold
        (fun i aset (acc, seen) ->
          if Id.equal i i_ex then (acc, seen)
          else
            let paths, nseen =
              Id.S.fold
                (fun a (acc, seen) ->
                  if Id.S.mem a seen then (acc, seen)
                  else
                    let nseen = Id.S.add a seen in
                    let nacc, nseen = aux env a i_ex targets nseen in
                    (nacc @ acc, nseen))
                aset (acc, seen)
            in
            match paths with
            | [] -> (acc, nseen)
            | _ ->
                let path = add_step i aset paths in
                (path :: acc, nseen))
        links ([], seen)
  in
  fun env id i_ex targets ->
    if Id.S.is_empty targets then None
    else
      match fst (aux env id i_ex targets (Id.S.singleton id)) with
      | [] -> None
      | [ p ] -> Some p
      | paths -> Some (PSplit paths)

let pp_ht = Out.pp
let pp_class = Class.pp
