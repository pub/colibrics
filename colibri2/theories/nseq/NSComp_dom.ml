(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_core
open Colibri2_popop_lib
open Common
open NSComp_info
module Pattern = Colibri2_theories_quantifiers.Pattern
module InvertedPath = Colibri2_theories_quantifiers.InvertedPath
module RealValue = Colibri2_theories_LRA.RealValue
module P = Colibri2_theories_LRA.Polynome

module NNSCLocs = struct
  (* TODO: currently he switch back and forth from nodes to ids, which is not
      the most efficient thing to do, it should be improved
     - Maybe use two hash tables?
       - nseq ids -> nsc ids
       - nsc ids -> nseq loc ids *)
  module HT = Id.MkIHT (struct
    type t = Node.S.t [@@deriving show]

    let name = "NNSCLocs"
  end)

  let eq_nseqs_norm env kid rid =
    match HT.find_opt env rid with
    | None -> ()
    | Some s ->
        HT.remove env rid;
        HT.change
          ~f:(function None -> Some s | Some s' -> Some (Node.S.union s s'))
          env kid

  let rm_id env id =
    Debug.dprintf2 debug "NNSCLocs.rm_id %a" (Id.pp_nid env) id;
    HT.remove env id

  let get_locs env id =
    match HT.find_opt env id with
    | None ->
        Debug.dprintf2 debug "NNSCLocs.get_locs %a {empty}" (Id.pp_nid env) id;
        Node.S.empty
    | Some s ->
        let ids = Id.S.singleton id in
        let _, s' =
          Node.S.fold
            (fun n (ids, acc) ->
              let nid = Id.NSeq.get_id env n in
              if Id.S.mem nid ids then (ids, acc)
              else (ids, Node.S.add (Egraph.find env n) acc))
            s (ids, Node.S.empty)
        in
        Debug.dprintf4 debug "NNSCLocs.get_locs %a {%a}" (Id.pp_nid env) id
          (fun fmt s -> Node.S.iter (Id.NSeq.pp env fmt) s)
          s';
        s'

  let add_loc env id n =
    Debug.dprintf4 debug "NNSCLocs.add_loc %a %a" (Id.pp_nid env) id
      (Id.NSeq.pp env) n;
    if not (Id.equal id (Id.NSeq.get_id env n)) then
      HT.change env id ~f:(function
        | None -> Some (Node.S.singleton n)
        | Some s -> Some (Node.S.add n s))

  let rm_loc env id n =
    Debug.dprintf4 debug "NNSCLocs.rm_loc %a %a" (Id.pp_nid env) id
      (Id.NSeq.pp env) n;
    let nid = Id.NSeq.get_id env n in
    if not (Id.equal nid id) then
      (* TODO: real solution to avoid having NSeqs as their own components *)
      HT.change env id ~f:(function
        | None -> assert false
        | Some s ->
            let nid = Id.NSeq.get_id env n in
            let ids =
              Node.S.fold
                (fun n acc -> Id.S.add (Id.NSeq.get_id env n) acc)
                s Id.S.empty
            in
            assert (
              if Id.S.mem nid ids then true
              else (
                Debug.dprintf6 debug ">>> %a :{%a} - %a@." (Id.pp_nid env) id
                  Id.S.pp ids (Id.NSeq.pp env) n;
                false));
            let ids' = Id.S.remove nid ids in
            Some
              (Id.S.fold
                 (fun id acc -> Node.S.add (Id.get env id) acc)
                 ids' Node.S.empty))
end

let get_n_cdom_locs = NNSCLocs.get_locs

module NSThE = struct
  type t = { node : Node.t; ty : Ground.Ty.t } [@@deriving show, eq, ord, hash]
end

module ThE = ThTerm.Register (struct
  include NSThE
  include Colibri2_popop_lib.Popop_stdlib.MkDatatype (NSThE)

  let name = "NSeqThE"
  let ty { ty } = ty
end)

let fresh_var_id env ?val_gty n f l =
  Debug.dprintf6 debug "fresh_var_id [%a;%a] of %a" (Id.Index.pp env) f
    (Id.Index.pp env) l (Id.NSeq.pp env) n;
  let ty = Opt.get_def (get_nseq_gty env n) val_gty in
  let node = mk_nseq_slice env ty n f l in
  let thn = ThE.node (ThE.index { node; ty }) in
  Egraph.register env thn;
  let nsid = NSeq_dom.new_nseq env thn in
  Bounds.upd_bounds_id env nsid f l;
  thn

let relocate_node env ?val_gty n fst lst =
  let val_gty =
    match val_gty with Some vgty -> vgty | None -> get_nseq_val_gty env n
  in
  let rel_a = mk_nseq_relocate env val_gty n fst in
  Egraph.register env rel_a;
  Bounds.upd_bounds env rel_a fst lst;
  add_nseq_gty env rel_a val_gty;
  rel_a

let relocate_node_wbounds env ?val_gty n fst fstid lst lstid dist =
  if P.is_zero dist then (n, fst, fstid, lst, lstid)
  else
    let fst', fstid' = Relocate.reloc_by_delta env fst dist in
    let lst', lstid' = Relocate.reloc_by_delta env lst dist in
    let n' = relocate_node env ?val_gty n fst' lst' in
    (n', fst', fstid', lst', lstid')

(* TODO: add a "remove" option, which will remove the nsc var if it is set.
   - when an atom is relocated, check if another atom already exists at that
     position, if that is the case, use that one instead of relocating the new
     one. *)
let relocate_atom env ?val_gty a afst nfst nlst =
  let fst, lst, ares =
    if Egraph.is_equal env afst nfst then
      (* TODO: should be unreachable, but can be because of delayed normlization
         in Relocate due to delayed substitutions *)
      (Id.Index.get_id_safe env nfst, Id.Index.get_id_safe env nlst, a)
    else
      let rel_a = relocate_node env ?val_gty a nfst nlst in
      (Id.Index.get_id_safe env nfst, Id.Index.get_id_safe env nlst, rel_a)
  in
  Debug.dprintf10 debug "relocate_atom %a[%a; _] = %a[%a; %a]" NSC.pp_atom a
    (Id.Index.pp env) afst NSC.pp_atom ares (Id.pp_nid env) fst (Id.pp_nid env)
    lst;
  (fst, lst, ares)

let id_sm_union m1 m2 =
  Id.M.union (fun _ ids1 ids2 -> Some (Id.S.union ids1 ids2)) m1 m2

module rec ConcatDom : sig
  type t = NSC.t

  val pp_ht : Format.formatter -> _ Egraph.t -> unit
  val rm_cdom : Egraph.wt -> Id.t -> t -> unit
  val get_cdom : Egraph.wt -> Id.t -> t option
  val set_cdom : Egraph.wt -> Id.t -> t -> unit
  val upd_cdom : Egraph.wt -> ?upd_loc:bool -> Id.t -> t -> unit

  val upd_dom' :
    Egraph.wt -> Node.t -> ?next:Node.t -> (NSC.atom * Id.t * Id.t) list -> unit

  val register_on_first_set_cdom_hook :
    Egraph.wt -> (Egraph.wt -> Id.t -> t -> unit) -> unit

  val register_on_set_cdom_hook :
    Egraph.wt -> (Egraph.wt -> Id.t -> t -> unit) -> unit

  val subst_n_dom : Egraph.wt -> Id.t -> t -> unit
  val apply_splits : Egraph.wt -> Id.IdT.S.t Node.M.t -> unit
  val add_split : ?splits:Id.IdT.S.t -> Id.t -> Id.t -> Id.t -> Id.IdT.S.t

  val splits_union :
    Id.IdT.S.t Node.M.t -> Id.IdT.S.t Node.M.t -> Id.IdT.S.t Node.M.t

  val add_split_acc :
    Node.t -> Id.IdT.S.t -> Id.IdT.S.t Node.M.t -> Id.IdT.S.t Node.M.t

  val match_atom_pos_ret :
    Egraph.rw Egraph.t ->
    ?upd_loc:bool ->
    Node.t ->
    Node.t ->
    Node.t ->
    NSC.atom_pos ->
    ?nsb:Node.t ->
    NSC.atom_pos ->
    NSC.atom_pos

  val update : 'a Id.M.t Id.M.t -> Id.t -> Id.t -> 'a -> 'a Id.M.t Id.M.t

  val apply_nth :
    Egraph.wt ->
    (Egraph.wt -> NSC.atom -> Id.t -> Id.t -> ?next:Node.t -> 'a -> 'a) ->
    Node.t ->
    t ->
    Node.t ->
    'a ->
    'a

  val relocate_cc :
    Egraph.wt -> ?upd_locs:bool -> ?old_repr:Id.t -> Id.t -> t -> P.t -> NSC.t
end = struct
  include NSC

  module HT = Id.MkIHT (struct
    type nonrec t = t

    let pp = pp ?env:None
    let name = "ConcatDomHT"
  end)

  let set_cdom_ht env id c =
    Debug.dprintf4 debug "ConcatDom.set_cdom_ht %a: {%a}" Id.pp id (pp ~env) c;
    HT.set env id c

  let pp_ht = HT.pp

  let rm_cdom env id c =
    Debug.dprintf4 debug "ConcatDom.rm_cdom %a {%a}" Id.pp id (pp ~env) c;
    HT.remove env id

  let get_cdom = HT.find_opt

  let add_split ?(splits = Id.IdT.S.empty) fst lst1 lst2 =
    let lst1, lst2 =
      if Id.compare lst1 lst2 >= 0 then (lst1, lst2) else (lst2, lst1)
    in
    Id.IdT.S.add (fst, lst1, lst2) splits

  let add_split_acc ns splits splits_acc =
    Node.M.change
      (function
        | None -> Some splits | Some s -> Some (Id.IdT.S.union s splits))
      ns splits_acc

  let splits_union splits1 splits2 =
    Node.M.union (fun _ s1 s2 -> Some (Id.IdT.S.union s1 s2)) splits1 splits2

  let match_atom_pos_ret env ?(upd_loc = true) fst lst nsa { a; next = na } ?nsb
      { a = b; next = nb } =
    Debug.dprintf10 debug "match_atom_pos %a [%a; %a] (%a) (%a)" Node.pp nsa
      Node.pp fst Node.pp lst (pp_atom_pos ~env) { a; next = na }
      (pp_atom_pos ~env) { a = b; next = nb };
    let resa =
      if not (Egraph.is_equal env a b) then (
        Egraph.merge env a b;
        if upd_loc then
          match nsb with
          | None -> ()
          | Some nsb -> NNSCLocs.rm_loc env (Id.NSeq.get_id env b) nsb);
      a
    in
    let next =
      match (na, nb) with
      | None, _ | _, None -> None
      | Some na, Some nb ->
          Egraph.merge env na nb;
          Some na
    in
    { a = resa; next }

  let relocate_cc =
    let add_r_ap env repr rfst rlst ap acc =
      match Id.M.find_opt rlst acc with
      | None -> Id.M.add rlst ap acc
      | Some ap' ->
          let res_ap =
            match_atom_pos_ret env (Id.get env rfst) (Id.get env rlst)
              (Id.get env repr) ap ap'
          in
          Id.M.add rlst res_ap acc
    in
    let relocate_ap env NSC.{ a; next } delta fst rfst rlst =
      (* TODO: relocation of the next index is done twice, here and when it is
         relocated in the nscc. It should only be done once, by using a map of
         substs.
      *)
      let next =
        match next with
        | None -> None
        | Some next ->
            let rnext, _ = Relocate.reloc_by_delta env next delta in
            Some rnext
      in
      let rfstid, rlstid, ra = relocate_atom env a fst rfst rlst in
      (rfstid, rlstid, NSC.{ a = ra; next })
    in
    fun env ?(upd_locs = false) ?old_repr repr (c : NSC.t) delta ->
      Debug.dprintf8 debug
        "NSComp_dom.relocate_cc with %a from (%a) to %a: {%a}" P.pp delta
        (Fmt.option (Id.pp_nid env))
        old_repr (Id.pp_nid env) repr (NSC.pp ~env) c;
      let res =
        if P.is_zero delta then c
        else
          Id.M.fold
            (fun fst m acc ->
              let nfst = Id.get env fst in
              let rfstn, rfst = Relocate.reloc_by_delta env nfst delta in
              let acc =
                Id.M.fold
                  (fun lst ap acc ->
                    let nlst = Id.get env lst in
                    let rlstn, rlst = Relocate.reloc_by_delta env nlst delta in
                    Opt.iter
                      (fun old_repr ->
                        if upd_locs then
                          NNSCLocs.rm_loc env
                            (Id.NSeq.get_id env ap.NSC.a)
                            (Id.get env old_repr);
                        NSCLocs.subst_locs env (old_repr, fst, lst)
                          (repr, rfst, rlst))
                      old_repr;
                    let rfst, rlst, ap' =
                      relocate_ap env ap delta nfst rfstn rlstn
                    in
                    let acc =
                      match Id.M.find_opt rfst acc with
                      | None -> Id.M.add rfst (Id.M.singleton rlst ap') acc
                      | Some m ->
                          let m' = add_r_ap env repr rfst rlst ap' m in
                          Id.M.add rfst m' acc
                    in
                    acc)
                  m acc
              in
              acc)
            c Id.M.empty
      in
      Debug.dprintf10 debug
        "NSComp_dom.relocate_cc res with %a from (%a) to %a\n{%a}\n{%a}" P.pp
        delta
        (Fmt.option (Id.pp_nid env))
        old_repr (Id.pp_nid env) repr (NSC.pp ~env) c (NSC.pp ~env) res;
      res

  let on_first_set_cdom_hook :
      (Egraph.wt -> Id.t -> NSC.t -> unit) Datastructure.Push.t =
    Datastructure.Push.create Fmt.nop "ConcatDom.on_first_set_cdom_hook"

  let register_on_first_set_cdom_hook env f =
    Datastructure.Push.push on_first_set_cdom_hook env f

  let on_set_cdom_hook :
      (Egraph.wt -> Id.t -> NSC.t -> unit) Datastructure.Push.t =
    Datastructure.Push.create Fmt.nop "ConcatDom.on_set_cdom_hook"

  let register_on_set_cdom_hook env f =
    Datastructure.Push.push on_set_cdom_hook env f

  let rec upd_dom' =
    let add_l env n ?next m l =
      let rec aux splits rms m l =
        match l with
        | [ (v, fst, lst) ] ->
            let splits', rms', res = add_ap env n ~m v ?next fst lst in
            (splits_union splits' splits, id_sm_union rms' rms, res)
        | (v, fst, lst) :: ((_, fst', _) :: _ as tl) ->
            let splits', rms', res =
              add_ap env n ~m v ~next:(Id.get env fst') fst lst
            in
            aux (splits_union splits' splits) (id_sm_union rms' rms) res tl
        | [] -> (splits, rms, m)
      in
      let splits, rms, res = aux Node.M.empty Id.M.empty m l in
      (splits, rms, res)
    in
    fun env n ?next l ->
      Debug.dprintf6 debug "ConcatDom.upd_cdom' %a: [%a] (%a)" Node.pp n
        (Fmt.list (fun fmt (ap, i1, i2) ->
             Fmt.pf fmt "; (%a, %a, %a)" (NSC.pp_atom ~env) ap (Id.pp_nid env)
               i1 (Id.pp_nid env) i2))
        l
        (Fmt.option (Id.Index.pp env))
        next;
      let nid = Id.NSeq.get_id env n in
      let um = Opt.get_def Id.M.empty (get_cdom env nid) in
      Debug.dprintf2 debug ">>> {%a}" (NSC.pp ~env) um;
      let splits, rms, um' = add_l env n ?next um l in
      upd_cdom env nid um';
      EqHook.rm_comps env rms;
      apply_splits env splits

  and upd_cdom env ?(upd_loc = false) nid newd =
    Debug.dprintf4 debug "ConcatDom.upd_cdom %a {%a}" (Id.pp_nid env) nid
      (Id.M.pp (Id.M.pp (pp_atom_pos ~env)))
      newd;
    let n = Id.get env nid in
    (* TODO: delay remove_nsc? *)
    let nsc_splits, rms, rescd =
      match get_cdom env nid with
      | None ->
          let splits, rms, rescd =
            Id.M.fold
              (fun fst m acc ->
                Id.M.fold
                  (fun lst { a; next } (split_acc, rm_acc, acc) ->
                    let splits, rms, acc =
                      add_ap env ~upd_loc n ~m:acc a ?next fst lst
                    in
                    (splits_union splits split_acc, id_sm_union rms rm_acc, acc))
                  m acc)
              newd
              (Node.M.empty, Id.M.empty, Id.M.empty)
          in
          set_cdom_ht env nid rescd;
          Datastructure.Push.iter on_first_set_cdom_hook env ~f:(fun f ->
              f env nid rescd);
          (splits, rms, rescd)
      | Some old ->
          let splits, rms, rescd =
            Id.M.fold
              (fun fst m acc ->
                Id.M.fold
                  (fun lst { a; next } (split_acc, rm_acc, acc) ->
                    let splits, rms, acc =
                      add_ap env ~upd_loc n ~m:acc a ?next fst lst
                    in
                    (splits_union splits split_acc, id_sm_union rms rm_acc, acc))
                  m acc)
              newd
              (Node.M.empty, Id.M.empty, old)
          in
          set_cdom_ht env nid rescd;
          (splits, rms, rescd)
    in
    EqHook.rm_comps env rms;
    apply_splits env nsc_splits;
    Datastructure.Push.iter on_set_cdom_hook env ~f:(fun f -> f env nid rescd)

  and set_cdom env nid d =
    Debug.dprintf4 debug "ConcatDom.set_cdom %a {%a}" (Id.pp_nid env) nid
      (NSC.pp ~env) d;
    let () =
      match get_cdom env nid with
      | None ->
          set_cdom_ht env nid d;
          Datastructure.Push.iter on_first_set_cdom_hook env ~f:(fun f ->
              f env nid d)
      | Some _ -> set_cdom_ht env nid d
    in
    Datastructure.Push.iter on_set_cdom_hook env ~f:(fun f -> f env nid d)

  and upd_dom_aux env ?gnext n newd oldd =
    Debug.dprintf6 debug "ConcatDom.upd_dom_aux %a {%a} {%a}" Node.pp n
      (NSC.pp ~env) newd (NSC.pp ~env) oldd;
    Id.M.fold
      (fun fst m acc ->
        Id.M.fold
          (fun lst { a; next } (splits_acc, acc) ->
            let next = match next with None -> gnext | _ -> next in
            let splits, acc = add_ap_aux env n ~m:acc a ?next fst lst in
            (splits_union splits splits_acc, acc))
          m acc)
      newd (Node.M.empty, oldd)

  and subst_n_dom =
    let subst_n env nloc n f fid l lid d =
      Debug.dprintf10 debug "subst_n_dom.subst_n %a (%a [%a; %a] <- [ %a])"
        (Id.NSeq.pp env) nloc (Id.NSeq.pp env) n (Id.Index.pp env) f
        (Id.Index.pp env) l (NSC.pp ~env) d;
      let nlid = Id.NSeq.get_id env nloc in
      let m0 = Opt.get_exn Not_found (get_cdom env nlid) in
      Debug.dprintf2 debug ">> %a" (NSC.pp ~env) m0;
      let m0', m1_opt = Id.M.find_remove fid m0 in
      let m1 = Opt.get_exn Not_found m1_opt in
      Debug.dprintf2 debug "> %a" (Id.M.pp (pp_atom_pos ~env)) m1;
      let m1', ap_opt = Id.M.find_remove lid m1 in
      let ap = Opt.get_exn Not_found ap_opt in
      let splits =
        match ap with
        | { a = n'; next } when Egraph.is_equal env n n' ->
            NSCLocs.rm_bounds env (Id.NSeq.get_id env nloc) fid lid;
            let nm = if Id.M.is_empty m1' then m0' else Id.M.add fid m1' m0' in
            let splits, resd = upd_dom_aux env ?gnext:next nloc d nm in
            set_cdom env nlid resd;
            splits
        | _ -> assert false
      in
      splits
    in
    let aux env ?(split_acc = Node.M.empty) val_gty n nid ?dist d =
      Debug.dprintf8 debug "subst_n_dom.aux %a (%a) (dist: %a) <- %a"
        (Id.NSeq.pp env) n Id.pp nid (Fmt.option P.pp) dist (NSC.pp ~env) d;
      let fst, fstid, lst, lstid = Bounds.get_mk_bounds_n env n in
      Debug.dprintf8 debug ">  %a  %a  %a  %a" (Id.Index.pp env) fst
        (Id.pp_nid env) fstid (Id.Index.pp env) lst (Id.pp_nid env) lstid;
      let nlocs = NNSCLocs.get_locs env nid in
      NNSCLocs.rm_id env nid;
      (* let nlocs =
           (* To avoid recursive substitutions, when a term appears as a
              component of itself. Happens rarely in some cases, when the merging
              of the nodes of the bounds of the nseq term was not done yet.
              TODO: make sure nseq component concatenetations never contain just
              one component *)
           Node.S.filter (fun nl -> not (Egraph.is_equal env nl orig)) nlocs
         in *)
      Debug.dprintf2 debug ">>> nlocs: {%a}"
        (fun fmt -> Node.S.iter (Fmt.pf fmt "%a" (Id.NSeq.pp env)))
        nlocs;
      if Node.S.is_empty nlocs then (split_acc, None)
      else
        let d =
          match dist with
          | Some dist when not (P.is_zero dist) ->
              let res = relocate_cc env nid d dist in
              res
          | _ -> d
        in
        let fst, fid, lst, lid = Bounds.get_mk_bounds_n env ~val_gty n in
        let splits =
          Node.S.fold
            (fun nloc split_acc ->
              let splits = subst_n env nloc n fst fid lst lid d in
              splits_union splits split_acc)
            nlocs split_acc
        in
        (splits, Some d)
    in
    fun env nid d ->
      Debug.dprintf4 debug "subst_n_dom %a <- %a" (Id.pp_nid env) nid
        (NSC.pp ~env) d;
      let n = Id.get env nid in
      let nsc_splits =
        match Relocate.get_repr env nid with
        | Repr m ->
            Debug.dprintf2 debug "> m1: {%a}" (P.M.pp (Id.pp_nid env)) m;
            let val_gty = get_nseq_val_gty env n in
            let splits, _ = aux env val_gty n nid d in
            P.M.fold
              (fun dist id split_acc ->
                fst @@ aux ~split_acc env val_gty (Id.get env id) id ~dist d)
              m splits
        | NonRepr { repr; dist = odist } -> (
            match Relocate.get_repr env repr with
            | NonRepr _ -> assert false
            | Repr m -> (
                let m = P.M.remove odist m in
                Debug.dprintf4 debug "> m2: %a & {%a}" (Id.pp_nid env) repr
                  (P.M.pp (Id.pp_nid env))
                  m;
                let val_gty = get_nseq_val_gty env n in
                let split_acc, _ = aux env val_gty n nid d in
                let dist = P.neg odist in
                let split_acc, d' =
                  aux env ~split_acc val_gty (Id.get env repr) repr ~dist d
                in
                match d' with
                | None ->
                    P.M.fold
                      (fun dist' id split_acc ->
                        let dist = P.add dist dist' in
                        fst
                        @@ aux env ~split_acc val_gty (Id.get env id) id ~dist d)
                      m split_acc
                | Some d ->
                    P.M.fold
                      (fun dist id split_acc ->
                        fst
                        @@ aux env ~split_acc val_gty (Id.get env id) id ~dist d)
                      m split_acc))
      in
      apply_splits env nsc_splits

  and add_ap_aux env n ?m a ?next fst lst =
    let v = NSC.{ a; next } in
    Debug.dprintf8 debug "ConcatDom.add_ap_aux  n=%a [%a; %a]  ap=%a" Node.pp n
      (Id.pp_nid env) fst (Id.pp_nid env) lst (NSC.pp_atom_pos ~env) v;
    let upd_locs () =
      Debug.dprintf4 debug "upd_bounds()  ap=%a a=%a" (NSC.pp_atom_pos ~env) v
        NSC.pp_atom a;
      (* let nfst = Id.get env fst in
         let nlst = Id.get env lst in *)
      (let nsid = Id.NSeq.get_id_safe env a in
       NNSCLocs.add_loc env nsid n);
      NSCLocs.add_bounds env (Id.NSeq.get_id_safe env n) fst lst
    in
    match m with
    | None ->
        upd_locs ();
        (Node.M.empty, Id.M.singleton fst (Id.M.singleton lst v))
    | Some m -> (
        match Id.M.find_opt fst m with
        | None ->
            upd_locs ();
            (Node.M.empty, Id.M.add fst (Id.M.singleton lst v) m)
        | Some m' -> (
            match Id.M.find_opt lst m' with
            | None ->
                upd_locs ();
                let splits =
                  Id.M.fold
                    (fun lst' _ splits ->
                      (* split_choice env n (Id.get env fst) (Id.get env lst)
                           (Id.get env lst');
                         splits *)
                      add_split ~splits fst lst lst')
                    m' Id.IdT.S.empty
                in
                let resm' = Id.M.add_new Impossible lst v m' in
                (add_split_acc n splits Node.M.empty, Id.M.add fst resm' m)
            | Some v' ->
                let resm' =
                  let ns =
                    match_atom_pos_ret env ~upd_loc:false (Id.get env fst)
                      (Id.get env lst) n ~nsb:n v' v
                  in
                  Id.M.add lst ns m'
                in
                (Node.M.empty, Id.M.add fst resm' m)))

  (** TODO: when a sequence component is added, if it is added to an existing
      component that is potentially bigger than it, a decision needs to be added
      on whether or not that component needs splitting. *)
  and add_ap env ?(upd_loc = false) n ?m a ?next fst lst =
    Debug.dprintf8 debug "ConcatDom.add_ap1  n=%a [%a; %a]  ap=%a" Node.pp n
      (Id.pp_nid env) fst (Id.pp_nid env) lst (NSC.pp_atom_pos ~env) { a; next };
    let cancel =
      match next with
      | None -> false
      | Some next ->
          (* TODO: temporary fix, should be removed when precs are added *)
          let nextid = Id.Index.get_id env next in
          if Id.equal fst nextid then (
            NSeq_dom.set_is_empty env (Id.NSeq.get_id env a);
            true)
          else (
            EqHook.add_rmnsc_hook env fst nextid (Id.get env fst, Id.get env lst);
            false)
    in
    if cancel then (
      let rms = Id.M.singleton fst (Id.S.singleton lst) in
      if upd_loc then NSCLocs.rm_bounds env (Id.NSeq.get_id env n) fst lst;
      (Node.M.empty, rms, Opt.get_def Id.M.empty m))
    else
      let raux () = add_ap_aux env n ?m a ?next fst lst in
      let splits, res =
        match Id.NSeq.get_id env a with
        | exception Colibri2_theories_utils.Ident.NoIdFound _ -> raux ()
        | nid' -> (
            let id, d =
              match Relocate.get_repr env nid' with
              | Repr _ -> (nid', P.zero)
              | NonRepr { repr; dist } -> (repr, dist)
            in
            match get_cdom env id with
            | None -> raux ()
            | Some m' ->
                let m' =
                  if P.is_zero d then m'
                  else relocate_cc env (Id.NSeq.get_id env n) m' d
                in
                Debug.dprintf2 debug "ConcatDom.add_ap2 {%a}" (NSC.pp ~env) m';
                Id.M.fold
                  (fun fst m acc ->
                    Id.M.fold
                      (fun lst { a; next } (splits_acc, acc) ->
                        let splits, acc =
                          add_ap_aux env n ~m:acc a ?next fst lst
                        in
                        (splits_union splits splits_acc, acc))
                      m acc)
                  m'
                  (Node.M.empty, Opt.get_def Id.M.empty m))
      in
      (splits, Id.M.empty, res)

  and split_choice =
    let split_choice_unit env n unit_bound nlst =
      Debug.dprintf10 debug "split_choice_unit in %a: [%a; %a] over [%a; %a]"
        Node.pp n (Id.Index.pp env) unit_bound (Id.Index.pp env) nlst
        (Id.Index.pp env) unit_bound (Id.Index.pp env) unit_bound;
      let le_cond = mk_le env [ unit_bound; nlst ] in
      Egraph.register env le_cond;
      let cons env = split env n unit_bound unit_bound nlst in
      let alt env =
        EqHook.rm_comp env
          (Id.Index.get_id env unit_bound)
          (Id.Index.get_id env nlst)
      in
      new_decision env le_cond "Decision from NSeq.Theory2.split_choice_unit"
        ~cons ~alt (fun _ ->
          DecTodo
            [
              (fun env ->
                Boolean.set_true env le_cond;
                cons env);
              (fun env ->
                Boolean.set_false env le_cond;
                alt env);
            ])
    in
    fun env n nfst nlst1 nlst2 ->
      Debug.dprintf10 debug "mk_split_choice %a [%a; %a] [%a; %a]" Node.pp n
        (Id.Index.pp env) nfst (Id.Index.pp env) nlst1 (Id.Index.pp env) nfst
        (Id.Index.pp env) nlst2;
      if Egraph.is_equal env nfst nlst1 then split_choice_unit env n nfst nlst2
      else if Egraph.is_equal env nfst nlst2 then
        split_choice_unit env n nfst nlst1
      else
        (* TODO: only split with the inequality is strict? *)
        let le_cond1 = mk_le env [ nfst; nlst1 ] in
        Egraph.register env le_cond1;
        let cons3 env = split env n nfst nlst2 nlst1 in
        let alt3 env =
          EqHook.rm_comp env (Id.Index.get_id env nfst)
            (Id.Index.get_id env nlst2)
        in
        let cons2 env = split env n nfst nlst1 nlst2 in
        let alt2 env =
          let le_cond3 = mk_le env [ nfst; nlst2 ] in
          Egraph.register env le_cond3;
          new_decision env le_cond3 "Decision from NSeq.Theory2.split_choice1"
            ~cons:cons3 ~alt:alt3 (fun _ ->
              DecTodo
                [
                  (fun env ->
                    Boolean.set_true env le_cond3;
                    cons3 env);
                  (fun env ->
                    Boolean.set_false env le_cond3;
                    alt3 env);
                ])
        in
        let cons1 env =
          let le_cond2 = mk_le env [ nlst1; nlst2 ] in
          Egraph.register env le_cond2;
          new_decision env le_cond2 "Decision from NSeq.Theory2.split_choice2"
            ~cons:cons2 ~alt:alt2 (fun _ ->
              DecTodo
                [
                  (fun env ->
                    Boolean.set_true env le_cond2;
                    cons2 env);
                  (fun env ->
                    Boolean.set_false env le_cond2;
                    let le_cond3 = mk_le env [ nfst; nlst2 ] in
                    Egraph.register env le_cond3;
                    Boolean.set_true env le_cond3;
                    cons3 env);
                  (fun env ->
                    Boolean.set_false env le_cond2;
                    let le_cond3 = mk_le env [ nfst; nlst2 ] in
                    Egraph.register env le_cond3;
                    Boolean.set_false env le_cond3;
                    alt3 env);
                ])
        in
        let alt1 env =
          EqHook.rm_comp env (Id.Index.get_id env nfst)
            (Id.Index.get_id env nlst1)
        in
        new_decision env le_cond1 "Decision from NSeq.Theory2.split_choice3"
          ~cons:cons1 ~alt:alt1 (fun _ ->
            DecTodo
              [
                (fun env ->
                  Boolean.set_false env le_cond1;
                  alt1 env);
                (fun env ->
                  Boolean.set_true env le_cond1;
                  let le_cond2 = mk_le env [ nlst1; nlst2 ] in
                  Egraph.register env le_cond2;
                  Boolean.set_true env le_cond2;
                  cons2 env);
                (fun env ->
                  Boolean.set_true env le_cond1;
                  let le_cond2 = mk_le env [ nlst1; nlst2 ] in
                  Egraph.register env le_cond2;
                  let le_cond3 = mk_le env [ nfst; nlst2 ] in
                  Egraph.register env le_cond3;
                  Boolean.set_false env le_cond2;
                  Boolean.set_true env le_cond3;
                  cons3 env);
                (fun env ->
                  Boolean.set_true env le_cond1;
                  let le_cond2 = mk_le env [ nlst1; nlst2 ] in
                  Egraph.register env le_cond2;
                  let le_cond3 = mk_le env [ nfst; nlst2 ] in
                  Egraph.register env le_cond3;
                  Boolean.set_false env le_cond2;
                  Boolean.set_false env le_cond3;
                  alt3 env);
              ])

  and split =
    let split_aux env nid n nfst fst nlst2 lst2 nlst1 lst1 =
      if Id.equal fst lst1 then (
        Debug.dprintf14 debug
          "split_aux [%a; %a] over [%a; %a] in %a\n\
           Not splitting because %a = %a"
          (Id.Index.pp env) nfst (Id.Index.pp env) nlst1 (Id.Index.pp env) nfst
          (Id.Index.pp env) nlst2 (Id.NSeq.pp env) n (Id.Index.pp env) nfst
          (Id.Index.pp env) nlst1;
        EqHook.rm_comp env fst lst2)
      else (
        Debug.dprintf10 debug "split_aux [%a; %a] over [%a; %a] in %a"
          (Id.Index.pp env) nfst (Id.Index.pp env) nlst1 (Id.Index.pp env) nfst
          (Id.Index.pp env) nlst2 (Id.NSeq.pp env) n;
        let m = Opt.get_exn Not_found (get_cdom env nid) in
        Debug.dprintf2 debug "> {%a}" (NSC.pp ~env) m;
        Debug.dprintf4 debug "split because %a <> %a" (Id.Index.pp env) nfst
          (Id.Index.pp env) nlst1;
        let nfst', fst' = nsucc env nlst2 in
        Egraph.register env nfst';
        match Id.M.find_opt fst m with
        | None ->
            (* This means that the split already happened and that there's
               nothing to be done. *)
            ()
        | Some m' -> (
            match Id.M.find_opt lst1 m' with
            | None ->
                (* This means that the split already happened and that there's
                   nothing to be done. *)
                ()
            | Some ap1 -> (
                match Id.M.find_opt lst2 m' with
                | None ->
                    (* Same in this case *)
                    ()
                | Some ap2 -> (
                    (* TODO: upd_dom of reprs *)
                    let n1id = Id.NSeq.get_id env ap1.NSC.a in
                    match Relocate.get_repr env n1id with
                    | Repr _ ->
                        let fv = fresh_var_id env n nfst' nlst1 in
                        ignore
                        @@ upd_dom' env ap1.NSC.a
                             [ (ap2.a, fst, lst2); (fv, fst', lst1) ]
                    | NonRepr { repr; dist } ->
                        let dist = P.neg dist in
                        let rn1 = Id.get env repr in
                        let nfst'', fst'' =
                          Relocate.reloc_by_delta env nfst' dist
                        in
                        let nlst1', lst1' =
                          Relocate.reloc_by_delta env nlst1 dist
                        in
                        let nfst', fst' =
                          Relocate.reloc_by_delta env nfst dist
                        in
                        let nlst2', lst2' =
                          Relocate.reloc_by_delta env nlst2 dist
                        in
                        let _, _, ra =
                          relocate_atom env ap2.a nfst nfst' nlst2'
                        in
                        let fv = fresh_var_id env n nfst'' nlst1' in
                        ignore
                        @@ upd_dom' env rn1
                             [ (ra, fst', lst2'); (fv, fst'', lst1') ]))))
    in
    fun env n nfst nlst2 nlst1 ->
      Debug.dprintf10 debug "split [%a; %a] over [%a; %a] in %a"
        (Id.Index.pp env) nfst (Id.Index.pp env) nlst1 (Id.Index.pp env) nfst
        (Id.Index.pp env) nlst2 (Id.NSeq.pp env) n;
      let nid = Id.NSeq.get_id env n in
      match Relocate.get_repr env nid with
      | Repr _ ->
          let fst = Id.Index.get_id env nfst in
          let lst1 = Id.Index.get_id env nlst1 in
          let lst2 = Id.Index.get_id env nlst2 in
          split_aux env nid n nfst fst nlst2 lst2 nlst1 lst1
      | NonRepr _ ->
          (* If the repr changed and the decision is still persitent it will
             be recreated with the right bounds on the new repr.
             TODO: it's probably better to find a way to do it without
             creating a new decision *)
          ()
  (* let dist = P.neg dist in
     let rn = Id.get env repr in
     let nfst', fst' = Relocate.reloc_by_delta env nfst dist in
     let nlst2', lst2' = Relocate.reloc_by_delta env nlst2 dist in
     let nlst1', lst1' = Relocate.reloc_by_delta env nlst1 dist in
     split_aux env repr rn nfst' fst' nlst2' lst2' nlst1' lst1' *)

  and apply_splits env nsc_splits =
    Debug.dprintf2 debug "apply_splits: {%a}" (Node.M.pp Id.IdT.S.pp) nsc_splits;
    Node.M.iter
      (fun n s ->
        Id.IdT.S.iter
          (fun (fst, lst1, lst2) ->
            split_choice env n (Id.get env fst) (Id.get env lst1)
              (Id.get env lst2))
          s)
      nsc_splits

  let update m fst lst n =
    Id.M.change
      (function
        | None -> assert false
        | Some m' ->
            Some
              (Id.M.change
                 (function None -> assert false | Some _ -> Some n)
                 lst m'))
      fst m

  let apply_nth env f nfst t inode acc =
    Debug.dprintf6 debug "apply_nth [%a; _] %a\n%a" (Id.Index.pp env) nfst
      (Id.Index.pp env) inode (NSC.pp ~env) t;
    let rec aux env fst precs =
      match Id.M.find_opt fst t with
      | None -> (true, precs)
      | Some m ->
          let check_precs, acc =
            Id.M.fold
              (fun lst atl (check_precs, acc) ->
                let nlst = Id.get env lst in
                match atl with
                | { a; next = None } -> (
                    match (is_le env nfst inode, is_le env inode nlst) with
                    | Some true, Some true | Some true, None ->
                        (false, (a, fst, lst, None) :: acc)
                    | None, (None | Some true) ->
                        (check_precs, (a, fst, lst, None) :: acc)
                    | (None | Some true), Some false -> (false, acc)
                    | Some false, _ -> (check_precs, (a, fst, lst, None) :: acc)
                    )
                | { a; next = Some infst as next } -> (
                    let ifst = Id.Index.get_id env infst in
                    match (is_le env nfst inode, is_le env inode nlst) with
                    | Some true, Some true -> (false, (a, fst, lst, next) :: acc)
                    | Some true, None ->
                        let _, acc' = aux env ifst [ (a, fst, lst, next) ] in
                        (false, ((a, fst, lst, next) :: acc) @ acc')
                    | None, None ->
                        let check', acc' =
                          aux env ifst [ (a, fst, lst, next) ]
                        in
                        ( check' && check_precs,
                          ((a, fst, lst, next) :: acc) @ acc' )
                    | None, Some true ->
                        (check_precs, (a, fst, lst, next) :: acc)
                    | Some true, Some false -> (false, acc)
                    | None, Some false ->
                        let _, acc' = aux env ifst [] in
                        (false, acc @ acc')
                    | Some false, _ -> (true, [])))
              m (true, [])
          in
          if check_precs then (check_precs, precs @ acc) else (check_precs, acc)
    in
    let _, al = aux env (Id.Index.get_id env nfst) [] in
    List.fold_left
      (fun acc (a, afst, alst, next) -> f env a afst alst ?next acc)
      acc al
end

and NSCLocs : sig
  val add_bounds : Egraph.wt -> Id.t -> Id.t -> Id.t -> unit
  val rm_nr_nseq : Egraph.wt -> Id.t -> unit
  val eq_indices_norm : Egraph.wt -> Id.t -> Id.t -> unit
  val rm_bounds : Egraph.wt -> Id.t -> Id.t -> Id.t -> unit
  val bounds_exist : Egraph.wt -> Id.t -> Id.t -> Id.t -> bool
  val subst_locs : Egraph.wt -> Id.t * Id.t * Id.t -> Id.t * Id.t * Id.t -> unit
  val get_fsts : Egraph.wt -> Id.t -> Id.t -> Id.S.t
  val pp : Format.formatter -> Egraph.wt -> unit [@@warning "-32"]
end = struct
  module T = struct
    type t = B of (Id.S.t * Id.S.t) Id.M.t | N of Id.S.t
    (* | BN of Id.S.t * (Id.S.t * Id.S.t) Id.M.t *)
    [@@deriving hash]

    let pp fmt = function
      | N s -> Fmt.pf fmt "N: %a" Id.S.pp s
      | B m ->
          Fmt.pf fmt "B: %a"
            (Id.M.pp (fun fmt (s1, s2) ->
                 Fmt.pf fmt "(%a | %a)" Id.S.pp s1 Id.S.pp s2))
            m
  end

  module HT = Id.MkIHT (struct
    include T

    let name = "NSCLocs"
  end)

  let pp = HT.pp

  let add_bounds env nsid fstid lstid =
    Debug.dprintf6 debug "NSCLocs.add_bounds  %a  %a  %a" Id.pp nsid Id.pp fstid
      Id.pp lstid;
    HT.change env nsid ~f:(function
      | Some (B _) -> assert false
      | None -> Some (N (Id.S.add lstid (Id.S.singleton fstid)))
      | Some (N s) -> Some (N (Id.S.add lstid (Id.S.add fstid s))));
    HT.change env fstid ~f:(function
      | Some (N _) -> assert false
      | None ->
          Some (B (Id.M.singleton nsid (Id.S.singleton lstid, Id.S.empty)))
      | Some (B m) ->
          Some
            (B
               (Id.M.change
                  (function
                    | None -> Some (Id.S.singleton lstid, Id.S.empty)
                    | Some (s1, s2) -> Some (Id.S.add lstid s1, s2))
                  nsid m)));
    HT.change env lstid ~f:(function
      | Some (N _) -> assert false
      | None ->
          Some (B (Id.M.singleton nsid (Id.S.empty, Id.S.singleton fstid)))
      | Some (B m) ->
          Some
            (B
               (Id.M.change
                  (function
                    | None -> Some (Id.S.empty, Id.S.singleton fstid)
                    | Some (s1, s2) -> Some (s1, Id.S.add fstid s2))
                  nsid m)))

  let rm_nr_nseq env id =
    Debug.dprintf2 debug "NSCLocs.rm_nr_nseq %a" Id.pp id;
    match HT.find_opt env id with
    | None -> ()
    | Some (B _) -> assert false
    | Some (N s) ->
        HT.remove env id;
        Id.S.iter
          (fun bid ->
            HT.change env bid ~f:(function
              | None -> raise Not_found
              | Some (N _) -> assert false
              | Some (B m) -> (
                  match Id.M.find_remove id m with
                  | m', Some _ -> Some (B m')
                  | _ -> assert false)))
          s

  let subst_lst env ns fstid kid rid m =
    Debug.dprintf6 debug "NSCLocs.subst_lst (%a <- %a) in %a" Id.pp rid Id.pp
      kid Node.pp ns;
    match Id.M.find_remove rid m with
    | _, None -> assert false
    | m', Some a -> (
        match Id.M.find_opt kid m' with
        | None -> Id.M.add kid a m'
        | Some a' ->
            let res =
              ConcatDom.match_atom_pos_ret env (Id.get env fstid)
                (Id.get env rid) ns a' ~nsb:ns a
            in
            Id.M.add kid res m')

  let add_lsts env ns kid _rid c m =
    Debug.dprintf8 debug "NSCLocs.add_lsts %a (%a <- %a): %a" Node.pp ns Id.pp
      _rid Id.pp kid
      (Id.M.pp (NSC.pp_atom_pos ~env))
      m;
    match Id.M.find_opt kid c with
    | None -> (Node.M.empty, Id.M.add kid m c)
    | Some m' ->
        let splits, nm =
          Id.M.fold
            (fun lst a1 (acc_splits, acc_m) ->
              match Id.M.find_opt lst acc_m with
              | None ->
                  let splits =
                    Id.M.fold
                      (fun lst' _ splits ->
                        ConcatDom.add_split ~splits kid lst lst')
                      m' Id.IdT.S.empty
                  in
                  ( ConcatDom.add_split_acc ns splits acc_splits,
                    Id.M.add lst a1 acc_m )
              | Some a2 ->
                  let res =
                    ConcatDom.match_atom_pos_ret env (Id.get env kid)
                      (Id.get env lst) ns a1 ~nsb:ns a2
                  in
                  (acc_splits, Id.M.add lst res acc_m))
            m (Node.M.empty, m')
        in
        (splits, Id.M.add kid nm c)

  let upd_nsc_locs env seen m nsid kid rid =
    Debug.dprintf8 debug "NSCLocs.upd_nsc_locs %a (%a <- %a): %a" Id.pp nsid
      Id.pp rid Id.pp kid
      (Id.M.pp (NSC.pp_atom_pos ~env))
      m;
    Id.M.fold
      (fun lst _ seen ->
        if Id.S.mem lst seen then seen
        else (
          HT.change env lst ~f:(function
            | None -> assert false
            | Some (N _) -> assert false
            | Some (B m) ->
                Some
                  (B
                     (Id.M.change
                        (function
                          | None -> assert false
                          | Some (s1, s2) ->
                              Some
                                ( (if Id.S.mem rid s1 then
                                     Id.S.add kid (Id.S.remove rid s1)
                                   else s1),
                                  if Id.S.mem rid s2 then
                                    Id.S.add kid (Id.S.remove rid s2)
                                  else s2 ))
                        nsid m)));
          Id.S.add lst seen))
      m seen

  let bounds_exist env nsid fst lst =
    match HT.find_opt env nsid with
    | None -> false
    | Some (B _) -> false
    | Some (N s) ->
        if Id.S.mem fst s && Id.S.mem lst s then
          (match HT.find_opt env fst with
          | None -> assert false
          | Some (N _) -> assert false
          | Some (B m) -> (
              match Id.M.find_opt nsid m with
              | None -> assert false
              | Some (s1, _) -> Id.M.mem lst s1))
          &&
          match HT.find_opt env lst with
          | None -> assert false
          | Some (N _) -> assert false
          | Some (B m) -> (
              match Id.M.find_opt nsid m with
              | None -> assert false
              | Some (_, s2) -> Id.M.mem fst s2)
        else false

  let rm_bounds env nsid fst lst =
    Debug.dprintf6 debug "NSCLocs.rm_bounds %a %a %a" Id.pp nsid Id.pp fst Id.pp
      lst;
    let kfst =
      match HT.find env fst with
      | N _ -> assert false
      | B m ->
          let s1, s2 = Id.M.find nsid m in
          let s1' = Id.M.remove lst s1 in
          if Id.S.is_empty s1' && Id.S.is_empty s2 then (
            let m' = Id.M.remove nsid m in
            if Id.M.is_empty m' then HT.remove env fst
            else HT.set env fst (B m');
            false)
          else
            let m' = Id.M.add nsid (s1', s2) m in
            HT.set env fst (B m');
            true
    in
    let klst =
      match HT.find env lst with
      | N _ -> assert false
      | B m ->
          let s1, s2 = Id.M.find nsid m in
          let s2' = Id.M.remove fst s2 in
          if Id.S.is_empty s1 && Id.S.is_empty s2' then (
            let m' = Id.M.remove nsid m in
            if Id.M.is_empty m' then HT.remove env lst
            else HT.set env lst (B m');
            false)
          else
            let m' = Id.M.add nsid (s1, s2') m in
            HT.set env lst (B m');
            true
    in
    HT.change env nsid ~f:(function
      | None | Some (B _) -> assert false
      | Some (N s) ->
          if kfst then if klst then Some (N s) else Some (N (Id.S.remove lst s))
          else
            let s' = Id.S.remove fst (if klst then s else Id.S.remove lst s) in
            if Id.S.is_empty s' then None else Some (N s'))

  let subst_nsc_bounds env seen nsid is_fst is_lst s1 s2 kid rid =
    Debug.dprintf12 debug
      "NSCLocs.subst_nsc_bounds %a %b %b  (%a <- %a) (%a | %a)" (Id.pp_nid env)
      nsid is_fst is_lst Id.pp rid Id.pp kid Id.S.pp s1 Id.S.pp s2;
    let ns = Id.get env nsid in
    match ConcatDom.get_cdom env nsid with
    | None -> assert false
    | Some c ->
        Debug.dprintf2 debug "c = {%a}" (NSC.pp ~env) c;
        let seen, splits, c, s2' =
          if is_fst then
            let seen, splits, c =
              if is_lst then
                match Id.M.find_remove rid c with
                | c', Some m ->
                    let seen = upd_nsc_locs env seen m nsid kid rid in
                    let nm = subst_lst env ns rid kid rid m in
                    let splits, nm = add_lsts env ns kid rid c' nm in
                    (seen, splits, nm)
                | _ -> assert false
              else
                match Id.M.find_remove rid c with
                | c', Some m ->
                    let seen = upd_nsc_locs env seen m nsid kid rid in
                    let splits, nm = add_lsts env ns kid rid c' m in
                    (seen, splits, nm)
                | _ -> assert false
            in
            (seen, splits, c, Id.S.remove rid s2)
          else (seen, Node.M.empty, c, s2)
        in
        let c' =
          Id.S.fold
            (fun fstid c_acc ->
              HT.change env fstid ~f:(function
                | None -> assert false
                | Some (N _) -> assert false
                | Some (B m) ->
                    Some
                      (B
                         (Id.M.change
                            (function
                              | None -> assert false
                              | Some (s1, s2) ->
                                  Some (Id.S.add kid (Id.S.remove rid s1), s2))
                            nsid m)));
              match Id.M.find_opt fstid c_acc with
              | None -> assert false
              | Some m ->
                  let res = subst_lst env ns fstid kid rid m in
                  Id.M.add fstid res c_acc)
            s2' c
        in
        Id.S.iter
          (fun lstid ->
            HT.change env lstid ~f:(function
              | None -> assert false
              | Some (N _) -> assert false
              | Some (B m) ->
                  Some
                    (B
                       (Id.M.change
                          (function
                            | None -> assert false
                            | Some (s1, s2) ->
                                Some (s1, Id.S.add kid (Id.S.remove rid s2)))
                          nsid m))))
          s1;
        ConcatDom.set_cdom env nsid c';
        (splits, seen)

  let subst_locs env (nsid1, fst1, lst1) (nsid2, fst2, lst2) =
    Debug.dprintf12 debug "NSCLocs.subst_locs (%a, %a, %a) (%a, %a, %a)" Id.pp
      nsid1 Id.pp fst1 Id.pp lst1 Id.pp nsid2 Id.pp fst2 Id.pp lst2;
    rm_bounds env nsid1 fst1 lst1;
    add_bounds env nsid2 fst2 lst2

  let eq_indices_norm env kid rid =
    Debug.dprintf4 debug "NSCLocs.eq_indices_norm (%a <- %a)" Id.pp rid Id.pp
      kid;
    let nsc_splits =
      match HT.find_opt env rid with
      | None -> Node.M.empty
      | Some (N _) -> assert false
      | Some (B m) ->
          let _, splits, m =
            Id.M.fold
              (fun nsid (s1, s2) (seen, split_acc, acc) ->
                let is_f = not (Id.S.is_empty s1) in
                let is_fl = Id.S.mem rid s1 in
                let splits, seen =
                  subst_nsc_bounds env seen nsid is_f is_fl s1 s2 kid rid
                in
                HT.change env nsid ~f:(function
                  | None -> assert false
                  | Some (B _) -> assert false
                  | Some (N s) -> Some (N (Id.S.add kid (Id.S.remove rid s))));
                ( seen,
                  ConcatDom.splits_union splits split_acc,
                  Id.M.add nsid
                    ( (if is_fl then Id.S.add kid (Id.S.remove rid s1) else s1),
                      if is_fl then Id.S.add kid (Id.S.remove rid s2) else s2 )
                    acc ))
              m
              (Id.S.empty, Node.M.empty, Id.M.empty)
          in
          HT.remove env rid;
          HT.change env kid ~f:(function
            | None -> Some (B m)
            | Some (N _) -> assert false
            | Some (B m') ->
                let m'' =
                  Id.M.union
                    (fun _ (s1, s2) (s1', s2') ->
                      Some (Id.S.union s1 s1', Id.S.union s2 s2'))
                    m m'
                in
                Some (B m''));
          splits
    in
    ConcatDom.apply_splits env nsc_splits

  let get_fsts env lst nid =
    match HT.find_opt env lst with
    | None -> Id.S.empty
    | Some (N _) -> assert false
    | Some (B m) -> (
        match Id.M.find_opt nid m with None -> Id.S.empty | Some (_, s) -> s)
end

and EqHook : sig
  (* a = slice(b, f, l):
     - if fst(b) = f then fst(a) = f
     - if lst(b) = l then lst(b) = l *)
  val add_rmnsc_hook : Egraph.wt -> Id.t -> Id.t -> Node.t * Node.t -> unit
  val add_mrnodes_hook : Egraph.wt -> Id.t -> Id.t -> Node.t * Node.t -> unit
  val rm_comp : Egraph.wt -> Id.t -> Id.t -> unit
  val rm_comps : Egraph.wt -> Id.S.t Id.M.t -> unit
  val eq_indices_norm : Egraph.wt -> Id.t -> Id.t -> unit
end = struct
  module RmNSCS = Extset.Make (struct
    type t = Node.t * Node.t [@@deriving ord, hash, show]
  end)

  (* merge the nodes *)
  module MrNodes = Extset.Make (struct
    type t = Node.t * Node.t [@@deriving ord, hash, show]
  end)

  module HT = Id.MkIHT (struct
    let name = "EqHookDB"

    type t = (RmNSCS.t * MrNodes.t) Id.M.t [@@deriving hash, show]
  end)

  let add_rmnsc_hook =
    let aux k v = function
      | None -> Some (Id.M.singleton k (RmNSCS.singleton v, MrNodes.empty))
      | Some m ->
          Some
            (Id.M.change
               (function
                 | None -> Some (RmNSCS.singleton v, MrNodes.empty)
                 | Some (s1, s2) -> Some (RmNSCS.add v s1, s2))
               k m)
    in
    fun env k1 k2 ((f, l) as v) ->
      Debug.dprintf8 debug "EqHook.add_rmnsc_hook %a %a (%a, %a)" Id.pp k1 Id.pp
        k2 Node.pp f Node.pp l;
      if not (Egraph.is_equal env f l) then (
        assert (k1 <> k2);
        HT.change ~f:(aux k2 v) env k1;
        HT.change ~f:(aux k1 v) env k2)

  let add_mrnodes_hook =
    let aux k v = function
      | None -> Some (Id.M.singleton k (RmNSCS.empty, MrNodes.singleton v))
      | Some m ->
          Some
            (Id.M.change
               (function
                 | None -> Some (RmNSCS.empty, MrNodes.singleton v)
                 | Some (s1, s2) -> Some (s1, MrNodes.add v s2))
               k m)
    in
    fun env k1 k2 ((n1, n2) as v) ->
      Debug.dprintf8 debug "EqHook.add_mrnodes_hook %a %a (%a, %a)" Id.pp k1
        Id.pp k2 Node.pp n1 Node.pp n2;
      if Id.equal k1 k2 then Egraph.merge env n1 n2
      else (
        HT.change ~f:(aux k2 v) env k1;
        HT.change ~f:(aux k1 v) env k2)

  let remove_nsc env ?(exists = true) ?(rm_nloc = false) n nscfst nsclst =
    Debug.dprintf6 debug "EqHook.remove_nsc [%a; %a] in %a" (Id.pp_nid env)
      nscfst (Id.pp_nid env) nsclst (Id.NSeq.pp env) n;
    let nid = Id.NSeq.get_id env n in
    if (not exists) || NSCLocs.bounds_exist env nid nscfst nsclst then (
      NSCLocs.rm_bounds env nid nscfst nsclst;
      let c = Opt.get_exn Not_found (ConcatDom.get_cdom env nid) in
      Debug.dprintf2 debug "c = %a" (NSC.pp ~env) c;
      let id_opt, c' =
        match Id.M.find_remove nscfst c with
        | _, None -> assert false
        | c', Some m -> (
            match Id.M.find_remove nsclst m with
            | _, None ->
                (* TODO: means that it was already removed, should
                   EqHook be updated when an NSeq (var) is removed? *)
                (None, c)
            | m', Some { a = an; next } ->
                let anid = Id.NSeq.get_id env an in
                NSeq_dom.set_is_empty env anid;
                if rm_nloc then NNSCLocs.rm_loc env anid n;
                let c' =
                  if Id.M.is_empty m' then c' else Id.M.add nscfst m' c'
                in
                let c' =
                  match next with
                  | Some nxt ->
                      Egraph.merge env nxt (Id.get env nscfst);
                      c'
                  | None ->
                      let c' =
                        let _, plst =
                          nprec env ~id:nscfst (Id.get env nscfst)
                        in
                        let fsts = NSCLocs.get_fsts env plst nid in
                        Id.S.fold
                          (fun fst c ->
                            Id.M.change
                              (function
                                | None -> assert false
                                | Some m' ->
                                    Some
                                      (Id.M.change
                                         (function
                                           | None -> assert false
                                           | Some ap ->
                                               Some NSC.{ ap with next = None })
                                         plst m'))
                              fst c)
                          fsts c'
                      in
                      c'
                in
                (Some anid, c'))
      in
      Debug.dprintf2 debug "ret = %a" (Opt.pp (Id.pp_nid env)) id_opt;
      ConcatDom.set_cdom env nid c';
      id_opt)
    else
      let c = Opt.get_exn Not_found (ConcatDom.get_cdom env nid) in
      Debug.dprintf2 debug "c = %a" (NSC.pp ~env) c;
      Debug.dprintf0 debug "ret = not found";
      (* Means that the NSC was either removed or split *)
      None

  let rm_comp_aux =
    let aux_nlocs env id fst lst =
      let nlocs = NNSCLocs.get_locs env id in
      NNSCLocs.rm_id env id;
      Debug.dprintf8 debug "EqHook.rm_comp_aux_nlocs [%a; %a] in %a: {%a}"
        (Id.pp_nid env) fst (Id.pp_nid env) lst (Id.pp_nid env) id
        (fun fmt s -> Node.S.iter (Fmt.pf fmt "%a" (Id.NSeq.pp env)) s)
        nlocs;
      Node.S.iter (fun n -> ignore (remove_nsc env n fst lst)) nlocs
    in
    let aux env id =
      match Bounds.get_bounds env id with
      | None -> assert false
      | Some (fst, lst) -> aux_nlocs env id fst lst
    in
    fun env n fst lst ->
      Debug.dprintf6 debug "rm_comp_aux %a %a %a" (Id.NSeq.pp env) n
        (Id.pp_nid env) fst (Id.pp_nid env) lst;
      let id_opt = remove_nsc env ~exists:true ~rm_nloc:true n fst lst in
      match id_opt with
      | None -> ()
      | Some id -> (
          aux_nlocs env id fst lst;
          match Relocate.get_repr env id with
          | Repr m -> P.M.iter (fun _ id' -> aux env id') m
          | NonRepr { repr } -> (
              match Relocate.get_repr env repr with
              | NonRepr _ -> assert false
              | Repr m -> P.M.iter (fun _ id' -> aux env id') m))

  let rm_comp =
    let get_nlocs env s =
      Id.S.fold
        (fun id acc ->
          match Id.get env id with
          | _ ->
              let nlocs = NNSCLocs.get_locs env id in
              Node.S.fold
                (fun n acc -> Node.S.add (Egraph.find env n) acc)
                nlocs acc
          | exception Not_found -> assert false)
        s Node.M.empty
    in
    fun env f l ->
      let idset =
        match Bounds.get_locs env f with
        | None -> Id.S.empty
        | Some (m, _) -> (
            match Id.M.find_opt l m with None -> Id.S.empty | Some s -> s)
      in
      let nlocs = get_nlocs env idset in
      Debug.dprintf8 debug "EqHook.rm_comp (%a, %a): {%a}: {%a}" (Id.pp_nid env)
        f (Id.pp_nid env) l Id.S.pp idset
        (fun fmt -> Node.S.iter (Fmt.pf fmt "%a; " (Id.NSeq.pp env)))
        nlocs;
      Node.S.iter (fun nloc -> rm_comp_aux env nloc f l) nlocs

  let rm_comps env m =
    Id.M.iter (fun f lset -> Id.S.iter (fun l -> rm_comp env f l) lset) m

  let rm_comp_set env s =
    Debug.dprintf2 debug "EqHook.rm_comp_set {%a}"
      (fun fmt nts ->
        RmNSCS.iter
          (fun (f, l) ->
            Fmt.pf fmt "(%a, %a)" (Id.Index.pp env) f (Id.Index.pp env) l)
          nts)
      s;
    Debug.dprintf2 debug "%a" Bounds.pp_ht env;
    let s =
      RmNSCS.fold
        (fun (f, l) acc ->
          Id.IdP.S.add (Id.Index.get_id env f, Id.Index.get_id env l) acc)
        s Id.IdP.S.empty
    in
    Debug.dprintf2 debug "s: {%a}" Id.IdP.S.pp s;
    Id.IdP.S.iter (fun (f, l) -> rm_comp env f l) s

  let eq_indices_norm env kid rid =
    Debug.dprintf4 debug "EqHook.eq_indices_norm %a <- %a" Id.pp rid Id.pp kid;
    match HT.find_opt env rid with
    | None -> ()
    | Some rm ->
        let km =
          match HT.find_opt env kid with None -> Id.M.empty | Some m -> m
        in
        let km', rm' =
          match Id.M.find_remove rid km with
          | km', Some (s1, s2) ->
              let rm' = Id.M.remove kid rm in
              HT.change env kid ~f:(function
                | None -> assert false
                | Some m -> Some (Id.M.remove rid m));
              rm_comp_set env s1;
              MrNodes.iter (fun (n1, n2) -> Egraph.merge env n1 n2) s2;
              (km', rm')
          | _, None -> (km, rm)
        in
        let nkm =
          Id.M.fold
            (fun rnid (s1, s2) acc ->
              HT.change env rnid ~f:(function
                | None -> assert false
                | Some m ->
                    assert (Id.M.mem rid m);
                    Some
                      (Id.M.change
                         (function
                           | None -> Some (s1, s2)
                           | Some (s1', s2') ->
                               Some (RmNSCS.union s1 s1', MrNodes.union s2 s2'))
                         kid (Id.M.remove rid m)));
              Id.M.change
                (function
                  | None -> Some (s1, s2)
                  | Some (s1', s2') ->
                      Some (RmNSCS.union s1 s1', MrNodes.union s2 s2'))
                rnid acc)
            rm' km'
        in
        HT.remove env rid;
        HT.set env kid nkm
end

let compare_concdoms =
  let compare_comps env NSC.{ a = a1 } NSC.{ a = a2 } =
    let res = eq_vals env a1 a2 in
    Debug.dprintf5 debug "compare_comps %a %a = %d" (NSC.pp_atom ~env) a1
      (NSC.pp_atom ~env) a2 res;
    res
  in
  let rec aux env fst (m1 : NSC.t) (m2 : NSC.t) =
    match (Id.M.find_opt fst m1, Id.M.find_opt fst m2) with
    | None, None | Some _, None | None, Some _ -> 0
    | Some m1', Some m2' ->
        Id.M.fold2_inter
          (fun _ ap1 ap2 acc ->
            if acc <> 0 then acc
            else
              let r = compare_comps env ap1 ap2 in
              if r > 0 then
                let next =
                  match (ap1.NSC.next, ap2.NSC.next) with
                  | None, None -> None
                  | Some nf1, Some nf2 ->
                      assert (Egraph.is_equal env nf1 nf2);
                      Some (Id.Index.get_id env nf1)
                  | _ -> None (* how safe is this? *)
                in
                match next with None -> r | Some fst' -> aux env fst' m1 m2
              else r)
          m1' m2' 0
  in
  fun env fst _ id1 m1 id2 m2 ->
    let res = aux env fst m1 m2 in
    Debug.dprintf11 debug
      "compare_concdoms.aux  %a  %a  fst:%a = %d \nm1 = {%a}\nm2 = {%a}"
      (Id.pp_nid env) id1 (Id.pp_nid env) id2 (Id.pp_nid env) fst res
      (NSC.pp ~env) m1 (NSC.pp ~env) m2;
    res

let rec mk_eqs env acc l =
  match l with
  | [] -> (false, acc)
  | nsid :: t -> (
      if NSeq_dom.is_empty env nsid then (true, [])
      else
        match ConcatDom.get_cdom env nsid with
        | (exception Not_found) | None -> mk_eqs env acc t
        | Some m -> mk_eqs env ((nsid, m) :: acc) t)

let check_concdom_eq =
  let rec aux env fst lst = function
    | [] -> ()
    | (id, m) :: t ->
        let nid = Id.get env id in
        let t' =
          List.filter
            (fun (id', m') ->
              let r = compare_concdoms env fst lst id m id' m' in
              Debug.dprintf9 debug "compare_concdoms %a:{%a} %a:{%a} = %d" Id.pp
                id (NSC.pp ~env) m Id.pp id' (NSC.pp ~env) m' r;
              if r = 1 then (
                Egraph.merge env nid (Id.get env id');
                false)
              else (
                if r = -1 then make_disequal env nid (Id.get env id');
                true))
            t
        in
        aux env fst lst t'
  in
  let empty_case env nsl =
    Debug.dprintf2 debug "empty_case [%a]" (Fmt.list ~sep:Fmt.comma Id.pp) nsl;
    let nsl =
      List.filter_map
        (fun id ->
          match Id.get env id with hn -> Some hn | exception Not_found -> None)
        nsl
    in
    match nsl with
    | [] -> ()
    | hn :: t -> List.iter (fun n -> Egraph.merge env hn n) t
  in
  fun env fid lid s ->
    let nsl = Id.S.elements s in
    let is_empty, iml = mk_eqs env [] nsl in
    if is_empty then empty_case env nsl else aux env fid lid iml

let find_nsc_eqs env =
  Debug.dprintf0 debug "find_nsc_eqs";
  Debug.dprintf2 debug "%a" NSeq_dom.pp_nsht env;
  Debug.dprintf2 debug "%a" Bounds.pp_ht env;
  Bounds.iter env check_concdom_eq

let on_reloc_repr_change env ~old_repr ~repr delta =
  Debug.dprintf6 debug "NSComp_dom.on_reloc_repr_change from %a to %a with %a"
    Id.pp old_repr Id.pp repr P.pp delta;
  match ConcatDom.get_cdom env old_repr with
  | None -> ()
  | Some orcc when P.is_zero delta ->
      Id.M.iter
        (fun _ m ->
          Id.M.iter
            (fun _ { NSC.a } ->
              NNSCLocs.rm_loc env (Id.NSeq.get_id env a) (Id.get env old_repr))
            m)
        orcc;
      NSCLocs.rm_nr_nseq env old_repr;
      ConcatDom.rm_cdom env old_repr orcc;
      ConcatDom.upd_cdom env repr orcc
  | Some orcc ->
      let relocated_cc =
        ConcatDom.relocate_cc env ~upd_locs:true ~old_repr repr orcc delta
      in
      NSCLocs.rm_nr_nseq env old_repr;
      ConcatDom.rm_cdom env old_repr orcc;
      ConcatDom.upd_cdom env repr relocated_cc

let ppg_defval_ccc env _val_gty m v =
  Debug.dprintf4 debug "ppg_defval_ccc %a {%a}" Node.pp v NSC.pp m;
  Id.M.iter
    (fun _fst m' ->
      (* let nfst = Id.get env fst in *)
      Id.M.iter
        (fun _lst NSC.{ a; _ } ->
          (* let nlst = Id.get env lst in *)
          NSeq_dom.set_defval env (Id.NSeq.get_id_safe env a) v)
        m')
    m

let on_set_defval_cdom env nid v =
  (match Relocate.get_repr env nid with
  | Repr _ -> ()
  | NonRepr _ ->
      (* should be unreachable *)
      assert false);
  let n = Id.get env nid in
  match ConcatDom.get_cdom env nid with
  | None -> ()
  | Some m ->
      let val_gty = get_nseq_val_gty env n in
      ppg_defval_ccc env val_gty m v

let init_hooks env =
  NSeq_dom.set_on_set_defval_hook env on_set_defval_cdom;
  ConcatDom.register_on_set_cdom_hook env (fun env nid c ->
      match NSeq_dom.get_defval env nid with
      | None -> ()
      | Some v ->
          let val_gty = get_nseq_val_gty env (Id.get env nid) in
          ppg_defval_ccc env val_gty c v);
  Id.Index.register_merge_hook env (fun env kid rid ->
      EqHook.eq_indices_norm env kid rid;
      NSCLocs.eq_indices_norm env kid rid);
  Id.NSeq.register_merge_hook env (fun env kid rid ->
      NNSCLocs.eq_nseqs_norm env kid rid)
