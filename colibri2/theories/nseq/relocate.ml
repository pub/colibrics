(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Common
module LRA = Colibri2_theories_LRA
module DP = LRA.Dom_polynome
module P = LRA.Polynome
module NHT = Node.HC

(* x --> dx --> repr1: fst(x) = dx + fst(repr1)
   y --> dy --> repr2: fst(y) = yx + fst(repr2)
   Given a = relocate(b, first(a)):
   if b is chosen as the representative and a the non-representative then:
   an edge is added from a to b weighted with:
   a -- {first(a) - first(b)} --> b
*)
module T = struct
  type t =
    | Repr of Id.t P.M.t (* y -> c -> b *)
    | NonRepr of { repr : Id.t; dist : P.t }

  let pp fmt = function
    | Repr m -> Fmt.pf fmt "repr {%a}" (P.M.pp Id.pp) m
    | NonRepr { repr; dist } ->
        Fmt.pf fmt "nonrepr {%a; %a}" Id.pp repr P.pp dist
end

include T

module Locs = struct
  let db = NHT.create Id.S.pp "RelocateNLocsDB"

  let pp fmt env =
    Fmt.pf fmt "RelocateNLocsDB:[%a]"
      (fun fmt () ->
        NHT.iter db env ~f:(fun n s ->
            Fmt.pf fmt "\n  (%a -> {%a});" Node.pp n Id.S.pp s))
      ()
  [@@warning "-32"]

  let iter env f = NHT.iter db env ~f [@@warning "-32"]

  let add_s env n ids =
    NHT.change
      (function None -> Some ids | Some s -> Some (Id.S.fold Id.S.add ids s))
      db env n

  let add env n id = add_s env n (Id.S.singleton id)

  let add_d env dist id =
    let ns = P.nodes dist in
    Node.S.iter
      (fun n ->
        NHT.change
          (function
            | None -> Some (Id.S.singleton id) | Some s -> Some (Id.S.add id s))
          db env n)
      ns

  let rm_s env n ids =
    Debug.dprintf4 debug "Relocate.Locs.rm_s %a {%a}" Node.pp n Id.S.pp ids;
    NHT.change
      (function
        | None -> assert false
        | Some s ->
            let s' =
              Id.S.fold
                (fun id acc ->
                  (* assert (Id.S.mem id acc); *)
                  Id.S.remove id acc)
                ids s
            in
            if Id.S.is_empty s' then None else Some s')
      db env n

  let rm_nloc env n id = rm_s env n (Id.S.singleton id)

  let rm_d env d id =
    Debug.dprintf4 debug "Relocate.Locs.rm_d %a %a" P.pp d Id.pp id;
    let nodes = P.nodes d in
    Node.S.iter (fun n -> rm_nloc env n id) nodes

  let upd env ns kid rid =
    Debug.dprintf6 debug "Relocate.Locs.upd {%a} (%a <- %a)" Node.S.pp ns Id.pp
      rid Id.pp kid;
    Node.S.iter
      (fun n ->
        NHT.change
          (function
            | None -> assert false
            | Some s ->
                assert (Id.S.mem rid s);
                Some (Id.S.add kid (Id.S.remove rid s)))
          db env n)
      ns

  let upd_nlocs_d env d kid rid = upd env (P.nodes d) kid rid
end

module HT : sig
  val set : Egraph.wt -> Id.t -> T.t -> unit
  val find_opt : Egraph.wt -> Id.t -> T.t option
  val remove : Egraph.wt -> Id.t -> unit

  val change_repr :
    Egraph.wt ->
    Id.t ->
    ?none:(unit -> Id.t P.M.t * 'a) ->
    (Id.t P.M.t -> Id.t P.M.t * 'a) ->
    'a

  val change_repr_to_nonrepr :
    Egraph.wt -> Id.t -> (Id.t P.M.t -> Id.t * P.t) -> unit

  val change_nonrepr :
    Egraph.wt -> Id.t -> (Id.t -> P.t -> Id.t * P.t * 'a) -> 'a

  val set_precs : Egraph.wt -> Id.t -> Id.t P.M.t -> unit
  val get_precs : Egraph.wt -> Id.t -> Id.t P.M.t
  val pp : Format.formatter -> 'a Egraph.t -> unit
end = struct
  include Id.MkIHT (struct
    include T

    let name = "RelocateHT"
  end)

  let change_repr env id ?(none = fun () -> assert false) f =
    Debug.dprintf2 debug "Relocate.change_repr %a" Id.pp id;
    match find_opt env id with
    | Some (NonRepr _) -> assert false
    | None ->
        let m', r = none () in
        set env id (Repr m');
        r
    | Some (Repr m) ->
        let m', r = f m in
        set env id (Repr m');
        r

  let change_repr_to_nonrepr env id f =
    Debug.dprintf2 debug "Relocate.change_repr_to_nonrepr %a" Id.pp id;
    match find_opt env id with
    | None | Some (NonRepr _) -> assert false
    | Some (Repr m) ->
        let repr, dist = f m in
        set env id (NonRepr { repr; dist })

  let change_nonrepr env id f =
    Debug.dprintf2 debug "Relocate.change_nonrepr %a" Id.pp id;
    match find_opt env id with
    | None | Some (Repr _) -> assert false
    | Some (NonRepr { repr; dist }) ->
        let repr, dist, r = f repr dist in
        set env id (NonRepr { repr; dist });
        r

  let set_precs env id precs =
    if P.M.is_empty precs then remove env id else set env id (Repr precs)

  let get_precs env id =
    match find_opt env id with
    | None -> assert false
    | Some (NonRepr _) -> assert false
    | Some (Repr m) -> m
end

let repr_change_hooks =
  Datastructure.Push.create Fmt.nop "Relocate.repr_change_hook"

let register_repr_change_hook env
    (f : Egraph.wt -> old_repr:Id.t -> repr:Id.t -> P.t -> unit) =
  Datastructure.Push.push repr_change_hooks env f

let rec add_sbst_map id1 id2 m =
  match Id.M.find_opt id1 m with
  | None -> Id.M.add id1 id2 m
  | Some id2' -> add_sbst_map id2' id2 m

let on_repr_change env old_repr repr delta =
  (* TODO: make sure that delta is always zero if it was the representatives
     that caused the mergure of the equivalence calsses modulo relocation *)
  Debug.dprintf6 debug "Relocate.on_repr_change %a -> %a: (%a)" (Id.pp_nid env)
    old_repr (Id.pp_nid env) repr Colibri2_theories_LRA.Polynome.pp delta;
  Datastructure.Push.iter
    ~f:(fun f -> f env ~old_repr ~repr delta)
    repr_change_hooks env

let merge_ids env id1 id2 =
  Debug.dprintf4 debug "Relocate.merge %a %a" (Id.pp_nid env) id1
    (Id.pp_nid env) id2;
  match Id.get env id1 with
  | nid1 -> Egraph.merge env nid1 (Id.get env id2)
  | exception Not_found -> assert false

let merge_ids_m env m =
  Debug.dprintf2 debug "Relocate.merge_m {%a}" (Id.M.pp Id.pp) m;
  Id.M.iter (fun id r -> merge_ids env id r) m

let new_non_repr env src dst dist =
  HT.change_repr env dst
    ~none:(fun () -> (P.M.singleton dist src, ()))
    (fun m ->
      match P.M.find_opt dist m with
      | None ->
          HT.set env src (NonRepr { repr = dst; dist });
          Locs.add_d env dist src;
          (P.M.add dist src m, ())
      | Some src' when Id.equal src' src -> (m, ())
      | Some src' ->
          merge_ids env src src';
          (m, ()))

let merge_polys env p1 p2 =
  (* TODO: is [p1 - p2 = 0] better than [p1 = p2]? *)
  let p = P.sub p1 p2 in
  let pn = DP.node_of_polynome env p in
  Egraph.register env pn;
  Egraph.merge env pn LRA.RealValue.zero

let poly_is_zero env d =
  let d_node = DP.node_of_polynome env d in
  Egraph.register env d_node;
  Egraph.merge env d_node LRA.RealValue.zero

let precs_merge env d id1 id2 =
  if Id.equal id1 id2 then Some id2
  else (
    merge_ids env id1 id2;
    Locs.rm_d env d id1;
    HT.remove env id1;
    Some id2)

let add_new_nonrepr env id d m acc =
  match P.M.find_opt d m with
  | None -> (P.M.add d id m, acc)
  | Some id' ->
      assert (not (Id.equal id id'));
      Locs.rm_d env d id;
      HT.remove env id;
      (m, Id.M.add id id' acc)

let point_to_new_repr env old_rm new_rm old_repr new_repr dist_delta =
  Debug.dprintf10 debug "point_to_new_repr  {%a}  {%a}  %a  %a  %a"
    (P.M.pp Id.pp) old_rm (P.M.pp Id.pp) new_rm Id.pp old_repr Id.pp new_repr
    P.pp dist_delta;
  P.M.fold
    (fun idist iid acc ->
      let new_dist = P.add idist dist_delta in
      let idist_nodes = P.nodes idist in
      let ndnodes = P.nodes new_dist in
      let removed_idist_nodes =
        Node.S.filter (fun idn -> not (Node.M.mem idn ndnodes)) idist_nodes
      in
      let new_idist_nodes =
        Node.S.filter (fun ndn -> not (Node.M.mem ndn idist_nodes)) ndnodes
      in
      Node.S.iter (fun n -> Locs.rm_nloc env n iid) removed_idist_nodes;
      Node.S.iter (fun n -> Locs.add env n iid) new_idist_nodes;
      HT.change_nonrepr env iid (fun repr _ ->
          assert (Id.equal repr old_repr);
          (new_repr, new_dist, ()));
      let acc, subst = add_new_nonrepr env iid new_dist acc Id.M.empty in
      Id.M.iter (fun id1 id2 -> ignore @@ merge_ids env id1 id2) subst;
      acc)
    old_rm new_rm

(* r1 = relocate(r2, d) *)
let repr_vs_repr env r1 m1 r2 m2 dist =
  Debug.dprintf10 debug "repr_vs_repr %a {%a} %a {%a} %a" Id.pp r1
    (P.M.pp Id.pp) m1 Id.pp r2 (P.M.pp Id.pp) m2 P.pp dist;
  let r1, m1, r2, m2, dist =
    if P.M.cardinal m2 >= P.M.cardinal m1 then (r1, m1, r2, m2, dist)
    else (r2, m2, r1, m1, P.neg dist)
  in
  (* repr change for r1 with a delta of dist *)
  on_repr_change env r1 r2 dist;
  HT.change_repr_to_nonrepr env r1 (fun _ -> (r2, dist));
  let dist_nodes = P.nodes dist in
  Node.S.iter
    (fun n ->
      NHT.change
        (function
          | None -> Some (Id.S.singleton r1) | Some s -> Some (Id.S.add r1 s))
        Locs.db env n)
    dist_nodes;
  let nm2 = point_to_new_repr env m1 (P.M.add dist r1 m2) r1 r2 dist in
  HT.change_repr env r2 (fun m ->
      (* TODO: not ideal, repeats things that are done during the call to
                 [point_to_new_repr]. *)
      (P.M.union (fun d id1 id2 -> precs_merge env d id1 id2) m nm2, ()));
  if P.is_zero dist then merge_ids env r1 r2

(* dxy = fst(x) - fst(y); x -- dxy --> y
   nonrepr to repr: fst(x) - dxy = fst(y)
   repr to nonrepr: fst(y) + dxy = fst(x)
*)
let add_reloc_rel env x y dxy =
  Debug.dprintf6 debug "Relocate.add_reloc_rel %a %a: %a" (Id.pp_nid env) x
    (Id.pp_nid env) y P.pp dxy;
  if Id.equal x y then
    assert (
      (* TODO: merge with zero if it is different from it? *)
      P.is_zero dxy)
  else if P.is_zero dxy then merge_ids env x y
  else
    match (HT.find_opt env x, HT.find_opt env y) with
    | None, None ->
        (* repr_change: x -> y: dxy *)
        HT.set env x (NonRepr { repr = y; dist = dxy });
        HT.set env y (Repr (P.M.singleton dxy x));
        Node.S.iter
          (fun n ->
            NHT.change
              (function
                | None -> Some (Id.S.singleton x)
                | Some s -> Some (Id.S.add x s))
              Locs.db env n)
          (P.nodes dxy);
        on_repr_change env x y dxy
    | None, Some (Repr _) ->
        (* repr_change: x -> y: dxy *)
        new_non_repr env x y dxy;
        on_repr_change env x y dxy
    | Some (Repr _), None ->
        (* repr_change: y -> x: (neg dxy) *)
        let ndxy = P.neg dxy in
        new_non_repr env y x ndxy;
        on_repr_change env y x ndxy
    | Some (NonRepr { repr; dist }), None ->
        (* x = relocate(repr, dist)
           x = relocate(y, dxy)
           relocate(y, dxy) = relocate(repr, dist)
           y = relocate(repr, dist - dxy)
           -> repr_change: y -> repr: (dist - dxy) *)
        let dxy' = P.sub dist dxy in
        new_non_repr env y repr dxy';
        on_repr_change env y repr dxy';
        if P.is_zero dxy' then merge_ids env y repr
    | None, Some (NonRepr { repr; dist }) ->
        let dxy' = P.add dist dxy in
        (* repr_change: x -> repr: (dist + dxy) *)
        new_non_repr env x repr dxy';
        on_repr_change env x repr dxy';
        if P.is_zero dxy' then merge_ids env x repr
    | Some (Repr xprecs), Some (Repr yprecs) ->
        repr_vs_repr env x xprecs y yprecs dxy
        (* repr_change: x -> y: (dxy) or
           repr_change: y -> x: (- dxy) *)
    | Some (NonRepr { repr = r1 }), Some (NonRepr { repr = r2 })
      when Id.equal r1 r2 ->
        ()
        (* TODO: might want to propagate equality between the polynomes of the
           distance to the representive of one of the two non-representatives
           changes by taking into account the distance between them [dxy] *)
        (* TODO: if dist = 0 then they are equal, would a decision help? *)
    | ( Some (NonRepr { repr = rx; dist = dx }),
        Some (NonRepr { repr = ry; dist = dy }) ) ->
        (* x = relocate (y, dxy)
           x = relocate (rx, dx)
           y = relocate (ry, dy)
           -> relocate (relocate (ry, dy), dxy) = relocate (rx, dx)
           -> relocate (ry, dy + dxy) = relocate (rx, dx)
           -> rx = relocate (ry, dy + dxy - dx) *)
        let xprecs = HT.get_precs env rx in
        let yprecs = HT.get_precs env ry in
        let rxy = P.sub (P.add dy dxy) dx in
        repr_vs_repr env rx xprecs ry yprecs rxy
        (* repr_change: x -> y: (dy + dxy - dx) or
           repr_change: y -> x: (- (dy + dxy - dx)) *)
    | Some (Repr _), Some (NonRepr { repr = ry }) when Id.equal ry x ->
        ()
        (* TODO: check if distances are equal, and make them if they aren't. *)
    | Some (NonRepr { repr = rx }), Some (Repr _) when Id.equal rx y ->
        ()
        (* TODO: check if distances are equal, and make them if they aren't. *)
    | Some (Repr xprecs), Some (NonRepr { repr = ry; dist = dy }) ->
        (* x = rx
           x = relocate (y, dxy)
           y = relocate (ry, dy)
           -> x = relocate (relocate (ry, dy), dxy)
           -> x = relocate (ry, dy + dxy) *)
        let yprecs = HT.get_precs env ry in
        let rxy = P.add dy dxy in
        repr_vs_repr env x xprecs ry yprecs rxy
        (* repr_change: x -> y: (dy + dxy) or
           repr_change: y -> x: (- (dy + dxy)) *)
    | Some (NonRepr { repr = rx; dist = dx }), Some (Repr yprecs) ->
        (* x = relocate (rx, dx)
           x = relocate (y, dxy)
           y = ry
           -> relocate (rx, dx) = relocate (y, dxy)
           -> rx = relocate (y, dxy - dx) *)
        let xprecs = HT.get_precs env rx in
        let rxy = P.sub dxy dx in
        (* repr_change: x -> y: (dxy - dy) or
           repr_change: y -> x: (dy - dxy) *)
        repr_vs_repr env rx xprecs y yprecs rxy

let get_poly env x =
  let dx, cx = DP.get_proxy env x in
  P.of_list cx [ (dx, A.one) ]

(* x = relocate (y, xfst): d = first(x) - first(y) *)
let new_relocate env x xfst y yfst =
  (* TODO: polynomes domain vs proxys? *)
  let px = get_poly env xfst in
  let py = get_poly env yfst in
  let dxy = P.sub px py in
  let subst = add_reloc_rel env x y dxy in
  subst

let rm_non_repr env r rid d =
  HT.remove env rid;
  Locs.rm_d env d rid;
  HT.change_repr env r (fun m ->
      match P.M.find_remove d m with
      | _, None -> assert false
      | m', Some rid' ->
          assert (Id.equal rid rid');
          (m', ()))

let eq_nseqs_norm env kid rid =
  Debug.dprintf4 debug "Relocate.eq_nseqs_norm %a <- %a" Id.pp rid Id.pp kid;
  match (HT.find_opt env kid, HT.find_opt env rid) with
  | None, None | Some (Repr _), None -> on_repr_change env rid kid P.zero
  | Some (NonRepr { repr; dist }), None -> on_repr_change env rid repr dist
  | None, Some (Repr rprecs) ->
      P.M.iter
        (fun _ pns ->
          HT.change_nonrepr env pns (fun repr dist ->
              assert (Id.equal repr rid);
              (kid, dist, ())))
        rprecs;
      HT.remove env rid;
      HT.set_precs env kid rprecs;
      on_repr_change env rid kid P.zero
  | None, Some (NonRepr { repr; dist }) ->
      HT.change_repr env repr (fun m ->
          ( P.M.change
              (function
                | None -> assert false
                | Some id ->
                    assert (Id.equal id rid);
                    Some kid)
              dist m,
            () ));
      Locs.upd_nlocs_d env dist kid rid;
      HT.remove env rid;
      HT.set env kid (NonRepr { repr; dist });
      on_repr_change env kid repr dist
  | Some (Repr kprecs), Some (Repr rprecs) ->
      P.M.iter
        (fun _ rpn ->
          HT.change_nonrepr env rpn (fun repr dist ->
              assert (Id.equal repr rid);
              (kid, dist, ())))
        rprecs;
      HT.remove env rid;
      let nprecs =
        P.M.union (fun d id1 id2 -> precs_merge env d id1 id2) kprecs rprecs
      in
      HT.set_precs env kid nprecs;
      on_repr_change env rid kid P.zero
      (* repr change, from rid to kid, but they get merged so nothing needs to
         be done *)
  | Some (Repr kprecs), Some (NonRepr { repr = r2; dist = d2 })
    when Id.equal r2 kid ->
      HT.remove env rid;
      let kprecs' = P.M.remove d2 kprecs in
      HT.set_precs env kid kprecs';
      Locs.rm_d env d2 rid;
      poly_is_zero env d2
      (* no repr change for rid *)
  | Some (NonRepr { repr = r1; dist = d1 }), Some (Repr rprecs)
    when Id.equal r1 rid ->
      HT.remove env rid;
      let kprecs' = P.M.remove d1 rprecs in
      Locs.rm_d env d1 kid;
      P.M.iter
        (fun d id ->
          HT.change_nonrepr env id (fun r d' ->
              assert (Id.equal r rid);
              assert (P.equal d d');
              (kid, d', ())))
        kprecs';
      HT.set_precs env kid kprecs';
      (* let d1n = DP.node_of_polynome env d1 in
         Egraph.register env d1n;
         Egraph.merge env d1n LRA.RealValue.zero; *)
      on_repr_change env rid kid P.zero
  | ( Some (NonRepr { repr = r1; dist = d1 }),
      Some (NonRepr { repr = r2; dist = d2 }) )
    when Id.equal r1 r2 ->
      rm_non_repr env r2 rid d2;
      merge_polys env d1 d2 (* same as the previous one  *)
  | Some (Repr kprecs), Some (NonRepr { repr = r2; dist = d2 }) ->
      (* rid = relocate(r2, d2) <- kid
         - xi = relocate(kid, di)
         - xi = relocate(relocate(r2, d2), di)
         - xi = relocate(r2, d2 + di) +-*)
      let r2precs = HT.get_precs env r2 in
      let r2precs' =
        P.M.change
          (function
            | None -> assert false
            | Some oid ->
                assert (Id.equal oid rid);
                Some kid)
          d2 r2precs
      in
      HT.remove env rid;
      HT.set env kid (NonRepr { repr = r2; dist = d2 });
      Locs.upd_nlocs_d env d2 kid rid;
      let nr2precs = point_to_new_repr env kprecs r2precs' kid r2 d2 in
      HT.set_precs env r2 nr2precs;
      on_repr_change env kid r2 d2
      (* repr change for kid, which is no longer a repr, but a nonrepr for r2
         at a distance of d2 *)
  | Some (NonRepr { repr = r1; dist = d1 }), Some (Repr rprecs) ->
      (* rid <- kid = relocate(r1, d1)
         - xi = relocate(rid, di)
         - xi = relocate(relocate(r1, d1), di)
         - xi = relocate(r1, d1 + di) *)
      let kprecs = HT.get_precs env r1 in
      let nkprecs = point_to_new_repr env rprecs kprecs rid r1 d1 in
      HT.remove env rid;
      HT.set_precs env r1 nkprecs;
      on_repr_change env rid r1 d1
      (* repr change for rid, which becomes a nonrepr for r1 at a distance of
         d1 *)
  | ( Some (NonRepr { repr = r1; dist = d1 }),
      Some (NonRepr { repr = r2; dist = d2 }) ) ->
      (* kid = rid
         kid = relocate(r1, d1)
         rid = relocate(r2, d2)
         -> relocate(r1, d1) = relocate(r2, d2)
         -> r1 = relocate(r2, d2 - d1) *)
      let precs1 = HT.get_precs env r1 in
      let precs2 = HT.get_precs env r2 in
      let d = P.sub d2 d1 in
      let precs2' =
        P.M.change
          (function
            | None -> assert false
            | Some rid' ->
                assert (Id.equal rid rid');
                None)
          d2 precs2
      in
      HT.remove env rid;
      Locs.rm_d env d2 rid;
      (* repr change for either r1 or r2, whichever is chosen to no longer be
         a representative *)
      repr_vs_repr env r1 precs1 r2 precs2' d

let upd_dist_locs env id dist dist' =
  let dnodes = P.nodes dist in
  let dnodes' = P.nodes dist' in
  let removed_nodes =
    Node.S.filter (fun dn -> not (Node.S.mem dn dnodes')) dnodes
  in
  let added_nodes =
    Node.S.filter (fun dn -> not (Node.S.mem dn dnodes)) dnodes'
  in
  Node.S.iter (fun dn -> Locs.rm_nloc env dn id) removed_nodes;
  Node.S.iter (fun dn -> Locs.add env dn id) added_nodes

let subst_dist env id r dist dist' acc =
  Debug.dprintf8 debug "Relocate.subst_dist %a (repr: %a) (%a <- %a)"
    (Id.pp_nid env) id (Id.pp_nid env) r P.pp dist P.pp dist';
  if P.is_zero dist' then (
    Locs.rm_d env dist id;
    HT.remove env id;
    HT.change_repr env r (fun m ->
        match P.M.find_remove dist m with
        | _, None -> assert false
        | m', Some id' ->
            assert (Id.equal id id');
            (m', ()));
    (add_sbst_map id r acc, false))
  else
    HT.change_repr env r (fun m ->
        match P.M.find_remove dist m with
        | _, None -> assert false
        | m', Some id' -> (
            assert (Id.equal id id');
            match P.M.find_opt dist' m' with
            | None ->
                upd_dist_locs env id dist dist';
                (P.M.add dist' id m', (acc, true))
            | Some id' ->
                assert (not (Id.equal id id'));
                Locs.rm_d env dist id;
                HT.remove env id;
                (P.M.remove dist m, (add_sbst_map id id' acc, false))))

let subst_nonrepr_dist env ?(none = fun () -> Id.M.empty) id f acc =
  match HT.find_opt env id with
  | None -> none ()
  | Some (Repr _) -> assert false
  | Some (NonRepr { dist; repr }) ->
      let dist' = f dist in
      let acc, res = subst_dist env id repr dist dist' acc in
      if res then HT.set env id (NonRepr { dist = dist'; repr });
      acc

let on_lra_repr_change env =
  LRA.Dom_polynome.attach_any_repr_change env (fun env ~old_repr ~repr ~delta ->
      Debug.dprintf6 debug "Relocate.on_lra_repr_change1 %a: %a + (%a)"
        (Id.Index.pp env) old_repr (Id.Index.pp env) repr A.pp delta;
      match NHT.find_opt Locs.db env old_repr with
      | None -> ()
      | Some s ->
          Debug.dprintf6 debug "Relocate.on_lra_repr_change2 %a: %a + (%a)"
            (Id.Index.pp env) old_repr (Id.Index.pp env) repr A.pp delta;
          let m =
            Id.S.fold
              (fun id acc ->
                subst_nonrepr_dist env
                  ~none:(fun () -> assert false)
                  id
                  (fun dist ->
                    fst
                      (P.subst dist old_repr
                         (P.of_list delta [ (repr, A.one) ])))
                  acc)
              s Id.M.empty
          in
          merge_ids_m env m)

let get_repr env i =
  match HT.find_opt env i with
  | None ->
      Debug.dprintf2 debug "Relocate.get_repr1 %a is repr" Id.pp i;
      Repr P.M.empty
  | Some (Repr m) ->
      Debug.dprintf2 debug "Relocate.get_repr2 %a is repr" Id.pp i;
      Repr m
  | Some (NonRepr { repr; dist }) when P.is_zero dist -> (
      Debug.dprintf6 debug "Relocate.get_repr2 %a -> (%a, %a)" Id.pp i Id.pp
        repr P.pp dist;
      match HT.find_opt env repr with
      | Some (Repr m) -> Repr m
      | _ -> assert false)
  | Some (NonRepr { repr; dist }) ->
      Debug.dprintf6 debug "Relocate.get_repr3 %a -> (%a, %a)" Id.pp i Id.pp
        repr P.pp dist;
      NonRepr { repr; dist }

let is_eq_mod_reloc env id1 id2 =
  Id.equal id1 id2
  ||
  match (HT.find_opt env id1, HT.find_opt env id2) with
  | None, _ | _, None | Some (Repr _), Some (Repr _) -> false
  | Some (Repr _), Some (NonRepr { repr }) -> Id.equal repr id1
  | Some (NonRepr { repr }), Some (Repr _) -> Id.equal repr id2
  | Some (NonRepr { repr = r1 }), Some (NonRepr { repr = r2 }) -> Id.equal r1 r2

let eqclass_mod_reloc env id =
  match HT.find_opt env id with
  | None -> P.M.empty
  | Some (Repr m) -> m
  | Some (NonRepr { repr; dist = odist }) -> (
      match HT.find_opt env repr with
      | None | Some (NonRepr _) -> assert false
      | Some (Repr m) ->
          let m' = P.M.remove odist m in
          P.M.fold
            (fun dist id acc -> P.M.add (P.sub dist odist) id acc)
            m'
            (P.M.singleton (P.neg odist) repr))

let fold_class env f id acc =
  match HT.find_opt env id with
  | None -> acc
  | Some (Repr m) -> P.M.fold f m (f P.zero id acc)
  | Some (NonRepr { repr; dist = odist }) -> (
      match HT.find_opt env repr with
      | None | Some (NonRepr _) -> assert false
      | Some (Repr m) ->
          let m = P.M.remove odist m in
          let acc = f P.zero id acc in
          let acc = f (P.neg odist) repr acc in
          P.M.fold (fun dist id -> f (P.sub dist odist) id) m acc)

(* i -> i - ofst + nfst *)
let relocate_ind env i ofst nfst =
  let ind = mk_add env (mk_sub env i ofst) nfst in
  (* TODO: instead of using ids, use a patricia tree of polynomes, do not create
     terms that are not necessary. *)
  Egraph.register env ind;
  let indid = Id.Index.get_id_safe env ind in
  Debug.dprintf8 debug "relocate_ind %a + (%a - %a) = %a" (Id.Index.pp env) nfst
    (Id.Index.pp env) i (Id.Index.pp env) ofst (Id.Index.pp env) ind;
  indid

let reloc_by_delta env i delta =
  if P.is_zero delta then (i, Id.Index.get_id_safe env i)
  else
    match DP.get_repr env i with
    | Some kp ->
        let kp' = P.add kp delta in
        let rind = DP.node_of_polynome env kp' in
        Egraph.register env rind;
        let rindid = Id.Index.get_id_safe ~debug env rind in
        (rind, rindid)
    | None ->
        let nd = DP.node_of_polynome env delta in
        let rind = mk_add env i nd in
        Egraph.register env rind;
        let rindid = Id.Index.get_id_safe ~debug env rind in
        (rind, rindid)

let pp_locs fmt env =
  Locs.iter env (fun n _ ->
      Fmt.pf fmt "%a: [ %a | %a | %a ]@." Node.pp n Node.pp (Egraph.find env n)
        (Fmt.option A.pp)
        (Colibri2_theories_LRA.RealValue.get env n)
        (fun fmt (n, x) -> Fmt.pf fmt "%a + (%a)" Node.pp n A.pp x)
        (DP.get_proxy env n))

let pp_ht = HT.pp

let on_merge env ~repr:rn n =
  match NHT.find_opt Locs.db env n with
  | None -> ()
  | Some locs ->
      let rnp = get_poly env rn in
      Debug.dprintf6 debug "Relocate.on_merge %a -> %a (%a)" Node.pp n Node.pp
        rn P.pp rnp;
      let m =
        Id.S.fold
          (fun id acc ->
            subst_nonrepr_dist env
              ~none:(fun () -> acc)
              id
              (fun dist -> fst (P.subst dist n rnp))
              acc)
          locs Id.M.empty
      in
      merge_ids_m env m

let add_dn env d ?dnode n =
  match DP.get_repr env n with
  | Some kp ->
      let kp' = P.add kp d in
      let inode' = DP.node_of_polynome env kp' in
      Egraph.register env inode';
      inode'
  | None ->
      let dn = Option.value ~default:(DP.node_of_polynome env d) dnode in
      let inode' = mk_add env n dn in
      Egraph.register env inode';
      inode'

let reloc_nsv_by_delta =
  let ( let+ ) = Interp.Seq.( let+ ) in
  let ( and* ) = Interp.Seq.( and* ) in
  fun env (interp : Egraph.wt -> Node.t -> Value.t Interp.Seq.t) val_gty dist
      ?dnode (nsv : NSeqValue.nsval) lower_bound_n upper_bound_n ->
    let res =
      let+ lower_bound = interp env lower_bound_n
      and* upper_bound = interp env upper_bound_n in
      let dn = Option.value ~default:(DP.node_of_polynome env dist) dnode in
      let+ vals =
        Value.M.fold
          (fun idv vv (acc : Value.t Value.M.t Interp.Seq.t) ->
            let inode = Value.node idv in
            let ivn = add_dn env dist ~dnode:dn inode in
            let acc =
              let+ iv = interp env ivn and* acc = acc in
              try Value.M.add_new Exit iv vv acc with Exit -> acc
            in
            acc)
          nsv.vals
          (Interp.Seq.return Value.M.empty)
      in
      NSeqValue.{ val_gty; vals; lower_bound; upper_bound; other = None }
    in
    Interp.Seq.concat res
