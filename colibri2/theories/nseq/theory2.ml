(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_core
open Colibri2_popop_lib
open Common
open NSComp_info
open Colibri2_theories_quantifiers
module Pattern = Colibri2_theories_quantifiers.Pattern
module InvertedPath = Colibri2_theories_quantifiers.InvertedPath
module RealValue = Colibri2_theories_LRA.RealValue
module P = Colibri2_theories_LRA.Polynome
module ConcatDom = NSComp_dom.ConcatDom

(* TODO:
   - As last effort, for every value node or nseq node that has in one of its
   domains an nseq.get or an nseq.set on a list of nseq components, create
   decisions that determine to which one of those it belongs.
   - As last effort, try comapre the `NSCDom`s of nodes which can be equal
   - in `match_atom`, when a variable v1 is replaced by a variable v2, update
   the locs of v1
*)

let ext_right =
  let ext_right_1 env a c fstid clst clstid lst lstid =
    if Egraph.is_equal env clst lst then
      (* TODO: remove this fix,
         due to bounds difference between nonrepr and repr *)
      Egraph.merge env a c
    else
      let fk2, fk2id = nsucc env ~id:clstid clst in
      let k2 = NSComp_dom.fresh_var_id env a fk2 lst in
      NSComp_dom.EqHook.add_rmnsc_hook env clstid lstid (fk2, lst);
      ConcatDom.upd_dom' env a [ (c, fstid, clstid); (k2, fk2id, lstid) ]
  in
  let ext_right_2_1 env b cfst cfstid clst clstid alst alstid =
    let fk2, fk2id = nsucc env ~id:clstid clst in
    let k1 = NSComp_dom.fresh_var_id env b cfst clst in
    let k2 = NSComp_dom.fresh_var_id env b fk2 alst in
    NSComp_dom.EqHook.add_rmnsc_hook env clstid alstid (fk2, alst);
    ConcatDom.upd_dom' env b [ (k1, cfstid, clstid); (k2, fk2id, alstid) ];
    (k2, fk2, fk2id)
  in
  let ext_right_2_2 env a c k2 afstid clstid fk2 fk2id alst alstid =
    NSComp_dom.EqHook.add_rmnsc_hook env clstid alstid (fk2, alst);
    ConcatDom.upd_dom' env a [ (c, afstid, clstid); (k2, fk2id, alstid) ]
  in
  let ext_right_2 env a b c fstid cfst cfstid clst clstid lst lstid =
    let k2, fk2, fk2id =
      ext_right_2_1 env b cfst cfstid clst clstid lst lstid
    in
    ext_right_2_2 env a c k2 fstid clstid fk2 fk2id lst lstid
  in
  fun env val_gty a ?b c fst fstid cfst cfstid clst clstid lst lstid ->
    Debug.dprintf14 debug "ext_right %a (%a) [%a; %a] %a [%a; %a]"
      (Id.NSeq.pp env) a
      (Fmt.option (Id.NSeq.pp env))
      b (Id.Index.pp env) fst (Id.Index.pp env) lst (Id.NSeq.pp env) c
      (Id.Index.pp env) cfst (Id.Index.pp env) clst;
    match b with
    | None -> (
        let aid = Id.NSeq.get_id env a in
        match Relocate.get_repr env aid with
        | Repr _ -> ext_right_1 env a c fstid clst clstid lst lstid
        | NonRepr { repr; dist } ->
            let dist = P.neg dist in
            let a' = Id.get env repr in
            let _, fstid', lst', lstid' = Bounds.get_mk_bounds_id env repr in
            let c', _, _, clst', clstid' =
              NSComp_dom.relocate_node_wbounds env ~val_gty c cfst cfstid clst
                clstid dist
            in
            ext_right_1 env a' c' fstid' clst' clstid' lst' lstid')
    | Some b -> (
        let aid = Id.NSeq.get_id env a in
        let bid = Id.NSeq.get_id env b in
        match Relocate.get_repr env aid with
        | Repr _ -> (
            match Relocate.get_repr env bid with
            | Repr _ ->
                ext_right_2 env a b c fstid cfst cfstid clst clstid lst lstid
            | NonRepr { repr = repr_b; dist = dist_b } ->
                let dist_b = P.neg dist_b in
                let b' = Id.get env repr_b in
                let _, _, lst', lstid' = Bounds.get_mk_bounds_id env repr_b in
                let cfst', cfstid' = Relocate.reloc_by_delta env cfst dist_b in
                let clst', clstid' = Relocate.reloc_by_delta env clst dist_b in
                let k2', fk2', _ =
                  ext_right_2_1 env b' cfst' cfstid' clst' clstid' lst' lstid'
                in
                let fk2'', fk2id'' =
                  Relocate.reloc_by_delta env fk2' (P.neg dist_b)
                in
                let _, _, k2'' =
                  NSComp_dom.relocate_atom env k2' fk2' fk2'' lst
                in
                ext_right_2_2 env a c k2'' fstid clstid fk2'' fk2id'' lst lstid)
        | NonRepr { repr = repr_a; dist = dist_a } -> (
            let dist_a = P.neg dist_a in
            match Relocate.get_repr env bid with
            | Repr _ ->
                let k2, fk2, _ =
                  ext_right_2_1 env b cfst cfstid clst clstid lst lstid
                in
                let a' = Id.get env repr_a in
                let _, fstid', lst', lstid' =
                  Bounds.get_mk_bounds_id env repr_a
                in
                let c', _, _, _, clstid' =
                  NSComp_dom.relocate_node_wbounds env ~val_gty c cfst cfstid
                    clst clstid dist_a
                in
                let fk2', fk2id' = Relocate.reloc_by_delta env fk2 dist_a in
                let _, _, k2' = NSComp_dom.relocate_atom env k2 fk2 fk2' lst' in
                ext_right_2_2 env a' c' k2' fstid' clstid' fk2' fk2id' lst'
                  lstid'
            | NonRepr { repr = repr_b; dist = dist_b } ->
                let dist_b = P.neg dist_b in
                let b' = Id.get env repr_b in
                let cfst', cfstid' = Relocate.reloc_by_delta env cfst dist_b in
                let clst', clstid' = Relocate.reloc_by_delta env clst dist_b in
                let _, _, lst', lstid' = Bounds.get_mk_bounds_id env repr_b in
                let k2', fk2', _ =
                  ext_right_2_1 env b' cfst' cfstid' clst' clstid' lst' lstid'
                in
                let a'' = Id.get env repr_a in
                let _, fstid'', lst'', lstid'' =
                  Bounds.get_mk_bounds_id env repr_a
                in
                let c'', _, _, _, clstid'' =
                  NSComp_dom.relocate_node_wbounds env ~val_gty c cfst cfstid
                    clst clstid dist_a
                in
                let fk2'', fk2id'' =
                  Relocate.reloc_by_delta env fk2' (P.sub dist_a dist_b)
                in
                let _, _, k2'' =
                  NSComp_dom.relocate_atom env k2' fk2' fk2'' lst
                in
                ext_right_2_2 env a'' c'' k2'' fstid'' clstid'' fk2'' fk2id''
                  lst'' lstid''))

let ext_left =
  let ext_left_1 env a c fst fstid cfst cfstid lstid =
    let lk1, lk1id = nprec env ~id:cfstid cfst in
    let k1 = NSComp_dom.fresh_var_id env a fst lk1 in
    NSComp_dom.EqHook.add_rmnsc_hook env fstid cfstid (fst, lk1);
    ConcatDom.upd_dom' env a [ (k1, fstid, lk1id); (c, cfstid, lstid) ]
  in
  let ext_left_2_1 env b fst fstid cfst cfstid clst lstid =
    let lk1, lk1id = nprec env ~id:cfstid cfst in
    let k1 = NSComp_dom.fresh_var_id env b fst lk1 in
    let k2 = NSComp_dom.fresh_var_id env b cfst clst in
    NSComp_dom.EqHook.add_rmnsc_hook env fstid cfstid (fst, lk1);
    ConcatDom.upd_dom' env b [ (k1, fstid, lk1id); (k2, cfstid, lstid) ];
    (k1, lk1, lk1id)
  in
  let ext_left_2_2 env a c k1 fst fstid lk1 lk1id cfstid lstid =
    NSComp_dom.EqHook.add_rmnsc_hook env fstid cfstid (fst, lk1);
    ConcatDom.upd_dom' env a [ (k1, fstid, lk1id); (c, cfstid, lstid) ]
  in
  let ext_left_2 env a b c fst fstid cfst cfstid clst lstid =
    let k1, lk1, lk1id = ext_left_2_1 env b fst fstid cfst cfstid clst lstid in
    ext_left_2_2 env a c k1 fst fstid lk1 lk1id cfstid lstid
  in
  fun env val_gty a ?b c fst fstid cfst cfstid clst clstid lst lstid ->
    Debug.dprintf14 debug "ext_left %a (%a) [%a; %a] %a [%a; %a]"
      (Id.NSeq.pp env) a
      (Fmt.option (Id.NSeq.pp env))
      b (Id.Index.pp env) fst (Id.Index.pp env) lst (Id.NSeq.pp env) c
      (Id.Index.pp env) cfst (Id.Index.pp env) clst;
    match b with
    | None -> (
        let aid = Id.NSeq.get_id env a in
        match Relocate.get_repr env aid with
        | Repr _ -> ext_left_1 env a c fst fstid cfst cfstid lstid
        | NonRepr { repr; dist } ->
            let dist = P.neg dist in
            let a' = Id.get env repr in
            let fst', fstid', _, lstid' = Bounds.get_mk_bounds_id env repr in
            let c', cfst', cfstid', _, _ =
              NSComp_dom.relocate_node_wbounds env ~val_gty c cfst cfstid clst
                clstid dist
            in
            ext_left_1 env a' c' fst' fstid' cfst' cfstid' lstid')
    | Some b -> (
        let aid = Id.NSeq.get_id env a in
        let bid = Id.NSeq.get_id env b in
        match Relocate.get_repr env aid with
        | Repr _ -> (
            match Relocate.get_repr env bid with
            | Repr _ -> ext_left_2 env a b c fst fstid cfst cfstid clst lstid
            | NonRepr { repr = repr_b; dist = dist_b } ->
                let dist_b = P.neg dist_b in
                let b' = Id.get env repr_b in
                let cfst', cfstid' = Relocate.reloc_by_delta env cfst dist_b in
                let clst', _ = Relocate.reloc_by_delta env clst dist_b in
                let fst', fstid', _, lstid' =
                  Bounds.get_mk_bounds_id env repr_b
                in
                let k1', lk1', _ =
                  ext_left_2_1 env b' fst' fstid' cfst' cfstid' clst' lstid'
                in
                let lk1'', lk1id'' =
                  Relocate.reloc_by_delta env lk1' (P.neg dist_b)
                in
                let _, _, k1'' =
                  NSComp_dom.relocate_atom env k1' fst' fst lk1''
                in
                ext_left_2_2 env a c k1'' fst fstid lk1'' lk1id'' cfstid lstid)
        | NonRepr { repr = repr_a; dist = dist_a } -> (
            let dist_a = P.neg dist_a in
            match Relocate.get_repr env bid with
            | Repr _ ->
                let k1, lk1, _ =
                  ext_left_2_1 env b fst fstid cfst cfstid clst lstid
                in
                let a' = Id.get env repr_a in
                let lk1', lk1id' = Relocate.reloc_by_delta env lk1 dist_a in
                let _, _, k1' = NSComp_dom.relocate_atom env k1 fst lk1 lk1' in
                let fst', fstid', _, lstid' =
                  Bounds.get_mk_bounds_id env repr_a
                in
                let c', _, cfstid', _, _ =
                  NSComp_dom.relocate_node_wbounds env ~val_gty c cfst cfstid
                    clst clstid dist_a
                in
                ext_left_2_2 env a' c' k1' fst' fstid' lk1' lk1id' cfstid'
                  lstid'
            | NonRepr { repr = repr_b; dist = dist_b } ->
                let dist_b = P.neg dist_b in
                let b' = Id.get env repr_b in
                let cfst', cfstid' = Relocate.reloc_by_delta env cfst dist_b in
                let clst', _ = Relocate.reloc_by_delta env clst dist_b in
                let fst', fstid', _, lstid' =
                  Bounds.get_mk_bounds_id env repr_b
                in
                let k1', lk1', _ =
                  ext_left_2_1 env b' fst' fstid' cfst' cfstid' clst' lstid'
                in
                let a' = Id.get env repr_a in
                let fst'', fstid'', _, lstid'' =
                  Bounds.get_mk_bounds_id env repr_a
                in
                let lk1'', lk1id'' =
                  Relocate.reloc_by_delta env lk1' (P.sub dist_a dist_b)
                in
                let c'', _, cfstid'', _, _ =
                  NSComp_dom.relocate_node_wbounds env ~val_gty c cfst cfstid
                    clst clstid dist_a
                in
                let _, _, k1'' =
                  NSComp_dom.relocate_atom env k1' fst' fst lk1''
                in
                ext_left_2_2 env a' c'' k1'' fst'' fstid'' lk1'' lk1id''
                  cfstid'' lstid''))

let ext_left_right =
  let ext_left_right_1 env a c fst fstid cfst cfstid clst clstid lst lstid =
    (* TODO: remove this fix,
       due to bounds difference between nonrepr and repr *)
    if Egraph.is_equal env lst clst then (
      if Egraph.is_equal env fst cfst then Egraph.merge env a c
      else
        let lk1, lk1id = nprec env ~id:cfstid cfst in
        let k1 = NSComp_dom.fresh_var_id env a fst lk1 in
        NSComp_dom.EqHook.add_rmnsc_hook env fstid cfstid (fst, lk1);
        ConcatDom.upd_dom' env a [ (k1, fstid, lk1id); (c, cfstid, clstid) ])
    else if Egraph.is_equal env fst cfst then (
      let fk3, fk3id = nsucc env ~id:clstid clst in
      let k3 = NSComp_dom.fresh_var_id env a fk3 lst in
      NSComp_dom.EqHook.add_rmnsc_hook env lstid clstid (fk3, lst);
      ConcatDom.upd_dom' env a [ (c, cfstid, clstid); (k3, fk3id, lstid) ])
    else
      let lk1, lk1id = nprec env ~id:cfstid cfst in
      let k1 = NSComp_dom.fresh_var_id env a fst lk1 in
      let fk3, fk3id = nsucc env ~id:clstid clst in
      let k3 = NSComp_dom.fresh_var_id env a fk3 lst in
      NSComp_dom.EqHook.add_rmnsc_hook env fstid cfstid (fst, lk1);
      NSComp_dom.EqHook.add_rmnsc_hook env lstid clstid (fk3, lst);
      ConcatDom.upd_dom' env a
        [ (k1, fstid, lk1id); (c, cfstid, clstid); (k3, fk3id, lstid) ]
  in
  let ext_left_right_2_1 env b fst fstid cfst cfstid clst clstid lst lstid =
    let lk1, lk1id = nprec env ~id:cfstid cfst in
    let k1 = NSComp_dom.fresh_var_id env b fst lk1 in
    let k2 = NSComp_dom.fresh_var_id env b cfst clst in
    let fk3, fk3id = nsucc env ~id:clstid clst in
    let k3 = NSComp_dom.fresh_var_id env b fk3 lst in
    NSComp_dom.EqHook.add_rmnsc_hook env fstid cfstid (fst, lk1);
    NSComp_dom.EqHook.add_rmnsc_hook env lstid clstid (fk3, lst);
    ConcatDom.upd_dom' env b
      [ (k1, fstid, lk1id); (k2, cfstid, clstid); (k3, fk3id, lstid) ];
    (k1, lk1, lk1id, k3, fk3, fk3id)
  in
  let ext_left_right_2_2 env a c k1 k3 fst fstid lk1 lk1id cfstid clstid fk3
      fk3id lst lstid =
    NSComp_dom.EqHook.add_rmnsc_hook env fstid cfstid (fst, lk1);
    NSComp_dom.EqHook.add_rmnsc_hook env lstid clstid (fk3, lst);
    ConcatDom.upd_dom' env a
      [ (k1, fstid, lk1id); (c, cfstid, clstid); (k3, fk3id, lstid) ]
  in
  let ext_left_right_2 env a b c fst fstid cfst cfstid clst clstid lst lstid =
    let k1, lk1, lk1id, k3, fk3, fk3id =
      ext_left_right_2_1 env b fst fstid cfst cfstid clst clstid lst lstid
    in
    ext_left_right_2_2 env a c k1 k3 fst fstid lk1 lk1id cfstid clstid fk3 fk3id
      lst lstid
  in
  fun env val_gty a ?b c fst fstid cfst cfstid clst clstid lst lstid ->
    Debug.dprintf14 debug "ext_left_right %a (%a) [%a; %a] %a [%a; %a]"
      (Id.NSeq.pp env) a
      (Fmt.option (Id.NSeq.pp env))
      b (Id.Index.pp env) fst (Id.Index.pp env) lst (Id.NSeq.pp env) c
      (Id.Index.pp env) cfst (Id.Index.pp env) clst;
    match b with
    | None -> (
        let aid = Id.NSeq.get_id env a in
        match Relocate.get_repr env aid with
        | Repr _ ->
            ext_left_right_1 env a c fst fstid cfst cfstid clst clstid lst lstid
        | NonRepr { repr; dist } ->
            let dist = P.neg dist in
            let a' = Id.get env repr in
            let fst', fstid', lst', lstid' = Bounds.get_mk_bounds_id env repr in
            let c', cfst', cfstid', clst', clstid' =
              NSComp_dom.relocate_node_wbounds env ~val_gty c cfst cfstid clst
                clstid dist
            in
            ext_left_right_1 env a' c' fst' fstid' cfst' cfstid' clst' clstid'
              lst' lstid')
    | Some b -> (
        let aid = Id.NSeq.get_id env a in
        let bid = Id.NSeq.get_id env b in
        match Relocate.get_repr env aid with
        | Repr _ -> (
            match Relocate.get_repr env bid with
            | Repr _ ->
                ext_left_right_2 env a b c fst fstid cfst cfstid clst clstid lst
                  lstid
            | NonRepr { repr = repr_b; dist = dist_b } ->
                let dist_b = P.neg dist_b in
                let b' = Id.get env repr_b in
                let cfst', cfstid' = Relocate.reloc_by_delta env cfst dist_b in
                let clst', clstid' = Relocate.reloc_by_delta env clst dist_b in
                let fst', fstid', lst', lstid' =
                  Bounds.get_mk_bounds_id env repr_b
                in
                let k1', lk1', _, k3', fk3', _ =
                  ext_left_right_2_1 env b' fst' fstid' cfst' cfstid' clst'
                    clstid' lst' lstid'
                in
                let lk1'', lk1id'' =
                  Relocate.reloc_by_delta env lk1' (P.neg dist_b)
                in
                let fk3'', fk3id'' =
                  Relocate.reloc_by_delta env fk3' (P.neg dist_b)
                in
                let _, _, k1'' =
                  NSComp_dom.relocate_atom env k1' fst' fst lk1''
                in
                let _, _, k3'' =
                  NSComp_dom.relocate_atom env k3' fk3' fk3'' lst
                in
                ext_left_right_2_2 env a c k1'' k3'' fst fstid lk1'' lk1id''
                  cfstid clstid fk3'' fk3id'' lst lstid)
        | NonRepr { repr = repr_a; dist = dist_a } -> (
            let dist_a = P.neg dist_a in
            match Relocate.get_repr env bid with
            | Repr _ ->
                let k1, lk1, _, k3, fk3, _ =
                  ext_left_right_2_1 env b fst fstid cfst cfstid clst clstid lst
                    lstid
                in
                let fst', fstid', lst', lstid' =
                  Bounds.get_mk_bounds_id env repr_a
                in
                (* let cfst', cfstid' = Relocate.reloc_by_delta env cfst dist_a in
                   let clst', clstid' = Relocate.reloc_by_delta env clst dist_a in *)
                let lk1', lk1id' = Relocate.reloc_by_delta env lk1 dist_a in
                let fk3', fk3id' = Relocate.reloc_by_delta env fk3 dist_a in
                let _, _, k1' = NSComp_dom.relocate_atom env k1 fst fst' lk1' in
                let _, _, k3' = NSComp_dom.relocate_atom env k3 fk3 fk3' lst' in
                let a' = Id.get env repr_a in
                let c', _, cfstid', _, clstid' =
                  NSComp_dom.relocate_node_wbounds env ~val_gty c cfst cfstid
                    clst clstid dist_a
                in
                ext_left_right_2_2 env a' c' k1' k3' fst' fstid' lk1' lk1id'
                  cfstid' clstid' fk3' fk3id' lst' lstid'
            | NonRepr { repr = repr_b; dist = dist_b } ->
                let dist_b = P.neg dist_b in
                let b' = Id.get env repr_b in
                let cfst', cfstid' = Relocate.reloc_by_delta env cfst dist_b in
                let clst', clstid' = Relocate.reloc_by_delta env clst dist_b in
                let fst', fstid', lst', lstid' =
                  Bounds.get_mk_bounds_id env repr_b
                in
                let k1', lk1', _, k3', fk3', _ =
                  ext_left_right_2_1 env b' fst' fstid' cfst' cfstid' clst'
                    clstid' lst' lstid'
                in
                let lk1'', lk1id'' =
                  Relocate.reloc_by_delta env lk1' (P.sub dist_a dist_b)
                in
                let fk3'', fk3id'' =
                  Relocate.reloc_by_delta env fk3' (P.sub dist_a dist_b)
                in
                let fst'', fstid'', lst'', lstid'' =
                  Bounds.get_mk_bounds_id env repr_a
                in
                let _, _, k1'' =
                  NSComp_dom.relocate_atom env k1' fst' fst lk1''
                in
                let _, _, k3'' =
                  NSComp_dom.relocate_atom env k3' fk3' fk3'' lst
                in
                let a' = Id.get env repr_a in
                let c', _, cfstid'', _, clstid'' =
                  NSComp_dom.relocate_node_wbounds env ~val_gty c cfst cfstid
                    clst clstid dist_a
                in
                ext_left_right_2_2 env a' c' k1'' k3'' fst'' fstid'' lk1''
                  lk1id'' cfstid'' clstid'' fk3'' fk3id'' lst'' lstid''))

let split_cc env val_gty a ?b fst fstid lst lstid c cfst cfstid clst clstid =
  Debug.dprintf14 debug "split_cc %a (%a) [%a; %a] %a [%a; %a]" (Id.NSeq.pp env)
    a
    (Fmt.option (Id.NSeq.pp env))
    b (Id.Index.pp env) fst (Id.Index.pp env) lst (Id.NSeq.pp env) c
    (Id.Index.pp env) cfst (Id.Index.pp env) clst;
  (* if Egraph.is_equal env fst lst then
       (* TODO: make sure that the precondition that c is not empty
          is always held *)
       Egraph.merge env a c
     else *)
  if Egraph.is_equal env fst cfst then
    if Egraph.is_equal env clst lst then Egraph.merge env a c
    else
      ext_right env val_gty a ?b c fst fstid cfst cfstid clst clstid lst lstid
  else if Egraph.is_equal env clst lst then
    ext_left env val_gty a ?b c fst fstid cfst cfstid clst clstid lst lstid
  else
    ext_left_right env val_gty a ?b c fst fstid cfst cfstid clst clstid lst
      lstid

let nseq_concat =
  let nseq_concat_aux env val_gty n a afst alst b bfst blst =
    Debug.dprintf14 debug "nseq_concat_aux %a = concat(%a[%a; %a], %a[%a; %a])"
      (Id.NSeq.pp env) n (Id.NSeq.pp env) a (Id.Index.pp env) afst
      (Id.Index.pp env) alst (Id.NSeq.pp env) b (Id.Index.pp env) bfst
      (Id.Index.pp env) blst;
    let nid = Id.NSeq.get_id env n in
    let afstid = Id.Index.get_id env afst in
    let alstid = Id.Index.get_id env alst in
    let bfstid = Id.Index.get_id env bfst in
    let blstid = Id.Index.get_id env blst in
    match Relocate.get_repr env nid with
    | Repr _ ->
        Bounds.upd_bounds env n afst blst;
        ConcatDom.upd_dom' env n [ (a, afstid, alstid); (b, bfstid, blstid) ]
    | NonRepr { repr; dist } ->
        let dist = P.neg dist in
        let n' = Id.get env repr in
        let a', afst', afstid', _, alstid' =
          NSComp_dom.relocate_node_wbounds env ~val_gty a afst afstid alst
            alstid dist
        in
        let b', _, bfstid', blst', blstid' =
          NSComp_dom.relocate_node_wbounds env ~val_gty b bfst bfstid blst
            blstid dist
        in
        Bounds.upd_bounds env n' afst' blst';
        ConcatDom.upd_dom' env n'
          [ (a', afstid', alstid'); (b', bfstid', blstid') ]
  in
  fun env val_gty f n args ->
    NSeq_value.propagate_value env f;
    let a, b = IArray.extract2_exn args in
    Egraph.register env a;
    Egraph.register env b;
    let aid = NSeq_dom.new_nseq env a in
    if NSeq_dom.is_empty env aid then Egraph.merge env n b
    else (
      ignore (NSeq_dom.new_nseq env b);
      let afst, _, alst, _ = Bounds.get_mk_bounds_n env ~val_gty a in
      let bfst, _, blst, _ = Bounds.get_mk_bounds_n env ~val_gty b in
      let a_isnt_empty = mk_le env [ afst; alst ] in
      Egraph.register env a_isnt_empty;
      let b_isnt_empty = mk_le env [ bfst; blst ] in
      Egraph.register env b_isnt_empty;
      let cond =
        Equality.equality env [ mk_add env alst RealValue.one; bfst ]
      in
      Egraph.register env cond;
      let cons env =
        Bounds.upd_bounds env n afst blst;
        (* TODO: reasoning over bounds should be better than this, it should
           detect on its own the simplification. *)
        if Egraph.is_equal env n a then
          NSeq_dom.set_is_empty env (Id.NSeq.get_id env b)
        else if Egraph.is_equal env n b then
          NSeq_dom.set_is_empty env (Id.NSeq.get_id env a)
        else (
          Debug.dprintf2 debug "%a" Bounds.pp_ht env;
          nseq_concat_aux env val_gty n a afst alst b bfst blst)
      in
      let a_is_empty env =
        NSeq_dom.set_is_empty env (Id.NSeq.get_id env a);
        Egraph.merge env n b
      in
      let b_is_empty env =
        NSeq_dom.set_is_empty env (Id.NSeq.get_id env b);
        Egraph.merge env n a
      in
      let n_eq_a env = Egraph.merge env n a in
      new_decision env a_isnt_empty "Decision from Theory2.NSeq_concat 1"
        ~cons:(fun env ->
          new_decision env b_isnt_empty "Decision from Theory2.NSeq_concat 2"
            ~cons:(fun env ->
              new_decision env cond "Decision from Theory2.NSeq_concat 3" ~cons
                ~alt:n_eq_a (fun _ -> bool_dec cond ~cons ~alt:n_eq_a))
            ~alt:b_is_empty
            (fun _ ->
              DecTodo
                [
                  (fun env ->
                    Boolean.set_false env b_isnt_empty;
                    b_is_empty env);
                  (fun env ->
                    Boolean.set_true env b_isnt_empty;
                    Boolean.set_false env cond;
                    n_eq_a env);
                  (fun env ->
                    Boolean.set_true env b_isnt_empty;
                    Boolean.set_true env cond;
                    cons env);
                ]))
        ~alt:a_is_empty
        (fun _ ->
          DecTodo
            [
              (fun env ->
                Boolean.set_false env a_isnt_empty;
                a_is_empty env);
              (fun env ->
                Boolean.set_true env a_isnt_empty;
                Boolean.set_false env b_isnt_empty;
                b_is_empty env);
              (fun env ->
                Boolean.set_true env a_isnt_empty;
                Boolean.set_true env b_isnt_empty;
                Boolean.set_false env cond;
                n_eq_a env);
              (fun env ->
                Boolean.set_true env a_isnt_empty;
                Boolean.set_true env b_isnt_empty;
                Boolean.set_true env cond;
                cons env);
            ]))

let nseq_slice_aux env val_gty n a f fid l lid afst alst =
  Debug.dprintf12 debug "nseq_slice_aux %a = slice(%a[%a; %a], %a, %a)"
    (Id.NSeq.pp env) n (Id.NSeq.pp env) a (Id.Index.pp env) afst
    (Id.Index.pp env) alst (Id.Index.pp env) f (Id.Index.pp env) l;
  let nid = Id.NSeq.get_id env n in
  let aid = Id.NSeq.get_id env a in
  if Relocate.is_eq_mod_reloc env nid aid then Egraph.merge env n a
  else
    let afstid = Id.Index.get_id env afst in
    let alstid = Id.Index.get_id env alst in
    split_cc env val_gty a afst afstid alst alstid n f fid l lid

let nseq_slice env val_gty f n args =
  NSeq_value.propagate_value env f;
  let a, f, l = IArray.extract3_exn args in
  Egraph.register env a;
  Egraph.register env f;
  Egraph.register env l;
  let aid = NSeq_dom.new_nseq env a in
  if NSeq_dom.is_empty env aid then Egraph.merge env n a
  else
    let afst, afstid, alst, alstid = Bounds.get_mk_bounds_n env ~val_gty a in
    let nfst, _, nlst, _ = Bounds.get_mk_bounds_n env ~val_gty n in
    let fid = Id.Index.get_id_safe env f in
    let lid = Id.Index.get_id_safe env l in
    (* TODO: are these "add_mrnodes_hook" necessary? *)
    NSComp_dom.EqHook.add_mrnodes_hook env fid afstid (nfst, afst);
    NSComp_dom.EqHook.add_mrnodes_hook env lid alstid (nlst, alst);
    let c1 = mk_le env [ afst; f ] in
    let c2 = mk_le env [ l; alst ] in
    let cond = Boolean._and env [ c1; c2 ] in
    let c3 = mk_le env [ f; l ] in
    Egraph.register env c3;
    Egraph.register env cond;
    let cons env =
      let fid, lid = Bounds.upd_bounds_ret env n f l in
      if not (Egraph.is_equal env n a) then
        let nfid = Id.get env fid in
        let nlid = Id.get env lid in
        nseq_slice_aux env val_gty n a nfid fid nlid lid afst alst
    in
    let alt1 env = Egraph.merge env n a in
    let alt2 env =
      Bounds.upd_bounds env n f l;
      NSeq_dom.set_is_empty env (Id.NSeq.get_id_safe env n)
    in
    new_decision env cond "Decision from Theory2.NSeq_slice 1"
      ~cons:(fun env ->
        new_decision env c3 "Decision from Theory2.NSeq_slice 2" ~cons ~alt:alt2
          (fun _ -> bool_dec ~cons ~alt:alt2 c3))
      ~alt:alt1
      (fun env ->
        match Boolean.is env c3 with
        | Some true ->
            DecTodo
              [
                (fun env ->
                  Boolean.set_true env cond;
                  cons env);
                (fun env ->
                  Boolean.set_false env cond;
                  alt1 env);
              ]
        | Some false ->
            alt1 env;
            DecNo
        | None ->
            DecTodo
              [
                (fun env ->
                  Boolean.set_true env cond;
                  Boolean.set_true env c3;
                  cons env);
                (fun env ->
                  Boolean.set_true env cond;
                  Boolean.set_false env c3;
                  alt2 env);
                (fun env ->
                  Boolean.set_false env cond;
                  alt1 env);
              ])

let nseq_update_aux env val_gty a b afst alst c cfst clst =
  Debug.dprintf14 debug "nseq_update_aux %a[%a; %a] = update(%a, %a[%a; %a])"
    (Id.NSeq.pp env) a (Id.Index.pp env) afst (Id.Index.pp env) alst
    (Id.NSeq.pp env) b (Id.NSeq.pp env) c (Id.Index.pp env) cfst
    (Id.Index.pp env) clst;
  let afstid = Id.Index.get_id env afst in
  let alstid = Id.Index.get_id env alst in
  let cfstid = Id.Index.get_id env cfst in
  let clstid = Id.Index.get_id env clst in
  split_cc env val_gty a ~b afst afstid alst alstid c cfst cfstid clst clstid

let nseq_update env val_gty f n args =
  NSeq_value.propagate_value env f;
  let ns1, ns2 = IArray.extract2_exn args in
  Egraph.register env ns1;
  Egraph.register env ns2;
  let nid1 = NSeq_dom.new_nseq env ns1 in
  let nid2 = NSeq_dom.new_nseq env ns2 in
  if NSeq_dom.is_empty env nid1 || NSeq_dom.is_empty env nid2 then
    Egraph.merge env n ns1
  else
    let nfst1, _, nlst1, _ = Bounds.get_mk_bounds_n env ~val_gty ns1 in
    Bounds.upd_bounds env n nfst1 nlst1;
    let nfst2, _, nlst2, _ = Bounds.get_mk_bounds_n env ~val_gty ns2 in
    let cond =
      Boolean._and env
        [ mk_le env [ nfst1; nfst2 ]; mk_le env [ nlst2; nlst1 ] ]
    in
    let ns2_not_empty = mk_le env [ nfst2; nlst2 ] in
    Egraph.register env cond;
    Egraph.register env ns2_not_empty;
    let cons env =
      if Egraph.is_equal env nfst1 nlst1 then
        (* TODO: avoid using this workaround, its only used because
           if nfst1 = nfst2 and nfst1 <= nfst2 <= nlst2 <= nlst1
           we can't infer that nfst1 = nfst2 = nlst2 = nlst1
           to reproduce, remove ite and test with
           smt_seq/sat/test84.smt2 using --nseq-ext
        *)
        Egraph.merge env n ns2
      else nseq_update_aux env val_gty n ns1 nfst1 nlst1 ns2 nfst2 nlst2
    in
    let ns2_is_empty env =
      NSeq_dom.set_is_empty env (Id.NSeq.get_id env ns2);
      Egraph.merge env n ns1
    in
    let alt env = Egraph.merge env n ns1 in
    new_decision env cond "Decision from Theory2.NSeq_update 1"
      ~cons:(fun env ->
        new_decision env ns2_not_empty "Decision from Theory2.NSeq_update 2"
          ~cons ~alt:ns2_is_empty (fun _ ->
            bool_dec ~cons ~alt:ns2_is_empty ns2_not_empty))
      ~alt
      (fun env ->
        match Boolean.is env ns2_not_empty with
        | Some true -> bool_dec ~cons ~alt cond
        | Some false ->
            alt env;
            DecNo
        | None ->
            DecTodo
              [
                (fun env ->
                  Boolean.set_true env cond;
                  Boolean.set_true env ns2_not_empty;
                  cons env);
                (fun env ->
                  Boolean.set_true env cond;
                  Boolean.set_false env ns2_not_empty;
                  ns2_is_empty env);
                (fun env ->
                  Boolean.set_false env cond;
                  alt env);
              ])

let init_callbacks env =
  Relocate.register_repr_change_hook env NSComp_dom.on_reloc_repr_change;
  Id.Index.register_merge_hook env (fun env kid rid ->
      NSComp_dom.EqHook.eq_indices_norm env kid rid);
  Common_conv.init_hooks env;
  (* order is important for the first one *)
  NSComp_dom.init_hooks env

module Base = struct
  let nseq_set_aux env val_gty n a nfst nlst i v =
    Debug.dprintf12 debug "nseq_set_aux %a = set(%a[%a; %a], %a, %a)"
      (Id.NSeq.pp env) n (Id.NSeq.pp env) a (Id.Index.pp env) nfst
      (Id.Index.pp env) nlst (Id.Index.pp env) i Node.pp v;
    let nfstid = Id.Index.get_id env nfst in
    let nlstid = Id.Index.get_id env nlst in
    let const_ns = mk_nseq_const env val_gty i i v in
    Egraph.register env const_ns;
    add_nseq_gty env const_ns val_gty;
    let iid = Id.Index.get_id_safe env i in
    split_cc env val_gty n ~b:a nfst nfstid nlst nlstid const_ns i iid i iid

  let nseq_get_aux env val_gty n a afst alst i =
    Debug.dprintf10 debug "nseq_get_aux %a = get(%a[%a; %a], %a)" Node.pp n
      (Id.NSeq.pp env) a (Id.Index.pp env) afst (Id.Index.pp env) alst
      (Id.Index.pp env) i;
    let afstid = Id.Index.get_id env afst in
    let alstid = Id.Index.get_id env alst in
    let const_ns = mk_nseq_const env val_gty i i n in
    Egraph.register env const_ns;
    add_nseq_gty env const_ns val_gty;
    let iid = Id.Index.get_id_safe env i in
    split_cc env val_gty a afst afstid alst alstid const_ns i iid i iid

  let add_new_nsget env val_gty str aid a i n =
    Debug.dprintf6 debug "add_new_nsget %a = get(%a, %a)" Node.pp n
      (Id.NSeq.pp env) a (Id.Index.pp env) i;
    if NSeq_dom.is_empty env aid then ()
    else
      let nfst, _, nlst, _ = Bounds.get_mk_bounds_n env ~val_gty a in
      let cond = mk_in_bounds env i nfst nlst in
      Egraph.register env cond;
      let cons env = nseq_get_aux env val_gty n a nfst nlst i in
      new_decision env cond str ~cons (fun _ -> bool_dec ~cons cond)

  let converter env f =
    let s = Ground.sem f in
    let n = Ground.node f in
    match s with
    | { app = { builtin = Builtin.NSeq_set }; args; tyargs = [ val_gty ] } ->
        NSeq_value.propagate_value env f;
        let a, i, v = IArray.extract3_exn args in
        Egraph.register env a;
        Egraph.register env i;
        Egraph.register env v;
        let aid = NSeq_dom.new_nseq env a in
        if NSeq_dom.is_empty env aid then Egraph.merge env a n
        else
          let afst, _, alst, _ = Bounds.get_mk_bounds_n env ~val_gty a in
          Bounds.upd_bounds env n afst alst;
          let cond = mk_in_bounds env i afst alst in
          Egraph.register env cond;
          let cons env = nseq_set_aux env val_gty n a afst alst i v in
          let alt env = Egraph.merge env a n in
          new_decision env cond "Decision from Theory2.Base.NSeq_set" ~cons ~alt
            (fun _ -> bool_dec ~cons ~alt cond)
    | { app = { builtin = Builtin.NSeq_get }; args; tyargs = [ val_gty ] } ->
        NSeq_value.propagate_value env f;
        let a, i = IArray.extract2_exn args in
        Egraph.register env a;
        Egraph.register env i;
        let aid = NSeq_dom.new_nseq env a in
        add_new_nsget env val_gty "Decision from Theory2.Base.NSeq_get" aid a i
          n
    | { app = { builtin = Builtin.NSeq_concat }; args; tyargs = [ val_gty ] } ->
        nseq_concat env val_gty f n args
    | { app = { builtin = Builtin.NSeq_update }; args; tyargs = [ val_gty ] } ->
        nseq_update env val_gty f n args
    | { app = { builtin = Builtin.NSeq_slice }; args; tyargs = [ val_gty ] } ->
        nseq_slice env val_gty f n args
    | _ -> ()

  let th_register env =
    Colibri2_theories_utils.Nsa_content.set_new_arr_kv_hook env
      (fun env aid i v ->
        Debug.dprintf6 debug "NSeq.Base.Nsa_content.new_arr_kv %a %a %a" Id.pp
          aid Node.pp i Node.pp v;
        let a = Id.get env aid in
        let val_gty = get_nseq_val_gty env a in
        add_new_nsget env val_gty
          "Decision from Theory2.Base.Nsa_content.new_arr_kv_hook" aid a i v);
    init_callbacks env;
    DaemonLastEffortLateUncontextual.schedule_immediately env
      NSComp_dom.find_nsc_eqs;
    Ground.register_converter env converter;
    ConcatDom.register_on_first_set_cdom_hook env ConcatDom.subst_n_dom
end

(* Get-Split *)
(* Is this necessary? *)
let new_nseq_get =
  let module GHT = Datastructure.Hashtbl (Ground.Ty) in
  let db_gty = GHT.create (Node.M.pp Node.S.pp) "known_nseq_gets" in
  fun env val_gty n i ->
    let n = Egraph.find env n in
    GHT.change
      (function
        | None -> Some (Node.M.singleton i (Node.S.singleton n))
        | Some m ->
            let m' =
              Node.M.mapi
                (fun i' ns ->
                  let c = Egraph.is_equal env i i' in
                  let ns' =
                    Node.S.filter
                      (fun n' ->
                        if Egraph.is_equal env n n' then false
                        else (
                          if c then (
                            let eq = mk_eq env n n' in
                            Egraph.register env eq;
                            Choice.register_global env (Boolean.chobool eq));
                          true))
                      ns
                  in
                  Node.S.add n ns')
                m
            in
            Some m')
      db_gty env val_gty

module Ext = struct
  (* Set-Unit *)
  let update_unit_pat, update_unit_run =
    let const_term =
      Expr.Term.apply_cst Builtin.nseq_const [ Builtin.val_ty ]
        [ Builtin.nsi_t; Builtin.nsi_t; Builtin.nsv_t ]
    in
    let set_term =
      mk_nseq_set_term Builtin.val_ty const_term Builtin.nsi_t Builtin.nsv'_t
    in
    let pattern = Pattern.of_term_exn set_term in
    let run env subst =
      Debug.dprintf4 debug
        "Set-Unit: Found nseq_set on nseq_const term (%a) with %a" Expr.Term.pp
        set_term Ground.Subst.pp subst;
      let a = Subst.convert ~subst_new:subst env const_term in
      Egraph.register env a;
      let nconst =
        Expr.Term.apply_cst Builtin.nseq_const [ Builtin.val_ty ]
          [ Builtin.nsi_t; Builtin.nsi_t; Builtin.nsv'_t ]
      in
      let eq = Expr.Term.eq set_term nconst in
      let n = Subst.convert ~subst_new:subst env eq in
      Egraph.register env n;
      Boolean.set_true env n
    in
    (pattern, run)

  (* Set-Bound *)
  (* handled by NSeq_dom *)
  (* let set_bound env n a aget v =
     let cond = mk_eq env aget v in
     Egraph.register env cond;
     let cons env = Egraph.merge env n a in
     let alt env =
       let diseq = Equality.disequality env [ n; a ] in
       Egraph.register env diseq;
       Boolean.set_true env diseq
     in
     new_decision env cond "Decision from Theory2.Ext.Update_bound 1" ~cons ~alt
       (fun _ -> bool_dec cond ~cons ~alt) *)

  (* Set-Concat *)
  let set_concat env n a val_gty fst ca i v =
    Debug.dprintf10 debug "set_concat [%a; _] %a = %a [%a <- %a]"
      (Id.Index.pp env) fst (Id.NSeq.pp env) n (Id.NSeq.pp env) a
      (Id.Index.pp env) i Node.pp v;
    ConcatDom.apply_nth env
      (fun env ns afst alst ?next cn ->
        let nset = mk_nseq_set env val_gty ns i v in
        Egraph.register env nset;
        add_nseq_gty env nset val_gty;
        ConcatDom.update cn afst alst NSC.{ a = nset; next }
        (* let fst = Id.get env afst in
           let lst = Id.get env alst in
           let id' = NSComp_dom.fresh_var_id env n fst lst in
           (* NSVLocs.add_cloc env id' fst lst n; *)
           (* NSSHT.add_nsset env id' id i v; *)
           let nset = mk_nseq_set env val_gty id i v in
           Egraph.register env nset;
           Egraph.merge env ns nset;
           (* Store info : nc = set(c, i, v) *)
           ConcatDom.update cn afst alst NSC.{ a = NSeq id'; next } *))
      fst ca i ca

  (* Set-Concat-Inv *)
  let set_concat_inv env n a val_gty nfst cn i v =
    Debug.dprintf10 debug "set_concat_inv [%a; _] %a = %a[%a <- %a]"
      (Id.Index.pp env) nfst (Id.NSeq.pp env) n (Id.NSeq.pp env) a
      (Id.Index.pp env) i Node.pp v;
    ConcatDom.apply_nth env
      (fun env ns afst alst ?next cn ->
        let id' =
          NSComp_dom.fresh_var_id env a (Id.get env afst) (Id.get env alst)
        in
        (let nset = mk_nseq_set env val_gty id' i v in
         Egraph.register env nset;
         Egraph.merge env ns nset);
        let cn = ConcatDom.update cn afst alst NSC.{ a = id'; next } in
        (* Store info : c.v = set(id', i, v) *)
        cn)
      nfst cn i cn

  (* Get-Concat *)
  let get_concat env aid ca i v =
    Debug.dprintf6 debug "get_concat %a[%a] = %a" (Id.pp_nid env) aid
      (Id.Index.pp env) i Node.pp v;
    Id.M.iter
      (fun _fst t' ->
        (* let nfst = Id.get env fst in *)
        Id.M.iter
          (fun _lst ap ->
            NSeq_dom.add_kv_check_bounds env (Id.NSeq.get_id env ap.NSC.a) i v)
          t')
      ca

  let converter env f =
    let s = Ground.sem f in
    let n = Ground.node f in
    match s with
    | { app = { builtin = Builtin.NSeq_set }; args; tyargs = [ val_gty ] } ->
        NSeq_value.propagate_value env f;
        let a, i, v = IArray.extract3_exn args in
        Egraph.register env a;
        Egraph.register env i;
        Egraph.register env v;
        let aid = NSeq_dom.new_nseq env a in
        if NSeq_dom.is_empty env aid then Egraph.merge env a n
        else
          let fst, _, lst, _ = Bounds.get_mk_bounds_n env ~val_gty a in
          Bounds.upd_bounds env n fst lst;
          let cond = mk_in_bounds env i fst lst in
          Egraph.register env cond;
          let cons env =
            (* Get-Intro *)
            (* let aget = mk_nseq_get env val_gty a i in
               Egraph.register env aget; *)
            (* Set-Bound *)
            (* set_bound env n a aget v; *)
            (* Update-Concat && Update-Concat-Inv *)
            let aid = Id.Index.get_id_safe env a in
            let nid = Id.Index.get_id_safe env n in
            let nr, nrid, nri, nfst, delta_n =
              match Relocate.get_repr env nid with
              | Repr _ -> (n, nid, i, fst, P.zero)
              | NonRepr { repr; dist } ->
                  let dist = P.neg dist in
                  let i', _ = Relocate.reloc_by_delta env i dist in
                  let rfst, _ = Relocate.reloc_by_delta env fst dist in
                  (Id.get env repr, repr, i', rfst, dist)
            in
            let ar, arid, ari, afst, delta_a =
              match Relocate.get_repr env aid with
              | Repr _ -> (a, aid, i, fst, P.zero)
              | NonRepr { repr; dist } ->
                  let dist = P.neg dist in
                  let i', _ = Relocate.reloc_by_delta env i dist in
                  let rfst, _ = Relocate.reloc_by_delta env fst dist in
                  (Id.get env repr, repr, i', rfst, dist)
            in
            match
              (ConcatDom.get_cdom env nrid, ConcatDom.get_cdom env arid)
            with
            | None, None ->
                NSeq_dom.add_kv env (Id.NSeq.get_id env n)
                  ~b:(Id.NSeq.get_id env a)
                  (Id.Index.get_id_safe env i)
                  v
            | None, Some ca ->
                let ncn = set_concat env nr ar val_gty afst ca ari v in
                let ncn' =
                  match (P.is_zero delta_a, P.is_zero delta_n) with
                  | false, false -> ncn
                  | false, true -> ConcatDom.relocate_cc env nrid ncn delta_n
                  | true, false ->
                      ConcatDom.relocate_cc env nrid ncn (P.neg delta_a)
                  | true, true ->
                      ConcatDom.relocate_cc env nrid ncn (P.sub delta_n delta_a)
                in
                ConcatDom.upd_cdom env nrid ncn'
            | Some cn, None ->
                let nca = set_concat_inv env nr ar val_gty nfst cn nri v in
                let nca' =
                  match (P.is_zero delta_a, P.is_zero delta_n) with
                  | false, false -> nca
                  | false, true ->
                      ConcatDom.relocate_cc env arid nca (P.neg delta_n)
                  | true, false -> ConcatDom.relocate_cc env arid nca delta_a
                  | true, true ->
                      ConcatDom.relocate_cc env arid nca (P.sub delta_a delta_n)
                in
                ConcatDom.upd_cdom env arid nca'
            | Some cn, Some ca ->
                let ncn = set_concat env nr ar val_gty afst ca ari v in
                let nca = set_concat_inv env nr ar val_gty fst cn nri v in
                let ncn', nca' =
                  match (P.is_zero delta_a, P.is_zero delta_n) with
                  | false, false -> (ncn, nca)
                  | false, true ->
                      let ncn' = ConcatDom.relocate_cc env nrid ncn delta_n in
                      let nca' =
                        ConcatDom.relocate_cc env arid nca (P.neg delta_n)
                      in
                      (ncn', nca')
                  | true, false ->
                      let ncn' =
                        ConcatDom.relocate_cc env nrid ncn (P.neg delta_a)
                      in
                      let nca' = ConcatDom.relocate_cc env arid nca delta_a in
                      (ncn', nca')
                  | true, true ->
                      let ncn' =
                        ConcatDom.relocate_cc env nrid ncn
                          (P.sub delta_n delta_a)
                      in
                      let nca' =
                        ConcatDom.relocate_cc env arid nca
                          (P.sub delta_a delta_n)
                      in
                      (ncn', nca')
                in
                ConcatDom.upd_cdom env nrid ncn';
                ConcatDom.upd_cdom env arid nca'
          in
          let alt env = Egraph.merge env a n in
          new_decision env cond "Decision from Theory2.Ext.NSeq_set" ~cons ~alt
            (fun _ -> bool_dec ~cons ~alt cond)
    | { app = { builtin = Builtin.NSeq_get }; args; tyargs = [ val_gty ] } ->
        NSeq_value.propagate_value env f;
        let a, i = IArray.extract2_exn args in
        Egraph.register env a;
        Egraph.register env i;
        new_nseq_get env val_gty a i;
        let aid = NSeq_dom.new_nseq env a in
        if NSeq_dom.is_empty env aid then ()
        else
          let afst, _, alst, _ = Bounds.get_mk_bounds_n env ~val_gty a in
          let cond = mk_in_bounds env i afst alst in
          Egraph.register env cond;
          let cons env =
            let aid = Id.NSeq.get_id env a in
            let a, i =
              match Relocate.get_repr env aid with
              | Repr _ -> (a, i)
              | NonRepr { repr; dist } ->
                  let dist = P.neg dist in
                  let i', _ = Relocate.reloc_by_delta env i dist in
                  (Id.get env repr, i')
            in
            (* Get-Const | Get-Concat*)
            match ConcatDom.get_cdom env aid with
            | None ->
                NSeq_dom.add_kv env (Id.NSeq.get_id env a)
                  (Id.Index.get_id_safe env i)
                  n
            | Some ca -> get_concat env aid ca i n
          in
          new_decision env cond "Decision from Theory2.Ext.NSeq_get" ~cons
            (fun _ -> bool_dec ~cons cond)
    | { app = { builtin = Builtin.NSeq_update }; args; tyargs = [ val_gty ] } ->
        nseq_update env val_gty f n args
    | { app = { builtin = Builtin.NSeq_concat }; args; tyargs = [ val_gty ] } ->
        nseq_concat env val_gty f n args
    | { app = { builtin = Builtin.NSeq_slice }; args; tyargs = [ val_gty ] } ->
        nseq_slice env val_gty f n args
    | _ -> ()

  let on_set_ConcatDom env id d =
    Debug.dprintf2 debug "Th2.Ext.on_set_ConcatDom %a" (Id.pp_nid env) id;
    match NSeq_dom.get_kvs env id with
    | None -> ()
    | Some m ->
        Id.M.iter (fun i (v, _, _) -> get_concat env id d (Id.get env i) v) m

  let on_new_known_value env a i v =
    Debug.dprintf6 debug "Th2.Ext.on_new_known_value %a[%a] = %a"
      (Id.pp_nid env) a (Id.pp_nid env) i Node.pp v;
    Relocate.fold_class env
      (fun dist id () ->
        let nlocs = NSComp_dom.get_n_cdom_locs env id in
        let _, i' = Relocate.reloc_by_delta env (Id.get env i) dist in
        Node.S.iter
          (fun n ->
            let nid = Id.NSeq.get_id env n in
            NSeq_dom.add_kv env nid i' v)
          nlocs)
      a ()

  let th_register env =
    Colibri2_theories_utils.Nsa_content.set_new_arr_kv_hook env
      (fun env aid i v ->
        Debug.dprintf6 debug "NSeq.Ext.Nsa_content.new_arr_kv %a %a %a" Id.pp
          aid Node.pp i Node.pp v;
        NSeq_dom.add_kv_check_bounds env aid i v);
    init_callbacks env;
    DaemonLastEffortLateUncontextual.schedule_immediately env
      (Common_conv.check_eq_nseqs
         ~mk_gets:(not (Options.get env ns_no_get_intro)));
    DaemonLastEffortLateUncontextual.schedule_immediately env
      NSComp_dom.find_nsc_eqs;
    InvertedPath.add_callback env update_unit_pat update_unit_run;
    Ground.register_converter env converter;
    NSeq_dom.register_on_new_val_hook env on_new_known_value;
    ConcatDom.register_on_first_set_cdom_hook env ConcatDom.subst_n_dom;
    ConcatDom.register_on_first_set_cdom_hook env on_set_ConcatDom;
    Id.Index.register_merge_hook env (fun env id1 id2 ->
        WEGraph.eq_indices_norm env id1 id2);
    Id.NSeq.register_merge_hook env (fun env id1 id2 ->
        WEGraph.eq_nseqs_norm env id1 id2)

  (* TODO:
     - add on-set-dom of n, move kvs from n to components using apply_nth
     - add detect equalities
     - add relocation *)
end
