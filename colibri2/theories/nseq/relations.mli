(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_core
open Colibri2_popop_lib
open Popop_stdlib

type t = {
  (* a = nseq.concat(b, c): b -> c *)
  concat : (Id.t * DBool.t) Id.M.t;
  (* a = nseq.slice(b, _, _): a *)
  slices : Id.S.t;
  (* a = nseq.update(b, c): b -> c *)
  updates : (Id.S.t * DBool.t) Id.M.t;
}

val pp_im : (Id.t * bool) Id.M.t Pp.pp [@@warning "-32"]
val pp_sm : (Id.S.t * bool) Id.M.t Pp.pp [@@warning "-32"]
val pp : t Pp.pp [@@warning "-32"]
val add_concat : Egraph.wt -> Id.t -> Id.t -> Id.t -> unit
val add_slice : Egraph.wt -> Id.t -> Id.t -> unit
val add_update : Egraph.wt -> Id.t -> Id.t -> Id.t -> unit
val get_updates : Egraph.wt -> Id.t -> (Id.S.t * DBool.t) Id.M.t
val eq_nseqs_norm : Egraph.rw Egraph.t -> Id.t -> Id.t -> unit
val get_succs : _ Egraph.t -> Id.t -> t option
val get_precs : _ Egraph.t -> Id.t -> (Id.S.t * Id.S.t * Id.S.t) option
val on_bounds_change : Egraph.wt -> Id.t -> Id.t * Id.t -> Id.t * Id.t -> unit
val get_shared_slices : Egraph.wt -> Id.t -> Id.t Id.IdP.M.t option
val pp_shs : _ Egraph.t Pp.pp
val pp_ht : _ Egraph.t Pp.pp
