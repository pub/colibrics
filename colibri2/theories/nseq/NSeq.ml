(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_core
(* module LRA = Colibri2_theories_LRA
   module DP = LRA.Dom_polynome
   module P = LRA.Polynome *)

let ns_base =
  Options.register ~pp:Fmt.bool "NSeq.Base"
    Cmdliner.Arg.(
      value & flag
      & info [ "nseq-base" ] ~doc:"Use the Base theory of N-Sequences")

let ns_ext =
  Options.register ~pp:Fmt.bool "NSeq.Ext"
    Cmdliner.Arg.(
      value & flag
      & info [ "nseq-ext" ]
          ~doc:"Use the Ext theory of N-Sequences, takes over `nseq-base`")

let no_nseq =
  Options.register ~pp:Fmt.bool "NoNSeq"
    Cmdliner.Arg.(
      value & flag
      & info [ "no-nseq" ]
          ~doc:
            "Do not support the NSeq theory (but possibly use its solver for \
             Arrays)")

(* let dist_to_v env d = Egraph.get_value env (DP.node_of_polynome env d)
   let to_value v = LRA.RealValue.coerce_value v

   let reloc_values ?(neg = false) lb ub vals delta =
     let dv = to_value delta |> fun x -> if neg then A.neg x else x in
     let lbv = A.sub (to_value lb) dv in
     let ubv = A.sub (to_value ub) dv in
     let vals =
       Value.M.fold
         (fun k v acc ->
           let k' = LRA.RealValue.of_value (A.sub (to_value k) dv) in
           Value.M.add_new (Failure "already exists") k' v acc)
         vals Value.M.empty
     in
     (LRA.RealValue.of_value lbv, LRA.RealValue.of_value ubv, vals)
*)
let th_register env =
  if Options.get env Colibri2_theories_utils.Array_opt.array_use_nseqs then
    if Options.get env Common.array_nseqs_no_ppg then
      Ground.register_converter env NS_arrays.converter_no_ppg
    else Ground.register_converter env NS_arrays.converter;
  if not (Options.get env no_nseq) then (
    Common.Builtin.add_builtins ();
    NSeq_value.init env;
    Id.Index.register_new_id_hook env (fun env i n -> Id.set env i n);
    Id.NSeq.register_new_id_hook env (fun env i n -> Id.set env i n);
    Ground.register_converter env Common_conv.converter;
    Seq.init env;
    if Options.get env ns_ext then Theory2.Ext.th_register env
    else if Options.get env ns_base then Theory2.Base.th_register env
    else Theory1.th_register env;
    DaemonAlsoInterp.attach_reg_value env Common.NSeqValue.key (fun env v ->
        let ns = Common.NSeqValue.node v in
        let _, _, _, vals = NSeq_value.get_ns_values v in
        let nid = Id.NSeq.get_id_safe env ns in
        Value.M.iter
          (fun iv vv ->
            let i = Value.node iv in
            let v = Value.node vv in
            Egraph.register env i;
            Egraph.register env v;
            NSeq_dom.add_kv env nid (Id.Index.get_id_safe env i) v)
          vals
        (* match Relocate.get_repr env nid with
           | Repr m ->
               P.M.iter
                 (fun d ns ->
                   match dist_to_v env d with
                   | Some dv ->
                       (* does this ever work? *)
                       let lb', ub', vals' = reloc_values lbv ubv vals dv in
                       NSeq_value.set_ns_values env (Id.get env ns) lb' ub' val_gty
                         vals'
                   | None -> ())
                 m
           | NonRepr { repr; dist } -> (
               match Relocate.get_repr env repr with
               | NonRepr _ -> assert false
               | Repr m -> (
                   match dist_to_v env dist with
                   | Some dv -> (
                       let rlb, rub, rvals =
                         reloc_values ~neg:true lbv ubv vals dv
                       in
                       match P.M.find_remove dist m with
                       | _, None -> assert false
                       | m, Some _ ->
                           P.M.iter
                             (fun d ns ->
                               match dist_to_v env d with
                               | Some dv ->
                                   let lb', ub', vals' =
                                     reloc_values rlb rub rvals dv
                                   in
                                   NSeq_value.set_ns_values env (Id.get env ns) lb'
                                     ub' val_gty vals'
                               | None -> ())
                             m)
                   | None -> ())
                   ) *)))

let () = Init.add_default_theory th_register
