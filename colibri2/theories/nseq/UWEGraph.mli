(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

val register_class_merge_hook :
  Egraph.wt -> (Egraph.wt -> Id.S.t -> Id.S.t -> unit) -> unit

val add_edge : Egraph.wt -> Id.t -> Id.t -> Id.t -> unit
val eq_nseqs_norm : Egraph.wt -> Id.t -> Id.t -> unit
val eq_indices_norm : Egraph.wt -> Id.t -> Id.t -> unit
val get_repr : _ Egraph.t -> Id.t -> Id.t
val are_linked : _ Egraph.t -> Id.t -> Id.t -> bool

type path = (Id.t * Node.t * Id.t * Node.t) list

val pp_path : path Fmt.t
val pp_path_list : path list Fmt.t
val shortest_path : _ Egraph.t -> ?ex_ind:Id.t -> Id.t -> Id.S.t -> Id.t * path
val filter_paths : ('a * Id.S.t) list -> ('a * Id.S.t) list

val all_paths :
  _ Egraph.t ->
  filter:bool ->
  ?ex_ind:Id.t ->
  ?ex_arrs:Id.S.t ->
  Id.t ->
  Id.S.t ->
  path list

val last_edge : path -> Id.t * Node.t
val rev_path : path -> Id.t -> Node.t -> Id.t * Node.t * path
val filter_by_comp : _ Egraph.t -> Id.S.t -> Id.t list list
val pp : Format.formatter -> _ Egraph.t -> unit
