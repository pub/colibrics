(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_core
open Colibri2_popop_lib
open Common

(* - when a[i] <> a[j] then i <> j
   - on last step always propagate:
     - a[k] = v1, b[k] = v2, v1 <> v2, a--i--b -> k=i  if they are equal on all other values on the path
     - *)
module Arrays = struct
  module ArrGHT = Datastructure.Hashtbl (struct
    include Ground.Ty
  end)

  let ns_arrs_db = ArrGHT.create Id.S.pp "ns_arrs_db"

  let add_ns_arrs_set env id gty =
    ArrGHT.change
      (function
        | None -> Some (Id.S.singleton id) | Some s -> Some (Id.S.add id s))
      ns_arrs_db env gty

  let rm_ns_arrs_set env id gty =
    (* Add asserts when we start checking that we are removing arrays *)
    ArrGHT.change
      (function
        | None -> (* assert false *) None
        | Some s ->
            (* assert (Id.S.mem id s); *)
            Some (Id.S.remove id s))
      ns_arrs_db env gty

  let add_array_gty env n ind_gty val_gty =
    Ground.add_ty env n (Ground.Ty.array ind_gty val_gty)

  let get_array_gty env n =
    List.find
      (function
        | Ground.Ty.{ app = { builtin = Expr.Array; _ }; _ } -> true
        | _ -> false)
      (Ground.Ty.S.elements (Ground.tys env n))

  let rm_array_id env rid =
    try
      let gty = get_array_gty env (Id.get env rid) in
      rm_ns_arrs_set env rid gty
    with Not_found -> ()

  let eq_ns_arrays_norm env _ rid =
    (* check if it is an array first *)
    rm_array_id env rid

  let iter_arrays env f = ArrGHT.iter ~f:(f env) ns_arrs_db env
  let fold_arrays env f acc = ArrGHT.fold (f env) ns_arrs_db env acc

  (* let pp fmt env =
     Fmt.pf fmt "NS_arrays.db:[%a]"
       (fun fmt () ->
         ArrGHT.iter ns_arrs_db env ~f:(fun i v ->
             Fmt.pf fmt "\n  (%a -> (%a));" Ground.Ty.pp i Id.S.pp v))
       () *)
end

module Selects = struct
  let pp_node_map fmt =
    Fmt.pf fmt "{%a}"
      (Id.M.pp (fun fmt (v, b) -> Fmt.pf fmt "(%a, %b)" Node.pp v b))

  (* a -> i -> (v, isppg) *)
  module HT = Id.MkIHT (struct
    type t = (Node.t * bool) Id.M.t

    let pp = pp_node_map
    let name = "NS.Arrays.Selects.HT"
  end)

  (* i -> a -> (v, isppg) *)
  module Inds = Id.MkIHT (struct
    type t = (Node.t * bool) Id.M.t

    let pp = pp_node_map
    let name = "NS.Arrays.Selects.Inds"
  end)

  let get_val env a i =
    match HT.find_opt env a with
    | None -> None
    | Some m -> (
        match Id.M.find_opt i m with None -> None | Some (v, _) -> Some v)

  let has_distinct_val env a b =
    match (HT.find_opt env a, HT.find_opt env b) with
    | Some am, Some bm ->
        Id.M.fold2_inter
          (fun _ (v1, _) (v2, _) acc ->
            acc || Disequality.is_disequal env v1 v2)
          am bm false
    | _ -> false

  let are_holding_dist_vals env i j =
    match (Inds.find_opt env i, Inds.find_opt env j) with
    | Some im, Some jm ->
        Id.M.fold2_inter
          (fun _ (v1, _) (v2, _) acc ->
            acc || Disequality.is_disequal env v1 v2)
          im jm false
    | _ -> false

  let pp_step fmt (id1, _, id2, _) = Fmt.pf fmt "(%a, %a)" Id.pp id1 Id.pp id2

  (* Bad performance *)
  (* let link_inequalities env v1 v2 i1 i2 =
     (* TODO: also link inequalities between values and arrays *)
     let eq = Equality.equality env [ v1; v2 ] in
     Egraph.register env eq;
     Daemon.attach_value env eq Boolean.BoolValue.key (fun env _ b ->
         if not (Boolean.BoolValue.value b) then make_disequal env i1 i2) *)

  (* TODO: generalize by making sure that whenever two values a[i] and a[j]
     become disequal then i and j must become disequal too
     - Do it on normalization too
     Currently no positive impact
  *)
  (* let on_distinct_slects env inode v1 m =
     Id.M.iter
       (fun j (v2, _) ->
         let jnode = Id.get env j in
         if Equality.is_disequal env v1 v2 then make_disequal env inode jnode
         (* else if not (Egraph.is_equal env v1 v2) then
            link_inequalities env v1 v2 inode jnode *))
       m *)

  let rec add_select_uweg env ?(isppg = false) anode a inode i v =
    Debug.dprintf7 debug "NS_arrays.add_select_uweg %a %a (%a,%b)"
      (Id.NSeq.pp env) anode (Id.Index.pp env) inode Node.pp v isppg;
    match HT.find_opt env a with
    | None ->
        HT.set env a (Id.M.singleton i (v, isppg));
        Inds.change env i ~f:(function
          | None -> Some (Id.M.singleton a (v, isppg))
          | Some m -> Some (Id.M.add a (v, isppg) m))
        (* if not isppg then on_new_select env anode a inode i v *)
    | Some m -> (
        (* on_distinct_slects env inode v m; *)
        match Id.M.find_opt i m with
        | None ->
            HT.set env a (Id.M.add i (v, isppg) m);
            Inds.change env i ~f:(function
              | None -> Some (Id.M.singleton a (v, isppg))
              | Some m' -> Some (Id.M.add a (v, isppg) m'))
            (*  if not isppg then on_new_select env anode a inode i v *)
        | Some (v', isppg') ->
            Egraph.merge env v v';
            if isppg' && not isppg then (
              HT.set env a (Id.M.add i (v', false) m);
              Inds.change env i ~f:(function
                | None -> assert false
                | Some m' -> Some (Id.M.add a (v, false) m'))
              (*  on_new_select env anode a inode i v *)))

  and _on_new_select env anode a inode i av =
    Debug.dprintf6 debug "NS_arrays.on_new_select %a %a %a" (Id.NSeq.pp env)
      anode (Id.Index.pp env) inode Node.pp av;
    match Inds.find_opt env i with
    | None -> assert false
    | Some m ->
        let m = Id.M.remove a m in
        let dests, val_dec_dests =
          Id.M.fold
            (fun b (bv, isppg) (acc1, acc2) ->
              if
                isppg || Egraph.is_equal env bv av
                || not (UWEGraph.are_linked env a b)
              then (acc1, acc2)
              else if Equality.is_disequal env bv av then ((b, bv) :: acc1, acc2)
              else (acc1, (b, bv) :: acc2))
            m ([], [])
        in
        List.iter
          (fun (bid, bv) ->
            dec_on_val_for_ppg env anode (Id.get env bid) inode av bv)
          val_dec_dests;
        List.iter
          (fun (b, _) ->
            ignore (ppg_over_indices env anode a (Id.get env b) b inode i av))
          dests

  and dec_on_val_for_ppg env anode bnode inode av bv =
    Debug.dprintf10 debug "NS_arrays.dec_on_val_for_ppg (%a(%a) -> %a(%a)) %a"
      (Id.NSeq.pp env) anode Node.pp av (Id.NSeq.pp env) bnode Node.pp bv
      (Id.Index.pp env) inode;
    Choice.register_global env
      Choice.
        {
          prio = 1;
          print_cho =
            (fun fmt () ->
              Fmt.pf fmt "decide_on_values (%a = %a)" Node.pp av Node.pp bv);
          key = None;
          choice =
            (fun env ->
              if Egraph.is_equal env av bv then (
                Debug.dprintf4 debug "(%a = %a) is true" Node.pp av Node.pp bv;
                DecNo)
              else if Equality.is_disequal env av bv then (
                Debug.dprintf4 debug "(%a = %a) is false" Node.pp av Node.pp bv;
                ignore
                  (ppg_over_indices env anode (Id.NSeq.get_id env anode) bnode
                     (Id.NSeq.get_id env bnode) inode
                     (Id.Index.get_id env inode)
                     av);
                DecNo)
              else
                DecTodo
                  [
                    (fun env ->
                      Debug.dprintf4 debug "decided that (%a = %a) is true"
                        Node.pp av Node.pp bv;
                      Egraph.merge env av bv);
                    (fun env ->
                      Debug.dprintf4 debug "decided that (%a = %a) is false"
                        Node.pp av Node.pp bv;
                      make_disequal env av bv;
                      (* TODO: In this case, when searching for paths for v',
                         values equal to v should be targeted *)
                      ignore
                        (ppg_over_indices env anode (Id.NSeq.get_id env anode)
                           bnode (Id.NSeq.get_id env bnode) inode
                           (Id.Index.get_id env inode)
                           av));
                  ]);
        }

  and explore_path env anode a prec inode i v path =
    Debug.dprintf10 debug
      "NS_arrays.explore_path %a prec=%a (%a -> %a) path=[%a]" (Id.NSeq.pp env)
      anode (Id.NSeq.pp env) prec (Id.Index.pp env) inode Node.pp v
      (Fmt.list pp_step) path;
    match path with
    | [] -> (true, None)
    | (nid, next_ind, b, bnode) :: t ->
        if Egraph.is_equal env inode next_ind then (false, None)
        else if Equality.is_disequal env inode next_ind then (
          add_select_uweg ~isppg:true env bnode b inode i v;
          explore_path env anode a bnode inode i v t)
        else if are_holding_dist_vals env i nid then (
          make_disequal env inode next_ind;
          add_select_uweg ~isppg:true env bnode b inode i v;
          explore_path env anode a bnode inode i v t)
        else (false, Some (path, prec, next_ind, bnode))
  (* TODO: replace path by t? *)

  and get_and_explore_paths =
    let rec aux env acc anode a inode i v = function
      | [] -> (false, acc)
      | path :: t -> (
          match explore_path env anode a anode inode i v path with
          | true, Some _ -> assert false
          | false, Some (path', curr_node, next_ind, next_node) ->
              let dec_count, _ =
                List.fold_left
                  (fun (c, acc) (j, jnode, _, _) ->
                    if Id.S.mem j acc then (c, acc)
                    else (
                      assert (not (Egraph.is_equal env inode jnode));
                      if Disequality.is_disequal env inode jnode then
                        (c, Id.S.add i acc)
                      else if are_holding_dist_vals env i j then (
                        make_disequal env inode jnode;
                        (c, Id.S.add i acc))
                      else (c + 1, Id.S.add i acc)))
                  (0, Id.S.empty) path'
              in
              aux env
                ((dec_count, path', curr_node, next_ind, next_node) :: acc)
                anode a inode i v t
          | false, None -> aux env acc anode a inode i v t
          | true, None ->
              let _ = aux env acc anode a inode i v t in
              (true, []))
    in
    fun env ?paths ?ex_arrs anode a dsts inode i v ->
      Debug.dprintf8 debug
        "get_and_explore_paths from %a to {%a} with (%a -> %a)" (Id.NSeq.pp env)
        anode Id.S.pp dsts (Id.Index.pp env) inode Node.pp v;
      assert (not (Id.S.mem a dsts));
      let paths =
        try
          match paths with
          | None ->
              UWEGraph.all_paths env ~filter:false ?ex_arrs ~ex_ind:i a dsts
          | Some paths -> paths
        with Not_found -> []
      in
      Debug.dprintf2 debug "paths = %a" UWEGraph.pp_path_list paths;
      match paths with
      | [] -> (false, [])
      | _ -> aux env [] anode a inode i v paths

  and ppg_over_indices_decision =
    let mk_decisions anode bnode inode v next_ind next_node =
      [
        (fun env ->
          Debug.dprintf10 debug
            "mk_decisions1 ppg_over_indices_decision: src:%a curr_node:%a \
             dst:%a (%a -> %a)"
            (Id.NSeq.pp env) anode (Id.NSeq.pp env) next_node (Id.NSeq.pp env)
            bnode (Id.Index.pp env) inode Node.pp v;
          Debug.dprintf4 debug
            "ppg_over_indices_decision: set (%a = %a) to true" Node.pp inode
            Node.pp next_ind;
          Egraph.merge env inode next_ind);
        (fun env ->
          Debug.dprintf10 debug
            "mk_decisions2 ppg_over_indices_decision: src:%a curr_node:%a \
             dst:%a (%a -> %a)"
            (Id.NSeq.pp env) anode (Id.NSeq.pp env) next_node (Id.NSeq.pp env)
            bnode (Id.Index.pp env) inode Node.pp v;
          Debug.dprintf4 debug
            "ppg_over_indices_decision: set (%a = %a) to false" Node.pp inode
            Node.pp next_ind;
          let a' = Id.NSeq.get_id env next_node in
          let b = Id.NSeq.get_id env bnode in
          let i = Id.Index.get_id env inode in
          if Egraph.is_equal env next_node bnode then (
            Debug.dprintf4 debug "%a and %a are equal" (Id.NSeq.pp env) anode
              (Id.NSeq.pp env) bnode;
            make_disequal env inode next_ind;
            add_select_uweg ~isppg:true env bnode b inode i v)
          else (
            make_disequal env inode next_ind;
            ignore (ppg_over_indices env next_node a' bnode b inode i v)));
      ]
    in
    fun env anode inode bnode v curr_node ->
      Debug.dprintf10 debug
        "register ppg_over_indices_decision: src:%a curr_node:%a dst:%a (%a -> \
         %a)"
        (Id.NSeq.pp env) anode (Id.NSeq.pp env) curr_node (Id.NSeq.pp env) bnode
        (Id.Index.pp env) inode Node.pp v;
      if Egraph.is_equal env anode bnode then ()
      else
        Choice.register_global env
          Choice.
            {
              prio = 1;
              print_cho =
                (fun fmt () ->
                  Fmt.pf fmt "Decision from ppg_over_indices_decision ");
              key = None;
              choice =
                (fun env ->
                  Debug.dprintf10 debug
                    "ppg_over_indices_decision: src:%a curr_node:%a dst:%a (%a \
                     -> %a)"
                    (Id.NSeq.pp env) anode (Id.NSeq.pp env) curr_node
                    (Id.NSeq.pp env) bnode (Id.Index.pp env) inode Node.pp v;
                  (* TODO: don't re-compute path? *)
                  let curr_node_id = Id.NSeq.get_id env curr_node in
                  let b = Id.NSeq.get_id env bnode in
                  let i = Id.Index.get_id env inode in
                  if Egraph.is_equal env curr_node bnode then (
                    Debug.dprintf4 debug
                      "ppg_over_indices_decision: %a and %a became equal"
                      (Id.NSeq.pp env) curr_node (Id.NSeq.pp env) bnode;
                    DecNo)
                  else
                    match
                      get_and_explore_paths env curr_node curr_node_id
                        (Id.S.singleton b) inode i v
                    with
                    | _, [] -> DecNo
                    | true, _ -> assert false
                    | false, pcnl -> (
                        let pcnl =
                          List.filter
                            (fun (_, paths, _, _, _) ->
                              match paths with
                              | [] -> assert false
                              | (_, next_ind, nnid, _) :: _ ->
                                  let skip =
                                    match get_val env nnid i with
                                    | None -> false
                                    | Some v' -> Equality.is_disequal env v v'
                                  in
                                  if skip then (
                                    Egraph.merge env next_ind inode;
                                    false)
                                  else true)
                            pcnl
                        in
                        let pcnl =
                          List.sort
                            (fun (c1, _, _, _, _) (c2, _, _, _, _) ->
                              Int.compare c1 c2)
                            pcnl
                        in
                        match pcnl with
                        | [] -> DecNo
                        | (_, [], _, _, _) :: _ -> assert false
                        | (_, (_, next_ind, nnid, next_node) :: _, _, _, _) :: _
                          ->
                            let skip =
                              match get_val env nnid i with
                              | None -> false
                              | Some v' -> Equality.is_disequal env v v'
                            in
                            if skip then (
                              Egraph.merge env next_ind inode;
                              DecNo)
                            else
                              DecTodo
                                (mk_decisions anode bnode inode v next_ind
                                   next_node)));
            }

  and ppg_over_indices env ?paths ?(decide = true) anode a bnode b inode i v =
    Debug.dprintf8 debug "ppg_over_indices from %a to %a with (%a -> %a)"
      (Id.NSeq.pp env) anode (Id.NSeq.pp env) bnode (Id.Index.pp env) inode
      Node.pp v;
    if Egraph.is_equal env anode bnode then (
      Debug.dprintf4 debug "%a and %a are equal" (Id.NSeq.pp env) anode
        (Id.NSeq.pp env) bnode;
      (true, []))
    else
      match
        get_and_explore_paths env ?paths anode a (Id.S.singleton b) inode i v
      with
      | true, [] -> (true, [])
      | false, [] -> (false, [])
      | true, _ -> assert false
      | false, pcnl when not decide -> (false, pcnl)
      | false, pcnl ->
          (* TODO: avoid duplicate decisions *)
          List.iter
            (fun (_, _, curr_node, next_ind, next_node) ->
              let skip =
                match get_val env (Id.NSeq.get_id env next_node) i with
                | None -> false
                | Some v' -> Equality.is_disequal env v v'
              in
              if skip then Egraph.merge env inode next_ind
              else ppg_over_indices_decision env anode inode bnode v curr_node)
            pcnl;
          (false, pcnl)

  let eq_indices_norm env kid rid =
    Debug.dprintf4 debug "NS_arrays.eq_indices_norm %a <- %a" Id.pp rid Id.pp
      kid;
    match Inds.find_opt env rid with
    | None -> ()
    | Some rm ->
        Inds.remove env rid;
        let km = Opt.get_def Id.M.empty (Inds.find_opt env kid) in
        let nm, _new_selects =
          Id.M.fold
            (fun rnsid (rv, risppg) (acc, acc_selects) ->
              match HT.find_opt env rnsid with
              | None -> assert false
              | Some im -> (
                  match Id.M.find_opt kid im with
                  | None ->
                      let res = (rv, risppg) in
                      HT.set env rnsid (Id.M.add kid res (Id.M.remove rid im));
                      ( Id.M.change
                          (function None -> Some res | Some _ -> assert false)
                          rnsid acc,
                        if not risppg then Id.M.add rnsid rv acc_selects
                        else acc_selects )
                  | Some (v, isppg) ->
                      Egraph.merge env rv v;
                      let res = (rv, risppg && isppg) in
                      HT.set env rnsid (Id.M.add kid res (Id.M.remove rid im));
                      ( Id.M.change
                          (function None -> assert false | Some _ -> Some res)
                          rnsid acc,
                        if (not risppg) && isppg then
                          Id.M.add rnsid rv acc_selects
                        else acc_selects )))
            rm (km, Id.M.empty)
        in
        Inds.set env kid nm

  let eq_ns_arrays_norm env kid rid =
    Debug.dprintf4 debug "NS_arrays.eq_ns_arrays_norm %a <- %a" Id.pp rid Id.pp
      kid;
    match HT.find_opt env rid with
    | None -> ()
    | Some oldr_sm ->
        HT.remove env rid;
        let newr_sm = Opt.get_def Id.M.empty (HT.find_opt env kid) in
        let res_sm, _new_selects =
          Id.M.fold
            (fun i (v, isppg) (acc, acc_selects) ->
              (* on_distinct_slects env (Id.get env i) v newr_sm; *)
              Inds.change env i ~f:(function
                | None -> assert false
                | Some m -> (
                    match Id.M.find_remove rid m with
                    | _, None -> assert false
                    | m', Some (v', isppg') ->
                        assert (Bool.equal isppg isppg');
                        assert (Egraph.is_equal env v v');
                        if Id.M.is_empty m' then None else Some m'));
              let res_acc, acc_selects =
                match Id.M.find_opt i acc with
                | None ->
                    Inds.change env i ~f:(function
                      | None -> Some (Id.M.singleton kid (v, isppg))
                      | Some m ->
                          Some
                            (Id.M.change
                               (function
                                 | None -> Some (v, isppg)
                                 | Some _ -> assert false)
                               kid m));
                    ( Id.M.add i (v, isppg) acc,
                      if not isppg then Id.M.add i v acc_selects
                      else acc_selects )
                | Some (v', isppg') ->
                    Egraph.merge env v v';
                    let r = (v', isppg && isppg') in
                    Inds.change env i ~f:(function
                      | None -> Some (Id.M.singleton kid (v, isppg))
                      | Some m ->
                          Some
                            (Id.M.change
                               (function
                                 | None -> assert false | Some _ -> Some r)
                               kid m));
                    ( Id.M.add i r acc,
                      if (not isppg) && isppg' then Id.M.add i v acc_selects
                      else acc_selects )
              in
              (res_acc, acc_selects))
            oldr_sm (newr_sm, Id.M.empty)
        in
        HT.set env kid res_sm
end

(* module WeqCache = struct
     module HT = Id.MkIHT (struct
       type t = Id.S.t Id.M.t

       let pp fmt m =
         Fmt.pf fmt "{%a}" (Id.M.pp (fun fmt s -> Fmt.pf fmt "{%a}" Id.S.pp s)) m

       let name = "WeqCache.HT"
     end)

     module Locs = Id.MkIHT (struct
       type t = Id.S.t

       let pp fmt s = Fmt.pf fmt "{%a}" Id.S.pp s
       let name = "WeqCache.Locs"
     end)
   end *)
(* TODO: add weak-equivalency cache *)

include Arrays
include Selects

let iter_selects = Selects.HT.iter
let get_selects = Selects.HT.find_opt
let get_selects_ind = Selects.Inds.find_opt

let common_converter env s fn =
  let nid =
    match s.Ground.ty with
    | { app = { builtin = Expr.Array }; args = [ ind_gty; val_gty ] } ->
        Arrays.add_array_gty env fn ind_gty val_gty;
        let nid = NSeq_dom.new_nseq env fn in
        Arrays.add_ns_arrs_set env nid s.ty;
        Some nid
    | _ -> None
  in
  nid

let converter env f =
  let s = Ground.sem f in
  let fn = Ground.node f in
  let nid = common_converter env s fn in
  match s with
  | { app = { builtin = Expr.Select }; args; tyargs = [ _ind_gty; _val_gty ] }
    ->
      NSeq_value.propagate_value env f;
      let a, i = IArray.extract2_exn args in
      Egraph.register env a;
      Egraph.register env i;
      let aid = NSeq_dom.new_nseq env a in
      let iid = Id.Index.get_id_safe env i in
      NSeq_dom.add_kv env aid iid fn
  | { app = { builtin = Expr.Store }; args; tyargs = [ _ind_gty; _val_gty ] } ->
      NSeq_value.propagate_value env f;
      let a, i, v = IArray.extract3_exn args in
      Egraph.register env a;
      Egraph.register env i;
      Egraph.register env v;
      let aid = NSeq_dom.new_nseq env a in
      let iid = Id.Index.get_id_safe env i in
      let nid = Opt.get nid in
      NSeq_dom.add_kv env nid ~b:aid iid v;
      WEGraph.new_set env nid aid iid
  | _ -> ()

let converter_no_ppg env f =
  let s = Ground.sem f in
  let fn = Ground.node f in
  let nid = common_converter env s fn in
  match s with
  | { app = { builtin = Expr.Select }; args; tyargs = [ _ind_gty; _val_gty ] }
    ->
      NSeq_value.propagate_value env f;
      let a, i = IArray.extract2_exn args in
      Egraph.register env a;
      Egraph.register env i;
      let aid = NSeq_dom.new_nseq env a in
      let iid = Id.Index.get_id_safe env i in
      Selects.add_select_uweg env a aid i iid fn
  | { app = { builtin = Expr.Store }; args; tyargs = [ _ind_gty; _val_gty ] } ->
      NSeq_value.propagate_value env f;
      let a, i, v = IArray.extract3_exn args in
      Egraph.register env a;
      Egraph.register env i;
      Egraph.register env v;
      let aid = NSeq_dom.new_nseq env a in
      let iid = Id.Index.get_id_safe env i in
      let nid = Opt.get nid in
      UWEGraph.add_edge env nid iid aid;
      Selects.add_select_uweg env fn nid i iid v
  | _ -> ()

let get_weg_val =
  (* TODO: better value choice heuristic?
     At least paramterize whether we look for equal, unequal or any values *)
  let rec aux env ~ov id idn i inode m acc = function
    | [] -> (acc, None)
    | [] :: _ -> assert false
    | path :: t -> (
        let id', idn', path = UWEGraph.rev_path path id (Id.get env id) in
        let v = fst (Id.M.find id' m) in
        match
          Selects.ppg_over_indices env ~paths:[ path ] ~decide:false idn' id'
            idn id inode i v
        with
        | true, [] -> ([], Some v)
        | false, [] -> ([], None)
        | false, [ (_, path, curr_node, next_ind, _) ] ->
            let _, dec_count =
              List.fold_left
                (fun (s, count) (jid, jnode, _, _) ->
                  assert (not (Egraph.is_equal env inode jnode));
                  if Id.S.mem jid s then (s, count)
                  else if Equality.is_disequal env inode jnode then
                    (Id.S.add jid s, count)
                  else (Id.S.add jid s, count + 1))
                (Id.S.empty, 0) path
            in
            assert (dec_count > 0);
            aux env ~ov id idn i inode m
              ({
                 dec_count;
                 src = idn';
                 inode;
                 dst = idn;
                 v;
                 curr_node;
                 next_ind;
                 ov;
               }
              :: acc)
              t
        | _ -> assert false)
  in
  fun env ?ov id i ->
    match Selects.Inds.find_opt env i with
    | None -> assert false
    | Some m -> (
        Debug.dprintf6 debug "get_weg_val %a[%a] {%a}" Id.pp id Id.pp i Id.S.pp
          (Id.S.remove id (Id.S.of_list (Id.M.keys m)));
        try
          let paths =
            UWEGraph.all_paths env ~filter:false ~ex_ind:i id
              (Id.S.remove id (Id.S.of_list (Id.M.keys m)))
          in
          aux env ~ov id (Id.get env id) i (Id.get env i) m [] paths
        with Not_found -> ([], None))

let is_weq_mod_i_v1 =
  let rec aux env i inode seen path =
    match path with
    | [] -> (1, None, 0)
    | (j, _, _, _) :: t when Id.S.mem j seen -> aux env i inode seen t
    | (j, _, _, _) :: _ when Id.equal i j -> (-1, None, 0)
    | (j, jnode, _, _) :: t when Equality.is_disequal env inode jnode ->
        aux env i inode (Id.S.add j seen) t
    | (j, jnode, _, _) :: t ->
        if are_holding_dist_vals env i j then (
          make_disequal env inode jnode;
          aux env i inode (Id.S.add j seen) t)
        else
          let r, _, dec_count = aux env i inode (Id.S.add j seen) t in
          if r = -1 then (-1, None, 0) else (0, Some jnode, dec_count + 1)
  in
  let rec aux_paths env i inode paths =
    match paths with
    | [] -> (-1, [])
    | path :: t ->
        let r, dec_opt, dec_count = aux env i inode Id.S.empty path in
        if r = 1 then (1, [])
        else if r = -1 then aux_paths env i inode t
        else
          let rt, decs = aux_paths env i inode t in
          if rt = 1 then (1, [])
          else
            let dec =
              match dec_opt with None -> assert false | Some dec -> dec
            in
            (0, (dec_count, dec) :: decs)
  in
  fun env id1 id2 i ->
    Debug.dprintf6 debug "is_weq_mod_i_v1 %a %a i=%a" Id.pp id1 Id.pp id2 Id.pp
      i;
    let paths =
      UWEGraph.all_paths env ~filter:true ~ex_ind:i id1 (Id.S.singleton id2)
    in
    let paths =
      List.map (fun l -> (List.length l, l)) paths
      |> List.sort (fun (l1, _) (l2, _) -> Int.compare l1 l2)
      |> List.map snd
    in
    Debug.dprintf2 debug "%a" UWEGraph.pp_path_list paths;
    let r, decs = aux_paths env i (Id.get env i) paths in
    Debug.dprintf2 debug "aux_paths res: %d %d" r (List.length decs);
    let decs = List.sort (fun (l1, _) (l2, _) -> Int.compare l1 l2) decs in
    if r <> 0 then (r, None)
    else match decs with [] -> assert false | _ -> (r, Some (decs |> List.hd))

let pp = Selects.HT.pp
let pp_inds = Selects.Inds.pp

let on_new_graph_link =
  let rm_ppg_values m =
    Id.M.map_filter (fun (v, b) -> if b then None else Some v) m
  in
  fun env s1 s2 ->
    Debug.dprintf4 debug "on_new_graph_link {%a} {%a}" Id.S.pp s1 Id.S.pp s2;
    let m1 =
      Id.S.fold
        (fun id acc ->
          match Selects.HT.find_opt env id with
          | None -> acc
          | Some m -> Id.M.add id (rm_ppg_values m) acc)
        s1 Id.M.empty
    in
    let m2 =
      Id.S.fold
        (fun id acc ->
          match Selects.HT.find_opt env id with
          | None -> acc
          | Some m -> Id.M.add id (rm_ppg_values m) acc)
        s2 Id.M.empty
    in
    Id.M.iter
      (fun aid avm ->
        let anode = Id.get env aid in
        Id.M.iter
          (fun iid av ->
            let inode = Id.get env iid in
            let dests =
              Id.M.fold
                (fun bid bvm acc ->
                  match Id.M.find_opt iid bvm with
                  | None -> acc
                  | Some bv when Egraph.is_equal env av bv -> acc
                  | Some bv when Disequality.is_disequal env av bv ->
                      (bid, bv) :: acc
                  | Some bv ->
                      Selects.dec_on_val_for_ppg env anode (Id.get env bid)
                        inode av bv;
                      acc)
                m2 []
            in
            List.iter
              (fun (b, _) ->
                ignore
                  (Selects.ppg_over_indices env anode aid (Id.get env b) b inode
                     iid av))
              dests)
          avm)
      m1

let eq_indices_norm env kid rid = Selects.eq_indices_norm env kid rid

let eq_ns_arrays_norm env kid rid =
  Arrays.eq_ns_arrays_norm env kid rid;
  Selects.eq_ns_arrays_norm env kid rid
