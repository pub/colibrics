(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

val debug : Debug.flag
val ns_no_get_intro : bool Options.t
val array_nseqs_no_ppg : bool Options.t
val nseq_no_dist_first : bool Options.t

exception NotAnNSeqType of Expr.ty

module NSeqValue : sig
  type nsval = {
    val_gty : Ground.Ty.t;
    vals : Value.t Value.M.t;
    upper_bound : Value.t;
    lower_bound : Value.t;
    other : Value.t option;
  }

  val pp_nsval : nsval Fmt.t

  include Value.S with type s = nsval
end

module Builtin : sig
  type _ Expr.t +=
    | NSeq
    | NSeq_first
    | NSeq_last
    | NSeq_content
    | NSeq_length
    | NSeq_get
    | NSeq_set
    | NSeq_const
    | NSeq_relocate
    | NSeq_concat
    | NSeq_slice
    | NSeq_update
    | Seq
    | Seq_empty
    | Seq_unit
    | Seq_len
    | Seq_get
    | Seq_set
    | Seq_update
    | Seq_concat
    | Seq_extract

  val nseq_ty_const : Expr.Term.ty_const
  val nseq_ty : Expr.ty -> Expr.ty
  val val_ty_var : Expr.Term.ty_var
  val val_ty : Expr.ty
  val val_nseq_ty : Expr.ty
  val nsi_v : Expr.Term.Var.t
  val nsj_v : Expr.Term.Var.t
  val nsk_v : Expr.Term.Var.t
  val nsv_v : Expr.Term.Var.t
  val nsv'_v : Expr.Term.Var.t
  val ns_v1 : Expr.Term.Var.t
  val ns_v2 : Expr.Term.Var.t
  val ns_v3 : Expr.Term.Var.t
  val fst_v1 : Expr.Term.Var.t
  val lst_v1 : Expr.Term.Var.t
  val fst_v2 : Expr.Term.Var.t
  val lst_v2 : Expr.Term.Var.t
  val nsi_t : Expr.term
  val nsj_t : Expr.term
  val nsk_t : Expr.term
  val nsv_t : Expr.term
  val nsv'_t : Expr.term
  val ns_t1 : Expr.term
  val ns_t2 : Expr.term
  val ns_t3 : Expr.term
  val fst_t1 : Expr.term
  val lst_t1 : Expr.term
  val fst_t2 : Expr.term
  val lst_t2 : Expr.term
  val nseq_first : Expr.Term.Const.t
  val nseq_last : Expr.Term.Const.t
  val nseq_content : Expr.Term.Const.t
  val nseq_length : Expr.Term.Const.t
  val nseq_get : Expr.Term.Const.t
  val nseq_set : Expr.Term.Const.t
  val nseq_const : Expr.Term.Const.t
  val nseq_concat : Expr.Term.Const.t
  val nseq_relocate : Expr.Term.Const.t
  val nseq_slice : Expr.Term.Const.t
  val nseq_update : Expr.Term.Const.t
  val seq_ty_const : Dolmen_std.Expr.ty_cst
  val seq_empty : Dolmen_std.Expr.term_cst
  val seq_unit : Dolmen_std.Expr.term_cst
  val seq_length : Dolmen_std.Expr.term_cst
  val seq_get : Dolmen_std.Expr.term_cst
  val seq_set : Dolmen_std.Expr.term_cst
  val seq_update : Dolmen_std.Expr.term_cst
  val seq_extract : Dolmen_std.Expr.term_cst
  val get_ty_arg : Expr.ty -> Expr.ty
  val seq_concat : int -> Dolmen_std.Expr.term_cst
  val add_builtins : unit -> unit
end

val nseq_val_gty : Ground.Ty.t -> Ground.Ty.t
val get_nseq_gty : _ Egraph.t -> Node.t -> Ground.Ty.t
val is_seq : _ Egraph.t -> Node.t -> bool
val is_array : _ Egraph.t -> Node.t -> bool
val get_nseq_val_gty : _ Egraph.t -> Node.t -> Ground.Ty.t
val empty_seq_nsval : Ground.Ty.t -> NSeqValue.nsval
val empty_seq_gt : _ Egraph.t -> Ground.Ty.t -> Ground.t
val empty_seq_n : Ground.Ty.t -> Node.t
val mk_ite : _ Egraph.t -> Node.t -> Node.t -> Node.t -> Node.t
val mk_le : _ Egraph.t -> Node.t list -> Node.t
val mk_ge : _ Egraph.t -> Node.t list -> Node.t
val mk_lt : _ Egraph.t -> Node.t list -> Node.t
val mk_sub : _ Egraph.t -> Node.t -> Node.t -> Node.t
val mk_add : _ Egraph.t -> Node.t -> Node.t -> Node.t
val mk_mul : _ Egraph.t -> Node.t -> Node.t -> Node.t
val mk_in_bounds : _ Egraph.t -> Node.t -> Node.t -> Node.t -> Node.t
val mk_nseq_ty : Ground.Ty.t list -> Ground.Ty.t
val mk_nseq_first : _ Egraph.t -> Ground.Ty.t -> Node.t -> Node.t
val mk_nseq_last : _ Egraph.t -> Ground.Ty.t -> Node.t -> Node.t
val mk_nseq_length : _ Egraph.t -> Ground.Ty.t -> Node.t -> Node.t
val mk_nseq_get : _ Egraph.t -> Ground.Ty.t -> Node.t -> Node.t -> Node.t

val mk_nseq_set :
  _ Egraph.t -> Ground.Ty.t -> Node.t -> Node.t -> Node.t -> Node.t

val mk_nseq_const :
  _ Egraph.t -> Ground.Ty.t -> Node.t -> Node.t -> Node.t -> Node.t

val mk_nseq_slice :
  _ Egraph.t -> Ground.Ty.t -> Node.t -> Node.t -> Node.t -> Node.t

val mk_nseq_concat : _ Egraph.t -> Ground.Ty.t -> Node.t -> Node.t -> Node.t
val mk_nseq_relocate : _ Egraph.t -> Ground.Ty.t -> Node.t -> Node.t -> Node.t
val mk_eq : _ Egraph.t -> Node.t -> Node.t -> Node.t
val mk_diseq : _ Egraph.t -> Node.t -> Node.t -> Node.t
val mk_imp : _ Egraph.t -> Node.t -> Node.t -> Node.t
val add_nseq_gty : Egraph.wt -> Node.t -> Ground.Ty.t -> unit
val mk_in_bounds_term : Expr.term -> Expr.term -> Expr.term -> Expr.term
val mk_nseq_fst_term : Expr.ty -> Expr.term -> Expr.term
val mk_nseq_lst_term : Expr.ty -> Expr.term -> Expr.term
val mk_nseq_get_term : Expr.ty -> Expr.term -> Expr.term -> Expr.term

val mk_nseq_set_term :
  Expr.ty -> Expr.term -> Expr.term -> Expr.term -> Expr.term

val rv_of_node : _ Egraph.t -> Node.t -> A.t option
val set_int_dom : Egraph.wt -> Node.t -> unit
val is_in_bounds : _ Egraph.t -> Node.t -> Node.t -> Node.t -> bool option
val is_le : _ Egraph.t -> Node.t -> Node.t -> bool option
val reg_prec_succ : Egraph.wt -> Id.t -> Id.t -> unit
val eq_indices_norm_prec_succ : Egraph.wt -> Id.t -> Id.t -> unit
val nsucc : Egraph.wt -> ?id:Id.t -> Node.t -> Node.t * Id.t
val nprec : Egraph.wt -> ?id:Id.t -> Node.t -> Node.t * Id.t

val split_cond :
  Egraph.wt ->
  ?cons:(Egraph.wt -> unit) ->
  ?alt:(Egraph.wt -> unit) ->
  ?none:(Egraph.wt -> unit) ->
  Node.t ->
  unit

val new_decision :
  Egraph.wt ->
  Node.t ->
  string ->
  ?cons:(Egraph.wt -> unit) ->
  ?alt:(Egraph.wt -> unit) ->
  (Egraph.wt -> Choice.choice_state) ->
  unit

val bool_dec :
  ?cons:(Egraph.wt -> unit) ->
  ?alt:(Egraph.wt -> unit) ->
  Node.t ->
  Choice.choice_state

(* [eq_vals _ v1 v2] if v1 = v2 then 1, if v1 <> v2 then -1, otherwise 0  *)
val eq_vals : Egraph.wt -> Node.t -> Node.t -> int
val make_disequal : Egraph.wt -> Node.t -> Node.t -> unit
val on_multiple_kns : Egraph.wt -> Id.t -> Id.t -> Id.t -> Id.t -> unit

val decide_equality :
  Egraph.wt ->
  ?dec_check:(Egraph.wt -> Node.t -> Node.t -> bool) ->
  Node.t ->
  Node.t ->
  unit

type weq_dec = { dec_count : int; inode : Node.t; jnode : Node.t }

type ppg_dec = {
  dec_count : int;
  src : Node.t;
  inode : Node.t;
  dst : Node.t;
  v : Node.t;
  curr_node : Node.t;
  next_ind : Node.t;
  ov : Node.t option;
}

type dec = {
  val_eq : (bool * Node.t * Node.t) option;
  weq : weq_dec option;
  ppg1 : ppg_dec option;
  ppg2 : (ppg_dec * ppg_dec) option;
}

type dec_union =
  | VEq of bool * Node.t * Node.t
  | Weq of weq_dec
  | Ppg1 of ppg_dec
  | Ppg2 of ppg_dec * ppg_dec

val pp_dec : dec Fmt.t

val add_val_eq_dec :
  Id.t -> bool -> Node.t -> Node.t -> dec Id.M.t -> dec Id.M.t

val add_weq_dec : Id.t -> int -> Node.t -> Node.t -> dec Id.M.t -> dec Id.M.t
val add_ppg1_dec : Id.t -> ppg_dec -> dec Id.M.t -> dec Id.M.t
val add_ppg2_dec : Id.t -> ppg_dec -> ppg_dec -> dec Id.M.t -> dec Id.M.t
val choose_decision : dec -> int * dec_union
