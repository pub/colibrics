(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

let seplogic_parsing_extension =
  Dolmen_std.Extensions.Smtlib2.create ~name:"seplogic"
    ~stmts:[ "declare-heap" ]

let loc_v = Expr.Ty.Var.mk "loc"
let loc_ty = Expr.Ty.of_var loc_v
let data_v = Expr.Ty.Var.mk "data"
let data_ty = Expr.Ty.of_var data_v

type _ Expr.t += Emp | Sep | Wand | Pto | Nil

let emp : Expr.term_cst =
  Expr.Id.mk ~name:"emp" ~builtin:Emp
    (Dolmen_std.Path.global "emp")
    (Expr.Ty.pi [ loc_v; data_v ]
       (Expr.Ty.arrow [ loc_ty; data_ty ] Expr.Ty.bool))

let sep : Expr.term_cst =
  Expr.Id.mk ~name:"sep" ~builtin:Sep
    (Dolmen_std.Path.global "sep")
    (Expr.Ty.arrow [ Expr.Ty.bool; Expr.Ty.bool ] Expr.Ty.bool)

let wand : Expr.term_cst =
  Expr.Id.mk ~name:"wand" ~builtin:Wand
    (Dolmen_std.Path.global "wand")
    (Expr.Ty.arrow [ Expr.Ty.bool; Expr.Ty.bool ] Expr.Ty.bool)

let pto : Expr.term_cst =
  Expr.Id.mk ~name:"pto" ~builtin:Pto
    (Dolmen_std.Path.global "pto")
    (Expr.Ty.pi [ loc_v; data_v ]
       (Expr.Ty.arrow [ loc_ty; data_ty ] Expr.Ty.bool))

let nil : Expr.term_cst =
  Expr.Id.mk ~name:"nil" ~builtin:Nil
    (Dolmen_std.Path.global "nil")
    (Expr.Ty.pi [ loc_v ] (Expr.Ty.arrow [ loc_ty ] Expr.Ty.bool))

let add_builtins () =
  let app0 env s f =
    Dolmen_loop.Typer.T.builtin_term
      (Dolmen_type.Base.app0
         (module Dolmen_loop.Typer.T)
         env s
         (Expr.Term.apply_cst f [ Expr.Ty.of_var (Expr.Ty.Var.wildcard ()) ] []))
  in
  let app2 env s f =
    Dolmen_loop.Typer.T.builtin_term
      (Dolmen_type.Base.term_app2
         (module Dolmen_loop.Typer.T)
         env s
         (fun a b -> Expr.Term.apply_cst f [] [ a; b ]))
  in
  let app2_loc_data env s f =
    Dolmen_loop.Typer.T.builtin_term
      (Dolmen_type.Base.term_app2
         (module Dolmen_loop.Typer.T)
         env s
         (fun a b ->
           Expr.Term.apply_cst f [ a.Expr.term_ty; b.Expr.term_ty ] [ a; b ]))
  in
  Expr.add_builtins (fun env s ->
      match s with
      | Dolmen_loop.Typer.T.Id { ns = Sort; name = Simple "emp" } ->
          app0 env s emp
      | Dolmen_loop.Typer.T.Id { ns = Sort; name = Simple "sep" } ->
          app2 env s sep
      | Dolmen_loop.Typer.T.Id { ns = Sort; name = Simple "wand" } ->
          app2 env s wand
      | Dolmen_loop.Typer.T.Id { ns = Sort; name = Simple "pto" } ->
          app2_loc_data env s pto
      | Dolmen_loop.Typer.T.Id { ns = Sort; name = Simple "nil" } ->
          app2_loc_data env s nil
      | _ -> `Not_found)

let converter _env f =
  let s = Ground.sem f in
  let _n = Ground.node f in
  match s.ty with _ -> ()

let init env =
  Dolmen_std.Extensions.Smtlib2.enable seplogic_parsing_extension;
  add_builtins ();
  Ground.register_converter env converter
