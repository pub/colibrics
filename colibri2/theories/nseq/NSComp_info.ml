(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_core
open Colibri2_popop_lib
open Popop_stdlib

module NSC = struct
  let hash_fold_option = Base.Option.hash_fold_t
  let hash_fold_list = Base.List.hash_fold_t

  type atom = Node.t [@@deriving ord, hash, eq]
  type atom_pos = { a : atom; next : Node.t option } [@@deriving hash, eq]
  type t = atom_pos Id.M.t Id.M.t [@@deriving hash, eq]
  (* maps `first` to a map of `last`s to an atom *)
  (* TODO: make sure that if there is only one component, then the component
     is made equal to the containing NSeq term. *)

  let pp_atom ?env fmt n =
    match env with
    | None -> Fmt.pf fmt "NS %a" Node.pp n
    | Some env ->
        Fmt.pf fmt "NS %a(%a)" (Id.NSeq.pp env) n Node.pp (Egraph.find env n)

  let pp_atom_pos ?env fmt ap =
    match ap with
    | { a; next = Some n } ->
        Fmt.pf fmt "{a: %a; next: %a}" (pp_atom ?env) a
          (Opt.fold (fun _ -> Id.Index.pp) Node.pp env)
          n
    | { a; _ } -> Fmt.pf fmt "{a: %a; last }" (pp_atom ?env) a

  let pp ?env fmt (m : t) =
    Fmt.pf fmt "(%a)"
      (Id.M.pp (fun fmt -> Fmt.pf fmt "[%a]" (Id.M.pp (pp_atom_pos ?env))))
      m

  module L = struct
    module L' = struct
      type t = (atom * Node.t * Node.t) list [@@deriving ord, show, hash, eq]
    end

    include L'
    include MkDatatype (L')
  end
end
