(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

include Colibri2_theories_utils.Ident

module ITN = MkIHT (struct
  type t = Node.t

  let pp = Node.pp
  let name = "IdsToNodes"
end)

let pp_nid env fmt i =
  match ITN.find_opt env i with
  | Some n -> Format.fprintf fmt "%a{id = %a}" Node.pp n pp i
  | None -> Format.fprintf fmt "Not_found{id = %a}" pp i

module NSeq = MkIdDom (struct
  let name = "NSeq.Id"
end)

module Index = MkIdDom (struct
  let name = "NSeq.Index.Id"
end)

let get = ITN.find
let set = ITN.set
let pp_ht = ITN.pp

module IdP = struct
  module T = struct
    type t = Colibri2_theories_utils.Ident.t * Colibri2_theories_utils.Ident.t
    [@@deriving show, ord, eq, hash]
  end

  include T
  include Colibri2_popop_lib.Popop_stdlib.MkDatatype (T)
end

module IdT = struct
  module T = struct
    type t =
      Colibri2_theories_utils.Ident.t
      * Colibri2_theories_utils.Ident.t
      * Colibri2_theories_utils.Ident.t
    [@@deriving show, ord, eq, hash]
  end

  include T
  include Colibri2_popop_lib.Popop_stdlib.MkDatatype (T)
end

module HT = Datastructure.Hashtbl (IdP)

let sort (id1, id2) =
  let c = compare id1 id2 in
  assert (not (equal id1 id2));
  if c < 0 then (id2, id1) else (id1, id2)

module MkIPHT (V : sig
  type t

  val pp : t Fmt.t
  val name : string
end) : HTS with type key = t * t and type t = V.t = struct
  type key = IdP.t
  type t = V.t

  let db = HT.create V.pp V.name
  let set env i v = HT.set db env (sort i) v
  let find env i = HT.find db env (sort i)
  let find_opt env i = HT.find_opt db env (sort i)
  let change ~f env i = HT.change f db env (sort i)
  let remove env i = HT.remove db env (sort i)
  let iter = HT.iter db
  let fold f env acc = HT.fold f db env acc

  let pp fmt env =
    Fmt.pf fmt "%s:[%a]" V.name
      (fun fmt () ->
        iter env ~f:(fun i v -> Fmt.pf fmt "\n  (%a -> (%a));" IdP.pp i V.pp v))
      ()
end
