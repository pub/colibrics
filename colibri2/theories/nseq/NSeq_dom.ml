(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_core
open Colibri2_popop_lib
open Common
module P = Colibri2_theories_LRA.Polynome
module DP = Colibri2_theories_LRA.Dom_polynome
module Ref = Datastructure.Ref

let compute_relocation env nonrepr repr i =
  let ni = Id.get env i in
  let afst, _, _, _ = Bounds.get_mk_bounds_id env nonrepr in
  let rfst, _, _, _ = Bounds.get_mk_bounds_id env repr in
  let i' = Relocate.relocate_ind env ni afst rfst in
  Debug.dprintf8 debug "compute_relocation %a (%a -> %a): %a" (Id.pp_nid env) i
    (Id.pp_nid env) nonrepr (Id.pp_nid env) repr (Id.pp_nid env) i';
  i'

let on_new_val_hook :
    (Egraph.wt -> Id.t -> Id.t -> Node.t -> unit) Datastructure.Push.t =
  Datastructure.Push.create Fmt.nop "NSeq_dom.on_new_val_hook"

let register_on_new_val_hook env f =
  Datastructure.Push.push on_new_val_hook env f

module KV = struct
  (* for all: a = nseq.set(bi, i, v):
     - a -> (i|i' -> (v, bi1 set, nonrepr -> bi2 set)))
     - i|i' -> a -> v
     - bi1 -> (a|nonrepr) -> i|i'
     - nonrepr -> bi2 set *)

  (* let _pp' ?env =
     Id.M.pp (fun fmt (v, s, m) ->
         Fmt.pf fmt "(%a(%a), {%a}, {%a})" Node.pp v
           (fun fmt n ->
             match env with
             | None -> ()
             | Some env -> Node.pp fmt (Egraph.find env n))
           v Id.S.pp s (Id.M.pp Id.S.pp) m) *)

  let pp =
    Id.M.pp (fun fmt (v, s, m) ->
        Fmt.pf fmt "(%a, {%a}, {%a})" Node.pp v Id.S.pp s (Id.M.pp Id.S.pp) m)

  module HT = Id.MkIHT (struct
    type t = (Node.t * Id.S.t * Id.S.t Id.M.t) Id.M.t

    let pp = pp
    let name = "NSeqSetsHT"
  end)

  module Inds = Id.MkIHT (struct
    type t = Node.t Id.M.t [@@deriving show]

    let name = "NSeqSetsInds"
  end)

  module KNeighbours = Id.MkIHT (struct
    type t = Id.t Id.M.t
    (* TODO: decision need to be made whenever there is more than one diseq
       witness. Only one can be a diseq witness so either the two nseqs are
       equal, or they are different in which case, either the two (or more)
       witnesses are equal, or they are fake except for one of them. *)

    let pp = Id.M.pp Id.pp
    let name = "NSeqKNeighbours"
  end)

  module NRLocs = Id.MkIHT (struct
    type t = Id.S.t

    let pp = Id.S.pp
    let name = "NRLocs"
  end)

  let add_b_opt env ?(s = Id.S.empty) ?(m = Id.M.empty) b nonrepr v =
    match b with
    | None -> (true, (v, s, m))
    | Some b -> (
        match nonrepr with
        | None when Id.S.mem b s -> (false, (v, s, m))
        | None -> (true, (v, Id.S.add b s, m))
        | Some nonrepr ->
            NRLocs.change env nonrepr ~f:(function
              | None -> Some (Id.S.singleton b)
              | Some s -> Some (Id.S.add b s));
            let isnew_kn, nm =
              match Id.M.find_opt nonrepr m with
              | None -> (true, Id.M.add nonrepr (Id.S.singleton b) m)
              | Some s when Id.S.mem b s -> (false, m)
              | Some s -> (true, Id.M.add nonrepr (Id.S.add b s) m)
            in
            (isnew_kn, (v, s, nm)))

  (* says that (v1 = v2) <=> (ns1 = ns2) *)
  let link_equalities env v1 v2 ns1 ns2 =
    Debug.dprintf8 debug "NSeq_dom.KV.link_equalities: (%a = %a) <=> (%a = %a)"
      Node.pp v1 Node.pp v2 (Id.NSeq.pp env) ns1 (Id.NSeq.pp env) ns2;
    if Egraph.is_equal env v1 v2 then Egraph.merge env ns1 ns2
    else if Equality.is_disequal env v1 v2 then (
      let n = mk_diseq env ns1 ns2 in
      Egraph.register env n;
      Boolean.set_true env n)
    else
      let cond = mk_eq env v1 v2 in
      Egraph.register env cond;
      DaemonAlsoInterp.attach_value env cond Boolean.dom (fun env _ v ->
          if Boolean.BoolValue.value v then Egraph.merge env ns1 ns2
          else make_disequal env ns1 ns2)

  (* TODO: check for consts as well, and support relocations  *)
  let add_eq_cond env a ?b ?nonrepr i v =
    Debug.dprintf10 debug "NSeq_dom.KV.add_eq_cond %a (%a) (%a) %a %a"
      (Id.pp_nid env) a
      (Opt.pp (Id.pp_nid env))
      b
      (Opt.pp (Id.pp_nid env))
      nonrepr (Id.pp_nid env) i Node.pp v;
    if Option.is_none nonrepr then (
      Option.iter
        (fun b ->
          match HT.find_opt env b with
          | None -> ()
          | Some m -> (
              match Id.M.find_opt i m with
              | None -> ()
              | Some (v', _, _) ->
                  link_equalities env v v' (Id.get env a) (Id.get env b)))
        b;
      match KNeighbours.find_opt env a with
      | None -> ()
      | Some m ->
          let s =
            Id.M.fold
              (fun kn k acc -> if Id.equal k i then Id.S.add kn acc else acc)
              m Id.S.empty
          in
          Id.S.iter
            (fun b ->
              match HT.find_opt env b with
              | None -> ()
              | Some m -> (
                  match Id.M.find_opt i m with
                  | None -> ()
                  | Some (v', _, _) ->
                      link_equalities env v v' (Id.get env a) (Id.get env b)))
            s)

  let rm_kneighbour env id k kneighbour =
    Debug.dprintf6 debug "rm_kneighbour %a (%a) %a" Id.pp id Id.pp k Id.pp
      kneighbour;
    match Relocate.get_repr env id with
    | Repr _ ->
        HT.change env id ~f:(function
          | None -> assert false
          | Some m ->
              Some
                (Id.M.change
                   (function
                     | None -> assert false
                     | Some (v, ks, km) ->
                         assert (Id.S.mem kneighbour ks);
                         Some (v, Id.S.remove kneighbour ks, km))
                   k m))
    | NonRepr { repr } ->
        HT.change env repr ~f:(function
          | None -> assert false
          | Some m ->
              Some
                (Id.M.change
                   (function
                     | None -> assert false
                     | Some (v, ks, km) ->
                         Some
                           ( v,
                             ks,
                             Id.M.change
                               (function
                                 | None -> assert false
                                 | Some ks ->
                                     assert (Id.S.mem kneighbour ks);
                                     Some (Id.S.remove kneighbour ks))
                               id km ))
                   k m))

  let add_kv_aux env a ?b ?nonrepr i v =
    Debug.dprintf10 debug "NSeq.KV.add_kv_aux %a (%a) (%a) %a %a" Id.pp a
      (Fmt.option Id.pp) b (Fmt.option Id.pp) nonrepr Id.pp i Node.pp v;
    let b =
      match b with Some b when not (Id.equal a b) -> Some b | _ -> None
    in
    let is_new_kn, is_new_v =
      match HT.find_opt env a with
      | None ->
          add_eq_cond env a ?b ?nonrepr i v;
          let _, nv = add_b_opt env b nonrepr v in
          let nm = Id.M.singleton i nv in
          HT.set env a nm;
          (true, true)
      | Some m ->
          let is_new_kn, is_new_v, nm =
            match Id.M.find_opt i m with
            | None ->
                add_eq_cond env a ?b ?nonrepr i v;
                let _, nv = add_b_opt env b nonrepr v in
                (true, true, Id.M.add i nv m)
            | Some (v', s, m') ->
                Egraph.merge env v v';
                let is_new_kn, nv = add_b_opt env ~s ~m:m' b nonrepr v' in
                if is_new_kn then add_eq_cond env a ?b ?nonrepr i v;
                (is_new_kn, false, Id.M.add i nv m)
          in
          HT.set env a nm;
          (is_new_kn, is_new_v)
    in
    Inds.change env i ~f:(function
      | None -> Some (Id.M.singleton a v)
      | Some m ->
          Some
            (Id.M.change
               (function
                 | None -> Some v
                 | Some v' ->
                     Egraph.merge env v v';
                     (* should be useless as it's done while changing NS *)
                     Some v')
               a m));
    Opt.iter
      (fun b ->
        let kn = Opt.get_def a nonrepr in
        KNeighbours.change env b ~f:(function
          | None -> Some (Id.M.singleton kn i)
          | Some m ->
              Some
                (Id.M.change
                   (function
                     | None -> Some i
                     | Some i' when Id.equal i i' -> Some i
                     | Some i' ->
                         rm_kneighbour env kn i b;
                         on_multiple_kns env b kn i i';
                         Some i')
                   kn m)))
      b;
    if is_new_v then (
      Datastructure.Push.iter on_new_val_hook env ~f:(fun f -> f env a i v);
      Colibri2_theories_utils.Nsa_content.add_ns_kv env a (Id.get env i) v);
    (is_new_kn, is_new_v)

  let is_const env a cv =
    HT.change env a ~f:(function
      | None -> None
      | Some m ->
          Id.M.iter (fun _ (v, _, _) -> Egraph.merge env cv v) m;
          Some m (* TODO: should be removed? *))

  let findi env a i =
    match HT.find_opt env a with
    | None -> None
    | Some m -> (
        match Id.M.find_opt i m with None -> None | Some v -> Some v)

  let getv env a i =
    match HT.find_opt env a with
    | None -> None
    | Some m -> (
        match Id.M.find_opt i m with None -> None | Some (v, _, _) -> Some v)
end

let on_set_defval_hook : (Egraph.wt -> Id.t -> Node.t -> unit) Ref.t =
  Ref.create Fmt.nop "NSeq_dom-on-set-default-value" (fun _ _ _ -> ())

let set_on_set_defval_hook env f = Ref.set on_set_defval_hook env f

let on_set_is_empty_hook :
    (Egraph.wt -> Id.t -> Node.t -> unit) Datastructure.Push.t =
  Datastructure.Push.create Fmt.nop "NSeq_dom.on_set_is_empty_hook"

let register_on_set_is_empty_hook env f =
  Datastructure.Push.push on_set_is_empty_hook env f

module NSeqs = struct
  type t = Empty | Unconstrained | Default of Node.t [@@deriving show]

  module HT = Id.MkIHT (struct
    type nonrec t = t

    let pp = pp
    let name = "NSeqsHT"
  end)

  let update env id t =
    Debug.dprintf4 debug "update %a %a" Id.pp id pp t;
    match HT.find_opt env id with
    | None -> HT.set env id t
    | Some old_t -> (
        match (old_t, t) with
        | Empty, Empty -> ()
        | Default v1, Default v2 when Egraph.is_equal env v1 v2 -> ()
        | Unconstrained, Unconstrained -> ()
        | Empty, _ ->
            (* TODO: what should be done when one that was thought not to
               be empty, turns out to be empty? *)
            ()
        | _, Empty ->
            HT.set env id Empty;
            let nid = Id.get env id in
            Datastructure.Push.iter
              ~f:(fun f -> f env id nid)
              on_set_is_empty_hook env
        | Default v, Unconstrained -> HT.set env id (Default v)
        | Unconstrained, Default v ->
            KV.is_const env id v;
            HT.set env id (Default v);
            let f = Ref.get on_set_defval_hook env in
            f env id v
        | Default v1, Default v2 ->
            Egraph.merge env v1 v2;
            HT.set env id (Default v1))
end

let ( ||> ) x f a b = f a b x

let wrap_reloc env id f =
  match Relocate.get_repr env id with
  | Repr _ -> f env id
  | NonRepr { repr; _ } -> f env repr

let new_nseq env n =
  let nsid = Id.NSeq.get_id_safe env ~debug n in
  wrap_reloc env nsid (NSeqs.Unconstrained ||> NSeqs.update);
  nsid

let new_empty_nseq env n =
  let nsid = Id.NSeq.get_id_safe env ~debug n in
  wrap_reloc env nsid (NSeqs.Empty ||> NSeqs.update);
  nsid

let new_const_nseq env n other =
  let nsid = Id.NSeq.get_id_safe env ~debug n in
  wrap_reloc env nsid (NSeqs.Default other ||> NSeqs.update);
  nsid

let set_is_empty env id = wrap_reloc env id (NSeqs.Empty ||> NSeqs.update)
let set_defval env id v = wrap_reloc env id (NSeqs.Default v ||> NSeqs.update)

let is_empty env id =
  wrap_reloc env id (fun env repr ->
      match NSeqs.HT.find_opt env repr with Some Empty -> true | _ -> false)

let get_defval env id =
  wrap_reloc env id (fun env repr ->
      match NSeqs.HT.find_opt env repr with
      | Some (Default other) -> Some other
      | _ -> None)

let get_kvs env id =
  match KV.HT.find_opt env id with Some m -> Some m | _ -> None

let get_kns env id =
  match KV.KNeighbours.find_opt env id with Some m -> Some m | _ -> None

let find_kv env aid iid =
  match KV.HT.find_opt env aid with
  | None -> None
  | Some m -> Id.M.find_opt iid m

let rec add_kv env a ?b i v =
  Debug.dprintf8 debug "NSeq_dom.add_kv %a (%a) %a %a" (Id.pp_nid env) a
    (Opt.pp (Id.pp_nid env))
    b (Id.pp_nid env) i Node.pp v;
  let repr_a, repr_i, nonrepr =
    match Relocate.get_repr env a with
    | Repr _ -> (a, i, None)
    | NonRepr { repr } -> (repr, compute_relocation env a repr i, Some a)
  in
  match NSeqs.HT.find_opt env repr_a with
  | Some (Default ao) -> Egraph.merge env ao v
  | _ -> (
      try
        match b with
        | None -> raise Exit
        | Some b -> (
            match NSeqs.HT.find_opt env b with
            | Some (Default bo) when Egraph.is_equal env bo v ->
                Egraph.merge env (Id.get env a) (Id.get env b)
            | _ -> raise Exit)
      with Exit ->
        let is_new_kn, is_new_v =
          match KV.HT.find_opt env repr_a with
          | None ->
              let is_new_kn, is_new_v =
                KV.add_kv_aux env repr_a ?b ?nonrepr repr_i v
              in
              ppg_shared_slices env repr_a repr_i v;
              (is_new_kn, is_new_v)
          | Some m -> (
              match Id.M.find_opt repr_i m with
              | Some _ -> KV.add_kv_aux env repr_a ?b ?nonrepr repr_i v
              | None ->
                  let is_new_kn, is_new_v =
                    KV.add_kv_aux env repr_a ?b ?nonrepr repr_i v
                  in
                  read_over_write env repr_a (Id.get env repr_a) repr_i
                    (Id.get env repr_i) v m;
                  ppg_shared_slices env repr_a repr_i v;
                  (* TODO: is this really the only case in which it should be
                     done? *)
                  (is_new_kn, is_new_v))
        in
        if is_new_kn then
          Option.iter
            (fun b -> new_k_neighbour env repr_a b ?nonrepr repr_i i)
            b;
        if is_new_v then write_over_read env repr_a repr_i v)

and ppg_kvs_over_outgoing_kns env a i v ks km =
  Debug.dprintf10 debug "NSeq_dom.ppg_kvs_over_kns %a %a %a {%a} {%a}" Id.pp a
    Id.pp i Node.pp v Id.S.pp ks (Id.M.pp Id.S.pp) km;
  Id.S.iter (fun bid -> add_kv env bid i v) ks;
  Id.M.iter
    (fun nr bids ->
      let i' = compute_relocation env a nr i in
      Id.S.iter (fun bid -> add_kv env bid i' v) bids)
    km

and read_over_write env a anode i inode v m =
  Debug.dprintf12 debug "NSeq_dom.read_over_write %a(%a) %a(%a) %a {%a}" Id.pp a
    Node.pp anode Id.pp i Node.pp inode Node.pp v KV.pp m;
  Id.M.iter
    (fun k (_, ks, km) ->
      (* TODO: delay those in km? *)
      let knode = Id.get env k in
      if Disequality.is_disequal env inode knode then
        ppg_kvs_over_outgoing_kns env a i v ks km
      else
        let cond = Equality.disequality env [ inode; knode ] in
        Egraph.register env cond;
        let cons env =
          let a = Id.NSeq.get_id env anode in
          let k = Id.Index.get_id env knode in
          let i = Id.Index.get_id env inode in
          let a, k, i =
            match Relocate.get_repr env a with
            | Repr _ -> (a, k, i)
            | NonRepr { repr } ->
                ( repr,
                  compute_relocation env a repr k,
                  compute_relocation env a repr i )
          in
          match KV.findi env a k with
          | None -> ()
          | Some (_, ks, km) -> ppg_kvs_over_outgoing_kns env a i v ks km
        in
        new_decision env cond "Decision from NSeq.read_over_write" ~cons
          (fun _ -> bool_dec cond ~cons))
    m

and write_over_read env a i v =
  Debug.dprintf6 debug "NSeq_dom.write_over_read %a %a %a" Id.pp a Id.pp i
    Node.pp v;
  match KV.KNeighbours.find_opt env a with
  | None -> ()
  | Some knm ->
      Id.M.iter
        (fun bi k ->
          if not (Id.equal bi a) then (
            (* should be always true *)
            let knode = Id.get env k in
            let inode = Id.get env i in
            if Disequality.is_disequal env inode knode then
              let bi, i =
                match Relocate.get_repr env bi with
                | Repr _ -> (bi, i)
                | NonRepr { repr } -> (repr, compute_relocation env bi repr i)
              in
              add_kv env bi i v
            else
              let cond = Equality.disequality env [ inode; knode ] in
              Egraph.register env cond;
              let binode = Id.get env bi in
              let cons env =
                let bi = Id.NSeq.get_id env binode in
                let i = Id.Index.get_id env inode in
                let bi, i =
                  match Relocate.get_repr env bi with
                  | Repr _ -> (bi, i)
                  | NonRepr { repr } -> (repr, compute_relocation env bi repr i)
                in
                add_kv env bi i v
              in
              new_decision env cond "Decision from NSeq.write_over_read" ~cons
                (fun _ -> bool_dec cond ~cons)))
        knm

and new_k_neighbour =
  let normalize_modulo_reloc env m afst bfst =
    Id.M.fold
      (fun k v acc ->
        let k' = Relocate.relocate_ind env afst bfst (Id.get env k) in
        Id.M.add_new (Failure "index already exists") k' v acc)
      m Id.M.empty
  in
  let directed_ppg env m a i =
    Debug.dprintf6 debug "NSeq_dom.directed_ppg %a %a {%a}" (Id.pp_nid env) a
      (Id.pp_nid env) i
      (Id.M.pp (fun fmt (v, _, _) -> Fmt.pf fmt "%a" Node.pp v))
      m;
    let inode = Id.get env i in
    Id.M.iter
      (fun k (v, _, _) ->
        let knode = Id.get env k in
        if Disequality.is_disequal env inode knode then add_kv env a k v
        else
          let cond = Equality.disequality env [ inode; knode ] in
          Egraph.register env cond;
          let anode = Id.get env a in
          let cons env =
            let a = Id.NSeq.get_id env anode in
            let k = Id.Index.get_id env knode in
            let a, k =
              match Relocate.get_repr env a with
              | Repr _ -> (a, k)
              | NonRepr { repr } -> (repr, compute_relocation env a repr k)
            in
            add_kv env a k v
          in
          new_decision env cond "Decision from NSeq.directed_ppg" ~cons
            (fun _ -> bool_dec cond ~cons))
      m
  in
  fun env a b ?nonrepr repr_i i ->
    Debug.dprintf10 debug "NSeq_dom.new_k_neighbour %a %a (%a) %a %a"
      (Id.pp_nid env) a (Id.pp_nid env) b
      (Opt.pp (Id.pp_nid env))
      nonrepr (Id.pp_nid env) repr_i (Id.pp_nid env) i;
    let () =
      match nonrepr with
      | None -> WEGraph.new_set env a b i
      | Some nr -> WEGraph.new_set env nr b i
    in
    match (KV.HT.find_opt env a, KV.HT.find_opt env b) with
    | None, _ -> assert false
    | Some am, None ->
        let am = Id.M.remove repr_i am in
        let am_to_b =
          match nonrepr with
          | None -> am
          | Some nr ->
              let afst, _, _, _ = Bounds.get_mk_bounds_id env a in
              let nrfst, _, _, _ = Bounds.get_mk_bounds_id env nr in
              let bfst, _, _, _ = Bounds.get_mk_bounds_id env b in
              assert (Egraph.is_equal env nrfst bfst);
              normalize_modulo_reloc env am afst nrfst
        in
        directed_ppg env am_to_b b i
    | Some am, Some bm ->
        let am = Id.M.remove repr_i am in
        let bm = Id.M.remove i bm in
        let am_to_b, bm_to_a =
          match nonrepr with
          | None -> (am, bm)
          | Some nr ->
              let afst, _, _, _ = Bounds.get_mk_bounds_id env a in
              let nrfst, _, _, _ = Bounds.get_mk_bounds_id env nr in
              let bfst, _, _, _ = Bounds.get_mk_bounds_id env b in
              assert (Egraph.is_equal env nrfst bfst);
              let am_to_b = normalize_modulo_reloc env am afst bfst in
              let bm_to_a = normalize_modulo_reloc env bm bfst afst in
              (am_to_b, bm_to_a)
        in
        directed_ppg env am_to_b b i;
        directed_ppg env bm_to_a a repr_i

and ppg_shared_slices =
  let ppg_succs env a i v =
    match Relations.get_succs env a with
    | None -> ()
    | Some { concat; slices; updates } ->
        Debug.dprintf12 debug
          "add_kv_aux: ppg_succs %a %a %a ({%a}, {%a}, {%a})" Id.pp a Id.pp i
          Node.pp v
          (Id.M.pp (fun fmt (i, _) -> Id.pp fmt i))
          (Id.M.filter (fun _ (_, b) -> b) concat)
          Id.S.pp slices
          (Id.M.pp (fun fmt (s, _) -> Fmt.pf fmt "{%a}" Id.S.pp s))
          (Id.M.filter (fun _ (_, b) -> b) updates);
        Id.M.iter
          (fun cid1 (cid2, b) ->
            if b then
              let n1 = Id.get env cid1 in
              let fst1, _, lst1, _ = Bounds.get_mk_bounds_n env n1 in
              let n2 = Id.get env cid2 in
              let fst2, _, lst2, _ = Bounds.get_mk_bounds_n env n2 in
              add_kv_concat_aux env (Id.get env i) i v (Id.get env cid1) cid1
                fst1 lst1 (Id.get env cid2) cid2 fst2 lst2)
          concat;
        Id.S.iter
          (fun sid ->
            let ns = Id.get env sid in
            let fst, _, lst, _ = Bounds.get_mk_bounds_n env ns in
            add_kv_slice_aux env (Id.get env i) i v ns sid fst lst)
          slices;
        Id.M.iter
          (fun uid1 (uid2s, b) ->
            if b then
              Id.S.iter
                (fun uid2 ->
                  let n2 = Id.get env uid2 in
                  let fst2, _, lst2, _ = Bounds.get_mk_bounds_n env n2 in
                  add_kv_update_aux env (Id.get env i) i v (Id.get env uid1)
                    uid1 (Id.get env uid2) uid2 fst2 lst2)
                uid2s)
          updates
  in
  let ppg_precs env a i v =
    match Relations.get_precs env a with
    | None -> ()
    | Some (s1, s2, s3) ->
        Debug.dprintf12 debug "add_kv_aux ppg_precs %a %a %a ({%a}, {%a}, {%a})"
          Id.pp a Id.pp i Node.pp v Id.S.pp s1 Id.S.pp s2 Id.S.pp s3;
        let ni = Id.get env i in
        Id.S.iter
          (fun srcid ->
            let srcn = Id.get env srcid in
            let m = Relations.get_updates env srcid in
            match Id.M.find_opt a m with
            | None -> assert false
            | Some (s, true) ->
                Id.S.iter
                  (fun id2 ->
                    let fst2, _, lst2, _ = Bounds.get_mk_bounds_id env id2 in
                    let cond = mk_in_bounds env ni fst2 lst2 in
                    Egraph.register env cond;
                    let alt env =
                      add_kv env (Id.NSeq.get_id env srcn)
                        (Id.Index.get_id env ni) v
                    in
                    new_decision env cond ~alt
                      "Decision from Theory1.ppg_upd_precs" (fun _ ->
                        DecTodo
                          [
                            (fun env -> Boolean.set_true env cond);
                            (fun env ->
                              Boolean.set_false env cond;
                              add_kv env (Id.NSeq.get_id env srcn)
                                (Id.Index.get_id env ni) v);
                          ]))
                  s
            | Some (_, false) ->
                add_kv env (Id.NSeq.get_id env srcn) (Id.Index.get_id env ni) v)
          s3;
        let s = Id.S.union s1 s2 in
        Id.S.iter (fun pid -> add_kv env pid i v) s
  in
  fun env aid iid v ->
    ppg_precs env aid iid v;
    ppg_succs env aid iid v

and add_kv_concat_aux env i iid v n1 id1 fst1 lst1 n2 id2 fst2 lst2 =
  Debug.dprintf6 debug "add_kv_concat_aux %a(%a) %a" Node.pp i Id.pp iid Node.pp
    v;
  Debug.dprintf8 debug "> %a(%a)[%a; %a]" Node.pp n1 Id.pp id1 Node.pp fst1
    Node.pp lst1;
  Debug.dprintf8 debug "> %a(%a)[%a; %a]" Node.pp n2 Id.pp id2 Node.pp fst2
    Node.pp lst2;
  let cond1 = mk_in_bounds env i fst1 lst1 in
  Egraph.register env cond1;
  let do1 env = add_kv env id1 iid v in
  let do2 env = add_kv env id2 iid v in
  split_cond env cond1 ~cons:do1 ~alt:do2 ~none:(fun env ->
      let cond2 = mk_in_bounds env i fst2 lst2 in
      Egraph.register env cond2;
      new_decision env cond2 "Decision from Theory1.add_kv_concat_aux" ~cons:do2
        ~alt:do1 (fun _ ->
          DecTodo
            [
              (fun env ->
                Boolean.set_true env cond1;
                Boolean.set_false env cond2;
                add_kv env (Id.NSeq.get_id env n1) (Id.Index.get_id env i) v);
              (fun env ->
                Boolean.set_false env cond1;
                Boolean.set_true env cond2;
                add_kv env (Id.NSeq.get_id env n2) (Id.Index.get_id env i) v);
            ]))

and add_kv_slice_aux env i iid v ns nsid sfst slst =
  Debug.dprintf6 debug "add_kv_slice_aux %a(%a) %a" Node.pp i Id.pp iid Node.pp
    v;
  Debug.dprintf8 debug "> %a(%a)[%a; %a]" Node.pp ns Id.pp nsid Node.pp sfst
    Node.pp slst;
  let cond = mk_in_bounds env i sfst slst in
  Egraph.register env cond;
  let cons env = add_kv env (Id.NSeq.get_id env ns) (Id.Index.get_id env i) v in
  new_decision env cond "Decision from Theory1.add_kv_slice_aux" ~cons (fun _ ->
      bool_dec ~cons cond)

and add_kv_update_aux env i iid v n1 id1 n2 id2 fst2 lst2 =
  Debug.dprintf10 debug "add_kv_update_aux (%a(%a) -> %a) %a(%a)" Node.pp i
    Id.pp iid Node.pp v Node.pp n1 Id.pp id1;
  Debug.dprintf8 debug "> %a(%a)[%a; %a]" Node.pp n2 Id.pp id2 Node.pp fst2
    Node.pp lst2;
  let cond = mk_in_bounds env i fst2 lst2 in
  Egraph.register env cond;
  new_decision env cond "Decision from Theory1.add_kv_update_aux"
    ~cons:(fun env -> add_kv env id2 iid v)
    ~alt:(fun env -> add_kv env id1 iid v)
    (fun _ ->
      bool_dec
        ~cons:(fun env ->
          add_kv env (Id.NSeq.get_id env n2) (Id.Index.get_id env i) v)
        ~alt:(fun env ->
          add_kv env (Id.NSeq.get_id env n1) (Id.Index.get_id env i) v)
        cond)

let add_kvs env m id =
  Debug.dprintf4 debug "add_kvs to %a: {%a}" Id.pp id
    (Id.M.pp (fun fmt (n, s, m) ->
         Fmt.pf fmt "(%a, {%a}, {%a})" Node.pp n Id.S.pp s (Id.M.pp Id.S.pp) m))
    m;
  Id.M.iter (fun iid (v, _, _) -> add_kv env id iid v) m

let add_ns_concat env n n1 n2 =
  Debug.dprintf6 debug "add_ns_concat %a %a %a" (Id.NSeq.pp env) n
    (Id.NSeq.pp env) n1 (Id.NSeq.pp env) n2;
  let id = Id.NSeq.get_id env n in
  let id1 = Id.NSeq.get_id env n1 in
  let id2 = Id.NSeq.get_id env n2 in
  if Id.equal id id1 then set_is_empty env id2
  else if Id.equal id id2 then set_is_empty env id1
  else (
    (match (get_defval env id1, get_defval env id2) with
    | Some v1, Some v2 when Egraph.is_equal env v1 v2 ->
        (* concatenation of two consts, produces a new const. *)
        ignore (new_const_nseq env n v1)
    | _ -> ());
    let fst1, _, lst1, _ = Bounds.get_mk_bounds_n env n1 in
    let kvs1 = Opt.get_def Id.M.empty (get_kvs env id1) in
    add_kvs env kvs1 id;
    let fst2, _, lst2, _ = Bounds.get_mk_bounds_n env n2 in
    let kvs2 = Opt.get_def Id.M.empty (get_kvs env id2) in
    add_kvs env kvs2 id;
    let kvs = Opt.get_def Id.M.empty (get_kvs env id) in
    Id.M.iter
      (fun iid (v, _, _) ->
        let i = Id.get env iid in
        add_kv_concat_aux env i iid v n1 id1 fst1 lst1 n2 id2 fst2 lst2)
      kvs;
    Relations.add_concat env id id1 id2)

let add_ns_slice env sln slfst sllst a =
  Debug.dprintf4 debug "add_ns_slice %a = slice(%a, _, _)" (Id.NSeq.pp env) sln
    (Id.NSeq.pp env) a;
  let aid = Id.NSeq.get_id env a in
  let slid = Id.NSeq.get_id env sln in
  if Id.equal aid slid then ()
  else
    let sl_kvs = Opt.get_def Id.M.empty (get_kvs env slid) in
    add_kvs env sl_kvs aid;
    let kvs = Opt.get_def Id.M.empty (get_kvs env aid) in
    Id.M.iter
      (fun iid (v, _, _) ->
        let i = Id.get env iid in
        add_kv_slice_aux env i iid v a slid slfst sllst)
      kvs;
    Relations.add_slice env aid slid

let retreive_upd_kvs env n id kvm fst2 lst2 =
  Debug.dprintf10 debug "retreive_upd_kvs %a %a [%a; %a] {%a}" (Id.NSeq.pp env)
    n Id.pp id (Id.Index.pp env) fst2 (Id.Index.pp env) lst2
    (Id.M.pp (fun fmt (n, s, m) ->
         Fmt.pf fmt "(%a, {%a}, {%a})" Node.pp n Id.S.pp s (Id.M.pp Id.S.pp) m))
    kvm;
  Id.M.iter
    (fun iid (v, _, _) ->
      let i = Id.get env iid in
      let cond1 = mk_lt env [ lst2; i ] in
      Egraph.register env cond1;
      let cons env =
        add_kv env (Id.NSeq.get_id env n) (Id.Index.get_id env i) v
      in
      split_cond env cond1
        ~cons:(fun env -> add_kv env id iid v)
        ~alt:(fun env ->
          let cond2 = mk_lt env [ i; fst2 ] in
          Egraph.register env cond2;
          new_decision env cond2 "Decision from Theory1.retreive_upd_kvs 1"
            ~cons (fun _ -> bool_dec ~cons cond2))
        ~none:(fun env ->
          let cond2 = mk_lt env [ i; fst2 ] in
          Egraph.register env cond2;
          new_decision env cond2 "Decision from Theory1.retreive_upd_kvs 2"
            ~cons
            ~alt:(fun env ->
              new_decision env cond1 "Decision from Theory1.retreive_upd_kvs 3"
                ~cons (fun _ -> bool_dec ~cons cond1))
            (fun _ ->
              DecTodo
                [
                  (fun env ->
                    Boolean.set_true env cond1;
                    cons env);
                  (fun env -> Boolean.set_false env cond1);
                  (fun env ->
                    Boolean.set_true env cond2;
                    cons env);
                  (fun env -> Boolean.set_false env cond2);
                ])))
    kvm

let add_ns_update env val_gty n n1 fst1 lst1 n2 fst2 lst2 =
  if Egraph.is_equal env fst1 fst2 && Egraph.is_equal env lst1 lst2 then
    Egraph.merge env n n2
  else if Egraph.is_equal env n n1 then (
    let ns = mk_nseq_slice env val_gty n fst2 lst2 in
    Egraph.register env ns;
    Egraph.merge env ns n2)
  else (
    Debug.dprintf6 debug "add_ns_update %a %a %a" (Id.NSeq.pp env) n
      (Id.NSeq.pp env) n1 (Id.NSeq.pp env) n2;
    let id = Id.NSeq.get_id env n in
    let id1 = Id.NSeq.get_id env n1 in
    let id2 = Id.NSeq.get_id env n2 in
    let kvs1 = Opt.get_def Id.M.empty (get_kvs env id1) in
    retreive_upd_kvs env n id kvs1 fst2 lst2;
    let kvs2 = Opt.get_def Id.M.empty (get_kvs env id2) in
    add_kvs env kvs2 id;
    let kvs = Opt.get_def Id.M.empty (get_kvs env id) in
    Id.M.iter
      (fun iid (v, _, _) ->
        let i = Id.get env iid in
        add_kv_update_aux env i iid v n1 id1 n2 id2 fst2 lst2)
      kvs;
    Relations.add_update env id id1 id2)

let aux_kns_eq_nseqs_norm env rid kid kkn =
  match Id.M.find_remove rid kkn with
  | _, None -> Some kkn
  | kkn', Some k ->
      (* TODO: should be unreachable *)
      (match Relocate.get_repr env rid with
      | Repr _ ->
          KV.HT.change env rid ~f:(function
            | None -> assert false
            | Some im ->
                let im' =
                  Id.M.change
                    (function
                      | None -> assert false
                      | Some (v, s, m) -> (
                          match Id.M.find_remove kid s with
                          | _, None -> assert false
                          | s', Some _ -> Some (v, s', m)))
                    k im
                in
                if Id.M.is_empty im' then None else Some im')
      | NonRepr { repr } ->
          KV.HT.change env repr ~f:(function
            | None -> None
            | Some im ->
                Some
                  (Id.M.change
                     (function
                       | None -> assert false
                       | Some (v, s, m) ->
                           KV.NRLocs.change env rid ~f:(function
                             | None -> assert false
                             | Some s ->
                                 let s' = Id.S.remove kid s in
                                 if Id.S.is_empty s' then None else Some s');
                           let m' =
                             Id.M.change
                               (function
                                 | None -> assert false
                                 | Some s ->
                                     let s' =
                                       match Id.M.find_remove kid s with
                                       | _, None -> assert false
                                       | s', Some _ -> s'
                                     in
                                     if Id.S.is_empty s' then None else Some s')
                               rid m
                           in
                           Some (v, s, m'))
                     k im)));
      if Id.M.is_empty kkn' then None else Some kkn'

let eq_nseqs_norm env kid rid =
  Debug.dprintf4 debug "NSeqDom.eq_nseqs_norm %a <- %a" Id.pp rid Id.pp kid;
  let open KV in
  (match KNeighbours.find_opt env rid with
  | None ->
      KNeighbours.change env kid ~f:(function
        | None -> None
        | Some kkn -> aux_kns_eq_nseqs_norm env rid kid kkn)
  | Some rkn ->
      let rkn =
        match aux_kns_eq_nseqs_norm env kid rid rkn with
        | None -> Id.M.empty
        | Some rkn -> (
            match Id.M.find_remove kid rkn with
            | _, None -> rkn
            | rkn', Some iid -> (
                match Relocate.get_repr env kid with
                | Repr _ ->
                    HT.change env kid ~f:(function
                      | None -> assert false
                      | Some m ->
                          let m' =
                            Id.M.change
                              (function
                                | None -> assert false
                                | Some (v, s, m) ->
                                    Some (v, Id.M.remove rid s, m))
                              iid m
                          in
                          Some m');
                    rkn'
                | NonRepr { repr = r } ->
                    HT.change env r ~f:(function
                      | None -> assert false
                      | Some m ->
                          let m' =
                            Id.M.change
                              (function
                                | None -> assert false
                                | Some (v, s, m) ->
                                    let nm =
                                      Id.M.change
                                        (function
                                          | None -> assert false
                                          | Some s -> (
                                              match Id.M.find_remove rid s with
                                              | _, None -> assert false
                                              | s', Some _ -> Some s'))
                                        kid m
                                    in
                                    Some (v, s, nm))
                              iid m
                          in
                          Some m');
                    rkn'))
      in
      Debug.dprintf2 debug "rkn = {%a}" (Id.M.pp Id.pp) rkn;
      let kkn =
        match KNeighbours.find_opt env kid with
        | None -> Id.M.empty
        | Some kkn -> (
            match aux_kns_eq_nseqs_norm env rid kid kkn with
            | None -> Id.M.empty
            | Some m -> m)
      in
      Debug.dprintf2 debug "kkn = {%a}" (Id.M.pp Id.pp) kkn;
      let nkn =
        Id.M.fold
          (fun kneighbour k acc ->
            match Relocate.get_repr env kneighbour with
            | Repr _ ->
                let knm = HT.find env kneighbour in
                let knm' =
                  let v, s, m = Id.M.find k knm in
                  match Id.M.find_remove rid s with
                  | _, None ->
                      (* rid is in m *)
                      assert false
                  | s', Some nr -> Id.M.add k (v, Id.M.add kid nr s', m) knm
                in
                HT.set env kneighbour knm';
                new_k_neighbour env kneighbour kid k k;
                Id.M.change
                  (function
                    | None -> Some k
                    | Some k' when Id.equal k k' -> Some k
                    | Some k' ->
                        rm_kneighbour env kneighbour k kid;
                        on_multiple_kns env kid kneighbour k' k;
                        Some k')
                  kneighbour acc
            | NonRepr { repr = knr } ->
                let knm = HT.find env knr in
                let knm' =
                  let v, s, m = Id.M.find k knm in
                  let m' =
                    Id.M.change
                      (function
                        | None -> assert false
                        | Some s -> (
                            match Id.M.find_remove rid s with
                            | _, None -> assert false
                            | s', Some _ when Id.S.is_empty s' -> None
                            | s', Some _ -> Some s'))
                      kneighbour m
                  in
                  Id.M.add k (v, s, m') knm
                in
                NRLocs.change env kneighbour ~f:(function
                  | None -> assert false
                  | Some s ->
                      assert (Id.S.mem rid s);
                      let s' = Id.S.remove rid s in
                      if Id.S.is_empty s' then None else Some s');
                HT.set env knr knm';
                if Id.equal knr kid then acc
                else
                  Id.M.change
                    (function
                      | None -> Some k
                      | Some k' when Id.equal k k' -> Some k
                      | _ -> assert false)
                    knr acc)
          rkn kkn
      in
      KNeighbours.remove env rid;
      if not (Id.M.is_empty nkn) then KNeighbours.set env kid nkn);
  match NRLocs.find_opt env rid with
  | None -> ()
  | Some rs ->
      let ks =
        match NRLocs.find_opt env kid with None -> Id.S.empty | Some ks -> ks
      in
      let nks =
        Id.S.fold
          (fun bi_id acc ->
            KNeighbours.change env bi_id ~f:(function
              | None -> assert false
              | Some nrm -> (
                  match Id.M.find_remove rid nrm with
                  | _, None -> assert false
                  | nrm', Some i -> (
                      match Relocate.get_repr env kid with
                      | Repr _ -> assert false
                      | NonRepr { repr = a } ->
                          HT.change env a ~f:(function
                            | None -> assert false
                            | Some m ->
                                let nm =
                                  Id.M.change
                                    (function
                                      | None -> assert false
                                      | Some (v, s, m) -> (
                                          match Id.M.find_remove rid m with
                                          | _, None -> assert false
                                          | m', Some bi_set ->
                                              let nm =
                                                Id.M.change
                                                  (function
                                                    | None -> Some bi_set
                                                    | Some bi_set' ->
                                                        Some
                                                          (Id.S.union bi_set
                                                             bi_set'))
                                                  kid m'
                                              in
                                              Some (v, s, nm)))
                                    i m
                                in
                                Some nm);
                          Some
                            (Id.M.change
                               (function
                                 | None -> Some i
                                 | Some i' when Id.equal i i' -> Some i
                                 | _ -> assert false)
                               kid nrm'))));
            Id.S.add bi_id acc)
          rs ks
      in
      NRLocs.remove env rid;
      NRLocs.set env kid nks

let eq_indices_norm =
  let upd_kneighbours_inds =
    let upd_kneighbour_inds env kid rid nsid knid =
      Debug.dprintf8 debug "upd_kneighbour (%a <- %a) %a %a" Id.pp rid Id.pp kid
        Id.pp nsid Id.pp knid;
      KV.KNeighbours.change env knid ~f:(function
        | None -> assert false
        | Some m -> (
            match Id.M.find_remove nsid m with
            | _, None -> assert false
            | m', Some rid' when Id.equal rid' rid ->
                Some (Id.M.add nsid kid m')
            | _ -> assert false))
    in
    fun env kid rid nsid s m ->
      Debug.dprintf10 debug "upd_kneighbours_inds %a (%a <- %a) {%a} {%a}" Id.pp
        nsid Id.pp rid Id.pp kid Id.S.pp s (Id.M.pp Id.S.pp) m;
      Id.S.iter (fun knid -> upd_kneighbour_inds env kid rid nsid knid) s;
      Id.M.iter
        (fun nonrepr knids ->
          Id.S.iter
            (fun knid -> upd_kneighbour_inds env kid rid nonrepr knid)
            knids)
        m
  in
  fun env kid rid ->
    Debug.dprintf4 debug "NSeqDom.eq_indices_norm %a <- %a" Id.pp rid Id.pp kid;
    let open KV in
    match Inds.find_opt env rid with
    | None -> ()
    | Some rm ->
        let km =
          match Inds.find_opt env kid with None -> Id.M.empty | Some m -> m
        in
        let nm =
          Id.M.fold
            (fun rnsid rv acc ->
              HT.change env rnsid ~f:(function
                | None -> assert false
                | Some im -> (
                    match (Id.M.find_opt kid im, Id.M.find_opt rid im) with
                    | None, None -> assert false
                    | Some _, None -> None
                    | None, Some (v, s, m) ->
                        assert (Egraph.is_equal env rv v);
                        (match KV.getv env rnsid kid with
                        | None -> ()
                        | Some v' -> Egraph.merge env v v');
                        upd_kneighbours_inds env kid rid rnsid s m;
                        Some (Id.M.add kid (v, s, m) (Id.M.remove rid im))
                    | Some (v1, s1, m1), Some (v2, s2, m2) ->
                        assert (
                          Egraph.is_equal env rv v1 || Egraph.is_equal env rv v2);
                        upd_kneighbours_inds env kid rid rnsid s2 m2;
                        Egraph.merge env v1 v2;
                        Some
                          (Id.M.add kid
                             ( rv,
                               Id.M.union (fun _ _ _ -> assert false) s1 s2,
                               Id.M.union
                                 (fun _ s1 s2 ->
                                   Some
                                     (Id.M.union
                                        (fun _ _ _ -> assert false)
                                        s1 s2))
                                 m1 m2 )
                             (Id.M.remove rid im))));
              Id.M.change
                (function
                  | None -> Some rv
                  | Some v' ->
                      Egraph.merge env rv v';
                      Some rv)
                rnsid acc)
            rm km
        in
        Inds.remove env rid;
        Inds.set env kid nm

let if_distinct_ppg env k1 v k2 knid =
  Debug.dprintf8 debug "if_distinct_ppg %a %a %a %a" Id.pp k1 Node.pp v Id.pp k2
    Id.pp knid;
  if not (Id.equal k1 k2) then
    let k1node = Id.get env k1 in
    let k2node = Id.get env k2 in
    if Disequality.is_disequal env k1node k2node then add_kv env knid k1 v
    else
      let knode = Id.get env knid in
      Choice.register_global env
        {
          print_cho = (fun fmt () -> Fmt.pf fmt "NSeq_dom.if_distinct_ppg");
          prio = 0;
          key = None;
          choice =
            (fun env ->
              if Egraph.is_equal env k1node k2node then DecNo
              else if Disequality.is_disequal env k1node k2node then (
                add_kv env (Id.NSeq.get_id env knode)
                  (Id.Index.get_id env k1node)
                  v;
                DecNo)
              else
                DecTodo
                  [
                    (fun env -> Egraph.merge env k1node k2node);
                    (fun env ->
                      make_disequal env k1node k2node;
                      add_kv env (Id.NSeq.get_id env knode)
                        (Id.Index.get_id env k1node)
                        v);
                  ]);
        }

let ppg_over_kns_m env k1 v rknsm brm newkns =
  Debug.dprintf6 debug "ppg_over_kns_m %a %a {%a}" Id.pp k1 Node.pp v
    (Id.M.pp Id.pp) rknsm;
  Id.M.iter (fun knid k2 -> if_distinct_ppg env k1 v k2 knid) rknsm;
  Id.M.iter
    (fun k' (v', kns, _) ->
      Id.S.iter (fun knid -> if_distinct_ppg env k1 v k' knid) kns;
      Id.S.iter (fun knid -> if_distinct_ppg env k' v' k1 knid) newkns)
    brm

let ppg_over_new_kns env k2 kns m =
  Id.S.iter
    (fun knid ->
      Id.M.iter (fun k1 (v, _, _) -> if_distinct_ppg env k1 v k2 knid) m)
    kns

let merge_reprs env old_repr repr =
  Debug.dprintf4 debug "NSeq_dom.merge_reprs (%a <- %a)" Id.pp old_repr Id.pp
    repr;
  match KV.HT.find_opt env old_repr with
  | None -> ()
  | Some oldr_sm ->
      KV.NRLocs.remove env old_repr;
      KV.NRLocs.remove env repr;
      KV.HT.remove env old_repr;
      Debug.dprintf2 debug "oldr_sm: {%a}" KV.pp oldr_sm;
      KV.HT.change env repr ~f:(fun newr_sm_opt ->
          let newr_sm = Opt.get_def Id.M.empty newr_sm_opt in
          Debug.dprintf2 debug "newr_sm: {%a}" KV.pp newr_sm;
          let repr_kns_m =
            match KV.KNeighbours.find_opt env repr with
            | None -> Id.M.empty
            | Some m -> m
          in
          let res_sm =
            Id.M.fold
              (fun i (v, s, m) acc ->
                let s =
                  match Id.M.find_remove repr s with
                  | _, None -> s
                  | s', Some () ->
                      KV.KNeighbours.change env repr ~f:(function
                        | None -> assert false
                        | Some m ->
                            let m' =
                              match Id.M.find_remove old_repr m with
                              | _, None -> assert false
                              | m', Some _ -> m'
                            in
                            if Id.M.is_empty m' then None else Some m');
                      s'
                in
                let s =
                  Id.S.fold
                    (fun kn_id acc ->
                      match KV.KNeighbours.find_opt env kn_id with
                      | None -> assert false
                      | Some m ->
                          let m', rm =
                            match Id.M.find_remove old_repr m with
                            | _, None -> assert false
                            | m', Some k' -> (
                                match Id.M.find_opt repr m' with
                                | None -> (Id.M.add repr k' m', false)
                                | Some k'' when Id.equal k' k'' -> (m', false)
                                | Some k'' ->
                                    on_multiple_kns env kn_id old_repr k'' k';
                                    (Id.M.add repr k'' m', true))
                          in
                          KV.KNeighbours.set env kn_id m';
                          if rm then Id.M.remove kn_id acc else acc)
                    s s
                in
                KV.Inds.change env i ~f:(function
                  | None -> assert false
                  | Some m -> (
                      match Id.M.find_remove old_repr m with
                      | _, None -> assert false
                      | m', Some v' ->
                          assert (Egraph.is_equal env v v');
                          Some m'));
                let res_acc =
                  Id.M.change
                    (function
                      | Some (v', s', m') ->
                          Egraph.merge env v v';
                          let news =
                            Id.S.filter (fun id -> not (Id.S.mem id s')) s
                          in
                          ppg_over_new_kns env i news acc;
                          (* TODO: do it for m as well *)
                          Some
                            ( v',
                              Id.S.union s s',
                              Id.M.union
                                (fun _ s1 s2 -> Some (Id.S.union s1 s2))
                                m m' )
                      | None ->
                          (match KV.getv env repr i with
                          | None -> ()
                          | Some v' -> Egraph.merge env v v');
                          KV.Inds.change env i ~f:(function
                            | None -> Some (Id.M.singleton repr v)
                            | Some m ->
                                Some
                                  (Id.M.change
                                     (function
                                       | None -> Some v | Some _ -> assert false)
                                     repr m));
                          ppg_over_kns_m env i v repr_kns_m acc s;
                          Some (v, s, m))
                    i acc
                in
                Debug.dprintf2 debug "res_acc: {%a}\n"
                  (Id.M.pp (fun fmt (v, s, m) ->
                       Fmt.pf fmt "(%a, {%a}, {%a})" Node.pp v Id.S.pp s
                         (Id.M.pp Id.S.pp) m))
                  res_acc;
                res_acc)
              oldr_sm newr_sm
          in
          Debug.dprintf2 debug "res_sm: {%a}" KV.pp res_sm;
          Some res_sm)

let are_k_neighbours env r1 r2 d =
  if
    Opt.fold
      (fun acc m1 -> acc || Id.M.mem r2 m1)
      false
      (KV.KNeighbours.find_opt env r1)
    || Opt.fold
         (fun acc m2 -> acc || Id.M.mem r1 m2)
         false
         (KV.KNeighbours.find_opt env r2)
  then (
    let pn = DP.node_of_polynome env d in
    Egraph.merge env pn Colibri2_theories_LRA.RealValue.zero;
    true)
  else false

let have_the_same_bounds env r1 r2 d =
  match (Bounds.get_bounds env r1, Bounds.get_bounds env r2) with
  | Some (lb1, ub1), Some (lb2, ub2) when Id.equal lb1 lb2 && Id.equal ub1 ub2
    ->
      if Id.equal lb1 lb2 && Id.equal lb1 ub1 then (
        let pn = DP.node_of_polynome env d in
        Egraph.merge env pn Colibri2_theories_LRA.RealValue.zero;
        true)
      else false
  | _ -> false

let on_reloc_repr_change env ~old_repr ~repr d =
  Debug.dprintf6 debug "NSeqDom.on_reloc_repr_change %a -> %a: (%a)"
    (Id.pp_nid env) old_repr (Id.pp_nid env) repr P.pp d;
  Opt.iter
    (fun t ->
      NSeqs.HT.remove env old_repr;
      NSeqs.update env repr t)
    (NSeqs.HT.find_opt env old_repr);
  if
    P.is_zero d
    || are_k_neighbours env old_repr repr d
    || have_the_same_bounds env old_repr repr d
  then merge_reprs env old_repr repr
  else (
    KV.NRLocs.remove env repr;
    match KV.HT.find_opt env old_repr with
    | None -> ()
    | Some oldr_sm ->
        let nd = DP.node_of_polynome env d in
        KV.HT.remove env old_repr;
        KV.HT.change env repr ~f:(fun newr_sm_opt ->
            let newr_sm = Opt.get_def Id.M.empty newr_sm_opt in
            let res_sm =
              Id.M.fold
                (fun i (v, s, m) acc ->
                  let ni = Id.get env i in
                  let ni' = mk_add env ni nd in
                  Egraph.register env ni';
                  let i' = Id.Index.get_id_safe env ni' in
                  KV.NRLocs.change env old_repr ~f:(function
                    | None -> Some s
                    | Some s' -> Some (Id.S.union s s'));
                  Id.S.iter
                    (fun id ->
                      KV.KNeighbours.change env id ~f:(function
                        | None -> assert false
                        | Some m ->
                            Some
                              (Id.M.change
                                 (function
                                   | None -> assert false
                                   | Some j ->
                                       assert (Id.equal i j);
                                       Some i')
                                 old_repr m)))
                    s;
                  let s', m' =
                    match Id.M.find_remove repr m with
                    | _, None ->
                        ( Id.S.empty,
                          Id.M.change
                            (function
                              | None -> Some s
                              | Some s' -> Some (Id.S.union s s'))
                            old_repr m )
                    | m', Some s' ->
                        ( s',
                          Id.M.change
                            (function
                              | None -> Some s
                              | Some s'' -> Some (Id.S.union s s''))
                            old_repr m' )
                  in
                  KV.Inds.change env i ~f:(function
                    | None -> assert false
                    | Some m -> (
                        match Id.M.find_remove old_repr m with
                        | _, None -> assert false
                        | m', Some v' ->
                            assert (Egraph.is_equal env v v');
                            if Id.M.is_empty m' then None else Some m'));
                  Id.M.change
                    (function
                      | Some (v', s'', m'') ->
                          Egraph.merge env v v';
                          Some
                            ( v',
                              Id.S.union s' s'',
                              Id.M.union
                                (fun _ s1 s2 -> Some (Id.S.union s1 s2))
                                m' m'' )
                      | None ->
                          (match KV.getv env repr i' with
                          | None -> ()
                          | Some v' -> Egraph.merge env v v');
                          KV.Inds.change env i' ~f:(function
                            | None -> Some (Id.M.singleton repr v)
                            | Some m ->
                                Some
                                  (Id.M.change
                                     (function
                                       | None -> Some v | Some _ -> assert false)
                                     repr m));
                          Some (v, s', m'))
                    i' acc)
                oldr_sm newr_sm
            in
            Some res_sm))

let add_kv_check_bounds env aid i v =
  Debug.dprintf6 debug "add_kv_check_bounds %a %a %a" Id.pp aid Node.pp i
    Node.pp v;
  let a = Id.get env aid in
  let fst, _, lst, _ = Bounds.get_mk_bounds_n env a in
  let cond = mk_in_bounds env i fst lst in
  Egraph.register env cond;
  let cons env =
    let aid = Id.NSeq.get_id_safe env a in
    let iid = Id.Index.get_id_safe env i in
    add_kv env aid iid v
  in
  new_decision env cond "Decision from add_kv_check_bounds" ~cons (fun _ ->
      bool_dec ~cons cond)

let pp_nsht = NSeqs.HT.pp
let pp_ht = KV.HT.pp
let pp_kns = KV.KNeighbours.pp
