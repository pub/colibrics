(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_core
open Colibri2_popop_lib
open Popop_stdlib
module Case : Datatype

val case_of_int : int -> Case.t

module Field : Datatype

val field_of_int : int -> Field.t

module MonoAdt : sig
  type t = private { mono : Ground.Ty.t; cases : Ground.Ty.t list IArray.t }
  [@@deriving eq, ord, hash, show]

  val index : Expr.Ty.Const.t -> Ground.Ty.t list -> t option

  val case : t -> Case.t -> Ground.Ty.t list
  (** Return the type of the argument of the given cases *)
end

val cases_of_adt : MonoAdt.t -> Case.S.t

type ts = { adt : MonoAdt.t; case : Case.t; fields : Value.t Field.M.t }

include Value.S with type s := ts

val th_register : Egraph.wt -> unit
val propagate_value : Egraph.wt -> Ground.t -> unit

val sequence_of_cases :
  _ Egraph.t -> MonoAdt.t -> Case.t Base.Sequence.t -> Value.t Base.Sequence.t
