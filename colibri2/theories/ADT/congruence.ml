(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib

open Adt_value
(** information added on nodes *)

module MonoADT_Pos = struct
  module T = struct
    type t = { f : Adt_value.MonoAdt.t; case : Case.t; pos : Field.t }
    [@@deriving eq, ord, hash]

    let pp fmt { f; case; pos } =
      Fmt.pf fmt "%a.%a.%a" Adt_value.MonoAdt.pp f Case.pp case Field.pp pos
  end

  include T
  include Popop_stdlib.MkDatatype (T)
end

type parent = Node.S.t MonoADT_Pos.M.t [@@deriving show]
(** The node must be an ADT with a domain known *)

let _ = pp_parent

(** because it is currently not hidden and too long *)
let pp_parent = Fmt.nop

let congruence_closure d ~other parent0 ~repr parent1 =
  let find_as_if cl =
    (* as if the merge is already done *)
    let cl = Egraph.find_def d cl in
    if Node.equal cl other then repr else cl
  in
  let create_h () =
    Hashtbl.create
      (module struct
        type t = Node.t

        let sexp_of_t = Node.sexp_of_t

        (* hash and comparison modulo find_as_if *)
        let hash n : int =
          match Egraph.get_dom d Adt_dom.key n with
          | Some (Unk _) | None -> raise Impossible
          | Some (One { fields; _ }) ->
              Hash.of_fold
                (Field.M.fold_left (fun state _ n ->
                     Node.hash_fold_t state (find_as_if n)))
                fields

        let compare n1 n2 : int =
          match
            (Egraph.get_dom d Adt_dom.key n1, Egraph.get_dom d Adt_dom.key n2)
          with
          | ( Some (One { fields = fields1; _ }),
              Some (One { fields = fields2; _ }) ) ->
              Field.M.compare
                (fun a1 a2 ->
                  if Node.equal a1 a2 then 0
                  else Node.compare (find_as_if a1) (find_as_if a2))
                fields1 fields2
          | _ -> raise Impossible
      end)
  in
  MonoADT_Pos.M.union
    (fun _ g1 g2 ->
      let h = create_h () in
      let s = ref Node.S.empty in
      let first g = s := Node.S.add g !s in
      Node.S.iter
        (fun g ->
          Hashtbl.change
            ~f:(function
              | None ->
                  first g;
                  Some g
              | Some _ as s ->
                  (* List.iter (Egraph.merge d (Ground.node g)) l; *)
                  s)
            h g)
        g1;
      Node.S.iter
        (fun g ->
          match Hashtbl.find h g with
          | None -> first g
          | Some g0 -> Egraph.merge d g g0)
        g2;
      assert (not (Node.S.is_empty !s));
      Some !s)
    parent0 parent1

let merge d ~other ~repr info info' =
  congruence_closure d ~other ~repr info info'

let dom =
  Dom.Kind.create
    (module struct
      type nonrec t = parent

      let name = "Quantifier.info"
    end)

let () =
  Dom.register
    (module struct
      type t = parent [@@deriving show]

      let key = dom

      let merged (b1 : t option) (b2 : t option) =
        match (b1, b2) with
        | Some b1, Some b2 -> CCEqual.physical b1 b2
        (* We always want to go once through merge *)
        | None, None -> true
        | _ -> false

      let merge d (dom0, cl0) (dom1, cl1) inv =
        let repr, other = if inv then (cl0, cl1) else (cl1, cl0) in
        let repr = Egraph.find d repr in
        let other = Egraph.find d other in
        match (dom0, dom1) with
        | None, None -> ()
        | Some info0, None -> Egraph.set_dom d dom cl1 info0
        | None, Some info1 -> Egraph.set_dom d dom cl0 info1
        | Some info0, Some info1 ->
            let info = merge d ~other ~repr info0 info1 in
            Egraph.set_dom d dom cl0 info;
            Egraph.set_dom d dom cl1 info

      let wakeup_threshold = None
    end)

let add_info_on_complete_adt d n adt case fields =
  Field.M.iter
    (fun field other ->
      let info =
        MonoADT_Pos.M.singleton
          { f = adt; case; pos = field }
          (Node.S.singleton n)
      in
      let repr = Egraph.find_def d other in
      match Egraph.get_dom d dom repr with
      | None -> Egraph.set_dom d dom repr info
      | Some info' ->
          Egraph.set_dom d dom repr (merge d ~repr ~other info info'))
    fields

let init d =
  DaemonAlsoInterp.attach_any_dom d Adt_dom.key (fun d n ->
      match Egraph.get_dom d Adt_dom.key n with
      | None | Some (Unk _) -> ()
      | Some (One { adt; case; fields }) ->
          if
            List.length (Adt_value.MonoAdt.case adt case)
            = Field.M.cardinal fields
          then add_info_on_complete_adt d n adt case fields)
