(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Adt_value

let debug = Debug.register_info_flag "adt" ~desc:"Algebraic@ Datatype"

module D = struct
  type t =
    | Unk of { adt : Adt_value.MonoAdt.t; cases : Case.S.t }
    | One of {
        adt : Adt_value.MonoAdt.t;
        case : Case.t;
        fields : Node.t Field.M.t;
      }
  [@@deriving eq]

  let pp fmt = function
    | Unk { cases; adt = _ } -> Fmt.pf fmt "{%a}" Case.S.pp cases
    | One c -> Fmt.pf fmt "%a(%a)" Case.pp c.case (Field.M.pp Node.pp) c.fields

  let key =
    Dom.Kind.create
      (module struct
        type nonrec t = t

        let name = "adt"
      end)

  let inter d d1 d2 =
    match (d1, d2) with
    | Unk { cases = s1; adt = adt1 }, Unk { cases = s2; adt = adt2 } ->
        assert (Adt_value.MonoAdt.equal adt1 adt2);
        let s = Case.S.inter s1 s2 in
        if Case.S.is_empty s then None else Some (Unk { cases = s; adt = adt1 })
    | ( One { case = c1; fields = f1; adt = adt1 },
        One { case = c2; fields = f2; adt = adt2 } ) ->
        assert (Adt_value.MonoAdt.equal adt1 adt2);
        if Case.equal c1 c2 then
          Some
            (One
               {
                 adt = adt1;
                 case = c1;
                 fields =
                   Field.M.union
                     (fun _ n1 n2 ->
                       Egraph.merge d n1 n2;
                       Some n1)
                     f1 f2;
               })
        else None
    | (One { case = c1; adt = adt1; _ } as d1), Unk { adt = adt2; cases = c2 }
    | Unk { cases = c2; adt = adt1 }, (One { case = c1; adt = adt2; _ } as d1)
      ->
        assert (Adt_value.MonoAdt.equal adt1 adt2);
        if Case.S.mem c1 c2 then Some d1 else None

  let is_singleton d _ = function
    | Unk s ->
        assert (not (Case.S.is_empty s.cases));
        if Case.S.is_num_elt 1 s.cases then
          let case = Case.S.choose s.cases in
          match Adt_value.MonoAdt.case s.adt case with
          | [] ->
              Some
                (Adt_value.nodevalue @@ Adt_value.index
                @@ Adt_value.{ adt = s.adt; case; fields = Field.M.empty })
          | _ -> None
        else None
    | One { case; fields; adt } ->
        if
          List.length (Adt_value.MonoAdt.case adt case)
          = Field.M.cardinal fields
        then
          try
            let fields =
              Field.M.map
                (fun f ->
                  match Egraph.get_value d f with
                  | None -> raise Stdlib.Exit
                  | Some v -> v)
                fields
            in
            Some
              (Adt_value.nodevalue @@ Adt_value.index
              @@ Adt_value.{ adt; case; fields })
          with Stdlib.Exit -> None
        else None

  let wakeup_threshold = None
end

include D
include Dom.Lattice (D)
