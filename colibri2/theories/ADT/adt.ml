(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

module IArray = Colibri2_popop_lib.IArray
open Adt_value
open Adt_dom

(** Decide for destructors *)
module Decide = struct
  let make_decision n adt cases d =
    Debug.dprintf4 Debug.decision "[ADT] decide %a on %a" Case.S.pp cases
      Node.pp n;
    upd_dom d n (Unk { adt; cases })

  let new_decision d f n adt c =
    let register () =
      Choice.register d f
        {
          Choice.print_cho = (fun fmt () -> Fmt.pf fmt "adt on %a" Node.pp n);
          Choice.prio = 1;
          choice =
            (fun d ->
              let decisions s =
                Choice.DecTodo
                  [
                    make_decision n adt (Case.S.singleton c);
                    make_decision n adt (Case.S.remove c s);
                  ]
              in
              match Egraph.get_dom d Adt_dom.key n with
              | Some (One _) -> Choice.DecNo
              | Some (Unk s) when Case.S.is_num_elt 1 s.cases -> Choice.DecNo
              | Some (Unk s) when not (Case.S.mem c s.cases) -> Choice.DecNo
              | Some (Unk s) -> decisions s.cases
              | None -> decisions (cases_of_adt adt));
          key = Some (Ground.node f);
        }
    in
    match Egraph.get_dom d Adt_dom.key n with
    | Some (One _) -> ()
    | Some (Unk s) when Case.S.is_num_elt 1 s.cases -> ()
    | Some (Unk s) when not (Case.S.mem c s.cases) -> ()
    | Some (Unk _) -> register ()
    | None ->
        let cases = cases_of_adt adt in
        if Case.S.is_num_elt 1 cases then upd_dom d n (Unk { adt; cases })
        else register ()
end

(** {2 Initialization} *)

let converter d (f : Ground.t) =
  let r = Ground.node f in
  let reg n = Egraph.register d n in
  let open MonadAlsoInterp in
  let setb = setv Boolean.dom in
  let getb = getv Boolean.dom in
  let set = updd upd_dom in
  let get = getd Adt_dom.key in
  match Ground.sem f with
  | { app = { builtin = Expr.Tester { adt; case; _ }; _ }; args; tyargs; _ } ->
      let case = case_of_int case in
      let adt = Option.value_exn (Adt_value.MonoAdt.index adt tyargs) in
      let e = IArray.extract1_exn args in
      reg e;
      Decide.new_decision d f e adt case;
      attach [%here] d
        (set e
           (let+ vr = getb r in
            if vr then Adt_dom.Unk { adt; cases = Case.S.singleton case }
            else
              let cases = Case.S.remove case (cases_of_adt adt) in
              if Case.S.is_empty cases then (
                Debug.dprintf4 Debug.contradiction
                  "[ADT] tester %a removed the only case %a of the type" Node.pp
                  r Case.pp case;
                Egraph.contradiction ())
              else Adt_dom.Unk { adt; cases }));
      attach [%here] d
        (setb r
           (let* ve = get e in
            Debug.dprintf6 Debug.contradiction
              "[ADT] tester %a wakeup for case %a with %a" Node.pp r Case.pp
              case Adt_dom.pp ve;
            match ve with
            | Unk s when Case.S.is_num_elt 1 s.cases ->
                Some (Case.S.mem case s.cases)
            | Unk s when not (Case.S.mem case s.cases) -> Some false
            | Unk _ -> None
            | One { case = case'; _ } -> Some (Case.equal case case')));
      Adt_value.propagate_value d f
  | {
   app = { builtin = Expr.Constructor { case; adt; _ }; _ };
   args;
   tyargs;
   _;
  } ->
      let case = case_of_int case in
      let adt = Option.value_exn (Adt_value.MonoAdt.index adt tyargs) in
      IArray.iter ~f:reg args;
      let fields =
        IArray.foldi ~init:Field.M.empty args ~f:(fun field acc a ->
            let field = field_of_int field in
            Field.M.add field a acc)
      in
      upd_dom d r (One { adt; case; fields });
      Adt_value.propagate_value d f
  | {
   app = { builtin = Expr.Destructor { case; field; adt; _ }; _ };
   args;
   tyargs;
   ty;
   _;
  } ->
      (* not completely satisfactory but needed, it is not yet clear who has the responsibility of choice for booleans *)
      if Stdlib.( && ) (Boolean.dec_at_literal d) Ground.Ty.(equal ty bool) then
        Choice.register_thterm d (Ground.thterm f) (Boolean.chobool r);
      let case = case_of_int case in
      let field = field_of_int field in
      let adt = Option.value_exn (Adt_value.MonoAdt.index adt tyargs) in
      let e = IArray.extract1_exn args in
      reg e;
      Decide.new_decision d f e adt case;
      attach [%here] d
        (set e
           (let* ve = get e in
            match ve with
            | Unk s when Case.S.is_num_elt 1 s.cases ->
                if Case.S.mem case s.cases then
                  Some
                    (Adt_dom.One
                       { case; fields = Field.M.singleton field r; adt })
                else None
            | Unk _ -> None
            | One { case = case'; _ } ->
                if Case.equal case case' then
                  Some
                    (Adt_dom.One
                       { case; fields = Field.M.singleton field r; adt })
                else None));
      Adt_value.propagate_value d f
  | _ -> ()

let init_node d =
  Interp.Register.node d (fun interp_node d n ->
      match Egraph.get_dom d Adt_dom.key n with
      | None -> None
      | Some adt ->
          Debug.dprintf4 debug "[ADT] node %a: %a" Node.pp n Adt_dom.pp adt;
          Some
            (match adt with
            | Adt_dom.Unk { adt; cases } ->
                let seq = Sequence.of_list (Case.S.elements cases) in
                Interp.Seq.of_seq @@ Adt_value.sequence_of_cases d adt seq
            | Adt_dom.One { adt; case; fields } ->
                let open Interp.Seq in
                let args_ty = Adt_value.MonoAdt.case adt case in
                let fields =
                  Field.M.merge
                    (fun _ l r ->
                      match (l, r) with
                      | Some l, _ -> Some (`Node l)
                      | None, Some r -> Some (`Ty r)
                      | None, None -> assert false)
                    fields
                    (Field.M.of_list
                       (List.mapi ~f:(fun i x -> (field_of_int i, x)) args_ty))
                in
                let rec aux seq = function
                  | [] -> seq
                  | (i, `Node arg) :: args ->
                      let seq =
                        let+ l = seq
                        and* a =
                          Debug.dprintf4 debug "[ADT] interp_node %a:%a" Node.pp
                            n Node.pp arg;
                          interp_node d arg
                        in
                        Field.M.add i a l
                      in
                      aux seq args
                  | (i, `Ty arg) :: args ->
                      let seq =
                        let+ l = seq
                        and* a = Interp.Seq.of_seq @@ Interp.ty d arg in
                        Field.M.add i a l
                      in
                      aux seq args
                in
                let+ fields =
                  aux
                    (Interp.Seq.of_seq @@ Sequence.singleton Field.M.empty)
                    (Field.M.bindings fields)
                in
                Adt_value.nodevalue (Adt_value.index { adt; case; fields })))

let th_register env : unit =
  Adt_value.th_register env;
  Ground.register_converter env converter;
  init_node env;
  Congruence.init env

let () = Init.add_default_theory th_register
