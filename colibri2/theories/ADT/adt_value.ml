(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Base
open Colibri2_core
open Colibri2_popop_lib.Popop_stdlib
open Colibri2_popop_lib
module Case = DInt
module Field = DInt

let case_of_int i = i
let field_of_int i = i
let debug = Debug.register_info_flag "Adt_value" ~desc:""

(** Monomorph version *)
module MonoAdt : sig
  type t = private { mono : Ground.Ty.t; cases : Ground.Ty.t list IArray.t }
  [@@deriving eq, ord, hash, show]

  val index : Expr.Ty.Const.t -> Ground.Ty.t list -> t option

  val case : t -> int -> Ground.Ty.t list
  (** Return the type of the argument of the given cases *)
end = struct
  type t = { mono : Ground.Ty.t; cases : Ground.Ty.t list IArray.t }
  [@@deriving ord, hash, show]

  let equal = phys_equal

  let index =
    let h = Ground.Ty.H.create 10 in
    fun adt tyargs ->
      Ground.Ty.H.memo
        (fun mono ->
          match Ground.Ty.definition adt with
          | Abstract -> None
          | Adt { cases; _ } ->
              let map { Expr.cstr; _ } =
                let fun_vars, fun_args, _ = Expr.Ty.poly_sig cstr.id_ty in
                let subst =
                  List.fold2_exn
                    ~f:(fun acc v ty -> Expr.Ty.Var.M.add v ty acc)
                    ~init:Expr.Ty.Var.M.empty fun_vars tyargs
                in
                List.map ~f:(Ground.Ty.convert subst) fun_args
              in
              let cases = IArray.of_array_map ~f:map cases in
              Some { mono; cases })
        h
        { app = adt; args = tyargs }

  let case t i = IArray.get t.cases i
end

let cases_of_adt adt =
  Case.S.of_list (List.init (IArray.length adt.MonoAdt.cases) ~f:(fun i -> i))

let definition { MonoAdt.mono = { app = adt; _ }; _ } =
  match Ground.Ty.definition adt with
  | Abstract -> assert false
  | Adt { cases; _ } -> cases

let _ = MonoAdt.show

type ts = { adt : MonoAdt.t; case : Case.t; fields : Value.t Field.M.t }

module T' = struct
  module T = struct
    type t = ts = { adt : MonoAdt.t; case : Case.t; fields : Value.t Field.M.t }
    [@@deriving eq, ord, hash]

    let pp fmt c = Fmt.pf fmt "ADT.%i(%a)" c.case (Field.M.pp Value.pp) c.fields
  end

  include T
  include MkDatatype (T)

  let name = "ADT.value"
end

include Value.Register (T')

let interp d n = Opt.get_exn Impossible (Egraph.get_value d n)

let compute d g =
  match Ground.sem g with
  | { app = { builtin = Expr.Tester { case; _ }; _ }; args; _ } ->
      let e = IArray.extract1_exn args in
      let v = coerce_nodevalue (interp d e) in
      let v = value v in
      `Some
        (Colibri2_theories_bool.Boolean.values_of_bool (Case.equal case v.case))
  | {
   app = { builtin = Expr.Constructor { adt; case; _ }; _ };
   args;
   tyargs;
   _;
  } -> (
      let fields =
        IArray.foldi ~init:Field.M.empty args ~f:(fun field acc a ->
            Field.M.add field (interp d a) acc)
      in
      match MonoAdt.index adt tyargs with
      | None -> raise Impossible
      | Some adt ->
          let v = { adt; case; fields } in
          `Some (nodevalue (index v)))
  | { app = { builtin = Expr.Destructor { case; field; _ }; _ }; args; _ } ->
      let e = IArray.extract1_exn args in
      let v = coerce_nodevalue (interp d e) in
      let v = value v in
      if Case.equal case v.case then
        let v = Field.M.find_opt field v.fields in
        match v with None -> raise Impossible | Some v -> `Some v
      else `Uninterpreted
  | _ -> `None

let init_check d =
  Interp.Register.check d (fun d t ->
      match compute d t with
      | `None -> NA
      | `Some r ->
          Interp.check_of_bool (Value.equal r (interp d (Ground.node t)))
      | `Uninterpreted ->
          Interp.check_of_bool
            (Colibri2_theories_quantifiers.Uninterp.On_uninterpreted_domain
             .check d t));
  Interp.Register.compute d (fun d t ->
      match compute d t with
      | `None -> NA
      | `Some v -> Value v
      | `Uninterpreted ->
          Colibri2_theories_quantifiers.Uninterp.On_uninterpreted_domain.compute
            d t)

let propagate_value d g =
  let f d g =
    match compute d g with
    | `None -> raise Impossible
    | `Some v -> Egraph.set_value d (Ground.node g) v
    | `Uninterpreted ->
        Colibri2_theories_quantifiers.Uninterp.On_uninterpreted_domain.propagate
          d g
  in
  Interp.WatchArgs.create d f g

let sequence_of_cases d t cases =
  let open Std.Sequence in
  let* case = cases in
  Debug.dprintf4 debug "[ADT] case %a of %a" Case.pp case MonoAdt.pp t;
  let args = MonoAdt.case t case in
  let rec aux seq i = function
    | [] -> seq
    | ty :: args ->
        let seq =
          let+ l = seq and* a = Interp.ty d ty in
          Field.M.add i a l
        in
        aux seq (i + 1) args
  in
  let+ fields = aux (Sequence.singleton Field.M.empty) 0 args in
  nodevalue (index { adt = t; case; fields })

let init_ty d =
  Interp.Register.ty d (fun d ty ->
      match ty with
      | { app = { builtin = Expr.Base; _ } as sym; args } -> (
          match MonoAdt.index sym args with
          | None -> None
          | Some adt ->
              let cases =
                List.init (IArray.length adt.cases) ~f:(fun c ->
                    let args = MonoAdt.case adt c in
                    let i =
                      List.count args ~f:(fun ty -> Ground.Ty.equal ty adt.mono)
                    in
                    (c, i + List.length args))
              in
              let cases =
                List.sort ~compare:(fun (_, i) (_, j) -> Int.compare i j) cases
              in
              Debug.dprintf4 debug "Order cases for %a:%a" MonoAdt.pp adt
                Fmt.(list ~sep:semi (pair ~sep:comma Case.pp Int.pp))
                cases;
              let cases = List.map cases ~f:fst in
              let seq = Sequence.of_list cases in
              Some (sequence_of_cases d adt seq))
      | _ -> None)

let th_register d =
  init_check d;
  init_ty d;
  let pp d _ fmt c =
    let c = value c in
    let case = (definition c.adt).(c.case) in
    let fields =
      List.zip_exn (IArray.get c.adt.cases c.case) (Field.M.values c.fields)
    in
    let print_field fmt (ty, v) = Interp.print_value_smt d ty fmt v in
    Fmt.pf fmt "%a(%a)" Expr.Term.Const.pp case.cstr
      Fmt.(list ~sep:comma print_field)
      fields
  in
  Interp.Register.print_value_smt key pp
