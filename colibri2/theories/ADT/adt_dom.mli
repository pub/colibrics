(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib.Popop_stdlib
open Adt_value

val debug : Debug.flag

type t =
  | Unk of { adt : Adt_value.MonoAdt.t; cases : Case.S.t }
  | One of {
      adt : Adt_value.MonoAdt.t;
      case : Case.t;
      fields : Node.t Field.M.t;
    }

val pp : t Fmt.t
val key : t Dom.Kind.t
val set_dom : Egraph.wt -> Node.t -> t -> DUnit.t
val upd_dom : Egraph.wt -> Node.t -> t -> DUnit.t
