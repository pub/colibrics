(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

include module type of Farith.F with type t = Farith.F.t
include Colibri2_popop_lib.Popop_stdlib.Datatype with type t := t

(** {2 Qcheck helpers use 32bit floats mw = 23 ew = 8}*)

val gen : t QCheck2.Gen.t
val interesting_values : t list

(** {2 Comparisons and operations that differentiate zeros (-0 < +0 )} *)

val lt_diff_zero : t -> t -> bool
val le_diff_zero : t -> t -> bool
val min_diff_zero : t -> t -> t
val max_diff_zero : t -> t -> t
val compare_diff_zero : t -> t -> int
val succ_diff_zero : t -> t
val pred_diff_zero : t -> t

(** Test helpers *)

val is_negative_infinite : t -> bool
val is_positive_infinite : t -> bool
val is_negative_zero : t -> bool
val is_positive_zero : t -> bool
val round_q : neg:bool -> mw:int -> ew:int -> Farith.Mode.t -> Q.t -> t
