(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

include Farith.F
include Colibri2_popop_lib.Popop_stdlib.MkDatatype (Farith.F)

let is_negative_infinite f = is_infinite f && is_negative f
let is_positive_infinite f = is_infinite f && is_positive f
let is_negative_zero f = is_zero f && is_negative f
let is_positive_zero f = is_zero f && is_positive f

let interesting_values =
  let mw = 23 and ew = 8 in
  [
    zero ~mw ~ew false;
    zero ~mw ~ew true;
    inf ~mw ~ew false;
    inf ~mw ~ew true;
    nan ~mw ~ew;
    Farith.F.of_q ~mw ~ew NE Q.one;
    Farith.F.of_q ~mw ~ew NE Q.minus_one;
    Farith.F.of_q ~mw ~ew NE Q.two;
  ]

let gen =
  let mw = 23 and ew = 8 in
  QCheck2.Gen.oneof
    [
      QCheck2.Gen.oneofl interesting_values;
      (let open QCheck2.Gen in
       let+ bits = QCheck2.Gen.int32 in
       of_bits ~mw ~ew @@ Z.of_int32 bits);
    ]

let lt_diff_zero x y =
  assert ((not (is_nan x)) && not (is_nan y));
  lt x y || (is_zero x && is_zero y && is_negative x && is_positive y)

let le_diff_zero x y = equal x y || lt_diff_zero x y
let min_diff_zero x y = if le_diff_zero x y then x else y
let max_diff_zero x y = if le_diff_zero x y then y else x

let compare_diff_zero x y =
  if lt_diff_zero x y then -1 else if equal x y then 0 else 1

let succ_diff_zero x = if is_zero x && is_negative x then neg x else succ x
let pred_diff_zero x = if is_zero x && is_positive x then neg x else pred x

let round_q ~neg ~mw ~ew mode r =
  if Q.equal Q.zero r then zero ~mw ~ew neg else of_q ~mw ~ew mode r
