(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib
open Colibri2_core
(* open Colibri2_stdlib.Std *)

let debug =
  Debug.register_info_flag ~desc:"for intervals for the arithmetic theory"
    "FP.interval"

module D = Fp_interval

let dom =
  Dom.Kind.create
    (module struct
      type t = D.t

      let name = "FP_ARITH"
    end)

let mw_ew_of_node d n =
  match Ground.Ty.S.choose (Ground.tys d n) with
  | exception Not_found -> assert false (* Why?? *)
  | { app = { builtin = Builtin.Float (ew, prec); _ }; _ } -> (prec - 1, ew)
  | _ -> assert false (* Why ?? *)

include Dom.Lattice (struct
  include D

  let key = dom
  let inter _ d1 d2 = inter d1 d2

  let is_singleton e n d =
    match D.is_singleton d with
    | No -> None
    | NaN ->
        let mw, ew = mw_ew_of_node e n in
        Some (Fp_value.nodevalue @@ Fp_value.index (F.nan ~mw ~ew))
    | Sin f -> Some (Fp_value.nodevalue @@ Fp_value.index f)

  let wakeup_threshold = None
end)

let com_cmp = function
  | `Lt -> `Gt
  | `Le -> `Ge
  | `Ge -> `Le
  | `Gt -> `Lt
  | `Eq -> `Eq

let backward_true ~ew ~mw c x =
  let r =
    match c with
    | `Lt -> D.Backward.Cmp_True.lt ~ew ~mw x
    | `Gt -> D.Backward.Cmp_True.gt ~ew ~mw x
    | `Le -> D.Backward.Cmp_True.le ~ew ~mw x
    | `Ge -> D.Backward.Cmp_True.ge ~ew ~mw x
    | `Eq -> D.Backward.Cmp_True.eq ~ew ~mw x
  in
  match r with None -> Egraph.contradiction () | Some r -> r

let backward_false ~ew ~mw c x =
  match c with
  | `Lt -> D.Backward.Cmp_False.lt ~ew ~mw x
  | `Gt -> D.Backward.Cmp_False.gt ~ew ~mw x
  | `Le -> D.Backward.Cmp_False.le ~ew ~mw x
  | `Ge -> D.Backward.Cmp_False.ge ~ew ~mw x
  | `Eq -> D.Backward.Cmp_False.eq ~ew ~mw x

let backward ~mw ~ew cmp vr va =
  if vr then backward_true ~mw ~ew cmp va else backward_false ~mw ~ew cmp va

let test = function
  | Fp_interval.Uncomparable -> None
  | Included -> Some true
  | Disjoint -> Some false

(** {2 Initialization} *)
let converter d (f : Ground.t) =
  let r = Ground.node f in
  let reg n = Egraph.register d n in
  let open MonadAlsoInterp in
  (* let setb = setv Boolean.dom in *)
  let getb = getv Boolean.dom in
  let setb = updd Boolean.set_bool in
  let set = updd upd_dom in
  let get ?def = getd ?def dom in
  let getr = generic_wait Colibri2_theories_LRA.Dom_interval.wait_dom in
  let setr = updd Colibri2_theories_LRA.Dom_interval.upd_dom in
  let getm = getv Rounding_mode.key in
  let cmp d ~mw ~ew cmp ~r ~a ~b =
    if Boolean.dec_at_literal d then Choice.register d f (Boolean.chobool r);
    let top = D.top ~mw ~ew in
    let reg n = Egraph.register d n in
    reg a;
    reg b;
    attach [%here] d ~thterm:(Ground.thterm f)
      ((* setb r
        *    (let* va = get a and+ vb = get b in
        *     test (D.is_comparable va vb))
        * && *)
       set b
         (let+ va = get ~def:top a and+ vr = getb r in
          backward ~mw ~ew cmp vr va)
      && set a
           (let+ vb = get ~def:top b and+ vr = getb r in
            backward ~mw ~ew (com_cmp cmp) vr vb))
  in
  let propagate_is_predicate ~ew ~prec dom r args =
    if Boolean.dec_at_literal d then Choice.register d f (Boolean.chobool r);
    let a = IArray.extract1_exn args in
    reg a;
    attach [%here] d
      (setb r
         (let* va = get a in
          let dom = dom ~mw:(prec - 1) ~ew in
          test (D.is_distinct_or_included va dom))
      && set a
           (let+ vr = getb r in
            let dom = dom ~mw:(prec - 1) ~ew in
            let top = Fp_interval.top ~ew ~mw:(prec - 1) in
            if vr then dom else Option.get @@ Fp_interval.diff top dom))
  in
  match Ground.sem f with
  | { app = { builtin = Expr.Fp_add (ew, prec); _ }; args; _ } ->
      let m, a, b = IArray.extract3_exn args in
      reg m;
      reg a;
      reg b;
      attach [%here] d
        (set r
           (let+ vm = getm m and+ va = get a and+ vb = get b in
            D.Forward.add ~mw:(prec - 1) ~ew vm va vb))
  | { app = { builtin = Expr.Fp_mul (ew, prec); _ }; args; _ } ->
      let m, a, b = IArray.extract3_exn args in
      reg m;
      reg a;
      reg b;
      attach [%here] d
        (set r
           (let+ vm = getm m and+ va = get a and+ vb = get b in
            D.Forward.mul ~mw:(prec - 1) ~ew vm va vb))
  | { app = { builtin = Expr.Fp_div (ew, prec); _ }; args; _ } ->
      let m, a, b = IArray.extract3_exn args in
      reg m;
      reg a;
      reg b;
      attach [%here] d
        (set r
           (let+ vm = getm m and+ va = get a and+ vb = get b in
            D.Forward.div ~mw:(prec - 1) ~ew vm va vb))
  | { app = { builtin = Expr.Fp_isNaN (ew, prec); _ }; args; _ } ->
      let a = IArray.extract1_exn args in
      let mw = prec - 1 in
      let nan = F.nan ~mw ~ew in
      reg a;
      attach [%here] d
        (set a
           (let+ vr = getb r in
            if vr then D.singleton nan else D.except ~mw ~ew nan)
        && setb r
             (let* va = get a in
              match D.is_singleton va with
              | NaN -> Some true
              | No -> None
              | Sin _ -> Some false))
  | { app = { builtin = Expr.Fp_isNormal (ew, prec); _ }; args; _ } ->
      propagate_is_predicate ~ew ~prec Fp_interval.normal r args
  | { app = { builtin = Expr.Fp_isSubnormal (ew, prec); _ }; args; _ } ->
      propagate_is_predicate ~ew ~prec Fp_interval.subnormal r args
  | { app = { builtin = Expr.Fp_isZero (ew, prec); _ }; args; _ } ->
      propagate_is_predicate ~ew ~prec Fp_interval.zeros r args
  | { app = { builtin = Expr.Fp_isInfinite (ew, prec); _ }; args; _ } ->
      propagate_is_predicate ~ew ~prec Fp_interval.infinites r args
  | { app = { builtin = Expr.Fp_isNegative (ew, prec); _ }; args; _ } ->
      propagate_is_predicate ~ew ~prec Fp_interval.negatives r args
  | { app = { builtin = Expr.Fp_isPositive (ew, prec); _ }; args; _ } ->
      propagate_is_predicate ~ew ~prec Fp_interval.positives r args
  | { app = { builtin = Expr.Real_to_fp (ew, prec); _ }; args; _ } ->
      let m, a = IArray.extract2_exn args in
      reg m;
      reg a;
      attach [%here] d
        (set r
           (let+ va = getr a and+ vm = getm m in
            let va = Colibri2_theories_LRA.Dom_interval.D.get_convexe_hull va in
            D.Forward.of_real ~mw:(prec - 1) ~ew vm va))
  | { app = { builtin = Expr.Ieee_format_to_fp (ew, prec); _ }; args; _ } ->
      let bv = IArray.extract1_exn args in
      reg bv;
      attach [%here] d
        (set r
           (let+ va = getr bv in
            let va = Colibri2_theories_LRA.Dom_interval.D.get_convexe_hull va in
            D.Forward.of_ieee_bits ~mw:(prec - 1) ~ew va)
        && setr bv
             (let+ vr = get r in
              D.Backward.of_ieee_bits ~mw:(prec - 1) ~ew vr))
  | { app = { builtin = Expr.Fp_eq (ew, prec); _ }; args; _ } ->
      let a, b = IArray.extract2_exn args in
      cmp d ~mw:(prec - 1) ~ew `Eq ~r ~a ~b
  | { app = { builtin = Expr.Fp_leq (ew, prec); _ }; args; _ } ->
      let a, b = IArray.extract2_exn args in
      cmp d ~mw:(prec - 1) ~ew `Le ~r ~a ~b
  | { app = { builtin = Expr.Fp_lt (ew, prec); _ }; args; _ } ->
      let a, b = IArray.extract2_exn args in
      cmp d ~mw:(prec - 1) ~ew `Lt ~r ~a ~b
  | { app = { builtin = Expr.Fp_geq (ew, prec); _ }; args; _ } ->
      let a, b = IArray.extract2_exn args in
      cmp d ~mw:(prec - 1) ~ew `Ge ~r ~a ~b
  | { app = { builtin = Expr.Fp_gt (ew, prec); _ }; args; _ } ->
      let a, b = IArray.extract2_exn args in
      cmp d ~mw:(prec - 1) ~ew `Gt ~r ~a ~b
  | _ -> ()

let init env =
  Ground.register_converter env converter;
  DaemonAlsoInterp.attach_reg_value env Fp_value.key (fun d value ->
      let v = Fp_value.value value in
      let s = D.singleton v in
      Debug.dprintf4 debug "[FP] set dom %a for %a" D.pp s Node.pp
        (Fp_value.node value);
      Egraph.set_dom d dom (Fp_value.node value) s);
  Interp.Register.node env (fun _ d n ->
      let ty =
        try Some (Ground.Ty.S.choose (Ground.tys d n)) with Not_found -> None
      in
      match (Egraph.get_dom d dom n, ty) with
      | Some nd, Some { app = { builtin = Builtin.Float (ew, prec); _ }; _ } ->
          Some
            (D.to_seq ~mw:(prec - 1) ~ew nd
            |> Sequence.map ~f:Fp_value.of_value
            |> Interp.Seq.of_seq)
      | _ -> None)
