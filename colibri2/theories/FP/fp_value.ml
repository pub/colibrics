(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib

module Float = Value.Register (struct
  include F

  let name = "Fp_value"
end)

include Float

(** Generate the sequence of all positive integers (without 0) *)
let posint_sequence_without_zero =
  let open Sequence.Generator in
  let rec loop i = yield i >>= fun () -> loop (Z.succ i) in
  run (loop Z.one)

(** Generate the sequence of positive integers (without 0) *)
let posint_sequence = Sequence.shift_right posint_sequence_without_zero Z.zero

(** Initialise type interpretation TODO : fix bounds (there is a finite set of
    32 bits floats) *)
let init_ty d =
  let specials ~mw ~ew =
    let open Sequence.Generator in
    let ( let* ) = ( >>= ) in
    run
      (let* () = yield (F.nan ~mw ~ew) in
       let* () = yield (F.zero ~mw ~ew true) in
       let* () = yield (F.zero ~mw ~ew false) in
       let* () = yield (F.inf ~mw ~ew false) in
       yield (F.inf ~mw ~ew true))
  in
  let open Sequence in
  let finites ~mw ~ew = posint_sequence >>| F.of_bits ~mw ~ew in
  let fp_seq ~mw ~ew = append (specials ~mw ~ew) (finites ~mw ~ew) in
  let fp_values ~mw ~ew = fp_seq ~mw ~ew >>| of_value in
  Interp.Register.ty d (fun _ ty ->
      match ty with
      | { app = { builtin = Builtin.Float (ew, prec); _ }; _ } ->
          Some (fp_values ~ew ~mw:(prec - 1))
      | _ -> None)

exception ValueNeeded

let interp d n = Opt.get_exn ValueNeeded (Egraph.get_value d n)

let compute_ground d t =
  let ( !>>> ) t = Colibri2_theories_LRA.RealValue.coerce_value (interp d t) in
  let ( !>> ) t = Rounding_mode.coerce_value (interp d t) in
  let ( !> ) t = coerce_value (interp d t) in
  let ( !< ) v = `Some (nodevalue (index v)) in
  let ( !<< ) v =
    `Some (Boolean.BoolValue.nodevalue (Boolean.BoolValue.index v))
  in
  let ( !<<< ) v =
    `Some Colibri2_theories_LRA.RealValue.(nodevalue (index v))
  in
  match Ground.sem t with
  | { app = { builtin = Expr.Real_to_fp (ew, prec); _ }; args; _ } ->
      let m, r = IArray.extract2_exn args in
      let r =
        match !>>>r with Q q -> q | A _ -> assert false
        (* TODO *)
      in
      !<(F.of_q ~ew ~mw:(prec - 1) !>>m r)
  | { app = { builtin = Expr.Fp_roundToIntegral (ew, prec); _ }; args; _ } -> (
      let m, f = IArray.extract2_exn args in
      let nearest_no_tie x =
        (* the tie break should be handled before *)
        assert (not (Z.equal x.Q.den (Z.of_int 2)));
        Q.ceil (Q.sub x (Q.make Z.one (Z.of_int 2)))
      in
      let toIntegral mode q =
        match mode with
        | Mode.NE ->
            if Z.equal (Z.of_int 2) q.Q.den then
              (* denominator is 2 so in the middle *)
              let r = Q.floor q in
              if Z.is_even r.num then r else Q.add Q.one r
            else nearest_no_tie q
        | Mode.NA ->
            if Z.equal (Z.of_int 2) q.Q.den then
              (* denominator is 2 so in the middle *)
              let r = if Z.sign q.Q.num < 0 then Q.floor q else Q.ceil q in
              r
            else nearest_no_tie q
        | Mode.ZR -> Q.truncate q
        | Mode.DN -> Q.floor q
        | Mode.UP -> Q.ceil q
      in
      let f = !>f in
      let mode = !>>m in
      let mw = prec - 1 in
      match F.classify f with
      | NaN | PInf | NInf | PZero | NZero -> !<f
      | PNormal | NNormal | PSubn | NSubn ->
          let q = F.to_q f in
          let n = toIntegral mode q in
          !<(F.round_q ~neg:(F.is_negative f) ~mw ~ew mode n))
  | { app = { builtin = Expr.Fp_to_fp (_ew1, _prec1, ew2, prec2); _ }; args; _ }
    ->
      let m, f1 = IArray.extract2_exn args in
      !<(F.round ~ew:ew2 ~mw:(prec2 - 1) !>>m !>f1)
  | { app = { builtin = Expr.Sbv_to_fp (n, ew, prec); _ }; args; _ } ->
      let m, bv = IArray.extract2_exn args in
      !<(F.of_q ~ew ~mw:(prec - 1) !>>m
           (Q.of_bigint (Colibri2_theories_LRA.RealValue.signed_bitv n !>>>bv)))
  | { app = { builtin = Expr.Ubv_to_fp (n, ew, prec); _ }; args; _ } ->
      let m, bv = IArray.extract2_exn args in
      !<(F.of_q ~ew ~mw:(prec - 1) !>>m
           (Q.of_bigint
              (Colibri2_theories_LRA.RealValue.unsigned_bitv n !>>>bv)))
  | { app = { builtin = Expr.Fp (ew, prec); _ }; args; _ } ->
      let bvs, bve, bvm = IArray.extract3_exn args in
      !<(F.of_bits ~ew ~mw:(prec - 1)
           (Z.logor
              (Z.logor
                 (Z.shift_left
                    (Colibri2_theories_LRA.RealValue.unsigned_bitv 1 !>>>bvs)
                    (ew + prec - 1))
                 (Z.shift_left
                    (Colibri2_theories_LRA.RealValue.unsigned_bitv ew !>>>bve)
                    (prec - 1)))
              (Colibri2_theories_LRA.RealValue.unsigned_bitv (prec - 1) !>>>bvm)))
  | { app = { builtin = Expr.Ieee_format_to_fp (ew, prec); _ }; args; _ } ->
      let bv = IArray.extract1_exn args in
      !<(F.of_bits ~ew ~mw:(prec - 1)
           (Colibri2_theories_LRA.RealValue.unsigned_bitv (ew + prec) !>>>bv))
  | { app = { builtin = Expr.To_real (_ew, _prec); _ }; args; _ } -> (
      let r = IArray.extract1_exn args in
      let r = !>r in
      match F.classify r with
      | Farith.Classify.PInf | Farith.Classify.NInf | Farith.Classify.NaN ->
          `Uninterp
      | _ -> !<<<(A.of_q @@ F.to_q r))
  | { app = { builtin = Expr.Plus_infinity (ew, prec); _ }; args; _ } ->
      assert (IArray.is_empty args);
      !<(F.inf ~ew ~mw:(prec - 1) false)
  | { app = { builtin = Expr.Minus_infinity (ew, prec); _ }; args; _ } ->
      assert (IArray.is_empty args);
      !<(F.inf ~ew ~mw:(prec - 1) true)
  | { app = { builtin = Expr.NaN (ew, prec); _ }; args; _ } ->
      assert (IArray.is_empty args);
      !<(F.nan ~ew ~mw:(prec - 1))
  | { app = { builtin = Expr.Plus_zero (ew, prec); _ }; args; _ } ->
      assert (IArray.is_empty args);
      !<(F.zero ~ew ~mw:(prec - 1) false)
  | { app = { builtin = Expr.Minus_zero (ew, prec); _ }; args; _ } ->
      assert (IArray.is_empty args);
      !<(F.zero ~ew ~mw:(prec - 1) true)
  | { app = { builtin = Expr.Fp_add (_ew, _prec); _ }; args; _ } ->
      let m, a, b = IArray.extract3_exn args in
      !<(F.add !>>m !>a !>b)
  | { app = { builtin = Expr.Fp_sub (_ew, _prec); _ }; args; _ } ->
      let m, a, b = IArray.extract3_exn args in
      !<(F.sub !>>m !>a !>b)
  | { app = { builtin = Expr.Fp_mul (_ew, _prec); _ }; args; _ } ->
      let m, a, b = IArray.extract3_exn args in
      !<(F.mul !>>m !>a !>b)
  | { app = { builtin = Expr.Fp_abs (_ew, _prec); _ }; args; _ } ->
      let a = IArray.extract1_exn args in
      !<(F.abs !>a)
  | { app = { builtin = Expr.Fp_neg (_ew, _prec); _ }; args; _ } ->
      let a = IArray.extract1_exn args in
      !<(F.neg !>a)
  | { app = { builtin = Expr.Fp_sqrt (_ew, _prec); _ }; args; _ } ->
      let m, a = IArray.extract2_exn args in
      !<(F.sqrt !>>m !>a)
  | { app = { builtin = Expr.Fp_div (_ew, _prec); _ }; args; _ } ->
      let m, a, b = IArray.extract3_exn args in
      !<(F.div !>>m !>a !>b)
  | { app = { builtin = Expr.Fp_fma (_ew, _prec); _ }; args; _ } ->
      let m, a, b, c = IArray.extract4_exn args in
      !<(F.fma !>>m !>a !>b !>c)
  | { app = { builtin = Expr.Fp_eq (_ew, _prec); _ }; args; _ } ->
      let a, b = IArray.extract2_exn args in
      !<<(F.eq !>a !>b)
  | { app = { builtin = Expr.Fp_leq (_ew, _prec); _ }; args; _ } ->
      let a, b = IArray.extract2_exn args in
      !<<(F.le !>a !>b)
  | { app = { builtin = Expr.Fp_lt (_ew, _prec); _ }; args; _ } ->
      let a, b = IArray.extract2_exn args in
      !<<(F.lt !>a !>b)
  | { app = { builtin = Expr.Fp_geq (_ew, _prec); _ }; args; _ } ->
      let a, b = IArray.extract2_exn args in
      !<<(F.ge !>a !>b)
  | { app = { builtin = Expr.Fp_gt (_ew, _prec); _ }; args; _ } ->
      let a, b = IArray.extract2_exn args in
      !<<(F.gt !>a !>b)
  | { app = { builtin = Expr.Fp_isInfinite (_ew, _prec); _ }; args; _ } ->
      let a = IArray.extract1_exn args in
      !<<(F.is_infinite !>a)
  | { app = { builtin = Expr.Fp_isZero (_ew, _prec); _ }; args; _ } ->
      let a = IArray.extract1_exn args in
      !<<(F.is_zero !>a)
  | { app = { builtin = Expr.Fp_isNaN (_ew, _prec); _ }; args; _ } ->
      let a = IArray.extract1_exn args in
      !<<(F.is_nan !>a)
  | { app = { builtin = Expr.Fp_isNegative (_ew, _prec); _ }; args; _ } ->
      let a = IArray.extract1_exn args in
      !<<(F.is_negative !>a)
  | { app = { builtin = Expr.Fp_isPositive (_ew, _prec); _ }; args; _ } ->
      let a = IArray.extract1_exn args in
      !<<(F.is_positive !>a)
  | { app = { builtin = Expr.Fp_isNormal (_ew, _prec); _ }; args; _ } ->
      let a = IArray.extract1_exn args in
      !<<(F.is_normal !>a)
  | { app = { builtin = Expr.Fp_isSubnormal (_ew, _prec); _ }; args; _ } ->
      let a = IArray.extract1_exn args in
      !<<(F.is_subnormal !>a)
  | _ -> `None

let init_check d =
  Interp.Register.check d (fun d t ->
      let check r = Value.equal r (interp d (Ground.node t)) in
      match compute_ground d t with
      | `None -> NA
      | `Some v -> Interp.check_of_bool (check v)
      | `Uninterp ->
          Interp.check_of_bool
            (Colibri2_theories_quantifiers.Uninterp.On_uninterpreted_domain
             .check d t))

let attach_interp d g =
  let f d g =
    match compute_ground d g with
    | `None -> raise Impossible
    | `Some v -> Egraph.set_value d (Ground.node g) v
    | `Uninterp ->
        Colibri2_theories_quantifiers.Uninterp.On_uninterpreted_domain.propagate
          d g
  in
  Interp.WatchArgs.create d f g

let converter d (f : Ground.t) =
  match compute_ground d f with
  | exception ValueNeeded ->
      IArray.iter (Ground.sem f).args ~f:(Egraph.register d);
      attach_interp d f
  | `None -> ()
  | `Some v -> Egraph.set_value d (Ground.node f) v
  | `Uninterp ->
      IArray.iter (Ground.sem f).args ~f:(Egraph.register d);
      attach_interp d f

let init env =
  Ground.register_converter env converter;
  init_ty env;
  init_check env
