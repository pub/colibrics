(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib
open Popop_stdlib

module FarithModes = struct
  let string_of_mode : Farith.Mode.t -> string = function
    | NE -> "RoundNearestTiesToEven"
    | NA -> "RoundNearestTiesToAway"
    | UP -> "RoundTowardPositive"
    | DN -> "RoundTowardNegative"
    | ZR -> "RoundTowardZero"

  type t = Farith.Mode.t

  let hash = Hashtbl.hash
  let compare = Stdlib.compare
  let equal = Stdlib.( = )
  let hash_fold_t s t = Base.Hash.fold_int s (hash t)
  let sexp_of_t t = Base.Sexp.Atom (string_of_mode t)
  let pp fmt t = Format.pp_print_string fmt (string_of_mode t)
end

module Modes = Value.Register (struct
  include FarithModes
  module M = Map.Make (FarithModes)
  module S = Extset.MakeOfMap (M)
  module H = XHashtbl.Make (FarithModes)

  let name = "RoundingMode"
end)

include Modes

let rounding_mode_sequence =
  let open Sequence.Generator in
  yield Farith.Mode.NE
  >>= (fun () ->
  yield Farith.Mode.NA >>= fun () ->
  yield Farith.Mode.UP >>= fun () ->
  yield Farith.Mode.DN >>= fun () -> yield Farith.Mode.ZR)
  |> run

let init_ty d =
  Interp.Register.ty d (fun _ ty ->
      match ty with
      | { app = { builtin = Expr.RoundingMode; _ }; _ } ->
          let seq =
            let open Std.Sequence in
            let+ e = rounding_mode_sequence in
            e |> index |> nodevalue
          in
          Some seq
      | _ -> None)

let interp d n = Opt.get_exn Impossible (Egraph.get_value d n)

let compute_cst t =
  let ( !< ) v = `Some (Modes.nodevalue (Modes.index v)) in
  match Ground.sem t with
  | { app = { builtin = Expr.RoundNearestTiesToEven; _ }; _ } ->
      !<Farith.Mode.NE
  | { app = { builtin = Expr.RoundNearestTiesToAway; _ }; _ } ->
      !<Farith.Mode.NA
  | { app = { builtin = Expr.RoundTowardNegative; _ }; _ } -> !<Farith.Mode.DN
  | { app = { builtin = Expr.RoundTowardPositive; _ }; _ } -> !<Farith.Mode.UP
  | { app = { builtin = Expr.RoundTowardZero; _ }; _ } -> !<Farith.Mode.ZR
  | _ -> `None

let converter d f =
  let r = Ground.node f in
  let cst c = node (index c) in
  let merge n =
    Egraph.register d n;
    Egraph.merge d r n
  in
  match Ground.sem f with
  | { app = { builtin = Expr.RoundNearestTiesToEven; _ }; _ } ->
      merge (cst Farith.Mode.NE)
  | { app = { builtin = Expr.RoundNearestTiesToAway; _ }; _ } ->
      merge (cst Farith.Mode.NA)
  | { app = { builtin = Expr.RoundTowardNegative; _ }; _ } ->
      merge (cst Farith.Mode.DN)
  | { app = { builtin = Expr.RoundTowardPositive; _ }; _ } ->
      merge (cst Farith.Mode.UP)
  | { app = { builtin = Expr.RoundTowardZero; _ }; _ } ->
      merge (cst Farith.Mode.ZR)
  | _ -> ()

let init_check d =
  Interp.Register.check d (fun d t ->
      let check r = Value.equal r (interp d (Ground.node t)) in
      match compute_cst t with
      | `None -> NA
      | `Some v -> Interp.check_of_bool (check v))

let init env =
  Ground.register_converter env converter;
  init_ty env;
  init_check env
