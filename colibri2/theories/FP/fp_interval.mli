(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

(** {1 Domain of union of interval of floating point numbers}*)

type t [@@deriving show, eq]
(** Domain *)

val gen : t QCheck2.Gen.t

val mem : t -> F.t -> bool
(** Membership operation *)

val top : mw:int -> ew:int -> t
(** All possible floating point number of this size *)

val union : t -> t -> t
(** Union operation *)

val inter : t -> t -> t option
(** Intersection operation *)

val diff : t -> t -> t option
(** Diff operation *)

val is_included : t -> t -> bool
(** [is_included t1 t2] check if t1 ⊆ t2 *)

val is_distinct : t -> t -> bool
(** check that the arguments don't share any elements *)

type is_distinct_or_included = Uncomparable | Included | Disjoint
[@@deriving show]

val is_distinct_or_included : t -> t -> is_distinct_or_included
(** check that the arguments don't share any elements *)

val nan : t
(** singleton NaN *)

val normal : mw:int -> ew:int -> t
(** normal number *)

val subnormal : mw:int -> ew:int -> t
(** subnormal number *)

val infinites : mw:int -> ew:int -> t
(** the two infinite number *)

val zeros : mw:int -> ew:int -> t
(** the two zeros number *)

val finites : mw:int -> ew:int -> t
(** finites number *)

val negatives : mw:int -> ew:int -> t
(** negatives number ( NaN is not) *)

val positives : mw:int -> ew:int -> t
(** positives number (NaN is not) *)

val singleton : F.t -> t
(** Create singleton of any floating point number *)

type is_singleton =
  | NaN  (** It is the singleton NaN *)
  | No  (** It is not a singleton *)
  | Sin of F.t  (** It is a singleton (but not NaN) *)

val is_singleton : t -> is_singleton
(** Test if the domain is reduced to a singleton *)

val except : mw:int -> ew:int -> F.t -> t
(** Domain of all the floating point except the given one *)

val to_seq : mw:int -> ew:int -> t -> F.t Base.Sequence.t

module Backward : sig
  val of_ieee_bits :
    mw:int -> ew:int -> t -> Colibri2_theories_LRA.Dom_interval.D.t

  module Cmp_True : sig
    (** {2 Propagate backward true comparisons} *)

    val le : ew:int -> mw:int -> t -> t option
    val lt : ew:int -> mw:int -> t -> t option
    val ge : ew:int -> mw:int -> t -> t option
    val gt : ew:int -> mw:int -> t -> t option
    val eq : ew:int -> mw:int -> t -> t option
  end

  module Cmp_False : sig
    (** {2 Propagate backward false comparisons} *)

    val le : ew:int -> mw:int -> t -> t
    val lt : ew:int -> mw:int -> t -> t
    val ge : ew:int -> mw:int -> t -> t
    val gt : ew:int -> mw:int -> t -> t
    val eq : ew:int -> mw:int -> t -> t
  end
end

module Forward : sig
  (** {2 Forward propagator for floating point operations} *)

  val add : mw:int -> ew:int -> Farith.Mode.t -> t -> t -> t
  val mul : mw:int -> ew:int -> Farith.Mode.t -> t -> t -> t
  val div : mw:int -> ew:int -> Farith.Mode.t -> t -> t -> t

  val of_real :
    mw:int ->
    ew:int ->
    Farith.Mode.t ->
    (A.t * 'a) option * (A.t * 'b) option ->
    t
  (** [of_real ~mw ~ew m ch] ch is the convex hull, the strict or not bounds are
      not yet taken into account *)

  val of_ieee_bits :
    mw:int -> ew:int -> (A.t * 'a) option * (A.t * 'b) option -> t
  (** [of_ieee_bits ~mw ~ew ch] ch is the convex hull, the strict or not bounds
      are not yet taken into account *)
end
