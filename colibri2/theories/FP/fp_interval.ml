(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Base

type t = { u : (F.t * F.t) list; nan : bool } [@@deriving show, eq]
(** bottom [{u = []; nan = false }] should never be returned outside *)

let mem t v =
  if F.is_nan v then t.nan
  else
    List.exists t.u ~f:(fun (a, b) -> F.le_diff_zero a v && F.le_diff_zero v b)

let invariant t =
  ((not (List.is_empty t.u)) || t.nan)
  && snd
       (List.fold
          ~f:(fun (prev, r) (a, b) ->
            match prev with
            | None -> (Some b, r && F.le a b)
            | Some prev -> (Some b, r && F.le a b && F.lt_diff_zero prev a))
          ~init:(None, true) t.u)

let gen =
  let open QCheck2.Gen in
  let+ nan = bool and* u = small_list F.gen in
  let u = List.filter ~f:(fun x -> not (F.is_nan x)) u in
  let u = List.sort_and_group ~compare:F.compare_diff_zero u in
  let rec by_pair = function
    | (a :: _ :: _) :: l -> (a, a) :: by_pair l
    | [ a ] :: (b :: _) :: l -> (a, b) :: by_pair l
    | [ [ a ] ] -> [ (a, F.inf ~mw:23 ~ew:8 false) ]
    | [] | [] :: _ | _ :: [] :: _ -> []
  in
  let r = { u = by_pair u; nan = nan || List.is_empty u } in
  assert (invariant r);
  r

let interesting_values t =
  F.interesting_values
  @ List.concat_map
      ~f:(fun (a, b) -> [ F.pred a; a; F.pred b; b; F.succ b ])
      t.u

let count = 200

let unary_test f =
  let test = QCheck2.Test.make ~count ~print:show gen f in
  QCheck2.Test.check_exn test

let%test_unit "invariant" = unary_test invariant

let binary_test f =
  let test =
    QCheck2.Test.make ~count
      ~print:(fun (x, y) -> Fmt.str "%a and %a" pp x pp y)
      (QCheck2.Gen.pair gen gen) f
  in
  QCheck2.Test.check_exn test

let rec union' l1 l2 =
  match (l1, l2) with
  | [], l | l, [] -> l
  (*
    l1: =======
    l2:            ========   
  *)
  | ((_, b1) as c1) :: d1, (a2, _) :: _ when F.lt_diff_zero b1 a2 ->
      c1 :: union' d1 l2
  (*
    l1:            =======
    l2: ========   
  *)
  | (a1, _) :: _, ((_, b2) as c2) :: d2 when F.lt_diff_zero b2 a1 ->
      c2 :: union' l1 d2
  (*
    l1: =======-------
    l2: ----========   
  *)
  | (a1, b1) :: d1, (a2, b2) :: d2 when F.le_diff_zero a1 a2 ->
      if F.le_diff_zero b1 b2 then union' d1 ((a1, b2) :: d2) else union' l1 d2
  (*
    l1: ----=======
    l2: ========-------
  *)
  | (_, b1) :: d1, (a2, b2) :: d2 ->
      if F.le_diff_zero b1 b2 then union' d1 l2 else union' ((a2, b1) :: d1) d2

let union t1 t2 = { u = union' t1.u t2.u; nan = t1.nan || t2.nan }

let test_union_inter name f c inv =
  binary_test (fun (t1, t2) ->
      let t3 = f t1 t2 in
      if not (inv t3) then
        QCheck2.Test.fail_reportf "%a doesn't verify the invariant in %s" pp t3
          name;
      let l = List.concat_map ~f:interesting_values [ t1; t2; t3 ] in
      List.iter l ~f:(fun v ->
          if not (c t1 t2 t3 v) then
            QCheck2.Test.fail_reportf "%a is not the %s at %a" pp t3 name F.pp v);
      true)

let%test_unit "union" =
  test_union_inter "union" union
    (fun t1 t2 t3 v -> Bool.equal (mem t1 v || mem t2 v) (mem t3 v))
    invariant

let rec inter' l1 l2 =
  match (l1, l2) with
  | [], _ | _, [] -> []
  (*
          l1: =======
          l2:            ========   
        *)
  | (_, b1) :: d1, (a2, _) :: _ when F.lt_diff_zero b1 a2 -> inter' d1 l2
  (*
          l1:            =======
          l2: ========   
        *)
  | (a1, _) :: _, (_, b2) :: d2 when F.lt_diff_zero b2 a1 -> inter' l1 d2
  (*
          l1: ------========
          l2:     ========--   
        *)
  | (a1, b1) :: d1, (a2, b2) :: d2 when F.le_diff_zero b2 b1 ->
      let l1' = if F.equal b1 b2 then d1 else (F.succ_diff_zero b2, b1) :: d1 in
      (F.max_diff_zero a1 a2, b2) :: inter' l1' d2
  (*
          l1:     =======
          l2:  -----========
        *)
  | (a1, b1) :: d1, (a2, b2) :: d2 ->
      assert (F.lt_diff_zero b1 b2);
      let l2' = (F.succ_diff_zero b1, b2) :: d2 in
      (F.max_diff_zero a1 a2, b1) :: inter' d1 l2'

let inter t1 t2 = { u = inter' t1.u t2.u; nan = t1.nan && t2.nan }

let%test_unit "inter" =
  test_union_inter "inter" inter
    (fun t1 t2 t3 v -> Bool.equal (mem t1 v && mem t2 v) (mem t3 v))
    (fun t3 -> (List.is_empty t3.u && not t3.nan) || invariant t3)

let inter t1 t2 =
  let t3 = inter t1 t2 in
  if List.is_empty t3.u && not t3.nan then None else Some t3

let rec diff' l1 l2 =
  match (l1, l2) with
  | [], _ -> []
  | l1, [] -> l1
  (*
            l1: =======
            l2:            ========   
          *)
  | ((_, b1) as c1) :: d1, (a2, _) :: _ when F.lt_diff_zero b1 a2 ->
      c1 :: diff' d1 l2
  (*
            l1:            =======
            l2: ========   
          *)
  | (a1, _) :: _, (_, b2) :: d2 when F.lt_diff_zero b2 a1 -> diff' l1 d2
  (*
            l1: ------========
            l2:     ========--   
          *)
  | (a1, b1) :: d1, (a2, b2) :: d2 when F.le_diff_zero b2 b1 ->
      let l1' = if F.equal b1 b2 then d1 else (F.succ_diff_zero b2, b1) :: d1 in
      let l3 = diff' l1' d2 in
      if F.lt_diff_zero a1 a2 then (a1, F.pred_diff_zero a2) :: l3 else l3
  (*
            l1:     =======
            l2:  -----========
          *)
  | (a1, b1) :: d1, (a2, b2) :: _ ->
      assert (F.lt_diff_zero b1 b2);
      let l3 = diff' d1 l2 in
      if F.lt_diff_zero a1 a2 then (a1, F.pred_diff_zero a2) :: l3 else l3

let diff t1 t2 = { u = diff' t1.u t2.u; nan = t1.nan && not t2.nan }

let%test_unit "diff" =
  test_union_inter "diff" diff
    (fun t1 t2 t3 v -> Bool.equal (mem t1 v && not (mem t2 v)) (mem t3 v))
    (fun t3 -> (List.is_empty t3.u && not t3.nan) || invariant t3)

let diff t1 t2 =
  let t3 = diff t1 t2 in
  if List.is_empty t3.u && not t3.nan then None else Some t3

let test_included_distinct name pp f c skip =
  binary_test (fun (t1, t2) ->
      let t3 = f t1 t2 in
      skip t3
      ||
      let l = List.concat_map ~f:interesting_values [ t1; t2 ] in
      List.iter l ~f:(fun v ->
          if not (c t1 t2 t3 v) then
            QCheck2.Test.fail_reportf "%a is not the %s at %a" pp t3 name F.pp v);
      true)

let rec is_included' l1 l2 =
  match (l1, l2) with
  | [], _ -> true
  | _, [] -> false
  (*
              l1: =======
              l2:            ========   
            *)
  | (_, b1) :: _, (a2, _) :: _ when F.lt_diff_zero b1 a2 -> false
  (*
              l1:            =======
              l2: ========   
            *)
  | (a1, _) :: _, (_, b2) :: d2 when F.lt_diff_zero b2 a1 -> is_included' l1 d2
  (*
              l1: ------========
              l2:     ========--   
            *)
  | (a1, b1) :: d1, (a2, b2) :: d2 when F.le_diff_zero b2 b1 ->
      F.le_diff_zero a2 a1
      &&
      let l1' = if F.equal b1 b2 then d1 else (F.succ_diff_zero b2, b1) :: d1 in
      is_included' l1' d2
  (*
              l1:     =======
              l2:  -----========
            *)
  | (a1, b1) :: d1, (a2, b2) :: _ ->
      assert (F.lt_diff_zero b1 b2);
      F.le_diff_zero a2 a1 && is_included' d1 l2

let is_included t1 t2 = ((not t1.nan) || t2.nan) && is_included' t1.u t2.u

let%test_unit "is_included" =
  test_included_distinct "is_included" Fmt.bool is_included
    (fun t1 t2 _t3 v -> (not (mem t1 v)) || mem t2 v)
    (fun t3 -> not t3)

let rec is_distinct' l1 l2 =
  match (l1, l2) with
  | [], _ -> true
  | _, [] -> true
  (*
                    l1: =======
                    l2:            ========   
                  *)
  | (_, b1) :: d1, (a2, _) :: _ when F.lt_diff_zero b1 a2 -> is_distinct' d1 l2
  (*
                    l1:            =======
                    l2: ========   
                  *)
  | (a1, _) :: _, (_, b2) :: d2 when F.lt_diff_zero b2 a1 -> is_distinct' l1 d2
  (*
                    l1: ------========
                    l2:     ========--   
                  *)
  | (_, b1) :: _, (_, b2) :: _ when F.le_diff_zero b2 b1 -> false
  (*
                    l1:     =======
                    l2:  -----========
                  *)
  | _ -> false

let is_distinct t1 t2 = ((not t1.nan) || not t2.nan) && is_distinct' t1.u t2.u

let%test_unit "is_distinct" =
  test_included_distinct "is_distinct" Fmt.bool is_distinct
    (fun t1 t2 _t3 v -> (not (mem t1 v)) || not (mem t2 v))
    (fun t3 -> not t3)

type is_distinct_or_included = Uncomparable | Included | Disjoint
[@@deriving show]

let is_included' l1 l2 = if is_included' l1 l2 then Included else Uncomparable
let is_distinct' l1 l2 = if is_distinct' l1 l2 then Disjoint else Uncomparable

let rec is_distinct_or_included' l1 l2 =
  match (l1, l2) with
  | [], _ -> assert false
  | _, [] -> Disjoint
  (*
                      l1: =======
                      l2:            ========   
                    *)
  | (_, b1) :: d1, (a2, _) :: _ when F.lt_diff_zero b1 a2 -> is_distinct' d1 l2
  (*
                      l1:            =======
                      l2: ========   
                    *)
  | (a1, _) :: _, (_, b2) :: d2 when F.lt_diff_zero b2 a1 ->
      is_distinct_or_included' l1 d2
  (*
                      l1: ------========--
                      l2: ---========-----   
                    *)
  | _ -> is_included' l1 l2

let is_distinct_or_included t1 t2 =
  match (t1.nan, t2.nan) with
  | false, false | false, true -> is_distinct_or_included' t1.u t2.u
  | true, false -> is_distinct' t1.u t2.u
  | true, true -> is_included' t1.u t2.u

let%test_unit "is_included" =
  test_included_distinct "is_included" pp_is_distinct_or_included
    is_distinct_or_included
    (fun t1 t2 t3 v ->
      match t3 with
      | Uncomparable -> true
      | Included -> (not (mem t1 v)) || mem t2 v
      | Disjoint -> (not (mem t1 v)) || not (mem t2 v))
    (function Uncomparable -> true | _ -> false)

let nan = { u = []; nan = true }
let%test "nan" = invariant nan

let top ~mw ~ew =
  { u = [ (F.inf ~mw ~ew true, F.inf ~mw ~ew false) ]; nan = true }

let%test "top" = invariant (top ~mw:23 ~ew:8)
let bot_internal = { u = []; nan = false }
let singleton f = if F.is_nan f then nan else { u = [ (f, f) ]; nan = false }

type is_singleton = NaN | No | Sin of F.t

let except ~mw ~ew f =
  if F.is_nan f then
    { u = [ (F.inf ~mw ~ew true, F.inf ~mw ~ew false) ]; nan = false }
  else if F.is_infinite f then
    if F.is_negative f then
      { u = [ (F.succ f, F.inf ~ew ~mw false) ]; nan = true }
    else { u = [ (F.inf ~ew ~mw true, F.pred f) ]; nan = true }
  else
    {
      u =
        [
          (F.inf ~ew ~mw true, F.pred_diff_zero f);
          (F.succ_diff_zero f, F.inf ~ew ~mw false);
        ];
      nan = true;
    }

let is_singleton = function
  | { u = []; nan = true } -> NaN
  | { u = [ (a, b) ]; nan = false } when F.equal a b -> Sin a
  | _ -> No

(** 0+ finite+ inf+ nan+ 0- finite- inf- nan- *)

type 'a signed_special_values = {
  zero : 'a;
  first_subnormal : 'a;
  last_subnormal : 'a;
  first_normal : 'a;
  last_normal : 'a;
  infinite : 'a;
  first_nan : 'a;
  last_nan : 'a;
}

type special_values = {
  pos : Z.t signed_special_values;
  neg : Z.t signed_special_values;
  fpos : F.t signed_special_values;
  fneg : F.t signed_special_values;
}

let get_special_values ~mw ~ew =
  let pos_infinite = F.to_bits (F.inf ~mw ~ew false) in
  let pos =
    {
      zero = Z.zero;
      first_subnormal = Z.one;
      last_subnormal = Z.extract Z.minus_one 0 mw;
      first_normal = Z.shift_left Z.one mw;
      last_normal = Z.pred pos_infinite;
      infinite = pos_infinite;
      first_nan = Z.succ pos_infinite;
      last_nan = Z.extract Z.minus_one 0 (mw + ew);
    }
  in
  let sign_bit = Z.shift_left Z.one (mw + ew) in
  let neg z = Z.logor sign_bit z in
  let neg =
    {
      zero = neg pos.zero;
      first_subnormal = neg pos.first_subnormal;
      last_subnormal = neg pos.last_subnormal;
      first_normal = neg pos.first_normal;
      last_normal = neg pos.last_normal;
      infinite = neg pos.infinite;
      first_nan = neg pos.first_nan;
      last_nan = neg pos.last_nan;
    }
  in
  let of_bits = F.of_bits ~mw ~ew in
  {
    pos;
    neg;
    fpos =
      {
        zero = of_bits pos.zero;
        first_subnormal = of_bits pos.first_subnormal;
        last_subnormal = of_bits pos.last_subnormal;
        first_normal = of_bits pos.first_normal;
        last_normal = of_bits pos.last_normal;
        infinite = of_bits pos.infinite;
        first_nan = of_bits pos.first_nan;
        last_nan = of_bits pos.last_nan;
      };
    fneg =
      {
        zero = of_bits neg.zero;
        first_subnormal = of_bits neg.first_subnormal;
        last_subnormal = of_bits neg.last_subnormal;
        first_normal = of_bits neg.first_normal;
        last_normal = of_bits neg.last_normal;
        infinite = of_bits neg.infinite;
        first_nan = of_bits neg.first_nan;
        last_nan = of_bits neg.last_nan;
      };
  }

let h_special_values = Colibri2_popop_lib.Popop_stdlib.DInt.H.create 10

let get_special_values ~mw ~ew =
  Colibri2_popop_lib.Popop_stdlib.DInt.H.memo
    (fun mw ->
      let h = Colibri2_popop_lib.Popop_stdlib.DInt.H.create 10 in
      Colibri2_popop_lib.Popop_stdlib.DInt.H.memo
        (fun ew -> get_special_values ~mw ~ew)
        h)
    h_special_values mw ew

let%test "special_values" =
  let mw = 23 and ew = 8 in
  let sv = get_special_values ~mw ~ew in
  let ( @! ) f x = f (F.of_bits ~mw ~ew x) in
  true
  && (F.is_positive_zero @! sv.pos.zero)
  && (F.is_subnormal @! sv.pos.first_subnormal)
  && (F.is_positive @! sv.pos.first_subnormal)
  && (F.is_subnormal @! sv.pos.last_subnormal)
  && (F.is_positive @! sv.pos.last_subnormal)
  && (F.is_normal @! sv.pos.first_normal)
  && (F.is_positive @! sv.pos.first_normal)
  && (F.is_normal @! sv.pos.last_normal)
  && (F.is_positive @! sv.pos.last_normal)
  && (F.is_positive_infinite @! sv.pos.infinite)
  && (F.is_nan @! sv.pos.first_nan)
  && (F.is_nan @! sv.pos.last_nan)
  && (F.is_negative_zero @! sv.neg.zero)
  && (F.is_subnormal @! sv.neg.first_subnormal)
  && (F.is_negative @! sv.neg.first_subnormal)
  && (F.is_subnormal @! sv.neg.last_subnormal)
  && (F.is_negative @! sv.neg.last_subnormal)
  && (F.is_normal @! sv.neg.first_normal)
  && (F.is_negative @! sv.neg.first_normal)
  && (F.is_normal @! sv.neg.last_normal)
  && (F.is_negative @! sv.neg.last_normal)
  && (F.is_negative_infinite @! sv.neg.infinite)
  && (F.is_nan @! sv.neg.first_nan)
  && (F.is_nan @! sv.neg.last_nan)
  && Z.equal (Z.succ sv.pos.last_subnormal) sv.pos.first_normal
  && Z.equal (Z.succ sv.pos.last_normal) sv.pos.infinite
  && Z.equal (Z.succ sv.neg.last_subnormal) sv.neg.first_normal
  && Z.equal (Z.succ sv.neg.last_normal) sv.neg.infinite

let f_unary_test f =
  let test = QCheck2.Test.make ~count ~print:F.show F.gen f in
  QCheck2.Test.check_exn test

let normal ~mw ~ew =
  let sv = get_special_values ~mw ~ew in
  {
    u =
      [
        (sv.fneg.last_normal, sv.fneg.first_normal);
        (sv.fpos.first_normal, sv.fpos.last_normal);
      ];
    nan = false;
  }

let%test "normal_inv" = invariant (normal ~mw:23 ~ew:8)

let%test_unit "normal" =
  let normal = normal ~mw:23 ~ew:8 in
  f_unary_test (fun f -> Bool.equal (F.is_normal f) (mem normal f))

let subnormal ~mw ~ew =
  let sv = get_special_values ~mw ~ew in
  {
    u =
      [
        (sv.fneg.last_subnormal, sv.fneg.first_subnormal);
        (sv.fpos.first_subnormal, sv.fpos.last_subnormal);
      ];
    nan = false;
  }

let%test "subnormal_inv" = invariant (subnormal ~mw:23 ~ew:8)

let%test_unit "subnormal" =
  let subnormal = subnormal ~mw:23 ~ew:8 in
  f_unary_test (fun f -> Bool.equal (F.is_subnormal f) (mem subnormal f))

let zeros ~mw ~ew =
  let sv = get_special_values ~mw ~ew in
  { u = [ (sv.fneg.zero, sv.fpos.zero) ]; nan = false }

let%test "zeros_inv" = invariant (zeros ~mw:23 ~ew:8)

let%test_unit "zeros" =
  let zeros = zeros ~mw:23 ~ew:8 in
  f_unary_test (fun f -> Bool.equal (F.is_zero f) (mem zeros f))

let infinites ~mw ~ew =
  let sv = get_special_values ~mw ~ew in
  {
    u =
      [
        (sv.fneg.infinite, sv.fneg.infinite);
        (sv.fpos.infinite, sv.fpos.infinite);
      ];
    nan = false;
  }

let%test "infinites_inv" = invariant (infinites ~mw:23 ~ew:8)

let%test_unit "infinites" =
  let infinites = infinites ~mw:23 ~ew:8 in
  f_unary_test (fun f -> Bool.equal (F.is_infinite f) (mem infinites f))

let finites ~mw ~ew =
  let sv = get_special_values ~mw ~ew in
  { u = [ (sv.fneg.last_normal, sv.fpos.last_normal) ]; nan = false }

let%test "finites_inv" = invariant (finites ~mw:23 ~ew:8)

let%test_unit "finites" =
  let finites = finites ~mw:23 ~ew:8 in
  f_unary_test (fun f ->
      Bool.equal
        (F.is_normal f || F.is_subnormal f || F.is_zero f)
        (mem finites f))

let finites ~mw ~ew =
  let sv = get_special_values ~mw ~ew in
  { u = [ (sv.fneg.last_normal, sv.fpos.last_normal) ]; nan = false }

let%test "finites_inv" = invariant (finites ~mw:23 ~ew:8)

let%test_unit "finites" =
  let finites = finites ~mw:23 ~ew:8 in
  f_unary_test (fun f ->
      Bool.equal
        (F.is_normal f || F.is_subnormal f || F.is_zero f)
        (mem finites f))

let negatives ~mw ~ew =
  let sv = get_special_values ~mw ~ew in
  { u = [ (sv.fneg.infinite, sv.fneg.zero) ]; nan = false }

let%test "negatives_inv" = invariant (negatives ~mw:23 ~ew:8)

let%test_unit "negatives" =
  let negatives = negatives ~mw:23 ~ew:8 in
  f_unary_test (fun f -> Bool.equal (F.is_negative f) (mem negatives f))

let positives ~mw ~ew =
  let sv = get_special_values ~mw ~ew in
  { u = [ (sv.fpos.zero, sv.fpos.infinite) ]; nan = false }

let%test "positives_inv" = invariant (positives ~mw:23 ~ew:8)

let%test_unit "positives" =
  let positives = positives ~mw:23 ~ew:8 in
  f_unary_test (fun f -> Bool.equal (F.is_positive f) (mem positives f))

let to_seq ~mw ~ew t =
  let sv = get_special_values ~mw ~ew in
  let seq =
    Sequence.of_list
      [
        sv.fpos.first_nan;
        sv.fpos.zero;
        sv.fneg.zero;
        sv.fpos.infinite;
        sv.fneg.infinite;
        F.of_q ~mw ~ew UP Q.one;
        sv.fpos.first_subnormal;
        sv.fpos.first_normal;
      ]
    |> Sequence.filter ~f:(mem t)
  in
  let aux (a, b) =
    Sequence.unfold ~init:a ~f:(fun s ->
        if F.lt_diff_zero b s then None else Some (F.succ_diff_zero s, s))
  in
  Sequence.append seq
  @@ Sequence.interleave (Sequence.of_list (List.map ~f:aux t.u))

module Backward = struct
  let interval x y =
    Option.value_exn
    @@ Colibri2_theories_LRA.Dom_interval.D.from_convexe_hull
         (Some (A.of_z x, Large), Some (A.of_z y, Large))

  let of_ieee_bits ~mw ~ew t =
    let open Colibri2_theories_LRA.Dom_interval in
    let sv = get_special_values ~mw ~ew in
    let aux (l, h) =
      let bl = F.to_bits l in
      let bh = F.to_bits h in
      match (F.is_positive l, F.is_positive h) with
      | true, true -> interval bl bh
      | false, false -> interval bh bl
      | true, false -> assert false
      | false, true ->
          D.union (interval sv.pos.zero bh) (interval sv.neg.zero bl)
    in
    let nan () =
      D.union
        (interval sv.pos.first_nan sv.pos.last_nan)
        (interval sv.neg.first_nan sv.neg.last_nan)
    in
    let r =
      match t.u with
      | [] ->
          assert t.nan;
          nan ()
      | a :: l ->
          let acc = aux a in
          let acc = if t.nan then D.union (nan ()) acc else acc in
          List.fold_left ~init:acc ~f:(fun acc a -> D.union acc (aux a)) l
    in
    Option.value_exn @@ D.reduce_integers r

  let%test_unit "Backward.of_ieee_bits" =
    let open Colibri2_theories_LRA.Dom_interval in
    let sv = get_special_values ~mw:23 ~ew:8 in
    let all = interval sv.pos.zero sv.neg.last_nan in
    unary_test (fun t ->
        let r = of_ieee_bits ~mw:23 ~ew:8 t in
        D.is_included r all
        &&
        let vl = interesting_values t in
        List.iter vl ~f:(fun v ->
            let bv = F.to_bits v in
            if not (Bool.equal (mem t v) (D.mem (A.of_z bv) r)) then
              QCheck2.Test.fail_reportf "Error for %a with %a -> %a" D.pp r F.pp
                v Z.pp_print bv);
        true)

  let cmp_test f cmp =
    unary_test (fun t ->
        let t' = f ~ew:8 ~mw:23 t in
        let mem' t' v = match t' with None -> false | Some t' -> mem t' v in
        let vl = interesting_values t in
        let vl' =
          match t' with
          | None -> F.interesting_values
          | Some t' -> interesting_values t'
        in
        let vl0 = List.dedup_and_sort ~compare:F.compare (vl @ vl') in
        let vl = List.filter vl0 ~f:(fun v -> mem t v) in
        let vl' = List.filter vl0 ~f:(fun v' -> not (mem' t' v')) in
        List.iter vl ~f:(fun v ->
            let l = List.filter vl' ~f:(fun v' -> cmp v v') in
            if not (List.is_empty l) then
              QCheck2.Test.fail_reportf "Error for %a with %a and %a"
                Fmt.(option ~none:(any "no") pp)
                t' F.pp v
                Fmt.(list ~sep:comma F.pp)
                l);
        true)

  module Cmp_True = struct
    (* Backward propagation of true comparison *)

    let%test "order inf" =
      F.le (F.inf ~mw:12 ~ew:8 true) (F.inf ~mw:12 ~ew:8 false)

    (* return the domain of y, if x has domain [t] and x <= y *)
    let le ~ew ~mw t =
      match t.u with
      | [] -> (* comparisons with NaN are never true *) None
      | (f1, _) :: _ ->
          let f1 = if F.is_positive_zero f1 then F.zero ~ew ~mw true else f1 in
          Some { u = [ (f1, F.inf ~ew ~mw false) ]; nan = false }

    let%test_unit "le" = cmp_test le F.le

    let lt ~ew ~mw t =
      match t.u with
      | [] -> (* comparisons with NaN are never true *) None
      | (f1, _) :: _ when F.is_infinite f1 && F.is_positive f1 -> None
      | (f1, _) :: _ ->
          Some { u = [ (F.succ f1, F.inf ~ew ~mw false) ]; nan = false }

    let%test_unit "lt" = cmp_test lt F.lt

    let ge ~ew ~mw t =
      match List.last t.u with
      | None -> (* comparisons with NaN are never true *) None
      | Some (_, f2) ->
          let f2 = if F.is_negative_zero f2 then F.zero ~ew ~mw false else f2 in
          Some { u = [ (F.inf ~ew ~mw true, f2) ]; nan = false }

    let%test_unit "ge" = cmp_test ge F.ge

    let gt ~ew ~mw t =
      match List.last t.u with
      | None -> (* comparisons with NaN are never true *) None
      | Some (_, f2) when F.is_infinite f2 && F.is_negative f2 -> None
      | Some (_, f2) -> Some { u = [ (F.inf ~ew ~mw true, f2) ]; nan = false }

    let%test_unit "gt" = cmp_test gt F.gt

    let eq ~ew ~mw t =
      if
        List.exists t.u ~f:(fun (a, b) ->
            F.is_positive_zero a || F.is_negative_zero b)
      then
        let u = union' t.u [ (F.zero ~ew ~mw true, F.zero ~ew ~mw false) ] in
        Some { u; nan = false }
      else if t.nan then
        if List.is_empty t.u then None else Some { u = t.u; nan = false }
      else Some t

    let%test_unit "eq" = cmp_test eq F.eq
  end

  module Cmp_False = struct
    (* Backward propagation of false comparison *)

    let cmp_test f c =
      cmp_test
        (fun ~ew ~mw t -> Some (f ~ew ~mw t))
        (fun v1 v2 -> not (c v1 v2))

    let le ~ew ~mw t =
      if t.nan then top ~ew ~mw
      else
        let _, f2 = List.last_exn t.u in
        if F.is_negative_infinite f2 then nan
        else { u = [ (F.inf ~ew ~mw true, F.pred f2) ]; nan = true }

    let%test_unit "le" = cmp_test le F.le

    let lt ~ew ~mw t =
      if t.nan then top ~ew ~mw
      else
        let _, f2 = List.last_exn t.u in
        let f2 = if F.is_negative_zero f2 then F.zero ~mw ~ew false else f2 in
        { u = [ (F.inf ~ew ~mw true, f2) ]; nan = true }

    let%test_unit "lt" = cmp_test lt F.lt

    let ge ~ew ~mw t =
      if t.nan then top ~ew ~mw
      else
        let f1, _ = List.hd_exn t.u in
        if F.is_positive_infinite f1 then nan
        else { u = [ (F.succ f1, F.inf ~ew ~mw false) ]; nan = true }

    let%test_unit "ge" = cmp_test ge F.ge

    let gt ~ew ~mw t =
      if t.nan then top ~ew ~mw
      else
        let f1, _ = List.hd_exn t.u in
        let f1 = if F.is_positive_zero f1 then F.zero ~mw ~ew true else f1 in
        { u = [ (f1, F.inf ~ew ~mw false) ]; nan = true }

    let%test_unit "gt" = cmp_test gt F.gt

    let eq ~ew ~mw t =
      match t with
      | { u = [ (_, b) ]; nan = false } when F.is_negative_infinite b ->
          { u = [ (F.succ b, F.inf ~ew ~mw false) ]; nan = true }
      | { u = [ (a, _) ]; nan = false } when F.is_positive_infinite a ->
          { u = [ (F.inf ~ew ~mw true, F.pred a) ]; nan = true }
      | { u = [ (a, b) ]; nan = false } when F.eq a b ->
          {
            u =
              [
                (F.inf ~ew ~mw true, F.pred a); (F.succ b, F.inf ~ew ~mw false);
              ];
            nan = true;
          }
      | _ -> top ~ew ~mw

    let%test_unit "eq" = cmp_test eq F.eq
  end
end

module Forward = struct
  let binary_test_with_mode f =
    let test =
      QCheck2.Test.make ~count
        ~print:(fun (m, x, y) ->
          Fmt.str "%a and %a (in %a)" pp x pp y Mode.pp m)
        (QCheck2.Gen.triple Mode.gen gen gen)
        f
    in
    QCheck2.Test.check_exn test

  let binary_test f ff =
    binary_test_with_mode (fun (m, t1, t2) ->
        let t3 = f ~mw:23 ~ew:8 m t1 t2 in
        let vl1 = List.filter ~f:(fun v -> mem t1 v) @@ interesting_values t1 in
        let vl2 = List.filter ~f:(fun v -> mem t2 v) @@ interesting_values t2 in
        List.iter vl1 ~f:(fun v1 ->
            List.iter vl2 ~f:(fun v2 ->
                let v3 = ff m v1 v2 in
                if not (mem t3 v3) then
                  QCheck2.Test.fail_reportf
                    "Error with %a and %a the results@ %a ∉ %a" F.pp v1 F.pp v2
                    F.pp v3 pp t3));
        true)

  let binary_union_of_product ~f t1 t2 =
    List.fold
      ~init:(if t1.nan || t2.nan then nan else bot_internal)
      t1.u
      ~f:(fun acc e1 ->
        List.fold ~init:acc t2.u ~f:(fun acc e2 -> union (f e1 e2) acc))

  let add ~mw ~ew mode t1 t2 =
    binary_union_of_product t1 t2 ~f:(fun (a1, b1) (a2, b2) ->
        let a3 = F.add mode a1 a2 in
        let b3 = F.add mode b1 b2 in
        match (F.is_nan a3, F.is_nan b3) with
        | true, true -> (* -∞ + +∞ *) nan
        | false, false ->
            {
              u = [ (a3, b3) ];
              nan =
                (F.is_positive_infinite b1 && F.is_negative_infinite a2)
                || (F.is_positive_infinite b2 && F.is_negative_infinite a1);
            }
        | true, false ->
            (* b1 = +∞ \/ b2 = +∞ *)
            let infp = F.inf ~mw ~ew false in
            { u = [ (infp, infp) ]; nan = true }
        | false, true ->
            (* a1 = -∞ \/ a2 = -∞ *)
            let infm = F.inf ~mw ~ew true in
            { u = [ (infm, infm) ]; nan = true })

  let%test_unit "add" = binary_test add F.add

  let split sv (a, b) =
    if F.is_positive a then (None, Some (a, b))
    else if F.is_negative b then (Some (F.abs b, F.abs a), None)
    else (Some (sv.fpos.zero, F.abs a), Some (sv.fpos.zero, b))

  let neg_mode = function
    | Mode.UP -> Mode.DN
    | Mode.DN -> Mode.UP
    | Mode.NE -> Mode.NE
    | Mode.NA -> Mode.NA
    | Mode.ZR -> Mode.ZR

  let mul ~mw ~ew mode t1 t2 =
    let sv = get_special_values ~mw ~ew in
    binary_union_of_product t1 t2 ~f:(fun ab1 ab2 ->
        let neg1, pos1 = split sv ab1 in
        let neg2, pos2 = split sv ab2 in
        let aux sign a1 a2 =
          match (a1, a2) with
          | Some (a1, b1), Some (a2, b2) ->
              let mode = if sign then neg_mode mode else mode in
              let a = F.mul mode a1 a2 in
              let b = F.mul mode b1 b2 in
              if F.is_nan a then
                (* a1,b1 is inf+ ||  a2,b2 is inf+ *)
                if F.is_nan b then (* b2 is zero || b1 is zero *)
                  nan
                else
                  (* b2 not zero || b1 not zero *)
                  union (singleton (F.inf ~mw ~ew sign)) nan
              else if F.is_nan b then
                (* a1,b1 is zero || a2,b2 is zero *)
                (* a2 is not inf || a1 is not inf *)
                union (singleton (F.zero ~mw ~ew sign)) nan
              else
                {
                  u = [ (if sign then (F.neg b, F.neg a) else (a, b)) ];
                  nan =
                    (F.is_zero a1 && F.is_infinite b2)
                    || (F.is_zero a2 && F.is_infinite b1);
                }
          | _ -> bot_internal
        in
        union
          (union (aux false pos1 pos2) (aux false neg1 neg2))
          (union (aux true pos1 neg2) (aux true neg1 pos2)))

  let%test_unit "mul" = binary_test mul F.mul

  let div ~mw ~ew mode t1 t2 =
    let sv = get_special_values ~mw ~ew in
    binary_union_of_product t1 t2 ~f:(fun ab1 ab2 ->
        let neg1, pos1 = split sv ab1 in
        let neg2, pos2 = split sv ab2 in
        let aux sign a1 a2 =
          match (a1, a2) with
          | Some (a1, b1), Some (a2, b2) ->
              let mode = if sign then neg_mode mode else mode in
              let a = F.div mode a1 b2 in
              let b = F.div mode b1 a2 in
              if F.is_nan a then
                (* a1,b1 is inf+ ||  a2,b2 is zero *)
                if F.is_nan b then (* a2 is inf || b1 is zero *)
                  nan
                else
                  (* a2 not inf || b1 not zero *)
                  union (singleton (F.inf ~mw ~ew sign)) nan
              else if F.is_nan b then
                (* a1,b1 is zero || a2,b2 is inf *)
                (* a2 is not zero || a1 is not inf *)
                union (singleton (F.zero ~mw ~ew sign)) nan
              else
                {
                  u = [ (if sign then (F.neg b, F.neg a) else (a, b)) ];
                  nan =
                    (F.is_zero a1 && F.is_zero a2)
                    || (F.is_infinite b2 && F.is_infinite b1);
                }
          | _ -> bot_internal
        in
        union
          (union (aux false pos1 pos2) (aux false neg1 neg2))
          (union (aux true pos1 neg2) (aux true neg1 pos2)))

  let%test_unit "div" = binary_test div F.div

  let of_real ~mw ~ew mode (min, max) =
    let min =
      match min with
      | None -> F.inf ~mw ~ew true
      | Some (min, _) -> F.of_q ~mw ~ew mode @@ A.floor_q min
    in
    let max =
      match max with
      | None -> F.inf ~mw ~ew false
      | Some (max, _) -> F.of_q ~mw ~ew mode @@ A.ceil_q max
    in
    { u = [ (min, max) ]; nan = false }

  (** {2 ieee bits convertion} *)

  let of_ieee_bits ~mw ~ew (min, max) =
    match (min, max) with
    | Some (min, _), Some (max, _) ->
        let min = A.to_z @@ A.floor min in
        let max = A.to_z @@ A.ceil max in
        if Z.equal min max && Z.sign min >= 0 && Z.numbits min <= mw + ew + 1
        then singleton (F.of_bits ~mw ~ew min)
        else top ~mw ~ew
    | _ -> top ~mw ~ew
end
