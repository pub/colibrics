(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

module Id = Ident
module Push = Datastructure.Push
module Ref = Datastructure.Ref

module HT = Id.MkIHT (struct
  let name = "NSeqContentHT"

  include Id.S
end)

let new_ns_kv_hook =
  Ref.create Fmt.nop "NS-KV-hook" (fun _ _ _ _ -> assert false)

let new_arr_kv_hook =
  Ref.create Fmt.nop "Arr-KV-hook" (fun _ _ _ _ -> assert false)

let set_new_ns_kv_hook env f = Ref.set new_ns_kv_hook env f
let set_new_arr_kv_hook env f = Ref.set new_arr_kv_hook env f
let new_content_hooks = Push.create Fmt.nop "NSeqContent.new_content_hooks"
let add_new_content_hook env f = Push.push new_content_hooks env f

let exists env aid nsid =
  match HT.find_opt env aid with None -> false | Some s -> Id.S.mem nsid s

let add_aux env id1 id2 =
  HT.change env id1 ~f:(function
    | None -> Some (Id.S.singleton id2)
    | Some s -> Some (Id.S.add id2 s))

let new_content env aid nsid =
  if not (exists env aid nsid) then
    Push.iter new_content_hooks env ~f:(fun f -> f env aid nsid);
  add_aux env aid nsid;
  add_aux env aid nsid

let add_arr_kv env aid i v =
  match HT.find_opt env aid with
  | None -> ()
  | Some ns_set when Id.S.is_empty ns_set -> ()
  | Some ns_set ->
      let f = Ref.get new_arr_kv_hook env in
      Id.S.iter (fun ns_id -> f env ns_id i v) ns_set

let add_ns_kv env nsid iid v =
  match HT.find_opt env nsid with
  | None -> ()
  | Some aset when Id.S.is_empty aset -> ()
  | Some aset ->
      let f = Ref.get new_ns_kv_hook env in
      Id.S.iter (fun aid -> f env aid iid v) aset

let subst_ids env rid kid =
  match HT.find_opt env rid with
  | None -> ()
  | Some s ->
      Id.S.iter
        (fun id ->
          HT.change env id ~f:(function
            | None -> assert false
            | Some s ->
                assert (Id.S.mem rid s);
                Some (Id.S.add kid (Id.S.remove rid s))))
        s;
      HT.change env kid ~f:(function
        | None -> Some s
        | Some s' -> Some (Id.S.union s s'))
