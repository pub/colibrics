(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib

module type S = sig
  type t

  val pp : Format.formatter -> t -> unit
  val equal : t -> t -> bool

  val hash_fold_t :
    Base_internalhash_types.state -> t -> Base_internalhash_types.state

  module M : Map_intf.PMap with type key = t
  module S : Map_intf.Set with type 'a M.t = 'a M.t and type M.key = M.key
  module H : Exthtbl.Hashtbl.S with type key = t
  include Base.Hashtbl.Key.S with type t := t
end

type t

include S with type t := t

exception NoIdFound of string * Node.t

module type HTS = sig
  type key
  type t

  val set : Egraph.wt -> key -> t -> unit
  val find : _ Egraph.t -> key -> t
  val find_opt : _ Egraph.t -> key -> t option
  val change : f:(t option -> t option) -> Egraph.wt -> key -> unit
  val remove : Egraph.wt -> key -> unit
  val iter : f:(key -> t -> unit) -> _ Egraph.t -> unit
  val fold : (key -> t -> 'a -> 'a) -> _ Egraph.t -> 'a -> 'a
  val pp : Format.formatter -> _ Egraph.t -> unit
end

module MkIHT (V : sig
  type t

  val pp : t Pp.pp
  val name : string
end) : HTS with type key = t and type t = V.t

val new_id : unit -> t

module type IdDomSig = sig
  val pp : _ Egraph.t -> Format.formatter -> Node.t -> unit
  val register_merge_hook : Egraph.wt -> (Egraph.wt -> t -> t -> unit) -> unit

  val register_new_id_hook :
    Egraph.wt -> (Egraph.wt -> t -> Node.t -> unit) -> unit

  val set_id : Egraph.wt -> ?debug:Colibri2_stdlib.Debug.flag -> Node.t -> unit
  val get_id : _ Egraph.t -> Node.t -> t

  val get_id_safe :
    Egraph.wt -> ?debug:Colibri2_stdlib.Debug.flag -> Node.t -> t
  (** tries to get the id, if it doesn't exist, sets it *)
end

module MkIdDom (_ : sig
  val name : string
end) : IdDomSig
