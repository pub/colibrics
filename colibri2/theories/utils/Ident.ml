(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_core
open Colibri2_popop_lib
open Popop_stdlib
module HT = Datastructure.Hashtbl (DInt)

module type S = sig
  type t

  val pp : Format.formatter -> t -> unit
  val equal : t -> t -> bool

  val hash_fold_t :
    Base_internalhash_types.state -> t -> Base_internalhash_types.state

  module M : Map_intf.PMap with type key = t
  module S : Map_intf.Set with type 'a M.t = 'a M.t and type M.key = M.key
  module H : Exthtbl.Hashtbl.S with type key = t
  include Base.Hashtbl.Key.S with type t := t
end

include DInt

exception NoIdFound of string * Node.t

let () =
  Printexc.register_printer (function
    | NoIdFound (s, n) ->
        Some (Fmt.str "get_id(%s) of %a: No Id found!" s Node.pp n)
    | _ -> None)

module type HTS = sig
  type key
  type t

  val set : Egraph.wt -> key -> t -> unit
  val find : _ Egraph.t -> key -> t
  val find_opt : _ Egraph.t -> key -> t option
  val change : f:(t option -> t option) -> Egraph.wt -> key -> unit
  val remove : Egraph.wt -> key -> unit
  val iter : f:(key -> t -> unit) -> _ Egraph.t -> unit
  val fold : (key -> t -> 'a -> 'a) -> _ Egraph.t -> 'a -> 'a
  val pp : Format.formatter -> _ Egraph.t -> unit
end

module MkIHT (V : sig
  type t

  val pp : t Pp.pp
  val name : string
end) : HTS with type key = t and type t = V.t = struct
  type key = t
  type t = V.t

  let db = HT.create V.pp V.name
  let set env i v = HT.set db env i v
  let find env = HT.find db env
  let find_opt env = HT.find_opt db env
  let change ~f env i = HT.change f db env i
  let remove env i = HT.remove db env i
  let iter = HT.iter db
  let fold f env acc = HT.fold f db env acc

  let pp fmt env =
    Fmt.pf fmt "%s:[%a]" V.name
      (fun fmt () ->
        iter env ~f:(fun i v -> Fmt.pf fmt "\n  (%d -> (%a));" i V.pp v))
      ()
end

module type IdDomSig = sig
  val pp : _ Egraph.t -> Format.formatter -> Node.t -> unit

  val register_merge_hook :
    Egraph.wt -> (Egraph.wt -> int -> int -> unit) -> unit

  val register_new_id_hook :
    Egraph.wt -> (Egraph.wt -> int -> Node.t -> unit) -> unit

  val set_id : Egraph.wt -> ?debug:Colibri2_stdlib.Debug.flag -> Node.t -> unit
  val get_id : _ Egraph.t -> Node.t -> int

  val get_id_safe :
    Egraph.wt -> ?debug:Colibri2_stdlib.Debug.flag -> Node.t -> int
end

(** Global Id counter *)
let new_id =
  let id_counter = ref 0 in
  fun () ->
    incr id_counter;
    !id_counter

module MkIdDom (N : sig
  val name : string
end) : IdDomSig = struct
  let merge_hooks = Datastructure.Push.create Fmt.nop (N.name ^ ".merge_hooks")

  let register_merge_hook env (f : Egraph.wt -> int -> int -> unit) =
    Datastructure.Push.push merge_hooks env f

  module D = struct
    include Dom.Make (struct
      type t = DInt.t

      let equal = DInt.equal
      let pp = DInt.pp
      let is_singleton _ _ _ = None

      let key =
        Dom.Kind.create
          (module struct
            type nonrec t = t

            let name = N.name ^ ".DOM"
          end)

      let inter _ v _ = Some v
      let wakeup_threshold = None
    end)

    let merge env (d1, n1) (d2, n2) b =
      (match (d1, n1, d2, n2) with
      | Some id1, _, Some id2, _ ->
          (* let id1, id2 = if b then (id2, id1) else (id1, id2) in *)
          Datastructure.Push.iter merge_hooks env ~f:(fun f -> f env id1 id2)
      | _ -> ());
      merge env (d1, n1) (d2, n2) b
  end

  let () = Dom.register (module D)

  let pp env fmt n =
    match Egraph.get_dom env D.key n with
    | Some id -> Fmt.pf fmt "%a(%d)" Node.pp n id
    | None -> Fmt.pf fmt "%a(NoIdFound)" Node.pp n

  let new_id_hooks = Datastructure.Push.create Fmt.nop (N.name ^ ".new_id_hooks")

  let register_new_id_hook env (f : Egraph.wt -> int -> Node.t -> unit) =
    Datastructure.Push.push new_id_hooks env f

  let set_id, get_id, get_id_safe =
    let set_id env ?debug n =
      match Egraph.get_dom env D.key n with
      | None ->
          let id = new_id () in
          Opt.iter
            (fun debug ->
              Debug.dprintf4 debug "set_id(%s) of %a: %d" N.name Node.pp n id)
            debug;
          D.set_dom env n id;
          Datastructure.Push.iter new_id_hooks env ~f:(fun new_id_hook ->
              new_id_hook env id n)
      | Some _ -> ()
    in
    let get_id env n =
      match Egraph.get_dom env D.key n with
      | Some id -> id
      | None -> raise (NoIdFound (N.name, n))
    in
    let get_id_safe env ?debug n =
      match Egraph.get_dom env D.key n with
      | Some id -> id
      | None ->
          set_id env ?debug n;
          get_id env n
    in
    (set_id, get_id, get_id_safe)
end
