(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    OCamlPro                                                           *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

val set_new_ns_kv_hook :
  Egraph.wt -> (Egraph.wt -> Ident.t -> Node.t -> Node.t -> unit) -> unit

val set_new_arr_kv_hook :
  Egraph.wt -> (Egraph.wt -> Ident.t -> Node.t -> Node.t -> unit) -> unit

val add_new_content_hook :
  Egraph.wt -> (Egraph.rw Egraph.t -> Ident.t -> Ident.t -> unit) -> unit

val new_content : Egraph.wt -> Ident.t -> Ident.t -> unit
val add_arr_kv : Egraph.wt -> Ident.t -> Node.t -> Node.t -> unit
val add_ns_kv : Egraph.wt -> Ident.t -> Node.t -> Node.t -> unit
val subst_ids : Egraph.wt -> Ident.t -> Ident.t -> unit
