(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib
open Common

(** 1) match the subterm 2) possible patterns found 3) GoUp and match other
    arguments 4) goto 2) *)

type data =
  | Trigger of Trigger.t  (** triggers reached *)
  | Callback of Callback.t  (** generic triggers *)

and t' = data OptiPattern.inv_path
and t = { id : int; r : t' Context.Ref.t } [@@deriving show]

include (
  Popop_stdlib.MakeMSH (struct
    type nonrec t = t

    let tag t = t.id
    let pp = pp
  end) :
    Popop_stdlib.Datatype with type t := t)

let create =
  let get_counter, incr_counter = Util.get_counter () in
  fun creator ->
    incr_counter ();
    {
      id = get_counter ();
      r = Context.Ref.create creator OptiPattern.empty_inv_path;
    }

let stat = Debug.register_stats_int "Quantifier.InvertedPath.exec"

let exec d acc n ip =
  Debug.dprintf2 debug_full "[Quant] Exec: %a" Node.pp n;
  (* pp ip; *)
  let fold (triggers, callbacks) data subst =
    match data with
    | Trigger tr ->
        Debug.dprintf2 debug_full "[Quant] Trigger found %a" Trigger.pp tr;
        ( Trigger.M.add_change
            (fun s -> Ground.Subst.S.singleton s)
            Ground.Subst.S.add tr subst triggers,
          callbacks )
    | Callback call ->
        Debug.dprintf2 debug_full "[Quant] Callback %a found" Callback.pp call;
        ( triggers,
          Callback.M.add_change
            (fun s -> Ground.Subst.S.singleton s)
            Ground.Subst.S.add call subst callbacks )
  in
  let aux acc c =
    let r = ref acc in
    let res data subst = r := fold !r data subst in
    OptiPattern.exec_inv_path ~res d c ~down:n ~up:n;
    !r
  in
  aux acc (Context.Ref.get ip.r)

let exec_stats = Debug.register_stats_time "Quantifier.exec"

let exec d acc n ip =
  Debug.add_time_during exec_stats @@ fun _ ->
  Debug.dprintf5 debug "[Quant] IP Exec %i: %a: %a" ip.id Node.pp n pp ip;
  Debug.incr stat;
  exec d acc n ip

module GenFind
    (M1 : Map_intf.PMap)
    (M2 : Map_intf.PMap)
    (N : sig
      val name : string
    end) =
struct
  let ips : t M2.t M1.t Datastructure.Ref.t =
    Datastructure.Ref.create Fmt.nop N.name M1.empty

  let find d k1 k2 =
    let m1 = Datastructure.Ref.get ips d in
    match M1.find k1 m1 with
    | exception Not_found ->
        let c = create (Egraph.context d) in
        Datastructure.Ref.set ips d @@ M1.add k1 (M2.singleton k2 c) m1;
        c
    | m -> (
        match M2.find k2 m with
        | exception Not_found ->
            let c = create (Egraph.context d) in
            Datastructure.Ref.set ips d @@ M1.add k1 (M2.add k2 c m) m1;
            c
        | c -> c)
end

(** parent-parent, a variable appears two times *)
module MPP =
  GenFind (F_Pos.M) (F_Pos.M)
    (struct
      let name = "Quantifier.pp_ips"
    end)

(** parent-child, wait for a subterm with the right application *)
module MPC =
  GenFind (F_Pos.M) (F.M)
    (struct
      let name = "Quantifier.pc_ips"
    end)

(** parent-type, wait for a subterm with the right type (to match a variable) *)
module MPT =
  GenFind (F_Pos.M) (Expr.Ty.M)
    (struct
      let name = "Quantifier.pt_ips"
    end)

(** parent-node, wait for a subterm with the right class *)
module MPN =
  GenFind (F_Pos.M) (Node.M)
    (struct
      let name = "Quantifier.pn_ips"
    end)

type 'a generic_pattern_inv = Match of Pattern.t * t list | Node of t
[@@deriving show]

type 'a generic_pattern_up_inv =
  | Ups of
      F_Pos.t * Expr.Ty.t list * Pattern.t IArray.t * 'a generic_pattern_up_inv
  | M of 'a generic_pattern_inv
[@@deriving show]

type 'a generic_pattern_end_inv =
  | AnyMatch of Pattern.t * 'a generic_pattern_end_inv
  | U of 'a generic_pattern_up_inv list
[@@deriving show]

let add_in_ip v ipr =
  let ip = Context.Ref.get ipr.r in
  Context.Ref.set ipr.r @@ OptiPattern.merge_inv_path v ip

let rec reverse_end acc = function
  | AnyMatch (p, l) -> reverse_end (OptiPattern.AnyMatch (p, acc)) l
  | U l -> List.iter (reverse_up (OptiPattern.Multi acc)) l

and reverse_up acc = function
  | Ups (fp, tyl, pargs, l) ->
      reverse_up (OptiPattern.Ups (fp, tyl, pargs, acc)) l
  | M (Match (p, l)) ->
      let acc = OptiPattern.Match (p, acc) in
      let c = OptiPattern.convert_inv_path acc in
      List.iter (add_in_ip c) l
  | M (Node ip) ->
      let acc = OptiPattern.Direct acc in
      let c = OptiPattern.convert_inv_path acc in
      add_in_ip c ip

let add_trigger pat ip = reverse_end (End (Trigger pat)) ip
let add_callback pat ip = reverse_end (End (Callback pat)) ip

let insert_pattern d (pattern : Pattern.t) pats =
  let rec aux (fp : F_Pos.t) pp_vars p =
    match p with
    | Pattern.Var v ->
        let pairs = Expr.Term.Var.M.find_def [] v.var pp_vars in
        let pairs =
          List.filter (fun pp -> F_Pos.equal fp pp.PP.parent1) pairs
        in
        let ips =
          List.map (fun pp -> MPP.find d pp.PP.parent1 pp.PP.parent2) pairs
        in
        let ip = MPT.find d fp v.ty in
        let ips = ip :: ips in
        [ M (Match (p, ips)) ]
    | App (f, tytl, tl) ->
        let ips = insert_children pp_vars f tytl tl in
        let ip = MPC.find d fp f in
        let ip = M (Match (p, [ ip ])) in
        let ips = ip :: ips in
        ips
    | Node n ->
        let ip = MPN.find d fp n in
        let ips = [ M (Node ip) ] in
        ips
  and insert_children pp_vars f tytl tl =
    IArray.foldi
      ~f:(fun pos acc p ->
        let f_pos = F_Pos.{ f; pos } in
        let ips = aux f_pos pp_vars p in
        let ips = List.map (fun ip -> Ups (f_pos, tytl, tl, ip)) ips in
        ips @ acc)
      ~init:[] tl
  in
  let pp_vars = Pattern.get_pps pattern [] in
  match pattern with
  | Var _ | Node _ -> None
  | App (f, tytl, tl) ->
      let ips = insert_children pp_vars f tytl tl in
      let r =
        Base.List.fold_left ~f:(fun ip p -> AnyMatch (p, ip)) ~init:(U ips) pats
      in
      Some r

let insert_trigger d (trigger : Trigger.t) =
  let ips = insert_pattern d trigger.pat trigger.pats in
  Debug.dprintf4 debug_full "[Quant] For trigger %a inserts %a" Trigger.pp
    trigger
    Fmt.(option (pp_generic_pattern_end_inv pp_data))
    ips;
  Option.iter (add_trigger trigger) ips

let add_callback d pat f =
  let callback = Callback.mk d f pat in
  Debug.dprintf4 Common.debug "Add callback %a for %a" Callback.pp callback
    Pattern.pp pat;
  Trigger.add_callback d callback;
  let ips = insert_pattern d pat [] in
  Option.iter (add_callback callback) ips;
  let substs = Pattern.match_any_term d Pattern.init pat in
  Callback.run d callback substs

let add_trigger d t =
  Trigger.add_trigger d t;
  insert_trigger d t;
  let substs =
    List.fold_left
      (fun acc pat -> Pattern.match_any_term d acc pat)
      Pattern.init (t.pat :: t.pats)
  in
  Trigger.instantiate_many d t substs
