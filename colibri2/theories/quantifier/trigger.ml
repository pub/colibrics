(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib
open Common

type inst_step = NotSeen | Delayed | Instantiated
type eagerness = Eager | Delayed [@@deriving show]

module T : sig
  type t = private {
    id : int;
    pat : Pattern.t;
    pats : Pattern.t list;
    checks : Pattern.t list;
    form : Ground.ClosedQuantifier.t;
    eager : eagerness;
    substs : inst_step SubstTrie.t;
  }

  include Popop_stdlib.TaggedType with type t := t

  val mk :
    pat:Pattern.t ->
    pats:Pattern.t list ->
    checks:Pattern.t list ->
    form:Ground.ClosedQuantifier.t ->
    eager:eagerness ->
    _ Egraph.t ->
    t
end = struct
  open! Base

  type t = {
    id : int;
    pat : Pattern.t;
    pats : Pattern.t list;
    checks : Pattern.t list;
    form : Ground.ClosedQuantifier.t;
    eager : eagerness;
    substs : inst_step SubstTrie.t;
  }

  let tag t = t.id

  let pp fmt t =
    Fmt.pf fmt "[%a, %a ( %a ) -> %a]" Pattern.pp t.pat
      Fmt.(list ~sep:comma Pattern.pp)
      t.pats
      Fmt.(list ~sep:comma Pattern.pp)
      t.checks Ground.ClosedQuantifier.pp t.form

  let id_get, id_incr = Util.get_counter ()

  let mk ~pat ~pats ~checks ~form ~eager d =
    id_incr ();
    {
      id = id_get ();
      pat;
      pats;
      checks;
      form;
      eager;
      substs = SubstTrie.create (Egraph.context d);
    }
end

include T
include (Popop_stdlib.MakeMSH (T) : Popop_stdlib.Datatype with type t := t)

let pattern_remove_coercion_option =
  Options.register ~pp:Fmt.bool "Quant.pattern_coercion"
    Cmdliner.Arg.(
      value & flag
      & info [ "remove-coercion-in-pattern" ] ~doc:"Remove coercion in patterns")

let pattern_of_term d (cq : Ground.ClosedQuantifier.s) pat =
  try
    Pattern.of_term_exn
      ~coercion:(not (Options.get d pattern_remove_coercion_option))
      ~subst:cq.subst pat
  with Pattern.Unconvertible -> raise Impossible

let compute_top_triggers d (cq : Ground.ClosedQuantifier.t) =
  let cq' = Ground.ClosedQuantifier.sem cq in
  let tyvs = cq'.ty_vars in
  let tvs = cq'.term_vars in
  let t = cq'.body in
  let rec aux pats (t : Expr.Term.t) =
    match t.term_descr with
    | Expr.Binder (Let_seq l, f) ->
        (* could be both optimized with Let inside pattern; and efficient handling in
           match and invertedpath *)
        let subst =
          List.fold_left
            (fun subst (v, t) ->
              let t = Expr.Term.subst Expr.Subst.empty subst t in
              Expr.Subst.Var.bind subst v t)
            Expr.Subst.empty l
        in
        aux pats (Expr.Term.subst Expr.Subst.empty subst f)
    | Expr.Binder (Let_par l, f) ->
        let subst =
          List.fold_left
            (fun subst (v, t) -> Expr.Subst.Var.bind subst v t)
            Expr.Subst.empty l
        in
        aux pats (Expr.Term.subst Expr.Subst.empty subst f)
    | Expr.Binder (_, _) -> pats (* todo *)
    | Expr.Match (_, _) -> pats (* todo *)
    | Expr.App ({ term_descr = Cst { builtin; _ }; _ }, _, tl)
      when Info.Builtin_skipped_for_trigger.skipped d builtin ->
        List.fold_left aux pats tl
    | Expr.Var _ ->
        (* variable alone are bad pattern *)
        pats
    | _ -> Expr.Term.ForSmallTerm.remove_lets t :: pats
  in
  let pats = CCList.sort_uniq ~cmp:Expr.Term.compare (aux [] t) in
  let tyvs = Expr.Ty.Var.S.of_list tyvs in
  let tvs = Expr.Term.Var.S.of_list tvs in
  let pats =
    Base.List.map
      ~f:(fun pat ->
        let sty, st =
          Expr.Term.free_vars (Expr.Ty.Var.S.empty, Expr.Term.Var.S.empty) pat
        in
        let pat = pattern_of_term d cq' pat in
        (pat, sty, st))
      pats
  in
  let pats_full, pats_partial =
    Base.List.partition_tf
      ~f:(fun (_, sty, st) ->
        Expr.Ty.Var.S.subset tyvs sty && Expr.Term.Var.S.subset tvs st)
      pats
  in
  (* eliminate pattern variable *)
  let pats_partial =
    List.filter
      (function Pattern.Var _, _, _ -> false | _ -> true)
      pats_partial
  in
  let get_pat = List.map (fun (pat, _, _) -> pat) in
  if Base.List.is_empty pats_full then (
    (* compute smallest subsets of pats_partial that binds all the variables *)
    let rec aux acc pats_candidate remaining_sty remaining_st = function
      | [] -> acc
      | (pat, sty, st) :: pats_partial ->
          if
            Expr.Ty.Var.S.disjoint remaining_sty sty
            && Expr.Term.Var.S.disjoint remaining_st st
          then aux acc pats_candidate remaining_sty remaining_st pats_partial
          else
            let acc =
              aux acc pats_candidate remaining_sty remaining_st pats_partial
            in
            let remaining_sty = Expr.Ty.Var.S.diff remaining_sty sty in
            let remaining_st = Expr.Term.Var.S.diff remaining_st st in
            if
              Expr.Ty.Var.S.is_empty remaining_sty
              && Expr.Term.Var.S.is_empty remaining_st
            then (pat :: pats_candidate) :: acc
            else
              aux acc (pat :: pats_candidate) remaining_sty remaining_st
                pats_partial
    in
    let full_subsets = aux [] [] tyvs tvs pats_partial in
    let full_subsets =
      Base.List.sort full_subsets ~compare:(fun l m ->
          Int.compare (List.length l) (List.length m))
    in
    let full_subsets = Base.List.take full_subsets 4 in
    let pats_partial = get_pat pats_partial in
    let pats_full_with_others =
      let rec aux acc other = function
        | [] -> acc
        | a :: l ->
            aux
              (mk d ~pat:a ~pats:(other @ l) ~checks:pats_partial ~form:cq
                 ~eager:Eager
              :: acc)
              (a :: other) l
      in
      List.concat_map (fun pats -> aux [] [] pats) full_subsets
    in
    Debug.dprintf4 debug
      "@[@[<hv>no pats_full,@ @[pats_partial:%a@],@ @[tri:%a@]@]@]@."
      Fmt.(list ~sep:comma Pattern.pp)
      pats_partial
      Fmt.(list ~sep:comma (hovbox pp))
      pats_full_with_others;
    pats_full_with_others)
  else
    let pats_full = get_pat pats_full in
    let pats_partial = get_pat pats_partial in
    let pats_full_with_others =
      let rec aux acc other = function
        | [] -> acc
        | a :: l ->
            aux
              (mk d ~pat:a ~pats:[]
                 ~checks:(other @ l @ pats_partial)
                 ~form:cq ~eager:Eager
              :: acc)
              (a :: other) l
      in

      aux [] [] pats_full
    in
    Debug.dprintf6 debug
      "@[@[pats_full:%a@],@ @[pats_partial:%a@],@ @[tri:%a@]@]@."
      Fmt.(list ~sep:comma Pattern.pp)
      pats_full
      Fmt.(list ~sep:comma Pattern.pp)
      pats_partial
      Fmt.(list ~sep:comma pp)
      pats_full_with_others;
    pats_full_with_others

let compute_all_triggers d (cq : Ground.ClosedQuantifier.t) =
  let cq' = Ground.ClosedQuantifier.sem cq in
  let tyvs = cq'.ty_vars in
  let tvs = cq'.term_vars in
  let t = cq'.body in
  let union (sty, st) = function
    | None -> None
    | Some (sty', st') ->
        Some (Expr.Ty.Var.S.union sty sty', Expr.Term.Var.S.union st st')
  in
  let add t fv pats = (t, fv) :: pats in
  let rec aux (pats, acc_fv) (t : Expr.Term.t) =
    match t.term_descr with
    | Var _ ->
        let fv =
          Expr.Term.free_vars (Expr.Ty.Var.S.empty, Expr.Term.Var.S.empty) t
        in
        (pats, union fv acc_fv)
    | Expr.Cst _ ->
        (* let fv = (Expr.Ty.Var.S.empty, Expr.Term.Var.S.empty) in *)
        (pats, acc_fv)
    | Expr.App ({ term_descr = Cst { builtin; _ }; _ }, tyl, tl) -> (
        let pats, fv =
          List.fold_left aux
            (pats, Some (Expr.Ty.Var.S.empty, Expr.Term.Var.S.empty))
            tl
        in
        match fv with
        | None -> (pats, None)
        | Some _ when Info.Builtin_skipped_for_trigger.skipped d builtin ->
            (pats, fv)
        | Some (sty, st) ->
            let fv = (List.fold_left Expr.Ty.free_vars sty tyl, st) in
            (add t fv pats, union fv acc_fv))
    | Expr.App (_, _, _) -> (pats, None) (* todo? *)
    | Expr.Binder (_, _) -> (pats, None) (* todo *)
    | Expr.Match (_, _) -> (pats, None)
    (* todo *)
  in
  let pats, _ = aux ([], None) t in
  let tyvs = Expr.Ty.Var.S.of_list tyvs in
  let tvs = Expr.Term.Var.S.of_list tvs in
  let pats =
    List.filter_map
      (fun (c, (sty, st)) ->
        if Expr.Ty.Var.S.subset tyvs sty && Expr.Term.Var.S.subset tvs st then
          Some
            (mk d ~pat:(pattern_of_term d cq' c) ~pats:[] ~form:cq ~eager:Eager
               ~checks:[])
        else None)
      pats
  in
  pats

let get_user_triggers d (cq : Ground.ClosedQuantifier.t) =
  let cq' = Ground.ClosedQuantifier.sem cq in
  let pats = Expr.Term.get_tag_list cq'.body Expr.Tags.triggers in
  let tyvs = Expr.Ty.Var.S.of_list cq'.ty_vars in
  let tvs = Expr.Term.Var.S.of_list cq'.term_vars in
  let pats =
    List.map
      (fun pat ->
        match pat with
        | {
         Expr.term_descr =
           App
             ( { term_descr = Cst { builtin = Builtin.Multi_trigger; _ }; _ },
               _,
               pats );
         _;
        } ->
            pats
        | _ -> [ pat ])
      pats
  in
  let pats =
    Base.List.concat_map pats ~f:(fun pats ->
        let sty, st =
          List.fold_left
            (fun acc pat -> Expr.Term.free_vars acc pat)
            (Expr.Ty.Var.S.empty, Expr.Term.Var.S.empty)
            pats
        in
        if Expr.Ty.Var.S.subset tyvs sty && Expr.Term.Var.S.subset tvs st then
          let pats = List.map (pattern_of_term d cq') pats in
          let rec aux acc other = function
            | [] -> acc
            | a :: l ->
                aux
                  (mk d ~pat:a ~pats:(other @ l) ~checks:[] ~form:cq
                     ~eager:Eager
                  :: acc)
                  (a :: other) l
          in
          aux [] [] pats
        else [])
  in
  pats

type tri_callback = Trigger of t | Callback of Callback.t [@@deriving show]

let env_vars =
  Datastructure.Push.create pp_tri_callback "Quantifier.Trigger.vars"

let env_tri_by_apps =
  F.EnvApps.create (Context.Push.pp ~sep:Fmt.semi pp_tri_callback)
    "Quantifier.Trigger.tri_by_apps" (fun c _ -> Context.Push.create c)

let register_pattern d (pat : Pattern.t) t =
  match pat with
  | Var _ -> Datastructure.Push.push env_vars d t
  | App (f, _, _) -> Context.Push.push (F.EnvApps.find env_tri_by_apps d f) t
  | Node _ -> ()

let add_trigger d t = register_pattern d t.pat (Trigger t)
let add_callback d c = register_pattern d (Callback.pattern c) (Callback c)

let debug_new_instantiation =
  Debug.register_info_flag ~desc:"Handling of quantifiers"
    "quantifiers.instantiations"

let mode_option =
  Options.register ~pp:Fmt.bool "Quant.old"
    Cmdliner.Arg.(
      value & flag & info [ "quant-old" ] ~doc:"Use quant old heuristic")

let instantiate_aux d tri subst =
  SubstTrie.set tri.substs subst Instantiated;
  let form = Ground.ClosedQuantifier.sem tri.form in
  Debug.incr nb_instantiation;
  let n =
    if Options.get d mode_option then
      let subst = Ground.Subst.distinct_union subst form.subst in
      Ground.convert ~subst d form.body
    else Subst.convert ~subst_old:form.subst ~subst_new:subst d form.body
  in
  if
    Colibri2_stdlib.Debug.test_flag Colibri2_stdlib.Debug.stats
    && not (Egraph.is_equal d n (Ground.ClosedQuantifier.node tri.form))
    (* not (Egraph.is_registered d n) *)
  then Debug.incr nb_new_instantiation;
  Debug.dprintf6 debug_new_instantiation "[Quant] Instantiate %a with %a as %a"
    Ground.ClosedQuantifier.pp tri.form Ground.Subst.pp subst Node.pp n;
  Trace.message "Instantiate" ~data:(fun () ->
      ("quant", `String (Fmt.str "%a" Ground.ClosedQuantifier.pp tri.form))
      :: List.map
           (fun (v, s) ->
             (Fmt.str "%a" Expr.Term.Var.pp v, `String (Fmt.str "%a" Node.pp s)))
           (Expr.Term.Var.M.bindings subst.term));
  Egraph.register d n;
  Egraph.merge d n (Ground.ClosedQuantifier.node tri.form)

module Delayed_instantiation = struct
  let key =
    Events.Dem.create
      (module struct
        type nonrec t = t * Ground.Subst.t

        let name = "delayed_instantiation"
      end)

  let delay = Events.LastEffortUncontextual 1

  type runable = t * Ground.Subst.t [@@deriving show, eq, hash]

  let print_runable = pp_runable

  let run d (tri, subst) =
    let instantiate d =
      Debug.dprintf10 debug_instantiation
        "[Quant] %a delayed instantiation %a, pat %a %a, checks:%a"
        Ground.Subst.pp subst Ground.ClosedQuantifier.pp tri.form Pattern.pp
        tri.pat
        Fmt.(list ~sep:comma Pattern.pp)
        tri.pats
        Fmt.(list ~sep:comma Pattern.pp)
        tri.checks;
      instantiate_aux d tri subst
    in
    let n = Ground.ClosedQuantifier.node tri.form in
    match (Ground.ClosedQuantifier.sem tri.form, Boolean.is d n) with
    | { binder = Forall; _ }, Some true | { binder = Exists; _ }, Some false ->
        instantiate d
    | _, Some _ -> ()
    | _, None ->
        DaemonOnlyPropa.attach_value d n Boolean.dom (fun d _ v ->
            if Boolean.BoolValue.equal v Boolean.value_true then instantiate d)
end

let () =
  Events.register
    (module Delayed_instantiation)
    ~datatype:(module Delayed_instantiation)

let no_eager_instantiation =
  Options.register ~pp:Fmt.bool "Quant.no_eager_instantiation"
    Cmdliner.Arg.(
      value & flag
      & info [ "no-eager-instantiation" ] ~doc:"Don't do eager instantiation")

let eager_decision =
  Options.register ~pp:Fmt.bool "Quant.eager_decision"
    Cmdliner.Arg.(
      value & flag & info [ "eager-decision" ] ~doc:"Do decisions with eager")

let instantiate' d tri subst =
  let show_debug debug =
    Debug.dprintf12 debug
      "[Quant] %a instantiation found %a, pat %a,%a, checks:%a, eager:%a"
      Ground.Subst.pp subst Ground.ClosedQuantifier.pp tri.form Pattern.pp
      tri.pat
      Fmt.(list ~sep:comma Pattern.pp)
      tri.pats
      Fmt.(list ~sep:comma Pattern.pp)
      tri.checks pp_eagerness tri.eager
  in
  let level_inst = SubstTrie.find_def ~default:NotSeen tri.substs subst in
  let delayed () =
    match level_inst with
    | NotSeen ->
        SubstTrie.set tri.substs subst Delayed;

        Debug.dprintf0 debug_instantiation "[Quant]   Delayed";
        Debug.incr nb_delayed_instantiation;
        Events.new_pending_daemon d Delayed_instantiation.key (tri, subst)
    | Delayed | Instantiated ->
        Debug.dprintf0 debug_instantiation "[Quant] Already delayed"
  in
  let no_eager_instantiation = Options.get d no_eager_instantiation in
  match level_inst with
  | Instantiated -> show_debug debug_full
  | Delayed -> show_debug debug_full
  | NotSeen when no_eager_instantiation -> delayed ()
  | _ ->
      show_debug debug_instantiation;
      if Options.get d mode_option then
        if
          tri.eager = Eager
          && List.for_all
               (fun pat ->
                 not (Node.S.is_empty (Pattern.check_term_exists d subst pat)))
               tri.checks
        then (
          Debug.incr nb_eager_instantiation;
          instantiate_aux d tri subst;
          Debug.dprintf0 debug "[Quant]   Done")
        else delayed ()
      else if tri.eager = Eager then
        let form = Ground.ClosedQuantifier.sem tri.form in
        let choice_group =
          if Options.get d eager_decision then None
          else Some (Choice.Group.create d)
        in
        match
          Subst.convert_avoid_new_terms ~subst_old:form.subst ~subst_new:subst d
            choice_group form.body
        with
        | Some n ->
            if Options.get d eager_decision then
              SubstTrie.set tri.substs subst Instantiated;
            Debug.incr nb_instantiation;
            Debug.incr nb_eager_instantiation;
            if
              Colibri2_stdlib.Debug.test_flag Colibri2_stdlib.Debug.stats
              && not (Egraph.is_registered d n)
            then Debug.incr nb_new_instantiation;
            Debug.dprintf6 debug_new_instantiation
              "[Quant] Instantiate %a with %a and %a" Ground.ClosedQuantifier.pp
              tri.form Ground.Subst.pp subst Ground.Subst.pp form.subst;
            Trace.message "Instantiate" ~data:(fun () ->
                ( "quant",
                  `String (Fmt.str "%a" Ground.ClosedQuantifier.pp tri.form) )
                :: List.map
                     (fun (v, s) ->
                       ( Fmt.str "%a" Expr.Term.Var.pp v,
                         `String (Fmt.str "%a" Node.pp s) ))
                     (Expr.Term.Var.M.bindings form.subst.term)
                @ List.map
                    (fun (v, s) ->
                      ( Fmt.str "%a" Expr.Term.Var.pp v,
                        `String (Fmt.str "%a" Node.pp s) ))
                    (Expr.Term.Var.M.bindings subst.term));

            Egraph.register d n;
            Egraph.merge d n (Ground.ClosedQuantifier.node tri.form);
            if not (Options.get d eager_decision) then delayed ();
            Debug.dprintf0 debug "[Quant]   Done"
        | None -> delayed ()
      else delayed ()

let instantiate d t subst = instantiate' d t (Ground.Subst.map_repr d subst)

let instantiate_many d t substs =
  let substs =
    Ground.Subst.S.fold_left
      (fun acc s -> Ground.Subst.S.add (Ground.Subst.map_repr d s) acc)
      Ground.Subst.S.empty substs
  in
  Ground.Subst.S.iter (instantiate' d t) substs

let match_ d tri g =
  Trace.with_span ~__FUNCTION__ ~__FILE__ ~__LINE__ "match_" @@ fun _ ->
  Debug.dprintf4 debug "[Quant] match %a %a" pp tri Ground.pp g;
  let mvar = Pattern.match_ground d Pattern.init g tri.pat in
  let mvar =
    List.fold_left
      (fun acc pat -> Pattern.match_any_term d acc pat)
      mvar tri.pats
  in
  Ground.Subst.S.iter (fun subst -> instantiate d tri subst) mvar
