(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

(** Trigger *)

type inst_step = NotSeen | Delayed | Instantiated
type eagerness = Eager | Delayed

type t = private {
  id : int;
  pat : Pattern.t;  (** The pattern on which to wait for a substitution *)
  pats : Pattern.t list;
      (** The other ones used to obtain a complete substitution *)
  checks : Pattern.t list;  (** Guards for the existence of terms *)
  form : Ground.ClosedQuantifier.t;  (** the body of the formula *)
  eager : eagerness;
      (** If it should be eagerly applied, otherwise wait for LastEffort *)
  substs : inst_step SubstTrie.t;
}

include Colibri2_popop_lib.Popop_stdlib.Datatype with type t := t

val compute_top_triggers : _ Egraph.t -> Ground.ClosedQuantifier.t -> t list
(** Compute triggers, that should only add logical connective or equalities are
    new terms *)

val compute_all_triggers : _ Egraph.t -> Ground.ClosedQuantifier.t -> t list
(** Compute all the triggers whose patterns contain all the variables of the
    formula *)

val get_user_triggers : _ Egraph.t -> Ground.ClosedQuantifier.t -> t list
(** return the triggers given by the user *)

type tri_callback = Trigger of t | Callback of Callback.t [@@deriving show]

val env_vars : tri_callback Datastructure.Push.t
(** Triggers that are only variables *)

val env_tri_by_apps : tri_callback Context.Push.t F.EnvApps.t
(** Triggers sorted by their top symbol *)

val add_trigger : Egraph.wt -> t -> unit
(** Register a new trigger *)

val add_callback : Egraph.wt -> Callback.t -> unit
(** Register a new trigger *)

val instantiate : Egraph.wt -> t -> Ground.Subst.t -> unit
(** [instantiate d tri subst] instantiates the trigger [tri] with the
    substitution [subst]:

    * now if the trigger is eager and the checks of the trigger exists when
    substituted by [subst]

    * at last effort otherwise *)

val instantiate_many : Egraph.wt -> t -> Ground.Subst.S.t -> unit

val match_ : Egraph.wt -> t -> Ground.t -> unit
(** [match_ d tri g] match the pattern of [tri] with [g] and instantiate [tri]
    with the resulting substitutions *)
