(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

type t
(** An InvertedPath is a way to go from one subterm of patterns to substitution
    of triggers. It allows to know which nodes merge can create new
    substitutions for a pattern and how to find them. Cf Efficient E-matching +
    modifications *)

include Colibri2_popop_lib.Popop_stdlib.Datatype with type t := t

val exec :
  _ Egraph.t ->
  Ground.Subst.S.t Trigger.M.t * Ground.Subst.S.t Callback.M.t ->
  Node.t ->
  t ->
  Ground.Subst.S.t Trigger.M.t * Ground.Subst.S.t Callback.M.t
(** [exec d acc substs n ip] adds to [acc] new substitutions to the triggers
    that are obtained by the execution of the invertedpath *)

val add_callback :
  Egraph.wt -> Pattern.t -> (Egraph.wt -> Ground.Subst.t -> unit) -> unit
(** [add_callback d pat callback] wait for the pattern to be matched *)

val add_trigger : Egraph.wt -> Trigger.t -> unit
(** [add_trigger d trigger] wait for the trigger to be matched *)

module MPP : sig
  val ips : t F_Pos.M.t F_Pos.M.t Datastructure.Ref.t
  (** The database of inverted path for each parent-parent pairs *)
end

module MPC : sig
  val ips : t F.M.t F_Pos.M.t Datastructure.Ref.t
  (** The database of inverted path for each parent-child pairs *)
end

module MPT : sig
  val ips : t Expr.Ty.M.t F_Pos.M.t Datastructure.Ref.t
  (** The database of inverted path for each parent-type, needed for variables
      present unique times *)
end

module MPN : sig
  val ips : t Node.M.t F_Pos.M.t Datastructure.Ref.t
  (** The database of inverted path for each parent-node pairs *)
end
