(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Common
open Colibri2_popop_lib

type t = {
  id : int;
  f : Egraph.wt -> Ground.Subst.t -> unit;
  pat : Pattern.t;
  already : bool SubstTrie.t;
}

let pattern t = t.pat

include (
  Popop_stdlib.MakeMSH (struct
    type nonrec t = t

    let tag t = t.id
    let pp fmt t = Format.pp_print_int fmt t.id
  end) :
    Popop_stdlib.Datatype with type t := t)

let mk =
  let c = ref (-1) in
  fun d f pat ->
    incr c;
    { id = !c; f; already = SubstTrie.create (Egraph.context d); pat }

let run d t substs =
  let substs =
    Ground.Subst.S.fold_left
      (fun acc s -> Ground.Subst.S.add (Ground.Subst.map_repr d s) acc)
      Ground.Subst.S.empty substs
  in
  let iter s =
    let already = SubstTrie.find_and_add t.already s in
    if not already then t.f d s
  in

  Ground.Subst.S.iter iter substs

let match_ d t g =
  Debug.dprintf4 debug "[Quant] match %a %a" pp t Ground.pp g;
  let mvar = Pattern.match_ground d Pattern.init g t.pat in
  run d t mvar
