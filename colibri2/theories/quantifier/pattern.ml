(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib
open Common

(** Patterns *)

module T = struct
  open Base

  type t =
    | Var of { var : Expr.Term.Var.t; ty : Expr.Ty.t }
    | App of F.t * Expr.Ty.t list * t IArray.t
    | Node of Node.t
  [@@deriving eq, ord, hash]

  let rec pp fmt = function
    | Var v ->
        if Expr.Ty.equal v.var.id_ty v.ty then Expr.Term.Var.pp fmt v.var
        else Fmt.pf fmt "(%a:%a)" Expr.Term.Var.pp v.var Expr.Ty.pp v.ty
    | App (f, tyl, tl) ->
        Fmt.pf fmt "%a[%a](%a)" F.pp f
          Fmt.(list ~sep:comma Expr.Ty.pp)
          tyl
          Fmt.(IArray.pp ~sep:comma pp)
          tl
    | Node n -> Node.pp fmt n
end

include T
include Popop_stdlib.MkDatatype (T)

let rec convert_back (ty : Ground.Ty.t) =
  Expr.Ty.apply ty.app (List.map convert_back ty.args)

let rec subst_aux_ty subst (ty : Expr.Ty.t) =
  match ty.Expr.ty_descr with
  | TyVar v -> (
      match Expr.Ty.Var.M.find_opt v subst with
      | Some ty -> convert_back ty
      | None -> ty)
  | TyApp (f, args) -> Expr.Ty.apply f (List.map (subst_ty subst) args)
  | Arrow (args, ret) ->
      let new_args = List.map (subst_aux_ty subst) args in
      let new_ret = subst_aux_ty subst ret in
      Expr.Ty.arrow new_args new_ret
  | Pi (vars, body) ->
      let subst =
        List.fold_left (fun map v -> Expr.Ty.Var.M.remove v map) subst vars
      in
      let new_body = subst_aux_ty subst body in
      Expr.Ty.pi vars new_body

and subst_ty subst t =
  if Expr.Ty.Var.M.is_empty subst then t else subst_aux_ty subst t

exception Unconvertible

type of_term_res = NoVar | Pat of t

let rec of_term_exn ?(coercion = true) ?(subst = Ground.Subst.empty)
    (t : Expr.Term.t) =
  match t.term_descr with
  | Var v -> (
      match Expr.Term.Var.M.find v subst.Ground.Subst.term with
      | exception Not_found ->
          Pat (Var { var = v; ty = subst_ty subst.ty v.id_ty })
      | _ -> NoVar)
  | App ({ term_descr = Cst { builtin = Expr.Coercion; _ }; _ }, _, [ a ])
    when not coercion ->
      of_term_exn ~subst a
  | App
      ( { term_descr = Cst { builtin = Expr.Ite; _ }; _ },
        _,
        [
          cond;
          { term_descr = Cst { builtin = Expr.True; _ }; _ };
          { term_descr = Cst { builtin = Expr.False; _ }; _ };
        ] ) ->
      (* Why3 generates spurious (if v then true else false) *)
      of_term_exn ~subst cond
  | App ({ term_descr = Cst f; _ }, tys, tl) ->
      let tl = IArray.of_list tl in
      let tl' = IArray.map ~f:(of_term_exn ~subst) tl in
      let tys = List.map (subst_ty subst.ty) tys in
      if
        IArray.for_alli
          ~f:(fun _ -> function NoVar -> true | Pat _ -> false)
          tl'
        && List.for_all Expr.Ty.has_no_vars tys
      then NoVar
      else Pat (App (f, tys, IArray.map2_exn ~f:(to_pat ~subst) tl tl'))
  | Cst _ -> NoVar
  | _ -> raise Unconvertible

and to_pat ~subst t = function
  | NoVar -> (
      try Node (Ground.convert_not_choosable ~subst t)
      with _ -> invalid_arg (Fmt.str "term '%a'" Expr.Term.pp t))
  | Pat p -> p

let of_term_exn ?coercion ?(subst = Ground.Subst.empty) (t : Expr.Term.t) =
  to_pat ~subst t @@ of_term_exn ?coercion ~subst t

let init = Ground.Subst.S.singleton Ground.Subst.empty

let rec match_ty substs (ty : Ground.Ty.t) (p : Expr.Ty.t) : Ground.Subst.S.t =
  match p.ty_descr with
  | TyVar v ->
      Ground.Subst.S.fold_left
        (fun acc subst ->
          match Expr.Ty.Var.M.find v subst.ty with
          | exception Not_found ->
              Ground.Subst.S.add
                { subst with ty = Expr.Ty.Var.M.add v ty subst.ty }
                acc
          | ty' ->
              if Ground.Ty.equal ty ty' then Ground.Subst.S.add subst acc
              else acc)
        Ground.Subst.S.empty substs
  | TyApp (cst', tyl') ->
      if Expr.Ty.Const.equal ty.app cst' then
        List.fold_left2 match_ty substs ty.args tyl'
      else Ground.Subst.S.empty
  | _ -> assert false
(* absurd: should not be a trigger *)

let rec match_term d (substs : Ground.Subst.S.t) (n : Node.t) (p : t) :
    Ground.Subst.S.t =
  if Ground.Subst.S.is_empty substs then substs
  else (
    Debug.dprintf4 debug_full "[Quant] matching %a on %a" Node.pp n pp p;
    match p with
    | Var v ->
        let match_ty (acc : Ground.Subst.S.t) ty : Ground.Subst.S.t =
          Debug.dprintf4 debug_full "[Quant] match_ty %a %a" Expr.Ty.pp v.ty
            Ground.Ty.pp ty;
          Ground.Subst.S.union (match_ty substs ty v.ty) acc
        in
        Debug.dprintf4 debug_full "[Quant] Var: %a as type: %a." Node.pp n
          Ground.Ty.S.pp (Info.tys d n);
        let substs =
          Ground.Ty.S.fold_left match_ty Ground.Subst.S.empty (Info.tys d n)
        in
        Ground.Subst.S.fold_left
          (fun acc subst ->
            match Expr.Term.Var.M.find v.var subst.term with
            | exception Not_found ->
                Ground.Subst.S.add
                  { subst with term = Expr.Term.Var.M.add v.var n subst.term }
                  acc
            | n' ->
                if Egraph.is_equal d n n' then Ground.Subst.S.add subst acc
                else acc)
          Ground.Subst.S.empty substs
    | App (pf, ptyl, pargs) ->
        let s =
          Opt.get_def Ground.S.empty
          @@
          let open CCOption in
          let* info = Egraph.get_dom d Info.dom n in
          F.M.find_opt pf info.apps
        in
        Ground.S.fold_left
          (fun acc n ->
            let { Ground.app = f; tyargs = tyl; args; _ } = Ground.sem n in
            assert (Expr.Term.Const.equal f pf);
            let substs = List.fold_left2 match_ty substs tyl ptyl in
            if Ground.Subst.S.is_empty substs then acc
            else
              let substs =
                IArray.fold2_exn args pargs ~init:substs ~f:(match_term d)
              in
              Ground.Subst.S.union substs acc)
          Ground.Subst.S.empty s
    | Node n' -> if Egraph.is_equal d n n' then substs else Ground.Subst.S.empty)

let match_ground d (substs : Ground.Subst.S.t) (n : Ground.t) (p : t) :
    Ground.Subst.S.t =
  if Ground.Subst.S.is_empty substs then substs
  else (
    Debug.dprintf4 debug_full "[Quant] matching %a %a" pp p Ground.pp n;
    match p with
    | Var v ->
        let match_ty (acc : Ground.Subst.S.t) ty : Ground.Subst.S.t =
          Debug.dprintf4 debug_full "[Quant] match_ty %a %a" Expr.Ty.pp v.ty
            Ground.Ty.pp ty;
          Ground.Subst.S.union (match_ty substs ty v.ty) acc
        in
        let ty = (Ground.sem n).Ground.ty in
        Debug.dprintf4 debug_full "[Quant] Var: %a as type: %a." Ground.pp n
          Ground.Ty.pp ty;
        let substs = match_ty Ground.Subst.S.empty ty in
        Ground.Subst.S.fold_left
          (fun acc subst ->
            match Expr.Term.Var.M.find v.var subst.term with
            | exception Not_found ->
                Ground.Subst.S.add
                  {
                    subst with
                    term = Expr.Term.Var.M.add v.var (Ground.node n) subst.term;
                  }
                  acc
            | n' ->
                if Egraph.is_equal d (Ground.node n) n' then
                  Ground.Subst.S.add subst acc
                else acc)
          Ground.Subst.S.empty substs
    | App (pf, ptyl, pargs) ->
        Ground.S.fold_left
          (fun acc n ->
            let { Ground.app = f; tyargs = tyl; args; _ } = Ground.sem n in
            assert (Expr.Term.Const.equal f pf);
            let substs = List.fold_left2 match_ty substs tyl ptyl in
            if Ground.Subst.S.is_empty substs then acc
            else
              let substs =
                IArray.fold2_exn args pargs ~init:substs ~f:(match_term d)
              in
              Ground.Subst.S.union substs acc)
          Ground.Subst.S.empty (Ground.S.singleton n)
    | Node n' ->
        if Egraph.is_equal d (Ground.node n) n' then substs
        else Ground.Subst.S.empty)

let rec check_ty subst (ty : Ground.Ty.t) (p : Expr.Ty.t) : bool =
  match p.ty_descr with
  | TyVar v -> (
      match Expr.Ty.Var.M.find v subst.Ground.Subst.ty with
      | exception Not_found -> false
      | ty' -> Ground.Ty.equal ty ty')
  | TyApp (cst', tyl') ->
      Expr.Ty.Const.equal ty.app cst'
      && List.for_all2 (check_ty subst) ty.args tyl'
  | _ -> assert false
(* absurd: should not be a trigger *)

let rec check_term d (subst : Ground.Subst.t) (n : Node.t) (p : t) : bool =
  Debug.dprintf4 debug "[Quant] check %a %a" pp p Node.pp n;
  match p with
  | Var v -> (
      Ground.Ty.S.exists (fun ty -> check_ty subst ty v.ty) (Info.tys d n)
      &&
      match Expr.Term.Var.M.find v.var subst.term with
      | exception Not_found -> false
      | n' -> Egraph.is_equal d n n')
  | App (pf, ptyl, pargs) ->
      let s =
        Opt.get_def Ground.S.empty
        @@
        let open CCOption in
        let* info = Egraph.get_dom d Info.dom (Egraph.find_def d n) in
        F.M.find_opt pf info.apps
      in
      Ground.S.exists
        (fun n ->
          let { Ground.app = f; tyargs = tyl; args; _ } = Ground.sem n in
          assert (Expr.Term.Const.equal f pf);
          List.for_all2 (check_ty subst) tyl ptyl
          && IArray.for_all2_exn ~f:(check_term d subst) args pargs)
        s
  | Node n' -> Egraph.is_equal d n n'

let _ = check_term

exception IncompleteSubstitution

let rec check_term_exists_exn d (subst : Ground.Subst.t) (p : t) : Node.S.t =
  match p with
  | Var v -> (
      match Expr.Term.Var.M.find v.var subst.term with
      | exception Not_found -> raise IncompleteSubstitution
      | n ->
          if
            (Ground.Ty.S.exists (fun ty -> check_ty subst ty v.ty))
              (Info.tys d n)
          then Node.S.singleton (Egraph.find_def d n)
          else raise Not_found)
  | App (pf, ptyl, pargs) ->
      let nodes_args = IArray.map ~f:(check_term_exists_exn d subst) pargs in
      let nodes = find_existing_app d subst pf ptyl nodes_args in
      let nodes =
        Node.S.of_list (List.map Ground.node (Ground.S.elements nodes))
      in
      if Node.S.is_empty nodes then raise Not_found else nodes
  | Node n -> Node.S.singleton n

and find_existing_app d subst pf ptyl nodes_args =
  if IArray.is_empty nodes_args then
    let g =
      Ground.apply d pf
        (List.map (Ground.Ty.convert subst.ty) ptyl)
        IArray.empty
      |> Ground.index
    in
    if Egraph.is_registered d (Ground.node g) then Ground.S.singleton g
    else Ground.S.empty
  else
    let find_app pos n =
      Opt.get_def Ground.S.empty
      @@
      let open CCOption in
      let app = { F_Pos.f = pf; pos } in
      let* info = Egraph.get_dom d Info.dom n in
      let+ parents = F_Pos.M.find_opt app info.parents in
      (* Debug.dprintf6 debug "find_app %a: %a -- %a" F_Pos.pp app Node.pp n
       *   Ground.S.pp parents; *)
      parents
    in
    let get_parents pos nodes =
      let nodes =
        Node.S.fold_left
          (fun acc n ->
            Ground.S.fold_left
              (fun acc g ->
                if List.for_all2 (check_ty subst) (Ground.sem g).tyargs ptyl
                then Ground.S.add g acc
                else acc)
              acc (find_app pos n))
          Ground.S.empty nodes
      in
      nodes
    in
    (* let inter s1 s2 =
     *   (\* intersection modulo equality which keeps original node *\)
     *   Debug.dprintf4 debug "inter: %a -- %a" Node.S.pp s1 Node.S.pp s2;
     *   let cmp_with_repr n1 n2 =
     *     Node.compare (Egraph.find d n1) (Egraph.find d n2)
     *   in
     *   let s1 = Node.S.elements s1 |> List.sort_uniq cmp_with_repr in
     *   let s2 = Node.S.elements s2 |> List.sort_uniq cmp_with_repr in
     *   let rec inter l1 l2 acc =
     *     match (l1, l2) with
     *     | [], _ | _, [] -> acc
     *     | a :: l1', b :: l2' ->
     *         let c = cmp_with_repr a b in
     *         if c = 0 then inter l1' l2' (a :: acc)
     *         else if c < 0 then inter l1' l2 acc
     *         else inter l1 l2' acc
     *   in
     *   Node.S.of_list (inter s1 s2 [])
     * in *)
    let grounds =
      IArray.foldi_non_empty_exn
        ~f:(fun i acc nodes_arg ->
          (* It is important to just use [Node.S.inter] because we want to do
             [Ground.S.inter] so we should not take the representative *)
          Ground.S.inter acc (get_parents i nodes_arg))
        ~init:(get_parents 0) nodes_args
    in
    grounds

let check_term_exists d (subst : Ground.Subst.t) (p : t) : Node.S.t =
  try check_term_exists_exn d subst p with Not_found -> Node.S.empty

let get_pps pattern patterns =
  let rec aux acc fp p =
    match p with
    | Var v ->
        Expr.Term.Var.M.add_change F_Pos.S.singleton F_Pos.S.add v.var fp acc
    | App (f, _, tl) ->
        IArray.foldi
          ~f:(fun pos acc p -> aux acc F_Pos.{ f; pos } p)
          ~init:acc tl
    | Node _ -> acc
  in
  let compute_position =
    List.fold_left
      (fun acc pattern ->
        match pattern with
        | Var _ | Node _ -> acc
        | App (f, _, tl) ->
            IArray.foldi
              ~f:(fun pos acc p -> aux acc F_Pos.{ f; pos } p)
              ~init:acc tl)
      Expr.Term.Var.M.empty
  in
  let m = compute_position [ pattern ] in
  let ms = compute_position patterns in
  Expr.Term.Var.M.mapi
    (fun v s ->
      let l = F_Pos.S.elements s in
      let ls =
        Expr.Term.Var.M.find_def F_Pos.S.empty v ms
        |> F_Pos.S.union s |> F_Pos.S.elements
      in
      Lists.fold_product
        (fun acc parents1 parents2 ->
          if F_Pos.equal parents1 parents2 then acc
          else PP.create parents1 parents2 :: acc)
        [] l ls)
    m

let env_ground_by_apps =
  F.EnvApps.create (Context.Push.pp ~sep:Fmt.semi Ground.pp)
    "Quantifier.Trigger.ground_by_apps" (fun c _ -> Context.Push.create c)

module EnvTy = Datastructure.Memo (Ground.Ty)

let env_node_by_ty =
  EnvTy.create (Context.Push.pp ~sep:Fmt.semi Node.pp)
    "Quantifier.Trigger.node_by_ty" (fun c _ -> Context.Push.create c)

let match_any_term d substs (p : t) : Ground.Subst.S.t =
  Debug.dprintf2 debug "[Quant] matching any for %a" pp p;
  if Ground.Subst.S.is_empty substs then substs
  else
    match p with
    | Var v ->
        EnvTy.fold
          (fun ty q acc ->
            let substs = match_ty substs ty v.ty in
            if Ground.Subst.S.is_empty substs then acc
            else
              Ground.Subst.S.fold_left
                (fun acc subst ->
                  Context.Push.fold
                    (fun acc n ->
                      match Expr.Term.Var.M.find_opt v.var subst.term with
                      | None ->
                          Ground.Subst.S.add
                            {
                              subst with
                              term = Expr.Term.Var.M.add v.var n subst.term;
                            }
                            acc
                      | Some n' ->
                          if Egraph.is_equal d n n' then
                            Ground.Subst.S.add subst acc
                          else acc)
                    acc q)
                acc substs)
          env_node_by_ty d Ground.Subst.S.empty
    | App (pf, ptyl, pargs) ->
        Context.Push.fold
          (fun acc n ->
            let { Ground.app = f; tyargs = tyl; args; _ } = Ground.sem n in
            assert (Expr.Term.Const.equal f pf);
            let substs = List.fold_left2 match_ty substs tyl ptyl in
            let substs =
              IArray.fold2_exn args pargs ~init:substs ~f:(match_term d)
            in
            Ground.Subst.S.union substs acc)
          Ground.Subst.S.empty
          (F.EnvApps.find env_ground_by_apps d pf)
    | Node n ->
        if Egraph.is_registered d n then substs else Ground.Subst.S.empty
