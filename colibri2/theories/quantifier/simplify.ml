(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

(** Simplifications applied on quantifiers *)

open Base
open Colibri2_core

type simplified =
  | NoSimplification
  | ClosedQuantifier of Ground.ClosedQuantifier.t
  | ClosedQuantifierKeepOriginal of Ground.ClosedQuantifier.t
  | Ground of Node.t

let remove_unused_variables d (q : Ground.ClosedQuantifier.t) =
  let q = Ground.ClosedQuantifier.sem q in
  let tyvs, tvs =
    Expr.Term.free_vars (Expr.Ty.Var.S.empty, Expr.Term.Var.S.empty) q.body
  in
  let tyvs = Expr.Ty.Var.M.set_diff tyvs q.subst.ty in
  let tvs = Expr.Term.Var.M.set_diff tvs q.subst.term in
  if
    List.for_all q.ty_vars ~f:(fun tyv -> Expr.Ty.Var.S.mem tyv tyvs)
    && List.for_all q.term_vars ~f:(fun tv -> Expr.Term.Var.S.mem tv tvs)
  then NoSimplification
  else
    let q' =
      {
        Ground.ClosedQuantifier.body = q.body;
        ty_vars =
          List.filter q.ty_vars ~f:(fun tyv -> Expr.Ty.Var.S.mem tyv tyvs);
        term_vars =
          List.filter q.term_vars ~f:(fun tv -> Expr.Term.Var.S.mem tv tvs);
        binder = q.binder;
        subst = q.subst;
      }
    in
    if List.is_empty q'.ty_vars && List.is_empty q'.term_vars then
      Ground (Ground.convert ~subst:q'.subst d q'.body)
    else ClosedQuantifier (Ground.ClosedQuantifier.index q')

let regroup_forall_with_implies _ (q : Ground.ClosedQuantifier.t) =
  let q = Ground.ClosedQuantifier.sem q in
  let rec forall_below_implies (t : Expr.Term.t) =
    match t.term_descr with
    | Expr.App ({ term_descr = Cst { builtin = Expr.Imply; _ }; _ }, _, [ h; c ])
      -> (
        match forall_below_implies c with
        | None -> None
        | Some (vars, c) -> Some (vars, Expr.Term.imply h c))
    | Expr.Binder (Forall ([], vars), b) -> (
        match forall_below_implies b with
        | None -> Some (vars, b)
        | Some (vars', b) -> Some (vars @ vars', b))
    | _ -> None
  in
  match forall_below_implies q.body with
  | None -> NoSimplification
  | Some (vars, body) ->
      ClosedQuantifierKeepOriginal
        (Ground.ClosedQuantifier.index
           {
             Ground.ClosedQuantifier.body;
             ty_vars = q.ty_vars;
             term_vars = q.term_vars @ vars;
             binder = q.binder;
             subst = q.subst;
           })

let option_no_remove_unused_variables =
  Options.register_flag "no-quant-remove-unused-variables"

let option_no_regroup_forall_with_implies =
  Options.register_flag "no-quant-regroup-forall-with-implies"

let simplify d q =
  let rec apply = function
    | [] -> NoSimplification
    | (opt, f) :: l -> (
        if Options.get d opt then apply l
        else match f d q with NoSimplification -> apply l | x -> x)
  in
  apply
    [
      (option_no_remove_unused_variables, remove_unused_variables);
      (option_no_regroup_forall_with_implies, regroup_forall_with_implies);
    ]
