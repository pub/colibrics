(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

let debug =
  Debug.register_info_flag ~desc:"Handling of quantifiers" "quantifiers"

let debug_instantiation =
  Debug.register_info_flag ~desc:"Handling of quantifiers"
    "quantifiers.debug_instantiations"

let debug_full =
  Debug.register_flag ~desc:"Handling of quantifiers full" "quantifiers.full"

let nb_instantiation = Debug.register_stats_int "Quantifier.instantiation"

let nb_eager_instantiation =
  Debug.register_stats_int "Quantifier.eager_instantiation"

let nb_delayed_instantiation =
  Debug.register_stats_int "Quantifier.delayed_instantiation"

let nb_new_instantiation =
  Debug.register_stats_int "Quantifier.new_instantiation"
