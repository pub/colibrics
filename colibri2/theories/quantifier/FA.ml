(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

(** instantiated function symbol *)
module T = struct
  let hash_fold_list = Base.hash_fold_list

  type t = { tc : F.t; ta : Expr.Ty.t list } [@@deriving eq, ord, hash]

  let pp fmt { tc; ta } =
    Format.fprintf fmt "%a[%a]" F.pp tc Fmt.(list ~sep:comma Expr.Ty.pp) ta
end

include T
include Colibri2_popop_lib.Popop_stdlib.MkDatatype (T)
