(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

(** Cf Efficient E-Matching paper *)

open Colibri2_popop_lib
open Colibri2_core
open Common

module Quantifier_skipped = struct
  let q = Datastructure.Push.create Fmt.nop "quantifier_skipped"
  let register d f = Datastructure.Push.push q d f
  let skipped d builtin = Datastructure.Push.exists q d ~f:(fun p -> p builtin)

  type filter_term =
    | App of Expr.builtin * filter_ty list * filter_term list
    | Var of int
    | Choice of filter_term list
    | Any

  and filter_ty = TyApp of Expr.builtin * filter_ty list | TyAny
  and filter_quant = { vars : int list; filter : filter_term }

  let rec match_term env (filter : filter_term) (t : Expr.Term.t) =
    match (filter, t) with
    | ( App (fb, ftyl, ftl),
        {
          term_descr = App ({ term_descr = Cst { builtin = b; _ }; _ }, tyl, tl);
          _;
        } ) ->
        Base.Poly.equal fb b
        && List.for_all2 match_ty ftyl tyl
        && List.for_all2 (match_term env) ftl tl
    | Var v, { term_descr = Var v'; _ } ->
        Expr.Term.Var.equal (Popop_stdlib.DInt.M.find v env) v'
    | Choice filters, t -> List.exists (fun f -> match_term env f t) filters
    | Any, _ -> true
    | _ -> false

  and match_ty (filter : filter_ty) (ty : Expr.Ty.t) =
    match (filter, ty) with
    | TyApp (fb, ftyl), { ty_descr = TyApp ({ builtin = b; _ }, tyl); _ } ->
        Base.Poly.equal fb b && List.for_all2 match_ty ftyl tyl
    | TyAny, _ -> true
    | _ -> false

  and match_ (quant : Ground.ClosedQuantifier.s) filter =
    Base.List.is_empty quant.ty_vars
    &&
    match
      Base.List.fold2 ~init:Popop_stdlib.DInt.M.empty filter.vars
        quant.term_vars ~f:(fun acc i v ->
          Popop_stdlib.DInt.M.add_new Impossible i v acc)
    with
    | Unequal_lengths -> false
    | Ok env -> match_term env filter.filter quant.body
end

module Find_new_event = struct
  let two d n (info : Info.t) n' (info' : Info.t) acc0 =
    Debug.dprintf8 debug_full "Look for new event %a %a %a %a" Node.pp n Info.pp
      info Node.pp n' Info.pp info';
    let symmetric f acc na a nb b = f (f acc na a nb b) nb b na a in
    let do_pp acc _ info1 _ info2 =
      let ips = Datastructure.Ref.get InvertedPath.MPP.ips d in
      let fold _ _ ip acc =
        Debug.dprintf2 debug_full "[Quant] PP2 found for %a" Node.pp n;
        ip :: acc
      in
      F_Pos.M.fold2_inter
        (fun _ _ m acc -> F_Pos.M.fold2_inter fold info2.Info.parents m acc)
        info1.Info.parents ips acc
    in
    let do_pc acc _ info1 _ info2 =
      let ips = Datastructure.Ref.get InvertedPath.MPC.ips d in
      let fold _ _ ip acc =
        Debug.dprintf2 debug_full "[Quant] PC2 found for %a" Node.pp n;
        ip :: acc
      in
      F_Pos.M.fold2_inter
        (fun _ _ m acc -> F.M.fold2_inter fold info2.Info.apps m acc)
        info1.Info.parents ips acc
    in
    (* parent node *)
    let do_pn acc n info1 n' _ =
      let ips = Datastructure.Ref.get InvertedPath.MPN.ips d in
      let fold ip acc =
        Debug.dprintf4 debug_full "[Quant] PN2 found for %a-%a" Node.pp n
          Node.pp n';
        ip :: acc
      in
      F_Pos.M.fold2_inter
        (fun _ _ m acc ->
          match Node.M.find_opt n' m with None -> acc | Some ip -> fold ip acc)
        info1.Info.parents ips acc
    in

    (* parent type *)
    let do_pt acc n info1 _ info2 =
      let tys_info2_info1 = Ground.Ty.S.diff info2.Info.ty info1.Info.ty in
      let ips = Datastructure.Ref.get InvertedPath.MPT.ips d in
      let fold vty ip acc =
        let no_match ty : bool =
          Ground.Subst.S.is_empty (Pattern.match_ty Pattern.init ty vty)
        in
        if Ground.Ty.S.for_all no_match tys_info2_info1 then acc
        else (
          Debug.dprintf2 debug_full "[Quant] PT2 found for %a" Node.pp n;
          ip :: acc)
      in
      if Ground.Ty.S.is_empty tys_info2_info1 then acc
      else
        F_Pos.M.fold2_inter
          (fun _ _ m acc -> Expr.Ty.M.fold fold m acc)
          info1.Info.parents ips acc
    in

    let acc = symmetric do_pp acc0 n info n' info' in
    let acc = symmetric do_pc acc n info n' info' in
    let acc = do_pt acc n info n' info' in
    let acc = do_pt acc n' info' n info in
    let acc =
      if Node.equal n n' then do_pn acc n info n' info'
      else symmetric do_pn acc n info n' info'
    in
    if not (Base.phys_equal acc0 acc) then
      Debug.dprintf8 debug "New event found for %a %a %a %a" Node.pp n Info.pp
        info Node.pp n' Info.pp info';
    acc

  let one d n (info : Info.t) acc =
    Debug.dprintf4 debug "Find_new_event %a %a" Node.pp n Info.pp info;
    let do_pp acc _ info1 _ info2 =
      let ips = Datastructure.Ref.get InvertedPath.MPP.ips d in
      let fold _ _ ip acc =
        Debug.dprintf2 debug_full "[Quant] PP1 found for %a" Node.pp n;
        ip :: acc
      in
      F_Pos.M.fold2_inter
        (fun _ _ m acc -> F_Pos.M.fold2_inter fold info2.Info.parents m acc)
        info1.Info.parents ips acc
    in

    let do_pc acc _ info1 _ info2 =
      let ips = Datastructure.Ref.get InvertedPath.MPC.ips d in
      let fold _ _ ip acc =
        Debug.dprintf2 debug_full "[Quant] PC1 found for %a" Node.pp n;
        ip :: acc
      in
      F_Pos.M.fold2_inter
        (fun _ _ m acc -> F.M.fold2_inter fold info2.Info.apps m acc)
        info1.Info.parents ips acc
    in
    (* parent node *)
    let do_pn acc n info1 n' _ =
      let ips = Datastructure.Ref.get InvertedPath.MPN.ips d in
      let fold ip acc =
        Debug.dprintf4 debug_full "[Quant] PN1 found for %a-%a" Node.pp n
          Node.pp n';
        ip :: acc
      in
      F_Pos.M.fold2_inter
        (fun _ _ m acc ->
          match Node.M.find_opt n' m with None -> acc | Some ip -> fold ip acc)
        info1.Info.parents ips acc
    in

    (* parent type *)
    let do_pt acc n info1 _ info2 =
      let tys_info2_info1 = Ground.Ty.S.diff info2.Info.ty info1.Info.ty in
      let ips = Datastructure.Ref.get InvertedPath.MPT.ips d in
      let fold vty ip acc =
        let no_match ty : bool =
          Ground.Subst.S.is_empty (Pattern.match_ty Pattern.init ty vty)
        in
        if Ground.Ty.S.for_all no_match tys_info2_info1 then acc
        else (
          Debug.dprintf2 debug_full "[Quant] PT1 found for %a" Node.pp n;
          ip :: acc)
      in
      if Ground.Ty.S.is_empty tys_info2_info1 then acc
      else
        F_Pos.M.fold2_inter
          (fun _ _ m acc -> Expr.Ty.M.fold fold m acc)
          info1.Info.parents ips acc
    in
    let acc = do_pp acc n info n info in
    let acc = do_pc acc n info n info in
    let acc = do_pt acc n info n info in
    let acc = do_pn acc n info n info in
    acc
end

module Delayed_find_new_event : sig
  val enqueue : _ Egraph.t -> Node.t -> InvertedPath.t list -> unit
end = struct
  module Delayed_find_new_event = struct
    let key =
      Events.Dem.create
        (module struct
          type t = unit

          let name = "Quantifier.new_event"
        end)

    let delay = Events.Delayed_by 15

    type runable = unit

    let print_runable = Unit.pp

    let scheduled =
      Datastructure.Ref.create Fmt.nop "Quant.delayed_find_new_event"
        Node.M.empty

    let stats_time =
      Debug.register_stats_time "Quantifier.time_delayed_find_new_event"

    let run d () =
      let run _ =
        let m = Datastructure.Ref.get scheduled d in
        Datastructure.Ref.set scheduled d Node.M.empty;
        let triggers, callbacks =
          Trace.with_span ~__FILE__ ~__FUNCTION__ ~__LINE__ "InvertedPath"
          @@ fun _ ->
          Node.M.fold_left
            (fun acc n ips ->
              InvertedPath.S.fold_left
                (fun acc ip -> InvertedPath.exec d acc n ip)
                acc ips)
            (Trigger.M.empty, Callback.M.empty)
            m
        in
        Trace.with_span ~__FILE__ ~__FUNCTION__ ~__LINE__ "Instantiate triggers"
        @@ fun _ ->
        Trigger.M.iter
          (fun tri substs -> Trigger.instantiate_many d tri substs)
          triggers;
        Trace.with_span ~__FILE__ ~__FUNCTION__ ~__LINE__ "Run callbacks"
        @@ fun _ ->
        Callback.M.iter
          (fun callback substs -> Callback.run d callback substs)
          callbacks
      in
      Debug.add_time_during stats_time run
  end

  let () = Events.register (module Delayed_find_new_event)

  let enqueue d n l =
    if not (Base.List.is_empty l) then (
      let m = Datastructure.Ref.get Delayed_find_new_event.scheduled d in
      if Node.M.is_empty m then
        Events.new_pending_daemon d Delayed_find_new_event.key ();
      let m =
        Node.M.add_change InvertedPath.S.of_list
          (fun l v -> List.fold_right InvertedPath.S.add l v)
          n l m
      in
      Datastructure.Ref.set Delayed_find_new_event.scheduled d m)
end

let () =
  Dom.register
    (module struct
      type t = Info.t [@@deriving show]

      let key = Info.dom

      let merged (b1 : t option) (b2 : t option) =
        match (b1, b2) with
        | Some b1, Some b2 -> CCEqual.physical b1 b2
        (* We always want to go once through merge *)
        | None, None -> true
        | _ -> false

      let stats_time = Debug.register_stats_time "Quantifier.merge"

      let merge d (dom0, cl0) (dom1, cl1) inv =
        let repr, other = if inv then (cl0, cl1) else (cl1, cl0) in
        let repr = Egraph.find d repr in
        let other = Egraph.find d other in
        match (dom0, dom1) with
        | None, None -> ()
        | Some info0, None -> Egraph.set_dom d Info.dom cl1 info0
        | None, Some info1 -> Egraph.set_dom d Info.dom cl0 info1
        | Some info0, Some info1 ->
            let info = Info.merge d ~other ~repr info0 info1 in
            Egraph.set_dom d Info.dom cl0 info;
            Egraph.set_dom d Info.dom cl1 info;
            Debug.add_time_during stats_time (fun _ ->
                let ips = Find_new_event.two d cl0 info0 cl1 info1 [] in
                Delayed_find_new_event.enqueue d repr ips)

      let wakeup_threshold = None
    end)

module SkolemVariable = struct
  module T = struct
    type t = { e : Ground.ClosedQuantifier.t; v : Expr.Term.Var.t }
    [@@deriving eq, hash, ord, show]
  end

  include T
  include Popop_stdlib.MkDatatype (T)

  let ty (v : t) =
    Ground.Ty.convert (Ground.ClosedQuantifier.sem v.e).subst.ty v.v.Expr.id_ty

  let name = "SkolemVar"
end

module ThSkolemVariable = ThTerm.Register (SkolemVariable)

let h_skolem = ThSkolemVariable.H.create 10

let skolemize d (e' : Ground.ClosedQuantifier.t) =
  let e = Ground.ClosedQuantifier.sem e' in
  if not (Base.List.is_empty e.ty_vars) then
    invalid_arg "False type quantification";
  let subst_term =
    List.fold_left
      (fun acc v ->
        let v' = { SkolemVariable.e = e'; v } in
        let v'' = ThSkolemVariable.index v' in
        let n =
          ThSkolemVariable.H.memo
            (fun v ->
              let v = ThSkolemVariable.sem v in
              let id = Expr.Term.Const.mk v.v.Expr.path v.v.id_ty in
              let t = Expr.Term.apply_cst id [] [] in
              let e = Ground.ClosedQuantifier.sem v.e in
              let n = Ground.convert ~subst:e.subst d t in
              n)
            h_skolem v''
        in
        Expr.Term.Var.M.add v n acc)
      e.subst.term e.term_vars
  in
  let subst = { Ground.Subst.ty = e.subst.ty; term = subst_term } in
  let n = Ground.convert ~subst d e.body in
  Egraph.register d n;
  n

let all_forall_skipped =
  let doc =
    "Handle existential quantification but skip universal quantification"
  in
  Options.register "quantifier-skip-forall" ~pp:Fmt.bool
    Cmdliner.Arg.(value & flag & info ~doc [ "quantifier-skip-forall" ])

let add_def_from_axiom =
  let doc = "Handle axioms that look like definitions as definitions" in
  Options.register "quantifier-add-def-from-axiom" ~pp:Fmt.bool
    Cmdliner.Arg.(value & flag & info ~doc [ "quantifier-add-def-from-axiom" ])

let always_add_skolem =
  let doc =
    "Add quantifier skolemization even if the thruth value is not known"
  in
  Options.register_flag "always-add-skolem" ~doc

let attach d th =
  let skolemize d th =
    Debug.dprintf2 debug "[Quant] Skolemize %a" Ground.ClosedQuantifier.pp th;
    Egraph.merge d (skolemize d th) (Ground.ClosedQuantifier.node th)
  in
  if Options.get d always_add_skolem then skolemize d th;
  DaemonOnlyPropa.attach_value d (Ground.ClosedQuantifier.node th)
    Boolean.BoolValue.key (fun d _ bv ->
      match (Ground.ClosedQuantifier.sem th, Boolean.BoolValue.value bv) with
      | { binder = Exists; _ }, true | { binder = Forall; _ }, false ->
          skolemize d th
      | ({ binder = Exists; _ } as e), false
      | ({ binder = Forall; _ } as e), true
        when Quantifier_skipped.skipped d e ->
          Debug.dprintf2 debug "[Quant] Skip %a" Ground.ClosedQuantifier.pp th
      | { binder = Exists; _ }, false | { binder = Forall; _ }, true ->
          if
            not
              (Options.get d add_def_from_axiom
              && Definitions.check_if_it_is_a_definition d th)
          then
            if not (Options.get d all_forall_skipped) then (
              let triggers, delayed_triggers =
                match Trigger.get_user_triggers d th with
                | [] ->
                    ( Trigger.compute_top_triggers d th,
                      Trigger.compute_all_triggers d th )
                | triggers -> (triggers, [])
              in
              Debug.dprintf5 debug
                "[Quant] For %a adds %a and %i delayed triggers"
                Ground.ClosedQuantifier.pp th
                Fmt.(
                  list ~sep:semi (Fmt.using (fun t -> t.Trigger.pat) Pattern.pp))
                triggers
                (List.length delayed_triggers);
              List.iter (InvertedPath.add_trigger d) triggers;
              List.iter
                (fun tr ->
                  if false then
                    DaemonLastEffortLateUncontextual.schedule_immediately d
                      (fun d ->
                        DaemonOnlyPropa.attach_value d
                          (Ground.ClosedQuantifier.node th)
                          Boolean.BoolValue.key (fun d _ bv' ->
                            if Boolean.BoolValue.equal bv bv' then (
                              Debug.dprintf4 debug
                                "[Quant] For %a adds delayed %a"
                                Ground.ClosedQuantifier.pp th
                                (Fmt.using (fun t -> t.Trigger.pat) Pattern.pp)
                                tr;
                              InvertedPath.add_trigger d tr)))
                  else
                    DaemonLastEffortLate.schedule_immediately d (fun d ->
                        Debug.dprintf4 debug "[Quant] For %a adds delayed %a"
                          Ground.ClosedQuantifier.pp th
                          (Fmt.using (fun t -> t.Trigger.pat) Pattern.pp)
                          tr;
                        InvertedPath.add_trigger d tr))
                delayed_triggers))

let quantifier_registered d th =
  let n = Ground.ClosedQuantifier.node th in
  let s = Ground.ClosedQuantifier.sem th in
  Expr.Term.Var.M.iter (fun _ n -> Egraph.register d n) s.subst.term;
  let handle () =
    if Boolean.dec_at_literal d && Boolean.is_unknown d n then
      Choice.register_thterm d
        (Ground.ClosedQuantifier.thterm th)
        (Boolean.chobool n);
    attach d th
  in
  (* Simplify quantifier *)
  let simplified n' =
    Egraph.register d n';
    Egraph.merge d n n'
  in
  if Base.List.is_empty (Trigger.get_user_triggers d th) then
    match Simplify.simplify d th with
    | NoSimplification -> handle ()
    | Simplify.ClosedQuantifierKeepOriginal th' ->
        simplified (Ground.ClosedQuantifier.node th');
        handle ()
    | Simplify.ClosedQuantifier th' ->
        simplified (Ground.ClosedQuantifier.node th')
    | Simplify.Ground n' -> simplified n'
  else handle ()

let true_f = Debug.register_stats_int "true_f"
let false_f = Debug.register_stats_int "false_f"

let option_match_and_cc_on_bool_operators =
  Options.register ~pp:Fmt.bool "Quantifier.cc_for_boolean_operators"
    Cmdliner.Arg.(
      value & flag
      & info
          ~doc:
            "Match and apply congruence closure on : and, or, xor, imply and \
             not"
          [ "quantifier-and-cc-on-boolean-operators" ])

let match_and_cc_on_bool_operators d thg =
  Options.get d option_match_and_cc_on_bool_operators
  ||
  match (Ground.sem thg).app.builtin with
  | Expr.And | Expr.Or | Expr.Xor | Expr.Imply | Expr.Neg ->
      (* The check for those case is too costly *)
      false
  | _ -> true

(** Verify that this term is congruent closure to another already existing term
    So it is useless. f(a) is useless if a == b and f(a) == f(b) and f(b) is
    already known. *)
let application_useless d thg =
  let g = Ground.sem thg in
  let n = Ground.node thg in
  let apps =
    Opt.get_def Ground.S.empty
    @@
    let open CCOption in
    let* info = Egraph.get_dom d Info.dom (Egraph.find_def d n) in
    F.M.find_opt g.app info.apps
  in
  let r =
    Ground.S.exists
      (fun g' ->
        let b =
          List.for_all2 Ground.Ty.equal (Ground.sem g').tyargs g.tyargs
          && IArray.for_all2_exn ~f:(Egraph.is_equal d) (Ground.sem g').args
               g.args
        in
        if b then Egraph.merge d n (Ground.node g');
        b)
      apps
  in
  if r then Debug.incr true_f else Debug.incr false_f;
  r

let add_info_on_ground_terms d thg =
  Debug.dprintf2 debug_full "[Quant] add_info_on_ground_terms %a" Ground.pp thg;
  let res = Ground.node thg in
  let g = Ground.sem thg in
  let merge_info other info =
    let repr = Egraph.find_def d other in
    match Egraph.get_dom d Info.dom repr with
    | None ->
        Egraph.set_dom d Info.dom repr info;
        Delayed_find_new_event.enqueue d repr
          (Find_new_event.one d repr info [])
    | Some info' ->
        Egraph.set_dom d Info.dom repr (Info.merge d ~repr ~other info info');
        Find_new_event.one d repr info []
        |> Find_new_event.two d repr info repr info'
        |> Delayed_find_new_event.enqueue d repr
  in
  let sin_thg = Ground.S.singleton thg in
  let add_pc pos n =
    merge_info n
      { Info.empty with parents = F_Pos.M.singleton { f = g.app; pos } sin_thg }
  in
  IArray.iteri ~f:add_pc g.args;
  merge_info res
    {
      Info.empty with
      apps = F.M.singleton g.app sin_thg;
      ty = Ground.Ty.S.singleton g.ty;
    };
  Context.Push.push (F.EnvApps.find Pattern.env_ground_by_apps d g.app) thg;
  Context.Push.push (Pattern.EnvTy.find Pattern.env_node_by_ty d g.ty) res;
  (* Try pattern from the start *)
  let trgs = F.EnvApps.find Trigger.env_tri_by_apps d g.app in
  Context.Push.iter
    (function
      | Trigger.Trigger trg -> Trigger.match_ d trg thg
      | Trigger.Callback call -> Callback.match_ d call thg)
    trgs

let stats_time = Debug.register_stats_time "Quantifier.new_ground"

let stats_app_useless =
  Debug.register_stats_time "Quantifier.application_useless"

let add_ground_info sp g =
  Trace.add_data_to_span sp
    [ ("ground", `String (Fmt.str "%a" Ground.Term.pp (Ground.sem g))) ]

let th_register d =
  (* let delay = Events.Delayed_by 10 in *)
  DaemonOnlyPropa.attach_reg_sem d Ground.ClosedQuantifier.key
    quantifier_registered;
  Ground.register_converter d (fun d g ->
      if match_and_cc_on_bool_operators d g then
        if
          Debug.add_time_during stats_app_useless (fun sp ->
              if Trace.enabled () then add_ground_info sp g;
              application_useless d g)
        then Debug.dprintf2 debug "[Info] %a is redundant" Ground.pp g
        else
          Debug.add_time_during stats_time (fun sp ->
              if Trace.enabled () then add_ground_info sp g;
              add_info_on_ground_terms d g));
  Interp.Register.check_closed_quantifier d (fun d g ->
      let n = Ground.ClosedQuantifier.node g in
      let unknown =
        match (Ground.ClosedQuantifier.sem g).binder with
        | Forall -> Boolean.is_true d n
        | Exists -> Boolean.is_false d n
      in
      if unknown then Unknown else Right);
  Definitions.th_register d

let () = Init.add_default_theory th_register
