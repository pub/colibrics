(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

include Colibri2_popop_lib.Popop_stdlib.Datatype

val pattern : t -> Pattern.t
val mk : Egraph.wt -> (Egraph.wt -> Ground.Subst.t -> unit) -> Pattern.t -> t
val run : Egraph.wt -> t -> Ground.Subst.S.t -> unit

val match_ : Egraph.wt -> t -> Ground.t -> unit
(** [match_ d t g] match the callback [t] with [g] and run [t] with the
    resulting substitutions *)
