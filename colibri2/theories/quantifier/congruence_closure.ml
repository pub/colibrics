(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

(*
open Colibri2_popop_lib

module type With_Pos = sig
  type app

  type t = { f : app; pos : int }

  include Popop_stdlib.Datatype with type t := t
end

module With_Pos (App : Popop_stdlib.Datatype) :
  With_Pos with type app := App.t = struct
  module T = struct
    let hash_fold_int = Base.hash_fold_int

    type app = App.t

    type t = { f : App.t; pos : int } [@@deriving eq, ord, hash]

    let pp fmt { f; pos } = Format.fprintf fmt "%a.%i" App.pp f pos
  end

  include T
  include Popop_stdlib.MkDatatype (T)
end

module NodeArray = struct
  module T = struct
    type t = Node.t IArray.t [@@deriving eq, ord, hash, show]
  end

  include T
  include Popop_stdlib.MkDatatype (T)
end

module Make
    (App : Popop_stdlib.Datatype)
    (App_Pos : With_Pos with app = App.t) (Application : sig
      include Popop_stdlib.Datatype

      val to_nodearray : f:(Node.t -> Node.t) -> t -> App.t * NodeArray.t

      type app
    end) : sig
  type info = Application.S.t With_Pos.M.t

  val merge : Egraph.rw -> other:Node.t -> repr:Node.t -> info -> info -> info
end = struct
  type info = Application.S.t With_Pos.M.t

  let congruence_closure d ~other parent0 ~repr parent1 =
    let find_as_if cl =
      (* as if the merge is already done *)
      let cl = Egraph.find_def d cl in
      if Node.equal cl other then repr else cl
    in
    With_Pos.M.union
      (fun _ g1 g2 ->
        let h = Ground.Term.H.create 10 in
        let norm g =
          let g = Ground.sem g in
          { g with Ground.args = IArray.map ~f:find_as_if g.Ground.args }
        in
        let s = ref (Ground.S.union g1 g2) in
        let not_first g = s := Ground.S.remove g !s in
        Ground.S.iter
          (fun g ->
            Ground.Term.H.change
              (function
                | None -> Some [ Ground.node g ]
                | Some l ->
                    not_first g;
                    assert (1 = 2);
                    Some (Ground.node g :: l))
              h (norm g))
          g1;
        Ground.S.iter
          (fun g ->
            match Ground.Term.H.find_opt h (norm g) with
            | None -> ()
            | Some l ->
                not_first g;
                List.iter (Egraph.merge d (Ground.node g)) l)
          g2;
        assert (not (Ground.S.is_empty !s));
        Some !s)
      parent0 parent1

  let merge d ~other ~repr info info' =
    congruence_closure d ~other ~repr info info'
end
*)
