(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

module TTerm = Context.Trie (Node)
(** Only for substitutions that have the same domain *)

module TTy = Context.Trie (Ground.Ty)

type 'a t = 'a TTy.t TTerm.t

let create c = TTerm.create c

let fold_of_term_map s : _ Context.fold =
  {
    fold =
      (fun f acc -> Expr.Term.Var.M.fold_left (fun acc _ v -> f acc v) acc s);
  }

let fold_of_ty_map s : _ Context.fold =
  {
    fold = (fun f acc -> Expr.Ty.Var.M.fold_left (fun acc _ v -> f acc v) acc s);
  }

let find_and_add (t : bool t) (s : Ground.Subst.t) =
  let tv =
    TTerm.Fold.memo ~default:(fun c -> TTy.create c) t (fold_of_term_map s.term)
  in
  let not_set = ref false in
  ignore
    (TTy.Fold.memo
       ~default:(fun _ ->
         not_set := true;
         true)
       tv (fold_of_ty_map s.ty));
  not !not_set

let find_def ~default (t : _ t) (s : Ground.Subst.t) =
  let tv =
    TTerm.Fold.memo ~default:(fun c -> TTy.create c) t (fold_of_term_map s.term)
  in
  TTy.Fold.find_def ~default:(fun _ -> default) tv (fold_of_ty_map s.ty)

let set (t : _ t) (s : Ground.Subst.t) v =
  let tv =
    TTerm.Fold.memo ~default:(fun c -> TTy.create c) t (fold_of_term_map s.term)
  in
  TTy.Fold.set tv (fold_of_ty_map s.ty) v
