(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

type t = {
  parents : Ground.S.t F_Pos.M.t;  (** parents *)
  apps : Ground.S.t F.M.t;  (** parent parent *)
  ty : Ground.Ty.S.t;  (** same as in ground but simplify incrementalism *)
}
[@@deriving show]

val merge : Egraph.wt -> other:Node.t -> repr:Node.t -> t -> t -> t
(** merge the information and apply congruence closure on the parents *)

val empty : t
val dom : t Dom.Kind.t

val tys : _ Egraph.t -> Node.t -> Ground.Ty.S.t
(** Same as Ground.Tys but from Info.t *)

module Builtin_skipped_for_trigger : sig
  val register : _ Egraph.t -> (Expr.builtin -> bool) -> unit
  (** The registered function tells when an application should be skipped when
      looking for patterns in a term (top down) *)

  val skipped : _ Egraph.t -> Expr.builtin -> bool
end
