(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib
open Base

(** Optimization of application of multiple patterns at once *)

type 'a generic_pattern_end =
  | End of 'a
  | AnyMatch of Pattern.t * 'a generic_pattern_end

type 'a generic_pattern_up =
  | Ups of F_Pos.t * Expr.Ty.t list * Pattern.t IArray.t * 'a generic_pattern_up
  | Multi of 'a generic_pattern_end

type 'a generic_pattern =
  | Match of Pattern.t * 'a generic_pattern_up
  | Direct of 'a generic_pattern_up

(** patterns to match the node against bottom up *)

type 'a t [@@deriving show]
type 'a ground [@@deriving show]
type 'a node [@@deriving show]
type 'a ty [@@deriving show]
type 'a inv_path [@@deriving show]

val convert_ground : Pattern.t -> 'a -> 'a ground
val convert_node : Pattern.t -> 'a -> 'a node
val convert_ty : Expr.Ty.t -> 'a -> 'a ty
val convert_inv_path : 'a generic_pattern -> 'a inv_path

type 'a res = 'a -> Ground.Subst.t -> unit

val exec_ground : res:'a res -> _ Egraph.t -> 'a ground -> Ground.t -> unit
val exec_node : res:'a res -> _ Egraph.t -> 'a node -> Node.t -> unit
val exec_ty : res:'a res -> _ Egraph.t -> 'a ty -> Ground.Ty.t -> unit

val exec_inv_path :
  res:'a res -> _ Egraph.t -> 'a inv_path -> down:Node.t -> up:Node.t -> unit

val merge_inv_path : 'a inv_path -> 'a inv_path -> 'a inv_path
val empty_inv_path : 'a inv_path
