(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

val th_register : Egraph.wt -> unit

module Quantifier_skipped : sig
  val register : _ Egraph.t -> (Ground.ClosedQuantifier.s -> bool) -> unit

  type filter_term =
    | App of Expr.builtin * filter_ty list * filter_term list
    | Var of int
    | Choice of filter_term list
    | Any

  and filter_ty = TyApp of Expr.builtin * filter_ty list | TyAny
  and filter_quant = { vars : int list; filter : filter_term }

  val match_ : Ground.ClosedQuantifier.s -> filter_quant -> bool
end
