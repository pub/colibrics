(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

module Info2 = Info
open Base
module Info = Info2
open Colibri2_core
open Colibri2_popop_lib

let debug =
  Debug.register_info_flag ~desc:"Handling of definitions" "definitions"

module Fold = struct
  let criteria d (_sym : Expr.Term.Const.t) _tyl _tvl (body : Expr.term) =
    (* The definition should not start with a builtin symbol. The rational is that
       builtin symbols are hard to match efficiently: substitution not found often
       but takes a lot of time since there is a lot of term (e.g. boolean operators)
    *)
    match body.term_descr with
    | Expr.Var _ -> false
    | Expr.Cst _ -> false
    | Expr.App ({ term_descr = Expr.Cst s; _ }, _, _) ->
        not (Info.Builtin_skipped_for_trigger.skipped d s.builtin)
    | Expr.App (_, _, _) -> false
    | Expr.Binder (_, _) -> false
    | Expr.Match (_, _) -> false

  let nb_eager_folding = 25

  let add d contextual (sym : Expr.Term.Const.t) tyl tvl body =
    if criteria d sym tyl tvl body then
      let tys, tvs =
        Expr.Term.free_vars (Expr.Ty.Var.S.empty, Expr.Term.Var.S.empty) body
      in
      if
        List.for_all ~f:(fun v -> Expr.Ty.Var.S.mem v tys) tyl
        && List.for_all ~f:(fun v -> Expr.Term.Var.S.mem v tvs) tvl
      then
        let nb_folding = Context.Ref.create (Egraph.context d) (-1) in
        match Pattern.of_term_exn ~subst:Ground.Subst.empty body with
        | exception Pattern.Unconvertible -> ()
        | pat ->
            Debug.dprintf2 debug "[Def] Wait for folding %a" Expr.Term.Const.pp
              sym;
            InvertedPath.add_callback d pat (fun d subst ->
                let t =
                  Ground.apply d sym
                    (List.map
                       ~f:(fun s ->
                         Expr.Ty.Var.M.find_exn Impossible s subst.ty)
                       tyl)
                    (IArray.of_list_map
                       ~f:(fun s ->
                         Expr.Term.Var.M.find_exn Impossible s subst.term)
                       tvl)
                in
                Debug.dprintf2 debug "Fold definition of %a" Ground.Term.pp t;
                let n = Ground.node (Ground.index t) in
                if Context.Ref.get nb_folding < nb_eager_folding then (
                  Context.Ref.incr nb_folding;
                  Egraph.register d n)
                else
                  (match contextual with
                  | `Contextual -> DaemonLastEffort.schedule_immediately
                  | `Uncontextual ->
                      DaemonLastEffortUncontextual.schedule_immediately) d
                    (fun d -> Egraph.register d n))
end

module Unfold = struct
  type fundef = {
    tyl : Expr.Ty.Var.t list;
    tvl : Expr.Term.Var.t list;
    body : Expr.Term.t;
  }

  let fundefs = Expr.Term.Const.HC.create Fmt.nop "SynTerm.fundefs"

  let add d _ tc tyl tvl body =
    Expr.Term.Const.HC.set fundefs d tc { tyl; tvl; body }

  module ThTermH = Datastructure.Hashtbl (Ground)

  let levels = ThTermH.create Popop_stdlib.DInt.pp "ground_levels"

  (** Maximum level of unfolding *)
  let level_max = Options.register_int "def-max-unfolding-level" 15

  (** Maximum level of delayed decisions *)
  let level_dec_delayed =
    Options.register_int "def-max-delayed-decision-level" 5

  (** Maximum level where the terms can be directly decided *)
  let level_dec = Options.register_int "def-max-immediate-decision-level" 0

  let stats_function_call = Debug.register_stats_int "Ground.function_call"

  let converter d (v : Ground.t) =
    match Ground.sem v with
    | { app = tc; tyargs = tyl; args = tvl; ty = _ } -> (
        match Expr.Term.Const.HC.find_opt fundefs d tc with
        | None -> (* no definition associated *) ()
        | Some fundef ->
            let level = Option.value ~default:0 (ThTermH.find_opt levels d v) in
            if level <= Options.get d level_max then (
              Debug.dprintf3 debug "Unfold definition of %a at level %i"
                Expr.Term.Const.pp tc level;
              let cl = Ground.node v in
              let subst =
                {
                  Ground.Subst.term =
                    Expr.Term.Var.M.of_list
                      (Base.List.mapi
                         ~f:(fun i v -> (v, IArray.get tvl i))
                         fundef.tvl);
                  Ground.Subst.ty =
                    Expr.Ty.Var.M.of_list (List.zip_exn fundef.tyl tyl);
                }
              in
              let group = Choice.Group.create d in
              let n =
                Ground.convert_and_iter
                  (fun d th ->
                    if level <= Options.get d level_dec then
                      Choice.Group.make_choosable d (Ground.thterm th)
                    else Choice.Group.add_to_group d (Ground.thterm th) group;
                    match Ground.sem th with
                    | { app = tc; _ } -> (
                        match Expr.Term.Const.HC.find_opt fundefs d tc with
                        | None -> ()
                        | _ -> (
                            match ThTermH.find_opt levels d th with
                            | None -> ThTermH.set levels d th (1 + level)
                            | Some level' when 1 + level < level' ->
                                ThTermH.set levels d th (1 + level)
                            | _ -> ())))
                  (fun d th ->
                    if level <= Options.get d level_dec then
                      Choice.Group.make_choosable d
                        (Ground.ClosedQuantifier.thterm th)
                    else
                      Choice.Group.add_to_group d
                        (Ground.ClosedQuantifier.thterm th)
                        group)
                  (fun d th ->
                    if level <= Options.get d level_dec then
                      Choice.Group.make_choosable d
                        (Ground.NotTotallyApplied.thterm th)
                    else
                      Choice.Group.add_to_group d
                        (Ground.NotTotallyApplied.thterm th)
                        group)
                  d subst fundef.body
              in
              if
                Options.get d level_dec < level
                && level <= Options.get d level_dec_delayed
              then
                DaemonLastEffortUncontextual.schedule_immediately d (fun d ->
                    Choice.Group.activate d group);
              Debug.incr stats_function_call;
              Egraph.register d n;
              Egraph.merge d cl n))
end

let handler contextual d sym tyl tvl body =
  if List.is_empty tyl && List.is_empty tvl then (
    (* perhaps add the equality only later, when one or the other appear? *)
    let sym = Ground.convert d (Expr.Term.apply_cst sym [] []) in
    let body = Ground.convert d body in
    Egraph.register d sym;
    Egraph.register d body;
    Egraph.merge d sym body)
  else (
    Fold.add d contextual sym tyl tvl body;
    Unfold.add d contextual sym tyl tvl body)

(** Look for [if ... then f x y = ... else f x y = ...] *)
let check_if_it_is_a_definition d (g : Ground.ClosedQuantifier.t) =
  let equal_var_ty vty (ty : Expr.Ty.t) =
    match ty.ty_descr with
    | Expr.TyVar vty' -> Expr.Ty.Var.equal vty vty'
    | _ -> false
  in
  let equal_var_term vt (t : Expr.Term.t) =
    match t.term_descr with
    | Expr.Var vt' -> Expr.Term.Var.equal vt vt'
    | _ -> false
  in
  let rec find_application (e : Expr.Term.t) btys btl =
    match e.term_descr with
    | Var _ -> None
    | App ({ term_descr = Cst { builtin = Expr.Coercion; _ }; _ }, _, [ _ ]) ->
        None
    | App ({ term_descr = Cst { builtin = Expr.Ite; _ }; _ }, _, [ _; then_; _ ])
      ->
        find_application then_ btys btl
    | App
        ( { term_descr = Cst { builtin = Expr.Equal | Expr.Equiv; _ }; _ },
          _,
          [
            {
              term_descr =
                App
                  ( { term_descr = Cst ({ builtin = Expr.Base; _ } as cst); _ },
                    tys,
                    tl );
              _;
            };
            _;
          ] ) -> (
        match
          ( List.for_all2 ~f:equal_var_ty btys tys,
            List.for_all2 ~f:equal_var_term btl tl )
        with
        | Ok true, Ok true -> Some cst
        | _ -> None)
    | _ -> None
  in
  let rec check_is_definition (e : Expr.Term.t) bcst btys btl =
    match e.term_descr with
    | Var _ -> false
    | App ({ term_descr = Cst { builtin = Expr.Coercion; _ }; _ }, _, [ _ ]) ->
        false
    | App
        ( { term_descr = Cst { builtin = Expr.Ite; _ }; _ },
          _,
          [ _; then_; else_ ] ) ->
        check_is_definition then_ bcst btys btl
        && check_is_definition else_ bcst btys btl
    | App
        ( { term_descr = Cst { builtin = Expr.Equal | Expr.Equiv; _ }; _ },
          _,
          [
            {
              term_descr =
                App
                  ( { term_descr = Cst ({ builtin = Expr.Base; _ } as cst); _ },
                    tys,
                    tl );
              _;
            };
            _;
          ] ) -> (
        Expr.Term.Const.equal cst bcst
        &&
        match
          ( List.for_all2 ~f:equal_var_ty btys tys,
            List.for_all2 ~f:equal_var_term btl tl )
        with
        | Ok b1, Ok b2 -> b1 && b2
        | _ -> false)
    | _ -> false
  in
  let rec convert (e : Expr.Term.t) =
    match e.term_descr with
    | Var _ -> assert false
    | App ({ term_descr = Cst { builtin = Expr.Coercion; _ }; _ }, _, [ _ ]) ->
        assert false
    | App
        ( { term_descr = Cst { builtin = Expr.Ite; _ }; _ },
          _,
          [ cond; then_; else_ ] ) ->
        Expr.Term.ite cond (convert then_) (convert else_)
    | App
        ( { term_descr = Cst { builtin = Expr.Equal | Expr.Equiv; _ }; _ },
          _,
          [ _; lhs ] ) ->
        lhs
    | _ -> assert false
  in
  let s = Ground.ClosedQuantifier.sem g in
  match s.binder with
  | Exists -> false (* could be extended *)
  | Forall -> (
      match find_application s.body s.ty_vars s.term_vars with
      | Some cst when check_is_definition s.body cst s.ty_vars s.term_vars ->
          let b = convert s.body in
          Debug.dprintf4 debug "Found for %a the definition %a"
            Expr.Term.Const.pp cst Ground.ClosedQuantifier.pp g;
          handler `Contextual d cst s.ty_vars s.term_vars b;
          true
      | _ -> false)

let th_register d =
  Ground.Defs.add_handler d (handler `Uncontextual);
  Ground.register_converter d Unfold.converter
