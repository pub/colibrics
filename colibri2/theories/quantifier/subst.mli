(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

val convert :
  ?subst_old:Ground.Subst.t ->
  subst_new:Ground.Subst.t ->
  _ Egraph.t ->
  ?choice_group:Choice.Group.t ->
  Expr.term ->
  Node.t

(** Same as {!Ground.convert} but try to reuse terms that are equal and already
    in the egraph. Without choice_group make choosable *)

val convert_avoid_new_terms :
  subst_old:Ground.Subst.t ->
  subst_new:Ground.Subst.t ->
  _ Egraph.t ->
  Choice.Group.t option ->
  Expr.Term.t ->
  Node.t option
(** Same as {!convert} but doesn't create new terms (except for skipped builtins
    {!Trigger.Builtin_skipped_for_trigger.register} *)
