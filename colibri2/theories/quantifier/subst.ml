(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib

(** Copied from ground.ml *)

(** TODO The node substituted are not passed into fg since they come from
    somewhere else, inverted_path, ... fortunately we don't have too much
    pattern with booleans but we can decide on other thing. So the choice group
    and its life time must be better formalized *)

let debug = Debug.register_flag ~desc:"" "Quant.Subst"

type mode = { context : bool; avoid_new_term : bool }

exception New_term

let ft choice_group d th =
  match choice_group with
  | Some choice_group -> Choice.Group.add_to_group d th choice_group
  | None -> Choice.Group.make_choosable d th

let fg choice_group d th = ft choice_group d (Ground.thterm th)

let fcq choice_group d th =
  ft choice_group d (Ground.ClosedQuantifier.thterm th)

let fnt choice_group d th =
  ft choice_group d (Ground.NotTotallyApplied.thterm th)

(* let fg _ (_ : Ground.t) = () *)
(* let fcq _ (_ : Ground.ClosedQuantifier.t) = () *)
(* let fnt _ (_ : Ground.NotTotallyApplied.t) = () *)

let skipped_builtin d builtin =
  Info.Builtin_skipped_for_trigger.skipped d builtin

let of_node mode d n =
  if mode.avoid_new_term && not (Egraph.is_registered d n) then raise New_term;
  Node.S.singleton n

(** subst_old: substitution applied on top of the quantification subst_new:
    substitution to substitute the variable quantified subst: the disjoint merge
    of the two *)

let rec convert_and_iter mode d (subst_old : Ground.Subst.t)
    (subst_new : Ground.Subst.t) subst choice_group (t : Expr.Term.t) :
    Node.S.t * bool (* with freevars *) =
  match t.term_descr with
  | Var v -> (
      Debug.dprintf2 debug "ci var %a" Expr.Term.pp t;
      match
        ( Expr.Term.Var.M.find_opt v subst_old.term,
          Expr.Term.Var.M.find_opt v subst_new.term )
      with
      | None, None -> invalid_arg (Fmt.str "Not_ground: %a" Expr.Term.Var.pp v)
      | Some n1, Some n2 ->
          invalid_arg
            (Fmt.str "convert_iter: duplicate variable %a: %a -- %a"
               Expr.Term.Var.pp v Node.pp n1 Node.pp n2)
      | None, Some n -> (Node.S.singleton n, true)
      | Some n, None -> (Node.S.singleton n, false))
  | App
      ( { term_descr = Cst { builtin = Expr.Ite; _ }; _ },
        _,
        [
          cond;
          { term_descr = Cst { builtin = Expr.True; _ }; _ };
          { term_descr = Cst { builtin = Expr.False; _ }; _ };
        ] ) ->
      (* Why3 generates spurious (if v then true else false) *)
      convert_and_iter mode d subst_old subst_new subst choice_group cond
  | App (({ term_descr = Cst cst; _ } as f), tyargs, args) ->
      Debug.dprintf2 debug "ci app cst %a" Expr.Term.pp t;
      let with_freevars = ref false in
      let args =
        IArray.of_list_map
          ~f:(fun arg ->
            let arg', wf =
              convert_and_iter mode d subst_old subst_new subst choice_group arg
            in
            Debug.dprintf3 debug "fv %a: %b" Expr.Term.pp arg wf;
            with_freevars := !with_freevars || wf;
            arg')
          args
      in
      let nodes = Pattern.find_existing_app d subst cst tyargs args in
      Ground.S.iter (fg choice_group d) nodes;
      let nodes =
        Node.S.of_list (List.map Ground.node (Ground.S.elements nodes))
      in
      Debug.dprintf6 debug "find_existing_app: %a[%a] -> %a" F.pp cst
        (IArray.pp Node.S.pp) args Node.S.pp nodes;
      let mode =
        if mode.context then
          if skipped_builtin d cst.builtin then mode
          else { mode with context = false }
        else mode
      in
      if Node.S.is_empty nodes then (
        if !with_freevars && mode.avoid_new_term && not mode.context then (
          Debug.dprintf2 debug "new term cst: %a" Expr.Term.pp t;
          raise New_term);
        let args = IArray.map ~f:Node.S.choose args in
        let n =
          Ground.convert_one_app_and_iter d f tyargs args t.term_ty subst
            (fg choice_group) (fcq choice_group) (fnt choice_group)
        in
        (Node.S.singleton n, !with_freevars))
      else (nodes, !with_freevars)
  | App (f, tyargs, args) ->
      Debug.dprintf2 debug "ci app %a" Expr.Term.pp t;
      if mode.avoid_new_term then (
        Debug.dprintf2 debug "new term generic: %a" Expr.Term.pp t;
        raise New_term);
      let with_freevars = ref false in
      let args =
        IArray.of_list_map
          ~f:(fun arg ->
            let arg, wf =
              convert_and_iter mode d subst_old subst_new subst choice_group arg
            in
            with_freevars := !with_freevars || wf;
            arg)
          args
      in
      let args = IArray.map ~f:Node.S.choose args in
      let n =
        Ground.convert_one_app_and_iter d f tyargs args t.term_ty subst
          (fg choice_group) (fcq choice_group) (fnt choice_group)
      in
      (Node.S.singleton n, !with_freevars)
  | Cst f ->
      Debug.dprintf2 debug "ci cst %a" Expr.Term.pp t;
      (* even if it could create a new term, it will create it only once (global term) *)
      ( Node.S.singleton
          (Ground.convert_one_cst_and_iter d f t.term_ty subst (fg choice_group)
             (fnt choice_group)),
        false )
  | Expr.Binder (((Exists _ | Forall _ | Lambda _) as b), body) ->
      ( Ground.convert_one_binder_and_iter d b body t.term_ty subst
          (fcq choice_group) (fnt choice_group)
        |> of_node mode d,
        true )
  | Expr.Binder (Let_seq l, body) ->
      let subst_old, subst_new, subst =
        List.fold_left
          (fun (subst_old, subst_new, subst) (v, t) ->
            let n, fv =
              convert_and_iter
                { mode with context = false }
                d subst_old subst_new subst choice_group t
            in
            let n = Node.S.choose n in
            let add (s : Ground.Subst.t) =
              { s with term = Expr.Term.Var.M.add v n s.term }
            in
            if fv then (subst_old, add subst_new, add subst)
            else (add subst_old, subst_new, add subst))
          (subst_old, subst_new, subst)
          l
      in
      convert_and_iter mode d subst_old subst_new subst choice_group body
  | Expr.Binder (Let_par l, body) ->
      let subst_old, subst_new, subst =
        List.fold_left
          (fun (subst_old', subst_new', subst') (v, t) ->
            let n, fv =
              convert_and_iter
                { mode with context = false }
                d subst_old subst_new subst choice_group t
            in
            let n = Node.S.choose n in
            let add (s : Ground.Subst.t) =
              { s with term = Expr.Term.Var.M.add v n s.term }
            in
            if fv then (subst_old', add subst_new', add subst')
            else (add subst_old', subst_new', add subst'))
          (subst_old, subst_new, subst)
          l
      in
      convert_and_iter mode d subst_old subst_new subst choice_group body
  | Expr.Match (_, _) -> invalid_arg "match from dolmen not implemented"
(* TODO convert to one multitest like
   the match of why3  and projection *)

let rec check_context_aux d (subst_old : Ground.Subst.t)
    (subst_new : Ground.Subst.t) (subst : Ground.Subst.t)
    (choice_group : Choice.Group.t option) (t : Expr.Term.t) =
  match t.term_descr with
  | Var _ -> true
  | App
      ( { term_descr = Cst { builtin = Expr.Ite; _ }; _ },
        _,
        [
          cond;
          { term_descr = Cst { builtin = Expr.True; _ }; _ };
          { term_descr = Cst { builtin = Expr.False; _ }; _ };
        ] ) ->
      (* Why3 generates spurious (if v then true else false) *)
      check_context d subst_old subst_new subst choice_group cond
  | App ({ term_descr = Cst cst; _ }, _, args) ->
      if skipped_builtin d cst.builtin then
        List.for_all
          (check_context d subst_old subst_new subst choice_group)
          args
      else
        let n, _ =
          convert_and_iter
            { context = true; avoid_new_term = true }
            d subst_old subst_new subst choice_group t
        in
        not (Node.S.is_empty n)
  | App _ -> false
  | Cst _ ->
      true
      (* Ground.convert_one_cst subst d f
       * |> of_node { context = true; avoid_new_term = true } d
       * |> Node.S.is_empty |> not *)
  | Expr.Binder (((Exists _ | Forall _ | Lambda _) as b), body) ->
      Ground.convert_one_binder_and_iter d b body t.term_ty subst
        (fcq choice_group) (fnt choice_group)
      |> of_node { context = true; avoid_new_term = true } d
      |> Node.S.is_empty |> not
  | Expr.Binder (Let_seq l, body) ->
      let subst_old, subst_new, subst =
        List.fold_left
          (fun (subst_old, subst_new, subst) (v, t) ->
            let n, fv =
              convert_and_iter
                { context = false; avoid_new_term = true }
                d subst_old subst_new subst choice_group t
            in
            let n = Node.S.choose n in
            let add (s : Ground.Subst.t) =
              { s with term = Expr.Term.Var.M.add v n s.term }
            in
            if fv then (subst_old, add subst_new, add subst)
            else (add subst_old, subst_new, subst))
          (subst_old, subst_new, subst)
          l
      in
      check_context d subst_old subst_new subst choice_group body
  | Expr.Binder (Let_par l, body) ->
      let subst_old, subst_new, subst =
        List.fold_left
          (fun (subst_old', subst_new', subst') (v, t) ->
            let n, fv =
              convert_and_iter
                { context = false; avoid_new_term = true }
                d subst_old subst_new subst choice_group t
            in
            let n = Node.S.choose n in
            let add (s : Ground.Subst.t) =
              { s with term = Expr.Term.Var.M.add v n s.term }
            in
            if fv then (subst_old', add subst_new', add subst')
            else (add subst_old', subst_new', subst'))
          (subst_old, subst_new, subst)
          l
      in
      check_context d subst_old subst_new subst choice_group body
  | Expr.Match (_, _) -> invalid_arg "match from dolmen not implemented"
(* TODO convert to one multitest like
   the match of why3  and projection *)

and check_context d subst_old subst_new subst choice_group t =
  let c = check_context_aux d subst_old subst_new subst choice_group t in
  if not c then Debug.dprintf2 debug "check false: %a" Expr.Term.pp t;
  c

let convert ?(subst_old = Ground.Subst.empty) ~subst_new d ?choice_group t =
  let subst = Ground.Subst.distinct_union subst_old subst_new in
  try
    let n, _ =
      convert_and_iter
        { context = true; avoid_new_term = false }
        d subst_old subst_new subst choice_group t
    in
    Node.S.choose n
  with New_term -> assert false

let convert_avoid_new_terms ~subst_old ~subst_new d choice_group t =
  let subst = Ground.Subst.distinct_union subst_old subst_new in
  let b =
    try check_context d subst_old subst_new subst choice_group (* todo *) t
    with New_term ->
      Debug.dprintf0 debug "check_context";
      false
  in
  if b then
    try
      let n, _ =
        convert_and_iter
          { context = true; avoid_new_term = true }
          d subst_old subst_new subst choice_group t
      in
      Some (Node.S.choose n)
    with New_term -> None
  else None
