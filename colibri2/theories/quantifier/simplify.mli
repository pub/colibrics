(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_core

(** Simplify quantifiers *)
type simplified =
  | NoSimplification
  | ClosedQuantifier of Ground.ClosedQuantifier.t
  | ClosedQuantifierKeepOriginal of Ground.ClosedQuantifier.t
  | Ground of Node.t

val remove_unused_variables :
  _ Egraph.t -> Ground.ClosedQuantifier.t -> simplified

val regroup_forall_with_implies :
  _ Egraph.t -> Ground.ClosedQuantifier.t -> simplified

val simplify : _ Egraph.t -> Ground.ClosedQuantifier.t -> simplified
(** all the previous simplification *)
