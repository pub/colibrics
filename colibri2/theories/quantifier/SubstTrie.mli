(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

type 'a t
(** A map of substitution *)

val create : Context.creator -> 'a t

val find_and_add : bool t -> Ground.Subst.t -> bool
(** Return if the substitution is in the set, and add it if it is not the case
*)

val find_def : default:'a -> 'a t -> Ground.Subst.t -> 'a
(** Return the value associated to the substitution, or [default] if it is not
    binded *)

val set : 'a t -> Ground.Subst.t -> 'a -> unit
(** Return set the associated value of the substitution *)
