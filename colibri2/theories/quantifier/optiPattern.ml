(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib
module QInfo = Info
open Base
open Colibri2_core

[@@@ocaml.warning "+9"]

(** Efficient E-Matching paper, code trees part with inverted pathtrees boosted
    with code trees and early matching *)

let debug =
  Debug.register_flag ~desc:"Code trees used for optimizing pattern matching"
    "codetree"

module MReg () : sig
  type t = private int

  include Popop_stdlib.Datatype with type t := t

  type 'a arr

  val zero : t
  val next : t -> t
  val get : 'a arr -> t -> 'a
  val set : 'a arr -> t -> 'a -> unit
  val create_arr : max:t -> 'a -> 'a arr
  val max : t -> t -> t
end = struct
  include Popop_stdlib.DInt

  type 'a arr = 'a array

  let equal = Int.equal
  let zero = 0
  let next = Int.( + ) 1
  let get = Array.get
  let set = Array.set
  let create_arr ~max v = Array.create ~len:max v
  let max = Int.max
end

module Reg = MReg ()
module RegTy = MReg ()

module Sequence = struct
  include Sequence

  let pp _ fmt _ = Fmt.pf fmt "<sequence>"
end

type 'a code =
  | StaticRegistered of { n : Node.t; next : 'a code }
  | CheckNode of { i : Reg.t; n : Node.t; next : 'a code }
  | CheckTy of { i : RegTy.t; n : Ground.Ty.t; next : 'a code }
  | BindParent of { i : Reg.t; o : Reg.t; oty : RegTy.t; f : 'a code F_Pos.M.t }
  | BindApp of { i : Reg.t; o : Reg.t; oty : RegTy.t; f : 'a code F.M.t }
  | BindAnyApp of {
      o : Reg.t;
      oty : RegTy.t;
      f : Expr.Term.Const.t;
      next : 'a code;
    }
  | BindTy of { i : RegTy.t; o : RegTy.t; f : Expr.Ty.Const.t; next : 'a code }
  | CompareTy of { i1 : RegTy.t; i2 : RegTy.t; next : 'a code }
  | Compare of { i1 : Reg.t; i2 : Reg.t; next : 'a code }
  | TypeOf of { i : Reg.t; oty : RegTy.t; next : 'a code }
  | EnumGround of {
      save_elt : bool;
      o : Reg.t;
      oty : RegTy.t;
      enum : Ground.t Sequence.t;
      next : 'a code;
    }
  | EnumTy of { enum : Ground.Ty.t Sequence.t; oty : RegTy.t; next : 'a code }
  | Yield of {
      subst : Reg.t Expr.Term.Var.M.t;
      substty : RegTy.t Expr.Ty.Var.M.t;
      v : 'a;
      oty : RegTy.t;
      o : Reg.t;
    }
  | Alternative of 'a code * 'a code
[@@deriving show]

let shallow_pp_code fmt = function
  | StaticRegistered { n; next = _ } -> Fmt.pf fmt "Is %a reg?" Node.pp n
  | CheckNode { i; n; next = _ } -> Fmt.pf fmt "CN(%a=%a?)" Reg.pp i Node.pp n
  | CheckTy { i; n; next = _ } ->
      Fmt.pf fmt "CT(%a=%a?)" RegTy.pp i Ground.Ty.pp n
  | Yield { subst; substty; v = _; oty = _; o = _ } ->
      Fmt.pf fmt "Y(%a|%a)"
        (Expr.Ty.Var.M.pp RegTy.pp)
        substty
        (Expr.Term.Var.M.pp Reg.pp)
        subst
  | BindParent { i; o; oty; f } ->
      Fmt.pf fmt "BP(%a at %a =>%a,%a)" Reg.pp i (F_Pos.M.pp Fmt.nop) f RegTy.pp
        oty Reg.pp o
  | EnumGround { save_elt; o; oty; enum; next = _ } ->
      let shallow_pp_enum fmt enum =
        match Sequence.next enum with
        | None -> ()
        | Some (v, _) -> Ground.pp fmt v
      in
      Fmt.pf fmt "EG%s(%a=>%a,%a)"
        (if save_elt then "S" else "")
        shallow_pp_enum enum RegTy.pp oty Reg.pp o
  | BindApp { f; i; o; oty } ->
      Fmt.pf fmt "BP(%a at %a =>%a,%a)" Reg.pp i (F.M.pp Fmt.nop) f RegTy.pp oty
        Reg.pp o
  | BindAnyApp { f; o; oty; next = _ } ->
      Fmt.pf fmt "BAP(%a =>%a,%a)" F.pp f RegTy.pp oty Reg.pp o
  | BindTy { f; i; o; next = _ } ->
      Fmt.pf fmt "BP(%a at %a =>%a)" RegTy.pp i Expr.Ty.Const.pp f RegTy.pp o
  | CompareTy { i1; i2; next = _ } ->
      Fmt.pf fmt "CT(%a=%a)" RegTy.pp i1 RegTy.pp i2
  | Compare { i1; i2; next = _ } -> Fmt.pf fmt "CT(%a=%a)" Reg.pp i1 Reg.pp i2
  | TypeOf { i; oty; next = _ } ->
      Fmt.pf fmt "TO(%a => %a)" Reg.pp i RegTy.pp oty
  | EnumTy { oty; enum; next = _ } ->
      let shallow_pp_enum fmt enum =
        match Sequence.next enum with
        | None -> ()
        | Some (v, _) -> Ground.Ty.pp fmt v
      in
      Fmt.pf fmt "ET(%a=>%a)" shallow_pp_enum enum RegTy.pp oty
  | Alternative _ -> Fmt.pf fmt "Alternative"

(*
let rec check w wty = function
  | Yield { subst; substty; _ } ->
      Expr.Term.Var.M.for_all (fun _ v -> Reg.S.mem v w) subst
      && Expr.Ty.Var.M.for_all (fun _ v -> RegTy.S.mem v wty) substty
  | StaticRegistered { next; _ } -> check w wty next
  | CheckNode { i; next; _ } -> Reg.S.mem i w && check w wty next
  | CheckTy _ { i; next; _ } -> RegTy.S.mem i w && check w wty next
  | BindParent { f; o; oty; next; _} ->
    let w,_,ok = List.fold f.
    
    | BindApp _ | BindAnyApp _ | BindTy _ | CompareTy _
  | Compare _ | TypeOf _ | EnumGround _ | EnumTy _
  | Alternative (c1, c2) ->
      check w wty c1 & check w wty c2
      *)

type 'a init_ground =
  | Ground of {
      f : Expr.Term.Const.t;
      next : 'a code;
      o : Reg.t;
      oty : RegTy.t;
    }
  | GroundAsNode of { next : 'a code }
[@@deriving show]

type 'a init_node = Node of { next : 'a code } [@@deriving show]
type 'a init_ty = Ty of { next : 'a code } [@@deriving show]

type 'b t = { code : 'b; max_regty : RegTy.t; max_reg : Reg.t }
[@@deriving show]

type 'a ground = 'a init_ground t [@@deriving show]
type 'a node = 'a init_node t [@@deriving show]
type 'a ty = 'a init_ty t [@@deriving show]

let ground_map_sequence s =
  Sequence.unfold ~init:(Ground.M.start_enum s) ~f:(fun enum ->
      match Ground.M.val_enum enum with
      | None -> None
      | Some (v, ()) -> Some (v, Ground.M.next_enum enum))

type 'a env = {
  regty : Ground.Ty.t RegTy.arr;
  reg : Node.t Reg.arr;
  bstack : 'a code Stack.t;
  res : 'a -> Ground.Subst.t -> unit;
}

let exec_ground_set ~get_info ~fold2_inter ~f ?(save_elt = false) d env ~o ~oty
    ~i exec backtrack =
  let g = Reg.get env.reg i in
  let info = Egraph.get_dom d QInfo.dom g in
  match info with
  | Some info -> (
      let r =
        fold2_inter
          (fun _ next s acc ->
            let enum = ground_map_sequence s in
            let next = EnumGround { save_elt; o; oty; enum; next } in
            match acc with
            | None -> Some next
            | _ ->
                Stack.push env.bstack next;
                acc)
          f (get_info info) None
      in
      match r with None -> backtrack d env | Some next -> exec d env next)
  | None -> backtrack d env

let rec exec d env c : unit =
  Debug.dprintf2 debug "exec: %a" shallow_pp_code c;
  match c with
  | StaticRegistered { n; next } ->
      if Egraph.is_registered d n then exec d env next else backtrack d env
  | CheckNode { i; n; next } ->
      if Egraph.is_equal d (Reg.get env.reg i) n then exec d env next
      else backtrack d env
  | CheckTy { i; n; next } ->
      if Ground.Ty.equal (RegTy.get env.regty i) n then exec d env next
      else backtrack d env
  | BindTy { i; o; f; next } ->
      let ty = RegTy.get env.regty i in
      if Expr.Ty.Const.equal f ty.app then
        let _ =
          List.fold ty.args
            ~f:(fun o ty ->
              RegTy.set env.regty o ty;
              RegTy.next o)
            ~init:o
        in
        exec d env next
      else backtrack d env
  | BindApp { i; o; oty; f } ->
      exec_ground_set ~fold2_inter:F.M.fold2_inter
        ~get_info:(fun info -> info.QInfo.apps)
        ~save_elt:false d env ~o ~oty ~i ~f exec backtrack
  | BindParent { i; o; oty; f } ->
      exec_ground_set ~fold2_inter:F_Pos.M.fold2_inter
        ~get_info:(fun info -> info.QInfo.parents)
        ~save_elt:true d env ~o ~oty ~i ~f exec backtrack
  | BindAnyApp { o; oty; f; next } ->
      let enum =
        Context.Push.to_seq (F.EnvApps.find Pattern.env_ground_by_apps d f)
      in
      exec d env (EnumGround { save_elt = false; o; oty; enum; next })
  | Yield { subst; substty; v; oty = _; o = _ } ->
      let subst =
        Ground.Subst.
          {
            term = Expr.Term.Var.M.map (fun r -> Reg.get env.reg r) subst;
            ty = Expr.Ty.Var.M.map (fun r -> RegTy.get env.regty r) substty;
          }
      in
      env.res v subst;
      backtrack d env
  | Compare { i1; i2; next } ->
      if Egraph.is_equal d (Reg.get env.reg i1) (Reg.get env.reg i2) then
        exec d env next
      else backtrack d env
  | CompareTy { i1; i2; next } ->
      if Ground.Ty.equal (RegTy.get env.regty i1) (RegTy.get env.regty i2) then
        exec d env next
      else backtrack d env
  | TypeOf { i; oty; next } ->
      let enum =
        Sequence.unfold
          ~init:(Ground.Ty.M.start_enum (QInfo.tys d (Reg.get env.reg i)))
          ~f:(fun enum ->
            match Ground.Ty.M.val_enum enum with
            | None -> None
            | Some (v, ()) -> Some (v, Ground.Ty.M.next_enum enum))
      in

      exec d env (EnumTy { enum; oty; next })
  | EnumGround { save_elt; o; oty; enum; next } -> (
      match Sequence.next enum with
      | Some (g, enum) ->
          Stack.push env.bstack (EnumGround { save_elt; o; oty; enum; next });
          let o =
            if save_elt then (
              Reg.set env.reg o (Ground.node g);
              Reg.next o)
            else o
          in
          let { Ground.app = _; tyargs = tyl; args; _ } = Ground.sem g in
          let _ =
            List.fold tyl
              ~f:(fun o ty ->
                RegTy.set env.regty o ty;
                RegTy.next o)
              ~init:oty
          in
          let _ =
            IArray.fold args
              ~f:(fun o n ->
                Reg.set env.reg o n;
                Reg.next o)
              ~init:o
          in
          exec d env next
      | None -> backtrack d env)
  | EnumTy { enum; oty; next } -> (
      match Sequence.next enum with
      | None -> backtrack d env
      | Some (ty, enum) ->
          Stack.push env.bstack (EnumTy { enum; oty; next });
          RegTy.set env.regty oty ty;
          exec d env next)
  | Alternative (c1, c2) ->
      Stack.push env.bstack c2;
      exec d env c1

and backtrack d env =
  Debug.dprintf0 debug "exec: backtrack";
  match Stack.pop env.bstack with None -> () | Some c -> exec d env c

let exec ~res ~init d t next : unit =
  let regty = RegTy.create_arr ~max:t.max_regty Ground.Ty.bool in
  let reg = Reg.create_arr ~max:t.max_reg Boolean._true in
  let env = { regty; reg; bstack = Stack.create (); res } in
  init env;
  exec d env next

module TodoPattern : sig
  type t

  val empty : t
  val add : t -> Reg.t * Pattern.t -> t
  val next : t -> ((Reg.t * Pattern.t) * t) option
  val of_list : (Reg.t * Pattern.t) list -> t
end = struct
  type t = {
    quick : (Reg.t * Pattern.t) list;
    choice : (Reg.t * Pattern.t) list;
  }

  let empty = { quick = []; choice = [] }

  let add l p =
    match p with
    | _, Pattern.Var _ | _, Pattern.Node _ ->
        { quick = p :: l.quick; choice = l.choice }
    | _, App _ ->
        if true then { quick = p :: l.quick; choice = l.choice }
        else { quick = l.quick; choice = p :: l.choice }

  let next { quick; choice } =
    match quick with
    | x :: quick -> Some (x, { quick; choice })
    | [] -> (
        match choice with
        | [] -> None
        | x :: choice -> Some (x, { quick; choice }))

  let of_list l = Base.List.fold l ~init:empty ~f:add
end

let rec convert_ty (p : Expr.Ty.t) (i : RegTy.t) lty (l : TodoPattern.t)
    bindedty binded (oty : RegTy.t) (o : Reg.t) v : 'a code =
  if Expr.Ty.has_no_vars p then
    let ty = Ground.Ty.convert Expr.Ty.Var.M.empty p in
    CheckTy { i; n = ty; next = convert lty l bindedty binded oty o v }
  else
    match p.ty_descr with
    | TyVar var -> (
        match Expr.Ty.Var.M.find var bindedty with
        | exception Stdlib.Not_found ->
            let bindedty = Expr.Ty.Var.M.add var i bindedty in
            convert lty l bindedty binded oty o v
        | regty ->
            CompareTy
              {
                i1 = i;
                i2 = regty;
                next = convert lty l bindedty binded oty o v;
              })
    | TyApp (cst', tyl') ->
        let oty', lty = check_ty_list oty lty tyl' in
        BindTy
          {
            i;
            o = oty;
            f = cst';
            next = convert lty l bindedty binded oty' o v;
          }
    | _ -> assert false
(* absurd: should not be a trigger *)

and check_ty_list oty lty tyl' =
  let fold (oty, lty) ty = (RegTy.next oty, (oty, ty) :: lty) in
  let oty', lty = List.fold ~f:fold ~init:(oty, lty) tyl' in
  (oty', lty)

and check_args_list ?except o l pargs =
  let fold (o, l) ty = (Reg.next o, TodoPattern.add l (o, ty)) in
  let o, l =
    match except with
    | None -> IArray.fold ~f:fold ~init:(o, l) pargs
    | Some except ->
        IArray.foldi
          ~f:(fun i ((o, l) as acc) x ->
            if i = except then (Reg.next o, l) else fold acc x)
          ~init:(o, l) pargs
  in
  (o, l)

and convert_pat (p : Pattern.t) (i : Reg.t) lty (l : TodoPattern.t) bindedty
    binded (oty : RegTy.t) (o : Reg.t) v : 'a code =
  match p with
  | Var { var; ty } -> (
      let lty = (oty, ty) :: lty in
      let oty' = RegTy.next oty in
      match Expr.Term.Var.M.find var binded with
      | exception Stdlib.Not_found ->
          let binded = Expr.Term.Var.M.add var i binded in
          let lty = (oty, ty) :: lty in
          let next = convert lty l bindedty binded oty' o v in
          let next = TypeOf { i; oty; next } in
          next
      | reg ->
          let lty = (oty, ty) :: lty in
          let next = convert lty l bindedty binded oty' o v in
          let next = TypeOf { i; oty; next } in
          Compare { i1 = i; i2 = reg; next })
  | App (pf, ptyl, pargs) ->
      let oty', lty = check_ty_list oty lty ptyl in
      let o', l = check_args_list o l pargs in
      BindApp
        {
          i;
          o;
          oty;
          f = F.M.singleton pf (convert lty l bindedty binded oty' o' v);
        }
  | Node n' ->
      CheckNode { i; n = n'; next = convert lty l bindedty binded oty o v }
(* absurd: should not be a trigger *)

and convert (lty : (RegTy.t * Expr.Ty.t) list) (l : TodoPattern.t) bindedty
    binded (oty : RegTy.t) (o : Reg.t) v : 'a code =
  match lty with
  | (regty, ty) :: lty -> convert_ty ty regty lty l bindedty binded oty o v
  | [] -> (
      match TodoPattern.next l with
      | None -> v bindedty binded oty o
      | Some ((reg, p), l) -> convert_pat p reg lty l bindedty binded oty o v)

let rec get_next_output = function
  | Yield { oty; o; _ } -> (oty, o)
  | StaticRegistered { next; _ }
  | CheckNode { next; _ }
  | CheckTy { next; _ }
  | BindAnyApp { next; _ }
  | EnumGround { next; _ }
  | BindTy { next; _ }
  | CompareTy { next; _ }
  | Compare { next; _ }
  | TypeOf { next; _ }
  | EnumTy { next; _ } ->
      get_next_output next
  | BindParent { f; _ } ->
      F_Pos.M.fold
        (fun _ c (oty2, o2) ->
          let oty1, o1 = get_next_output c in
          (RegTy.max oty1 oty2, Reg.max o1 o2))
        f (RegTy.zero, Reg.zero)
  | BindApp { f; _ } ->
      F.M.fold
        (fun _ c (oty2, o2) ->
          let oty1, o1 = get_next_output c in
          (RegTy.max oty1 oty2, Reg.max o1 o2))
        f (RegTy.zero, Reg.zero)
  | Alternative (c1, c2) ->
      let oty1, o1 = get_next_output c1 in
      let oty2, o2 = get_next_output c2 in
      (RegTy.max oty1 oty2, Reg.max o1 o2)

let convert_gen ?(substty = Expr.Ty.Var.M.empty)
    ?(subst = Expr.Term.Var.M.empty) init lty l oty o v =
  let v bindedty binded oty o =
    Yield { subst = binded; substty = bindedty; oty; o; v }
  in
  let code = convert lty l substty subst oty o v in
  let oty, o = get_next_output code in
  { code = init code; max_regty = oty; max_reg = o }

let convert_node p v =
  let i = Reg.zero in
  let o = Reg.next i in
  convert_gen
    (fun next -> Node { next })
    []
    (TodoPattern.of_list [ (i, p) ])
    RegTy.zero o v

let exec_node ~res d t n =
  match t.code with
  | Node { next } ->
      exec ~res d t next ~init:(fun env -> Reg.set env.reg Reg.zero n)

let convert_ty p v =
  let i = RegTy.zero in
  let oty = RegTy.next i in
  convert_gen
    (fun next -> Ty { next })
    [ (i, p) ]
    TodoPattern.empty oty Reg.zero v

let exec_ty ~res d t n =
  match t.code with
  | Ty { next } ->
      exec ~res d t next ~init:(fun env -> RegTy.set env.regty RegTy.zero n)

let convert_ground (p : Pattern.t) v =
  match p with
  | App (pf, ptyl, pargs) ->
      let oty, lty = check_ty_list RegTy.zero [] ptyl in
      let o, l = check_args_list Reg.zero TodoPattern.empty pargs in
      convert_gen (fun next -> Ground { f = pf; next; oty; o }) lty l oty o v
  | Var _ | Node _ ->
      let o = Reg.next Reg.zero in
      convert_gen
        (fun next -> GroundAsNode { next })
        []
        (TodoPattern.of_list [ (Reg.zero, p) ])
        RegTy.zero o v

let exec_ground ~res d (t : _ init_ground t) (g : Ground.t) =
  match t.code with
  | Ground { f; o; oty; next } ->
      let { Ground.app; _ } = Ground.sem g in
      if Expr.Term.Const.equal app f then
        let next =
          EnumGround
            { save_elt = false; enum = Sequence.return g; o; oty; next }
        in
        exec ~res d t next ~init:(fun env ->
            Reg.set env.reg Reg.zero (Ground.node g))
      else ()
  | GroundAsNode { next } ->
      exec ~res d t next ~init:(fun env ->
          Reg.set env.reg Reg.zero (Ground.node g))

type 'a generic_pattern_end =
  | End of 'a
  | AnyMatch of Pattern.t * 'a generic_pattern_end

type 'a generic_pattern_up =
  | Ups of F_Pos.t * Expr.Ty.t list * Pattern.t IArray.t * 'a generic_pattern_up
  | Multi of 'a generic_pattern_end

type 'a generic_pattern =
  | Match of Pattern.t * 'a generic_pattern_up
  | Direct of 'a generic_pattern_up

let rec convert_generic_end g bindedty binded (oty : RegTy.t) (o : Reg.t) =
  match g with
  | End v -> Yield { subst = binded; substty = bindedty; oty; o; v }
  | AnyMatch (p, g) -> (
      match p with
      | Var _ -> assert false (* todo *)
      | App (f, ptyl, pargs) ->
          let oty', lty = check_ty_list oty [] ptyl in
          let o', l = check_args_list o TodoPattern.empty pargs in
          let next =
            convert lty l bindedty binded oty' o' (convert_generic_end g)
          in
          BindAnyApp { f; o; oty; next }
      | Node n ->
          StaticRegistered
            { n; next = convert_generic_end g bindedty binded oty o })

let rec convert_generic_ups g (i : Reg.t) bindedty binded (oty : RegTy.t)
    (o : Reg.t) =
  match g with
  | Ups (f, ptyl, pargs, g) ->
      let oty', lty = check_ty_list oty [] ptyl in
      let o', l =
        check_args_list ~except:f.pos (Reg.next o) TodoPattern.empty pargs
      in
      let next =
        convert lty l bindedty binded oty' o' (convert_generic_ups g o)
      in
      BindParent { f = F_Pos.M.singleton f next; i; o; oty }
  | Multi m -> convert_generic_end m bindedty binded oty o

type 'a inv_path = Empty | C of 'a code t [@@deriving show]

let convert_inv_path g =
  let i = Reg.zero in
  let i' = Reg.next i in
  let o = Reg.next i' in
  let bindedty = Expr.Ty.Var.M.empty in
  let binded = Expr.Term.Var.M.empty in
  let oty = RegTy.zero in
  match g with
  | Match (p, g) ->
      let code =
        convert_pat p i [] TodoPattern.empty bindedty binded oty o
          (convert_generic_ups g i')
      in
      let oty, o = get_next_output code in
      C { code; max_regty = oty; max_reg = o }
  | Direct g ->
      let code = convert_generic_ups g i' bindedty binded oty o in
      let oty, o = get_next_output code in
      C { code; max_regty = oty; max_reg = o }

let convert_inv_path g =
  let r = convert_inv_path g in
  Debug.dprintf2 debug "convert_inv_path:%a" (pp_inv_path Fmt.nop) r;
  r

let exec_inv_path ~res d (t : 'a inv_path) ~down ~up =
  match t with
  | Empty -> ()
  | C t ->
      exec ~res d t t.code ~init:(fun env ->
          let i = Reg.zero in
          let i' = Reg.next i in
          Reg.set env.reg i down;
          Reg.set env.reg i' up)

let rec merge_code c1 c2 =
  match (c1, c2) with
  | ( TypeOf { i = i1; oty = oty1; next = next1 },
      TypeOf { i = i2; oty = oty2; next = next2 } )
    when Reg.equal i1 i2 && RegTy.equal oty1 oty2 ->
      let next, len = merge_code next1 next2 in
      (TypeOf { i = i1; oty = oty1; next }, len + 1)
  | ( CheckNode { i = i1; n = n1; next = next1 },
      CheckNode { i = i2; n = n2; next = next2 } )
    when Reg.equal i1 i2 && Node.equal n1 n2 ->
      let next, len = merge_code next1 next2 in
      (CheckNode { i = i1; n = n1; next }, len + 1)
  | ( CheckTy { i = i1; n = n1; next = next1 },
      CheckTy { i = i2; n = n2; next = next2 } )
    when RegTy.equal i1 i2 && Ground.Ty.equal n1 n2 ->
      let next, len = merge_code next1 next2 in
      (CheckTy { i = i1; n = n1; next }, len + 1)
  | ( BindParent { i = i1; f = f1; o = o1; oty = oty1 },
      BindParent { i = i2; f = f2; o = o2; oty = oty2 } )
    when Reg.equal i1 i2 && Reg.equal o1 o2 && RegTy.equal oty1 oty2 ->
      let len = ref Int.max_value in
      let f =
        F_Pos.M.union
          (fun _ next1 next2 ->
            let next, l = merge_code next1 next2 in
            len := Int.min !len l;
            Some next)
          f1 f2
      in
      (BindParent { i = i1; f; o = o1; oty = oty1 }, !len + 1)
  | ( BindApp { i = i1; o = o1; oty = oty1; f = f1 },
      BindApp { i = i2; o = o2; oty = oty2; f = f2 } )
    when Reg.equal i1 i2 && Reg.equal o1 o2 && RegTy.equal oty1 oty2 ->
      let len = ref Int.max_value in

      let f =
        F.M.union
          (fun _ next1 next2 ->
            let next, l = merge_code next1 next2 in
            len := Int.min !len l;
            Some next)
          f1 f2
      in
      (BindApp { i = i1; f; o = o1; oty = oty1 }, !len + 1)
  | ( BindAnyApp { f = f1; o = o1; oty = oty1; next = next1 },
      BindAnyApp { f = f2; o = o2; oty = oty2; next = next2 } )
    when F.equal f1 f2 && Reg.equal o1 o2 && RegTy.equal oty1 oty2 ->
      let next, len = merge_code next1 next2 in
      (BindAnyApp { f = f1; o = o1; oty = oty1; next }, len + 1)
  | ( CompareTy { i1 = i11; i2 = i21; next = next1 },
      CompareTy { i1 = i12; i2 = i22; next = next2 } )
    when RegTy.equal i11 i12 && RegTy.equal i21 i22 ->
      let next, len = merge_code next1 next2 in
      (CompareTy { i1 = i11; i2 = i21; next }, len + 1)
  | ( Compare { i1 = i11; i2 = i21; next = next1 },
      Compare { i1 = i12; i2 = i22; next = next2 } )
    when Reg.equal i11 i12 && Reg.equal i21 i22 ->
      let next, len = merge_code next1 next2 in
      (Compare { i1 = i11; i2 = i21; next }, len + 1)
  | ( Yield { subst = subst1; substty = substty1; v = v1; o = o1; oty = oty1 },
      Yield { subst = subst2; substty = substty2; v = v2; o = o2; oty = oty2 } )
    when phys_equal v1 v2
         && Expr.Term.Var.M.equal Reg.equal subst1 subst2
         && Expr.Ty.Var.M.equal RegTy.equal substty1 substty2
         && Reg.equal o1 o2 && RegTy.equal oty1 oty2 ->
      ( Yield { subst = subst1; substty = substty1; v = v1; o = o1; oty = oty1 },
        100 )
  | Alternative (a1, a2), d ->
      let m1, len1 = merge_code a1 d in
      let m2, len2 = merge_code a2 d in
      if len1 = 0 && len2 = 0 then (Alternative (c1, d), 0)
      else if len1 < len2 then (Alternative (a1, m2), len2)
      else (Alternative (a2, m1), len1)
  | d, Alternative (a1, a2) ->
      let m1, len1 = merge_code d a1 in
      let m2, len2 = merge_code d a2 in
      if len1 = 0 && len2 = 0 then (Alternative (d, c2), 0)
      else if len1 < len2 then (Alternative (m2, a1), len2)
      else (Alternative (m1, a2), len1)
  | _ -> (Alternative (c1, c2), 0)

let merge_inv_path x y =
  match (x, y) with
  | Empty, _ -> y
  | _, Empty -> x
  | C x, C y ->
      let code, _ =
        if false then (Alternative (x.code, y.code), 0)
        else merge_code x.code y.code
      in
      C
        {
          code;
          max_regty = RegTy.max x.max_regty y.max_regty;
          max_reg = Reg.max x.max_reg y.max_reg;
        }

let empty_inv_path = Empty

type 'a res = 'a -> Ground.Subst.t -> unit
