(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib

(** Pattern *)

type t = private
  | Var of { var : Expr.Term.Var.t; ty : Expr.Ty.t }
  | App of F.t * Expr.Ty.t list * t IArray.t
  | Node of Node.t

include Colibri2_popop_lib.Popop_stdlib.Datatype with type t := t

exception Unconvertible

val of_term_exn : ?coercion:bool -> ?subst:Ground.Subst.t -> Expr.Term.t -> t

val init : Ground.Subst.S.t
(** Singleton set with the empty substitution, can be used as initial value for
    the following function. [Ground.Subst.empty] should not be used since the
    functions will do nothing *)

val match_ty : Ground.Subst.S.t -> Ground.Ty.t -> Expr.Ty.t -> Ground.Subst.S.t
(** [match_ty substs ty pat] match the [ty] on [pat] for each substitution of
    [substs] *)

val match_term :
  _ Egraph.t -> Ground.Subst.S.t -> Node.t -> t -> Ground.Subst.S.t
(** [match_term d substs n pat] match the node [n] on the pattern [pat] for each
    substitution. Each return substitution corresponds to one given as input. *)

val match_ground :
  _ Egraph.t -> Ground.Subst.S.t -> Ground.t -> t -> Ground.Subst.S.t
(** [match_ground d substs g pat] match the ground term [g] on the pattern [pat]
    for each substitution. Each return substitution corresponds to one given as
    input. *)

val check_ty : Ground.Subst.t -> Ground.Ty.t -> Expr.Ty.t -> bool
(** [check_ty subst ty pat] checks that the pattern [pat] substituted by [subst]
    is equal to ty *)

val check_term : _ Egraph.t -> Ground.Subst.t -> Node.t -> t -> bool
(** [check_term d subst n pat] checks the pattern [pat] substituted by [subst]
    is equal by congruence of [d] to [n] *)

val check_term_exists : _ Egraph.t -> Ground.Subst.t -> t -> Node.S.t
(** [check_term_exists d subst pat] return nodes that are equal to the pattern
    [pat] substituted by the substitution [subst] *)

val match_any_term : _ Egraph.t -> Ground.Subst.S.t -> t -> Ground.Subst.S.t
(** [match_any_term d subst pat] match the pattern [pat] to all the nodes of [d]
    for each substitution. Each return substitution corresponds to one given as
    input. *)

val get_pps : t -> t list -> PP.t list Expr.Term.Var.M.t
(** [get_pps pat pats] return for each variables the parent-parent pairs between
    position in [pat] and position in [pat] or [pats] *)

val env_ground_by_apps : Ground.t Context.Push.t F.EnvApps.t
(** The set of ground terms sorted by their top function *)

module EnvTy : Datastructure.Memo with type key := Ground.Ty.t

val env_node_by_ty : Node.t Context.Push.t EnvTy.t
(** The set of nodes sorted by their type. A node can have multiple types *)

val find_existing_app :
  'a Egraph.t ->
  Ground.Subst.t ->
  F.t ->
  Expr.ty list ->
  Node.S.t IArray.t ->
  Ground.S.t
(** [find_existing_app d s f tyl nodes_args] find existing application of [f]
    with one of the nodes as arguments *)
