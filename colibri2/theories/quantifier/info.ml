(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib
(** information added on nodes *)

type t = {
  parents : Ground.S.t F_Pos.M.t;  (** parents *)
  apps : Ground.S.t F.M.t;  (** parent parent *)
  ty : Ground.Ty.S.t;  (** same as in ground but simplify incrementalism *)
}
[@@deriving show]

let _ = pp

(** because it is currently not hidden and too long *)
let pp = Fmt.nop

let congruence_closure d ~other parent0 ~repr parent1 =
  let find_as_if cl =
    (* as if the merge is already done *)
    let cl = Egraph.find_def d cl in
    if Node.equal cl other then repr else cl
  in
  F_Pos.M.union
    (fun _ gs1 gs2 ->
      let h = Ground.Term.H.create 10 in
      let norm g =
        let g = Ground.sem g in
        { g with Ground.args = IArray.map ~f:find_as_if g.Ground.args }
      in
      let s = ref (Ground.S.union gs1 gs2) in
      let not_first g = s := Ground.S.remove g !s in
      Ground.S.iter
        (fun g ->
          Ground.Term.H.change
            (function
              | None -> Some [ Ground.node g ]
              | Some l ->
                  not_first g;
                  (* List.iter (Egraph.merge d (Ground.node g)) l; *)
                  Some (* Ground.node g ::  *) l)
            h (norm g))
        gs1;
      Ground.S.iter
        (fun g ->
          match Ground.Term.H.find_opt h (norm g) with
          | None -> ()
          | Some l ->
              not_first g;
              List.iter (Egraph.merge d (Ground.node g)) l)
        gs2;
      assert (not (Ground.S.is_empty !s));
      Some !s)
    parent0 parent1

let stats_cc = Debug.register_stats_time "congruence_closure"

let congruence_closure d ~other parent0 ~repr parent1 =
  Debug.add_time_during stats_cc (fun _ ->
      congruence_closure d ~other parent0 ~repr parent1)

let merge d ~other ~repr info info' =
  {
    parents = congruence_closure d ~other ~repr info.parents info'.parents;
    apps =
      F.M.union (fun _ a b -> Some (Ground.S.union a b)) info.apps info'.apps;
    ty = Ground.Ty.S.union info.ty info'.ty;
  }

let empty =
  { parents = F_Pos.M.empty; apps = F.M.empty; ty = Ground.Ty.S.empty }

let dom =
  Dom.Kind.create
    (module struct
      type nonrec t = t

      let name = "Quantifier.info"
    end)

let tys d n =
  Opt.get_def Ground.Ty.S.empty
  @@
  let open CCOption in
  let+ info = Egraph.get_dom d dom n in
  info.ty

module Builtin_skipped_for_trigger = struct
  let q = Datastructure.Push.create Fmt.nop "builtin_skipped_for_trigger"
  let register d f = Datastructure.Push.push q d f

  let skipped d builtin =
    match builtin with
    | Expr.And | Expr.Equal | Expr.Equiv | Expr.Or | Expr.Xor | Expr.Imply
    | Expr.Ite | Expr.Neg ->
        true
    | _ -> Datastructure.Push.exists q d ~f:(fun p -> p builtin)
end
