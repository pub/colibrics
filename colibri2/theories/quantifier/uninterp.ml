(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib
open Popop_stdlib
open Colibri2_core

(* let debug =
 *   Debug.register_info_flag ~desc:"for the uninterpreted function theory"
 *     "uninterp" *)

(* module Th = struct
 *   module T = struct
 *     let hash_fold_list = Base.hash_fold_list
 *     type t =
 *       | Fun of Expr.Term.Const.t * Ground.Ty.t list
 *       | App of Node.t * Node.t
 *     [@@ deriving eq, ord, hash]
 * 
 *     let pp fmt v =
 *       match v with
 *       | Fun (t,tyl) ->
 *         Format.fprintf fmt "%a[%a]"
 *           Expr.Term.Const.pp t
 *           (Pp.list Pp.colon Ground.Ty.pp) tyl
 *       | App (f,g) -> Format.fprintf fmt "(@[%a@],@,@[%a@])"
 *         Node.pp  f Node.pp g
 * 
 *   end
 * 
 *   include T
 *   include MkDatatype(T)
 * 
 *   let key = ThTermKind.create (module struct type nonrec t = t let name = "UF" end)
 * 
 * end
 * 
 * let sem = Th.key
 * 
 * let pp = Th.pp
 * 
 * module ThE = ThTermKind.Register(Th)
 * 
 * let app f g = Node.index_sem sem (App(f,g))
 * 
 * let appl f l =
 *   let rec aux acc = function
 *     | [] -> acc
 *     | a::l ->
 *       aux (app acc a) l
 *   in
 *   aux f l
 * 
 * (\* let result_of_sort = Ty.H.create 20
 *  * 
 *  * let register_sort ?dec ty =
 *  *   Ty.H.add result_of_sort ty dec
 *  * 
 *  * (\\** Bool can't register itself it is linked before uninterp *\\)
 *  * let () = register_sort Bool.ty ~dec:Bool.make_dec *\)
 * 
 * (\* let result_to_dec = Node.H.create 20
 *  * 
 *  * let app_fun node args =
 *  *   let (sort,arity) = Node.H.find_exn result_to_dec Impossible node in
 *  *   assert (List.length args = arity);
 *  *   let appnode = appl node args sort in
 *  *   appnode
 *  * 
 *  * let fun1 ty s =
 *  *   let f = fresh_fun ~result:ty ~arity:1 s in
 *  *   (fun x -> app_fun f [x])
 *  * 
 *  * let fun2 ty s =
 *  *   let f = fresh_fun ~result:ty ~arity:2 s in
 *  *   (fun x1 x2 -> app_fun f [x1;x2])
 *  * 
 *  * let fun3 ty s =
 *  *   let f = fresh_fun ~result:ty ~arity:3 s in
 *  *   (fun x1 x2 x3 -> app_fun f [x1;x2;x3])
 *  * 
 *  * let fun4 ty s =
 *  *   let f = fresh_fun ~result:ty ~arity:4 s in
 *  *   (fun x1 x2 x3 x4 -> app_fun f [x1;x2;x3;x4])
 *  * 
 *  * let fun5 ty s =
 *  *   let f = fresh_fun ~result:ty ~arity:5 s in
 *  *   (fun x1 x2 x3 x4 x5 -> app_fun f [x1;x2;x3;x4;x5]) *\)
 * 
 * 
 * module DaemonPropa = struct
 *   type k = ThE.t
 *   let key = Demon.Key.create "Uninterp.DaemonPropa"
 * 
 *   module Key = ThE
 *   module Data = DUnit
 *   type info = unit let default = ()
 * 
 *   let is_unborn d v =
 *     let open Demon.Key in
 *     match is_attached d key v with
 *     | SDead | SRedirected _ | SAlive () -> false
 *     | SUnborn -> true
 * 
 *   let attach d f g v =
 *     let open Demon.Create in
 *     assert (is_unborn d v);
 *     Demon.Key.attach [%here] d key v
 *       [EventChange(f,()); EventChange(g,())]
 * 
 *   let delay = Events.Immediate (\** can be not None *\)
 *   let wakeup d (nodesem:k) _ev () =
 *     match ThE.sem nodesem with
 *     | Th.Fun _ -> Demon.AliveStopped
 *     | Th.App(f,g) as v ->
 *       Debug.dprintf4 debug "[Uninterp] @[wakeup own %a v:%a@]"
 *         Node.pp (ThE.node nodesem) Th.pp v;
 *       let v' = Th.App(Egraph.find d f, Egraph.find d g) in
 *       assert (not (Th.equal v v'));
 *       let nodesem' = ThE.index v' in
 *       Egraph.set_thterm d (ThE.node nodesem) (ThE.thterm nodesem');
 *       Demon.AliveRedirected nodesem'
 * end
 * 
 * module RDaemonPropa = Demon.Key.Register(DaemonPropa)
 * 
 * module DaemonInit = struct
 *   let key = Demon.Key.create "Uninterp.DaemonInit"
 * 
 *   module Key = DUnit
 *   module Data = DUnit
 *   type info = unit let default = ()
 * 
 *   let delay = Events.Delayed_by 1
 * 
 *   let wakeup d () ev () =
 *     List.iter
 *       (function Events.Fired.EventRegSem(nodesem,()) ->
 *         begin
 *           let nodesem = ThE.coerce_thterm nodesem in
 *           let v = ThE.sem nodesem in
 *           let own = ThE.node nodesem in
 *           Debug.dprintf2 debug "[Uninterp] @[init %a@]" Th.pp v;
 *           if DaemonPropa.is_unborn d nodesem then
 *             match v with
 *             | Fun _ -> ()
 *             | App(f,g) ->
 *               Egraph.register d f; Egraph.register d g;
 *               let f' = Egraph.find d f in
 *               let g' = Egraph.find d g in
 *               if Node.equal f' f && Node.equal g' g then
 *                 DaemonPropa.attach [%here] d f g nodesem
 *               else
 *                 let v' = Th.App(f',g') in
 *                 let nodesem' = ThE.index v' in
 *                 Egraph.set_thterm d own (ThE.thterm nodesem')
 *         end
 *       | _ -> raise UnwaitedEvent
 *       ) ev;
 *     Demon.AliveReattached
 * 
 * 
 * end
 * 
 * module RDaemonInit = Demon.Key.Register(DaemonInit)
 * 
 * let converter d (t:Ground.t) =
 *   let res = Ground.node t in
 *   let reg_args args = List.iter (Egraph.register d) args in
 *   match Ground.sem t with
 *     | { app = {builtin = Dolmen_std.Expr.Base; _ } as f;tyargs;args=((_::_) as l); _ } ->
 *       let f = ThE.node (ThE.index (Fun (f,tyargs))) in
 *       reg_args l;
 *       let l,a = Lists.chop_last l in
 *       Egraph.set_thterm d res (ThE.thterm (ThE.index (App(appl f l,a))))
 *     | _ -> ()
 * 
 * let th_register env =
 *   RDaemonPropa.init env;
 *   RDaemonInit.init env;
 *   Demon.Key.attach [%here] env
 *     DaemonInit.key () [Demon.Create.EventRegSem(sem,())];
 *   Ground.register_converter env converter;
 *   ()
 * 
 * let () = Egraph.add_default_theory th_register *)

(** uninterpreted sorts *)
module USModel : sig
  module US : Value.S

  val new_value : 'a Egraph.t -> Ground.Ty.t -> Value.t Sequence.t
end = struct
  type usv = { id : DInt.t; ty : Ground.Ty.t }

  module US = Value.Register (struct
    module T = struct
      type t = usv = { id : DInt.t; ty : Ground.Ty.t }
      [@@deriving eq, ord, hash]

      let pp fmt v = Fmt.pf fmt "@us_%a_%i" Ground.Ty.pp v.ty v.id
    end

    include T
    include MkDatatype (T)

    let name = "US"
  end)

  let new_value_h = Ground.Ty.H.create 10

  let new_value _ =
    Ground.Ty.H.memo
      (fun ty ->
        Sequence.memoize
        @@ Sequence.unfold ~init:0 ~f:(fun id ->
               Some (US.nodevalue @@ US.index { id; ty }, id + 1)))
      new_value_h
end

let init_ty d =
  let is_abstract (ty : Ground.Ty.t) =
    match ty with
    | { app = { builtin = Expr.Base; _ } as sym; _ } -> (
        match Ground.Ty.definition sym with Abstract -> true | _ -> false)
    | _ -> false
  in
  Interp.Register.ty d (fun d ty ->
      if is_abstract ty then
        let q = USModel.new_value d ty in
        Some q
      else None);
  Interp.Register.node d (fun _ d n ->
      let tys = Ground.tys d n in
      if Ground.Ty.S.is_num_elt 1 tys && is_abstract (Ground.Ty.S.choose tys)
      then (
        let q = USModel.new_value d (Ground.Ty.S.choose tys) in
        let s = ref Value.S.empty in
        Equality.iter_on_value_different
          ~they_are_different:(fun _ v -> s := Value.S.add v !s)
          d n;
        let q = Sequence.filter q ~f:(fun v -> not (Value.S.mem v !s)) in
        Some (Interp.Seq.of_seq q))
      else None)

module UFModel = struct
  module FA = struct
    module T = struct
      let hash_fold_list = Base.hash_fold_list

      type t = { tc : F.t; ta : Ground.Ty.t list } [@@deriving eq, ord, hash]

      let pp fmt { tc; ta } =
        Format.fprintf fmt "%a[%a]" F.pp tc
          Fmt.(list ~sep:comma Ground.Ty.pp)
          ta
    end

    include T
    include Colibri2_popop_lib.Popop_stdlib.MkDatatype (T)
  end

  module HConst = Datastructure.Memo (FA)

  module Values = struct
    (** instantiated function symbol *)
    module T = struct
      type t = Value.t IArray.t [@@deriving eq, ord, hash]

      let pp fmt l = Fmt.(IArray.pp ~sep:comma Value.pp) fmt l
    end

    include T
    include Colibri2_popop_lib.Popop_stdlib.MkDatatype (T)
  end

  module Hargs = Context.Hashtbl (Values)

  type t = { known : Value.t Hargs.t; other : Value.t option Context.Ref.t }

  let env =
    HConst.create Fmt.nop "Uninterp.UFModel.env" (fun c _ ->
        { known = Hargs.create c; other = Context.Ref.create c None })

  let find d (g : Ground.s) lv =
    let h = HConst.find env d { tc = g.app; ta = g.tyargs } in
    Hargs.find_opt h.known lv

  let set d (g : Ground.s) lv v =
    let h = HConst.find env d { tc = g.app; ta = g.tyargs } in
    Hargs.set h.known lv v

  let get_other d (g : Ground.s) =
    let h = HConst.find env d { tc = g.app; ta = g.tyargs } in
    match Context.Ref.get h.other with
    | None ->
        let o = Sequence.hd_exn (Interp.ty d g.ty) in
        Context.Ref.set h.other (Some o);
        o
    | Some x -> x

  let on_uninterpreted_domain d g =
    let check_or_update g d _ _ =
      let interp n = Base.Option.value_exn (Egraph.get_value d n) in
      let s = Ground.sem g in
      let lv = IArray.map ~f:interp s.args in
      let v = interp (Ground.node g) in
      match find d s lv with
      | Some v' ->
          if not (Value.equal v v') then (
            Debug.dprintf8 Debug.contradiction
              "[Uninterp] The model of %a (%a) is %a and not %a" Ground.pp g
              Ground.pp g Value.pp v' Value.pp v;
            Egraph.contradiction ())
      | None -> set d s lv v
    in
    let f d g =
      DaemonFixingModel.attach_any_value d (Ground.node g) (check_or_update g)
    in
    f d g

  let init_attach_value d g =
    Interp.WatchArgs.create d on_uninterpreted_domain g
end

module V' = struct
  open Base

  module T = struct
    type t = {
      nb_arg : int;
      maps : Value.t UFModel.Values.M.t;
      other : Value.t;
    }
    [@@deriving ord, eq, hash, show]
  end

  include T
  include Colibri2_popop_lib.Popop_stdlib.MkDatatype (T)

  let name = "fun"
end

module V = Value.Register (V')

module On_uninterpreted_domain = struct
  (** For model fixing *)

  let propagate = UFModel.on_uninterpreted_domain

  let check d t =
    let interp n = Option.get (Egraph.get_value d n) in
    let s = Ground.sem t in
    let lv = IArray.map ~f:interp s.args in
    let v = interp (Ground.node t) in
    match UFModel.find d s lv with Some v' -> Value.equal v v' | None -> false

  let compute d t : Interp.compute =
    let interp n = Option.get (Egraph.get_value d n) in
    let s = Ground.sem t in
    let lv = IArray.map ~f:interp s.args in
    match UFModel.find d s lv with
    | Some v' -> Value v'
    | None -> Value (UFModel.get_other d s)

  let compute_term_const d (tc : Expr.Term.Const.t) _ : Interp.compute =
    let poly, args, result_ty = Expr.Ty.poly_sig tc.id_ty in
    if not (Base.List.is_empty poly) then NA
    else
      let h = UFModel.HConst.find UFModel.env d { tc; ta = [] } in
      let other =
        match Context.Ref.get h.other with
        | None ->
            let o =
              Sequence.hd_exn
                (Interp.ty d (Ground.Ty.convert Expr.Ty.Var.M.empty result_ty))
            in
            Context.Ref.set h.other (Some o);
            o
        | Some x -> x
      in
      let maps =
        UFModel.Hargs.fold
          (fun acc k v -> UFModel.Values.M.add k v acc)
          UFModel.Values.M.empty h.known
      in
      Value (V.nodevalue (V.index { nb_arg = List.length args; maps; other }))
end

let init_check d =
  Interp.Register.check d (fun d t ->
      match Ground.sem t with
      | { app = { builtin = Expr.Base; _ }; _ } ->
          Interp.check_of_bool @@ On_uninterpreted_domain.check d t
      | _ -> NA)

let converter d (t : Ground.t) =
  match Ground.sem t with
  | { app = { builtin = Expr.Base; _ }; args; _ } ->
      IArray.iter ~f:(Egraph.register d) args;
      UFModel.init_attach_value d t
  | _ -> ()

let th_register env =
  Ground.register_converter env converter;
  init_check env;
  init_ty env;
  Interp.Register.compute env (fun d t ->
      match Ground.sem t with
      | { app = { builtin = Expr.Base; _ }; args; _ }
        when IArray.length args >= 1 ->
          On_uninterpreted_domain.compute d t
      | _ -> NA);
  let pp _ _ fmt c = USModel.US.SD.pp fmt (USModel.US.value c) in
  Interp.Register.print_value_smt USModel.US.key pp;
  Interp.Register.print_value_smt V.key (fun d ty fmt p ->
      let print_var fmt i = Fmt.pf fmt "?x%i" i in
      let bind fmt ty (p : V.s) =
        let rec aux fmt i nb_arg (ty : Ground.Ty.t) =
          match (Ground.Ty.get_arrow ty, nb_arg) with
          | _, 0 -> ty
          | Some (arg, ret), nb_arg ->
              Fmt.pf fmt "(%a %a)" print_var i Ground.Ty.pp arg;
              aux fmt (i + 1) (nb_arg - 1) ret
          | None, _ -> assert false
        in
        aux fmt 0 p.nb_arg ty
      in
      let p = V.value p in
      Fmt.pf fmt "(lambda (";
      let ret_ty = bind fmt ty p in
      Fmt.pf fmt ") ";
      let rec aux i (ty : Ground.Ty.t) fmt args =
        match (Ground.Ty.get_arrow ty, args) with
        | Some (ty_arg, ty_ret), arg :: args ->
            Fmt.pf fmt "(= %a %a)@," print_var i
              (Interp.print_value_smt d ty_arg)
              arg;
            aux (i + 1) ty_ret fmt args
        | None, [] -> ()
        | _ -> assert false
      in
      UFModel.Values.M.iter
        (fun args ret ->
          Fmt.pf fmt "(ite (@[and %a@])(%a)" (aux 0 ty) (IArray.to_list args)
            (Interp.print_value_smt d ret_ty)
            ret)
        p.maps;
      Fmt.pf fmt "(%a)" (Interp.print_value_smt d ret_ty) p.other;
      UFModel.Values.M.iter (fun _ _ -> Fmt.pf fmt ")") p.maps;

      Fmt.pf fmt ")");
  Interp.Register.compute_not_totally_applied env (fun d t ->
      match Ground.NotTotallyApplied.sem t with
      | Cst { tc; ty } -> On_uninterpreted_domain.compute_term_const d tc ty
      | _ -> NA)

let () = Init.add_default_theory th_register
