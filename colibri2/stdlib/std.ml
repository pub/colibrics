(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2017-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    INRIA (Institut National de Recherche en Informatique et en        *)
(*           Automatique)                                                *)
(*    CNRS  (Centre national de la recherche scientifique)               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

include Std_sig

let debug =
  Debug.register_info_flag ~desc:"information about algebraic numbers"
    "algebraic-number"

let nnil = function [] -> false | _ :: _ -> true

module Poly = struct
  type (_, _, _) t =
    | Eq : ('a, 'a, [< `Eq | `IsEq | `Ord ]) t
    | Neq : (_, _, [ `IsEq ]) t
    | Gt : (_, _, [ `Ord ]) t
    | Lt : (_, _, [ `Ord ]) t

  type ('a, 'b) eq = ('a, 'b, [ `Eq ]) t
  type ('a, 'b) iseq = ('a, 'b, [ `IsEq ]) t
  type ('a, 'b) ord = ('a, 'b, [ `Ord ]) t

  exception NotEq

  let iseq (type a b) : (a, b, [< `Eq | `IsEq | `Ord ]) t -> (a, b) iseq =
    function
    | Eq -> Eq
    | _ -> Neq

  let eq (type a b) : (a, b, [< `Eq | `IsEq | `Ord ]) t -> (a, b) eq = function
    | Eq -> Eq
    | _ -> raise NotEq
end

module Goption = struct
  type (_, _) t = Some : 'a -> ('a, [ `Some ]) t | None : ('a, [ `None ]) t
end

(* Extending Q module from Zarith *)
module Q = struct
  module Arg = struct
    include Q (* Module from Zarith *)

    module Hash = struct
      module Z = struct
        type t = Z.t

        let hash_fold_t s (t : Z.t) = Base.Hash.fold_int s (Z.hash t)
      end

      type t = Q.t = { num : Z.t; den : Z.t } [@@deriving hash]
    end

    let hash = Hash.hash
    let hash_fold_t = Hash.hash_fold_t

    let pp fmt q =
      Format.(
        match Q.classify q with
        | Q.ZERO -> char fmt '0'
        | Q.INF -> string fmt "+∞"
        | Q.MINF -> string fmt "-∞"
        | Q.UNDEF -> string fmt "!undef!"
        | Q.NZERO -> Q.pp_print fmt q)
  end

  include Arg
  include Colibri2_popop_lib.Popop_stdlib.MkDatatype (Arg)

  let two = Q.of_int 2
  let ge = Q.geq
  let le = Q.leq

  let of_string_decimal =
    let dec_mark, decimal =
      let open Re in
      (* [+-]?[0-9]+([.][0-9]* )? *)
      let dec_mark, dec_part = mark (seq [ char '.'; group @@ rep digit ]) in
      ( dec_mark,
        compile @@ seq [ bos; opt (set "+-"); rep1 digit; opt @@ dec_part; eos ]
      )
    in
    fun s ->
      match Re.exec_opt decimal s with
      | None -> invalid_arg "of_string_decimal: bad format"
      | Some g ->
          if Re.Mark.test g dec_mark then (
            let s2 = Bytes.make Int.(String.length s - 1) '?' in
            let dec_start, dec_stop = Re.Group.offset g 1 in
            let dec_len = Int.(dec_stop - dec_start) in
            Bytes.blit_string s 0 s2 0 Int.(dec_start - 1);
            if not (Int.equal dec_len 0) then
              Bytes.blit_string s dec_start s2 Int.(dec_start - 1) dec_len;
            let n = Z.of_string (Bytes.unsafe_to_string s2) in
            if Int.equal dec_len 0 then Q.of_bigint n
            else Q.make n (Z.pow (Z.of_int 10) dec_len))
          else Q.of_bigint (Z.of_string s)

  let is_integer q = Z.equal Z.one q.Q.den

  let is_unsigned_integer size q =
    is_integer q && Int.(Z.sign q.Q.num >= 0) && Int.(Z.numbits q.Q.num <= size)

  let floor x = Q.of_bigint (Z.fdiv x.Q.num x.Q.den)
  let ceil x = Q.of_bigint (Z.cdiv x.Q.num x.Q.den)
  let truncate d = if Int.(Q.sign d > 0) then floor d else ceil d

  let div x y =
    assert (not (Q.equal Q.zero y));
    Q.div x y

  let make x y =
    assert (not (Z.equal Z.zero y));
    Q.make x y

  let div_e a b =
    let s = Q.sign b in
    let d = div a b in
    if Int.(s > 0) then floor d else ceil d

  let div_t a b = truncate (div a b)
  let div_f a b = floor (div a b)
  let mod_e a b = Q.sub a (Q.mul (div_e a b) b)
  let mod_t a b = Q.sub a (Q.mul (div_t a b) b)
  let mod_f a b = Q.sub a (Q.mul (div_f a b) b)
  let is_zero c = Int.equal (Q.sign c) 0
  let none_zero c = if is_zero c then None else Some c
  let is_not_zero c = not (Int.equal (Q.sign c) 0)

  let gen =
    let open QCheck2.Gen in
    let+ num = small_signed_int and+ den = small_nat in
    Q.make (Z.of_int num) (Z.of_int Int.(den + 1))

  let pow q n =
    if Int.(n < 0) then (
      assert (Int.(Q.sign q <> 0));
      Q.make (Z.pow q.den Int.(-n)) (Z.pow q.num Int.(-n)))
    else Q.make (Z.pow q.num n) (Z.pow q.den n)
end

module A = struct
  let ctx = Calcium.CTX.mk ()

  module T = struct
    module A = struct
      include Calcium.CA

      let hash_fold_t s t = Base.Hash.fold_int s (hash ~ctx t)
    end

    type t = Q of Q.t | A of A.t [@@deriving hash]

    let to_a = function Q q -> A.of_q ~ctx q | A a -> a

    let compare a b =
      match (a, b) with
      | Q a, Q b -> Q.compare a b
      | A a, Q b -> A.compare_q ~ctx a b
      | Q a, A b -> -A.compare_q ~ctx b a
      | A a, A b -> A.compare ~ctx a b

    let equal a b =
      match (a, b) with
      | Q a, Q b -> Q.equal a b
      | A a, Q b -> (not (Z.equal b.den Z.one)) && A.compare_q ~ctx a b = 0
      | Q a, A b -> (not (Z.equal a.den Z.one)) && A.compare_q ~ctx b a = 0
      | A a, A b -> A.compare ~ctx a b = 0

    let pp fmt = function Q q -> Q.pp fmt q | A a -> Calcium.CA.pp ~ctx fmt a
  end

  include T
  include Colibri2_popop_lib.Popop_stdlib.MkDatatype (T)

  let zero = Q Q.zero
  let one = Q Q.one
  let minus_one = Q Q.minus_one
  let two = Q Q.two
  let sign = function Q q -> Q.sign q | A a -> A.sign ~ctx a
  let ge a b = compare a b >= 0
  let gt a b = compare a b > 0
  let le a b = compare a b <= 0
  let lt a b = compare a b < 0
  let min a b = if lt a b then a else b
  let max a b = if gt a b then a else b
  let of_string_decimal s = Q (Q.of_string_decimal s)
  let of_string s = Q (Q.of_string s)
  let to_string = function Q q -> Q.to_string q | A a -> A.to_string ~ctx a
  let is_integer = function Q q -> Q.is_integer q | A _ -> false
  (* by normalization *)

  let is_real = function Q q -> Q.is_real q | A _ -> true
  let to_z = function Q q -> q.Q.num | A _ -> assert false
  let to_int = function Q q -> Q.to_int q | A _ -> assert false
  let inf = Q Q.inf
  let minus_inf = Q Q.minus_inf

  let normalize (a : A.t) =
    match A.to_q ~ctx a with None -> A a | Some q -> Q q

  let ( !! ) = normalize

  let is_unsigned_integer size = function
    | Q q -> Q.is_unsigned_integer size q
    | A _ -> false

  let of_q q = Q q
  let of_z z = Q (Q.of_bigint z)
  let of_int z = Q (Q.of_int z)
  let of_bigint = of_z
  let floor = function Q q -> Q (Q.floor q) | A a -> of_z (A.floor ~ctx a)
  let ceil = function Q q -> Q (Q.ceil q) | A a -> of_z (A.ceil ~ctx a)
  let prec = Int.shift_left 1 24

  let round_to_q round a =
    Q.make (round ~ctx (A.mul ~ctx (A.of_int ~ctx prec) a)) (Z.of_int prec)

  let floor_q = function Q q -> q | A a -> round_to_q A.floor a
  let ceil_q = function Q q -> q | A a -> round_to_q A.ceil a

  let truncate = function
    | Q q -> Q (Q.truncate q)
    | A a -> of_z (A.truncate ~ctx a)

  let neg = function Q q -> Q (Q.neg q) | A a -> normalize (A.neg ~ctx a)
  let inv = function Q q -> Q (Q.inv q) | A a -> normalize (A.inv ~ctx a)
  let abs = function Q q -> Q (Q.abs q) | A a -> normalize (A.abs ~ctx a)

  let combine2 name fq fa cv a b =
    match (a, b) with
    | Q a, Q b -> Q (fq a b)
    | _ ->
        let r = cv (fa ~ctx (to_a a) (to_a b)) in
        Debug.dprintf7 debug "[A] %s %a %a is %a" name pp a pp b pp r;
        r

  (* todo special case for one, zero, ... *)
  let div = combine2 "div" Q.div A.div normalize
  let add = combine2 "add" Q.add A.add normalize
  let sub = combine2 "sub" Q.sub A.sub normalize
  let mul = combine2 "mul" Q.mul A.mul normalize
  let ( + ) = add
  let ( - ) = sub
  let ( ~- ) = neg
  let ( ~+ ) x = x
  let ( * ) = mul
  let ( / ) = div
  let div_e = combine2 "div_e" Q.div_e A.div_e of_z
  let div_t = combine2 "div_t" Q.div_t A.div_t of_z
  let div_f = combine2 "div_f" Q.div_f A.div_f of_z
  let mod_e = combine2 "mod_e" Q.mod_e A.mod_e normalize
  let mod_t = combine2 "mod_t" Q.mod_t A.mod_t normalize
  let mod_f = combine2 "mod_f" Q.mod_f A.mod_f normalize
  let is_zero c = match c with Q q -> Q.is_zero q | A _ -> false
  (* by normalization *)

  let none_zero c = if is_zero c then None else Some c
  let is_not_zero c = not (Int.equal (sign c) 0)

  (* let a_gen : A.t QCheck.Gen.t = QChecl.Gen.empty
   *   let open QCheck.Gen in
   *   if false then
   *     let rec aux s =
   *       let coefs = small_list small_signed_int s in
   *       let u = Ocaml_poly.UPolynomial.construct (List.map Z.of_int coefs) in
   *       let roots = Ocaml_poly.UPolynomial.roots_isolate u in
   *       match roots with [] -> aux s | _ -> oneofl roots s
   *     in
   *     aux
   *   else fun s ->
   *     let a = (triple small_signed_int small_nat small_nat) s in
   *     let b = (triple small_signed_int small_nat small_nat) s in
   *     let coefs = [ a; b ] in
   *     List.fold_left
   *       (fun acc (p, c, r) ->
   *         A.add acc
   *           (A.mul (A.of_int p) (A.positive_root (A.of_int c) Int.(r + 1))))
   *       (A.zero ()) coefs *)

  let gen =
    let open QCheck2.Gen in
    frequency
      [ (999, map (fun q -> Q q) Q.gen) (* ; (1, map (fun a -> !!a) a_gen) *) ]

  let pow q n =
    match q with Q q -> Q (Q.pow q n) | A a -> !!(A.pow_int ~ctx a n)

  let positive_root q n =
    Debug.dprintf3 debug "[A] %i root computed: %a" n pp q;
    if n = 0 then one else !!(A.pow ~ctx (to_a q) (Q.make Z.one (Z.of_int n)))

  let positive_pow q n =
    let r = !!(A.pow ~ctx (to_a q) n) in
    Debug.dprintf6 debug "[A] %a pow computed: %a is %a" Q.pp n pp q pp r;
    r
end

(* let () = Ocaml_poly.PolyUtils.trace_enable "algebraic_number" *)

module Sequence = struct
  include Base.Sequence

  (** Fix interleave bad behavior *)
  let interleave s1 =
    let rec next (todo_stack, done_stack, s1) =
      match todo_stack with
      | s2 :: todo_stack -> (
          match Base.Sequence.Expert.next_step s2 with
          | Yield { value = x; state = s2 } ->
              Base.Sequence.Step.Yield
                { value = x; state = (todo_stack, s2 :: done_stack, s1) }
          | Skip { state = s2 } ->
              Skip { state = (todo_stack, s2 :: done_stack, s1) }
          | Done -> Skip { state = (todo_stack, done_stack, s1) })
      | [] -> (
          match (Base.Sequence.Expert.next_step s1, done_stack) with
          | Yield { value = t; state = s1 }, _ ->
              (* The original implementation added a skip here,
                 which delayed the yield. Many interleave one after another has a very bad complexity *)
              next (List.rev (t :: done_stack), [], s1)
          | Skip { state = s1 }, _ ->
              Skip { state = (List.rev done_stack, [], s1) }
          | Done, _ :: _ -> Skip { state = (List.rev done_stack, [], s1) }
          | Done, [] -> Done)
    in
    let state = ([], [], s1) in
    Base.Sequence.unfold_step ~init:state ~f:next

  let interleaved_cartesian_product s1 s2 =
    Base.Sequence.map s1 ~f:(fun x1 ->
        Base.Sequence.map s2 ~f:(fun x2 -> (x1, x2)))
    |> interleave

  let ( let+ ) x y = Base.Sequence.( >>| ) x y
  let ( let* ) t f = Base.Sequence.bind t ~f
  let ( and* ) x y = interleaved_cartesian_product x y
  let concat = interleave
end
