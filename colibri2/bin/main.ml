(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

(* This file is free software, copied from dolmen. See file "LICENSE" for more information *)

let stat_runtime = Colibri2_core.Debug.register_stats_time "runtime"

let () =
  let man =
    [
      `S Options.common_section;
      `P "Common options for the colibrics binary";
      `S Options.gc_section;
      `P "Options to fine-tune the gc, only experts should use these.";
      `S Cmdliner.Manpage.s_bugs;
      `P "You can report bugs at https://git.frama-c.com/";
      `S Cmdliner.Manpage.s_authors;
      `P "Francois Bobot <francois.bobot@cea.fr>";
    ]
  in
  let version =
    match Build_info.V1.version () with
    | None -> "n/a"
    | Some v -> Build_info.V1.Version.to_string v
  in
  let info = Cmdliner.Cmd.info ~man ~version "Colibri2" in
  let theories = Colibri2_core.ForSchedulers.default_theories () in
  let cli_term = Cmdliner.Cmd.v info (Options.state theories) in
  let st, show_steps, check_status, show_check_sat_result =
    match Cmdliner.Cmd.eval_value cli_term with
    | Ok (`Version | `Help) -> exit 0
    | Error (`Parse | `Term | `Exn) -> exit 2
    | Ok (`Ok opt) -> opt
  in
  try
    Fun.protect ~finally:(fun () -> Trace.shutdown ()) @@ fun () ->
    Colibri2_core.Debug.add_time_during stat_runtime @@ fun _ ->
    Colibri2_solver.Input.read ~show_steps
      ~set_true:Colibri2_theories_bool.Boolean.set_true
      ~handle_exn:Colibri2_solver.Input.handle_exn_with_exit st ~check_status
      ~show_check_sat_result
  with _exn -> Format.eprintf "here@."
