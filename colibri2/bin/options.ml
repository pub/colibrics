(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2017-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*    INRIA (Institut National de Recherche en Informatique et en        *)
(*           Automatique)                                                *)
(*    CNRS  (Centre national de la recherche scientifique)               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

(* This file is free software, part of dolmen. See file "LICENSE" for more information *)

open Cmdliner

(* Sections *)
(* ************************************************************************* *)

let gc_section = "GC OPTIONS"
let common_section = Manpage.s_options

(* State creation *)
(* ************************************************************************* *)

let gc_opts minor_heap_size major_heap_increment space_overhead max_overhead
    allocation_policy =
  Gc.
    {
      (get ()) with
      minor_heap_size;
      major_heap_increment;
      space_overhead;
      max_overhead;
      allocation_policy;
    }

let mk_state theories gc gc_opt bt colors time_limit size_limit lang mode input
    header_check header_licenses header_lang_version type_check debug max_warn
    step_limit debug_flags show_steps check_status dont_print_result learning
    limit_last_effort print_success negate_goal trace options =
  Option.iter Colibri2_stdlib.Tracing.setup trace;
  let last_effort_limit =
    match limit_last_effort with
    | None -> None
    | Some (`Absolute_time f) -> Some (Colibri2_solver.Scheduler.Time f)
    | Some (`Absolute_steps i) -> Some (Colibri2_solver.Scheduler.Steps i)
    | Some (`Relative_time f) ->
        Some (Colibri2_solver.Scheduler.Time (f *. time_limit))
    | Some (`Relative_steps f) -> (
        match step_limit with
        | None -> None
        | Some step_limit ->
            Some
              (Colibri2_solver.Scheduler.Steps
                 (int_of_float (f *. float step_limit))))
  in
  List.iter Colibri2_stdlib.Debug.set_flag_s debug_flags;
  Colibri2_stdlib.Debug.set_info_flags debug;
  ( Colibri2_solver.Input.mk_state ~gc ~gc_opt ~bt ~colors ~time_limit
      ~size_limit ?lang ?mode ~input ~header_check ~header_licenses
      ~header_lang_version ~type_check ~debug ~max_warn theories ~learning
      ?step_limit ?last_effort_limit ~print_success ~negate_goal ~options,
    show_steps,
    check_status,
    not dont_print_result )

(* Initialise error codes *)
(* ************************************************************************* *)

let () =
  Dolmen_loop.Code.init ~full:true
    [
      (Dolmen_loop.Code.generic, 1);
      (Dolmen_loop.Code.limit, 2);
      (Dolmen_loop.Code.parsing, 3);
      (Dolmen_loop.Code.typing, 4);
      (Dolmen_loop.Headers.code, 5);
      (Dolmen_loop.Flow.code, 7);
    ]

(* Input source converter *)
(* ************************************************************************* *)

(* Converter for input formats/languages *)
let input_format_conv = Arg.enum Dolmen_loop.Logic.enum

(* Converter for input file/stdin *)
let input_to_string = function
  | `Stdin -> "<stdin>"
  | `Raw _ -> "<raw>"
  | `File f -> f

let input_source_conv =
  let parse x = Ok (`File x) in
  let print fmt i = Format.fprintf fmt "%s" (input_to_string i) in
  Arg.conv (parse, print)

(* Converter for input modes *)
let mode_conv = Arg.enum [ ("full", `Full); ("incremental", `Incremental) ]

(* Output converters *)
(* ************************************************************************* *)

let output_to_string = function `Stdout -> "<stdout>" | `File f -> f
let parse_output = function "stdout" -> Ok `Stdout | f -> Ok (`File f)

let output_conv =
  let print fmt o = Format.fprintf fmt "%s" (output_to_string o) in
  Arg.conv (parse_output, print)

(* Argument converter for integer with multiplier suffix *)
(* ************************************************************************ *)

let nb_sec_minute = 60
let nb_sec_hour = 60 * nb_sec_minute
let nb_sec_day = 24 * nb_sec_hour

let time_string f =
  let n = int_of_float f in
  let aux n div = (n / div, n mod div) in
  let n_day, n = aux n nb_sec_day in
  let n_hour, n = aux n nb_sec_hour in
  let n_min, n = aux n nb_sec_minute in
  let print_aux s n = if n <> 0 then string_of_int n ^ s else "" in
  print_aux "d" n_day ^ print_aux "h" n_hour ^ print_aux "m" n_min
  ^ print_aux "s" n

let print_time fmt f = Format.fprintf fmt "%s" (time_string f)

let parse_time arg =
  let l = String.length arg in
  let multiplier m =
    let arg1 = String.sub arg 0 (l - 1) in
    `Ok (m *. float_of_string arg1)
  in
  assert (l > 0);
  try
    match arg.[l - 1] with
    | 's' -> multiplier 1.
    | 'm' -> multiplier (float nb_sec_minute)
    | 'h' -> multiplier (float nb_sec_hour)
    | 'd' -> multiplier (float nb_sec_day)
    | '0' .. '9' -> `Ok (float_of_string arg)
    | _ -> `Error "bad numeric argument"
  with Failure _ -> `Error "bad numeric argument"

let parse_limit_last_effort =
  let rint = Re.(group @@ rep1 digit) in
  let rfloat =
    Re.(group @@ seq [ rep1 digit; opt (seq [ char '.'; rep digit ]) ])
  in
  let relative unit = Re.(whole_string @@ seq [ rfloat; char '%'; str unit ]) in
  let absolute_steps = Re.(whole_string @@ seq [ rint; str "steps" ]) in
  let absolute_time = Re.(whole_string @@ seq [ rfloat; group (set "smhd") ]) in
  let all =
    Re.(
      compile
      @@ alt
           [ relative "time"; relative "steps"; absolute_steps; absolute_time ])
  in
  fun arg ->
    match Re.exec_opt all arg with
    | None -> `Error "Bad last effort limit format"
    | Some g -> (
        match Re.Group.get_opt g 1 with
        | Some s -> `Ok (`Relative_time (float_of_string s /. 100.))
        | None -> (
            match Re.Group.get_opt g 2 with
            | Some s -> `Ok (`Relative_steps (float_of_string s /. 100.))
            | None -> (
                match Re.Group.get_opt g 3 with
                | Some s -> `Ok (`Absolute_steps (int_of_string s))
                | None -> (
                    match Re.Group.get_opt g 4 with
                    | Some s ->
                        let coef =
                          match Re.Group.get g 5 with
                          | "s" -> 1.
                          | "m" -> float nb_sec_minute
                          | "h" -> float nb_sec_hour
                          | "d" -> float nb_sec_day
                          | _ -> assert false
                        in
                        `Ok (`Absolute_time (float_of_string s *. coef))
                    | None -> `Error "Bad last effort limit format"))))

let print_limit_last_effort : _ Arg.printer =
 fun fmt -> function
  | `Relative_time f -> Fmt.pf fmt "%f%%time" f
  | `Relative_steps f -> Fmt.pf fmt "%f%%steps" f
  | `Absolute_time f -> Fmt.pf fmt "%fs" f
  | `Absolute_steps i -> Fmt.pf fmt "%isteps" i

let size_string f =
  let n = int_of_float f in
  let aux n div = (n / div, n mod div) in
  let n_giga, n = aux n 1_000_000_000 in
  let n_mega, n = aux n 1_000_000 in
  let n_kilo, n = aux n 1_000 in
  let print_aux s n = if n <> 0 then string_of_int n ^ s else "" in
  print_aux "Go" n_giga ^ print_aux "Mo" n_mega ^ print_aux "ko" n_kilo
  ^ print_aux "" n

let print_size fmt f = Format.fprintf fmt "%s" (size_string f)

let parse_size arg =
  let l = String.length arg in
  let multiplier m =
    let arg1 = String.sub arg 0 (l - 1) in
    `Ok (m *. float_of_string arg1)
  in
  assert (l > 0);
  try
    match arg.[l - 1] with
    | 'k' -> multiplier 1e3
    | 'M' -> multiplier 1e6
    | 'G' -> multiplier 1e9
    | 'T' -> multiplier 1e12
    | '0' .. '9' -> `Ok (float_of_string arg)
    | _ -> `Error "bad numeric argument"
  with Failure _ -> `Error "bad numeric argument"

let c_time = (parse_time, print_time)
let c_size = (parse_size, print_size)
let c_limit_last_effort = (parse_limit_last_effort, print_limit_last_effort)

(* Gc Options parsing *)
(* ************************************************************************* *)

let gc_t =
  let docs = gc_section in
  let minor_heap_size =
    let doc = "Set Gc.minor_heap_size" in
    Arg.(value & opt int 1_000_000 & info [ "gc-s" ] ~doc ~docs)
  in
  let major_heap_increment =
    let doc = "Set Gc.major_heap_increment" in
    Arg.(value & opt int 100 & info [ "gc-i" ] ~doc ~docs)
  in
  let space_overhead =
    let doc = "Set Gc.space_overhead" in
    Arg.(value & opt int 200 & info [ "gc-o" ] ~doc ~docs)
  in
  let max_overhead =
    let doc = "Set Gc.max_overhead" in
    Arg.(value & opt int 500 & info [ "gc-O" ] ~doc ~docs)
  in
  let allocation_policy =
    let doc = "Set Gc.allocation policy" in
    Arg.(value & opt int 0 & info [ "gc-a" ] ~doc ~docs)
  in
  Term.(
    const gc_opts $ minor_heap_size $ major_heap_increment $ space_overhead
    $ max_overhead $ allocation_policy)

(* Main Options parsing *)
(* ************************************************************************* *)

let state theories =
  let docs = common_section in
  let gc =
    let doc = "Print statistics about the gc upon exiting" in
    Arg.(value & flag & info [ "g"; "gc" ] ~doc ~docs)
  in
  let bt =
    let doc = "Enables printing of backtraces." in
    Arg.(value & flag & info [ "b"; "backtrace" ] ~doc ~docs)
  in
  let colors =
    let doc = "Activate coloring of output" in
    Arg.(value & opt bool true & info [ "color" ] ~doc ~docs)
  in
  let time =
    let doc =
      "Stop the program after a time lapse of $(docv).\n\
      \                 Accepts usual suffixes for durations : s,m,h,d.\n\
      \                 Without suffix, default to a time in seconds."
    in
    Arg.(
      value & opt c_time infinity & info [ "t"; "time" ] ~docv:"TIME" ~doc ~docs)
  in
  let size =
    let doc =
      "Stop the program if it tries and use more the $(docv) memory space. "
      ^ "Accepts usual suffixes for sizes : k,M,G,T. "
      ^ "Without suffix, default to a size in octet."
    in
    Arg.(
      value & opt c_size infinity & info [ "s"; "size" ] ~docv:"SIZE" ~doc ~docs)
  in
  let in_lang =
    let doc =
      Format.asprintf "Set the input language to $(docv) (%s)."
        (Arg.doc_alts_enum ~quoted:false Dolmen_loop.Logic.enum)
    in
    Arg.(
      value
      & opt (some input_format_conv) None
      & info [ "i"; "input"; "lang" ] ~docv:"INPUT" ~doc ~docs)
  in
  let in_mode =
    let doc =
      Format.asprintf
        "Set the input mode. the full mode parses the entire file before \
         iterating\n\
        \         over its contents whereas the incremental mode processes \
         each delcaration\n\
        \         before parsing the next one. Default is incremental mode."
    in
    Arg.(value & opt (some mode_conv) None & info [ "m"; "mode" ] ~doc ~docs)
  in
  let input =
    let doc =
      "Input problem file. If no file is specified,\n\
      \               Colibri2 will enter interactive mode and read on stdin."
    in
    Arg.(value & pos 0 input_source_conv `Stdin & info [] ~docv:"FILE" ~doc)
  in
  let header_check =
    let doc =
      "If true, then the presence of headers will be checked in the\n\
      \               input file (and errors raised if they are not present)."
    in
    Arg.(value & opt bool false & info [ "check-headers" ] ~doc ~docs)
  in
  let header_licenses =
    let doc =
      "Set the allowed set of licenses in the headers.\n\
      \               An empty list means allow everything."
    in
    Arg.(value & opt (list string) [] & info [ "header-licenses" ] ~doc ~docs)
  in
  let header_lang_version =
    let doc =
      "Set the only allowed language verison for headers. If not set,\n\
      \               all conforming version numbers are allowed."
    in
    Arg.(
      value & opt (some string) None & info [ "header-lang-version" ] ~docs ~doc)
  in
  let typing =
    let doc =
      "Decide whether to type-check input expressions. If false, only parsing\n\
      \               is done. "
    in
    Arg.(value & opt bool true & info [ "type" ] ~doc ~docs)
  in
  (*
  let locs =
    let doc = "Whether to keep location information during typing. \
               Setting this to true results in better warning/error \
               messages, but will use more memory when running." in
    Arg.(value & opt bool true & info ["locs"] ~doc ~docs)
  in
  *)
  let debug =
    let doc =
      Format.asprintf
        "Print the parsed dolmen statement (after expansion of includes)"
    in
    Arg.(value & flag & info [ "debug" ] ~docs ~doc)
  in
  let max_warn =
    let doc =
      Format.asprintf
        "Maximum number of warnings to display (excess warnings will be\n\
        \         counted and a count of silenced warnings repoted at the end)."
    in
    Arg.(value & opt int max_int & info [ "max-warn" ] ~docs ~doc)
  in
  let step_limit =
    let doc = Format.asprintf "Maximum number of steps." in
    Arg.(value & opt (some int) None & info [ "max-steps" ] ~docs ~doc)
  in
  let show_steps =
    let doc = Format.asprintf "Show the number of steps after solving." in
    Arg.(value & flag & info [ "show-steps" ] ~docs ~doc)
  in
  let debug_flags =
    let doc =
      Format.asprintf
        "Debug flags. The --debug-flag=foo option, or more generally \
         --debug-flag=foo:level, activates flag 'foo' for the given level \
         (empty means 'info'). Level is case-insensitive and can be a full \
         word or a single-letter abbreviation among (d)ebug, (i)nfo (n)otice, \
         (w)arn(ing), (e)rror and (q)uiet. Levels display all the messages of \
         that level or higher: for example the 'warning' level prints both \
         warning and error messages, but not info or debug messages. The value \
         of all flags are set to 'notice' unless specified."
    in
    Arg.(value & opt_all string [] & info [ "debug-flag" ] ~docs ~doc)
  in
  let check_status =
    let doc =
      Format.asprintf
        "Check the answer using smtlib (:status) or colibri (:colibri2-status) \
         information. The check differ with unknown, in the first case any \
         answer is accepted in the second case only unknown."
    in
    Arg.(
      value
      & opt
          (enum
             [
               ("smtlib", `SMTLIB);
               ("colibri2", `COLIBRI2);
               ("sat", `Sat);
               ("unsat", `Unsat);
               ("unknown", `Unknown);
               ("steplimitreached", `StepLimitReached);
               ("no", `No);
             ])
          `No
      & info [ "check-status" ] ~docs ~doc)
  in
  let dont_print_result =
    let doc =
      Format.asprintf
        "Check the answer using smtlib (:status) or colibri (:colibri2-status) \
         information. The check differ with unknown, in the first case any \
         answer is accepted in the second case only unknown."
    in
    Arg.(value & flag & info [ "dont-print-result" ] ~docs ~doc)
  in
  let learning =
    let doc = Format.asprintf "Activate learning." in
    Arg.(
      value & flag
      & info
          ~env:(Cmd.Env.info ~doc "COLIBRI2_LEARNING")
          [ "learning" ] ~docs ~doc)
  in
  let print_success =
    let doc = Format.asprintf "Activate printing for success." in
    Arg.(value & flag & info [ "print-success" ] ~docs ~doc)
  in
  let last_effort_limit =
    let doc =
      "Deactivate last effort after the given time ( 90%steps, 90%time, \
       200steps, 200s )"
    in
    Arg.(
      value
      & opt (some c_limit_last_effort) None
      & info [ "limit-last-effort" ] ~docs ~doc)
  in
  let negate_goal =
    let doc =
      Format.asprintf "Negate assertions marked with (! .. :colibri2 goal)."
    in
    Arg.(value & flag & info [ "negate-goal" ] ~docs ~doc)
  in
  let trace =
    let doc = "Output tef json trace in the given file" in
    Arg.(value & opt (some string) None & info [ "trace" ] ~docs ~doc)
  in
  let other_options = Colibri2_core.ForSchedulers.Options.parser () in
  Term.(
    const (mk_state theories)
    $ gc $ gc_t $ bt $ colors $ time $ size $ in_lang $ in_mode $ input
    $ header_check $ header_licenses $ header_lang_version $ typing $ debug
    $ max_warn $ step_limit $ debug_flags $ show_steps $ check_status
    $ dont_print_result $ learning $ last_effort_limit $ print_success
    $ negate_goal $ trace $ other_options)
