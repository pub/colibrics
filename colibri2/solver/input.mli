(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

type ctx
type state = Dolmen_loop.State.t

val handle_exn_with_error : 'a -> Printexc.raw_backtrace -> exn -> 'b
val handle_exn_with_exit : state -> Printexc.raw_backtrace -> exn -> 'b
val get_ctx : state -> ctx

val get_state :
  state ->
  [ `Sat of Egraph.wt
  | `Unknown of Egraph.wt
  | `Search
  | `UnknownUnsat
  | `Unsat
  | `StepLimitReached ]

val read :
  ?show_check_sat_result:bool ->
  ?show_steps:bool ->
  ?check_status:
    [< `COLIBRI2
    | `No
    | `SMTLIB
    | `Sat
    | `StepLimitReached
    | `Unknown
    | `Unsat > `No ] ->
  set_true:(Egraph.wt -> Node.t -> unit) ->
  handle_exn:(state -> Printexc.raw_backtrace -> exn -> unit) ->
  state ->
  unit

val mk_state :
  ?gc:bool ->
  ?gc_opt:Gc.control ->
  ?bt:bool ->
  ?colors:bool ->
  ?time_limit:float ->
  ?size_limit:float ->
  ?step_limit:int ->
  ?lang:Dolmen_loop.Logic.language ->
  ?mode:[ `Full | `Incremental ] ->
  input:[< `File of string | `Stdin ] ->
  ?header_check:bool ->
  ?header_licenses:string list ->
  ?header_lang_version:string option ->
  ?type_check:bool ->
  ?debug:bool ->
  ?max_warn:int ->
  ?learning:bool ->
  ?last_effort_limit:Scheduler.last_effort_limit ->
  ?print_success:bool ->
  ?negate_goal:bool ->
  ?options:ForSchedulers.Options.options ->
  (Egraph.wt -> unit) list ->
  state
