(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

open Colibri2_popop_lib
open Colibri2_core
open ForSchedulers

let debug =
  Debug.register_info_flag ~desc:"for the scheduler in the simple version"
    "sched"

let debug_verbose =
  Debug.register_flag ~desc:"for the scheduler in the simple version"
    "sched.verbose"

let debug_queue =
  Debug.register_info_flag ~desc:"for the scheduler queues" "sched_queue"

let debug_pushpop =
  Debug.register_info_flag ~desc:"for the scheduler push/pop" "sched_pushpop"

let debug_learn =
  Debug.register_info_flag ~desc:"for the scheduler learning" "sched_learn"

let debug_dotgui =
  Debug.register_flag ~desc:"print graph at interesting time (push/pop)"
    "sched_dotgui"

let var_decay = 1. /. 0.95
let stats_propa = Debug.register_stats_int "Scheduler.daemon"
let stats_lasteffort = Debug.register_stats_int "Scheduler.lasteffort"

let stats_lasteffort_propa =
  Debug.register_stats_int "Scheduler.lasteffort_propa"

let stats_dec = Debug.register_stats_int "Scheduler.decision"
let stats_con = Debug.register_stats_int "Scheduler.conflict"
let stats_con_fix = Debug.register_stats_int "Scheduler.conflict_fix_model"
let stats_step = Debug.register_stats_int "Scheduler.step"
let stats_fix_model = Debug.register_stats_int "Scheduler.fix_model"
let stats_fix_model_cut = Debug.register_stats_int "Scheduler.fix_model_cut"

let nodec_option =
  Colibri2_core.Options.register ~pp:Fmt.bool "Scheduler.nodec"
    Cmdliner.Arg.(
      value & flag
      & info [ "no-dec" ]
          ~doc:"Make no decisions, only propagation and model search (fixmodel)")

let no_fixmodel_option =
  Colibri2_core.Options.register ~pp:Fmt.bool "Scheduler.fixmodel"
    Cmdliner.Arg.(
      value & flag
      & info [ "no-fix-model" ] ~doc:"Stop at the start of FixModel")

let cut_fixmodel_option =
  Colibri2_core.Options.register ~pp:Fmt.int "Scheduler.cutfixmodel"
    Cmdliner.Arg.(
      value & opt int 1000
      & info [ "cut-fix-model" ]
          ~doc:
            "Stop FixModel after the given number of conflict (lose the \
             possibility to answer unsat)")

let fixed_choice_order_option =
  Colibri2_core.Options.register_flag "fixed-choice-order"

let somewhat_useful_bias =
  Colibri2_core.Options.register_int "somewhat-useful-bias" 0
    ~doc:"deactivated if bias <= 0"

let sort_last_effort = Colibri2_core.Options.register_flag "sort-last-effort"

let first_restart =
  Colibri2_core.Options.register_int "first-restart" 0
    ~doc:"step of the first restart. Only one is done currently"

exception Contradiction
exception ReachStepLimit

type last_effort_limit = Time of float | Steps of int

module Prio = struct
  module S = struct
    type t = Choice.t

    let prio v = float_of_int v.Choice.prio
  end

  module Key = struct
    type t = { id : int; v : S.t }

    let pp fmt v = Fmt.int fmt v.id

    let equal t1 t2 =
      match (t1.v.key, t2.v.key) with
      | Some n1, Some n2 -> Node.equal n1 n2
      | _ -> Int.equal t1.id t2.id

    let hash t1 =
      match t1.v.key with Some n1 -> Node.hash n1 | _ -> Base.Int.hash t1.id
  end

  module HKey = Popop_stdlib.XHashtbl.Make (Key)

  module Att = struct
    type t = Key.t
    type prio = float

    let equal_prio = ( = )

    type db = float HKey.t

    let le (x : t) (xp : float) (y : t) (yp : float) =
      if xp = yp then x.id <= y.id else xp >= yp

    let reprio db x =
      let skew = HKey.find_def db 1. x in
      S.prio x.v +. skew
  end

  module P = Leftistheap.Make (Att)

  type t = {
    mutable next : int;
    db : float HKey.t;
    prio : P.t Context.Ref.t;
    size : int Context.Ref.t;
  }

  let get_prio t x = HKey.find_def t.db 1. x

  let reprio_is_changed t =
    let prio_old = Context.Ref.get t.prio in
    let prio = P.reprio t.db prio_old in
    if Base.phys_equal prio_old prio then false
    else (
      Context.Ref.set t.prio prio;
      true)

  let reprio t = ignore (reprio_is_changed t)

  let change t x coef =
    HKey.change
      (function None -> Some (1. +. coef) | Some v -> Some (coef +. v))
      t.db x

  let insert t v =
    Context.Ref.set t.size (Context.Ref.get t.size + 1);
    let x = { Key.v; id = t.next } in
    t.next <- t.next + 1;
    Context.Ref.set t.prio (P.insert t.db x (Context.Ref.get t.prio))

  let extract_min t =
    match P.extract_min (Context.Ref.get t.prio) with
    | x, prio ->
        Context.Ref.set t.prio prio;
        Context.Ref.set t.size (Context.Ref.get t.size - 1);
        Some x
    | exception Leftistheap.Empty -> None

  let size t = Context.Ref.get t.size

  let create c =
    {
      next = 0;
      db = HKey.create 10;
      prio = Context.Ref.create c P.empty;
      size = Context.Ref.create c 0;
    }
end

module LearnSet = Set.Make (struct
  type t = Ground.t

  let compare = compare
end)

type bp_kind =
  | Choices of {
      last : Egraph.wt -> unit;
      todos : (Egraph.wt -> unit) Std.Sequence.t;
      chogen : Prio.Att.t option;
    }
  | Restart
  | External

type solve_step =
  | Propagate
  | FixModel
  | BacktrackChoice of
      (Egraph.wt -> unit)
      * (Egraph.wt -> unit) Std.Sequence.t
      * solve_step
      * Prio.Att.t option

module LastEffort_att = struct
  type key = { v : Events.daemon_key; id : int }
  (** id: -1, contextual not in db 0: use daemon_key *)

  module DaemonWithDatatype = struct
    type t = key

    let pp fmt i = Fmt.int fmt i.id

    let has_datatype = function
      | Events.DaemonKey (xd, _, _) -> Option.is_some (Events.get_datatype xd)

    let equal x y =
      if x.id = 0 && y.id = 0 then
        match (x.v, y.v) with
        | Events.DaemonKey (xd, xv, _), Events.DaemonKey (yd, yv, _) -> (
            match Events.Dem.Eq.eq_type xd yd with
            | Std.Poly.Neq -> false
            | Std.Poly.Eq -> (
                match Events.get_datatype xd with
                | Some xm ->
                    let module X = (val xm) in
                    X.equal_runable xv yv
                | None -> false))
      else Int.equal x.id y.id

    let hash x =
      if x.id = 0 then
        match x.v with
        | Events.DaemonKey (xd, xv, _) -> (
            match Events.get_datatype xd with
            | Some xm ->
                let module X = (val xm) in
                X.hash_runable xv
            | None -> 0)
      else Base.Int.hash x.id
  end

  let pp_key fmt att =
    match att.v with
    | DaemonKey (xd, xv, _) -> (
        match Events.get_datatype xd with
        | Some xm ->
            let module X = (val xm) in
            Fmt.pf fmt "%a[%i] %a" Events.Dem.pp xd (X.hash_runable xv)
              Events.pp_daemon_key att.v
        | None -> Fmt.pf fmt "%a id:%i" Events.Dem.pp xd att.id)

  module H_DaemonWithDatatype =
    Colibri2_popop_lib.Exthtbl.Hashtbl.Make (DaemonWithDatatype)

  type t = {
    db : float H_DaemonWithDatatype.t;
    tw : key Context.TimeWheel.t;
    current : key list Context.Ref.t;
    mutable curr_id : int;
    (* last executed for which all branch are directly contradiction *)
    mutable last_effort_made_active : bool ref;
        (** last last effort executed *)
    current_last_effort : key Context.RefOpt.t;
    current_last_effort_level : int Context.Ref.t;
    sort : bool;
  }

  let is_contextual v = v.id = -1

  let change t x coef =
    if not (is_contextual x) then
      H_DaemonWithDatatype.change
        (function None -> Some (1. +. coef) | Some v -> Some (coef +. v))
        t.db x

  let get_prio t x =
    if not (is_contextual x) then H_DaemonWithDatatype.find_def t.db 1. x
    else -1.

  let create ~sort creator =
    {
      db = H_DaemonWithDatatype.create 10;
      tw = Context.TimeWheel.create creator;
      current = Context.Ref.create creator [];
      curr_id = 0;
      last_effort_made_active = ref false;
      current_last_effort = Context.RefOpt.create creator;
      current_last_effort_level = Context.Ref.create creator 0;
      sort;
    }

  let add ~contextual t att offset =
    let id =
      if contextual then -1
      else if DaemonWithDatatype.has_datatype att then 0
      else (
        t.curr_id <- t.curr_id + 1;
        t.curr_id)
    in
    let att = { v = att; id } in
    Context.TimeWheel.add t.tw att offset

  let next t =
    match Context.Ref.get t.current with
    | [] when t.sort -> (
        let rec all_at_same_time t acc =
          match Context.TimeWheel.next_at_same_time t.tw with
          | None -> acc
          | Some k -> all_at_same_time t (k :: acc)
        in
        match Context.TimeWheel.next t.tw with
        | None -> None
        | Some k ->
            let l = all_at_same_time t [ k ] in
            let cmp k1 k2 = Float.compare (get_prio t k1) (get_prio t k2) in
            let l = List.rev @@ List.fast_sort cmp l in
            let k, l = (Base.List.hd_exn l, Base.List.tl_exn l) in
            Context.Ref.set t.current l;
            Some k)
    | [] -> Context.TimeWheel.next t.tw
    | k :: l ->
        Context.Ref.set t.current l;
        Some k
end

type event =
  | Start
  | Bp of bp
  | PreChoice of { bp : event; chogen : Prio.Att.t }
  | LastEffort of { bp : event; att : LastEffort_att.key; active : bool ref }
  | StartFixModel of event

and bp = {
  pre_prev_scheduler_state : event;
  pre_backtrack_point : Context.bp;
  kind : bp_kind;
  pre_level : int;
  pre_trace : Trace.explicit_span;
}

let rec last_bp = function
  | Start -> None
  | Bp bp -> Some bp
  | PreChoice { bp } | LastEffort { bp } | StartFixModel bp -> last_bp bp

type t = {
  choices : Prio.t;
  daemons : Events.daemon_key Context.TimeWheel.t;
  lasteffort : LastEffort_att.t;
  fixing_model : Events.daemon_key list Context.Ref.t;
  mutable prev_scheduler_state : event;
  solver_state : Backtrackable.t;
  (* global *)
  var_inc : float ref;
  context : Context.context;
  mutable level : int;
  solve_step : solve_step Context.Ref.t;
  last_effort_limit : last_effort_limit option;
  mutable steps : int;
  last_effort_learnt : Events.daemon_key Context.Clicket.t;
  allow_learning : bool;  (** Learning *)
  learning_htbl : int ThTerm.H.t;
  mutable learning_tbl : bool array;
  position_in_tree : int list Context.Ref.t;
  mutable trace : Trace.explicit_span;
      (** A fix model has been stopped before completion unsat can't be reached.
      *)
  mutable cut_branch : bool;
  mutable current_fixmodel_conflict : int;
  mutable first_restart : int;  (** Negative if already done *)
}
(** To treat in the reverse order *)

let print_level fmt t =
  let nb_dec = Prio.size t.choices in
  Format.fprintf fmt "%i (dec waiting:%i)" t.level nb_dec

let get_steps t = t.steps

(* let new_handler t =
 *   if t.delayed <> None then raise NeedStopDelayed;
 *   {wakeup_daemons    = t.wakeup_daemons;
 *    prev_scheduler_state = t.prev_scheduler_state;
 *    solver_state      = S.Backtrackable.new_handle t.solver_state;
 *    learnt = t.learnt;
 *    delayed           = None;
 *    decprio = t.decprio;
 *    var_inc = t.var_inc
 *   } *)

let new_solver ~learning ?last_effort_limit
    ?(options = Options.default_options ()) () =
  let context = Context.create () in
  let creator = Context.creator context in
  let daemon_count = ref (-1) in
  let dec_count = ref (-1) in
  let t =
    {
      choices = Prio.create creator;
      daemons = Context.TimeWheel.create creator;
      lasteffort =
        LastEffort_att.create creator
          ~sort:(Options.get_from_options options sort_last_effort);
      fixing_model = Context.Ref.create creator [];
      prev_scheduler_state = Start;
      solver_state = Backtrackable.new_t creator;
      context;
      var_inc = ref 1.;
      level = 0;
      solve_step = Context.Ref.create creator Propagate;
      steps = 0;
      last_effort_learnt = Context.Clicket.create creator;
      last_effort_limit;
      allow_learning = learning;
      learning_htbl = ThTerm.H.create 32;
      learning_tbl = Array.make 0 false;
      position_in_tree = Context.Ref.create creator [ 0 ];
      trace =
        Trace.enter_manual_toplevel_span ~__FILE__ ~__LINE__ "create_solver";
      cut_branch = false;
      current_fixmodel_conflict = 0;
      first_restart = Options.get_from_options options first_restart;
    }
  in
  let sched_daemon att =
    incr daemon_count;
    (* Debug.dprintf2 debug "[Scheduler] New waiting daemon: %a"
     *   Events.pp_daemon_key att; *)
    match get_event_priority att with
    | Immediate -> assert false (* absurd *)
    | Delayed_by offset ->
        Debug.dprintf2 debug_verbose "New daemon %a@." Events.pp_daemon_key att;
        Context.TimeWheel.add t.daemons att offset
    | LastEffort offset ->
        LastEffort_att.add t.lasteffort att offset ~contextual:true
    | LastEffortUncontextual offset ->
        LastEffort_att.add t.lasteffort att offset ~contextual:false
    | FixingModel ->
        Context.Ref.set t.fixing_model (att :: Context.Ref.get t.fixing_model)
    | Delayed_by_and_FixingModel offset -> (
        match Context.Ref.get t.solve_step with
        | FixModel | BacktrackChoice (_, _, FixModel, _) ->
            Context.Ref.set t.fixing_model
              (att :: Context.Ref.get t.fixing_model)
        | Propagate | BacktrackChoice (_, _, _, _) ->
            Context.TimeWheel.add t.daemons att offset)
  and sched_decision (dec : Choice.t) =
    incr dec_count;
    Debug.dprintf3 debug "[Scheduler] New decision (%i): %a" !dec_count
      dec.print_cho ();
    Prio.insert t.choices dec
  in
  Backtrackable.set_sched ~sched_daemon ~sched_decision t.solver_state;
  let d = Backtrackable.get_delayed t.solver_state in
  Options.set_egraph d options;
  Backtrackable.delayed_stop t.solver_state;
  t

let print_position_in_tree fmtt t =
  Fmt.(list ~sep:comma int fmtt (List.rev (Context.Ref.get t.position_in_tree)))

let push ?(msg = Fmt.any "push") kind t =
  if Colibri2_stdlib.Debug.test_flag debug_dotgui then
    Backtrackable.draw_graph ~force:true t.solver_state;
  Debug.dprintf4 debug_pushpop "[Scheduler] push from %a: %a" print_level t
    print_position_in_tree t;
  let prev =
    {
      pre_prev_scheduler_state = t.prev_scheduler_state;
      pre_backtrack_point = Context.bp t.context;
      kind;
      pre_level = t.level;
      pre_trace = t.trace;
    }
  in
  t.level <- t.level + 1;
  t.prev_scheduler_state <- Bp prev;
  if Trace.enabled () then
    t.trace <-
      Trace.enter_manual_sub_span ~parent:t.trace "scheduler_state"
        ~data:(fun () -> [ ("msg", `String (Fmt.str "%a" msg ())) ])
        ~__FILE__ ~__LINE__;
  ignore (Context.push t.context);
  Context.Ref.set t.position_in_tree (0 :: Context.Ref.get t.position_in_tree);
  prev

(** Go to and remove the backtrack point *)
let pop_to t prev =
  Debug.dprintf4 debug_pushpop "[Scheduler] pop  from %a: %a" print_level t
    print_position_in_tree t;
  t.prev_scheduler_state <- prev.pre_prev_scheduler_state;
  t.level <- prev.pre_level;
  Trace.exit_manual_span t.trace;
  t.trace <- prev.pre_trace;
  Context.pop prev.pre_backtrack_point;
  Context.Ref.set t.position_in_tree
    (match Context.Ref.get t.position_in_tree with
    | [] -> [ min_int ]
    | i :: l -> (i + 1) :: l);
  if Colibri2_stdlib.Debug.test_flag debug_dotgui then
    Backtrackable.draw_graph ~force:true t.solver_state;
  Debug.dprintf4 debug_pushpop "[Scheduler] pop  to   %a: %a" print_level t
    print_position_in_tree t

(** only fix model *)
let learning t =
  let rec startup t step =
    match Context.Ref.get t.fixing_model with
    | att :: l -> (
        (match att with
        | Events.DaemonKey (_, _, Some blob) ->
            if not (ThTerm.H.mem t.learning_htbl blob) then
              ThTerm.H.add t.learning_htbl blob
                (ThTerm.H.length t.learning_htbl)
        | Events.DaemonKey (key, _, None) ->
            Debug.dprintf2 debug "[Learing] Missing fixmodel ground term for %a"
              Events.Dem.pp key);
        match
          Context.Ref.set t.fixing_model l;
          let d = Backtrackable.get_delayed t.solver_state in
          Backtrackable.run_daemon d att;
          Backtrackable.flush t.solver_state
        with
        | () -> startup t (step + 1)
        | exception Egraph.Contradiction -> step)
    | [] ->
        Debug.real_dprintf "Startup stopped after %i steps" step;
        assert false
  in

  let rec check t step =
    if step = -1 then false
    else
      match Context.Ref.get t.fixing_model with
      | att :: l ->
          Context.Ref.set t.fixing_model l;
          let run =
            match att with
            | Events.DaemonKey (_, _, Some blob) -> (
                match ThTerm.H.find_opt t.learning_htbl blob with
                | Some id -> t.learning_tbl.(id)
                | None -> false)
            | Events.DaemonKey (_, _, None) -> true
          in
          if run then
            match
              let d = Backtrackable.get_delayed t.solver_state in
              Backtrackable.run_daemon d att;
              Backtrackable.flush t.solver_state
            with
            | () -> check t (step - 1)
            | exception Egraph.Contradiction -> true
          else check t (step - 1)
      | [] -> false
  in

  match last_bp t.prev_scheduler_state with
  | None -> Debug.dprintf0 debug "[Learning] No learning to do, no decision"
  | Some prev -> (
      pop_to t prev;
      match prev.kind with
      | External | Restart ->
          Debug.dprintf0 debug "[Learning] No learning to do, no decision"
      | Choices { last; _ } as kind ->
          Debug.dprintf0 debug "[Learning] Start learning";
          let wrap f t steps =
            let bp = push ~msg:(Fmt.any "learning ") kind t in
            (try
               last (Backtrackable.get_delayed t.solver_state);
               Backtrackable.flush t.solver_state
             with Egraph.Contradiction -> raise Colibri2_stdlib.Std.Impossible);
            let r = f t steps in
            pop_to t bp;
            r
          in

          ThTerm.H.clear t.learning_htbl;
          let steps = wrap startup t 0 in

          let len = ThTerm.H.length t.learning_htbl in
          Debug.dprintf2 debug
            "[Learning] Startup done in %i steps for %i blobs" steps len;

          t.learning_tbl <- Array.make len true;

          assert (wrap check t steps);

          let set b i j =
            for k = i to j - 1 do
              t.learning_tbl.(k) <- b
            done
          in

          let rec bisect i j =
            Debug.dprintf2 debug "[Learning] Bisect %i %i" i j;
            if i + 1 = j then (
              set false i j;
              if not (wrap check t steps) then set true i j)
            else
              let ki = (i + j) / 2 in
              let kj = ki in
              set false kj j;
              if not (wrap check t steps) then (
                set true kj j;
                bisect kj j);
              set false i ki;
              if not (wrap check t steps) then (
                set true i ki;
                bisect i ki)
          in

          bisect 0 len;

          Debug.real_dprintf "[Learning] Learning";
          let blobs =
            ThTerm.H.fold
              (fun blob id acc -> (blob, id) :: acc)
              t.learning_htbl []
          in
          let blobs =
            List.sort (fun (_, id1) (_, id2) -> compare id1 id2) blobs
          in
          List.iter
            (fun (blob, id) ->
              Debug.real_dprintf "[Learning] Learning: %a is %a (%i:%b)" Node.pp
                (ThTerm.node blob) ThTerm.pp blob id t.learning_tbl.(id))
            blobs;

          ignore (push kind t);
          Debug.dprintf0 debug "[Learning] End of learning")

let conflict_analysis ~usefulness:_ ~no_learning t =
  let check_last_effort (v : LastEffort_att.key) ~msg_coef coef =
    let debug msg =
      Debug.dprintf6 debug_learn "[Learnt] %a is %s useful%s with %a"
        LastEffort_att.pp_key v msg_coef msg
        (fun fmt v ->
          Fmt.pf fmt "%03f" (LastEffort_att.get_prio t.lasteffort v))
        v
    in
    LastEffort_att.change t.lasteffort v coef;
    if LastEffort_att.get_prio t.lasteffort v >= 4. then
      if LastEffort_att.is_contextual v then
        debug " and  if uncontextual would have been learnt"
      else (
        Context.Clicket.push t.last_effort_learnt v.v;
        debug " and is now learnt")
    else debug ""
  in

  let check_last_leaf t v =
    let d = Backtrackable.get_delayed t.solver_state in
    if not (Colibri2_core.Options.get d fixed_choice_order_option) then
      Debug.dprintf4 debug_learn "[Learnt|%i] %a has a useful decision (%f)"
        v.Prio.Key.id v.Prio.Key.v.Choice.print_cho ()
        (Prio.get_prio t.choices v);
    Trace.message "useful decision" ~data:(fun () ->
        [ ("dec", `String (Fmt.str "%a" v.Prio.Key.v.Choice.print_cho ())) ]);
    Prio.change t.choices v 2.
  in
  Backtrackable.draw_graph t.solver_state;

  let rec rewind_before_fix_model t =
    match t.prev_scheduler_state with
    | Start ->
        Debug.dprintf0 debug "[Scheduler] learnt clause false at level 0";
        raise Contradiction
    | StartFixModel _ -> ()
    | Bp prev ->
        pop_to t prev;
        rewind_before_fix_model t
    | LastEffort { bp } | PreChoice { bp } ->
        t.prev_scheduler_state <- bp;
        rewind_before_fix_model t
  in

  (match Context.Ref.get t.solve_step with
  | FixModel ->
      Debug.incr stats_con_fix;
      if (not no_learning) && t.allow_learning then learning t;
      t.current_fixmodel_conflict <- t.current_fixmodel_conflict + 1;
      let d = Backtrackable.get_delayed t.solver_state in
      let cut_fixmodel_max = Colibri2_core.Options.get d cut_fixmodel_option in
      if
        cut_fixmodel_max >= 0 && t.current_fixmodel_conflict >= cut_fixmodel_max
      then (
        Debug.dprintf0 debug "[FixModel] cut short fix model";
        Debug.incr stats_fix_model_cut;
        t.cut_branch <- true;
        rewind_before_fix_model t)
  | BacktrackChoice _ | Propagate -> Debug.incr stats_con);

  t.var_inc := !(t.var_inc) *. var_decay;

  (* last effort litle push when conflict *)
  (match Context.RefOpt.get t.lasteffort.current_last_effort with
  | None -> ()
  | Some key ->
      let d = Backtrackable.get_delayed t.solver_state in
      let bias = Colibri2_core.Options.get d somewhat_useful_bias in
      if 0 < bias then
        let depth =
          t.level
          - Context.Ref.get t.lasteffort.current_last_effort_level
          + bias
        in
        let change = Float.pow 2. (Float.of_int (-depth)) in
        check_last_effort key ~msg_coef:"somewhat" change);

  let rec rewind ~first_choice =
    match t.prev_scheduler_state with
    | Start ->
        Debug.dprintf0 debug "[Scheduler] learnt clause false at level 0";
        raise Contradiction
    | PreChoice { chogen; bp } ->
        t.prev_scheduler_state <- bp;
        if first_choice then check_last_leaf t chogen;
        rewind ~first_choice:false
    | LastEffort { bp; att; active } ->
        t.prev_scheduler_state <- bp;
        if !active then check_last_effort att ~msg_coef:"" 1.;
        rewind ~first_choice
    | StartFixModel bp ->
        t.prev_scheduler_state <- bp;
        rewind ~first_choice:false
    | Bp prev -> (
        pop_to t prev;
        Prio.reprio t.choices;
        match prev.kind with
        | Choices { last = _; todos = seq; chogen } -> (
            match Std.Sequence.next seq with
            | None -> rewind ~first_choice
            | Some (choice, choices) ->
                Context.Clicket.iter t.last_effort_learnt ~f:(fun v ->
                    Debug.incr stats_lasteffort_propa;
                    let d = Backtrackable.get_delayed t.solver_state in
                    Backtrackable.run_daemon d v);
                Context.Ref.set t.solve_step
                  (BacktrackChoice
                     (choice, choices, Context.Ref.get t.solve_step, chogen)))
        | Restart | External -> raise Contradiction)
  in
  rewind ~first_choice:true

let rec try_run_dec : t -> Prio.Att.t -> unit =
 fun t chogen ->
  (* First we verify its the decision is at this point needed *)
  let choice = chogen.v in
  t.prev_scheduler_state <- PreChoice { chogen; bp = t.prev_scheduler_state };
  let d = Backtrackable.get_delayed t.solver_state in
  match
    let r = choice.choice d in
    Backtrackable.flush t.solver_state;
    r
  with
  | Choice.DecNo ->
      Debug.dprintf2 debug "Choice %a : no choice" choice.print_cho ()
  | Choice.DecTodo [] ->
      Debug.dprintf2 debug "Choice %a : no more choices" choice.print_cho ()
  | Choice.DecTodo [ todo ] -> (
      Debug.dprintf2 debug "Choice %a : only one choice" choice.print_cho ();
      try todo (Backtrackable.get_delayed t.solver_state)
      with Egraph.Contradiction ->
        Debug.dprintf2 debug_pushpop
          "[Scheduler] Contradiction when applying a uniq choice at level %a"
          print_level t;
        conflict_analysis ~usefulness:true ~no_learning:true t)
  | Choice.DecTodo (todo :: todos as l) ->
      (* We remove the decision it is replaced by the todos,
          we can change the interface of choice and in that case
          we want to keep the decision in the current branch *)
      Debug.dprintf3 debug "Choice %a : done (%i choices)" choice.print_cho ()
        (List.length l);
      Debug.incr stats_dec;
      Backtrackable.delayed_stop t.solver_state;
      make_choice (Some chogen) ~msg:choice.print_cho t todo
        (Std.Sequence.of_list todos)
  | exception Egraph.Contradiction ->
      Debug.dprintf2 debug_pushpop
        "[Scheduler] Contradiction when creating choice at level %a" print_level
        t;
      conflict_analysis ~usefulness:true ~no_learning:true t

and make_choice chogen ~msg t todo todos =
  ignore (push ~msg (Choices { last = todo; todos; chogen }) t);
  try todo (Backtrackable.get_delayed t.solver_state)
  with Egraph.Contradiction ->
    Debug.dprintf2 debug_pushpop
      "[Scheduler] Contradiction when applying choice at level %a" print_level t;
    conflict_analysis ~usefulness:true ~no_learning:true t

let[@inline always] protect_against_contradiction ~usefulness ~no_learning t f g
    =
  match f () with
  | exception Egraph.Contradiction ->
      Debug.dprintf2 debug_pushpop
        "[Scheduler] Contradiction when propagating at level %a" print_level t;
      conflict_analysis ~usefulness ~no_learning t;
      true
  | r -> g r

let run_one_step_propagation ~nodec t =
  match Context.TimeWheel.next t.daemons with
  | Some att ->
      Debug.incr stats_propa;
      protect_against_contradiction ~usefulness:true ~no_learning:false t
        (fun () ->
          let d = Backtrackable.get_delayed t.solver_state in
          Backtrackable.run_daemon d att)
        (fun () -> true)
  | None when nodec -> false
  | None -> (
      match Prio.extract_min t.choices with
      | Some chogen ->
          Debug.dprintf1 debug_queue "[Scheduler] Decision mode (%f)"
            (Prio.get_prio t.choices chogen);
          try_run_dec t chogen;
          true
      | None -> (
          let last_effort_deactivated =
            match t.last_effort_limit with
            | Some (Steps step_limit) -> step_limit <= t.steps
            | _ -> false
          in
          if last_effort_deactivated then (
            Debug.dprintf0 debug_queue "[Scheduler] LastEffort (deactivated)";
            false)
          else
            match LastEffort_att.next t.lasteffort with
            | Some att ->
                Debug.dprintf2 debug_queue "[Scheduler] LastEffort: after %a"
                  LastEffort_att.pp_key att;
                Debug.incr stats_lasteffort;
                t.lasteffort.last_effort_made_active := false;
                let active = ref true in
                t.lasteffort.last_effort_made_active <- active;
                Context.RefOpt.set t.lasteffort.current_last_effort att;
                Context.Ref.set t.lasteffort.current_last_effort_level t.level;
                t.prev_scheduler_state <-
                  LastEffort { bp = t.prev_scheduler_state; att; active };
                protect_against_contradiction ~usefulness:true ~no_learning:true
                  t
                  (fun () ->
                    let d = Backtrackable.get_delayed t.solver_state in
                    Backtrackable.run_daemon d att.v)
                  (fun () -> true)
            | None ->
                Debug.dprintf0 debug_queue "[Scheduler] LastEffort: 0";
                t.lasteffort.last_effort_made_active := false;
                Context.RefOpt.unset t.lasteffort.current_last_effort;
                false (* else false *)))

let run_one_step_fix_model ~nodec t =
  Debug.incr stats_fix_model;
  match Context.Ref.get t.fixing_model with
  | att :: l ->
      Context.Ref.set t.fixing_model l;
      protect_against_contradiction ~usefulness:false ~no_learning:false t
        (fun () ->
          (* Debug.dprintf0 debug "ICI"; *)
          let d = Backtrackable.get_delayed t.solver_state in
          Backtrackable.run_daemon d att;
          Backtrackable.flush t.solver_state)
        (fun () -> true)
  | [] when nodec -> false
  | [] ->
      protect_against_contradiction ~usefulness:false ~no_learning:true t
        (fun () ->
          let d = Backtrackable.get_delayed t.solver_state in
          let seq = Fix_model.next_dec d in
          assert (Backtrackable.has_nothing_todo t.solver_state);
          seq)
        (fun seq ->
          match Std.Sequence.next seq with
          | None -> false
          | Some (todo, todos) ->
              Backtrackable.delayed_stop t.solver_state;
              make_choice None ~msg:(Fmt.any "fixmodel") t todo todos;
              true)

let run_one_step ~nodec ~nofixmodel t =
  t.steps <- t.steps + 1;
  Debug.incr stats_step;
  Debug.dprintf1 debug "[Scheduler] Step %i" t.steps;
  match Context.Ref.get t.solve_step with
  | Propagate -> (
      match run_one_step_propagation ~nodec t with
      | true -> true
      | false when nodec -> false
      | false ->
          Debug.dprintf0 debug "[Scheduler] FixModel";
          t.current_fixmodel_conflict <- 0;
          t.prev_scheduler_state <- StartFixModel t.prev_scheduler_state;
          Context.Ref.set t.solve_step FixModel;
          true)
  | BacktrackChoice (choice, choices, step, chogen) -> (
      let res =
        match step with
        | BacktrackChoice _ -> assert false
        | Propagate -> run_one_step_propagation ~nodec:true t
        | FixModel -> run_one_step_fix_model ~nodec:true t
      in
      match res with
      | true -> true
      | false ->
          Context.Ref.set t.solve_step step;
          let msg =
            match chogen with
            | None -> Fmt.any "push after learnt"
            | Some chogen -> chogen.v.print_cho
          in
          make_choice chogen ~msg t choice choices;
          true)
  | FixModel ->
      if nofixmodel then raise ReachStepLimit;
      run_one_step_fix_model ~nodec t

let rec flush t =
  try Backtrackable.flush t.solver_state
  with Egraph.Contradiction ->
    Debug.dprintf2 debug_pushpop
      "[Scheduler] Contradiction when flushing at level %a" print_level t;
    conflict_analysis ~usefulness:true ~no_learning:true t;
    flush t

let run_one_step ~nodec ~nofixmodel t =
  if (not nodec) && 0 < t.first_restart && t.first_restart < t.steps then (
    Debug.dprintf0 debug "[Scheduler] Restart";
    t.first_restart <- 0;
    let rec rewind_at_restart t =
      match t.prev_scheduler_state with
      | Start ->
          Debug.dprintf0 debug "[Scheduler] Restart not reached!!";
          assert false
      | Bp ({ kind = Restart } as prev) -> pop_to t prev
      | Bp prev ->
          pop_to t prev;
          rewind_at_restart t
      | LastEffort { bp } | PreChoice { bp } | StartFixModel bp ->
          t.prev_scheduler_state <- bp;
          rewind_at_restart t
    in
    rewind_at_restart t;
    true)
  else run_one_step ~nodec ~nofixmodel t

let rec run_inf_step ~step_limit ~nodec ~nofixmodel t =
  if t.steps = step_limit then raise ReachStepLimit;
  flush t;
  match run_one_step ~nodec ~nofixmodel t with
  | true -> run_inf_step ~step_limit ~nodec ~nofixmodel t
  | false -> Backtrackable.delayed_stop t.solver_state

let run_inf_step ?(step_limit = -1) ?(nodec = false) t =
  if 0 < t.first_restart then ignore (push Restart t);
  let d = Backtrackable.get_delayed t.solver_state in
  let nofixmodel = nodec || Colibri2_core.Options.get d no_fixmodel_option in
  let nodec = nodec || Colibri2_core.Options.get d nodec_option in
  try
    run_inf_step ~step_limit ~nodec ~nofixmodel t;
    Debug.dprintf1 debug_pushpop "[Scheduler] sat(%i)" t.steps
  with Contradiction as e ->
    Debug.dprintf1 debug_pushpop "[Scheduler] unsat(%i)" t.steps;
    raise e

let init_theories ?(theories = ForSchedulers.default_theories ()) t =
  Trace.with_span ~__FUNCTION__ ~__LINE__ ~__FILE__ "init_theories" @@ fun _ ->
  try
    let d = Backtrackable.get_delayed t.solver_state in
    List.iter (fun f -> f d) (ground_init :: interp_init :: theories);
    Backtrackable.flush t.solver_state
  with Egraph.Contradiction ->
    Debug.dprintf0 debug
      "[Scheduler] Contradiction during theory initialization";
    raise Contradiction

let add_assertion t f =
  Trace.with_span ~__FUNCTION__ ~__LINE__ ~__FILE__ "add_assertions" @@ fun _ ->
  try
    let d = Backtrackable.get_delayed t.solver_state in
    let res = f d in
    flush t;
    res
  with Egraph.Contradiction ->
    Debug.dprintf0 debug "[Scheduler] Contradiction during assertion";
    raise Contradiction

let exit_solver s =
  let rec rewind () =
    match s.prev_scheduler_state with
    | Start -> ()
    | LastEffort { bp } | PreChoice { bp } | StartFixModel bp ->
        s.prev_scheduler_state <- bp;
        rewind ()
    | Bp prev ->
        pop_to s prev;
        rewind ()
  in
  rewind ()

let check_sat ?nodec ?step_limit t =
  Trace.with_span ~__FUNCTION__ ~__LINE__ ~__FILE__ "check_sat" @@ fun _ ->
  try
    flush t;
    run_inf_step ?nodec ?step_limit t;
    let d = Backtrackable.get_delayed t.solver_state in
    if Backtrackable.get_unknown t.solver_state then `Unknown d else `Sat d
  with Contradiction -> if t.cut_branch then `UnknownUnsat else `Unsat

let for_tests ?nodec ?step_limit ?last_effort_limit ?(learning = false) ?options
    ~theories f =
  let t = new_solver ~learning ?last_effort_limit ?options () in
  init_theories ~theories t;
  let rec aux todo =
    match todo with
    | `Add_assertion f ->
        let todo = add_assertion t f in
        flush t;
        run_inf_step ~nodec:true ?step_limit t;
        aux todo
    | `Check check ->
        flush t;
        run_inf_step ~nodec:(nodec = Some ()) ?step_limit t;
        let d = Backtrackable.get_delayed t.solver_state in
        check d
  in
  aux f

let run_exn ?nodec ?step_limit ?last_effort_limit ?(learning = false) ?options
    ~theories f =
  let t = new_solver ~learning ?last_effort_limit ?options () in
  init_theories ~theories t;
  let check = add_assertion t f in
  flush t;
  run_inf_step ~nodec:(nodec = Some ()) ?step_limit t;
  let d = Backtrackable.get_delayed t.solver_state in
  check d

let run ?nodec ?step_limit ?last_effort_limit ?options ~theories f =
  try `Done (run_exn ?nodec ?step_limit ?last_effort_limit ?options ~theories f)
  with Contradiction -> `Contradiction

let pop_to st bp = ignore (pop_to st bp)
let push ?msg st = push ?msg External st
let get_context t = Context.creator t.context
