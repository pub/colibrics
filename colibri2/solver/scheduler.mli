(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

type last_effort_limit =
  | Time of float
  (* nb seconds *)
  | Steps of int
(* nb steps *)

val run_exn :
  ?nodec:unit ->
  ?step_limit:int ->
  ?last_effort_limit:last_effort_limit ->
  ?learning:bool ->
  ?options:ForSchedulers.Options.options ->
  theories:(Egraph.wt -> unit) list ->
  (Egraph.wt -> Egraph.wt -> 'a) ->
  'a
(** [run_exn ?nodec ?limit ?learning ~theories add_assertions].

    [add_assertions d] should add the assertions. The resulting function is
    applied on the environment after propagation and possibly decisions *)

val run :
  ?nodec:unit ->
  ?step_limit:int ->
  ?last_effort_limit:last_effort_limit ->
  ?options:ForSchedulers.Options.options ->
  theories:(Egraph.wt -> unit) list ->
  (Egraph.wt -> Egraph.wt -> 'a) ->
  [> `Contradiction | `Done of 'a ]

type t

val new_solver :
  learning:bool ->
  ?last_effort_limit:last_effort_limit ->
  ?options:ForSchedulers.Options.options ->
  unit ->
  t

val exit_solver : t -> unit
val init_theories : ?theories:(Egraph.wt -> unit) list -> t -> unit
val add_assertion : t -> (Egraph.wt -> unit) -> unit

val check_sat :
  ?nodec:bool ->
  ?step_limit:int ->
  t ->
  [> `Unsat | `UnknownUnsat | `Sat of Egraph.wt | `Unknown of Egraph.wt ]

val get_steps : t -> int
val get_context : t -> Context.creator

type bp

val push : ?msg:unit Fmt.t -> t -> bp
val pop_to : t -> bp -> unit

module LearnSet : Set.S with type elt = Ground.t

exception ReachStepLimit

val for_tests :
  ?nodec:unit ->
  ?step_limit:int ->
  ?last_effort_limit:last_effort_limit ->
  ?learning:bool ->
  ?options:ForSchedulers.Options.options ->
  theories:(Egraph.wt -> unit) list ->
  ([< `Add_assertion of Egraph.wt -> 'a | `Check of Egraph.wt -> 'b ] as 'a) ->
  'b
