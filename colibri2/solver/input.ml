(*************************************************************************)
(*  This file is part of Colibri2.                                       *)
(*                                                                       *)
(*  Copyright (C) 2014-2021                                              *)
(*    CEA   (Commissariat à l'énergie atomique et aux énergies           *)
(*           alternatives)                                               *)
(*                                                                       *)
(*  you can redistribute it and/or modify it under the terms of the GNU  *)
(*  Lesser General Public License as published by the Free Software      *)
(*  Foundation, version 2.1.                                             *)
(*                                                                       *)
(*  It is distributed in the hope that it will be useful,                *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of       *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *)
(*  GNU Lesser General Public License for more details.                  *)
(*                                                                       *)
(*  See the GNU Lesser General Public License version 2.1                *)
(*  for more details (enclosed in the file licenses/LGPLv2.1).           *)
(*************************************************************************)

type config = {
  config_step_limit : int option;
  last_effort_limit : Scheduler.last_effort_limit option;
  options : ForSchedulers.Options.options option;
  learning : bool;
  theories : (Egraph.wt -> unit) list;
  negate_goal : bool;
}

type state = Dolmen_loop.State.t

type ctx = {
  config : config;
  scheduler : Scheduler.t;
  mutable state :
    [ `Sat of Egraph.wt
    | `Unknown of Egraph.wt
    | `Search
    | `UnknownUnsat
    | `Unsat
    | `StepLimitReached ];
  mutable pushpop : Scheduler.bp list;
  mutable status_colibri :
    [ `No | `Sat | `UnknownUnsat | `Unsat | `Unknown | `StepLimitReached ]
    Context.Ref.t;
  mutable decls : Expr.Term.Const.t Context.Queue.t;
  mutable step_limit : int option Context.Ref.t;
  mutable print_success : bool;
  mutable exit : bool;
}

let pp_status_state fmt = function
  | `Sat _ -> Fmt.pf fmt "sat"
  | `Unknown _ -> Fmt.pf fmt "unknown"
  | `Search -> Fmt.pf fmt "still searching"
  | `UnknownUnsat -> Fmt.pf fmt "unknown-cut-branch"
  | `Unsat -> Fmt.pf fmt "unsat"
  | `StepLimitReached -> Fmt.pf fmt "step limit reached"

let pp_status_colibri fmt = function
  | `Sat -> Fmt.pf fmt "sat"
  | `Unknown -> Fmt.pf fmt "unknown"
  | `No -> Fmt.pf fmt "no status expected"
  | `UnknownUnsat -> Fmt.pf fmt "unknown-cut-branch"
  | `Unsat -> Fmt.pf fmt "unsat"
  | `StepLimitReached -> Fmt.pf fmt "step limit reached"

let create_ctx ?step_limit ?last_effort_limit ?options ?(print_success = false)
    ?(negate_goal = false) learning theories =
  let t = Scheduler.new_solver ~learning ?last_effort_limit ?options () in
  let ctx = Scheduler.get_context t in
  Scheduler.init_theories t ~theories;
  {
    config =
      {
        config_step_limit = step_limit;
        last_effort_limit;
        options;
        learning;
        theories;
        negate_goal;
      };
    scheduler = t;
    state = `Search;
    pushpop = [];
    status_colibri = Context.Ref.create ctx `No;
    decls = Context.Queue.create ctx;
    step_limit = Context.Ref.create ctx step_limit;
    print_success;
    exit = false;
  }

let reset_ctx env =
  Trace.message "reset";
  Scheduler.exit_solver env.scheduler;
  let t =
    Scheduler.new_solver ~learning:env.config.learning
      ?last_effort_limit:env.config.last_effort_limit
      ?options:env.config.options ()
  in
  let ctx = Scheduler.get_context t in
  Scheduler.init_theories t ~theories:env.config.theories;
  env.status_colibri <- Context.Ref.create ctx `No;
  env.decls <- Context.Queue.create ctx;
  env.step_limit <- Context.Ref.create ctx env.config.config_step_limit;
  env.state <- `Search;
  env.pushpop <- []

let solver_state = Dolmen_loop.State.create_key ~pipe:"Colibri2" "solver_state"
let get_ctx = Dolmen_loop.State.get solver_state
let get_state s = (get_ctx s).state

let split_input = function
  | `Stdin -> (Sys.getcwd (), `Stdin)
  | `File f -> (Filename.dirname f, `File (Filename.basename f))

let success (st : Dolmen_loop.State.t) =
  match Dolmen_loop.State.(get logic_file st).lang with
  | Some (Dolmen_loop.Logic.Smtlib2 _) when (get_ctx st).print_success ->
      Format.printf "success@."
  | None | Some _ -> ()

module State = struct
  include Dolmen_loop.State

  let error_smtlib2 ?file ?loc st error payload =
    let open Dolmen_loop in
    let st = flush st () in
    let loc = Dolmen.Std.Misc.opt_map loc Dolmen.Std.Loc.full_loc in
    let aux _ = Code.exit (Report.Error.code error) in
    match get report_style st with
    | Minimal ->
        Format.kfprintf aux Format.std_formatter "(error \"%s\")@."
          (Report.Error.mnemonic error)
    | Regular | Contextual ->
        Format.kfprintf aux Format.std_formatter
          "@[(error \"@[<v>%a%a @[<hov>%a@]%a@]\")@]@." (pp_loc ?file st) loc
          Fmt.(styled `Bold @@ styled (`Fg (`Hi `Red)) string)
          "Error" Report.Error.print (error, payload) Report.Error.print_hints
          (error, payload)

  let error ?file ?loc st verror payload =
    match Dolmen_loop.State.(get logic_file st).lang with
    | Some (Dolmen_loop.Logic.Smtlib2 _) ->
        error_smtlib2 ?file ?loc st verror payload
    | None | Some _ -> error ?file ?loc st verror payload
end

module Pipeline = Dolmen_loop.Pipeline.Make (State)
module Parser = Dolmen_loop.Parser.Make (State)
module Header = Dolmen_loop.Headers.Make (State)

module Typer = struct
  module T = Dolmen_loop.Typer.Typer (State)
  include T

  include
    Dolmen_loop.Typer.Make (Dolmen.Std.Expr) (Dolmen.Std.Expr.Print) (State) (T)

  let init_pipe = init
  let init = T.init
end

type tag_colibri2 = Goal

let tag_colibri2 = Dolmen_std.Tag.create ()

let parse_tag_colibri2 env = function
  | ({ Dolmen_std.Term.term = Symbol s; _ } as ast)
  | ({ term = App ({ term = Symbol s; _ }, []); _ } as ast) -> (
      match Dolmen.Std.Id.name s with
      | Simple "goal" -> Goal
      | _ ->
          Dolmen_loop.Typer.T._error env (Ast ast)
            (Dolmen_loop.Typer.T.Expected ("goal", None)))
  | ast ->
      Dolmen_loop.Typer.T._error env (Ast ast)
        (Dolmen_loop.Typer.T.Expected ("symbol", None))

let additional_builtins _ _ env s =
  match s with
  (* Named formulas *)
  | Dolmen_loop.Typer.T.Id { name = Simple ":colibri2"; ns = Attr } ->
      Dolmen_loop.Typer.T.builtin_tags (fun _ tl ->
          List.map
            (fun t ->
              let name = parse_tag_colibri2 env t in
              Dolmen_loop.Typer.T.Set (tag_colibri2, name))
            tl)
  | _ -> Colibri2_core.Expr.additional_builtins env s

(* Exceptions *)
(* ************************************************************************* *)

let print_exn_and_exit st = function
  (* Sigint, potentially wrapped by the typechecker *)
  | Dolmen_loop.Pipeline.Sigint ->
      Format.pp_print_flush Format.std_formatter ();
      Format.pp_print_flush Format.err_formatter ();
      State.error st Dolmen_loop.Report.Error.user_interrupt ()
  (* Timeout, potentially wrapped by the typechecker *)
  | Dolmen_loop.Pipeline.Out_of_time ->
      Format.pp_print_flush Format.std_formatter ();
      State.error st Dolmen_loop.Report.Error.timeout ()
  | Dolmen_loop.Pipeline.Out_of_space ->
      Format.pp_print_flush Format.std_formatter ();
      Format.pp_print_flush Format.err_formatter ();
      State.error st Dolmen_loop.Report.Error.spaceout ()
  (* Internal Dolmen Expr errors *)
  | Dolmen.Std.Expr.Ty.Bad_arity (c, l) ->
      let pp_sep fmt () = Format.fprintf fmt ";@ " in
      State.error st Dolmen_loop.Report.Error.internal_error
        (Format.dprintf
           "@[<hv>Internal error: Bad arity for type constant '%a',@ which was \
            provided arguments:@ [@[<hv>%a@]]@]"
           Dolmen.Std.Expr.Print.ty_cst c
           (Format.pp_print_list ~pp_sep Dolmen.Std.Expr.Ty.print)
           l)
  | Dolmen.Std.Expr.Type_already_defined c ->
      State.error st Dolmen_loop.Report.Error.internal_error
        (Format.dprintf
           "@[<hv>Internal error: Type constant '%a' was already defined \
            earlier,@ cannot re-define it.@]"
           Dolmen.Std.Expr.Print.id c)
  | Dolmen.Std.Expr.Term.Wrong_type (t, ty) ->
      State.error st Dolmen_loop.Report.Error.internal_error
        (Format.dprintf
           "@[<hv>Internal error: A term of type@ %a@ was expected but instead \
            got a term of type@ %a@]"
           Dolmen.Std.Expr.Ty.print ty Dolmen.Std.Expr.Ty.print
           (Dolmen.Std.Expr.Term.ty t))
  (* Generic catch-all *)
  | exn ->
      let bt = Printexc.get_raw_backtrace () in
      State.error st Dolmen_loop.Report.Error.uncaught_exn (exn, bt)

let handle_exn_with_exit st bt exn =
  if State.get State.debug st || Printexc.backtrace_status () then (
    Printexc.print_raw_backtrace stderr bt;
    Format.eprintf "%s@." (Printexc.to_string exn));
  print_exn_and_exit st exn

let handle_exn_with_error _ bt exn = Printexc.raise_with_backtrace exn bt

let finally ~handle_exn st e =
  match ((State.get solver_state st).state, e) with
  | `StepLimitReached, None -> raise Scheduler.ReachStepLimit
  | _, None -> st
  | _, Some (bt, exn) ->
      handle_exn st bt exn;
      assert false

let debug_pre_pipe st c =
  if State.get State.debug st then
    Format.eprintf "[pre] @[<hov>%a@]" Dolmen.Std.Statement.print c;
  (st, c)

let debug_post_pipe st stmt =
  if State.get State.debug st then
    Format.eprintf "[post] @[<hov>%a@]@\n"
      (Fmt.list ~sep:Fmt.comma Typer.print)
      stmt;
  (st, stmt)

let def st (d : Typer.def) =
  match d with
  | `Type_alias _ -> () (* handled by dolmen *)
  | `Instanceof _ ->
      (* should be unreachable, only used by dolmen for model verification *)
      assert false
  | `Term_def (_, tc, tyl, tvl, b) ->
      Scheduler.add_assertion st.scheduler (fun d ->
          Ground.Defs.add d tc tyl tvl b)

let set_term_true ~set_true d t =
  let t = Ground.convert d t in
  Egraph.register d t;
  set_true d t

let hyp ~set_true st t =
  let t =
    match Expr.Term.get_tag t tag_colibri2 with
    | Some Goal when st.config.negate_goal -> Expr.Term.neg t
    | _ -> t
  in
  Scheduler.add_assertion st.scheduler (fun d -> set_term_true ~set_true d t)

let solve ~set_true (st : ctx) t =
  let check st =
    match
      Scheduler.check_sat
        ?step_limit:(Context.Ref.get st.step_limit)
        st.scheduler
    with
    | res -> st.state <- res
    | exception Scheduler.ReachStepLimit -> st.state <- `StepLimitReached
  in
  match t with
  | [] -> check st
  | l ->
      let bp = Scheduler.push st.scheduler in
      Scheduler.add_assertion st.scheduler (fun d ->
          List.iter (set_term_true ~set_true d) l);
      check st;
      Scheduler.pop_to st.scheduler bp

let rec push st i =
  assert (0 <= i);
  if 0 < i then (
    st.pushpop <- Scheduler.push st.scheduler :: st.pushpop;
    push st (i - 1))

let rec pop st i =
  assert (0 <= i);
  match (i, st.pushpop) with
  | 0, _ -> ()
  | _, [] -> assert false (* absurd by dolmen invariant *)
  | 1, bp :: l ->
      st.pushpop <- l;
      Scheduler.pop_to st.scheduler bp
  | n, _ :: l ->
      st.pushpop <- l;
      pop st (n - 1)

let error st loc fmt =
  let file = State.get State.logic_file st in
  let loc : Dolmen.Std.Loc.full = { file = file.loc; loc } in
  State.error ~loc st fmt

let unimplemented =
  Dolmen_loop.Report.Error.mk ~code:Dolmen_loop.Code.bug
    ~mnemonic:"unimplemented-stmt"
    ~message:(fun fmt stmt ->
      Fmt.pf fmt "Unimplemented stmt: %a" Typer.print stmt)
    ~name:"Unimplemented Statement" ()

let missing_status_colibri2 =
  Dolmen_loop.Report.Error.mk ~code:Dolmen_loop.Code.parsing
    ~mnemonic:"missing-status"
    ~message:(fun fmt () ->
      Fmt.pf fmt
        "The option --check-status@ needs a@ status given@ by@ (set-info \
         :status-colibri2 ...)@ or :status")
    ~name:"Missing Status" ()

let wrong_status =
  Dolmen_loop.Report.Error.mk ~code:Dolmen_loop.Code.parsing
    ~mnemonic:"wrong-status"
    ~message:(fun fmt (expected, actual) ->
      Fmt.pf fmt "wrong status %a obtained instead of expected %a"
        pp_status_state actual pp_status_colibri expected)
    ~name:"Wrong Status" ()

let unknown_status =
  Dolmen_loop.Report.Error.mk ~code:Dolmen_loop.Code.parsing
    ~mnemonic:"unknown-status"
    ~message:(fun fmt () ->
      Fmt.pf fmt "Expected sat|unsat|unknown|unknowncutbranch")
    ~name:"Unknown Status" ()

let expected_integer_for_option =
  Dolmen_loop.Report.Error.mk ~code:Dolmen_loop.Code.parsing
    ~mnemonic:"integer-expected-for-option"
    ~message:(fun fmt () -> Fmt.pf fmt "Integer expected for this option")
    ~name:"Integer expected" ()

let expected_boolean_for_option =
  Dolmen_loop.Report.Error.mk ~code:Dolmen_loop.Code.parsing
    ~mnemonic:"boolean-expected-for-option"
    ~message:(fun fmt () -> Fmt.pf fmt "Booleab expected for this option")
    ~name:"Boolean expected" ()

let option_get_model_no_function =
  let doc =
    "Skip the model of functions symbol and polymorphic constants during \
     get-model"
  in
  Options.register ~pp:Fmt.bool "get_model_no_function"
    Cmdliner.Arg.(value & flag & info [ "get-model-no-function" ] ~doc)

let handle_stmt ?(show_check_sat_result = true) ?(show_steps = false)
    ?(check_status = `No) ~set_true (st : State.t)
    (stmtl : Typer.typechecked Typer.stmt list) =
  let st =
    List.fold_left
      (fun st (stmt : Typer.typechecked Typer.stmt) ->
        let solve_state = State.get solver_state st in
        (match stmt.contents with
        | `Get_unsat_core | `Other _ | `Get_assignment | `Get_unsat_assumptions
        | `Get_info _ | `Get_proof | `Get_assertions | `Get_option _
        | `Reset_assertions ->
            error st stmt.loc unimplemented stmt
        | `Reset ->
            reset_ctx solve_state;
            success st
        | `Push i ->
            push solve_state i;
            success st
        | `Pop i ->
            pop solve_state i;
            success st
        | `Decls l ->
            Base.List.iter l ~f:(function
              | `Type_decl _ -> ()
              | `Term_decl c -> Context.Queue.enqueue solve_state.decls c);
            success st
        | `Defs d ->
            List.iter (def solve_state) d;
            success st
        | `Goal _ -> error st stmt.loc unimplemented stmt
        | `Clause l ->
            hyp ~set_true solve_state (Expr.Term._or l);
            success st
        | `Hyp d ->
            hyp ~set_true solve_state d;
            success st
        | `Solve (hyps, []) -> (
            solve ~set_true solve_state hyps;
            (if show_check_sat_result then
               match solve_state.state with
               | `UnknownUnsat -> Format.printf "unknown-branch-cut@."
               | `Unsat -> Format.printf "unsat@."
               | `Sat _ -> Format.printf "sat@."
               | `Unknown _ -> Format.printf "unknown@."
               | `Search -> Format.printf "unknown@."
               | `StepLimitReached -> Format.printf "steplimitreached@.");
            if show_steps then
              Format.printf "(steps %i)@."
                (Scheduler.get_steps solve_state.scheduler);
            let equal_status expected =
              match (expected, solve_state.state) with
              | `No, _ -> error st stmt.loc missing_status_colibri2 ()
              | `Sat, `Sat _
              | `Unknown, `Unknown _
              | `Unsat, `Unsat
              | `UnknownUnsat, `UnknownUnsat
              | `StepLimitReached, `StepLimitReached ->
                  ()
              | _ -> error st stmt.loc wrong_status (expected, solve_state.state)
            in

            match check_status with
            | `No | `SMTLIB -> ()
            | `COLIBRI2 ->
                equal_status (Context.Ref.get solve_state.status_colibri)
            | `Sat -> equal_status `Sat
            | `Unknown -> equal_status `Unknown
            | `Unsat -> equal_status `Unsat
            | `StepLimitReached -> equal_status `StepLimitReached)
        | `Solve (_, _) ->
            failwith
              "Unsupported statement: \"`Solve (hyps, goals)\" with an \
               non-empty list of goals."
        | `Echo s -> Format.printf "%s@." s
        | `Set_info
            Dolmen_std.
              {
                Term.term =
                  App
                    ( {
                        term =
                          Symbol
                            {
                              Id.ns = Namespace.Attr;
                              Id.name = Simple (":status-colibri2" | ":status");
                            };
                        _;
                      },
                      args );
                loc;
                _;
              } -> (
            match args with
            | [ { term = Symbol { name = Simple "sat"; _ }; _ } ] ->
                Context.Ref.set solve_state.status_colibri `Sat
            | [ { term = Symbol { name = Simple "unsat"; _ }; _ } ] ->
                Context.Ref.set solve_state.status_colibri `Unsat
            | [ { term = Symbol { name = Simple "unknowncutbranch"; _ }; _ } ]
              ->
                Context.Ref.set solve_state.status_colibri `UnknownUnsat
            | [ { term = Symbol { name = Simple "unknown"; _ }; _ } ] ->
                Context.Ref.set solve_state.status_colibri `Unknown
            | [ { term = Symbol { name = Simple "steplimitreached"; _ }; _ } ]
              ->
                Context.Ref.set solve_state.status_colibri `StepLimitReached
            | _ -> error st loc unknown_status ())
        | `Set_info _ -> success st
        | `Set_option
            Dolmen_std.
              {
                Term.term =
                  App
                    ( {
                        term =
                          Symbol
                            { Id.ns = Namespace.Attr; Id.name = Simple attr };
                        _;
                      },
                      args );
                loc;
                _;
              } -> (
            match (attr, args) with
            | ( ":max-steps-colibri2",
                [ { term = Symbol { name = Simple i; _ }; _ } ] ) -> (
                match int_of_string i with
                | i ->
                    (* only if there is already a step limit *)
                    if Option.is_some (Context.Ref.get solve_state.step_limit)
                    then Context.Ref.set solve_state.step_limit (Some i);
                    success st
                | exception _ -> error st loc expected_integer_for_option ())
            | ":max-steps-colibri2", _ ->
                error st loc expected_integer_for_option ()
            | ":print-success", [ { term = Symbol { name = Simple b; _ }; _ } ]
              ->
                let v =
                  match b with
                  | "true" -> true
                  | "false" -> false
                  | _ -> error st loc expected_boolean_for_option ()
                in
                solve_state.print_success <- v;
                success st
            | ":print-success", _ -> error st loc expected_boolean_for_option ()
            | _ -> success st)
        | `Set_option _ -> success st
        | `Set_logic _ -> success st
        | `Get_value l -> (
            match solve_state.state with
            | `Sat d | `Unknown d ->
                Format.printf "@[(";
                List.iter
                  (fun e ->
                    let v = Interp.interp d e in
                    let ty = Ground.Ty.convert Expr.Ty.Var.M.empty e.term_ty in
                    Format.printf "(%a %a)" Expr.Term.pp_smtlib e
                      (Interp.print_value_smt d ty)
                      v)
                  l;
                Format.printf ")@]@."
            | `Unsat -> Format.printf "(error last problem solved was unsat)"
            | `UnknownUnsat ->
                Format.printf
                  "(error last problem solved was unknown with cut branch)"
            | `StepLimitReached ->
                Format.printf "(error model after step limit not implemented)"
            | `Search -> Format.printf "(error no preceded by a check_sat)")
        | `Get_model -> (
            match solve_state.state with
            | `Sat d | `Unknown d ->
                let q = Base.Queue.create () in
                Context.Queue.iter
                  (fun (c : Expr.Term.Const.t) ->
                    (* polymorphic constant are currently not printed *)
                    let poly, args, _ = Expr.Ty.poly_sig c.id_ty in
                    if
                      Base.List.is_empty poly
                      || (not (Options.get d option_get_model_no_function))
                      || Base.List.is_empty args
                    then
                      let e = Expr.Term.apply_cst c [] [] in
                      let v = Interp.interp d e in
                      Base.Queue.enqueue q (c, v))
                  solve_state.decls;
                Format.printf "@[(";
                Base.Queue.iter q ~f:(fun (c, v) ->
                    let poly, _, _ = Expr.Ty.poly_sig c.id_ty in
                    match poly with
                    | [] ->
                        let ty =
                          Ground.Ty.convert Expr.Ty.Var.M.empty c.id_ty
                        in
                        Format.printf "(%a %a)" Expr.Term.Const.pp c
                          (Interp.print_value_smt d ty)
                          v
                    | _ -> () (* polymorphic function are not printed *));
                Format.printf ")@]@."
            | `UnknownUnsat ->
                Format.printf
                  "(error last problem solved was unknown with cut branch)"
            | `Unsat -> Format.printf "(error last problem solved was unsat)"
            | `StepLimitReached ->
                Format.printf "(error model after step limit not implemented)"
            | `Search -> Format.printf "(error no preceded by a check_sat)")
        | `Exit ->
            solve_state.exit <- true;
            success st);
        st)
      st stmtl
  in
  (st, ())

let read ?show_check_sat_result ?show_steps ?check_status ~set_true ~handle_exn
    st =
  let g =
    try Parser.parse_logic (State.get State.logic_file st)
    with exn ->
      let bt = Printexc.get_raw_backtrace () in
      handle_exn st bt exn;
      assert false
  in
  let g st =
    (* delayed handling of exit command *)
    let solve_state = State.get solver_state st in
    if solve_state.exit then (st, None) else g st
  in
  let st =
    let open Pipeline in
    run ~finally:(finally ~handle_exn) g st
      (fix
         (op ~name:"expand" Parser.expand)
         (op ~name:"debug_pre" debug_pre_pipe
         @>>> op ~name:"headers" Header.inspect
         @>>> op ~name:"typecheck" Typer.typecheck
         @>|> op ~name:"debug_post" debug_post_pipe
         @>>> op
                (handle_stmt ?show_check_sat_result ?show_steps ?check_status
                   ~set_true)
         @>>> _end))
  in
  let st = Header.check st in
  let _st = Dolmen_loop.State.flush st () in
  ()

let mk_state ?(gc = false) ?(gc_opt = Gc.get ()) ?(bt = false) ?(colors = true)
    ?(time_limit = 1_000_000_000.) ?(size_limit = 1_000_000_000.) ?step_limit
    ?lang ?mode ~input ?(header_check = false) ?(header_licenses = [])
    ?(header_lang_version = None) ?(type_check = true) ?(debug = true)
    ?(max_warn = max_int) ?(learning = false) ?last_effort_limit ?print_success
    ?negate_goal ?options theories =
  (* Side-effects *)
  let () = Gc.set gc_opt in
  let () =
    let style = if colors then `Ansi_tty else `None in
    Fmt.set_style_renderer Format.err_formatter style
  in
  let () = Printexc.record_backtrace bt in
  let () = if gc then at_exit (fun () -> Gc.print_stat stdout) in
  (* State creation *)
  let dir, source = split_input input in
  let logic_file = State.mk_file ?lang ?mode dir source in
  let response_file = State.mk_file dir (`Raw ("", "")) in
  let ctx =
    create_ctx ?step_limit ?last_effort_limit ?options ?print_success
      ?negate_goal learning theories
  in
  (* State creation *)
  let set = State.set in
  State.empty |> set solver_state ctx
  |> State.init ~debug ~report_style:Contextual
       ~reports:
         (Dolmen_loop.Report.Conf.mk
            ~default:Dolmen_loop.Report.Warning.Status.Disabled)
       ~max_warn ~time_limit ~size_limit ~response_file
  |> set State.logic_file logic_file
  |> Parser.init
  |> Typer.init
       ~ty_state:(Dolmen_loop.Typer.new_state ())
       ~smtlib2_forced_logic:None ~additional_builtins
  |> Typer.init_pipe ~type_check
  |> Header.init ~header_check ~header_licenses ~header_lang_version
