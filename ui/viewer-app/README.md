# colibri2-trace-viewer

Vizualizer for a `json` trace emitted by Colibri 2.

## Installation

In the current directory
```
npm install
```

Follow https://vitejs.dev/guide/ to install Vite.
In the current directory, calling `make` should start a Vite server for the project. 
The running program accepts commands from the terminal.
For example you can write the letter `o` and `Enter` to open the application in your browser.

## Overview

We assume the input file is a JSON file in a subset of the
[Catapult/TEF][tef] that corresponds to the output emitted by [Colibri
2][colibri2]. The actual format depends on tools being used
(e.g. [ocaml-trace][ocamltrace]) and the version of Colibri 2 used to
generate it.

The vizualizer aims to be able to open traces from older versions
(e.g. in order to analyze a bug report from a past release).  To that
effect, the tool is architectured around a series of modular filters,
with the expectation that a trace contains versioning informations
allowing it to select the appropriate set of filters for each file.

NB. The current version of Colibri 2 does not emit such metadata.

The implementation is Typescript and React, using [Vite][vite].

## Glossary

<dl>
  <dt>Event</dt>
  <dd>JS object handled by filters: all traces from the input file are first converted to a
      RegularEvent. Then the filters can modify then in any way they want. At the very least
      each event is expected to have a <code>render</code> property, which is a list
      of rendering functions.</dd>
  <dt>Trace</dt>
  <dd>Sorted, finite sequence of events.</dd>
  <dt>RegularEvent</dt>
  <dd>An event with a list of tags (initially <code>["regular", "ts"]</code>, a timestamp,
      a name, a list of arguments and a ph category.</dd>
  <dt>Tags</dt>
  <dd>The tags associated with an event are strings, they are used to help
      routing them across filters, in particular when testing if a filter
      can handle an event or not (e.g. the <code>"ts"</code> tag indicates
      that an event has a timestamp; some events don't).</dd>
  <dt>Filter</dt>
  <dd>An object responsible for processing events when loading a trace.</dd>
  <dt>Environment</dt>
  <dd>An object where filters can exchange information.</dd>
  <dt>RowProps</dt>
  <dd>Properties associated with a Row component when rendering
      it (Props is how React call such objects). A RowProps in particular holds different
      slots and lists where filters can attach data in a generic way. They also contain
      React hooks and the global environment that was built during filtering.
  </dd>
<dt>Rendering function</dt> <dd>Accepts an Event and a RowProps, and
 produce either a RowProps (that may be identical to the input) or
 null. Rendering functions are listed in an event from the most
 generic one to the most specific one. A filter may add a new
 rendering function (or in fact completely change the list of
 rendering functions). The last function is called first with an
 initial RowProps, then the result is given to the previous function,
 etc. The rendering process is called once for each event to produce a
 list of RowProps. The RowProps are not modified afterwards, but used
 to build React components of type Row.
 </dd> </dl>

## Adding a filter

- Write a module in `src/traces/filters/` whose default export is a function returning a *Filter* object.
- Export that function in `src/traces/Filters.tsx` by giving it a meaningful name.
- In `src/traces/MainFilter.tsx`, add the name of filter in
  `sortedNames` (the order matters, as filters are applied in
  sequence; it is however a good practice to make sure that filters
  are reasonably independant from each others).

## Filter chain

Filters are grouped in an `FilterChain` object, which is an object
satisfying the `Filter` interface defined below, but on a sequence of
filters. It defines how filters are combined. The first filter is
always `FromJson`, which parses the raw entry to produce a
`RegularEvent` event. In theory it is possible to nest `FilterChains`,
for example when supporting different versions of the trace format
(ie. one filter is activated when the version is v1, the other when
the version is v2, and each filter is actually a chain).

## Writing a filter

Generally speaking, a filter receives an event from a *previous
filter* and emits an array of zero or more events for some *next
filter*.

A filter is instantiated with a shared *environment* object. This is
used to aggregate data when processing a trace of events.

A filter object may implement zero or all of the following methods.
They are listed here in the order by which they are called during the
filter's lifecycle.


- `reset`: called first to reset the filter state. No output is
  expected from this function. This may be useful to allocate
  resources and/or initialize values from the global environment.

- `init`: called without any argument to produce an initial sequence
  of events. The events are then processed as ususal by the next
  filters in chain (calling `step` if `test` is true), after which the
  next filter may in turn add its own initial events.

- `test` : called on an *event* to determine if the filter should
  process it. If the function returns false, the event is rejected and
  passed down to the next filter. The helper module `FilterUtils.tsx`
  define higher-order functions that creates predicates, like
  `isTagged(tag)` or the combination of such predicates using
  `and(...)` and `or(...)`. Note in particular the existence of
  `onFalse(test, callback)` that performs the side-effect `callback`
  when the event is rejected. This is sometimes useful in filters to
  update a state (see `Stepper.tsx`).

- `step` : takes an event that satisfies `test` and produce zero or
  more events.

- `done` : like `init` but when the trace of events is completed. In a
  chain, the function is called to produce an array of events to be
  processed by the next filter (with `test` and `step`); then, the
  filter itself may add its own last events, etc.

- `exit` : called when all the events are processed; this may be used
  to store values in the global environment and/or free resources.

- `initRow`: takes a `RowProps` object and return a possibly
  modified `RowProps` object. It is called in a second phase, when
  all events are rendered in the interface. For example, when adding
  *breadcrumbs* information in a table, the associated filter's
  `initRow` function will make sure there is a cell available in
  the left margin of the current row specifically for this information
  (for all events!). The `BreadcrumbEvent` events can then fill the
  associated cell when they render themselves. This ensures that the
  right amount of columns exist in the generated table. More generally
  this is helpful to ensure that there is a uniform layout and to
  exchange data between a filter and the events that it
  generates. Inside a `FilterChain` object, the function is given an
  initial `RowProps` object, and each `initRow` function in the
  filter chain may update it.


[tef]: https://chromium.googlesource.com/catapult/+/refs/heads/main/docs/trace-event-format.md
[ocamltrace]: https://github.com/c-cube/ocaml-trace
[colibri2]: https://colibri.frama-c.com/
[vite]: https://vitejs.dev/
