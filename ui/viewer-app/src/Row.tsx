import './Row.css'
import {useState, useCallback, Children} from 'react';

function Margin ({cls, rowProps, items}) {
  return Children.toArray(
    items.map(
      item => <td className={cls}>{item}</td>
    ));
}

function Axis ({rowProps, value}) {
  if (!value)
    return <td></td>;
  return  <td className="axis"
            onClick={rowProps.selectable ? (e) => rowProps.onClick(e) : null}>
            {value}
          </td>;
}

function Main ({rowProps, title, items}) {
  return <>
           <td className="axis_main_sep">●</td>
           <td className="main">
             { Children.toArray(
               items.map(
                 item => <div>{item}</div>))}
           </td>
         </>;
}

function Scope ({rowProps, scope}) {
  return <td className="scope"></td>;
}

function onClick (rowProps) {
  if (rowProps.hooks.selected) {
    [get, set] = rowProps.hooks.selected;
    set(false);
  }
}

function selectRow(rowProps) {
  if (rowProps.environment.currentSelection) {
    const [idx, oldSetter] = rowProps.environment.currentSelection;
    oldSetter(false);
    if (idx == rowProps.index) {
      // deselect and leave
      rowProps.environment.currentSelection = undefined;
      if (rowProps.environment.onSelectionChange) {
        rowProps.environment.onSelectionChange(undefined);
      };
      return;
    }
  }
  const [_, newSetter] = rowProps.hooks.selected;
  rowProps.environment.currentSelection = [rowProps.index, newSetter];
  newSetter(true);
  if (rowProps.environment.onSelectionChange) {
    rowProps.environment.onSelectionChange(rowProps);
  };
}

export function Row ({rowProps}) {
  if (!rowProps)
    return null;
  let classes = ["row"].concat(rowProps.classes);
  if (rowProps.selectable) {
    rowProps.hooks = {
      ...rowProps.hooks,
      selected: useState(false)
    };
    let [selected, setSelected] = rowProps.hooks.selected;
    if (selected) {
      classes = classes.concat(["selected"]);
    }
    rowProps.onClick = useCallback((e) => {
      selectRow(rowProps)
    }, []);
  }
  return (
    <tr
      /* hidden={!visible} */
      key={rowProps.id}
      className={classes.filter((x) => x !== null).join(' ')}>
      <Margin rowProps={rowProps} cls={"left"} items={rowProps.leftMargin.reverse()}></Margin>
      <td className="targetted" onClick={rowProps.onClick}></td>
      <Axis rowProps={rowProps} value={rowProps.axis}></Axis>
      <Main rowProps={rowProps} items={rowProps.main}></Main>
      <Scope rowProps={rowProps} scope={rowProps.scope}></Scope>
      <Margin rowProps={rowProps} cls={"right"} items={rowProps.rightMargin}></Margin>
    </tr>
  );
}
