import {Children} from 'react';
import './Components.css';

export function Link ({id, target, children}) {
  return <a
           id={id}
           href={`#${target}`}>
           {children}
         </a>;
}

export function Timestamp ({ts}) {
  return <span className="timestamp">{ts.toFixed(3)}</span>;
}

export function KeyValue ({name, value}) {
  return <tr className="kv">
           <td>{name}</td>
           <td>{value}</td>
         </tr>;
}

export function KeyValueTable ({children}) {
  const rows = Children.toArray(children);
  return <table className="key-value-pairs">
           <tbody>
             {rows}
           </tbody>
         </table> ;
}

export function ObjectKeyValueTable ({object}) {
  return <KeyValueTable>
           {Children.toArray(
             Object.entries(object).map(
               ([k,v]) => <KeyValue name={k} value={v}>
                          </KeyValue>))}
         </KeyValueTable>;
}

export function SingleKeyValueTable({name, value}) {
  return <KeyValueTable>
           <KeyValue name={name} value={value}>
           </KeyValue>
         </KeyValueTable>;
}
