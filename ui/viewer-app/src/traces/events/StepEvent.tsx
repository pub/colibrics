import * as ui from '../Components.tsx';
import './StepEvent.css'

function Step({step, id}) {
  return <span>{`step ${step}`}</span>;
}

function render(event, rowProps) {
  return {
    ...rowProps,
    classes: rowProps.classes.concat(["step"]),
    // main: [<code><textarea></textarea></code>],
    axis: <Step step={event.step} id={rowProps.id}></Step>
  };
}

export default function StepEvent(step) {
  return {
    tags: ["step"],
    step: step,
    render: [render]
  };
}
