export default function CounterEvent(name, value) {
  return {
    tags: ["counter"],
    name: name,
    value: value,
    render: [() => null]
  };
}
