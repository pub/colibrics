import * as ui from '../Components.tsx';
import './InfoEvent.css'

function render (event, rowProps) {
  return {
    ...rowProps,
    classes: rowProps.classes.concat(["info"]),
    axis: <></>,
    main: [
      <ui.SingleKeyValueTable name={event.key} value={event.value}>
      </ui.SingleKeyValueTable>
    ]
  };
}

export default function InfoEvent (k, v) {
  return { 
    tags: ["info"], 
    key: k, 
    value: v, 
    render: [render] };
}
