import * as rp from '../RowProps.tsx';
import './BreadcrumbEvent.css';

import { Children } from 'react';

function renderCrumb(event, rowProps) {
  let contents = null;
  const [common, popped, pushed] = event.breadcrumb;

  if (common.length + popped.length + pushed.length == 0)
    return rowProps;

  const renderItems = (crumbs, here, there) => {
    let items = Children.toArray(
      crumbs.map(
        c => {
          const same = (u) => c.id == u.id && c.queue == u.queue;
          const text = `${c.queue}/${c.id}`;
          return (rowProps.environment.stale_breadcrumbs.find(same))
            ? <a className={"deadlink"} title={"never woken up"}>
                {text}
              </a>
            : <a
                id={rp.storeIndex(rowProps, `${here}${c.queue}${c.id}`)}
                href={`#${there}${c.queue}${c.id}`}>
                {text}
              </a>;
        }));
    return <div>{items}</div>;
  }

  const render = (crumbs, title, cls, here, there) => {
    if (!crumbs.length)
      return null;
    return <>
             <dt className={cls}>{title}</dt>
             <dd className={cls}>{renderItems(crumbs, here, there)}</dd>
           </>;
  }
  contents = <dl>
               {render(pushed, "delay", "push", "bcin", "bcout")}
               {render(popped, "wake up", "pop", "bcout", "bcin")}
               {/* {render(common, "delay and wake up", "immediate")} */}
             </dl>;

  const cell = <div className="breadcrumbs">{contents}</div>;
  rowProps.leftMargin[rowProps.breadcrumb] = cell;
  return rowProps;
}

export default function BreadcrumbEvent (parent, common, popped, pushed) {
  return {
    ...parent,
    render: [...parent.render, renderCrumb],
    breadcrumb: [ common, popped, pushed ]
  };
}
