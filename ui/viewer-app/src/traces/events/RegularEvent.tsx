import * as ui from '../Components.tsx';

function regularRender (event, rowProps) {
  rowProps.rightMargin[rowProps.debugIndex] = (
    <code className="annotate debug">
      {event.ph}/{event.tags.join('|')}
    </code>
  );
  return {
    ...rowProps,
    axis: <ui.Timestamp ts={event.ts}></ui.Timestamp>,
    main: [
      ...rowProps.main,
      <code>{event.name}</code>,
      <div className="annotate">
        <ui.ObjectKeyValueTable
          object={event.args}>
        </ui.ObjectKeyValueTable>
      </div>
    ],
    selectable: true
  };
}

export default function RegularEvent (ts, name, ph, args) {
  return {
    tags: ["regular", "ts"],
    ts: ts,
    name: name,
    ph: ph,
    args: args || [],
    render: [regularRender]
  };
}
