import CounterEvent from '../events/CounterEvent.tsx';
import RegularEvent from '../events/RegularEvent.tsx';
import * as rp from '../RowProps.tsx'

export default function Filter (Env) {
  return {
    description: "Read from Json entry in TEF format",
    initRow: (rowProps) => {
      rowProps.debugIndex = rp.reserveIndex(rowProps.rightMargin);
      return rowProps;
    },
    step : (t) => {
      if (t.ph == "C") {
        let [n,v] = Object.entries(t.args)[0];
        return [CounterEvent(n,v)];
      } else {
        return [RegularEvent(t.ts, t.name, t.ph, t.args)];
      }
    }
  };
}
