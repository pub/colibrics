import {and, isTagged, isPh} from '../FilterUtils';
import InfoEvent from '../events/InfoEvent.tsx'

export default function Filter (env) {
  return {
    description: "Observe counter events",
    test : isTagged("counter"),
    step : (t) => { 
      if (env.onCounter) {
        env.onCounter(t.name, t.value);
      }
      // leave event for other filters
      return [t];
    }
  };
}
