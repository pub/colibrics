import {isTagged} from '../FilterUtils';
import InfoEvent from '../events/InfoEvent.tsx';
import CounterEvent from '../events/CounterEvent.tsx';
import BreadcrumbEvent from '../events/BreadcrumbEvent.tsx';
import * as rp from '../RowProps.tsx'

export default function Filter (env) {
  let common;
  let pushed;
  let popped;

  let active;
  
  // 1. pop never happen before a corresponding push
  // 2. there is no risk of having duplicates ids in lists

  const reset = () => {
    common = [];
    pushed = [];
    popped = [];
  };

  const equal = (a, b) => a.id == b.id && a.queue == b.queue;
  const equalTo = (x) => ((y) => equal(x, y));

  const transform = (event) => {
    common = pushed.filter(c => popped.find(equalTo(c)));
    pushed = pushed.filter(c => !common.find(equalTo(c)));
    popped = popped.filter(c => !common.find(equalTo(c)));
    event = BreadcrumbEvent(event, common, popped, pushed);
    reset();
    return event;
  };

  return {
    description: "Enrich events with breadcrumbs data",
    reset: () => {
      reset();
      active = [];
    },
    test: isTagged("regular"),
    exit: () => {
      env.stale_breadcrumbs = active;
    },
    step: (t) => {
      const crumb = t.args;
      switch(t.name) {
        case "breadcrumb_push":
          active.push(crumb);
          pushed.push(crumb);
          return [];
        case "breadcrumb_pop":
          const pos = active.findIndex(equalTo(crumb));
          const last = active.pop();
          if (pos != active.length) {
            active[pos] = last;
          }
          popped.push(crumb);
          return [];
        default:
          let event = transform(t);
          reset();
          return [event];
      }
    },
    initRow: (rowProps) => {
      return {
        ...rowProps,
        breadcrumb: rp.reserveIndex(rowProps.leftMargin)
      };
    }
  };
}
