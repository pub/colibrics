import {isTagged} from '../FilterUtils';
import InfoEvent from '../events/InfoEvent.tsx'

export default function Filter (env) {
  let origin = null;
  let stepper = null;

  const next_steps = (t) => [{
    ...t,
    ts : (t.ts - origin)
  }];

  const first_step = (t) => {
    origin = t.ts;
    stepper = next_steps;
    return [
      InfoEvent("time_origin", origin),
      { ...t, ts : 0 }
    ];
  }

  return {
    description: "Make timestamps relative to first event",
    reset: () => {
      stepper = first_step;
    },
    test: isTagged("ts"),
    step : (t) => {
      return stepper(t);
    }
  };
}
