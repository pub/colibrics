// A FilterChain is a filter that applies of a chain of filters.
//
// For a given event, each filter is applied in cascade, produce zero
// or more new events, which are fed to the next filter, etc.
//

export default function FilterChain (env, filters) {

  // Normalize filters
  filters = filters.map((filter) => ({
    ...filter,
    env     : filter.env    || env,
    test    : filter.test   || ((t) => true),
    init    : filter.init   || (( ) => []),
    reset   : filter.reset  || (( ) => undefined),
    exit    : filter.exit   || (( ) => undefined),
    step    : filter.step   || ((t) => [t]),
    done    : filter.done   || (( ) => []),
    initRow :  filter.initRow || ((l) => l)
  }));

  // Map a function f generating arrays and concatenate them
  const mapconcat = (f, seq) =>
    seq.reduce((res, v) =>
      res.concat(f(v)), []);

  // transform with f only if test t is satisfied
  const applyIf = (t, f) =>
    (v) => t(v) ? f(v) : [v];

  // Step function of the current chain filter, which apply all
  // filters in cascade

  const chainStep = (event) =>
    filters.reduce((trace, filter) => {
      let res = mapconcat(applyIf(filter.test, filter.step), trace);
      if (env.debug && env.debugLevel > 1) {
        console.log("step for " + filter.name);
        console.log(res);
      }
      return res;
    }, [event]);

  // Filter

  return {
    // keep track of environment
    env: env,
    // transform according to each filter
    step : chainStep,
    reset: () => filters.map((f) => f.reset()),
    // some filters may add events initially (which must
    // be filtered by the next filters using chainStep)
    init : () =>
      filters.reduce((trace, filter) => {
        let res = mapconcat(applyIf(filter.test, filter.step), trace).concat(filter.init())
        if (env.debug) {
          console.log("init for filter " + filter.name);
          console.log(res);
        }
        // Process all the incoming events from past
        // filters and add our own initial events.
        return res;
      }, []),

    // some filters may retain information until later, here they
    // may produce a final sequence of events, all of them being
    // processed by chainStep too.
    done : () =>
      filters.reduce((trace, filter) => {
        let res = mapconcat(applyIf(filter.test, filter.step), trace).concat(filter.done());
        if (env.debug) {
          console.log("done for filter " + filter.name);
          console.log(res);
        }
        // Process all the incoming events from past filters
        // and add our own remaining events.
        return res;
      }, []),
    exit: () => filters.map((f) => f.exit()),
    initRow: (i) => filters.reduce((l,f) => f.initRow(l),i)
  };
}
