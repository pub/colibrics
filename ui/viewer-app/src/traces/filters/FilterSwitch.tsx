// A FilterSwitch is a filter that applies one filter among many.
//
// (in order to select an appropriate filter given a trace version)
//

export default function FilterSwitch (env, filters) {
  // Normalize filters
  filters = filters.map((filter) => ({
    ...filter,
    env    : filter.env    || env,
    test   : filter.test   || ((t) => true),
    init   : filter.init   || (( ) => []),
    reset  : filter.reset  || (( ) => undefined),
    exit   : filter.exit   || (( ) => undefined),
    step   : filter.step   || ((t) => [t]),
    done   : filter.done   || (( ) => []),
    render : filter.render || ((l) => l)
  }));
}
