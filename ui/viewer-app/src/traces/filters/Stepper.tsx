import {and, isTagged, onFalse, keyMatch} from '../FilterUtils';
import CounterEvent from '../events/CounterEvent.tsx';
import StepEvent from '../events/StepEvent.tsx';

export default function Filter (env) {
  // some other events occured between steps
  let someEvents;

  const clearStep = () => {
    someEvents = true;
  };

  return {
    description: "Inject step-changed events",
    test: onFalse(and(isTagged("counter"), 
      keyMatch("name", "step")),
      clearStep),
    reset: () => {
      someEvents = false;
    },
    init: () => [
      CounterEvent("step",0),
      StepEvent(0)
    ],
    step: (t) => {
      // sequences of step events without intermediate events are
      // noisy, only add a step if there has been at least one other
      // event during the previous step.
      if (!someEvents)
        return [t];
      someEvents = false;
      return [t, StepEvent(t.value)];
    }
  };
}
