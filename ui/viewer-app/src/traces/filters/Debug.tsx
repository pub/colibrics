import * as utils from '../FilterUtils';

let counter = 0;

export default function Filter (env) {
  counter = counter + 1;
  let local_counter = counter;
  let event_counter = 0;
  return {
    description: "Debug filter",
    counter: local_counter,
    reset: () => {
      // having a Debug filter turns on debugging
      env.debug = true;
      event_counter = 0;
    },
    step : (t) => {
      if (env.debugTraceLimit) {
        if (event_counter >= env.debugTraceLimit) {
          // after a defined number of events, turn of debugging
          env.debug = false;
          return [];
        }
        event_counter = event_counter + 1;
      }
      console.log("debug counter " + local_counter);
      console.log(t);
      return [t];
    }
  };
}
