import { JSONParser } from '@streamparser/json';

class TraceLoader {
  async onFile(file, props) {
    if (! props.onResult)
      throw("onResult missing");

    const reader = file.stream().getReader();
    const decoder = new TextDecoder('utf-8');
    const parser = new JSONParser({
      stringBufferSize: undefined,
      paths: ['$.*']
    });
    const trace = [];
    if (props.init) {
      trace.push(...props.init());
    }
    if (props.step) {
      parser.onValue = ({value}) => {
        trace.push(...props.step(value));
      }
      let result;
      while (!(result = await reader.read()).done) {
        parser.write(decoder.decode(result.value));
        if (props.traceLoaderEarlyQuit) {
          console.log("trace loader early quit (for debugging)");
          break;
        }
      }
    } else {
      console.log("warning: no step function");
    }
    if (props.done) {
      trace.push(...props.done());
    }
    props.onResult(trace);
  }
}

export default TraceLoader
