import FilterChain from './filters/FilterChain.tsx'
import * as filters from './BasicFilters.tsx'
import {useState} from 'react';

const sortedNames = [
  "RelativeTime",
  "Breadcrumbs",
  "Stepper",
  "Counters",
  // "Debug",
]

export default function MainFilter (env) {
  return FilterChain(env, ["FromJson", ...sortedNames].map(
    name => {
      let filter = {
        ...filters[name](env),
        name: name
      };
      if (env.debug)
        console.log(filter);
      return filter;
    }));
}
 
