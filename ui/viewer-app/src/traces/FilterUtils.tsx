
export function and(...tests) {
  return (event) => {
    return tests.every(t => t(event));
  };
}

export function or(...tests) {
  return (event) => {
    return tests.some(t => t(event));
  };
}

export function isTagged(t) {
  return (event) => {
    return event.tags.includes(t);
  };
}

export function isPh(ph) {
  return (event) => {
    return event.ph == ph;
  };
}

export function argsIncludes(key) {
  return (event) => {
    return key in event.args;
  };
}

export function keyMatch(key, value) {
  return (event) => {
    return (key in event) && (event[key] == value);
  };
}

// if test fails, call another function as a side-effect
export function onFalse(test, callback) {
  return (v) => {
    const result = test(v);
    if (!result) {
      callback();
    }
    return result;
  }
}
