import './TraceView.css'

import {Row} from './Row.tsx';
import {Children} from 'react';

const toRowProps = (filter, event, rowProps) =>
  event.render.reduceRight(
    /* apply all event's render functions */
    (rp, f) => rp && f(event, rp),
    /* starting from rowProps initialized by all filters */
    filter.initRow({
      ...rowProps,
      // list of css classes of current row
      classes: [],
      // index increases => further from the center
      leftMargin: [],
      // index increases => further from the center
      rightMargin: [],
      // spans are not necessarily nested
      spans: [], // unused for now
      // current scope list
      scope: [],
      // step/cycles/time
      axis: null,
      // list of content
      main: []
    }))

export default function TraceView ({rows, filter, onSelectionChange}) {
  const commonRowProps = {
    // keep track of the environment too
    environment: filter.env,
    // React hooks
    hooks : {
    }
  };
  commonRowProps.environment.onSelectionChange = onSelectionChange;
  return <div className="traceView">
           { (rows.length == 0)
             ? "No trace loaded"
             : <table>
                 <tbody>
                   {Children.toArray(
                     rows.map((event, index) => {
                       const rowProps = toRowProps(filter, event, {
                         ...commonRowProps,
                         index: index
                       });
                       if (!rowProps) {
                         return null;
                       }
                       return <Row rowProps={rowProps}></Row>;
                     }))}
                 </tbody>
               </table>}
         </div>;
}
