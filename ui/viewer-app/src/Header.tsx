import c2black from '/colibri2_black.svg'
import c2blue from '/colibri2_blue.svg'
import c2pink from '/colibri2_pink.svg'

import './Header.css'

export default function Header () {
  return (
      <div className="header">
        <div className="logo">
          <img src={c2black} className="bird bird-black" alt="Colibri2 logo" />
          <img src={c2blue} className="bird bird-blue" alt="Colibri2 logo" />
          <img src={c2pink} className="bird bird-pink" alt="Colibri2 logo" />
        </div>
        <div><h1>colibri2-trace-viewer</h1></div>
      </div>
  );  
}
