import './FileLoader.css'

export default function FileLoader({busy, file, onFile, clear}) {
  const select = (e) => {
    const file = e.target.files[0];
    onFile(file);
  };

  return (
    <div className="fileLoader">
      <form>
        <input
          disabled={busy}
          type="file"
          accept="application/json"
          onChange={select}>
        </input>
        <div hidden={!file || busy}>
          <button type='button' onClick={() => onFile(file)}>Reload</button>
          <button type='button' onClick={clear}>Clear</button>
        </div>
        <div hidden={!busy}>Loading...</div>
      </form>
    </div>
  );
}
