import TraceLoader from './traces/TraceLoader.tsx'
import MainFilter from './traces/MainFilter.tsx'

import FileLoader from './FileLoader.tsx'
import TraceView from './TraceView.tsx'
import LogView from './Log.tsx'
import Header from './Header.tsx'
import './App.css'

import {useState, useEffect} from 'react';
const loader = new TraceLoader();

export default function Root() {
  const [file, setFile] = useState(null);
  const [busy, setBusy] = useState(false);
  const [rows, setRows] = useState([]);
  const [filter, _] = useState(MainFilter({
    onCounter: null,
    debugTraceLimit : null, // 25,
    debug: false,
    debugLevel: 1
  }));

  const clear = () => {
    setFile(null);
    setRows([]);
  };

  const onFile = (f) => {
    setRows([]);
    setFile(f);
    loader.onFile(f, {
      traceLoaderEarlyQuit: false,
      init: () => {
        setBusy(true);
        filter.reset();
        return filter.init();
      },
      done: () => {
        let result = filter.done();
        filter.exit();
        return result;
      },
      step: filter.step,
      onResult: (trace) => {
        setBusy(false);
        setRows(trace);
      }
    });
  }

  const onSelectionChange = (e) => {
    // TODO link with other parts
    // console.log("selection changed", e);
  };

  return (
    <>
      <Header></Header>

      <FileLoader
        busy={busy}
        file={file}
        clear={clear}
        onFile={onFile}>
      </FileLoader>

      <TraceView rows={rows}
        filter={filter}
        onSelectionChange={onSelectionChange}>
      </TraceView>
    </>
  )
}
