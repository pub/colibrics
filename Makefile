##########################################################################
#  This file is part of Colibri2.                                        #
#                                                                        #
#  Copyright (C) 2014-2021                                               #
#    CEA   (Commissariat à l'énergie atomique et aux énergies            #
#           alternatives)                                                #
#                                                                        #
#  you can redistribute it and/or modify it under the terms of the GNU   #
#  Lesser General Public License as published by the Free Software       #
#  Foundation, version 2.1.                                              #
#                                                                        #
#  It is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of        #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
#  GNU Lesser General Public License for more details.                   #
#                                                                        #
#  See the GNU Lesser General Public License version 2.1                 #
#  for more details (enclosed in the file licenses/LGPLv2.1).            #
##########################################################################

all:
	dune build --root=$$(pwd) @install colibri2.opam colibrics.opam colibrilib.opam \
	colibri2/bin/colibri2_stage0.exe

test:
	dune runtest --root=$$(pwd)

check-ocamlformat:
	ocamlformat --check colibri2/theories/**/*.ml*

promote:
	dune promote --root=$$(pwd)

check:
	why3 prove --type-only -L src/lib  src/lib/*.mlw src/lib/constraints/*.mlw

replay:
	why3 replay -L src/lib  src/lib/cp

ide:
	why3 ide -L src/lib  src/lib/cp.mlw

doc:
	dune build @doc --root=$$(pwd)

WITH_HEADER_FILES=cp.mlw queens.mlw cp.ml cp.mli
DISTRIB_FILES=$(WITH_HEADER_FILES) Makefile dune dune-project cp.opam cp.drv Readme.md \
	$(addprefix src/, why3session.xml why3shapes.gz) \
	$(addprefix examples/, arith.ml arith.expected dune)

distrib: cp_why3.tar.gz

cp_why3.tar.gz: $(DISTRIB_FILES)
	tar --transform 's&^&cp_why3/&' -czf $@  $(DISTRIB_FILES)

compile-distrib: distrib
	rm -rf /tmp/cp_why3
	tar -xzf cp_why3.tar.gz -C /tmp
	make -C /tmp/cp_why3
	make -C /tmp/cp_why3 test-examples
	rm -rf /tmp/cp_why3

replay-distrib: distrib
	rm -rf /tmp/cp_why3
	tar -xzf cp_why3.tar.gz -C /tmp
	make -C /tmp/cp_why3
	make -C /tmp/cp_why3 replay
	rm -rf /tmp/cp_why3

headers_colibrics:
	headache -h CEA_PROPRIETARY -c headache.config $(WITH_HEADER_FILES)

test-examples:
	dune runtest --root=$$(pwd)


install: build-install
	dune install

uninstall:
	dune uninstall

###############
# file headers
###############

WHY3_FILES = $(addprefix colibri2/popop_lib/, cmdline.ml cmdline.mli	\
	exn_printer.ml exn_printer.mli hashcons.ml		\
	hashcons.mli lists.ml lists.mli number.ml number.mli	\
	opt.ml opt.mli pp.ml pp.mli print_tree.ml print_tree.mli		\
	popop_stdlib.ml popop_stdlib.mli strings.ml strings.mli sysutil.ml	\
	sysutil.mli util.ml util.mli weakhtbl.ml		\
	weakhtbl.mli ) $(addprefix colibri2/stdlib/, debug.ml debug.mli)

OCAML_FILES = $(addprefix colibri2/popop_lib/, map_intf.ml exthtbl.ml	\
	exthtbl.mli extmap.ml extmap.mli extset.ml extset.mli )

FRAMAC_FILES = $(addprefix colibri2/popop_lib/, intmap.ml intmap_hetero.ml	\
	intmap.mli intmap_hetero.mli bag.ml bag.mli)

JC_FILES = $(addprefix colibri2/popop_lib/, leftistheap.ml leftistheap.mli)

WITAN_FILES = colibri2/stdlib/std.ml colibri2/stdlib/std.mli		\
	colibri2/stdlib/hashtbl_hetero.ml					\
	colibri2/stdlib/hashtbl_hetero.mli					\
	colibri2/stdlib/hashtbl_hetero_sig.ml				\
	colibri2/stdlib/map_hetero.ml colibri2/stdlib/map_hetero.mli	\
	colibri2/stdlib/map_hetero_sig.ml colibri2/stdlib/keys.mli	\
	colibri2/stdlib/keys_sig.ml colibri2/stdlib/comp_keys.ml	\
	colibri2/stdlib/comp_keys.mli colibri2/bin/options.ml

OCAMLPRO_FILES = \
	$(filter-out colibri2/theories/array/dune colibri2/theories/nseq/dune, \
		$(wildcard colibri2/theories/array/* colibri2/theories/nseq/* \
			colibri2/theories/LRA/DL/* )) \
	$(addprefix colibri2/theories/utils/, nsa_content.ml nsa_content.mli)


COLIBRI2_FILES = Makefile $(filter-out $(WHY3_FILES) $(OCAML_FILES)	\
	$(FRAMAC_FILES) $(JC_FILES) $(WITAN_FILES) $(OCAMLPRO_FILES) ,	\
	$(wildcard colibri2/*/*/*.ml* colibri2/*/*.ml* colibri2/*.ml*))

COLIBRICS_FILES =


headers:
	headache -c misc/headache_config.txt -h misc/header_colibrics.txt	\
		$(COLIBRICS_FILES)
	headache -c misc/headache_config.txt -h misc/header_colibri2.txt	\
		$(COLIBRI2_FILES)
	headache -c misc/headache_config.txt -h misc/header_witan.txt	\
		$(WITAN_FILES)
	headache -c misc/headache_config.txt -h misc/header_why3.txt	\
		$(WHY3_FILES)
	headache -c misc/headache_config.txt -h misc/header_ocaml.txt	\
		$(OCAML_FILES)
	headache -c misc/headache_config.txt -h misc/header_cea_ocamlpro.txt	\
		$(OCAMLPRO_FILES)
	headache -c misc/headache_config.txt -h		\
		misc/header_framac.txt $(FRAMAC_FILES)
	headache -c misc/headache_config.txt -h		\
		misc/header_jc.txt $(JC_FILES)

.PHONY: clean doc all install uninstall remove reinstall test

clean:
	dune clean --root=$$(pwd)

PROJECT_ID=879
PACKAGE_URL="https://git.frama-c.com/api/v4/projects/$(PROJECT_ID)/packages/generic/colibri2/$(TAG)/colibri2-$(TAG).tbz"
DESCRIPTION="$(shell sed -n -e "p;n;:next;/^##/Q;p;n;b next" CHANGES | perl -pe 's/\n/\\n/')"

release:
	@echo -n $(DESCRIPTION)
	@echo "Is the CHANGES correct for $(TAG) (y/n)?"
	@read yesno; test "$$yesno" = y
	dune-release tag $(TAG)
	dune-release distrib --skip-build --skip-lint
	curl --header "PRIVATE-TOKEN: $(GITLAB_TOKEN)" \
	--upload-file _build/colibri2-$(TAG).tbz \
	$(PACKAGE_URL)
	echo $(PACKAGE_URL) > _build/asset-$(TAG).url
	git push origin $(TAG)
	curl --header 'Content-Type: application/json' \
	--header "PRIVATE-TOKEN: $(GITLAB_TOKEN)" \
	--request POST "https://git.frama-c.com/api/v4/projects/$(PROJECT_ID)/releases" \
	--data '{"name": "Release $(TAG)","tag_name": "$(TAG)", "description": $(DESCRIPTION), "milestones": [], "assets": { "links": [{ "name": "colibri2-$(TAG).tbz", "url": $(PACKAGE_URL), "link_type":"other" }] } }'
	dune-release opam pkg
	dune-release opam submit
