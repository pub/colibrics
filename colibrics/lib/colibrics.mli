(**************************************************************************)
(*                                                                        *)
(*  Copyright (C) 2019-2019                                               *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  All rights reserved.                                                  *)
(*  Contact CEA LIST for licensing.                                       *)
(*                                                                        *)
(**************************************************************************)

exception Unsat
exception UnknownVariable

module Model: sig
  type t
end

module Context: sig
  type t
  val create: unit -> t
  val solve: t -> Model.t
end

module Var : sig
   module I : sig
     type t
     val mk : min:Z.t -> max:Z.t -> Context.t -> t
     val get: Model.t -> t -> Z.t
     val zero: t
   end
   module B : sig
     type t
     val mk : Context.t -> t
     val get: Model.t -> t -> bool
     val true_: t
   end
end

module Constraint : sig
  val is_cst_reif: Context.t -> Var.I.t -> Z.t -> Var.B.t -> unit
  val is_cst: Context.t -> Var.I.t -> Z.t -> unit
  val add_reif: Context.t -> Var.I.t -> Var.I.t -> Var.I.t -> Var.B.t -> unit
  val add: Context.t -> Var.I.t -> Var.I.t -> Var.I.t -> unit
  val le_reif: Context.t -> Var.I.t -> Var.I.t -> Z.t -> Var.B.t -> unit
  val le: Context.t -> Var.I.t -> Var.I.t -> Z.t -> unit
  val or_: Context.t -> Var.B.t -> Var.B.t -> Var.B.t -> unit
  val not_: Context.t -> Var.B.t -> Var.B.t -> unit
  val is_true: Context.t -> Var.B.t -> unit
  val equiv: Context.t -> Var.B.t -> Var.B.t -> Var.B.t -> unit
end
