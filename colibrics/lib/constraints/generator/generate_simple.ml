type cstr = {
  file : string
; module_ : string [@key "module"]
; name : string
} [@@ deriving yojson]

module LOG = (val (Logs.src_log (Logs.Src.create "colibrics.generate_simple")))

let () = Logs.set_reporter (Logs.format_reporter ())

let constraints =
  match [% of_yojson : cstr list] (Yojson.Safe.from_file Sys.argv.(1)) with
  | exception Yojson.Json_error s ->
    LOG.err (fun m -> m "Can't read json configuration: %s" s); exit 1
  | Error s -> LOG.err (fun m -> m "Can't read json configuration: %s" s); exit 1
  | Ok config -> config

let why3_path cstr =
  let l = String.split_on_char '.' cstr.file in
  String.concat "/" l

let ocaml_module cstr =
  let l = String.split_on_char '.' cstr.file in
  (String.concat "__" l) ^ "__" ^ cstr.module_

let result =
  Jingoo.Jg_template.from_file Sys.argv.(2)
    ~models:Jingoo.Jg_types.[
      "constraints",
      Tlist (List.map
               (fun cstr -> Tobj ["file",Tstr cstr.file;
                                  "module",Tstr cstr.module_;
                                  "name",Tstr cstr.name;
                                  "path",Tstr (why3_path cstr);
                                  "ocaml_module",Tstr (ocaml_module cstr)] )
               constraints)
    ]

let () =
  let cout = open_out Sys.argv.(3) in
  output_string cout result;
  close_out cout
