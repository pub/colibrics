module C
       (* v1 <= v2 + i  <-> b *)

     use cp.Type
     use cp.Var as Var
     use mach.int.Int
     use utils.extstd.Bool
     use set.Fset as S
     use cp.DomI
     use cp.DomB
     use seq.Seq as Seq
     use utils.extstd.Int63 as Int63

     use cp.ConstraintHelpers


     type t = { v1 : Var.ti; v2 : Var.ti; i : int; b : Var.tb }
     invariant { v1 <> Var.dummy_ti }
     invariant { v2 <> Var.dummy_ti }
     invariant { b <> Var.dummy_tb }
     by { v1 = { Var.vi = 1}; v2 = { Var.vi = 1}; i = 0; b = { Var.vb = 1} }

     function constraint_is_true_in (m:interp) (c:t) : bool =
        ((Interp.get_int m c.v1) <= (Interp.get_int m c.v2) + c.i)  = (Interp.get_bool m c.b)

     let ghost function vars (c:t) : Var.vars =
        Var.addi c.v1 (Var.addi c.v2 (Var.addb c.b Var.empty))

     let compute_vars_ti (c:t) (si:Var.Si.t) : unit
         ensures { S.(==) si.Var.Si.elts (S.union (old si.Var.Si.elts) (vars c).Var.si) }
         writes { si } =
         Var.Si.add si c.v1; Var.Si.add si c.v2

     let compute_vars_tb (c:t) (sb:Var.Sb.t) : unit
         ensures { S.(==) sb.Var.Sb.elts (S.union (old sb.Var.Sb.elts) (vars c).Var.sb) }
         writes { sb } =
         Var.Sb.add sb c.b

     let partial propagate (e:env) (c:t) : unit
         requires { Var.subset (vars c) (defined e) }
         ensures { forall m:interp.
                      old (is_true_in m e) ->
                      constraint_is_true_in m c ->
                      is_true_in m e
                 }
         ensures { forall m:interp. is_true_in m e -> old (is_true_in m e) }
         ensures { not (old e.dirty) -> e.dirty -> size e < size (old e) }
         ensures { 0 <= size e <= size (old e) }
         raises { Unsat -> forall m:interp. old (is_true_in m e) -> not (constraint_is_true_in m c) }
         writes { e.domi.Var.Hbi.contents,
                  e.dirty
                }
       =
          match get_bool e c.b with
          | DomB.V True  ->
            let d1 = get_int e c.v1 in
            let d2 = get_int e c.v2 in
            set_int e c.v1 { DomI.min = d1.DomI.min; DomI.max = d2.DomI.max + c.i };
            set_int e c.v2 { DomI.min = d1.DomI.min - c.i; DomI.max = d2.DomI.max }

          | DomB.V False  ->
            let d1 = get_int e c.v1 in
            let d2 = get_int e c.v2 in
            set_int e c.v1 { DomI.min = d2.DomI.min + c.i + 1; DomI.max = d1.DomI.max };
            set_int e c.v2 { DomI.min = d2.DomI.min; DomI.max = d1.DomI.max - c.i - 1}

          | DomB.Top -> ()
          end

     let check_model (mo:model) (c:t) : bool
         requires { Var.subset (vars c) (defined_model mo) }
         ensures {
          if result
          then forall m:interp. is_model_of m mo -> constraint_is_true_in m c
          else forall m:interp. is_model_of m mo -> not (constraint_is_true_in m c)
         }
         =
          eqb (((get_mod_int mo c.v1)) <= (get_mod_int mo c.v2) + c.i) (get_mod_bool mo c.b)

end
