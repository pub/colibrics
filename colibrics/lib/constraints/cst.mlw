module C

     use cp.Type
     use cp.Var as Var
     use mach.int.Int
     use utils.extstd.Bool
     use set.Fset as S
     use cp.DomI
     use cp.DomB
     use seq.Seq as Seq
     use utils.extstd.Int63 as Int63

     use cp.ConstraintHelpers

     type t = { v : Var.ti; i : int; b : Var.tb }
     invariant { v <> Var.dummy_ti }
     invariant { b <> Var.dummy_tb }
     by { v = { Var.vi = 1}; i = 0; b = { Var.vb = 1} }

     function constraint_is_true_in (m:interp) (c:t) : bool =
          eqb ((Interp.get_int m c.v) = c.i : bool) (Interp.get_bool m c.b : bool)

     let ghost function vars (c:t) : Var.vars =
         Var.addi c.v (Var.addb c.b Var.empty)

     let compute_vars_ti (c:t) (si:Var.Si.t) : unit
         ensures { S.(==) si.Var.Si.elts (S.union (old si.Var.Si.elts) (vars c).Var.si) }
         writes { si } =
         Var.Si.add si c.v

     let compute_vars_tb (c:t) (sb:Var.Sb.t) : unit
         ensures { S.(==) sb.Var.Sb.elts (S.union (old sb.Var.Sb.elts) (vars c).Var.sb) }
         writes { sb } =
         Var.Sb.add sb c.b

     let partial propagate (e:env) (c:t) : unit
         requires { Var.subset (vars c) (defined e) }
         ensures { forall m:interp.
                      old (is_true_in m e) ->
                      constraint_is_true_in m c ->
                      is_true_in m e
                 }
         ensures { forall m:interp. is_true_in m e -> old (is_true_in m e) }
         ensures { not (old e.dirty) -> e.dirty -> size e < size (old e) }
         ensures { 0 <= size e <= size (old e) }
         raises { Unsat -> forall m:interp. old (is_true_in m e) -> not (constraint_is_true_in m c) }
         writes { e.domi.Var.Hbi.contents,
                  e.dirty
                }
       =
          match get_bool e c.b with
          | DomB.V True ->
            let di = {DomI.min=c.i;DomI.max=c.i} in
            set_int e c.v di
          | DomB.V False ->
            let d1 = get_int e c.v in
            if d1.DomI.min = d1.DomI.max && d1.DomI.min = c.i then
              raise Unsat
          | DomB.Top ->
             ()
          end

     let check_model (mo:model) (c:t) : bool
         requires { Var.subset (vars c) (defined_model mo) }
         ensures {
          if result
          then forall m:interp. is_model_of m mo -> constraint_is_true_in m c
          else forall m:interp. is_model_of m mo -> not (constraint_is_true_in m c)
         }
         =
          eqb (get_mod_int mo c.v = c.i) (get_mod_bool mo c.b)

end
