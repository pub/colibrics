(**************************************************************************)
(*                                                                        *)
(*  Copyright (C) 2019-2019                                               *)
(*    CEA (Commissariat à l'énergie atomique et aux énergies              *)
(*         alternatives)                                                  *)
(*                                                                        *)
(*  All rights reserved.                                                  *)
(*  Contact CEA LIST for licensing.                                       *)
(*                                                                        *)
(**************************************************************************)

exception Unsat = Cp__Type.Unsat
exception UnknownVariable = All__APIDefensive.UnknownVariable

module Model = struct
  type t = All__APIDefensive.model
end

module Context = struct
  type t = All__APIDefensive.context
  let create = All__APIDefensive.create_context
  let solve = All__APIDefensive.solve
end

module Var = struct
   module I = struct
     type t = All__APIDefensive.ti
     let mk ~min ~max c = All__APIDefensive.create_vari c min max
     let get m i = All__APIDefensive.get_model_i m i
     let zero = All__APIDefensive.zero_
   end
   module B = struct
     type t = All__APIDefensive.tb
     let mk c = All__APIDefensive.create_varb c
     let get m i = All__APIDefensive.get_model_b m i
     let true_ = All__APIDefensive.true_
   end
end

module Constraint = struct
  let is_cst_reif = All__APIDefensive.is_cst_reif
  let is_cst = All__APIDefensive.is_cst
  let add_reif = All__APIDefensive.add_reif
  let add = All__APIDefensive.add
  let le_reif = All__APIDefensive.le_reif
  let le = All__APIDefensive.le
  let or_ = All__APIDefensive.or1
  let not_ = All__APIDefensive.not_
  let is_true = All__APIDefensive.is_true
  let equiv = All__APIDefensive.equiv
end
