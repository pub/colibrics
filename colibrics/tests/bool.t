  $ colibrics --lang=smt2 << EOF
  > (set-logic ALL)
  > (check-sat)
  > EOF
  sat

  $ colibrics --lang=smt2 << EOF
  > (set-logic ALL)
  > (assert true)
  > (check-sat)
  > EOF
  sat

  $ colibrics --lang=smt2 << EOF
  > (set-logic ALL)
  > (assert (or true true))
  > (check-sat)
  > EOF
  sat

  $ colibrics --lang=smt2 << EOF
  > (set-logic ALL)
  > (assert false)
  > (check-sat)
  > EOF
  unsat

  $ colibrics --lang=smt2 << EOF
  > (set-logic ALL)
  > (assert (or false true))
  > (check-sat)
  > EOF
  sat

  $ colibrics --lang=smt2 << EOF
  > (set-logic ALL)
  > (assert (not (or false true)))
  > (check-sat)
  > EOF
  unsat

  $ colibrics --lang=smt2 << EOF
  > (set-logic ALL)
  > (declare-fun a () Bool)
  > (assert a)
  > (check-sat)
  > EOF
  sat

  $ colibrics --lang=smt2 << EOF
  > (set-logic ALL)
  > (declare-fun a () Bool)
  > (assert a)
  > (assert (not a))
  > (check-sat)
  > EOF
  unsat

  $ colibrics --lang=smt2 << EOF
  > (set-logic ALL)
  > (declare-fun a () Bool)
  > (declare-fun b () Bool)
  > (assert (or (not a) b))
  > (assert a)
  > (assert (not b))
  > (check-sat)
  > EOF
  unsat
