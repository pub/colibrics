(* This file is free software, copied from dolmen. See file "LICENSE" for more information *)

open Loop

let handle_exn st _bt exn =
  let _st = Errors.exn st exn in
  exit 125

let finally st e =
  match e with
  | None -> st
  | Some (bt,exn) -> handle_exn st bt exn

let debug_pre_pipe st c =
  if State.get State.debug st then
    Format.eprintf "[pre] @[<hov>%a@]@." Dolmen.Std.Statement.print c;
  (st, c)

let debug_post_pipe st stmt =
  if State.get State.debug st then
    Format.eprintf "[post] @[<hov>%a@]@\n@." (Fmt.list ~sep:Fmt.comma Typer.print) stmt;
  (st, stmt)

let handle_stmt (st:Loop.State.t) (stmtl:Loop.Typer.typechecked Loop.Typer.stmt list) =
  let solve_state = State.get solver_state st in
  List.iter (fun (stmt:Loop.Typer.typechecked Loop.Typer.stmt)  ->
    match stmt.contents with
    | `Reset
    | `Get_unsat_core
    | `Other _
    | `Get_assignment
    | `Get_unsat_assumptions
    | `Push _
    | `Pop _
    | `Get_info _
    | `Get_proof
    | `Get_assertions
    | `Get_value _
    | `Get_option _
    | `Get_model
    | `Reset_assertions
    | `Set_option _ -> invalid_arg (Fmt.str "Unimplemented stmt: %a" Loop.Typer.print stmt)
    | `Decls d -> List.iter (Dolmen_to_colibrics.decl solve_state) d
    | `Defs d -> List.iter (Dolmen_to_colibrics.def solve_state) d
    | #Dolmen_to_colibrics.assume as d -> Dolmen_to_colibrics.hyp solve_state d
    | `Solve (hyps, []) -> begin match Dolmen_to_colibrics.solve solve_state hyps with
        | `Unsat -> Format.printf "unsat@."
        | `Sat -> Format.printf "sat@."
      end
    | `Solve (_, _) ->
      failwith
        "Unsupported statement: \"`Solve (hyps, goals)\" with an non-empty list of goals."
    | `Echo s -> Format.printf "%s@." s
    | `Set_info _
    | `Set_logic _
    | `Exit -> ()
  ) stmtl;
  st, ()


let () =
  let man = [
    `S Options.common_section;
    `P "Common options for the colibrics binary";
    `S Options.gc_section;
    `P "Options to fine-tune the gc, only experts should use these.";
    `S Cmdliner.Manpage.s_bugs;
    `P "You can report bugs at https://git.frama-c.com/";
    `S Cmdliner.Manpage.s_authors;
    `P "Francois Bobot <francois.bobot@cea.fr>"
  ] in
  let info = Cmdliner.Cmd.info ~man ~version:"0.1" "dolmen" in
  let cmd = Cmdliner.Cmd.v info Options.state in
  let st = match Cmdliner.Cmd.eval_value cmd with
    | Ok `Version | Ok `Help -> exit 0
    | Error (`Parse | `Term | `Exn) -> exit 2
    | Ok (`Ok opt) -> opt
  in
  let g =
    try Loop.Parser.parse_logic (State.get State.logic_file st)
    with exn ->
      let bt = Printexc.get_raw_backtrace () in
      handle_exn st bt exn
  in
  let st =
    let open Loop.Pipeline in
    run ~finally g st (
      (fix (op ~name:"expand" Loop.Parser.expand) (
          (op ~name:"debug_pre" debug_pre_pipe)
          @>>> (op ~name:"headers" Loop.Header.inspect)
          @>>> (op ~name:"typecheck" Loop.Typer.typecheck)
          @>|> (op ~name:"debug_post" debug_post_pipe)
          @>>> (op handle_stmt) @>>> _end
        )
      )
    )
  in
  let st = Loop.Header.check st in
  let _st = Dolmen_loop.State.flush st () in
  ()
