
module Expr = Dolmen.Std.Expr

module Hconst = Hashtbl.Make(Expr.Term.Const)
module Hterm = Hashtbl.Make(Expr.Term)

type t = {
  ctx : Colibrics.Context.t
; mapping: Colibrics.Var.B.t Hconst.t
}

let create () = {
  ctx = Colibrics.Context.create ();
  mapping = Hconst.create 10;
}

type decl =
  [ `Term_decl of Dolmen_std.Expr.term_cst
  | `Type_decl of Dolmen_std.Expr.ty_cst * Dolmen_std.Expr.ty_def option ]
  type def =
  [ `Instanceof of
      Dolmen_std.Id.t * Dolmen_std.Expr.term_cst * Expr.ty list *
      Dolmen_std.Expr.ty_var list *
      Dolmen_std.Expr.term_var list * Hterm.key
  | `Term_def of
      Dolmen_std.Id.t * Dolmen_std.Expr.term_cst *
      Dolmen_std.Expr.ty_var list *
      Dolmen_std.Expr.term_var list * Hterm.key
  | `Type_alias of
      Dolmen_std.Id.t * Dolmen_std.Expr.ty_cst *
      Dolmen_std.Expr.ty_var list * Expr.ty
  ]
type assume =
  [ `Clause of Dolmen_std.Expr.term list
  | `Goal of Dolmen_std.Expr.term
  | `Hyp of Dolmen_std.Expr.term ]
type solve = Dolmen_std.Expr.term list

let rec cstr_term st (t:Expr.Term.t) res : unit =
  match t with
  | { term_descr = Cst c; _} when Hconst.mem st.mapping c ->
    Colibrics.Constraint.equiv st.ctx (Hconst.find st.mapping c) res Colibrics.Var.B.true_
  | { term_descr = App ({ term_descr = Cst { builtin = Dolmen_std.Builtin.Or; _ }; _},_,l); _ } ->
      to_binary st Colibrics.Constraint.or_ res l
  | { term_descr = App ({ term_descr = Cst { builtin = Dolmen_std.Builtin.Neg; _ }; _ },_,[a]); _ } ->
    let va = Colibrics.Var.B.mk st.ctx in
    cstr_term st a va;
     Colibrics.Constraint.not_ st.ctx va res
  | { term_descr = Cst { builtin = Dolmen_std.Builtin.True; _ }; _ } ->
      Colibrics.Constraint.is_true st.ctx res
  | { term_descr = Cst { builtin = Dolmen_std.Builtin.False; _ }; _ } ->
    Colibrics.Constraint.not_ st.ctx Colibrics.Var.B.true_ res
  | _ -> invalid_arg (Fmt.str "unimplemented: %a" Expr.Term.print t)

and to_binary st cstr res = function
  | [] -> assert false
  | [a] -> cstr_term st a res
  | [a;b] ->
    let va = Colibrics.Var.B.mk st.ctx in
    let vb = Colibrics.Var.B.mk st.ctx in
    cstr_term st a va;
    cstr_term st b vb;
    cstr st.ctx va vb res
  | a::l ->
    let va = Colibrics.Var.B.mk st.ctx in
    let vb = Colibrics.Var.B.mk st.ctx in
    cstr_term st a va;
    to_binary st cstr vb l;
    cstr st.ctx va vb res


let decl st (d:decl) =
  match d with
  | `Term_decl ({ id_ty = ty; _ } as c)
    when Expr.Ty.equal Expr.Ty.bool ty  ->
    let b = Colibrics.Var.B.mk st.ctx in
    Hconst.add st.mapping c b
  | `Type_decl _ -> invalid_arg "unimplemented"
  | `Term_decl _ -> invalid_arg "unimplemented"

let def _st (_d:def) = invalid_arg "unimplemented"

let hyp st (d:assume) =
  match d with
  | `Goal _ -> invalid_arg "unimplemented"
  | `Hyp d -> cstr_term st d Colibrics.Var.B.true_
  | `Clause _ -> invalid_arg "unimplemented"

let solve st (d:solve) =
  match d with
  | [] -> begin
      match Colibrics.Context.solve st.ctx with
      | _ -> `Sat
      | exception Colibrics.Unsat -> `Unsat
    end
  | _ -> invalid_arg "unimplemented"
