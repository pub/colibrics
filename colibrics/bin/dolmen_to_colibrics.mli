type t

val create: unit -> t

type decl =
  [ `Term_decl of Dolmen_std.Expr.term_cst
  | `Type_decl of Dolmen_std.Expr.ty_cst * Dolmen_std.Expr.ty_def option ]
type def =
  [ `Term_def of
      Dolmen_std.Id.t * Dolmen_std.Expr.term_cst *
      Dolmen_std.Expr.ty_var list * Dolmen_std.Expr.term_var list *
      Dolmen_std.Expr.term
  | `Type_alias of
      Dolmen_std.Id.t * Dolmen_std.Expr.ty_cst * Dolmen_std.Expr.ty_var list * Dolmen_std.Expr.ty
   | `Instanceof of
      Dolmen_std.Id.t * Dolmen_std.Expr.term_cst * Dolmen_std.Expr.ty list *
      Dolmen_std.Expr.ty_var list * Dolmen_std.Expr.term_var list *
      Dolmen_std.Expr.term
  ]
type assume =
  [ `Clause of Dolmen_std.Expr.term list
  | `Goal of Dolmen_std.Expr.term
  | `Hyp of Dolmen_std.Expr.term ]
type solve = Dolmen_std.Expr.term list


val decl: t -> decl -> unit
val def: t -> def -> unit
val hyp: t -> assume -> unit
val solve: t -> solve -> [ `Sat | `Unsat ]
