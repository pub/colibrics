
(* This file is free software, part of dolmen. See file "LICENSE" for more information *)

module State = struct
  include Dolmen_loop.State
  let is_interactive _ = false
end
module Pipeline = Dolmen_loop.Pipeline.Make(State)

module Parser = Dolmen_loop.Parser.Make(State)
module Header = Dolmen_loop.Headers.Make(State)
module Typer = struct
  module T = Dolmen_loop.Typer.Typer(State)
  include T
  include Dolmen_loop.Typer.Make(Dolmen.Std.Expr)(Dolmen.Std.Expr.Print)(State)(T)

  let init_pipe = init
  let init = T.init
end

let solver_state : Dolmen_to_colibrics.t State.key = State.create_key ~pipe:"Colibrics" "solver_state"
