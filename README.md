# Colibrics and Colibri2

A prototype implementation of a CP solver for the smtlib. Reimplementation of
COLIBRICS written in Eclipse Prolog by Bruno Marre

## Installation ##

Using [opam](http://opam.ocaml.org/):

```shell
opam pin add https://git.frama-c.com/bobot/colibrics.git
```

## Development ##

```shell
opam repo add coq-released https://coq.inria.fr/opam/released
git clone --recurse-submodules https://git.frama-c.com/bobot/colibrics.git
cd colibrics
opam install why3.1.4.0 why3 core jingoo yojson logs core coq-flocq.3.4.2 coq-coq2html pp_loc ounit2
opam install --deps-only .
make
make test
```
