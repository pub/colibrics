TMPDIR=/tmp/benchsresult
TIMEOUT=10
ERRORMEASURE=0.04
MEMOUT=1000

all: 	rev-to-consider.aim.provedovertime.png \
	rev-to-consider.bmc-ibm.provedovertime.png \
	rev-to-consider.pigeon.provedovertime.png \
	master.master~1.aim.compare.png \
	master~1.master~2.aim.compare.png \
	master.master~2.aim.compare.png \
	evolution.aim.provedovertime.png \


#From a user readable list of commit make a list of sha1 (same ordering)
%.commits: ../.git/refs/heads/master
	remake $*
	grep -v "cvc4\|alt-ergo" $* > $@.tmp.pre || true
	grep "cvc4\|alt-ergo" $* > $@.tmp || true
	xargs -a $@.tmp.pre git rev-parse >> $@.tmp
	rm $@.tmp.pre
	mv $@.tmp $@

%.commit: ../.git/refs/heads/master
	git rev-parse $* > $@.tmp
	mv $@.tmp $@

cvc4.commit:
	echo cvc4 > $@

alt-ergo.commit:
	echo alt-ergo > $@

# Run Bench

#Run the test for some version and some bench
%.res:
	eval `echo $@ | sed -e "s/^\(.\+\)\.\(.\+\)\.\([^.]\+\)\.res$/\
	NAME=\"\1.\2\" \
	EXT=\"\2\" \
	COMMIT=\"\3\"/"`
	TEST=$EXT.test
	BIN=bin/$COMMIT.native
	mkdir -p $$(dirname $@)
	remake $TEST ../$NAME $BIN
	./$TEST $BIN ../$NAME $@ $COMMIT

#Construt the test for some syntax
#default
%.test:
	cat >> $@.tmp <<"EOF"
	#!/bin/sh
	(why3-cpulimit $(TIMEOUT) $(MEMOUT) -s $1 $2 || echo "\nError") > $3.tmp 2>&1
	if grep -q Error $3.tmp
	then echo "$4 $(TIMEOUT)" > $3
	else sed $3.tmp -e "s/^why3cpulimit time : \([0-9.]*\) s$/$4 \1/p" -n > $3
	fi
	rm $3.tmp
	EOF
	mv $@.tmp $@
	chmod u+x $@

smt2.test:
	cat >> $@.tmp <<"EOF"
	#!/bin/sh
	why3-cpulimit $(TIMEOUT) $(MEMOUT) -s $1 $2 > $3.tmp 2>&1
	if grep -q "^unsat\|sat$" $3.tmp
	then sed $3.tmp -e "s/^why3cpulimit time : \([0-9.]*\) s$/$4 \1/p" -n > $3
	else echo "$4 $(TIMEOUT)" > $3
	fi
	rm $3.tmp
	EOF
	mv $@.tmp $@
	chmod u+x $@


#Construct the binary for some version
bin/%.native:
	SRCDIR=$(TMPDIR)/$*
	mkdir -p $(TMPDIR) bin
	rm -rf $SRCDIR
	GITDIR=../.git
	if test -h $GITDIR/config; then
		GITDIR=`readlink $GITDIR/config`
		GITDIR=`dirname $GITDIR`
	fi
	sh /usr/share/doc/git/contrib/workdir/git-new-workdir $GITDIR $SRCDIR $* > /dev/null 2>&1
	make -C $SRCDIR tests > /dev/null 2>&1
	cp $SRCDIR/popop.native $@
	rm -rf $SRCDIR

bin/cvc4.native:
	cp `which cvc4` $@

bin/alt-ergo.native:
	cp `which alt-ergo` $@

##Partial sum (provedovertime)

#For a pair revs bench create the datafile for gnuplot
%.provedovertime.dat: provedovertime.native
	REVSBENCH=$*
	REVS=${REVSBENCH%.*}
	BENCHS=${REVSBENCH##*.}
	remake $REVS.commits $BENCHS
	rm -f $@.files.tmp
	(for commits in $$(cat $REVS.commits); do
		sed $BENCHS -e "s%^\(.*\)$$%\1.$commits.res%" >> $@.files.tmp
	done)
	xargs -a $@.files.tmp remake
	xargs -a $@.files.tmp cat | ./provedovertime.native $(TIMEOUT) $REVS.commits > $@.tmp
	rm $@.files.tmp
	mv $@.tmp $@

#Make the partial sum of the bench proved for each commit
provedovertime.native: provedovertime.ml
	ocamlopt -I ../_build/src/util unix.cmxa \
	 $(addprefix ../_build/src/util/, extmap.cmx exthtbl.cmx pp.cmx weakhtbl.cmx intmap.cmx opt.cmx unit.cmx lists.cmx sysutil.cmx stdlib.cmx hashcons.cmx)\
	 -o $@ $<

#Generate the graphics
%.provedovertime.png: %.provedovertime.gpl %.provedovertime.dat
	gnuplot -e 'set terminal pngcairo size 600, 400' -e "set output \"$@\"" $*.provedovertime.gpl > /dev/null

%.provedovertime.svg: %.provedovertime.gpl %.provedovertime.dat
	gnuplot -e 'set terminal svg' -e "set output \"$@\"" $*.provedovertime.gpl > /dev/null

%.provedovertime.qt: %.provedovertime.gpl %.provedovertime.dat
	gnuplot -e "set terminal qt persist" $*.provedovertime.gpl > /dev/null

%.provedovertime.gpl:
	REVSBENCH=$*
	REVS=${REVSBENCH%.*}
	BENCHS=${REVSBENCH##*.}
	remake $REVS $BENCHS
	echo "set title \"$BENCHS\"" > $@.tmp
	cat >> $@.tmp <<"EOF"
	set xlabel "time(s)"
	set ylabel "problems proved"

	set key rmargin width -4 samplen 2

	plot \
	EOF
	i=2
	for commits in $$(cat $REVS); do
		echo "\"$*.provedovertime.dat\" using 1:$i with lines title \"$commits\", \\" >> $@.tmp
		i=$$(expr $i + 1)
	done
	mv $@.tmp $@

##Compare two commits
%.compare.dat:
	ARGS=$*
	BENCHS=${ARGS##*.}
	ARGS2=${ARGS%.*}
	COMMIT2=${ARGS2##*.}
	COMMIT1=${ARGS2%.*}
	remake $COMMIT1.commit $COMMIT2.commit $BENCHS
	SHA1=$$(cat $COMMIT1.commit)
	SHA2=$$(cat $COMMIT2.commit)
	rm -f $@.files.tmp $@.tmp
	sed $BENCHS -e "s%^\(.*\)$$%\1.$SHA1.res%" >> $@.files.tmp
	sed $BENCHS -e "s%^\(.*\)$$%\1.$SHA2.res%" >> $@.files.tmp
	xargs -a $@.files.tmp remake
	for bench in $$(cat $BENCHS); do
		RESULT1=$$(cat $bench.$SHA1.res)
		RESULT2=$$(cat $bench.$SHA2.res)
		RESULT1=${RESULT1#* }
		RESULT2=${RESULT2#* }
		echo $RESULT1 $RESULT2 >> $@.tmp
	done
	rm $@.files.tmp
	mv $@.tmp $@

%.compare.gpl:
	ARGS=$*
	BENCHS=${ARGS##*.}
	ARGS=${ARGS%.*}
	COMMIT2=${ARGS##*.}
	COMMIT1=${ARGS%.*}
	echo "set title \"$BENCHS\"" > $@.tmp
	cat >> $@.tmp <<"EOF"
	set logscale xy 10
	set xrange [$(ERRORMEASURE):*]
	set yrange [$(ERRORMEASURE):*]
	set key off
	max(x,y) = x > y ? x : y
	EOF
	echo set xlabel "\"$COMMIT1\"" >> $@.tmp
	echo set ylabel "\"$COMMIT2\"" >> $@.tmp
	echo "plot \"$*.compare.dat\" using (max(column(1),$(ERRORMEASURE))):(max(column(2),$(ERRORMEASURE))), x+$(ERRORMEASURE) with lines linecolor rgbcolor \"green\", x-$(ERRORMEASURE) with lines linecolor rgbcolor \"green\"" >> $@.tmp
	mv $@.tmp $@

%.compare.png: %.compare.gpl %.compare.dat
	gnuplot -e 'set terminal pngcairo size 600, 400' -e "set output \"$@\"" $*.compare.gpl > /dev/null

%.compare.svg: %.compare.gpl %.compare.dat
	gnuplot -e 'set terminal svg' -e "set output \"$@\"" $*.compare.gpl > /dev/null

%.compare.qt: %.compare.gpl %.compare.dat
	gnuplot -e "set terminal qt persist"  $*.compare.gpl > /dev/null

#Cleaning

clean:
	rm -f *.png *.svg *.dat *.native *.cmi *.cmx *.o *.gpl *.commits *.tmp

clean-bin:
	rm -rf bin

clean-results:
	rm -rf tests benchs