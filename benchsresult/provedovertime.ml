open Stdlib

let usage = "provedovertime timeout shas_file"

let nbticks = 100.
let timeout = float_of_string Sys.argv.(1)
let shas_file = Sys.argv.(2)
let interval = timeout /. nbticks
let shas = Hstr.create 10
let nb_shas, shal =
  let cin = open_in shas_file in
  let nb_shas,shal =
    Sysutil.fold_channel (fun (i,shal) s ->
        if s<>""
        then begin Hstr.add shas s i; (i+1,s::shal) end
        else (i,shal))
      (0,[]) cin in
  close_in cin;
  nb_shas, List.rev shal

let int_of_sha s = Hstr.find shas s


type info = int Mint.t Mfloat.t

let rec gather_info (info:info) =
  try
    let sha,time = Scanf.fscanf stdin "%s %f\n" (fun sha time -> sha,time) in
    let sha = int_of_sha sha in
    let empty sha =
      Mint.singleton sha 1 in
    let add sha m =
      Mint.add_change (fun () -> 1) (fun () n -> succ n) sha () m in
    gather_info (Mfloat.add_change empty add time sha info)
  with
  | Not_found -> gather_info info
  | End_of_file -> info

let print_header () =
  print_string "time";
  List.iter (fun s ->
      print_string " "; print_string s
    ) shal;
  print_newline ()

let print_row tick row =
  print_float tick;
  for i = 0 to (Array.length row - 1) do
    print_string " "; print_int row.(i)
  done;
  print_newline ()

let rec print_row_if_needed f next row =
  if f >= next && next < timeout
  then (print_row next row; print_row_if_needed f (next+.interval) row)
  else next

let compute_and_print info =
  let row = Array.make nb_shas 0 in
  let fold (row,next) f m =
    let next = print_row_if_needed f next row in
    ignore (Mint.fold_left
              (fun row sha nb -> row.(sha) <- row.(sha) + nb; row) row m);
    row,next in
  ignore (Mfloat.fold_left fold (row,0.) info);
  print_row timeout row

let () =
  let info = gather_info Mfloat.empty in
  compute_and_print info
