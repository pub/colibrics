exception FinitePositiveExpected

let round_down_to (a: Q.t) (m: Q.t) : Q.t =
  if (Q.equal a (Q.of_bigint Z.zero))
  then a
  else
    begin
      let q' = Q__Q.floor (Q.(/) a m) in
      let r = (Q.( * ) (Q.of_bigint q') m) in r end

let round_up_to (a: Q.t) (m: Q.t) : Q.t =
  let res = round_down_to (Q.(-) (Q.of_bigint Z.zero) a) m in
  (Q.(-) (Q.of_bigint Z.zero) res)

let divisible (a: Q.t) (b: Q.t) : bool =
  if Z.equal (b.Q.num) Z.zero
  then Z.equal (a.Q.num) Z.zero
  else
    begin
      let b1 = (Z.divisible (a.Q.num) (b.Q.num)) in
      if b1
      then
        let b2 = (Z.divisible (b.Q.den) (a.Q.den)) in
        if b2 then true else false
      else false end

let mult_cst_divisible (a: Q.t) (q: Q.t) : Q.t = (Q.( * ) a q)

let union_divisible (a: Q.t) (b: Q.t) : Q.t =
  let n = (Z.gcd (a.Q.num) (b.Q.num)) in
  let d = (Z.lcm (a.Q.den) (b.Q.den)) in
  ({ Q.num = n; Q.den = d })

let inter_divisible (a: Q.t) (b: Q.t) : Q.t =
  let n = (Z.gcd (a.Q.den) (b.Q.den)) in
  let d = (Z.lcm (a.Q.num) (b.Q.num)) in
  ({ Q.num = d; Q.den = n })

