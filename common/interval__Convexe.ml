module Make(Q: sig
  type t
  val infix_eq : t -> t -> bool
  val infix_lseq : t -> t -> bool
  val infix_ls : t -> t -> bool
  val abs : t -> t
  val compare : t -> t -> Ord.t
  val infix_pl : t -> t -> t
  val infix_mn : t -> t -> t
  val infix_as : t -> t -> t
  val infix_sl : t -> t -> t
  val of_int : Z.t -> t
  val truncate : t -> Z.t
  val floor : t -> Z.t
  val ceil : t -> Z.t
  val is_integer : t -> bool end) =
struct
  type t =
    | Finite of Q.t * Interval__Bound.t * Q.t * Interval__Bound.t
    | InfFinite of Q.t * Interval__Bound.t
    | FiniteInf of Q.t * Interval__Bound.t
    | Inf
  
  type t' = t
  
  let is_singleton (e: t') : Q.t option =
    match e with
    | Inf -> None 
    | InfFinite (v, _) -> None 
    | FiniteInf (v, _) -> None 
    | Finite (v, b, v', b') -> if Q.infix_eq v v' then Some v else None 
  
  let singleton (q: Q.t) : t' =
    Finite (q, Interval__Bound.Large, q, Interval__Bound.Large)
  
  let except (e: t') (x: Q.t) : t' option =
    match e with
    | (Inf | (InfFinite (_, Interval__Bound.Strict) | FiniteInf (_,
              Interval__Bound.Strict))) ->
      Some e
    | InfFinite (v,
      Interval__Bound.Large) ->
      if Q.infix_eq v x
      then Some (InfFinite (v, Interval__Bound.Strict))
      else Some e
    | FiniteInf (v,
      Interval__Bound.Large) ->
      if Q.infix_eq v x
      then Some (FiniteInf (v, Interval__Bound.Strict))
      else Some e
    | Finite (_, Interval__Bound.Strict, _, Interval__Bound.Strict) -> Some e
    | Finite (v,
      Interval__Bound.Strict,
      v',
      Interval__Bound.Large) ->
      if Q.infix_eq v' x
      then
        Some (Finite (v, Interval__Bound.Strict, v', Interval__Bound.Strict))
      else Some e
    | Finite (v,
      Interval__Bound.Large,
      v',
      Interval__Bound.Strict) ->
      if Q.infix_eq v x
      then
        Some (Finite (v, Interval__Bound.Strict, v', Interval__Bound.Strict))
      else Some e
    | Finite (v,
      Interval__Bound.Large,
      v',
      Interval__Bound.Large) ->
      (let is_min = Q.infix_eq v x in let is_max = Q.infix_eq v' x in
       if is_min && is_max
       then None 
       else
         begin
           if is_min
           then
             Some (Finite (v, Interval__Bound.Strict, v',
                   Interval__Bound.Large))
           else
             begin
               if is_max
               then
                 Some (Finite (v, Interval__Bound.Large, v',
                       Interval__Bound.Strict))
               else Some e end end)
  
  type is_comparable =
    | Gt
    | Lt
    | Ge
    | Le
    | Eq
    | Uncomparable
  
  let is_comparable (e1: t') (e2: t') : is_comparable =
    match (e1, e2) with
    | ((Inf,
       Inf) | ((Inf,
               _) | ((_,
                     Inf) | ((InfFinite (_, _), InfFinite (_,
                             _)) | (FiniteInf (_, _), FiniteInf (_, _)))))) ->
      Uncomparable
    | ((FiniteInf (v1, b1), InfFinite (v2,
       b2)) | ((Finite (v1, b1, _, _), InfFinite (v2, b2)) | (FiniteInf (v1,
               b1), Finite (_, _, v2, b2)))) ->
      begin match Q.compare v1 v2 with
      | Ord.Eq ->
        begin match (b1, b2) with
        | (Interval__Bound.Large, Interval__Bound.Large) -> Ge
        | _ -> Gt
        end
      | Ord.Lt -> Uncomparable
      | Ord.Gt -> Gt
      end
    | ((Finite (_, _, v1, b1), FiniteInf (v2,
       b2)) | ((InfFinite (v1, b1), FiniteInf (v2, b2)) | (InfFinite (v1,
               b1), Finite (v2, b2, _, _)))) ->
      begin match Q.compare v1 v2 with
      | Ord.Eq ->
        begin match (b1, b2) with
        | (Interval__Bound.Large, Interval__Bound.Large) -> Le
        | _ -> Lt
        end
      | Ord.Lt -> Lt
      | Ord.Gt -> Uncomparable
      end
    | (Finite (v1,
      b1,
      v1',
      b1'),
      Finite (v2,
      b2,
      v2',
      b2')) ->
      begin match (Q.compare v1' v2, Q.compare v1 v2') with
      | (Ord.Eq, Ord.Eq) -> Eq
      | (Ord.Eq,
        _) ->
        begin match (b1', b2) with
        | (Interval__Bound.Large, Interval__Bound.Large) -> Le
        | _ -> Lt
        end
      | (_,
        Ord.Eq) ->
        begin match (b1, b2') with
        | (Interval__Bound.Large, Interval__Bound.Large) -> Ge
        | _ -> Gt
        end
      | (Ord.Lt, _) -> Lt
      | (_, Ord.Gt) -> Gt
      | _ -> Uncomparable
      end
  
  let is_distinct (e1: t') (e2: t') : bool =
    match is_comparable e1 e2 with
    | (Uncomparable | (Le | (Ge | Eq))) -> false
    | _ -> true
  
  let is_included (e1: t') (e2: t') : bool =
    match (e1, e2) with
    | (Inf, Inf) -> true
    | ((Inf,
       _) | ((InfFinite (_, _),
             (FiniteInf (_, _) | Finite (_, _, _, _))) | (FiniteInf (_, _),
             (InfFinite (_, _) | Finite (_, _, _, _))))) ->
      false
    | (_, Inf) -> true
    | ((InfFinite (v1, b1), InfFinite (v2, b2)) | (Finite (_, _, v1, b1),
       InfFinite (v2, b2))) ->
      begin match Q.compare v1 v2 with
      | Ord.Eq ->
        begin match (b1, b2) with
        | (Interval__Bound.Large, Interval__Bound.Strict) -> false
        | _ -> true
        end
      | Ord.Lt -> true
      | Ord.Gt -> false
      end
    | ((FiniteInf (v1, b1), FiniteInf (v2, b2)) | (Finite (v1, b1, _, _),
       FiniteInf (v2, b2))) ->
      begin match Q.compare v1 v2 with
      | Ord.Eq ->
        begin match (b1, b2) with
        | (Interval__Bound.Large, Interval__Bound.Strict) -> false
        | _ -> true
        end
      | Ord.Lt -> false
      | Ord.Gt -> true
      end
    | (Finite (v1,
      b1,
      v1',
      b1'),
      Finite (v2,
      b2,
      v2',
      b2')) ->
      begin match (Q.compare v2 v1, Q.compare v1' v2', b1, b2, b1', b2') with
      | (Ord.Eq,
        _,
        Interval__Bound.Large,
        Interval__Bound.Strict,
        _,
        _) ->
        false
      | (_,
        Ord.Eq,
        _,
        _,
        Interval__Bound.Large,
        Interval__Bound.Strict) ->
        false
      | ((Ord.Lt | Ord.Eq), (Ord.Lt | Ord.Eq), _, _, _, _) -> true
      | _ -> false
      end
  
  let cmp (x: Q.t) (b: Interval__Bound.t) (y: Q.t) : bool =
    match b with
    | Interval__Bound.Large -> Q.infix_lseq x y
    | Interval__Bound.Strict -> Q.infix_ls x y
  
  let mem (x: Q.t) (e: t') : bool =
    match e with
    | Inf -> true
    | InfFinite (v, b) -> cmp x b v
    | FiniteInf (v, b) -> cmp v b x
    | Finite (v, b, v', b') -> cmp v b x && cmp x b' v'
  
  let absent (x: Q.t) (e: t') : bool = not (mem x e)
  
  let mult_pos (q: Q.t) (e: t') : t' =
    match e with
    | Inf -> Inf
    | InfFinite (v, b) -> InfFinite (Q.infix_as q v, b)
    | FiniteInf (v, b) -> FiniteInf (Q.infix_as q v, b)
    | Finite (v,
      b,
      v',
      b') ->
      Finite (Q.infix_as q v, b, Q.infix_as q v', b')
  
  let mult_neg (q: Q.t) (e: t') : t' =
    match e with
    | Inf -> Inf
    | InfFinite (v, b) -> FiniteInf (Q.infix_as q v, b)
    | FiniteInf (v, b) -> InfFinite (Q.infix_as q v, b)
    | Finite (v,
      b,
      v',
      b') ->
      Finite (Q.infix_as q v', b', Q.infix_as q v, b)
  
  let mult_cst (c: Q.t) (e: t') : t' =
    if Q.infix_eq c (Q.of_int Z.zero)
    then singleton (Q.of_int Z.zero)
    else
      begin
        if Q.infix_ls (Q.of_int Z.zero) c then mult_pos c e else mult_neg c e end
  
  let add_cst (q: Q.t) (e: t') : t' =
    match e with
    | Inf -> Inf
    | InfFinite (v, b) -> InfFinite (Q.infix_pl q v, b)
    | FiniteInf (v, b) -> FiniteInf (Q.infix_pl q v, b)
    | Finite (v,
      b,
      v',
      b') ->
      Finite (Q.infix_pl q v, b, Q.infix_pl q v', b')
  
  let add_bound (b1: Interval__Bound.t) (b2: Interval__Bound.t) :
    Interval__Bound.t =
    match (b1, b2) with
    | (Interval__Bound.Large, Interval__Bound.Large) -> Interval__Bound.Large
    | (Interval__Bound.Large,
      Interval__Bound.Strict) ->
      Interval__Bound.Strict
    | (Interval__Bound.Strict,
      Interval__Bound.Large) ->
      Interval__Bound.Strict
    | (Interval__Bound.Strict,
      Interval__Bound.Strict) ->
      Interval__Bound.Strict
  
  let add (e1: t') (e2: t') : t' =
    match (e1, e2) with
    | ((Inf, _) | (_, Inf)) -> Inf
    | ((InfFinite (_, _), FiniteInf (_, _)) | (FiniteInf (_, _),
       InfFinite (_, _))) ->
      Inf
    | ((InfFinite (v1, b1),
       (InfFinite (v2, b2) | Finite (_, _, v2, b2))) | (Finite (_, _, v2,
       b2), InfFinite (v1, b1))) ->
      InfFinite (Q.infix_pl v1 v2, add_bound b1 b2)
    | ((FiniteInf (v1, b1),
       (FiniteInf (v2, b2) | Finite (v2, b2, _, _))) | (Finite (v2, b2, _,
       _), FiniteInf (v1, b1))) ->
      FiniteInf (Q.infix_pl v1 v2, add_bound b1 b2)
    | (Finite (v1,
      b1,
      v1',
      b1'),
      Finite (v2,
      b2,
      v2',
      b2')) ->
      Finite (Q.infix_pl v1 v2,
      add_bound b1 b2,
      Q.infix_pl v1' v2',
      add_bound b1' b2')
  
  let minus (e1: t') (e2: t') : t' =
    add e1 (mult_neg (Q.of_int (Z.of_string "-1")) e2)
  
  let gt (c: Q.t) : t' = FiniteInf (c, Interval__Bound.Strict)
  
  let ge (c: Q.t) : t' = FiniteInf (c, Interval__Bound.Large)
  
  let lt (c: Q.t) : t' = InfFinite (c, Interval__Bound.Strict)
  
  let le (c: Q.t) : t' = InfFinite (c, Interval__Bound.Large)
  
  let gt' (c: t') : t' =
    match c with
    | Finite (v, _, _, _) -> FiniteInf (v, Interval__Bound.Strict)
    | InfFinite (_, _) -> Inf
    | FiniteInf (_, Interval__Bound.Strict) -> c
    | FiniteInf (v,
      Interval__Bound.Large) ->
      FiniteInf (v, Interval__Bound.Strict)
    | Inf -> Inf
  
  let ge' (c: t') : t' =
    match c with
    | Finite (v, b, _, _) -> FiniteInf (v, b)
    | InfFinite (_, _) -> Inf
    | FiniteInf (_, _) -> c
    | Inf -> Inf
  
  let lt' (c: t') : t' =
    match c with
    | Finite (_, _, v, _) -> InfFinite (v, Interval__Bound.Strict)
    | InfFinite (_, Interval__Bound.Strict) -> c
    | InfFinite (v,
      Interval__Bound.Large) ->
      InfFinite (v, Interval__Bound.Strict)
    | FiniteInf (_, _) -> Inf
    | Inf -> Inf
  
  let le' (c: t') : t' =
    match c with
    | Finite (_, _, v, b) -> InfFinite (v, b)
    | InfFinite (_, _) -> c
    | FiniteInf (_, _) -> Inf
    | Inf -> Inf
  
  let min_bound_sup (v1: Q.t) (b1: Interval__Bound.t) (v2: Q.t)
                    (b2: Interval__Bound.t) : Q.t * Interval__Bound.t =
    match (Q.compare v1 v2, b1, b2) with
    | (Ord.Eq,
      Interval__Bound.Large,
      Interval__Bound.Large) ->
      (v1, Interval__Bound.Large)
    | (Ord.Eq, _, _) -> (v1, Interval__Bound.Strict)
    | (Ord.Lt, _, _) -> (v1, b1)
    | (Ord.Gt, _, _) -> (v2, b2)
  
  let min_bound_inf (v1: Q.t) (b1: Interval__Bound.t) (v2: Q.t)
                    (b2: Interval__Bound.t) : Q.t * Interval__Bound.t =
    match (Q.compare v1 v2, b1, b2) with
    | (Ord.Eq,
      Interval__Bound.Strict,
      Interval__Bound.Strict) ->
      (v1, Interval__Bound.Strict)
    | (Ord.Eq, _, _) -> (v1, Interval__Bound.Large)
    | (Ord.Lt, _, _) -> (v1, b1)
    | (Ord.Gt, _, _) -> (v2, b2)
  
  let max_bound_sup (v1: Q.t) (b1: Interval__Bound.t) (v2: Q.t)
                    (b2: Interval__Bound.t) : Q.t * Interval__Bound.t =
    match (Q.compare v1 v2, b1, b2) with
    | (Ord.Eq,
      Interval__Bound.Strict,
      Interval__Bound.Strict) ->
      (v1, Interval__Bound.Strict)
    | (Ord.Eq, _, _) -> (v1, Interval__Bound.Large)
    | (Ord.Lt, _, _) -> (v2, b2)
    | (Ord.Gt, _, _) -> (v1, b1)
  
  let max_bound_inf (v1: Q.t) (b1: Interval__Bound.t) (v2: Q.t)
                    (b2: Interval__Bound.t) : Q.t * Interval__Bound.t =
    match (Q.compare v1 v2, b1, b2) with
    | (Ord.Eq,
      Interval__Bound.Large,
      Interval__Bound.Large) ->
      (v1, Interval__Bound.Large)
    | (Ord.Eq, _, _) -> (v1, Interval__Bound.Strict)
    | (Ord.Lt, _, _) -> (v2, b2)
    | (Ord.Gt, _, _) -> (v1, b1)
  
  let union (e1: t') (e2: t') : t' =
    match (e1, e2) with
    | ((Inf, _) | (_, Inf)) -> Inf
    | ((InfFinite (_, _), FiniteInf (_, _)) | (FiniteInf (_, _),
       InfFinite (_, _))) ->
      Inf
    | ((InfFinite (v1, b1),
       (InfFinite (v2, b2) | Finite (_, _, v2, b2))) | (Finite (_, _, v2,
       b2), InfFinite (v1, b1))) ->
      (let (v3, b3) = max_bound_sup v1 b1 v2 b2 in InfFinite (v3, b3))
    | ((FiniteInf (v1, b1),
       (FiniteInf (v2, b2) | Finite (v2, b2, _, _))) | (Finite (v2, b2, _,
       _), FiniteInf (v1, b1))) ->
      (let (v31, b31) = min_bound_inf v1 b1 v2 b2 in FiniteInf (v31, b31))
    | (Finite (v1,
      b1,
      v1',
      b1'),
      Finite (v2,
      b2,
      v2',
      b2')) ->
      (let (v32, b32) = min_bound_inf v1 b1 v2 b2 in
       let (v4,
       b4) =
       max_bound_sup v1' b1' v2' b2' in
       Finite (v32, b32, v4, b4))
  
  let mk_finite (v1: Q.t) (b1: Interval__Bound.t) (v2: Q.t)
                (b2: Interval__Bound.t) : t' option =
    match Q.compare v1 v2 with
    | Ord.Eq ->
      begin match (b1, b2) with
      | (Interval__Bound.Large,
        Interval__Bound.Large) ->
        Some (Finite (v1, Interval__Bound.Large, v2, Interval__Bound.Large))
      | _ -> None 
      end
    | Ord.Lt -> Some (Finite (v1, b1, v2, b2))
    | Ord.Gt -> None 
  
  let inter (e1: t') (e2: t') : t' option =
    match (e1, e2) with
    | (Inf, _) -> Some e2
    | (_, Inf) -> Some e1
    | ((InfFinite (v1, b1), FiniteInf (v2, b2)) | (FiniteInf (v2, b2),
       InfFinite (v1, b1))) ->
      mk_finite v2 b2 v1 b1
    | (InfFinite (v1,
      b1),
      InfFinite (v2,
      b2)) ->
      (let (v33, b33) = min_bound_sup v1 b1 v2 b2 in
       Some (InfFinite (v33, b33)))
    | ((InfFinite (v1, b1), Finite (v2, b2, v34, b34)) | (Finite (v2, b2,
       v34, b34), InfFinite (v1, b1))) ->
      (let (v41, b41) = min_bound_sup v1 b1 v34 b34 in
       mk_finite v2 b2 v41 b41)
    | (FiniteInf (v1,
      b1),
      FiniteInf (v2,
      b2)) ->
      (let (v34, b34) = max_bound_inf v1 b1 v2 b2 in
       let r = FiniteInf (v34, b34) in Some r)
    | ((FiniteInf (v1, b1), Finite (v2, b2, v35, b35)) | (Finite (v2, b2,
       v35, b35), FiniteInf (v1, b1))) ->
      (let (v42, b42) = max_bound_inf v1 b1 v2 b2 in
       mk_finite v42 b42 v35 b35)
    | (Finite (v1,
      b1,
      v1',
      b1'),
      Finite (v2,
      b2,
      v2',
      b2')) ->
      (let (v35, b35) = max_bound_inf v1 b1 v2 b2 in
       let (v43,
       b43) =
       min_bound_sup v1' b1' v2' b2' in
       mk_finite v35 b35 v43 b43)
  
  let zero = singleton (Q.of_int Z.zero)
  
  let reals = Inf
  
  let is_reals (e: t') : bool =
    match e with
    | Inf -> true
    | InfFinite (v, _) -> false
    | FiniteInf (v, _) -> false
    | Finite (v, _, _, _) -> false
  
  let integers = reals
  
  let is_integer (e: t') : bool =
    match e with
    | Finite (v1,
      Interval__Bound.Large,
      v2,
      Interval__Bound.Large) ->
      Q.infix_eq v1 v2 && Q.is_integer v1
    | (Inf | (InfFinite (_, _) | (FiniteInf (_, _) | Finite (_, _, _, _)))) ->
      false
  
  let choose (e: t') : Q.t =
    match e with
    | Inf -> Q.of_int Z.zero
    | (InfFinite (v,
       Interval__Bound.Large) | (FiniteInf (v,
                                 Interval__Bound.Large) | (Finite (v,
                                                           Interval__Bound.Large,
                                                           _, _) | Finite (_,
                                                           _, v,
                                                           Interval__Bound.Large)))) ->
      v
    | InfFinite (v, Interval__Bound.Strict) -> Q.infix_mn v (Q.of_int Z.one)
    | FiniteInf (v, Interval__Bound.Strict) -> Q.infix_pl v (Q.of_int Z.one)
    | Finite (v1,
      Interval__Bound.Strict,
      v2,
      Interval__Bound.Strict) ->
      (let half = Q.infix_sl (Q.of_int Z.one) (Q.of_int (Z.of_string "2")) in
       Q.infix_pl (Q.infix_as v1 half) (Q.infix_as v2 half))
  
  type split_heuristic =
    | Singleton of Q.t
    | Splitted of t' * t'
    | NotSplitted
  
  let split_heuristic (c: t') : split_heuristic =
    let qzero = Q.of_int Z.zero in
    let zero = singleton qzero in
    match c with
    | Inf ->
      Splitted (InfFinite (qzero, Interval__Bound.Strict),
      FiniteInf (qzero, Interval__Bound.Large))
    | InfFinite (v,
      b) ->
      begin match (Q.compare qzero v, b) with
      | (Ord.Eq,
        Interval__Bound.Large) ->
        Splitted (InfFinite (qzero, Interval__Bound.Strict), zero)
      | ((Ord.Eq, Interval__Bound.Strict) | (Ord.Gt, _)) -> NotSplitted
      | (Ord.Lt,
        _) ->
        Splitted (InfFinite (qzero, Interval__Bound.Strict),
        Finite (qzero, Interval__Bound.Large, v, b))
      end
    | FiniteInf (v,
      b) ->
      begin match (Q.compare v qzero, b) with
      | (Ord.Eq,
        Interval__Bound.Large) ->
        Splitted (zero, FiniteInf (v, Interval__Bound.Strict))
      | ((Ord.Eq, Interval__Bound.Strict) | (Ord.Gt, _)) -> NotSplitted
      | (Ord.Lt,
        _) ->
        Splitted (Finite (v, b, qzero, Interval__Bound.Strict),
        FiniteInf (qzero, Interval__Bound.Large))
      end
    | Finite (v1,
      b1,
      v2,
      b2) ->
      begin
        match
        (Q.compare v1 qzero, b1, Q.compare v2 qzero, b2, Q.compare v1 v2)
      with
      | (_, _, _, _, Ord.Eq) -> Singleton v1
      | (Ord.Eq,
        Interval__Bound.Large,
        _,
        _,
        _) ->
        Splitted (zero, Finite (qzero, Interval__Bound.Strict, v2, b2))
      | (_,
        _,
        Ord.Eq,
        Interval__Bound.Large,
        _) ->
        Splitted (Finite (v1, b1, qzero, Interval__Bound.Strict), zero)
      | _ ->
        (let half = Q.infix_sl (Q.of_int Z.one) (Q.of_int (Z.of_string "2")) in
         let m = Q.infix_pl (Q.infix_as half v1) (Q.infix_as half v2) in
         Splitted (Finite (v1, b1, m, Interval__Bound.Strict),
         Finite (m, Interval__Bound.Large, v2, b2)))
      end
end

