module QUtils = struct
  (** Add some useful function to Q of zarith *)

  let floor = Q__Q.floor
  (** [floor a] return the greatest integer smaller or equal than [a] *)

  let ceil = Q__Q.ceil
  (** [ceil a] return the smallest integer greater or equal than [a] *)

  let divisible_down_to = Modulo__Divisible.round_down_to
  (** [divisible_down_to a m] return the greatest multiple of [m] smaller or equal
      than [a] *)

  let divisible_up_to = Modulo__Divisible.round_up_to
  (** [divisible_down_to a m] return the smallest multiple of [m] greater or equal
      than [a] *)

  let divisible = Modulo__Divisible.divisible
  (** [divisible a b] test if a is divisible by b *)

  let mult_cst_divisible = Modulo__Divisible.mult_cst_divisible
  (** [mult_cst_divisible a b] *)

  let union_divisible a b =
    if Q.equal a Q.zero || Q.equal b Q.zero then Q.zero
    else Modulo__Divisible.union_divisible a b

  let inter_divisible a b =
    if Q.equal a Q.zero then b else
    if Q.equal b Q.zero then a
    else Modulo__Divisible.inter_divisible a b

end

module Interval = struct
  module Bound = Bound__Bound
  module Union = Union__Union
end

module Ord = Ord
