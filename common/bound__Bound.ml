type t =
  | Strict
  | Large

let inv_bound (x: t) : t = match x with
                           | Large -> Strict
                           | Strict -> Large

