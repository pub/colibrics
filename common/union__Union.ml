module Make(Q: sig
  type t
  val infix_eq : t -> t -> bool
  val infix_lseq : t -> t -> bool
  val infix_ls : t -> t -> bool
  val abs : t -> t
  val compare : t -> t -> Ord.t
  val infix_pl : t -> t -> t
  val infix_mn : t -> t -> t
  val infix_as : t -> t -> t
  val infix_sl : t -> t -> t
  val of_int : Z.t -> t
  val truncate : t -> Z.t
  val floor : t -> Z.t
  val ceil : t -> Z.t
  val is_integer : t -> bool end) =
struct
  type t'' =
    | Sin of Q.t * t''
    | Chg of Q.t * Bound__Bound.t * t''
    | Inf
  
  type t' =
    | On of t''
    | Off of t''
  
  type t = t'
  
  let singleton (q: Q.t) : t = Off (Sin (q, Inf))
  
  let is_singleton (u: t) : Q.t option =
    match u with
    | Off Sin (q, Inf) -> Some q
    | Off Sin (q, Sin (q', _)) -> None 
    | Off Sin (_,
      Chg (q,
      _,
      on)) ->
      begin match on with
      | Sin (q', _) -> None 
      | Chg (q', _, _) -> None 
      | Inf -> None 
      end
    | Off Chg (q,
      _,
      on) ->
      begin match on with
      | Sin (q', _) -> None 
      | Chg (q', _, _) -> None 
      | Inf -> None 
      end
    | On Sin (q, _) -> None 
    | On Chg (q, _, _) -> None 
    | On Inf -> None 
    | Off Inf -> assert false (* absurd *)
  
  let min_bound_sup (v1: Q.t) (b1: Bound__Bound.t) (v2: Q.t)
                    (b2: Bound__Bound.t) : Q.t * Bound__Bound.t =
    match (Q.compare v1 v2, b1, b2) with
    | (Ord.Eq,
      Bound__Bound.Large,
      Bound__Bound.Large) ->
      (v1, Bound__Bound.Large)
    | (Ord.Eq, _, _) -> (v1, Bound__Bound.Strict)
    | (Ord.Lt, _, _) -> (v1, b1)
    | (Ord.Gt, _, _) -> (v2, b2)
  
  let min_bound_inf (v1: Q.t) (b1: Bound__Bound.t) (v2: Q.t)
                    (b2: Bound__Bound.t) : Q.t * Bound__Bound.t =
    match (Q.compare v1 v2, b1, b2) with
    | (Ord.Eq,
      Bound__Bound.Strict,
      Bound__Bound.Strict) ->
      (v1, Bound__Bound.Strict)
    | (Ord.Eq, _, _) -> (v1, Bound__Bound.Large)
    | (Ord.Lt, _, _) -> (v1, b1)
    | (Ord.Gt, _, _) -> (v2, b2)
  
  let max_bound_sup (v1: Q.t) (b1: Bound__Bound.t) (v2: Q.t)
                    (b2: Bound__Bound.t) : Q.t * Bound__Bound.t =
    match (Q.compare v1 v2, b1, b2) with
    | (Ord.Eq,
      Bound__Bound.Strict,
      Bound__Bound.Strict) ->
      (v1, Bound__Bound.Strict)
    | (Ord.Eq, _, _) -> (v1, Bound__Bound.Large)
    | (Ord.Lt, _, _) -> (v2, b2)
    | (Ord.Gt, _, _) -> (v1, b1)
  
  let max_bound_inf (v1: Q.t) (b1: Bound__Bound.t) (v2: Q.t)
                    (b2: Bound__Bound.t) : Q.t * Bound__Bound.t =
    match (Q.compare v1 v2, b1, b2) with
    | (Ord.Eq,
      Bound__Bound.Large,
      Bound__Bound.Large) ->
      (v1, Bound__Bound.Large)
    | (Ord.Eq, _, _) -> (v1, Bound__Bound.Strict)
    | (Ord.Lt, _, _) -> (v2, b2)
    | (Ord.Gt, _, _) -> (v1, b1)
  
  let inter (u: t) (v: t) : t option =
    let rec inter' (ifu: bool) (ifv: bool) (u1: t'') (v1: t'') : t'' =
      match (u1, v1) with
      | (u2, Inf) -> if ifv then u2 else Inf
      | (Inf, v2) -> if ifu then v2 else Inf
      | (Sin (qu,
        lu),
        Sin (qv,
        lv)) ->
        begin match Q.compare qu qv with
        | Ord.Eq ->
          (let l = inter' ifu ifv lu lv in
           if ifu <> ifv then l else Sin (qu, l))
        | Ord.Lt ->
          (let l = inter' ifu ifv lu v1 in if ifv then Sin (qu, l) else l)
        | Ord.Gt ->
          (let l = inter' ifu ifv u1 lv in if ifu then Sin (qv, l) else l)
        end
      | (Chg (qu,
        bu,
        lu),
        Chg (qv,
        bv,
        lv)) ->
        begin match Q.compare qu qv with
        | Ord.Eq ->
          (let l = inter' (not ifu) (not ifv) lu lv in
           match (bu, bv) with
           | ((Bound__Bound.Large,
              Bound__Bound.Large) | (Bound__Bound.Strict,
              Bound__Bound.Strict)) ->
             if ifu <> ifv then l else Chg (qu, bu, l)
           | (Bound__Bound.Large,
             Bound__Bound.Strict) ->
             begin match (ifu, ifv) with
             | (true, true) -> Chg (qu, Bound__Bound.Large, l)
             | (false, false) -> Chg (qu, Bound__Bound.Strict, l)
             | (true, false) -> l
             | (false, true) -> Sin (qu, l)
             end
           | (Bound__Bound.Strict,
             Bound__Bound.Large) ->
             begin match (ifv, ifu) with
             | (true, true) -> Chg (qu, Bound__Bound.Large, l)
             | (false, false) -> Chg (qu, Bound__Bound.Strict, l)
             | (true, false) -> l
             | (false, true) -> Sin (qu, l)
             end)
        | Ord.Lt ->
          (let l = inter' (not ifu) ifv lu v1 in
           if ifv then Chg (qu, bu, l) else l)
        | Ord.Gt ->
          (let l = inter' ifu (not ifv) u1 lv in
           if ifu then Chg (qv, bv, l) else l)
        end
      | (Sin (qu,
        lu),
        Chg (qv,
        bv,
        lv)) ->
        begin match Q.compare qu qv with
        | Ord.Eq ->
          (let l = inter' ifu (not ifv) lu lv in
           if ifu
           then
             Chg (qu,
             (if ifv then Bound__Bound.Large else Bound__Bound.Strict),
             l)
           else
             begin match (ifv, bv) with
             | ((false, Bound__Bound.Large) | (true, Bound__Bound.Strict)) ->
               Sin (qu, l)
             | _ -> l
             end)
        | Ord.Lt ->
          (let l = inter' ifu ifv lu v1 in if ifv then Sin (qu, l) else l)
        | Ord.Gt ->
          (let l = inter' ifu (not ifv) u1 lv in
           if ifu then Chg (qv, bv, l) else l)
        end
      | (Chg (qu,
        bu,
        lu),
        Sin (qv,
        lv)) ->
        begin match Q.compare qu qv with
        | Ord.Eq ->
          (let l = inter' (not ifu) ifv lu lv in
           if ifv
           then
             Chg (qv,
             (if ifu then Bound__Bound.Large else Bound__Bound.Strict),
             l)
           else
             begin match (ifu, bu) with
             | ((false, Bound__Bound.Large) | (true, Bound__Bound.Strict)) ->
               Sin (qv, l)
             | _ -> l
             end)
        | Ord.Lt ->
          (let l = inter' (not ifu) ifv lu v1 in
           if ifv then Chg (qu, bu, l) else l)
        | Ord.Gt ->
          (let l = inter' ifu ifv u1 lv in if ifu then Sin (qv, l) else l)
        end in
    let mk_off_option (l: t'') : t option =
      match l with
      | Inf -> None 
      | l1 -> Some (Off l1) in
    match (u, v) with
    | (On u1, On v1) -> Some (On (inter' true true u1 v1))
    | (On u1, Off v1) -> mk_off_option (inter' true false u1 v1)
    | (Off u1, On v1) -> mk_off_option (inter' false true u1 v1)
    | (Off u1, Off v1) -> mk_off_option (inter' false false u1 v1)
  
  let rec union' (ifu: bool) (ifv: bool) (u: t'') (v: t'') : t'' =
    match (u, v) with
    | (u1, Inf) -> if ifv then Inf else u1
    | (Inf, v1) -> if ifu then Inf else v1
    | (Sin (qu,
      lu),
      Sin (qv,
      lv)) ->
      begin match Q.compare qu qv with
      | Ord.Eq ->
        (let l = union' ifu ifv lu lv in
         if ifu <> ifv then l else Sin (qu, l))
      | Ord.Lt ->
        (let l = union' ifu ifv lu v in if ifv then l else Sin (qu, l))
      | Ord.Gt ->
        (let l = union' ifu ifv u lv in if ifu then l else Sin (qv, l))
      end
    | (Chg (qu,
      bu,
      lu),
      Chg (qv,
      bv,
      lv)) ->
      begin match Q.compare qu qv with
      | Ord.Eq ->
        (let l = union' (not ifu) (not ifv) lu lv in
         match (bu, bv) with
         | ((Bound__Bound.Large, Bound__Bound.Large) | (Bound__Bound.Strict,
            Bound__Bound.Strict)) ->
           if ifu <> ifv then l else Chg (qu, bu, l)
         | (Bound__Bound.Large,
           Bound__Bound.Strict) ->
           begin match (ifu, ifv) with
           | (true, true) -> Chg (qu, Bound__Bound.Strict, l)
           | (false, false) -> Chg (qu, Bound__Bound.Large, l)
           | (true, false) -> Sin (qu, l)
           | (false, true) -> l
           end
         | (Bound__Bound.Strict,
           Bound__Bound.Large) ->
           begin match (ifv, ifu) with
           | (true, true) -> Chg (qu, Bound__Bound.Strict, l)
           | (false, false) -> Chg (qu, Bound__Bound.Large, l)
           | (true, false) -> Sin (qu, l)
           | (false, true) -> l
           end)
      | Ord.Lt ->
        (let l = union' (not ifu) ifv lu v in
         if ifv then l else Chg (qu, bu, l))
      | Ord.Gt ->
        (let l = union' ifu (not ifv) u lv in
         if ifu then l else Chg (qv, bv, l))
      end
    | (Sin (qu,
      lu),
      Chg (qv,
      bv,
      lv)) ->
      begin match Q.compare qu qv with
      | Ord.Eq ->
        (let l = union' ifu (not ifv) lu lv in
         if ifu
         then
           match (ifv, bv) with
           | ((false, Bound__Bound.Large) | (true, Bound__Bound.Strict)) -> l
           | _ -> Sin (qu, l)
         else
           Chg (qu,
           (if ifv then Bound__Bound.Strict else Bound__Bound.Large),
           l))
      | Ord.Lt ->
        (let l = union' ifu ifv lu v in if ifv then l else Sin (qu, l))
      | Ord.Gt ->
        (let l = union' ifu (not ifv) u lv in
         if ifu then l else Chg (qv, bv, l))
      end
    | (Chg (qu,
      bu,
      lu),
      Sin (qv,
      lv)) ->
      begin match Q.compare qu qv with
      | Ord.Eq ->
        (let l = union' (not ifu) ifv lu lv in
         if ifv
         then
           match (ifu, bu) with
           | ((false, Bound__Bound.Large) | (true, Bound__Bound.Strict)) -> l
           | ((false, Bound__Bound.Strict) | (true, Bound__Bound.Large)) ->
             Sin (qv, l)
         else
           Chg (qv,
           (if ifu then Bound__Bound.Strict else Bound__Bound.Large),
           l))
      | Ord.Lt ->
        (let l = union' (not ifu) ifv lu v in
         if ifv then l else Chg (qu, bu, l))
      | Ord.Gt ->
        (let l = union' ifu ifv u lv in if ifu then l else Sin (qv, l))
      end
  
  let union_a (u: t') (v: t') : t' =
    match (u, v) with
    | (On u1, On v1) -> On (union' true true u1 v1)
    | (On u1, Off v1) -> On (union' true false u1 v1)
    | (Off u1, On v1) -> On (union' false true u1 v1)
    | (Off u1, Off v1) -> Off (union' false false u1 v1)
  
  let union (u: t) (v: t) : t = let w = union_a u v in w
  
  let cmp (x: Q.t) (b: Bound__Bound.t) (y: Q.t) : bool =
    match b with
    | Bound__Bound.Large -> Q.infix_lseq x y
    | Bound__Bound.Strict -> Q.infix_ls x y
  
  let mem (x: Q.t) (l: t) : bool =
    let rec mem' (if_: bool) (x1: Q.t) (l1: t'') : bool =
      match l1 with
      | Sin (q,
        l2) ->
        begin match Q.compare x1 q with
        | Ord.Eq -> not if_
        | Ord.Lt -> if_
        | Ord.Gt -> mem' if_ x1 l2
        end
      | Chg (q, b, l2) -> if cmp q b x1 then mem' (not if_) x1 l2 else if_
      | Inf -> if_ in
    match l with
    | On l1 -> mem' true x l1
    | Off l1 -> mem' false x l1
  
  let except (l: t) (x: Q.t) : t option =
    let rec except' (if_: bool) (x1: Q.t) (l1: t'') : t'' =
      match l1 with
      | Sin (q,
        l') ->
        begin match Q.compare x1 q with
        | Ord.Eq -> if if_ then l1 else l'
        | Ord.Lt -> if if_ then Sin (x1, l1) else l1
        | Ord.Gt -> Sin (q, except' if_ x1 l')
        end
      | Chg (q,
        bv,
        l') ->
        begin match Q.compare x1 q with
        | Ord.Eq ->
          Chg (q,
          (if if_ then Bound__Bound.Large else Bound__Bound.Strict),
          l')
        | Ord.Lt -> if if_ then Sin (x1, l1) else l1
        | Ord.Gt -> Chg (q, bv, except' (not if_) x1 l')
        end
      | Inf -> if if_ then Sin (x1, Inf) else Inf in
    match l with
    | On l1 -> Some (On (except' true x l1))
    | Off l1 ->
      begin match except' false x l1 with
      | Inf -> None 
      | l' -> Some (Off l')
      end
  
  let complement (l: t) : t option =
    match l with
    | On Inf -> None 
    | On l1 -> Some (Off l1)
    | Off l1 -> Some (On l1)
  
  let add_bound (b1: Bound__Bound.t) (b2: Bound__Bound.t) : Bound__Bound.t =
    match (b1, b2) with
    | (Bound__Bound.Large, Bound__Bound.Large) -> Bound__Bound.Large
    | (Bound__Bound.Large, Bound__Bound.Strict) -> Bound__Bound.Strict
    | (Bound__Bound.Strict, Bound__Bound.Large) -> Bound__Bound.Strict
    | (Bound__Bound.Strict, Bound__Bound.Strict) -> Bound__Bound.Strict
  
  let both_strict (b1: Bound__Bound.t) (b2: Bound__Bound.t) : Bound__Bound.t
    =
    match (b1, b2) with
    | (Bound__Bound.Strict, Bound__Bound.Strict) -> Bound__Bound.Strict
    | (Bound__Bound.Large, Bound__Bound.Strict) -> Bound__Bound.Large
    | (Bound__Bound.Strict, Bound__Bound.Large) -> Bound__Bound.Large
    | (Bound__Bound.Large, Bound__Bound.Large) -> Bound__Bound.Large
  
  let bound_compare :
    type xi. ((bool) -> ((bool) -> xi)) -> (bool) -> (bool) ->
             Bound__Bound.t -> Bound__Bound.t ->  xi =
    fun test if1 if2 b1 b2 -> match (b1, b2) with
      | (Bound__Bound.Large, Bound__Bound.Large) -> test if1 if2
      | (Bound__Bound.Strict, Bound__Bound.Large) -> test (not if1) if2
      | (Bound__Bound.Large, Bound__Bound.Strict) -> test if1 (not if2)
      | (Bound__Bound.Strict,
        Bound__Bound.Strict) ->
        test (not if1) (not if2)
  
  let rec for_all (test: bool -> bool -> bool) (t1: t) (t2: t) : bool =
    let rec aux (test1: bool -> bool -> bool) (if1: bool) (if2: bool)
                (t11: t'') (t21: t'') : bool =
      match (t11, t21) with
      | (Inf, Inf) -> test1 if1 if2
      | (_, Inf) -> test1 (not if1) if2
      | (Inf, _) -> test1 if1 (not if2)
      | (Sin (q1,
        l1),
        Sin (q2,
        l2)) ->
        begin match Q.compare q1 q2 with
        | Ord.Lt -> test1 (not if1) if2 && aux test1 if1 if2 l1 t21
        | Ord.Gt -> test1 if1 (not if2) && aux test1 if1 if2 t11 l2
        | Ord.Eq -> test1 (not if1) (not if2) && aux test1 if1 if2 l1 l2
        end
      | (Sin (q1,
        l1),
        Chg (q2,
        b2,
        l2)) ->
        begin match Q.compare q1 q2 with
        | Ord.Lt -> test1 (not if1) if2 && aux test1 if1 if2 l1 t21
        | Ord.Gt ->
          test1 if1 (not if2) && test1 if1 (not if2) && aux test1
                                                        if1
                                                        (not if2)
                                                        t11
                                                        l2
        | Ord.Eq ->
          bound_compare test1 (not if1) (not if2) Bound__Bound.Large b2 && 
          test1 if1 (not if2) && aux test1 if1 (not if2) l1 l2
        end
      | (Chg (q1,
        b1,
        l1),
        Sin (q2,
        l2)) ->
        begin match Q.compare q1 q2 with
        | Ord.Lt ->
          test1 (not if1) if2 && test1 (not if1) if2 && aux test1
                                                        (not if1)
                                                        if2
                                                        l1
                                                        t21
        | Ord.Gt ->
          test1 if1 (not if2) && test1 if1 if2 && aux test1 if1 if2 t11 l2
        | Ord.Eq ->
          bound_compare test1 (not if1) (not if2) b1 Bound__Bound.Large && 
          test1 (not if1) if2 && aux test1 (not if1) if2 l1 l2
        end
      | (Chg (q1,
        b1,
        l1),
        Chg (q2,
        b2,
        l2)) ->
        begin match Q.compare q1 q2 with
        | Ord.Lt -> test1 (not if1) if2 && aux test1 (not if1) if2 l1 t21
        | Ord.Gt -> test1 if1 (not if2) && aux test1 if1 (not if2) t11 l2
        | Ord.Eq ->
          bound_compare test1 (not if1) (not if2) b1 b2 && test1 (not if1) (not if2) && 
                                                           aux test1
                                                           (not if1)
                                                           (not if2)
                                                           l1
                                                           l2
        end in
    match (t1, t2) with
    | (On on1, On on2) -> test true true && aux test true true on1 on2
    | (On on1, Off off2) -> test true false && aux test true false on1 off2
    | (Off off1, On on2) -> test false true && aux test false true off1 on2
    | (Off off1,
      Off off2) ->
      test false false && aux test false false off1 off2
  
  let rec distinct (t1: t) (t2: t) : bool =
    let nand (x: bool) (y: bool) : bool = not (x && y) in for_all nand t1 t2
  
  let impb (b1: bool) (b2: bool) : bool =
    match (b1, b2) with
    | (true, false) -> false
    | _ -> true
  
  let rec is_included (t1: t) (t2: t) : bool = for_all impb t1 t2
  
  let gt (q: Q.t) : t = Off (Chg (q, Bound__Bound.Strict, Inf))
  
  let lt (q: Q.t) : t = On (Chg (q, Bound__Bound.Large, Inf))
  
  let ge (q: Q.t) : t = Off (Chg (q, Bound__Bound.Large, Inf))
  
  let le (q: Q.t) : t = On (Chg (q, Bound__Bound.Strict, Inf))
  
  let le' (u: t) : t =
    let rec aux1 (if_: bool) (u1: t'') : t'' =
      match u1 with
      | Sin (q,
        Inf) ->
        if if_ then Inf else Chg (q, Bound__Bound.Strict, Inf)
      | Chg (q, b, Inf) -> if if_ then Chg (q, b, Inf) else Inf
      | Sin (_, u2) -> aux1 if_ u2
      | Chg (_, _, u2) -> aux1 (not if_) u2
      | Inf -> Inf in
    match u with
    | On Inf -> On Inf
    | On on -> On (aux1 true on)
    | Off off -> On (aux1 false off)
  
  let lt' (u: t) : t =
    let rec aux2 (if_: bool) (u1: t'') : t'' =
      match u1 with
      | Sin (q, Inf) -> if if_ then Inf else Chg (q, Bound__Bound.Large, Inf)
      | Chg (q,
        _,
        Inf) ->
        if if_ then Chg (q, Bound__Bound.Large, Inf) else Inf
      | Sin (_, u2) -> aux2 if_ u2
      | Chg (_, _, u2) -> aux2 (not if_) u2
      | Inf -> Inf in
    match u with
    | On Inf -> On Inf
    | On on -> On (aux2 true on)
    | Off off -> On (aux2 false off)
  
  let gt' (u: t) : t =
    match u with
    | On _ -> On Inf
    | Off Sin (q, _) -> Off (Chg (q, Bound__Bound.Strict, Inf))
    | Off Chg (q, _, _) -> Off (Chg (q, Bound__Bound.Strict, Inf))
    | _ -> assert false (* absurd *)
  
  let ge' (u: t) : t =
    match u with
    | On _ -> On Inf
    | Off Sin (q, _) -> Off (Chg (q, Bound__Bound.Large, Inf))
    | Off Chg (q, b, _) -> Off (Chg (q, b, Inf))
    | _ -> assert false (* absurd *)
  
  module IsComparable =
  struct
    type is_comparable =
      | Gt
      | Lt
      | Ge
      | Le
      | Eq
      | Uncomparable
    
    let rec is_lt (if_: bool) (t1: t'') (q: Q.t) : is_comparable =
      match (t1, if_) with
      | (Sin (_, Inf), true) -> Uncomparable
      | (Chg (_, _, Inf), false) -> Uncomparable
      | (Sin (q1,
        l1),
        _) ->
        begin match (Q.compare q1 q, l1) with
        | (Ord.Lt, Inf) -> Lt
        | (Ord.Eq, Inf) -> Le
        | (Ord.Eq, _) -> Uncomparable
        | (Ord.Lt, _) -> is_lt if_ l1 q
        | (Ord.Gt, _) -> Uncomparable
        end
      | (Chg (q1,
        b1,
        l1),
        _) ->
        begin match (Q.compare q1 q, b1, l1) with
        | (Ord.Lt, _, Inf) -> Lt
        | (Ord.Eq, Bound__Bound.Large, Inf) -> Lt
        | (Ord.Eq, Bound__Bound.Strict, Inf) -> Le
        | (Ord.Eq, _, _) -> Uncomparable
        | (Ord.Lt, _, _) -> is_lt (not if_) l1 q
        | (Ord.Gt, _, _) -> Uncomparable
        end
      | (Inf, true) -> Uncomparable
      | (Inf, false) -> Lt
    
    let is_comparable (t1: t) (t2: t) : is_comparable =
      match (t1, t2) with
      | (On _, On _) -> Uncomparable
      | (On l1,
        Off l2) ->
        (let (q2, b2) =
         match l2 with
         | Sin (q21, _) -> (q21, Bound__Bound.Large)
         | Chg (q21, b21, _) -> (q21, b21)
         | _ -> assert false (* absurd *) in
         match (is_lt true l1 q2, b2) with
         | (Lt, _) -> Lt
         | (Le, Bound__Bound.Strict) -> Lt
         | (Le, Bound__Bound.Large) -> Le
         | (Uncomparable, _) -> Uncomparable
         | _ -> assert false (* absurd *))
      | (Off l1,
        On l2) ->
        (let (q1, b1) =
         match l1 with
         | Sin (q11, _) -> (q11, Bound__Bound.Large)
         | Chg (q11, b11, _) -> (q11, b11)
         | _ -> assert false (* absurd *) in
         match (is_lt true l2 q1, b1) with
         | (Lt, _) -> Gt
         | (Le, Bound__Bound.Strict) -> Gt
         | (Le, Bound__Bound.Large) -> Ge
         | (Uncomparable, _) -> Uncomparable
         | _ -> assert false (* absurd *))
      | (Off l1,
        Off l2) ->
        (let (q11, b11) =
         match l1 with
         | Sin (q12, _) -> (q12, Bound__Bound.Large)
         | Chg (q12, b12, _) -> (q12, b12)
         | _ -> assert false (* absurd *) in
         let (q21,
         b21) =
         match l2 with
         | Sin (q22, _) -> (q22, Bound__Bound.Large)
         | Chg (q22, b22, _) -> (q22, b22)
         | _ -> assert false (* absurd *) in
         match Q.compare q11 q21 with
         | Ord.Eq ->
           begin match (is_lt false l1 q21, b21, is_lt false l2 q11, b11)
           with
           | (Lt, _, _, _) -> assert false (* absurd *)
           | (_, _, Lt, _) -> assert false (* absurd *)
           | (Le, _, Le, _) -> Eq
           | (Le, Bound__Bound.Strict, _, _) -> Lt
           | (Le, Bound__Bound.Large, _, _) -> Le
           | (Uncomparable, _, Le, Bound__Bound.Large) -> Ge
           | (Uncomparable, _, Le, Bound__Bound.Strict) -> Gt
           | (Uncomparable, _, Uncomparable, _) -> Uncomparable
           | _ -> assert false (* absurd *)
           end
         | Ord.Lt ->
           begin match (is_lt false l1 q21, b21) with
           | (Lt, _) -> Lt
           | (Le, Bound__Bound.Strict) -> Lt
           | (Le, Bound__Bound.Large) -> Le
           | (Uncomparable, _) -> Uncomparable
           | _ -> assert false (* absurd *)
           end
         | Ord.Gt ->
           begin match (is_lt false l2 q11, b11) with
           | (Lt, _) -> Gt
           | (Le, Bound__Bound.Strict) -> Gt
           | (Le, Bound__Bound.Large) -> Ge
           | (Uncomparable, _) -> Uncomparable
           | _ -> assert false (* absurd *)
           end)
  end
  
  type convexe_hull =
    ((Q.t * Bound__Bound.t) option) * ((Q.t * Bound__Bound.t) option)
  
  let from_convexe_hull (x:
                        ((Q.t * Bound__Bound.t) option) *
                        ((Q.t * Bound__Bound.t) option)) : t option =
    match x with
    | (None, None) -> Some (On Inf)
    | (None,
      Some (q, b)) ->
      Some (On (Chg (q, Bound__Bound.inv_bound b, Inf)))
    | (Some (q, b), None) -> Some (Off (Chg (q, b, Inf)))
    | (Some (q12, b12),
      Some (q22, b22)) ->
      begin match (Q.compare q12 q22, b12, b22) with
      | (Ord.Eq,
        Bound__Bound.Large,
        Bound__Bound.Large) ->
        Some (Off (Sin (q12, Inf)))
      | (Ord.Eq, _, _) -> None 
      | (Ord.Lt,
        _,
        _) ->
        Some (Off (Chg (q12, b12,
                   Chg (q22, Bound__Bound.inv_bound b22, Inf))))
      | (Ord.Gt, _, _) -> None 
      end
  
  let get_convexe_hull (x: t) :
    ((Q.t * Bound__Bound.t) option) * ((Q.t * Bound__Bound.t) option) =
    let rec get_last_on (x1: t'') : (Q.t * Bound__Bound.t) option =
      match x1 with
      | Sin (_, on) -> get_last_on on
      | Chg (q, b, Inf) -> Some (q, Bound__Bound.inv_bound b)
      | Chg (_, _, off) -> get_last_off off
      | Inf -> None 
    and get_last_off (x1: t'') : (Q.t * Bound__Bound.t) option =
      match x1 with
      | Sin (q, Inf) -> Some (q, Bound__Bound.Large)
      | Sin (_, off) -> get_last_off off
      | Chg (_, _, on) -> get_last_on on
      | Inf -> assert false (* absurd *) in
    match x with
    | On on -> (None , get_last_on on)
    | Off off ->
      begin match off with
      | Sin (q,
        Inf) ->
        (Some (q, Bound__Bound.Large), Some (q, Bound__Bound.Large))
      | Sin (q, off1) -> (Some (q, Bound__Bound.Large), get_last_off off1)
      | Chg (q, b, on) -> (Some (q, b), get_last_on on)
      | Inf -> assert false (* absurd *)
      end
  
  module Increasing =
  struct
    type infr =
      | R of Why3__BuiltIn.real
      | I
    
    module Unary =
    struct
      type siop = Q.t -> Q.t
      
      let rec op_si1_cst' (op: siop) (l: t'') : t'' =
        match l with
        | Sin (q, l') -> Sin (op q, op_si1_cst' op l')
        | Chg (q, bv, l') -> Chg (op q, bv, op_si1_cst' op l')
        | Inf -> Inf
      
      let op_si1_cst (op: siop) (l: t) : t =
        match l with
        | On Inf -> On Inf
        | On l' -> On (op_si1_cst' op l')
        | Off Inf -> assert false (* absurd *)
        | Off l' -> Off (op_si1_cst' op l')
      
      let test__inv_sub (min_pos: Q.t) (l: t) : t =
        let fuq (x: Q.t) : Q.t =
          if Q.infix_lseq min_pos x
          then Q.infix_mn (Q.of_int Z.zero) (Q.infix_sl (Q.of_int Z.one) x)
          else Q.of_int Z.zero in
        let op = fuq in
        op_si1_cst op l
      
      let rec op_sd1_cst' (op: siop) (l: t'') (acc: t'') (if_: bool) : t =
        match l with
        | Sin (q, l') -> op_sd1_cst' op l' (Sin (op q, acc)) if_
        | Chg (q,
          bv,
          l') ->
          op_sd1_cst' op
          l'
          (Chg (op q, Bound__Bound.inv_bound bv, acc))
          (not if_)
        | Inf -> if if_ then On acc else Off acc
      
      let op_sd1_cst (op: siop) (l: t) : t =
        match l with
        | On Sin (q, l') -> op_sd1_cst' op l' (Sin (op q, Inf)) true
        | On Chg (q,
          bv,
          l') ->
          op_sd1_cst' op
          l'
          (Chg (op q, Bound__Bound.inv_bound bv, Inf))
          false
        | On Inf -> On Inf
        | Off l' -> op_sd1_cst' op l' Inf false
      
      let neg (l: t) : t =
        let negq (x: Q.t) : Q.t = Q.infix_mn (Q.of_int Z.zero) x in
        let op = negq in
        op_sd1_cst op l
      
      let mult_cst (c: Q.t) (l: t) : t =
        let fq (x: Q.t) : Q.t = Q.infix_as c x in
        let op = fq in
        match Q.compare (Q.of_int Z.zero) c with
        | Ord.Eq -> singleton (Q.of_int Z.zero)
        | Ord.Lt -> op_si1_cst op l
        | Ord.Gt -> op_sd1_cst op l
    end
    
    type presence =
      | Near of t''
      | Far of t''
    
    let rec split_at' (if_: bool) (l: t'') (acc: t'') :
      presence * (bool) * presence =
      match l with
      | Sin (q,
        l') ->
        begin match Q.compare q (Q.of_int Z.zero) with
        | Ord.Eq ->
          (let llt = if if_ then Near acc else Far acc in
           let lgt = if if_ then Near l' else Far l' in (llt, true, lgt))
        | Ord.Lt ->
          split_at' if_ l' (Sin (Q.infix_mn (Q.of_int Z.zero) q, acc))
        | Ord.Gt ->
          (let llt = if if_ then Near acc else Far acc in
           let lgt = if if_ then Near l else Far l in (llt, if_, lgt))
        end
      | Chg (q,
        bv,
        l') ->
        begin match Q.compare q (Q.of_int Z.zero) with
        | Ord.Eq ->
          (let llt = if if_ then Near acc else Far acc in
           let lgt = if if_ then Far l' else Near l' in
           let b =
             match bv with
             | Bound__Bound.Large -> not if_
             | Bound__Bound.Strict -> if_ in
           (llt, b, lgt))
        | Ord.Lt ->
          split_at' (not if_)
          l'
          (Chg (Q.infix_mn (Q.of_int Z.zero) q, Bound__Bound.inv_bound bv,
           acc))
        | Ord.Gt ->
          (let llt = if if_ then Near acc else Far acc in
           let lgt = if if_ then Near l else Far l in (llt, if_, lgt))
        end
      | Inf ->
        (let llt = if if_ then Near acc else Far acc in
         let lgt = if if_ then Near l else Far l in (llt, if_, lgt))
    
    let split_at (l: t) : presence * (bool) * presence =
      match l with
      | On l' ->
        begin match l' with
        | (Sin (q, _) | Chg (q, _, _)) ->
          if Q.infix_lseq q (Q.of_int Z.zero)
          then split_at' true l' Inf
          else (Near Inf, true, Near l')
        | Inf -> (Near Inf, true, Near Inf)
        end
      | Off l' ->
        begin match l' with
        | (Sin (q, _) | Chg (q, _, _)) ->
          if Q.infix_lseq q (Q.of_int Z.zero)
          then split_at' false l' Inf
          else (Far Inf, false, Far l')
        | Inf -> assert false (* absurd *)
        end
    
    let abs_forward (l: t) : t =
      let (neg,
      b,
      pos) =
      split_at l in
      let zero = Q.of_int Z.zero in
      let u =
        match (neg, pos) with
        | (Near neg1,
          Near pos1) ->
          (let u1 = union' true true neg1 pos1 in
           let b3 = if b then Bound__Bound.Large else Bound__Bound.Strict in
           Chg (zero, b3, u1))
        | (Far neg1,
          Near pos1) ->
          (let u1 = union' false true neg1 pos1 in
           let b3 = if b then Bound__Bound.Large else Bound__Bound.Strict in
           Chg (zero, b3, u1))
        | (Far neg1,
          Far pos1) ->
          (let u1 = union' false false neg1 pos1 in
           if b then Sin (zero, u1) else u1)
        | (Near neg1,
          Far pos1) ->
          (let u1 = union' true false neg1 pos1 in
           let b3 = if b then Bound__Bound.Large else Bound__Bound.Strict in
           Chg (zero, b3, u1)) in
      Off u
    
    let abs_backward (l: t) : t = union (Unary.neg l) l
    
    module Binary =
    struct
      type siop = Q.t -> Q.t -> Q.t
      
      let rec op_si_cst' (op: siop) (x: Q.t) (l: t'') : t'' =
        match l with
        | Sin (q, l') -> Sin (op x q, op_si_cst' op x l')
        | Chg (q, bv, l') -> Chg (op x q, bv, op_si_cst' op x l')
        | Inf -> Inf
      
      let op_si_cst (op: siop) (l: t) (x: Q.t) : t =
        match l with
        | On l1 -> On (op_si_cst' op x l1)
        | Off l1 -> (let l2 = op_si_cst' op x l1 in Off l2)
      
      let add_cst (l: t) (x: Q.t) : t =
        let op = Q.infix_pl in op_si_cst op l x
      
      let rec op_si_interval_on (op: siop) (bu: Bound__Bound.t) (u: Q.t)
                                (v: Q.t) (bv: Bound__Bound.t) (l: t'') : t''
        =
        match l with
        | Sin (_, l') -> op_si_interval_on op bu u v bv l'
        | Chg (qs,
          bq,
          l') ->
          (let br = both_strict bq bv in let r = op v qs in
           op_si_interval_off_remain op bu u v bv r br l')
        | Inf -> Inf
      and op_si_interval_off (op: siop) (bu: Bound__Bound.t) (u: Q.t)
                             (v: Q.t) (bv: Bound__Bound.t) (l: t'') : t'' =
        match l with
        | Sin (qs,
          l') ->
          (let r = op v qs in
           let res = op_si_interval_off_remain op bu u v bv r bv l' in
           Chg (op u qs, bu, res))
        | Chg (qs,
          b3,
          l') ->
          (let bbu = add_bound b3 bu in
           let res0 = op_si_interval_on op bu u v bv l' in
           Chg (op u qs, bbu, res0))
        | Inf -> Inf
      and op_si_interval_off_remain (op: siop) (bu: Bound__Bound.t) (u: Q.t)
                                    (v: Q.t) (bv: Bound__Bound.t) (r: Q.t)
                                    (br: Bound__Bound.t) (l: t'') : t'' =
        match l with
        | Sin (qs,
          l') ->
          (let qu = op u qs in let r' = op v qs in
           let res = op_si_interval_off_remain op bu u v bv r' bv l' in
           match Q.compare r qu with
           | Ord.Lt -> Chg (r, br, Chg (qu, bu, res))
           | Ord.Gt -> res
           | Ord.Eq ->
             begin match (br, bu) with
             | (Bound__Bound.Large, Bound__Bound.Strict) -> Sin (qu, res)
             | _ -> res
             end)
        | Chg (qs,
          b3,
          l') ->
          (let qu = op u qs in let bbu = add_bound b3 bu in
           let res = op_si_interval_on op bu u v bv l' in
           match Q.compare r qu with
           | Ord.Lt -> Chg (r, br, Chg (qu, bbu, res))
           | Ord.Gt -> res
           | Ord.Eq ->
             begin match (br, bbu) with
             | (Bound__Bound.Large, Bound__Bound.Strict) -> Sin (qu, res)
             | _ -> res
             end)
        | Inf -> Chg (r, br, Inf)
      
      let op_si_interval' (op: siop) (if_: bool) (l: t'')
                          (bu: Bound__Bound.t) (u: Q.t) (v: Q.t)
                          (bv: Bound__Bound.t) : t'' =
        if if_
        then op_si_interval_on op bu u v bv l
        else op_si_interval_off op bu u v bv l
      
      let op_si_interval (op: siop) (l: t) (bu: Bound__Bound.t) (u: Q.t)
                         (v: Q.t) (bv: Bound__Bound.t) : t =
        match l with
        | On l1 -> (let res = op_si_interval_on op bu u v bv l1 in On res)
        | Off l1 -> (let l2 = op_si_interval_off op bu u v bv l1 in Off l2)
      
      let rec op_si_minus_inf (op: siop) (if_: bool) (v: Q.t)
                              (bv: Bound__Bound.t) (l: t'') : t'' =
        match l with
        | Sin (qs, Inf) -> if if_ then Inf else Chg (op v qs, bv, Inf)
        | Sin (_, l') -> op_si_minus_inf op if_ v bv l'
        | Chg (qs,
          bs,
          Inf) ->
          if if_ then Chg (op v qs, add_bound bv bs, Inf) else Inf
        | Chg (_, _, l') -> op_si_minus_inf op (not if_) v bv l'
        | Inf -> Inf
      
      let rec op_si_on (op: siop) (if2: bool) (u: Q.t) (bu: Bound__Bound.t)
                       (l1: t'') (l2: t'') : t'' =
        match l1 with
        | Sin (qs,
          l1') ->
          (let res1 = op_si_interval' op if2 l2 bu u qs Bound__Bound.Large in
           let res2 = op_si_on op if2 qs Bound__Bound.Strict l1' l2 in
           union' if2 if2 res1 res2)
        | Chg (qs, bq, Inf) -> op_si_interval' op if2 l2 bu u qs bq
        | Chg (qs,
          bq,
          l1') ->
          (let res1 = op_si_interval' op if2 l2 bu u qs bq in
           let res2 = op_si_off op if2 l1' l2 in union' if2 if2 res1 res2)
        | Inf ->
          if if2
          then Inf
          else
            begin match l2 with
            | Sin (q22, _) -> Chg (op u q22, bu, Inf)
            | Chg (q22, b22, _) -> Chg (op u q22, add_bound bu b22, Inf)
            | Inf -> Inf
            end
      and op_si_off (op: siop) (if2: bool) (l1: t'') (l2: t'') : t'' =
        match l1 with
        | Sin (qs, Inf) -> op_si_cst' op qs l2
        | Sin (qs,
          l1') ->
          (let res1 = op_si_cst' op qs l2 in
           let res2 = op_si_off op if2 l1' l2 in union' if2 if2 res1 res2)
        | Chg (qs, b3, l1') -> op_si_on op if2 qs b3 l1' l2
        | Inf -> assert false (* absurd *)
      
      let op_si (op: siop) (u: t) (v: t) : t =
        let op_si_on' (op1: siop) (if2: bool) (l1: t'') (l2: t'') : t'' =
          match l1 with
          | Sin (qs,
            l1') ->
            (let res1 = op_si_minus_inf op1 if2 qs Bound__Bound.Large l2 in
             let res2 = op_si_on op1 if2 qs Bound__Bound.Strict l1' l2 in
             union' true if2 res1 res2)
          | Chg (qs, bq, Inf) -> op_si_minus_inf op1 if2 qs bq l2
          | Chg (qs,
            bq,
            l1') ->
            (let res1 = op_si_minus_inf op1 if2 qs bq l2 in
             let res2 = op_si_off op1 if2 l1' l2 in
             union' true if2 res1 res2)
          | Inf -> Inf in
        match (u, v) with
        | (On u1, On v1) -> On (op_si_on' op true u1 v1)
        | (On u1, Off v1) -> On (op_si_on' op false u1 v1)
        | (Off u1, On v1) -> On (op_si_off op true u1 v1)
        | (Off u1, Off v1) -> (let w = op_si_off op false u1 v1 in Off w)
      
      let add (u: t) (v: t) : t = let op = Q.infix_pl in op_si op u v
      
      let rec find_max (if_: bool) (l: t'') : (Bound__Bound.t * Q.t) option =
        match l with
        | Sin (q, Inf) -> if if_ then None  else Some (Bound__Bound.Large, q)
        | Sin (_, l1) -> find_max if_ l1
        | Chg (q,
          bv,
          Inf) ->
          if if_ then Some (Bound__Bound.inv_bound bv, q) else None 
        | Chg (_, _, l1) -> find_max (not if_) l1
        | Inf -> None 
      
      let hd_on (w: t'') : Q.t * Bound__Bound.t * t'' =
        match w with
        | Inf -> assert false (* absurd *)
        | Sin (q,
          l) ->
          (q, Bound__Bound.Strict, Chg (q, Bound__Bound.Strict, l))
        | Chg (q, bv, l) -> (q, Bound__Bound.inv_bound bv, l)
      
      let mul (u: t) (v: t) : t =
        let mul_off (w: t'') (x: t'') : t'' =
          match (w, x) with
          | ((Inf, _) | (_, Inf)) -> Inf
          | ((Sin (q12, _) | Chg (q12, _, _)),
            (Sin (q22, _) | Chg (q22, _, _))) ->
            (let op = Q.infix_as in op_si_off op false w x) in
        let mul_near_zero (if_w: bool) (w: t'') (xq: Q.t)
                          (xb: Bound__Bound.t) : t'' =
          match find_max if_w w with
          | None -> Chg (Q.of_int Z.zero, Bound__Bound.Strict, Inf)
          | Some (wmb, wm) ->
            Chg (Q.of_int Z.zero,
            Bound__Bound.Strict,
            Chg (Q.infix_as wm xq,
            Bound__Bound.inv_bound (add_bound wmb xb),
            Inf)) in
        let mul_aux (w: presence) (x: presence) : t'' =
          match (w, x) with
          | ((Near Inf, _) | (_, Near Inf)) ->
            Chg (Q.of_int Z.zero, Bound__Bound.Strict, Inf)
          | ((Far Inf, _) | (_, Far Inf)) -> Inf
          | (Near w1,
            Near x1) ->
            (let (wq, wb, wl) = hd_on w1 in
             let (xq,
             xb,
             xl) =
             hd_on x1 in
             let l1 = mul_near_zero true w1 xq xb in
             let l2 = mul_near_zero true x1 wq wb in
             let l3 = mul_off wl xl in
             union' false false (union' false false l1 l2) l3)
          | (Far w1, Far x1) -> mul_off w1 x1
          | ((Near w1, Far x1) | (Far x1, Near w1)) ->
            (let (wq1, wb1, wl1) = hd_on w1 in
             let l2 = mul_near_zero false x1 wq1 wb1 in
             let l3 = mul_off wl1 x1 in
             union' false false l2 l3) in
        let neg' (l: t'') : t =
          let negq1 (x: Q.t) : Q.t = Q.infix_mn (Q.of_int Z.zero) x in
          let op = negq1 in
          Unary.op_sd1_cst' op l Inf false in
        let (uinf,
        zu,
        upos) =
        split_at u in
        let (vinf,
        zv,
        vpos) =
        split_at v in
        let u1 = union' false false (mul_aux uinf vinf) (mul_aux upos vpos) in
        let u2 = union' false false (mul_aux uinf vpos) (mul_aux upos vinf) in
        let z = Off (Sin (Q.of_int Z.zero, Inf)) in
        match (u1, u2) with
        | (Inf, Inf) -> z
        | (Inf,
          u21) ->
          (let u22 = neg' u21 in if zu || zv then union u22 z else u22)
        | (u11,
          Inf) ->
          (let u12 = Off u11 in if zu || zv then union u12 z else u12)
        | (u11,
          u21) ->
          (let u12 = Off u11 in let u22 = neg' u21 in
           let r = union u12 u22 in if zu || zv then union r z else r)
      
      let inv (u: t) : t =
        let inv_pos_off (wl2: t'') : t'' =
          match wl2 with
          | Inf -> Inf
          | _ ->
            (let uq' (q: Q.t) : Q.t =
               if Q.infix_eq q (Q.of_int Z.zero)
               then q
               else Q.infix_sl (Q.of_int Z.one) q in
             let op = uq' in let v = Unary.op_sd1_cst' op wl2 Inf false in
             match v with
             | Off v1 -> v1
             | On v' -> Chg (Q.of_int Z.zero, Bound__Bound.Strict, v')) in
        let inv_pos (u1: presence) : t'' =
          match u1 with
          | Near Inf -> Chg (Q.of_int Z.zero, Bound__Bound.Strict, Inf)
          | Near w ->
            (let (wq2, wb2, wl2) = hd_on w in
             let wq' = Q.infix_sl (Q.of_int Z.one) wq2 in
             let acc = Chg (wq', wb2, Inf) in
             let acc' = inv_pos_off wl2 in
             union' false false acc acc')
          | Far w -> inv_pos_off w in
        let (uinf1,
        zu1,
        upos1) =
        split_at u in
        if zu1
        then On Inf
        else
          begin
            let uinf2 = inv_pos uinf1 in let upos2 = inv_pos upos1 in
            match (upos2, uinf2) with
            | (Inf, Inf) -> assert false (* absurd *)
            | (Inf, u2) -> Unary.neg (Off u2)
            | (u1, Inf) -> Off u1
            | (u1, u2) -> union (Unary.neg (Off u2)) (Off u1) end
      
      let div (u: t) (v: t) : t =
        if mem (Q.of_int Z.zero) v
        then On Inf
        else begin let v' = inv v in mul u v' end
    end
    end
    
    module NonIncreasing =
    struct
      type unary = Q.t -> Q.t
      
      let rec op_nsi1_sin (start: Q.t) (if_: bool) (op: unary) (l: t'') : t''
        =
        match l with
        | Sin (q,
          l') ->
          (let qop = op q in
           if Q.infix_eq start qop
           then op_nsi1_sin start if_ op l'
           else
             begin
               if if_
               then Chg (start, Bound__Bound.Large, op_nsi1_wait if_ op l')
               else Sin (start, op_nsi1_sin qop if_ op l') end)
        | Chg (q,
          _,
          l') ->
          (let qop = op q in
           if Q.infix_eq start qop
           then op_nsi1_sin start (not if_) op l'
           else
             begin
               if if_
               then Chg (start, Bound__Bound.Large, op_nsi1_inter qop op l')
               else Sin (start, op_nsi1_sin qop (not if_) op l') end)
        | Inf ->
          if if_
          then Chg (start, Bound__Bound.Large, Inf)
          else Sin (start, Inf)
      and op_nsi1_inter (stop: Q.t) (op: unary) (l: t'') : t'' =
        match l with
        | Sin (q,
          l') ->
          (let qop = op q in
           if Q.infix_eq stop qop
           then op_nsi1_inter stop op l'
           else Chg (stop, Bound__Bound.Strict, op_nsi1_sin qop false op l'))
        | Chg (q,
          _,
          l') ->
          (let qop = op q in
           if Q.infix_eq stop qop
           then op_nsi1_wait true op l'
           else Chg (stop, Bound__Bound.Strict, op_nsi1_sin qop true op l'))
        | Inf -> Chg (stop, Bound__Bound.Strict, Inf)
      and op_nsi1_wait (if_: bool) (op: unary) (l: t'') : t'' =
        match l with
        | Sin (q,
          l') ->
          (let qop = op q in
           if if_
           then op_nsi1_wait if_ op l'
           else op_nsi1_sin qop false op l')
        | Chg (q,
          _,
          l') ->
          (let qop = op q in
           if if_
           then op_nsi1_inter qop op l'
           else op_nsi1_sin qop true op l')
        | Inf -> Inf
      
      let op_nsi1 (op: unary) (l: t) : t =
        match l with
        | On l1 -> On (op_nsi1_wait true op l1)
        | Off l1 -> (let l2 = op_nsi1_wait false op l1 in Off l2)
      
      let ceil (l: t) : t =
        let qceil (x: Q.t) : Q.t = Q.of_int (Q.ceil x) in
        let op = qceil in
        op_nsi1 op l
      
      let floor (l: t) : t =
        let qfloor (x: Q.t) : Q.t = Q.of_int (Q.floor x) in
        let op = qfloor in
        op_nsi1 op l
      
      let truncate (l: t) : t =
        let q (x: Q.t) : Q.t = Q.of_int (Q.truncate x) in
        let op = q in
        op_nsi1 op l
      
      let relu (l: t) : t =
        let zero = Q.of_int Z.zero in
        let qrelu (x: Q.t) : Q.t = if Q.infix_lseq zero x then x else zero in
        let op = qrelu in
        op_nsi1 op l
    end
    
    module Map =
    struct
      type qbound =
        | BInf
        | BLarge of Q.t
        | BStrict of Q.t
      
      type interval =
        | Singleton of Q.t
        | Inter of qbound * qbound
      
      type res_interval =
        | OneInt of interval
        | TwoInt of interval * interval
        | ThreeInt of interval * interval * interval
      
      let qbound_of_bound (q3: Q.t) (b3: Bound__Bound.t) : qbound =
        match b3 with
        | Bound__Bound.Strict -> BStrict q3
        | Bound__Bound.Large -> BLarge q3
      
      let bound_of_qbound (b3: qbound) : Bound__Bound.t * Q.t =
        match b3 with
        | BStrict q3 -> (Bound__Bound.Strict, q3)
        | BLarge q3 -> (Bound__Bound.Large, q3)
        | BInf -> assert false (* absurd *)
      
      let convert_interval (i: interval) : t' =
        match i with
        | Singleton q3 -> Off (Sin (q3, Inf))
        | Inter (BInf, BInf) -> On Inf
        | Inter (BInf,
          bq) ->
          (let (b3, q3) = bound_of_qbound bq in
           On (Chg (q3, Bound__Bound.inv_bound b3, Inf)))
        | Inter (bq,
          BInf) ->
          (let (b4, q4) = bound_of_qbound bq in Off (Chg (q4, b4, Inf)))
        | Inter (bq1,
          bq2) ->
          (let (b12, q12) = bound_of_qbound bq1 in
           let (b22,
           q22) =
           bound_of_qbound bq2 in
           match (Q.compare q12 q22, b12, b22) with
           | (Ord.Eq,
             Bound__Bound.Large,
             Bound__Bound.Large) ->
             Off (Sin (q12, Inf))
           | (Ord.Lt,
             _,
             _) ->
             Off (Chg (q12, b12, Chg (q22, Bound__Bound.inv_bound b22, Inf)))
           | _ -> Off Inf)
      
      let convert_res_interval (i: res_interval) : t' =
        match i with
        | OneInt i1 -> convert_interval i1
        | TwoInt (i1,
          i2) ->
          union_a (convert_interval i1) (convert_interval i2)
        | ThreeInt (i1,
          i2,
          i3) ->
          union_a (union_a (convert_interval i1) (convert_interval i2))
          (convert_interval i3)
      
      type fmap = interval -> interval -> res_interval
      
      let map (fm: fmap) (l1: t) (l2: t) : t =
        let rec map_cst_left_on (i1: interval) (q23: qbound) (l21: t'') : t'
          =
          match l21 with
          | Inf ->
            (let u = fm i1 (Inter (q23, BInf)) in convert_res_interval u)
          | Sin (q5,
            l22) ->
            (let q31 = BStrict q5 in let u = fm i1 (Inter (q23, q31)) in
             union_a (convert_res_interval u) (map_cst_left_on i1 q31 l22))
          | Chg (q5,
            b5,
            l22) ->
            (let u =
               fm i1 (Inter (q23,
                      qbound_of_bound q5 (Bound__Bound.inv_bound b5))) in
             union_a (convert_res_interval u) (map_cst_left_off i1 l22))
        and map_cst_left_off (i1: interval) (l21: t'') : t' =
          match l21 with
          | Inf -> Off Inf
          | Sin (q5,
            l22) ->
            (let u = fm i1 (Singleton q5) in
             union_a (convert_res_interval u) (map_cst_left_off i1 l22))
          | Chg (q5,
            b5,
            l22) ->
            map_cst_left_on i1 (qbound_of_bound q5 b5) l22 in
        let map_cst_left (i1: interval) (l21: t) : t' =
          match l21 with
          | On l22 -> map_cst_left_on i1 BInf l22
          | Off l22 -> map_cst_left_off i1 l22 in
        let rec map_cst_on (q13: qbound) (l11: t'') (l21: t) : t' =
          match l11 with
          | Inf -> map_cst_left (Inter (q13, BInf)) l21
          | Sin (q5,
            l1') ->
            (let q23 = BStrict q5 in
             let u1 = map_cst_left (Inter (q13, q23)) l21 in
             let u2 = map_cst_on q23 l1' l21 in union_a u1 u2)
          | Chg (q5,
            b5,
            l1') ->
            (let u1 =
               map_cst_left (Inter (q13,
                             qbound_of_bound q5 (Bound__Bound.inv_bound b5)))
               l21 in
             let u2 = map_cst_off l1' l21 in union_a u1 u2)
        and map_cst_off (l11: t'') (l21: t) : t' =
          match l11 with
          | Inf -> Off Inf
          | Sin (q5,
            l1') ->
            (let u1 = map_cst_left (Singleton q5) l21 in
             let u2 = map_cst_off l1' l21 in union_a u1 u2)
          | Chg (q5, b5, l1') -> map_cst_on (qbound_of_bound q5 b5) l1' l21 in
        match l1 with
        | On l11 -> (let m = map_cst_on BInf l11 l2 in m)
        | Off l11 -> (let m = map_cst_off l11 l2 in m)
    end end
    
