open Ord

let compare a b =
  let c = Q.compare a b in
  if c = 0 then Eq
  else if c <= 0
  then Lt
  else Gt
