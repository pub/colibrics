let floor (a: Q.t) : Z.t = (Z.fdiv (a.Q.num) (a.Q.den))

let ceil (a: Q.t) : Z.t = (Z.cdiv (a.Q.num) (a.Q.den))

