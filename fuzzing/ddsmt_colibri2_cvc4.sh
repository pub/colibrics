#!/bin/bash

trap 'kill $(jobs -p)' SIGABRT

colibri2 --lang=smt2.6 --time=5s $1
cvc5 --tlimit=5000 $1
